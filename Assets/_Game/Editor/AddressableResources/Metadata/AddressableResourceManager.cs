﻿using Game.Core.DependencyInjection;
using Game.Editor.Core;
using Game.Editor.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace Game.Editor.Resource.Addressable
{
    /// <inheritdoc cref="IEditorAssetResourceManager"/>
    [Dependency(
        contract: typeof(IEditorAssetResourceManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class AddressableResourceManager : BaseEditorModule, IEditorAssetResourceManager
    {
        private AddressableAssetSettings _addressableSettings;

        private List<AddressableAssetEntry> _addressableAssetEntriesList;

        /// <inheritdoc />
        public override void Initialize()
        {
            base.Initialize();

            _addressableSettings = AddressableAssetSettingsDefaultObject.Settings;

            _addressableAssetEntriesList = new List<AddressableAssetEntry>();
            _addressableSettings.GetAllAssets(_addressableAssetEntriesList, false);

            _addressableSettings.OnModification += OnAddressablesModified;
        }

        private void OnAddressablesModified(AddressableAssetSettings settings, AddressableAssetSettings.ModificationEvent modificationEvent, object modifiedObject)
        {
            if (modificationEvent == AddressableAssetSettings.ModificationEvent.EntryAdded
                || modificationEvent == AddressableAssetSettings.ModificationEvent.EntryCreated
                || modificationEvent == AddressableAssetSettings.ModificationEvent.EntryModified
                || modificationEvent == AddressableAssetSettings.ModificationEvent.EntryMoved
                || modificationEvent == AddressableAssetSettings.ModificationEvent.EntryRemoved)
            {
                _addressableAssetEntriesList.Clear();
                _addressableSettings.GetAllAssets(_addressableAssetEntriesList, false);
            }
        }

        /// <inheritdoc />
        public bool IsLoadableAsset(UnityEngine.Object asset)
        {
            return GetAddressEntryForAsset(asset) != null;
        }

        /// <inheritdoc />
        public string GetAddressForAsset(UnityEngine.Object asset)
        {
            var addressEntry = GetAddressEntryForAsset(asset);
            if (addressEntry == null)
            {
                return string.Empty;
            }
            else
            {
                return addressEntry.address;
            }
        }

        /// <inheritdoc />
        public string GetAddressForAsset(string assetPath)
        {
            var addressEntry = GetAddressEntryForAssetPath(assetPath);
            if (addressEntry == null)
            {
                return string.Empty;
            }
            else
            {
                return addressEntry.address;
            }
        }

        /// <inheritdoc />
        public bool IsAddressValid(string address)
        {
            return _addressableAssetEntriesList.FirstOrDefault(a => a.address == address) != null;
        }

        /// <inheritdoc />
        public GUID[] GetAllAssetsForLabel(string label)
        {
            var assetEntries = new List<AddressableAssetEntry>();
            _addressableSettings.GetAllAssets(assetEntries, false, entryFilter: entry => entry.labels.Contains(label));

            var guids = new List<GUID>();
            foreach (var entry in assetEntries)
            {
                guids.Add(new GUID(entry.guid));
            }

            return guids.ToArray();
        }

        /// <inheritdoc />
        public Type GetMainTypeOfAssetAtAddress(string address)
        {
            var assetEntry = _addressableAssetEntriesList.FirstOrDefault(a => a.address == address);
            if (assetEntry == null)
            {
                return null;
            }

            return AssetDatabase.GetMainAssetTypeAtPath(assetEntry.AssetPath);
        }

        /// <inheritdoc />
        public T LoadAssetByAddress<T>(string address) where T : UnityEngine.Object
        {
            var assetEntry = _addressableAssetEntriesList.FirstOrDefault(a => a.address == address);
            if (assetEntry == null)
            {
                Debug.LogWarning($"Address '{address}' does not point to an asset");
                return null;
            }

            return AssetDatabase.LoadAssetAtPath<T>(assetEntry.AssetPath);
        }

        private AddressableAssetEntry GetAddressEntryForAsset(UnityEngine.Object asset)
        {
            return GetAddressEntryForAssetPath(AssetDatabase.GetAssetPath(asset));
        }
        private AddressableAssetEntry GetAddressEntryForAssetPath(string assetPath)
        {
            return _addressableSettings.FindAssetEntry(AssetDatabase.AssetPathToGUID(assetPath));
        }
    }
}
