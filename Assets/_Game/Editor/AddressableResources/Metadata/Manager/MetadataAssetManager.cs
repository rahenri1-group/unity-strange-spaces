﻿using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Editor.Core;
using Game.Editor.Core.Resource;
using System.Linq;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace Game.Editor.Resource.Addressable
{
    /// <inheritdoc cref="IMetadataAssetManager"/>
    [Dependency(
        contract: typeof(IMetadataAssetManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class MetadataAssetManager : BaseEditorModule, IMetadataAssetManager
    {
        private const string MetadataAssetFolderPath = "Assets/_Game/ScriptableObjects/Metadata/";
        private const string MetadataAddressableGroupName = "Metadata";
        private const string MetadataAddressableLabel = "Metadata";
        private const string MetadataAddressableSuffix = " - Metadata";

        private AddressableAssetSettings _addressableSettings;

        /// <inheritdoc/>
        public override void Initialize()
        {
            base.Initialize();

            _addressableSettings = AddressableAssetSettingsDefaultObject.Settings;
        }

        /// <inheritdoc/>
        public bool MetadataExists(string assetPath)
        {
            var assetAddress = GetAddressForAsset(assetPath);
            if (assetAddress == null)
            {
                return false;
            }

            var metadataAssetGroup = MetadataAddressableGroup();
            var metadataAddress = CreateMetadataAddressForResource(assetAddress);
            if (metadataAssetGroup.entries.Any(a => a.address == metadataAddress))
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public MetadataObject CreateMetadataForResource(string assetPath)
        {
            if (MetadataExists(assetPath))
            {
                return GetMetadataForResource(assetPath);
            }

            var assetAddress = GetAddressForAsset(assetPath);

            var metadata = ScriptableObject.CreateInstance<MetadataObject>();
            metadata.ResourceKey = assetAddress.address;

            var metadataAssetPath = $"{MetadataAssetFolderPath}{assetAddress.address}{MetadataAddressableSuffix}.asset";
            AssetDatabaseUtil.CreateAssetAndFolders(metadata, metadataAssetPath);

            var metadataGroup = MetadataAddressableGroup();

            var metadataEntry = _addressableSettings.CreateOrMoveEntry(AssetDatabase.AssetPathToGUID(metadataAssetPath), metadataGroup, false, false);
            metadataEntry.address = CreateMetadataAddressForResource(assetAddress);
            metadataEntry.labels.Add(MetadataAddressableLabel);

            return metadata;
        }

        /// <inheritdoc/>
        public MetadataObject GetMetadataForResource(string assetPath)
        {
            var metadataAssetGroup = MetadataAddressableGroup();

            var assetAddress = GetAddressForAsset(assetPath);
            if (assetAddress == null)
            {
                return null;
            }

            var metadataAddress = CreateMetadataAddressForResource(assetAddress);

            var metadata = metadataAssetGroup.entries.FirstOrDefault(a => a.address == metadataAddress);
            if (metadata != null)
            {
                return metadata.MainAsset as MetadataObject;
            }
            else
            {
                return null;
            }
        }
        
        private AddressableAssetEntry GetAddressForAsset(string assetPath)
        {
            return _addressableSettings.FindAssetEntry(AssetDatabase.AssetPathToGUID(assetPath));
        }


        private AddressableAssetGroup MetadataAddressableGroup()
        {
            return _addressableSettings.FindGroup(MetadataAddressableGroupName);
        }

        private string CreateMetadataAddressForResource(AddressableAssetEntry resourceEntry)
        {
            return resourceEntry.address + MetadataAddressableSuffix;
        }
    }
}
