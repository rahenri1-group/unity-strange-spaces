using Game.Editor.Core;
using Game.Editor.Core.Resource;
using UnityEditor;

namespace Game.Editor.Resource.Addressable
{
    /// <summary>
    /// Menu options for resource metadata
    /// </summary>
    public static class ResourceMetadataMenuItems
    {
        private static IEditorAssetResourceManager AssetResourceManager
        {
            get
            {
                if (_assetResourceManager == null) _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();
                return _assetResourceManager;
            }
        }
        private static IEditorAssetResourceManager _assetResourceManager = null;

        private static IMetadataAssetManager MetadataAssetManager
        {
            get
            {
                if (_metadataAssetManager == null) _metadataAssetManager = EditorModuleContext.DiContainer.Resolve<IMetadataAssetManager>();
                return _metadataAssetManager;
            }
        }
        private static IMetadataAssetManager _metadataAssetManager;

        [MenuItem("Assets/Resource Metadata/Create")]
        private static void CreateMetadataForResource()
        {
            Selection.activeObject = MetadataAssetManager.CreateMetadataForResource(AssetDatabase.GetAssetPath(Selection.activeObject));
        }

        [MenuItem("Assets/Resource Metadata/Create", true)]
        private static bool CreateMetadataForResourceValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }

            if (!AssetResourceManager.IsLoadableAsset(Selection.activeObject)) 
            {
                return false;
            }

            return !MetadataAssetManager.MetadataExists(AssetDatabase.GetAssetPath(Selection.activeObject));
        }

        [MenuItem("Assets/Resource Metadata/Modify")]
        private static void ModifyMetadataForResource()
        {
            Selection.activeObject = MetadataAssetManager.GetMetadataForResource(AssetDatabase.GetAssetPath(Selection.activeObject));        
        }

        [MenuItem("Assets/Resource Metadata/Modify", true)]
        private static bool ModifyMetadataForResourceValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }

            return MetadataAssetManager.MetadataExists(AssetDatabase.GetAssetPath(Selection.activeObject));
        }

        [MenuItem("Assets/Resource Metadata/Show")]
        private static void ShowMetadataForResource()
        {
            var metadata = MetadataAssetManager.GetMetadataForResource(AssetDatabase.GetAssetPath(Selection.activeObject));

            Selection.activeObject = metadata;
            EditorGUIUtility.PingObject(metadata);
        }

        [MenuItem("Assets/Resource Metadata/Show", true)]
        private static bool ShowMetadataForResourceValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }

            return MetadataAssetManager.MetadataExists(AssetDatabase.GetAssetPath(Selection.activeObject));
        }
    }
}
