﻿using Game.Core.AI.BehaviorTree;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BehaviorTreeDefinitionGraphView : BehaviorTreeGraphView
    {
        public BehaviorTreeDefinitionGraphView(BehaviorTreeDefinitionObject behaviorTreeObject)
            : base(behaviorTreeObject)
        {
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            BuildBlackboard();
            BuildEditableGraph();

            SetupResizeCallback();
        }

        private void BuildBlackboard()
        {
            Blackboard = new BehaviorTreeDefinitionBlackboard(this);
            Add(Blackboard);

            foreach (var definition in BehaviorTreeObject.BlackboardEntries)
            {
                Blackboard.AddEntryDefinition(definition);
            }
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);

            var remainTargets = evt.menu.MenuItems().FindAll(e =>
            {
                switch (e)
                {
                    case GraphDropdownAction a: return true;
                    case DropdownMenuAction a: return a.name == "Delete";
                    default: return false;
                }
            });

            evt.menu.MenuItems().Clear();

            if (evt.target == this)
            {
                evt.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Add New Leaf Node", action =>
                        {
                            var provider = ScriptableObject.CreateInstance<LeafNodeSearchWindowProvider>();
                            provider.Initialize(this, contentViewContainer.WorldToLocal(action.eventInfo.mousePosition));
                            var mousePosition = GUIUtility.GUIToScreenPoint(action.eventInfo.mousePosition);
                            SearchWindow.Open(new SearchWindowContext(mousePosition), provider);
                        }
                    )
                );

                evt.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Add New Decorator Node", action =>
                        {
                            var provider = ScriptableObject.CreateInstance<DecoratorNodeSearchWindowProvider>();
                            provider.Initialize(this, contentViewContainer.WorldToLocal(action.eventInfo.mousePosition));
                            var mousePosition = GUIUtility.GUIToScreenPoint(action.eventInfo.mousePosition);
                            SearchWindow.Open(new SearchWindowContext(mousePosition), provider);
                        }
                    )
                );

                evt.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Add New Composite Node", action =>
                        {
                            var provider = ScriptableObject.CreateInstance<CompositeNodeSearchWindowProvider>();
                            provider.Initialize(this, contentViewContainer.WorldToLocal(action.eventInfo.mousePosition));
                            var mousePosition = GUIUtility.GUIToScreenPoint(action.eventInfo.mousePosition);
                            SearchWindow.Open(new SearchWindowContext(mousePosition), provider);
                        }
                    )
                );
            }

            remainTargets.ForEach(evt.menu.MenuItems().Add);
        }

        public override bool Validate(out string errorMessage)
        {
            errorMessage = string.Empty;

            foreach (var n in nodes)
            {
                var node = (BaseNode)n;

                if (!node.ValidateNode(out errorMessage))
                {
                    return false;
                }
            }

            return true;
        }

        public override void Save()
        {
            BehaviorTreeObject.EntryNodePosition = EntryNode.GetPosition().position;
            BehaviorTreeObject.EntryNodeId = EntryNode.EntryNodeId;

            UpdateNodePriorities();

            var leafNodes = new List<LeafNodeDefinition>();
            var decoratorNodes = new List<DecoratorNodeDefinition>();
            var compositeNodes = new List<CompositeNodeDefinition>();

            foreach (var node in nodes)
            {
                if (node is LeafNode)
                {
                    var leafNode = (LeafNode)node;
                    leafNode.ApplyChangesToDefinition();
                    leafNodes.Add(leafNode.Definition);
                }
                else if (node is DecoratorNode)
                {
                    var decoratorNode = (DecoratorNode)node;
                    decoratorNode.ApplyChangesToDefinition();
                    decoratorNodes.Add(decoratorNode.Definition);
                }
                else if (node is CompositeNode)
                {
                    var compositeNode = (CompositeNode)node;
                    compositeNode.ApplyChangesToDefinition();
                    compositeNodes.Add(compositeNode.Definition);
                }
            }

            BehaviorTreeObject.LeafNodes = leafNodes
                .OrderBy(n => n.Id)
                .ToArray();

            BehaviorTreeObject.DecoratorNodes = decoratorNodes
                .OrderBy(n => n.Id)
                .ToArray();

            BehaviorTreeObject.CompositeNodes = compositeNodes
                .OrderBy(n => n.Id)
                .ToArray();

            var blackboardEntries = new List<BlackboardEntryDefinition>();
            foreach (var row in Blackboard.Rows)
            {
                row.ApplyChangesToDefinition();
                blackboardEntries.Add(row.Definition);
            }

            BehaviorTreeObject.BlackboardEntries = blackboardEntries.ToArray();

            EditorUtility.SetDirty(BehaviorTreeObject);
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var availablePorts = ports.Where(port => port != startPort && startPort.node != port.node && port.direction != startPort.direction).ToList();

            return availablePorts;
        }

        private void UpdateNodePriorities()
        {
            BaseBehaviorNode entry = null;
            foreach (var node in nodes)
            {
                if (node is BaseBehaviorNode)
                {
                    var behaviorNode = (BaseBehaviorNode)node;
                    behaviorNode.Priority = 0;

                    if (EntryNode.EntryNodeId == behaviorNode.NodeId)
                    {
                        entry = behaviorNode;
                    }
                }
            }

            if (entry != null)
            {
                int nextNodePriority = 0;
                UpdateNodePriority(entry, ref nextNodePriority);
            }
        }

        private void UpdateNodePriority(BaseBehaviorNode node, ref int priority)
        {
            node.Priority = priority;
            priority += 1;

            foreach (var children in node.ChildNodes)
            {
                UpdateNodePriority(children, ref priority);
            }
        }
    }
}
