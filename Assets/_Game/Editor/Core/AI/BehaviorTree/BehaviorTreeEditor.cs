﻿using Game.Core.AI.BehaviorTree;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    [CustomEditor(typeof(BehaviorTreeDefinitionObject))]
    public class BehaviorTreeEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Open Behavior Tree"))
            {
                var behaviorTree = target as BehaviorTreeDefinitionObject;
                BehaviorTreeEditorWindow.Show(behaviorTree);
            }
        }
    }
}
