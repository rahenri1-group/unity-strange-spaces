﻿using Game.Core.AI.BehaviorTree;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BehaviorTreeEditorWindow : EditorWindow
    {
        private static readonly Dictionary<BehaviorTreeDefinitionObject, BehaviorTreeEditorWindow> _windowCache = new Dictionary<BehaviorTreeDefinitionObject, BehaviorTreeEditorWindow>();

        public BehaviorTreeDefinitionObject BehaviorTreeObject { get; private set; }
        public IBehaviorTreeRunner BehaviorTreeRunner { get; private set; }

        private BehaviorTreeGraphView _graphView;

        public static void Show(BehaviorTreeDefinitionObject behaviorTreeObject)
        {
            var window = Create(behaviorTreeObject);
            window.Show();
            window.Focus();
        }

        public static void ShowForRunner(BehaviorTreeDefinitionObject behaviorTreeObject, IBehaviorTreeRunner runner)
        {
            var window = Create(behaviorTreeObject, runner);
            window.Show();
            window.Focus();
        }

        private static BehaviorTreeEditorWindow Create(BehaviorTreeDefinitionObject behaviorTreeObject, IBehaviorTreeRunner runner = null)
        {
            if (_windowCache.ContainsKey(behaviorTreeObject))
            {
                return _windowCache[behaviorTreeObject];
            }

            var window = CreateInstance<BehaviorTreeEditorWindow>();
            window.BehaviorTreeObject = behaviorTreeObject;
            window.BehaviorTreeRunner = runner;
            window.titleContent = new GUIContent($"BehaviorTree Editor {behaviorTreeObject.name}");
            window.SetupView();

            _windowCache[behaviorTreeObject] = window;

            return window;
        }

        private void OnDestroy()
        {
            _graphView.OnDestroy();

            if (BehaviorTreeObject != null && _windowCache.ContainsKey(BehaviorTreeObject))
            {
                _windowCache.Remove(BehaviorTreeObject);
            }
        }

        private void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            Reload();
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        private void OnPlayModeStateChanged(PlayModeStateChange playModeStateChange)
        {
            switch (playModeStateChange)
            {
                case PlayModeStateChange.EnteredEditMode:
                    Reload();
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    Reload();
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    break;
                default:
                    Debug.LogError($"Unknown state {playModeStateChange}");
                    break;
            }
        }

        private void Reload()
        {
            if (!Application.isPlaying)
            {
                BehaviorTreeRunner = null;
            }

            if (BehaviorTreeObject != null)
            {
                SetupView();
                Repaint();
            }
        }

        private void SetupView()
        {
            rootVisualElement.Clear();

            if (BehaviorTreeRunner == null)
            {
                _graphView = new BehaviorTreeDefinitionGraphView(BehaviorTreeObject);
            }
            else
            {
                _graphView = new BehaviorTreeRunnerGraphView(BehaviorTreeObject, BehaviorTreeRunner);
            }

            rootVisualElement.Add(CreateToolBar());
            rootVisualElement.Add(_graphView);
        }

        private VisualElement CreateToolBar()
        {
            return new IMGUIContainer(
                () =>
                {
                    GUILayout.BeginHorizontal(EditorStyles.toolbar);

                    if (!Application.isPlaying)
                    {
                        if (GUILayout.Button("Save", EditorStyles.toolbarButton))
                        {
                            var guiContent = new GUIContent();

                            if (_graphView.Validate(out string errorMessage))
                            {
                                _graphView.Save();
                                guiContent.text = $"Successfully updated {BehaviorTreeObject.name}";
                            }
                            else
                            {
                                guiContent.text = $"Could not save, {errorMessage}";
                            }

                            ShowNotification(guiContent);
                        }
                    }

                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
            );
        }
    }
}
