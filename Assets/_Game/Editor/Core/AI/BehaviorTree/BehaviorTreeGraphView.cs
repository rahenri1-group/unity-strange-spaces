﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public abstract class BehaviorTreeGraphView : GraphView
    {
        public BehaviorTreeBlackboard Blackboard { get; protected set; }

        protected BehaviorTreeDefinitionObject BehaviorTreeObject { get; private set; }
        protected EntryNode EntryNode { get; private set; }

        public BehaviorTreeGraphView(BehaviorTreeDefinitionObject behaviorTreeObject)
        {
            BehaviorTreeObject = behaviorTreeObject;

            style.flexGrow = 1;
            style.flexShrink = 1;

            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            Insert(0, new GridBackground());
        }

        public virtual void OnDestroy() {}

        public virtual bool Validate(out string errorMessage)
        {
            errorMessage = "Validate not setup for graph view";
            return false;
        }

        public virtual void Save()
        {
            Debug.LogWarning("Save not setup for graph view");
        }

        protected void BuildEditableGraph()
        {
            BuildGraph(false);
        }

        protected void BuildReadonlyGraph()
        {
            BuildGraph(true);
        }

        private void BuildGraph(bool isReadonly)
        {
            EntryNode = new EntryNode(BehaviorTreeObject.EntryNodePosition);
            AddElement(EntryNode);

            var nodeMap = new Dictionary<Guid, BaseBehaviorNode>();

            var leafNodes = new List<LeafNode>();
            var decoratorNodes = new List<DecoratorNode>();
            var compositeNodes = new List<CompositeNode>();

            // create nodes
            foreach (var leafDefinition in BehaviorTreeObject.LeafNodes)
            {
                var leafNode = new LeafNode(this, leafDefinition, isReadonly);
                leafNodes.Add(leafNode);
                nodeMap.Add(leafNode.NodeId, leafNode);
                AddElement(leafNode);
            }

            foreach (var decoratorDefinition in BehaviorTreeObject.DecoratorNodes)
            {
                var decoratorNode = new DecoratorNode(this, decoratorDefinition, isReadonly);
                decoratorNodes.Add(decoratorNode);
                nodeMap.Add(decoratorNode.NodeId, decoratorNode);
                AddElement(decoratorNode);
            }

            foreach (var compositeDefinition in BehaviorTreeObject.CompositeNodes)
            {
                var compositeNode = new CompositeNode(this, compositeDefinition, isReadonly);
                compositeNodes.Add(compositeNode);
                nodeMap.Add(compositeNode.NodeId, compositeNode);
                AddElement(compositeNode);
            }
            
            // link entry
            if (BehaviorTreeObject.EntryNodeId != Guid.Empty)
            {
                LinkPorts(EntryNode.Port, nodeMap[BehaviorTreeObject.EntryNodeId].InputPort, isReadonly);
            }

            // link decorators
            foreach (var decoratorNode in decoratorNodes)
            {
                var childNode = nodeMap[decoratorNode.Definition.ChildNodeId];

                LinkPorts(decoratorNode.OutputPort, childNode.InputPort, isReadonly);
            }

            // link composites
            foreach (var compositeNode in compositeNodes)
            {
                var ports = compositeNode.OutputPorts;
                var childNodeIds = compositeNode.Definition.ChildNodeIds;

                for (int i = 0; i < ports.Length; i++)
                {
                    var childNode = nodeMap[childNodeIds[i]];
                    LinkPorts(ports[i], childNode.InputPort, isReadonly);
                }
            }
        }

        protected void SetupResizeCallback()
        {
            RegisterCallback<GeometryChangedEvent>(evt =>
            {
                var blackboardRect = new Rect(5f, 5f, 300f, 400f);
                float blackboardWidth = blackboardRect.x + blackboardRect.width;
                Blackboard.SetPosition(blackboardRect);

                var frame = new Rect(layout.x, layout.y, layout.width - blackboardWidth, layout.height);

                var rectToFit = CalculateRectToFitAll(contentViewContainer);
                CalculateFrameTransform(rectToFit, frame, 4, out Vector3 frameTranslation, out Vector3 frameScaling);
                UpdateViewTransform(frameTranslation + new Vector3(blackboardWidth, 0f, 0f), frameScaling);
            });
        }

        private void LinkPorts(Port entryPort, Port exitPort, bool isReadonly)
        {
            var edge = entryPort.ConnectTo(exitPort);

            if (isReadonly)
            {
                edge.capabilities &= ~(Capabilities.Copiable | Capabilities.Deletable);
            }

            AddElement(edge);
        }
    }
}
