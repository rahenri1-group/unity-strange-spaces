﻿using Game.Core.AI.BehaviorTree;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    [CustomEditor(typeof(BehaviorTreeRunnerBehaviour))]
    public class BehaviorTreeRunnerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var runner = (BehaviorTreeRunnerBehaviour)target;          

            if (Application.isPlaying && runner.BehaviorTree != null)
            {
                GUI.enabled = false;
                EditorGUILayout.TextField("Current Node", runner.BehaviorTree.GetPathToCurrentNode());
                GUI.enabled = true;

                if (GUILayout.Button("Open Behavior Tree"))
                {
                    var behaviorTree = runner.BehaviorTreeDefinition as BehaviorTreeDefinitionObject;
                    BehaviorTreeEditorWindow.ShowForRunner(behaviorTree, runner);
                    
                }
            }
        }
    }
}
