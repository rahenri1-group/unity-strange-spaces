﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BehaviorTreeRunnerGraphView : BehaviorTreeGraphView
    {
        private Color DefaultNodeColor = new Color(0f, 0f, 0f, 0f);
        private Color PreviousNodeColor = Color.blue;
        private Color ActiveNodeColor = Color.cyan;

        private readonly IBehaviorTreeRunner _behaviorTreeRunner;

        public BehaviorTreeRunnerGraphView(BehaviorTreeDefinitionObject behaviorTreeObject, IBehaviorTreeRunner behaviorTreeRunner)
            : base(behaviorTreeObject)
        {
            _behaviorTreeRunner = behaviorTreeRunner;

            this.AddManipulator(new ContentDragger());

            BuildBlackboard();
            BuildReadonlyGraph();

            SetupResizeCallback();

            UpdateGraphView();

            _behaviorTreeRunner.BehaviorTree.CurrentNodeChanged += OnCurrentNodeChanged;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            _behaviorTreeRunner.BehaviorTree.CurrentNodeChanged -= OnCurrentNodeChanged;
        }

        private void BuildBlackboard()
        {
            Blackboard = new BehaviorTreeRunnerBlackboard(this, _behaviorTreeRunner);
            Add(Blackboard);

            foreach (var definition in BehaviorTreeObject.BlackboardEntries)
            {
                Blackboard.AddEntryDefinition(definition);
            }
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);

            evt.menu.MenuItems().Clear();
        }

        private void OnCurrentNodeChanged()
        {
            UpdateGraphView();
        }

        private void UpdateGraphView()
        {
            var behaviorTree = (Game.Core.AI.BehaviorTree.BehaviorTree)_behaviorTreeRunner.BehaviorTree;

            if (behaviorTree == null || behaviorTree.CurrentNode == null) return;

            var graphNodes = new Dictionary<Guid, BaseBehaviorNode>();
            foreach (var node in nodes)
            {
                var behaviorNode = node as BaseBehaviorNode;

                if (behaviorNode != null)
                {
                    graphNodes.Add(behaviorNode.NodeId, behaviorNode);

                    if (behaviorTree != null && behaviorTree.GetNode(behaviorNode.NodeId).HasExecuted)
                    {
                        behaviorNode.SetBorderColor(PreviousNodeColor);
                    }
                    else
                    {
                        behaviorNode.SetBorderColor(DefaultNodeColor);
                    }
                }
            }

            var activeNode = behaviorTree.CurrentNode;
            while (activeNode != null)
            {
                graphNodes[activeNode.Id].SetBorderColor(ActiveNodeColor);

                activeNode = activeNode.Parent;
            }
        }
    }
}
