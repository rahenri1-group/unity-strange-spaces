﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public abstract class BehaviorTreeBlackboard : Blackboard
    {
        public event Action BlackboardChanged;

        public BlackboardEntryRow[] Rows => RowList.ToArray();

        protected List<BlackboardEntryRow> RowList { get; private set; }

        public BehaviorTreeBlackboard(BehaviorTreeGraphView graphView)
            : base(graphView)
        {
            title = "Variables";

            RowList = new List<BlackboardEntryRow>();
        }

        public abstract void AddEntryDefinition(BlackboardEntryDefinition definition);

        public void RemoveEntryDefinition(BlackboardEntryDefinition definition)
        {
            var row = RowList.First(r => r.Definition.Id == definition.Id);

            RowList.Remove(row);
            Remove(row);
        }

        public void MoveEntryDefinitionUp(BlackboardEntryDefinition definition)
        {
            var rowIndex = RowList.FindIndex(r => r.Definition.Id == definition.Id);

            if (rowIndex > 0)
            {
                var swap = RowList[rowIndex];
                RowList[rowIndex] = RowList[rowIndex - 1];
                RowList[rowIndex - 1] = swap;

                RowList[rowIndex - 1].PlaceBehind(RowList[rowIndex]);
            }
        }

        public void MoveEntryDefinitionDown(BlackboardEntryDefinition definition)
        {
            var rowIndex = RowList.FindIndex(r => r.Definition.Id == definition.Id);

            if (rowIndex < RowList.Count - 1)
            {
                var swap = RowList[rowIndex];
                RowList[rowIndex] = RowList[rowIndex + 1];
                RowList[rowIndex + 1] = swap;

                RowList[rowIndex + 1].PlaceInFront(RowList[rowIndex]);
            }
        }

        public void InvokeBlackboardChanged()
        {
            BlackboardChanged?.Invoke();
        }
    }
}
