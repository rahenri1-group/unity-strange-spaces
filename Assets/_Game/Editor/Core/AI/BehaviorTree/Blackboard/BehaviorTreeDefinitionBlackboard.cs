﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Linq;
using UnityEditor;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BehaviorTreeDefinitionBlackboard : BehaviorTreeBlackboard
    {
        public BehaviorTreeDefinitionBlackboard(BehaviorTreeGraphView graphView)
            : base(graphView)
        {
            addItemRequested += (blackboard) =>
            {
                var definition = new BlackboardEntryDefinition();
                definition.Id = Guid.NewGuid();
                definition.BlackboardType = BlackboardType.String;

                definition.Name = "New Variable";
                int count = 0;
                while (RowList.FirstOrDefault(r => r.Name == definition.Name) != null)
                {
                    count += 1;
                    definition.Name = $"New Variable ({count})";
                }

                AddEntryDefinition(definition);
                InvokeBlackboardChanged();
            };

            editTextRequested += (blackboard, element, newValue) =>
            {
                newValue = newValue.Trim();

                var row = element.GetFirstAncestorOfType<BlackboardEntryRow>();

                if (row == null || row.Name == newValue)
                {
                    return;
                }

                if (RowList.FirstOrDefault(r => r.Name == newValue) != null)
                {
                    EditorUtility.DisplayDialog("Error", "This property name already exists, please chose another one.", "OK");

                    return;
                }

                row.Name = newValue;
            };
        }

        public override void AddEntryDefinition(BlackboardEntryDefinition definition)
        {
            var row = new BlackboardEntryDefinitionRow(this, definition);

            RowList.Add(row);
            Add(row);
        }
    }
}
