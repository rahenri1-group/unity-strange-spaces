﻿using Game.Core.AI.BehaviorTree;
using System.Linq;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BehaviorTreeRunnerBlackboard : BehaviorTreeBlackboard
    {
        private readonly IBehaviorTreeRunner _behaviorTreeRunner;

        public BehaviorTreeRunnerBlackboard(BehaviorTreeGraphView graphView, IBehaviorTreeRunner behaviorTreeRunner)
            : base(graphView)
        {
            _behaviorTreeRunner = behaviorTreeRunner;

            editTextRequested += (blackboard, element, newValue) =>
            {
                return;
            };
        }

        public override void AddEntryDefinition(BlackboardEntryDefinition definition)
        {
            var behaviorTree = (Game.Core.AI.BehaviorTree.BehaviorTree)_behaviorTreeRunner.BehaviorTree;
            BlackboardVariable blackboardVariable = null;

            if (behaviorTree != null)
            {
                blackboardVariable = behaviorTree.Blackboard.FirstOrDefault(b => b.Id == definition.Id);
            }

            var row = new BlackboardEntryRunnerRow(this, definition, blackboardVariable);

            RowList.Add(row);
            Add(row);
        }
    }
}
