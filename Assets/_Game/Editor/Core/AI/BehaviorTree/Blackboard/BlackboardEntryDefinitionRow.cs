﻿using Game.Core.AI.BehaviorTree;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BlackboardEntryDefinitionRow : BlackboardEntryRow
    {
        public BlackboardEntryDefinitionRow(BehaviorTreeBlackboard blackboard, BlackboardEntryDefinition definition)
            : base(blackboard, definition)
        {
            BlackboardField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                Blackboard.InvokeBlackboardChanged();
            });
            BlackboardField.RegisterCallback<ContextualMenuPopulateEvent>(populateEvent =>
            {
                populateEvent.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Move Up", _ =>
                        {
                            Blackboard.MoveEntryDefinitionUp(Definition);
                        }
                    )
                );

                populateEvent.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Move Down", _ =>
                        {
                            Blackboard.MoveEntryDefinitionDown(Definition);
                        }
                    )
                );

                populateEvent.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Delete Variable", _ =>
                        {
                            Blackboard.RemoveEntryDefinition(Definition);
                            Blackboard.InvokeBlackboardChanged();
                        }
                    )
                );
            });
        }
    }
}
