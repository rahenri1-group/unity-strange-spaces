﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public abstract class BlackboardEntryRow : VisualElement
    {
        public BlackboardEntryDefinition Definition { get; private set; }

        public string Name
        {
            get => BlackboardField.text;
            set => BlackboardField.text = value;
        }

        public BlackboardType BlackboardType
        {
            get => (BlackboardType)Enum.GetValues(typeof(BlackboardType)).GetValue(_blackboardTypeDropdown.index);
        }

        protected BehaviorTreeBlackboard Blackboard { get; private set; }
        protected BlackboardField BlackboardField { get; private set; }
        protected VisualElement EntryContents { get; private set; }

        private DropdownField _blackboardTypeDropdown;
        private Toggle _variableExposedToggle;

        public BlackboardEntryRow(BehaviorTreeBlackboard blackboard, BlackboardEntryDefinition definition)
            : base()
        {
            Blackboard = blackboard;
            Definition = definition;

            BlackboardField = new BlackboardField { text = Definition.Name, typeText = Definition.BlackboardType.ToString() };
            BlackboardField.capabilities &= ~(Capabilities.Copiable | Capabilities.Deletable);

            EntryContents = new VisualElement();

            _blackboardTypeDropdown = new DropdownField(
                Enum.GetNames(typeof(BlackboardType)).ToList(),
                (int)Definition.BlackboardType);
            _blackboardTypeDropdown.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                BlackboardField.typeText = Enum.GetNames(typeof(BlackboardType))[_blackboardTypeDropdown.index];
                Blackboard.InvokeBlackboardChanged();
            });
            EntryContents.Add(_blackboardTypeDropdown);

            _variableExposedToggle = new Toggle("Exposed");
            _variableExposedToggle.value = Definition.Exposed;
            _variableExposedToggle.RegisterCallback<ChangeEvent<bool>>(evt =>
            {
                UpdateExposedIcon();
            });
            EntryContents.Add(_variableExposedToggle);

            UpdateExposedIcon();

            var row = new BlackboardRow(BlackboardField, EntryContents);

            Add(row);
        }

        public void ApplyChangesToDefinition()
        {
            Definition.Name = Name;
            Definition.BlackboardType = BlackboardType;
            Definition.Exposed = _variableExposedToggle.value;
        }

        private void UpdateExposedIcon()
        {
            BlackboardField.icon = _variableExposedToggle.value ? EditorGUIUtility.IconContent("greenLight").image : null;
        }
    }
}
