﻿using Game.Core.AI.BehaviorTree;
using System;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BlackboardEntryRunnerRow : BlackboardEntryRow
    {
        private readonly BlackboardVariable _blackboardVariable;

        private TextField _valueTextField;

        public BlackboardEntryRunnerRow(BehaviorTreeBlackboard blackboard, BlackboardEntryDefinition definition, BlackboardVariable blackboardVariable)
            : base (blackboard, definition)
        {
            _blackboardVariable = blackboardVariable;

            EntryContents.SetEnabled(false);

            _valueTextField = new TextField();
            _valueTextField.label = "Value";
            EntryContents.Add(_valueTextField);
            

            if (_blackboardVariable != null)
            {
                UpdateValueField();

                blackboardVariable.VariableChanged += OnVariableChanged;
            }
        }

        private void OnVariableChanged()
        {
            UpdateValueField();
        }

        private void UpdateValueField()
        {
            _valueTextField.value = _blackboardVariable.StringValue;
        }
    }
}
