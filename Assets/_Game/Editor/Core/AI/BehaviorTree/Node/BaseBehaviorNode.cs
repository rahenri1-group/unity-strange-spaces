﻿using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.Entity;
using Game.Core.Reflection;
using Game.Core.Space;
using Game.Editor.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public abstract class BaseBehaviorNode : BaseNode
    {
        public static readonly Vector2 Size = new Vector2(300f, 300f);

        public abstract Guid NodeId { get; }

        public abstract BaseBehaviorNode[] ChildNodes { get; }

        public int Priority
        {
            get => _priority;
            set
            {
                _priority = value;
                RefreshTitle();
            }
        }

        public Port InputPort { get; protected set; }

        protected int _priority;

        public BaseBehaviorNode(bool isReadonly)
            : base(isReadonly) { }

        protected abstract void RefreshTitle();
    }

    public abstract class BaseBehaviorNode<T_Definition, T_Interface> : BaseBehaviorNode
        where T_Definition : BaseNodeDefinition
    {
        public override Guid NodeId => Definition.Id;

        public T_Definition Definition { get; private set; }

        protected BehaviorTreeGraphView GraphView { get; private set; }
        protected Type NodeType { get; private set; }

        private TextField _stateNameTextField;

        private BaseSerializedFieldElement[] _serializedFieldElements;
        private BlackboardVariableElement[] _blackboardVariableElements;

        protected BaseBehaviorNode(BehaviorTreeGraphView graphView, T_Definition definition, bool isReadonly)
            : base(isReadonly)
        {
            Definition = definition;
            GraphView = graphView;
            NodeType = TypeUtil.GetTypeByName(definition.TypeName, typeof(T_Interface));

            _priority = definition.Priority;

            SetPosition(new Rect(Definition.NodePosition, Size));

            _stateNameTextField = CreateNameTextField();
            mainContainer.Add(_stateNameTextField);

            InputPort = CreateInputPort("entry", Port.Capacity.Single);
            inputContainer.Add(InputPort);

            if (NodeType == null)
            {
                title = "ERROR!!!";
                Debug.LogError($"Unable to find node type {Definition.TypeName}");
                return;
            }

            _blackboardVariableElements = CreateBlackboardFields();
            _serializedFieldElements = SerializedFieldUtil.CreateSerializedFields(mainContainer, NodeType, Definition.SerializedFields, IsReadonly);
        }

        public void Refresh()
        {
            _stateNameTextField.SetValueWithoutNotify(Definition.Name);
            _stateNameTextField.SetEnabled(false);

            RefreshTitle();
            RefreshPorts();
            RefreshExpandedState();
        }

        protected override void RefreshTitle()
        {
            if (NodeType == null) return;

            if (string.IsNullOrEmpty(_stateNameTextField.value))
            {
                title = $"<b>{ObjectNames.NicifyVariableName(NodeType.Name)}</b> : {Priority}";
            }
            else
            {
                title = $"<b>{_stateNameTextField.value}</b> : {Priority}" +
                    $"\n<size=10>{ObjectNames.NicifyVariableName(NodeType.Name)}</size>";
            }
        }

        public override bool ValidateNode(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!InputPort.connected)
            {
                errorMessage = $"Node {Definition.TypeName} missing an input connection";
                return false;
            }

            return true;
        }

        public virtual void ApplyChangesToDefinition()
        {
            Definition.Name = _stateNameTextField.value;
            Definition.NodePosition = GetPosition().position;
            Definition.Priority = Priority;

            var serializedFields = new Dictionary<string, string>();
            foreach (var mapping in _serializedFieldElements)
            {
                serializedFields[mapping.FieldName] = mapping.Value;
            }
            Definition.SerializedFields = serializedFields;

            var blackboardVariables = new Dictionary<string, Guid>();
            foreach (var mapping in _blackboardVariableElements)
            {
                blackboardVariables[mapping.FieldName] = mapping.VariableId;
            }
            Definition.BlackboardVariables = blackboardVariables;
        }

        public void EnableRename()
        {
            _stateNameTextField.SetEnabled(true);
            _stateNameTextField.Focus();
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);

            evt.menu.MenuItems().Add(
                new GraphDropdownAction(
                    "Rename Node", _ =>
                    {
                        EnableRename();
                    }
                )
            );
        }

        private TextField CreateNameTextField()
        {
            var textField = new TextField();
            textField.SetEnabled(false);
            textField.RegisterValueChangedCallback(evt =>
            {
                RefreshTitle();
            });
            textField.RegisterCallback<FocusOutEvent>(evt =>
            {
                _stateNameTextField.SetEnabled(false);
            });

            return textField;
        }

        private BlackboardVariableElement[] CreateBlackboardFields()
        {
            var blackboardVariableElements = new List<BlackboardVariableElement>();

            var members = NodeType.GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<BlackboardVariableAttribute>().Any())
                .ToArray();

            var initialVariables = Definition.BlackboardVariables;

            foreach (var field in members.OfType<FieldInfo>())
            {
                if (!typeof(IVariable).IsAssignableFrom(field.FieldType) && field.FieldType.IsGenericType)
                {
                    Debug.LogError($"Field {field.Name} is using attribute BlackboardVariable on a non IVariable<T>");
                    continue;
                }

                var genericType = field.FieldType.GenericTypeArguments[0];
                var blackboardType = BlackboardType.String;

                if (genericType == typeof(string))
                {
                    blackboardType = BlackboardType.String;
                }
                else if (genericType == typeof(bool))
                {
                    blackboardType = BlackboardType.Bool;
                }
                else if (genericType == typeof(int))
                {
                    blackboardType = BlackboardType.Integer;
                }
                else if (genericType == typeof(float))
                {
                    blackboardType = BlackboardType.Float;
                }
                else if (genericType == typeof(SpacePosition))
                {
                    blackboardType = BlackboardType.Position;
                }
                else if (genericType == typeof(IEntity))
                {
                    blackboardType = BlackboardType.Entity;
                }
                else
                {
                    Debug.LogError($"Field {field.Name} is using attribute BlackboardVariable on a IVariable of an unsupported generic type");
                    continue;
                }

                var blackboardVariableElement = new BlackboardVariableElement(GraphView.Blackboard, field.Name, blackboardType);

                if (initialVariables.ContainsKey(field.Name))
                {
                    blackboardVariableElement.VariableId = initialVariables[field.Name];
                }

                blackboardVariableElement.SetEnabled(!IsReadonly);

                blackboardVariableElements.Add(blackboardVariableElement);
                mainContainer.Add(blackboardVariableElement);
            }

            return blackboardVariableElements.ToArray();
        }
    }
}
