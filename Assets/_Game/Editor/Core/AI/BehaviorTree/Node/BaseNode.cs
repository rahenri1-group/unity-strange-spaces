﻿using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public abstract class BaseNode : Node
    {
        public bool IsReadonly { get; private set; }

        public BaseNode(bool isReadonly)
            : base()
        {
            IsReadonly = isReadonly;

            if (IsReadonly)
            {
                capabilities &= ~(Capabilities.Copiable | Capabilities.Deletable);
            }
        }

        public abstract bool ValidateNode(out string errorMessage);
                                                                                                                                                                            
        public void SetBorderColor(Color color)
        {                                                                                             
            style.borderTopWidth = 2f;                                                                                          
            style.borderBottomWidth = 2f;
            style.borderRightWidth = 2f;
            style.borderLeftWidth = 2f;

            style.borderTopLeftRadius = 2f;
            style.borderTopRightRadius = 2f;
            style.borderBottomLeftRadius = 2f;
            style.borderBottomRightRadius = 2f;

            style.borderTopColor = new StyleColor(color);
            style.borderBottomColor = new StyleColor(color);
            style.borderRightColor = new StyleColor(color);
            style.borderLeftColor = new StyleColor(color);
        }

        protected Port CreateInputPort(string name, Port.Capacity capacity)
        {
            var port = InstantiatePort(Orientation.Horizontal, Direction.Input, capacity, typeof(Port));
            port.portName = name;
            return port;
        }

        protected Port CreateOutputPort(string name, Port.Capacity capacity)
        {
            var port = InstantiatePort(Orientation.Horizontal, Direction.Output, capacity, typeof(Port));
            port.portName = name;
            return port;
        }

        protected BaseBehaviorNode GetConnectedNode(Port port)
        {
            if (!port.connected) return null;

            return (BaseBehaviorNode)port.connections.First().input.node;
        }
    }
}
