﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{ 
    public class CompositeNode : BaseBehaviorNode<CompositeNodeDefinition, ICompositeNode>
    {
        public override BaseBehaviorNode[] ChildNodes
        {
            get
            {
                var children = new List<BaseBehaviorNode>();
                foreach (var port in _outputPorts)
                {
                    var child = GetConnectedNode(port);
                    if (child != null)
                    {
                        children.Add(child);
                    }
                }

                return children.ToArray();
            }
        }

        public Port[] OutputPorts => _outputPorts.ToArray();

        private List<Port> _outputPorts;

        public CompositeNode(BehaviorTreeGraphView graphView, CompositeNodeDefinition definition, bool isReadonly = false)
            : base(graphView, definition, isReadonly)
        {
            _outputPorts = new List<Port>();

            for (int i = 0; i < Mathf.Max(1, definition.ChildNodeIds.Length); i++)
            {
                AddOutputPort();
            }

            Refresh();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            if (!base.ValidateNode(out errorMessage))
            {
                return false;
            }

            if (_outputPorts.Count == 0)
            {
                errorMessage = $"Node {Definition.TypeName} has no output ports";
                return false;
            }

            foreach (var port in _outputPorts)
            {
                if (!port.connected)
                {
                    errorMessage = $"Node {Definition.TypeName} missing an ouput connection";
                    return false;
                }
            }

            return true;
        }

        public override void ApplyChangesToDefinition()
        {
            base.ApplyChangesToDefinition();

            var ids = new Guid[_outputPorts.Count];
            for(int i = 0; i < _outputPorts.Count; i++)
            {
                ids[i] = GetConnectedNode(_outputPorts[i]).NodeId;
            }

            Definition.ChildNodeIds = ids;
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);

            evt.menu.MenuItems().Add(
                new GraphDropdownAction(
                    "Add Output Port ", _ =>
                    {
                        AddOutputPort();
                        Refresh();
                    }
                )
            );
        }

        private void AddOutputPort()
        {
            var outputElement = new VisualElement();
            outputElement.style.flexDirection = FlexDirection.Row;

            var port = CreateOutputPort("", Port.Capacity.Single);

            if (!IsReadonly)
            {
                var deleteButton = new Button();
                deleteButton.text = "X";
                deleteButton.clicked += () =>
                {
                    foreach (var edge in port.connections)
                    {
                        edge.parent.Remove(edge);
                    }

                    outputContainer.Remove(outputElement);

                    _outputPorts.Remove(port);
                };

                outputElement.Add(deleteButton);
            }

            outputElement.Add(port);

            outputContainer.Add(outputElement);

            _outputPorts.Add(port);

            return;
        }
    }
}
