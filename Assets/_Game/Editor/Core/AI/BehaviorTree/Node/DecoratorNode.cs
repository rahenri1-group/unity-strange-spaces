﻿using Game.Core.AI.BehaviorTree;
using UnityEditor.Experimental.GraphView;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class DecoratorNode : BaseBehaviorNode<DecoratorNodeDefinition, IDecoratorNode>
    {
        public override BaseBehaviorNode[] ChildNodes
        {
            get
            {
                var child = GetConnectedNode(OutputPort);
                if (child != null) return new BaseBehaviorNode[] { child } ;

                return new BaseBehaviorNode[0];
            }
        }

        public Port OutputPort { get; private set; }

        public DecoratorNode(BehaviorTreeGraphView graphView, DecoratorNodeDefinition definition, bool isReadonly = false)
            : base(graphView, definition, isReadonly)
        {
            OutputPort = CreateOutputPort("output", Port.Capacity.Single);
            outputContainer.Add(OutputPort);

            Refresh();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            if (!base.ValidateNode(out errorMessage))
            {
                return false;
            }

            if (!OutputPort.connected)
            {
                errorMessage = $"Node {Definition.TypeName} missing an ouput connection";
                return false;
            }

            return true;
        }

        public override void ApplyChangesToDefinition()
        {
            base.ApplyChangesToDefinition();

            Definition.ChildNodeId = GetConnectedNode(OutputPort).NodeId;
        }
    }
}
