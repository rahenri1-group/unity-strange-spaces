﻿using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class EntryNode : BaseNode
    {
        private static readonly Vector2 Size = new Vector2(100f, 100f);
        private static readonly Color NodeColor = Color.green;

        public Guid EntryNodeId => GetConnectedNode(Port).NodeId;

        public Port Port { get; private set; }

        public EntryNode(Vector2 position)
            : base(true)
        {
            title = "Entry";

            SetBorderColor(NodeColor);

            Port = CreateOutputPort("entry", Port.Capacity.Single);
            Port.portColor = NodeColor;
            outputContainer.Add(Port);

            SetPosition(new Rect(position, Size));

            RefreshExpandedState();
            RefreshPorts();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!Port.connected)
            {
                errorMessage = $"Entry node missing a connection";
                return false;
            }

            return true;
        }
    }
}
