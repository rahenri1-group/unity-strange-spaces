﻿using Game.Core.AI.BehaviorTree;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class LeafNode : BaseBehaviorNode<LeafNodeDefinition, ILeafNode>
    {
        public override BaseBehaviorNode[] ChildNodes => new BaseBehaviorNode[0];

        public LeafNode(BehaviorTreeGraphView graphView, LeafNodeDefinition definition, bool isReadonly = false)
            : base(graphView, definition, isReadonly)
        {
            Refresh();
        }
    }
}
