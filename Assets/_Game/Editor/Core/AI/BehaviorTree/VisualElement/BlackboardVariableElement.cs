﻿using Game.Core.AI.BehaviorTree;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class BlackboardVariableElement : VisualElement
    {
        public string FieldName { get; }

        public Guid VariableId 
        {
            get => _variableId;
            set
            {
                _variableId = value;
                RefreshDropdown();
            }
        }

        private readonly BehaviorTreeBlackboard _blackboard;
        private readonly DropdownField _blackboardDropdown;

        private Guid _variableId;
        private BlackboardType _blackboardType;

        public BlackboardVariableElement(BehaviorTreeBlackboard blackboard, string fieldName, BlackboardType blackboardType)
            : base()
        {
            _blackboard = blackboard;
            FieldName = fieldName;
            _blackboardType = blackboardType;
            _variableId = Guid.Empty;

            _blackboardDropdown = new DropdownField(ObjectNames.NicifyVariableName(FieldName));
            _blackboardDropdown.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                if (_blackboardDropdown.index == 0)
                {
                    _variableId = Guid.Empty;
                }
                else
                {
                    var blackboardRows = _blackboard.Rows
                        .Where(r => r.BlackboardType == _blackboardType)
                        .ToArray();
                    _variableId = blackboardRows[_blackboardDropdown.index - 1].Definition.Id;
                }
                
            });

            Add(_blackboardDropdown);

            RefreshDropdown();

            _blackboard.BlackboardChanged += OnBlackboardChanged;
            RegisterCallback<DetachFromPanelEvent>(evt =>
            {
                _blackboard.BlackboardChanged -= OnBlackboardChanged;
            });
        }

        private void OnBlackboardChanged()
        {
            var blackboardRows = _blackboard.Rows;
            if (!blackboardRows.Any(r => r.Definition.Id == VariableId && r.BlackboardType == _blackboardType))
            {
                _variableId = Guid.Empty;
            }

            RefreshDropdown();
        }

        private void RefreshDropdown()
        {
            var options = new List<string>();
            options.Add("None");

            var blackboardRows = _blackboard.Rows
                .Where(r => r.BlackboardType == _blackboardType)
                .ToArray();
            options.AddRange(blackboardRows.Select(r => r.Name));

            _blackboardDropdown.choices = options;

            bool foundBlackboardRow = false;
            for (int i = 0; i < blackboardRows.Length; i++)
            {
                if (blackboardRows[i].Definition.Id == VariableId)
                {
                    foundBlackboardRow = true;
                    _blackboardDropdown.index = i + 1;
                    break;
                }
            }

            if (!foundBlackboardRow)
            {
                _blackboardDropdown.index = 0;
            }
        }
    }
}
