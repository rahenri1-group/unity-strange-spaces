﻿using Game.Core.AI.BehaviorTree;
using System;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class CompositeNodeSearchWindowProvider : BaseTypeSearchWindowProvider<ICompositeNode>
    {
        protected override string SearchWindowTitle => "Select Composite";

        private BehaviorTreeGraphView _graphView;
        private Vector2 _nodePosition;

        public void Initialize(BehaviorTreeGraphView graphView, Vector2 nodePosition)
        {
            _graphView = graphView;
            _nodePosition = nodePosition;
        }

        protected override void OnTypeSelected(Type compositeType)
        {
            var compositeNodeDefinition = new CompositeNodeDefinition();
            compositeNodeDefinition.Id = Guid.NewGuid();
            compositeNodeDefinition.TypeName = compositeType.FullName;
            compositeNodeDefinition.NodePosition = _nodePosition;

            var compositeNode = new CompositeNode(_graphView, compositeNodeDefinition);
            _graphView.AddElement(compositeNode);
            compositeNode.EnableRename();
        }
    }
}
