﻿using Game.Core.AI.BehaviorTree;
using System;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class DecoratorNodeSearchWindowProvider : BaseTypeSearchWindowProvider<IDecoratorNode>
    {
        protected override string SearchWindowTitle => "Select Decorator";

        private BehaviorTreeGraphView _graphView;
        private Vector2 _nodePosition;

        public void Initialize(BehaviorTreeGraphView graphView, Vector2 nodePosition)
        {
            _graphView = graphView;
            _nodePosition = nodePosition;
        }

        protected override void OnTypeSelected(Type decoratorType)
        {
            var decoratorNodeDefinition = new DecoratorNodeDefinition();
            decoratorNodeDefinition.Id = Guid.NewGuid();
            decoratorNodeDefinition.TypeName = decoratorType.FullName;
            decoratorNodeDefinition.NodePosition = _nodePosition;

            var decoratorNode = new DecoratorNode(_graphView, decoratorNodeDefinition);
            _graphView.AddElement(decoratorNode);
            decoratorNode.EnableRename();
        }
    }
}
