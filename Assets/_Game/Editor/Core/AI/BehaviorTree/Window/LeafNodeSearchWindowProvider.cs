﻿using Game.Core.AI.BehaviorTree;
using System;
using UnityEngine;

namespace Game.Editor.Core.AI.BehaviorTree
{
    public class LeafNodeSearchWindowProvider : BaseTypeSearchWindowProvider<ILeafNode>
    {
        protected override string SearchWindowTitle => "Select Leaf";

        private BehaviorTreeGraphView _graphView;
        private Vector2 _nodePosition;

        public void Initialize(BehaviorTreeGraphView graphView, Vector2 nodePosition)
        {
            _graphView = graphView;
            _nodePosition = nodePosition;
        }

        protected override void OnTypeSelected(Type leafType)
        {
            var leafNodeDefinition = new LeafNodeDefinition();
            leafNodeDefinition.Id = Guid.NewGuid();
            leafNodeDefinition.TypeName = leafType.FullName;
            leafNodeDefinition.NodePosition = _nodePosition;

            var leafNode = new LeafNode(_graphView, leafNodeDefinition);
            _graphView.AddElement(leafNode);
            leafNode.EnableRename();
        }
    }
}
