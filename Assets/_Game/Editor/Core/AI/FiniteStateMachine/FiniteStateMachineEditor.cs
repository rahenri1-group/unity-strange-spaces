﻿using Game.Core.AI.FiniteStateMachine;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    [CustomEditor(typeof(FiniteStateMachineDefinitionObject))]
    public class FiniteStateMachineEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Open Finite State Machine"))
            {
                var fsm = target as FiniteStateMachineDefinitionObject;
                FiniteStateMachineEditorWindow.Show(fsm);
            }
        }
    }
}
