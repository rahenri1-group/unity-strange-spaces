﻿using Game.Core.AI.FiniteStateMachine;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class FiniteStateMachineEditorWindow : EditorWindow
    {
        private static readonly Dictionary<FiniteStateMachineDefinitionObject, FiniteStateMachineEditorWindow> _windowCache = new Dictionary<FiniteStateMachineDefinitionObject, FiniteStateMachineEditorWindow>();

        public FiniteStateMachineDefinitionObject FsmObject { get; private set; }

        private FiniteStateMachineGraphView _graphView;

        public static void Show(FiniteStateMachineDefinitionObject fsmObject)
        {
            var window = Create(fsmObject);
            window.Show();
            window.Focus();
        }

        private static FiniteStateMachineEditorWindow Create(FiniteStateMachineDefinitionObject fsmObject)
        {
            if (_windowCache.ContainsKey(fsmObject))
            {
                return _windowCache[fsmObject];
            }

            var window = CreateInstance<FiniteStateMachineEditorWindow>();
            window.FsmObject = fsmObject;
            window.titleContent = new GUIContent($"FSM Editor {fsmObject.name}");
            window.SetupView();

            _windowCache[fsmObject] = window;

            return window;
        }

        private void OnDestroy()
        {
            if (FsmObject != null && _windowCache.ContainsKey(FsmObject))
            {
                _windowCache.Remove(FsmObject);
            }
        }

        private void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            Reload();
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        private void OnPlayModeStateChanged(PlayModeStateChange playModeStateChange)
        {
            switch (playModeStateChange)
            {
                case PlayModeStateChange.EnteredEditMode:
                    Reload();
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    Reload();
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    break;
                default:
                    Debug.LogError($"Unknown state {playModeStateChange}");
                    break;
            }
        }

        private void Reload()
        {
            if (FsmObject != null)
            {
                SetupView();
                Repaint();
            }
        }

        private void SetupView()
        {
            rootVisualElement.Clear();

            _graphView = new FiniteStateMachineGraphView(FsmObject, this);

            rootVisualElement.Add(CreateToolBar());
            rootVisualElement.Add(_graphView);
        }

        private VisualElement CreateToolBar()
        {
            return new IMGUIContainer(
                () =>
                {
                    GUILayout.BeginHorizontal(EditorStyles.toolbar);

                    if (!Application.isPlaying)
                    {
                        if (GUILayout.Button("Save", EditorStyles.toolbarButton))
                        {
                            var guiContent = new GUIContent();

                            if (_graphView.Validate(out string errorMessage))
                            {
                                _graphView.Save();
                                guiContent.text = $"Successfully updated {FsmObject.name}";
                            }
                            else
                            {
                                guiContent.text = $"Could not save, {errorMessage}";
                            }
                            
                            ShowNotification(guiContent);
                        }
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
            );
        }
    }
}
