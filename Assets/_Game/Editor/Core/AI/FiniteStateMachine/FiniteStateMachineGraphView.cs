﻿using Game.Core.AI.FiniteStateMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class FiniteStateMachineGraphView : GraphView
    {
        private readonly FiniteStateMachineDefinitionObject _fsmObject;

        private EntryNode _entryNode;

        public FiniteStateMachineGraphView(FiniteStateMachineDefinitionObject fsmObject, EditorWindow editor)
        {
            _fsmObject = fsmObject;

            style.flexGrow = 1;
            style.flexShrink = 1;

            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            Insert(0, new GridBackground());


            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            BuildInitialGraph();
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);

            var remainTargets = evt.menu.MenuItems().FindAll(e =>
            {
                switch (e)
                {
                    case GraphDropdownAction a: return true;
                    case DropdownMenuAction a: return a.name == "Delete";
                    default: return false;
                }
            });
            
            evt.menu.MenuItems().Clear();

            if (evt.target == this)
            {
                evt.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Add New State", action =>
                        {
                            var stateDefinition = new StateDefinition();
                            stateDefinition.Id = Guid.NewGuid();
                            stateDefinition.Name = "New State";
                            stateDefinition.NodePosition = contentViewContainer.WorldToLocal(action.eventInfo.mousePosition);

                            var node = new StateNode(stateDefinition);
                            AddElement(node);
                            node.EnableRename();

                        }
                    )
                );

                evt.menu.MenuItems().Add(
                    new GraphDropdownAction(
                        "Add New Decision", action =>
                        {
                            var provider = ScriptableObject.CreateInstance<DecisionSearchWindowProvider>();
                            provider.Initialize(this, contentViewContainer.WorldToLocal(action.eventInfo.mousePosition));
                            var mousePosition = GUIUtility.GUIToScreenPoint(action.eventInfo.mousePosition);
                            SearchWindow.Open(new SearchWindowContext(mousePosition), provider);
                        }
                    )
                );
            }

            remainTargets.ForEach(evt.menu.MenuItems().Add);
        }

        public bool Validate(out string errorMessage)
        {
            errorMessage = string.Empty;

            foreach (var n in nodes)
            {
                var node = (BaseNode)n;

                if (!node.ValidateNode(out errorMessage))
                {
                    return false;
                }
            }

            return true;
        }

        public void Save()
        {
            _fsmObject.EntryNodePosition = _entryNode.GetPosition().position;
            _fsmObject.EntryStateId = _entryNode.EntryState.StateId;

            var stateDefinitions = new List<StateDefinition>();
            var decisionDefinitions = new List<DecisionDefinition>();
            foreach (var node in nodes)
            {
                if (node is StateNode)
                {
                    var stateNode = (StateNode)node;
                    stateNode.ApplyChangesToDefinition();
                    stateDefinitions.Add(stateNode.StateDefinition);
                }
                else if (node is DecisionNode)
                {
                    var decisionNode = (DecisionNode)node;
                    decisionNode.ApplyChangesToDefinition();
                    decisionDefinitions.Add(decisionNode.DecisionDefinition);
                }
            }

            _fsmObject.StateDefinitions = stateDefinitions
                .OrderBy(s => s.Id)
                .ToArray();

            _fsmObject.DecisionDefinitions = decisionDefinitions
                .OrderBy(d => d.Id)
                .ToArray();

            EditorUtility.SetDirty(_fsmObject);
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var availablePorts = ports.Where(port => port != startPort && startPort.node != port.node && port.direction != startPort.direction).ToList();

            if (startPort.node is EntryNode)
            {
                availablePorts = availablePorts.Where(port => port.node is StateNode).ToList();
            }
            else if (startPort.node is StateNode)
            {
                availablePorts = availablePorts.Where(port => port.node is EntryNode || port.node is DecisionNode).ToList();
            }
            else if (startPort.node is DecisionNode)
            {
                availablePorts = availablePorts.Where(port => port.node is StateNode).ToList();
            }

            return availablePorts;
        }

        private void BuildInitialGraph()
        {
            _entryNode = new EntryNode(_fsmObject.EntryNodePosition);
            AddElement(_entryNode);

            var stateNodes = new List<StateNode>();
            foreach (var stateDefinition in _fsmObject.StateDefinitions)
            {
                var stateNode = new StateNode(stateDefinition);
                stateNodes.Add(stateNode);
                AddElement(stateNode);
            }

            if (_fsmObject.EntryStateId != Guid.Empty)
            {
                LinkPorts(_entryNode.Port, stateNodes.First(s => s.StateId == _fsmObject.EntryStateId).EntryPort);
            }

            foreach (var decisionDefinition in _fsmObject.DecisionDefinitions)
            {
                var decisionNode = new DecisionNode(decisionDefinition);
                AddElement(decisionNode);

                LinkPorts(decisionNode.EntryPort, stateNodes.First(s => s.StateId == decisionDefinition.EntryStateId).ExitPort);
                LinkPorts(decisionNode.ExitPort, stateNodes.First(s => s.StateId == decisionDefinition.ExitStateId).EntryPort);
            }
        }

        private void LinkPorts(Port entryPort, Port exitPort)
        {
            var edge = entryPort.ConnectTo(exitPort);
            AddElement(edge);
        }
    }
}
