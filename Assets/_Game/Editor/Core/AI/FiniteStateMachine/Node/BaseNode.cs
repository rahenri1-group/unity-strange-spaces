﻿using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Base Node for <see cref="FiniteStateMachineGraphView"/>
    /// </summary>
    public abstract class BaseNode : Node
    {
        public abstract bool ValidateNode(out string errorMessage);

        protected void SetBorderColor(Color color)
        {
            style.borderTopWidth = 2f;
            style.borderBottomWidth = 2f;
            style.borderRightWidth = 2f;
            style.borderLeftWidth = 2f;

            style.borderTopColor = new StyleColor(color);
            style.borderBottomColor = new StyleColor(color);
            style.borderRightColor = new StyleColor(color);
            style.borderLeftColor = new StyleColor(color);
        }

        protected Port CreateInputPort(string name, Port.Capacity capacity)
        {
            var port = InstantiatePort(Orientation.Horizontal, Direction.Input, capacity, typeof(Port));
            port.portName = name;
            return port;
        }

        protected Port CreateOutputPort(string name, Port.Capacity capacity)
        {
            var port = InstantiatePort(Orientation.Horizontal, Direction.Output, capacity, typeof(Port));
            port.portName = name;
            return port;
        }
    }
}
