﻿using Game.Core.AI.FiniteStateMachine;
using Game.Core.Reflection;
using Game.Editor.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Node for <see cref="DecisionDefinition"/>
    /// </summary>
    public class DecisionNode : BaseNode
    {
        public static readonly Vector2 Size = new Vector2(300f, 300f);

        public Guid DecisionId => DecisionDefinition.Id;

        public DecisionDefinition DecisionDefinition { get; private set; }

        public Port EntryPort { get; private set; }
        public Port ExitPort { get; private set; }

        private Type _decisionType;
        private BaseSerializedFieldElement[] _serializedFieldElements;

        public DecisionNode(DecisionDefinition decisionDefinition)
           : base()
        {
            DecisionDefinition = decisionDefinition;

            SetPosition(new Rect(DecisionDefinition.NodePosition, Size));

            _decisionType = TypeUtil.GetTypeByName(DecisionDefinition.DecisionTypeName, typeof(IDecision));
            if (_decisionType == null)
            {
                title = "ERROR!!!";
                Debug.LogError($"Unable to find action {DecisionDefinition.DecisionTypeName}");
                return;
            }

            EntryPort = CreateInputPort("decide", Port.Capacity.Single);
            inputContainer.Add(EntryPort);

            ExitPort = CreateOutputPort("on true", Port.Capacity.Single);
            ExitPort.portColor = Color.green;
            outputContainer.Add(ExitPort);

            _serializedFieldElements = SerializedFieldUtil.CreateSerializedFields(mainContainer, _decisionType, DecisionDefinition.SerializedFields);

            Refresh();
        }

        public void Refresh()
        {
            title = ObjectNames.NicifyVariableName(_decisionType.Name);

            RefreshPorts();
            RefreshExpandedState();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!EntryPort.connected || !ExitPort.connected)
            {
                errorMessage = $"Decision '{title}' missing a connection";
                return false;
            }

            return true;
        }

        public void ApplyChangesToDefinition()
        {
            DecisionDefinition.NodePosition = GetPosition().position;

            if (EntryPort.connected)
            {
                var entryState = (StateNode)EntryPort.connections.First().output.node;
                DecisionDefinition.EntryStateId = entryState.StateId;
            }

            if (ExitPort.connected)
            {
                var exitState = (StateNode)ExitPort.connections.First().input.node;
                DecisionDefinition.ExitStateId = exitState.StateId;
            }

            var serializedFields = new Dictionary<string, string>();
            foreach (var mapping in _serializedFieldElements)
            {
                serializedFields[mapping.FieldName] = mapping.Value;
            }

            DecisionDefinition.SerializedFields = serializedFields;
        }
    }
}
