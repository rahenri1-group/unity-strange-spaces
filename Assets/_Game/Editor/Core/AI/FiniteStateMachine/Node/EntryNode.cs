﻿using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Entry node for <see cref="FiniteStateMachineGraphView"/>
    /// </summary>
    public class EntryNode : BaseNode
    {
        private static readonly Vector2 Size = new Vector2(100f, 100f);
        private static readonly Color NodeColor = Color.green;

        public StateNode EntryState 
        {
            get 
            {
                if (!Port.connected) return null;
                return (StateNode)Port.connections.First().input.node;
            }
        }

        public Port Port { get; private set; }

        public EntryNode(Vector2 position)
            : base()
        {
            title = "Entry";

            SetBorderColor(NodeColor);           

            capabilities &= ~(Capabilities.Copiable | Capabilities.Deletable);

            Port = CreateOutputPort("entry", Port.Capacity.Single);
            Port.portColor = NodeColor;
            outputContainer.Add(Port);

            SetPosition(new Rect(position, Size));

            RefreshExpandedState();
            RefreshPorts();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (EntryState == null)
            {
                errorMessage = "Entry node not connected to state";
                return false;
            }

            return true;
        }
    }
}
