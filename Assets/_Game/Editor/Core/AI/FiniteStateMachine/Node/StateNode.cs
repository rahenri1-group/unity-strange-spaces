﻿using Game.Core.AI.FiniteStateMachine;
using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Node for <see cref="StateDefinition"/>
    /// </summary>
    public class StateNode : BaseNode
    {
        private static readonly Vector2 Size = new Vector2(600f, 600f);
        private static readonly Color NodeColor = new Color(0f, 0.5f, 1f);

        public Guid StateId => StateDefinition.Id;

        public StateDefinition StateDefinition { get; private set; }

        public Port EntryPort { get; private set; }
        public Port ExitPort { get; private set; }

        private TextField _stateNameTextField;

        private ActionsListElement _actionsListElement;

        public StateNode(StateDefinition stateDefinition)
            : base()
        {
            StateDefinition = stateDefinition;

            SetBorderColor(NodeColor);

            _stateNameTextField = CreateNameTextField();
            mainContainer.Add(_stateNameTextField);

            _actionsListElement = new ActionsListElement();
            mainContainer.Add(_actionsListElement);

            EntryPort = CreateInputPort("entry", Port.Capacity.Multi);
            inputContainer.Add(EntryPort);

            ExitPort = CreateOutputPort("exit", Port.Capacity.Multi);
            ExitPort.portColor = NodeColor;
            outputContainer.Add(ExitPort);

            SetPosition(new Rect(StateDefinition.NodePosition, Size));

            Refresh();
        }

        public void Refresh()
        {
            title = StateDefinition.Name;
            _stateNameTextField.SetValueWithoutNotify(StateDefinition.Name);
            _stateNameTextField.SetEnabled(false);

            _actionsListElement.ClearList();
            foreach (var actionDefinition in StateDefinition.ActionDefinitions)
            {
                _actionsListElement.AddActionDefinition(actionDefinition);
            }


            RefreshPorts();
            RefreshExpandedState();
        }

        public override bool ValidateNode(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!EntryPort.connected)
            {
                errorMessage = $"State '{title}' missing an entry connection";
                return false;
            }

            return true;
        }

        public void ApplyChangesToDefinition()
        {
            StateDefinition.Name = _stateNameTextField.value;
            StateDefinition.NodePosition = GetPosition().position;
            StateDefinition.ActionDefinitions = _actionsListElement.ActionDefinitions;
        }

        public void EnableRename()
        {
            _stateNameTextField.SetEnabled(true);
            _stateNameTextField.Focus();
        }

        public void AddAction(ActionDefinition actionDefinition)
        {
            _actionsListElement.AddActionDefinition(actionDefinition);
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);
            
            evt.menu.MenuItems().Add(
                new GraphDropdownAction(
                    "Rename State", _ =>
                    {
                        EnableRename();
                    }
                )
            );

            evt.menu.MenuItems().Add(
                new GraphDropdownAction(
                    "Add Action", action =>
                    {
                        var provider = ScriptableObject.CreateInstance<ActionSearchWindowProvider>();
                        provider.Initialize(this);
                        var mousePosition = GUIUtility.GUIToScreenPoint(action.eventInfo.mousePosition);
                        SearchWindow.Open(new SearchWindowContext(mousePosition), provider);
                    }
                )
            );
        }

        private TextField CreateNameTextField()
        {
            var textField = new TextField();
            textField.SetEnabled(false);
            textField.RegisterValueChangedCallback(evt =>
            {
                title = _stateNameTextField.value;
            });
            textField.RegisterCallback<FocusOutEvent>(evt =>
            {
                _stateNameTextField.SetEnabled(false);
            });

            return textField;
        }

        
    }
}
