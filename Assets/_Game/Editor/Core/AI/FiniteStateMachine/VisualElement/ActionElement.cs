﻿using Game.Core.AI.FiniteStateMachine;
using Game.Core.Reflection;
using Game.Editor.Core.UI;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class ActionElement : VisualElement
    {
        public Action<ActionElement> DeleteAction;

        private Type _actionType;
        private BaseSerializedFieldElement[] _serializedFieldElements;

        public ActionElement(ActionDefinition actionDefinition)
            : base()
        {
            _actionType = TypeUtil.GetTypeByName(actionDefinition.ActionTypeName, typeof(IAction));
            if (_actionType == null)
            {
                Add(new Label("ERROR!!!"));
                Debug.LogError($"Unable to find action {actionDefinition.ActionTypeName}");
                return;
            }

            var actionVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/_Game/UXML/Editor/FSM_Action.uxml");
            actionVisualTree.CloneTree(this);

            this.Q<Label>("action-label").text = ObjectNames.NicifyVariableName(_actionType.Name);

            this.Q<Button>("action-delete-button").clicked += Delete;

            _serializedFieldElements = SerializedFieldUtil.CreateSerializedFields(this, _actionType, actionDefinition.SerializedFields);
        }

        public ActionDefinition BuildActionDefinition()
        {
            var actionDefinition = new ActionDefinition();
            actionDefinition.ActionTypeName = _actionType.FullName;

            var serializedFields = new Dictionary<string, string>();
            foreach (var mapping in _serializedFieldElements)
            {
                serializedFields[mapping.FieldName] = mapping.Value;
            }

            actionDefinition.SerializedFields = serializedFields;

            return actionDefinition;
        }

        private void Delete()
        {
            DeleteAction?.Invoke(this);
        }
    }
}
