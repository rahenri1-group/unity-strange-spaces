﻿using Game.Core.AI.FiniteStateMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class ActionsListElement : VisualElement
    {
        public ActionDefinition[] ActionDefinitions => _actionElements.Select(a => a.BuildActionDefinition()).ToArray();

        private List<ActionElement> _actionElements;

        public ActionsListElement()
            : base()
        {
            _actionElements = new List<ActionElement>();
        }

        public void AddActionDefinition(ActionDefinition actionDefinition)
        {
            var actionElement = new ActionElement(actionDefinition);
            actionElement.DeleteAction += OnDeleteAction;

            _actionElements.Add(actionElement);
            Add(actionElement);
        }

        public void ClearList()
        {
            foreach (var actionElement in _actionElements)
            {
                actionElement.DeleteAction -= OnDeleteAction;
            }

            _actionElements.Clear();
            Clear();
        }

        private void OnDeleteAction(ActionElement actionElement)
        {
            actionElement.DeleteAction -= OnDeleteAction;

            _actionElements.Remove(actionElement);
            Remove(actionElement);
        }
    }
}
