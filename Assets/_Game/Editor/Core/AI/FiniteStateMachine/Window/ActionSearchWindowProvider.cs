﻿using Game.Core.AI.FiniteStateMachine;
using System;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class ActionSearchWindowProvider : BaseTypeSearchWindowProvider<IAction>
    {
        protected override string SearchWindowTitle => "Select Action";

        private StateNode _stateNote;

        public void Initialize(StateNode stateNode)
        {
            _stateNote = stateNode;
        }

        protected override void OnTypeSelected(Type actionType)
        {
            var actionDefinition = new ActionDefinition();
            actionDefinition.ActionTypeName = actionType.FullName;

            _stateNote.AddAction(actionDefinition);
        }
    }
}
