﻿using Game.Core.AI.FiniteStateMachine;
using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game.Editor.Core.AI.FiniteStateMachine
{
    public class DecisionSearchWindowProvider : BaseTypeSearchWindowProvider<IDecision>
    {
        protected override string SearchWindowTitle => "Select Decision";

        private GraphView _graphView;
        private Vector2 _nodePosition;

        public void Initialize(GraphView graphView, Vector2 nodePosition)
        {
            _graphView = graphView;
            _nodePosition = nodePosition;
        }

        protected override void OnTypeSelected(Type decisionType)
        {
            var decisionDefinition = new DecisionDefinition();
            decisionDefinition.Id = Guid.NewGuid();
            decisionDefinition.DecisionTypeName = decisionType.FullName;

            decisionDefinition.NodePosition = _nodePosition;

            _graphView.AddElement(new DecisionNode(decisionDefinition));
        }
    }
}
