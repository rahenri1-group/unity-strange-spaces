﻿using System;
using UnityEngine.UIElements;

namespace Game.Editor.Core.AI
{
    public class GraphDropdownAction : DropdownMenuAction
    {
        public GraphDropdownAction(
            string actionName,
            Action<DropdownMenuAction> actionCallback,
            Func<DropdownMenuAction, Status> actionStatusCallback,
            object userData = null)
            : base(actionName, actionCallback, actionStatusCallback, userData) {}

        public GraphDropdownAction(
            string actionName,
            Action<DropdownMenuAction> actionCallback) 
            : this(actionName, actionCallback, (e) => DropdownMenuAction.Status.Normal, null) { }
    }
}
