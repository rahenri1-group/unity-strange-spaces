﻿using Game.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game.Editor.Core.AI
{
    public abstract class BaseTypeSearchWindowProvider<T> : ScriptableObject, ISearchWindowProvider
    {
        protected abstract string SearchWindowTitle { get; }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var entries = new List<SearchTreeEntry>();

            entries.Add(new SearchTreeGroupEntry(new GUIContent(SearchWindowTitle)));

            foreach (var assembly in AppDomain.CurrentDomain.GetAssembliesCached())
            {
                foreach (var type in assembly.GetTypesCached())
                {
                    if (!type.IsAbstract && type.GetInterfaces().Contains(typeof(T)))
                    {
                        var seachTreeEntry = new SearchTreeEntry(new GUIContent(ObjectNames.NicifyVariableName(type.Name)));
                        seachTreeEntry.level = 1;
                        seachTreeEntry.userData = type;
                        entries.Add(seachTreeEntry);
                    }
                }
            }

            return entries;
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            var selectedType = (Type)searchTreeEntry.userData;

            OnTypeSelected(selectedType);

            return true;
        }

        protected abstract void OnTypeSelected(Type type);
    }
}
