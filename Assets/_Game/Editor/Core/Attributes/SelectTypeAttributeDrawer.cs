﻿using Game.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core
{
    [CustomPropertyDrawer(typeof(SelectTypeAttribute))]
    public class SelectTypeAttributeDrawer : PropertyDrawer
    {
        private bool _initializeFold = false;

        private List<Type> _reflectionType;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.ManagedReference) return;

            SelectTypeAttribute utility = (SelectTypeAttribute)attribute;

            LazyGetAllInheritedType(utility.FieldType);

            Rect popupPosition = GetPopupPosition(position);

            string[] typePopupNameArray = _reflectionType.Select(type => type == null ? "<null>" : type.ToString()).ToArray();

            string[] typeFullNameArray = _reflectionType.Select(type => type == null ? "" : string.Format("{0} {1}", type.Assembly.ToString().Split(',')[0], type.FullName)).ToArray();

            //Get the type of serialized object 

            int currentTypeIndex = Array.IndexOf(typeFullNameArray, property.managedReferenceFullTypename);
            Type currentObjectType = _reflectionType[currentTypeIndex];

            int selectedTypeIndex = EditorGUI.Popup(popupPosition, currentTypeIndex, typePopupNameArray);

            if (selectedTypeIndex >= 0 && selectedTypeIndex < _reflectionType.Count)
            {
                if (currentObjectType != _reflectionType[selectedTypeIndex])
                {
                    if (_reflectionType[selectedTypeIndex] == null)
                    {
                        //bug? NullReferenceException occurs when put null 

                        property.managedReferenceValue = null;
                    }
                    else
                    {
                        property.managedReferenceValue = Activator.CreateInstance(_reflectionType[selectedTypeIndex]);
                    }

                    currentObjectType = _reflectionType[selectedTypeIndex];
                }
            }

            if (_initializeFold == false)
            {
                property.isExpanded = false;
                _initializeFold = true;
            }

            EditorGUI.PropertyField(position, property, label, true);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, true);
        }

        private void LazyGetAllInheritedType(System.Type baseType)
        {
            if (_reflectionType != null) return;

            _reflectionType = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => baseType.IsAssignableFrom(p) && !p.IsAbstract)
                .ToList();

            _reflectionType.Insert(0, null);
        }

        private Rect GetPopupPosition(Rect currentPosition)
        {
            Rect popupPosition = new Rect(currentPosition);

            popupPosition.width -= EditorGUIUtility.labelWidth;
            popupPosition.x += EditorGUIUtility.labelWidth;
            popupPosition.height = EditorGUIUtility.singleLineHeight;

            return popupPosition;
        }
    }
}
