﻿using UnityEngine;
using UnityEditor;
using Game.Core;
using System;

namespace Game.Editor.Core
{
    [CustomPropertyDrawer(typeof(TypeRestrictionAttribute))]
    public class TypeRestrictionAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var typeRestrictionAttribute = this.attribute as TypeRestrictionAttribute;

            Type fieldType = (fieldInfo.FieldType.IsArray) ? fieldInfo.FieldType.GetElementType() : fieldInfo.FieldType;

            var obj = EditorGUI.ObjectField(position, label, property.objectReferenceValue, fieldType, typeRestrictionAttribute.AllowSceneObjects);

            if (obj != null)
            {
                var component = obj as Component;

                if (component != null && component.GetComponent(typeRestrictionAttribute.Contract) != null)
                {
                    property.objectReferenceValue = obj;
                }
                else if (typeRestrictionAttribute.Contract.IsAssignableFrom(obj.GetType()))
                {
                    property.objectReferenceValue = obj;
                }
                else
                {
                    Debug.LogError($"Invalid selection, {property.name} must have a {typeRestrictionAttribute.Contract.Name}");
                }
            }
            else
            {
                property.objectReferenceValue = null;
            }

            EditorGUI.EndProperty();
        }

    }
}