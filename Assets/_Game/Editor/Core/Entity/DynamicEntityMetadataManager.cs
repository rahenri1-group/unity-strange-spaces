﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Resource;
using Game.Editor.Core.Resource;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.Core.Entity
{
    /// <summary>
    /// Manages the <see cref="MetadataObject"/> for the a <see cref="IDynamicEntity"/>.
    /// </summary>
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class DynamicEntityMetadataManager : BaseEditorModule
    {
        private readonly IMetadataAssetManager _metadataAssetManager;

        private MetadataObject _metadataForCurrentPrefab;
        private DynamicEntityMetadata _entityMetadataForCurrentPrefab;
        private bool _subscribedEvents;

        /// <summary>
        /// Constructor
        /// </summary>
        public DynamicEntityMetadataManager(IMetadataAssetManager metadataAssetManager)
        {
            _metadataAssetManager = metadataAssetManager;

            _metadataForCurrentPrefab = null;
            _entityMetadataForCurrentPrefab = null;
            _subscribedEvents = false;
        }

        /// <inheritdoc/>
        public override void Initialize()
        {
            base.Initialize();

            PrefabStage.prefabStageOpened += OnPrefabStageOpened;
            PrefabStage.prefabStageClosing += OnPrefabStageClosing;

            UpdateState();
        }

        private void OnPrefabStageOpened(PrefabStage stage)
        {
            UpdateState();
        }

        private void OnPrefabStageClosing(PrefabStage stage)
        {
            UpdateState();
        }

        private void UpdateState()
        {
            _metadataForCurrentPrefab = null;
            _entityMetadataForCurrentPrefab = null;

            var currentStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (currentStage != null)
            {
                _metadataForCurrentPrefab = _metadataAssetManager.GetMetadataForResource(currentStage.assetPath);

                if (_metadataForCurrentPrefab != null)
                {
                    _entityMetadataForCurrentPrefab = _metadataForCurrentPrefab.GetMetadataComponent<DynamicEntityMetadata>();
                }
            }

            if (!_subscribedEvents && _entityMetadataForCurrentPrefab != null)
            {
                SceneView.duringSceneGui += DuringSceneGui;
                PrefabStage.prefabSaved += OnPrefabSaved;

                _subscribedEvents = true;
            }
            else if (_subscribedEvents && _entityMetadataForCurrentPrefab == null)
            {
                SceneView.duringSceneGui -= DuringSceneGui;
                PrefabStage.prefabSaved -= OnPrefabSaved;

                _subscribedEvents = false;
            }
        }

        private void DuringSceneGui(SceneView view)
        {
            if (_entityMetadataForCurrentPrefab == null) return;

            Handles.color = Color.magenta;
            HandlesUtil.DrawBounds(_entityMetadataForCurrentPrefab.MeshBounds);
        }

        private void OnPrefabSaved(GameObject prefab)
        {
            if (_entityMetadataForCurrentPrefab == null) return;

            var bounds = new Bounds();

            var prefabRenderers = prefab.GetComponentsInChildren<Renderer>();
            foreach (var renderer in prefabRenderers)
            {
                // skip particle systems when calculating bounds
                if (renderer is ParticleSystemRenderer)
                {
                    continue;
                }

                bounds.Encapsulate(renderer.bounds);
            }

            _entityMetadataForCurrentPrefab.MeshBounds = bounds;

            EditorUtility.SetDirty(_metadataForCurrentPrefab);
            AssetDatabase.SaveAssets();
        }
    }
}
