﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Space;
using Game.Editor.Core.Resource;
using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Editor.Core.Entity
{
    [Serializable]
    public class SceneDynamicEntityEditorStage : PreviewSceneStage
    {
        [Inject] private IEditorAssetResourceManager _assetResourceManager = null;

        private SpaceData _spaceData;
        private MeshFilter[] _contextMeshes;

        private GameObject _contextGameObject;

        public static SceneDynamicEntityEditorStage CreateStage(SpaceData spaceData, MeshFilter[] contextMeshes)
        {
            var stage = CreateInstance<SceneDynamicEntityEditorStage>();
            stage.Init(spaceData, contextMeshes);
            return stage;
        }

        private void Init(SpaceData spaceData, MeshFilter[] contextMeshs)
        {
            EditorModuleContext.DiContainer.Inject(this);

            _spaceData = spaceData;
            _contextMeshes = contextMeshs;

            _contextGameObject = null;
        }

        protected override bool OnOpenStage()
        {
            base.OnOpenStage();

            _contextGameObject = new GameObject("Context");
            _contextGameObject.transform.position = Vector3.zero;
            _contextGameObject.transform.rotation = Quaternion.identity;
            _contextGameObject.transform.localScale = Vector3.one;
            SceneManager.MoveGameObjectToScene(_contextGameObject, scene);

            // create context
            foreach (var contextMesh in _contextMeshes)
            {
                var contextRender = contextMesh.GetComponent<MeshRenderer>();

                if (contextRender == null) continue;

                var gameObject = new GameObject();
                gameObject.transform.SetParent(_contextGameObject.transform);
                gameObject.transform.localPosition = contextMesh.transform.position;
                gameObject.transform.localRotation = contextMesh.transform.rotation;
                gameObject.transform.localScale = contextMesh.transform.lossyScale;

                var meshFilter = gameObject.AddComponent<MeshFilter>();
                meshFilter.sharedMesh = contextMesh.sharedMesh;

                var meshRenderer = gameObject.AddComponent<MeshRenderer>();
                meshRenderer.sharedMaterials = contextRender.sharedMaterials;
            }

            var sceneVisibilityManager = SceneVisibilityManager.instance;
            sceneVisibilityManager.DisablePicking(_contextGameObject, true);
            _contextGameObject.hideFlags = HideFlags.HideAndDontSave;

            // load entities
            var entitiesManifest = _spaceData.GetAdditionalData<EntityManifestComponent>();

            foreach (var entityData in entitiesManifest.Entities)
            {
                var asset = _assetResourceManager.LoadAssetByAddress<GameObject>(entityData.EntityAssetKey);
                if (asset == null)
                {
                    Debug.LogWarning($"Could not load '{entityData.Name}' : '{entityData.EntityAssetKey}'");
                    continue;
                }

                var entityGameObject = PrefabUtility.InstantiatePrefab(asset) as GameObject;
                var entity = entityGameObject.GetComponent<IDynamicEntity>();
                SceneManager.MoveGameObjectToScene(entityGameObject, scene);

                entityGameObject.name = entityData.Name;
                entityGameObject.transform.localPosition = entityData.Position;
                entityGameObject.transform.localRotation = entityData.Rotation;
                entityGameObject.transform.localScale = entityData.Scale;
                entity.InitializeFromEditorCache(entityData.Id, entityData.SerializedJson);
            }

            return true;
        }

        protected override void OnFirstTimeOpenStageInSceneView(SceneView sceneView)
        {
            Selection.activeObject = null;

            // Frame in scene view
            sceneView.FrameSelected(false, true);

            // Setup Scene view state
            sceneView.sceneViewState.showFlares = false;
            sceneView.sceneViewState.alwaysRefresh = false;
            sceneView.sceneViewState.showFog = false;
            sceneView.sceneViewState.showSkybox = false;
            sceneView.sceneViewState.showImageEffects = false;
            sceneView.sceneViewState.showParticleSystems = false;
            sceneView.sceneLighting = false;
        }

        protected override GUIContent CreateHeaderContent()
        {
            return new GUIContent("Scene Entity Configuration");
        }
    }
}
