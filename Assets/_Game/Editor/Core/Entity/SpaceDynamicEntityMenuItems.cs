﻿using Game.Core;
using Game.Core.Entity;
using Game.Editor.Core.Resource;
using Game.Editor.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.Core.Entity
{
    public static class SpaceDynamicEntityMenuItems
    {
        private static IEditorAssetResourceManager AssetResourceManager
        {
            get
            {
                if (_assetResourceManager == null) _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();
                return _assetResourceManager;
            }
        }
        private static IEditorAssetResourceManager _assetResourceManager = null;

        private static IEditorSpaceManager SpaceManager
        {
            get
            {
                if (_spaceManager == null) _spaceManager = EditorModuleContext.DiContainer.Resolve<IEditorSpaceManager>();
                return _spaceManager;
            }
        }
        private static IEditorSpaceManager _spaceManager = null;

        #region Load Entities

        [MenuItem("Game/Space/Load Dynamic Entities")]
        static void LoadDynamicSpaceEntities()
        {
            var contextMeshes = StageUtility.GetCurrentStage().FindComponentsOfType<MeshFilter>();

            var stage = SceneDynamicEntityEditorStage.CreateStage(SpaceManager.CurrentSpace, contextMeshes);
            StageUtility.GoToStage(stage, true);

            Debug.Log($"Loaded entities for '{SpaceManager.CurrentSpace.Name}'");
        }

        [MenuItem("Game/Space/Load Dynamic Entities", true)]
        static bool LoadSpaceDynamicEntitiesValidate()
        {
            return !EditorApplication.isPlaying && StageUtility.GetCurrentStage() == StageUtility.GetMainStage() && SpaceManager.CurrentSpace != null && SpaceManager.CurrentSpace.GetAdditionalData<EntityManifestComponent>() != null;
        }

        #endregion

        #region Save Entities

        [MenuItem("Game/Space/Save Dynamic Entities")]
        static void SaveSpaceDynamicEntities()
        {
            var entitiesManifest = SpaceManager.CurrentSpace.GetAdditionalData<EntityManifestComponent>();

            if (entitiesManifest.Entities.Length > 0)
            {
                if (!EditorUtility.DisplayDialog("Save Entities", "Overwrite existing entity data?", "Save", "Cancel")) return;
            }

            var sceneEntities = StageUtility.GetCurrentStage()
                .FindComponentsOfType<InjectedBehaviour>()
                .OfType<IDynamicEntity>()
                .ToArray();

            if (!ValidateEntitiesForSave(sceneEntities))
            {
                return;
            }

            var entitiesList = new List<EntityData>();

            foreach (var entity in sceneEntities)
            {
                var prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(entity.GameObject);

                var address = AssetResourceManager.GetAddressForAsset(prefabPath);

                var entityData = new EntityData(entity.EntityId, entity.GameObject.name, address);
                    
                entityData.Position = entity.GameObject.transform.localPosition;
                entityData.Rotation = entity.GameObject.transform.localRotation;
                entityData.Scale = entity.GameObject.transform.localScale;
                entityData.SerializedJson = entity.SerializeToJson();

                entitiesList.Add(entityData);
            }

            entitiesManifest.EntitiesEditor = entitiesList.ToArray();

            EditorUtility.SetDirty(SpaceManager.CurrentSpace);
            AssetDatabase.SaveAssets();

            Debug.Log($"Entities written to manifest for '{SpaceManager.CurrentSpace.name}'");
        }

        [MenuItem("Game/Space/Save Dynamic Entities", true)]
        static bool SaveSpaceDynamicEntitiesValidate()
        {
            return !EditorApplication.isPlaying && (StageUtility.GetCurrentStage()?.GetType() == typeof(SceneDynamicEntityEditorStage));
        }

        private static bool ValidateEntitiesForSave(IDynamicEntity[] entities)
        {
            foreach (var entity in entities)
            {
                if (!PrefabUtility.IsAnyPrefabInstanceRoot(entity.GameObject))
                {
                    Debug.LogError($"{entity.GameObject.name} is not a prefab");
                    return false;
                }

                if (entity.EntityId == System.Guid.Empty)
                {
                    Debug.LogError($"{entity.GameObject.name} missing id");
                    return false;
                }

                var duplicateGuidEntityNames = entities.Where(e => e.EntityId == entity.EntityId && e != entity).Select(e => e.GameObject.name);
                if (duplicateGuidEntityNames.Count() != 0)
                {
                    Debug.LogError($"Duplicate id '{entity.EntityId}' found for '{entity.GameObject.name}, {string.Join(", ", duplicateGuidEntityNames)}'");
                    return false;
                }

                var prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(entity.GameObject);
                
                if (string.IsNullOrEmpty(AssetResourceManager.GetAddressForAsset(prefabPath)))
                {
                    Debug.LogError($"{entity.GameObject.name} is not an addressable");
                    return false;
                }
            }
            
            return true;
        }

        #endregion
    }
}
