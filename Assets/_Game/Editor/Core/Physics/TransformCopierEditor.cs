﻿using Game.Core.Physics;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core.Physics
{
    [CustomEditor(typeof(TransformCopierBehaviour))]
    [CanEditMultipleObjects]
    public class TransformCopierEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!EditorApplication.isPlaying)
            {
                if (GUILayout.Button("Copy Transform"))
                {
                    foreach (var t in targets)
                    {
                        var transformCopier = t as TransformCopierBehaviour;
                        transformCopier.CopyTransform();
                    }
                }
            }
        }
    }
}
