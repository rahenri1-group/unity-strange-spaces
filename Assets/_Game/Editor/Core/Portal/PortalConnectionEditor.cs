﻿using Game.Core.Portal;
using Game.Core.Space;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core.Portal
{
    [CustomEditor(typeof(StaticPortalConnection))]
    public class PortalConnectionEditor : UnityEditor.Editor
    {
        private SerializedProperty _connectionIdProperty;
        private SerializedProperty _spaceIdAProperty;
        private SerializedProperty _spaceIdBProperty;

        private void OnEnable()
        {
            _connectionIdProperty = serializedObject.FindProperty("_portalConnectionId");
            _spaceIdAProperty = serializedObject.FindProperty("_spaceIdA");
            _spaceIdBProperty = serializedObject.FindProperty("_spaceIdB");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var spaceA = GetSpaceDataForId(_spaceIdAProperty.stringValue);
            var spaceB = GetSpaceDataForId(_spaceIdBProperty.stringValue);

            GUI.enabled = false;
            EditorGUILayout.TextField("Portal Connections Id", _connectionIdProperty.stringValue);
            GUI.enabled = true;

            EditorSpaceId(ref _spaceIdAProperty, ref spaceA);

            EditorSpaceId(ref _spaceIdBProperty, ref spaceB);

            serializedObject.ApplyModifiedProperties();
        }

        private void EditorSpaceId(ref SerializedProperty spaceIdProperty, ref SpaceData currentSpace)
        {
            var selectedSpace = (SpaceData)EditorGUILayout.ObjectField(spaceIdProperty.displayName, currentSpace, typeof(SpaceData), false);
            if (!SpaceUtil.SpaceEquals(selectedSpace, currentSpace))
            {
                currentSpace = selectedSpace;
                spaceIdProperty.stringValue = (currentSpace != null) ? currentSpace.SpaceId.ToString() : string.Empty;
            }
        }

        private SpaceData GetSpaceDataForId(string idString)
        {
            if (string.IsNullOrEmpty(idString) || !System.Guid.TryParse(idString, out System.Guid id)) return null;

            var resultGuids = AssetDatabase.FindAssets($"t:{typeof(SpaceData).Name}");
            foreach (var guid in resultGuids)
            {
                var spaceData = AssetDatabase.LoadAssetAtPath<SpaceData>(AssetDatabase.GUIDToAssetPath(guid));

                if (spaceData != null && spaceData.SpaceId == id)
                {
                    return spaceData;
                }
            }

            return null;
        }
    }
}
