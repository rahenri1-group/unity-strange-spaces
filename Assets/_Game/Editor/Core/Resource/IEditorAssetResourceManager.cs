﻿using System;
using UnityEditor;

namespace Game.Editor.Core.Resource
{
    /// <summary>
    /// Editor manager for resources that are loadable at runtime
    /// </summary>
    public interface IEditorAssetResourceManager : IEditorModule
    {
        /// <summary>
        /// Is the provided asset loadable at runtime
        /// </summary>
        bool IsLoadableAsset(UnityEngine.Object asset);

        /// <summary>
        /// Gets the resource address of an asset. 
        /// Returns <see cref="string.Empty"/> if it does not have an address.
        /// </summary>
        string GetAddressForAsset(UnityEngine.Object asset);

        /// <summary>
        /// Gets the resource address of an asset. 
        /// Returns <see cref="string.Empty"/> if it does not have an address.
        /// </summary>
        string GetAddressForAsset(string assetPath);

        /// <summary>
        /// Returns true if the provided <paramref name="address"/> points to a valid <see cref="UnityEngine.Object"/>
        /// </summary>
        bool IsAddressValid(string address);

        /// <summary>
        /// Returns the asset <see cref="GUID"/>s for all assets with the provided <paramref name="label"/>
        /// </summary>
        GUID[] GetAllAssetsForLabel(string label);

        /// <summary>
        /// Gets the main <see cref="Type"/> of the asset at the provided address. Returns null if the address is invalid.
        /// Similiar to <see cref="UnityEditor.AssetDatabase.GetMainAssetTypeAtPath"/>
        /// </summary>
        Type GetMainTypeOfAssetAtAddress(string address);

        /// <summary>
        /// Loads an <see cref="UnityEngine.Object"/> with the provided resource address. 
        /// If the address is invalid, null will be returned.
        /// </summary>
        T LoadAssetByAddress<T>(string address) where T : UnityEngine.Object;
    }
}
