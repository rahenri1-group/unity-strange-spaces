﻿using Game.Core.Resource;

namespace Game.Editor.Core.Resource
{
    /// <summary>
    /// Manager for <see cref="MetadataObject"/>s at edit time
    /// </summary>
    public interface IMetadataAssetManager : IEditorModule
    {
        /// <summary>
        /// Checks if there exists a <see cref="MetadataObject"/> for a resource. 
        /// Note: the resource must be an addressable
        /// </summary>
        /// <param name="assetPath"></param>
        /// <returns></returns>
        bool MetadataExists(string assetPath);

        /// <summary>
        /// Creates a <see cref="MetadataObject"/> for a resource. If metadata already exists, the existing metadata will be returned.
        /// Note: the resource must be an addressable
        /// </summary>
        /// <param name="assetPath"></param>
        /// <returns></returns>
        MetadataObject CreateMetadataForResource(string assetPath);

        /// <summary>
        /// Gets the <see cref="MetadataObject"/> for a resource
        /// </summary>
        /// <param name="assetPath"></param>
        /// <returns></returns>
        MetadataObject GetMetadataForResource(string assetPath);
    }
}
