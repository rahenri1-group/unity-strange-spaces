﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core
{
    /// <summary>
    /// Utility class for <see cref="AssetDatabase"/>
    /// </summary>
    public static class AssetDatabaseUtil
    {
        /// <summary>
        /// Creates a new asset at the path. If folders in the path do not exist, they will be created.
        /// See <see cref="AssetDatabase.CreateAsset(Object, string)"/>
        /// </summary>
        public static void CreateAssetAndFolders(Object asset, string path)
        {
            string[] splitPath = path.Split('/');

            string accumulatedPath = "Assets";

            // last entry is the filename
            for (int i = 0; i < splitPath.Length - 1; i++)
            {
                if (i == 0 && splitPath[i] == "Assets") continue;

                var subDirectoryName = splitPath[i];

                if (!AssetDatabase.IsValidFolder($"{accumulatedPath}/{subDirectoryName}"))
                {
                    AssetDatabase.CreateFolder(accumulatedPath, subDirectoryName);
                }

                accumulatedPath += $"/{subDirectoryName}";
            }

            AssetDatabase.CreateAsset(asset, path);
        }

        /// <summary>
        /// Finds and loads all scriptable object instances of <typeparamref name="T"/>
        /// </summary>
        public static T[] FindAllScriptableObjectInstances<T>() where T : ScriptableObject
        {
            var guids = AssetDatabase.FindAssets($"t:{typeof(T).Name}");

            var instances = new List<T>();
            foreach (var assetGuid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
                instances.Add(AssetDatabase.LoadAssetAtPath<T>(assetPath));
            }

            return instances.ToArray();
        }

        /// <summary>
        /// Finds and loads all prefab objects with <typeparamref name="T"/> at the root of the object
        /// </summary>
        public static T[] FindAllPrefabsWithScriptAtRoot<T>()
        {
            var guids = AssetDatabase.FindAssets($"t:prefab");

            var instances = new List<T>();
            foreach (var assetGuid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
                var rootObject = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                var behaviour = rootObject.GetComponent<T>();
                if (behaviour != null)
                {
                    instances.Add(behaviour);
                }
            }

            return instances.ToArray();
        }
    }
}
