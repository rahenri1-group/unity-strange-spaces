﻿using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core
{
    /// <summary>
    /// Utility class for <see cref="Handles"/>
    /// </summary>
    public static class HandlesUtil
    {
        /// <summary>
        /// Draws a wire cylinder
        /// </summary>
        /// <param name="bottomPosition"></param>
        /// <param name="normal"></param>
        /// <param name="radius"></param>
        /// <param name="height"></param>
        public static void DrawWireCylinder(Vector3 bottomPosition, Vector3 normal, float radius, float height)
        {
            int ringCount = Mathf.Max((int)(3f * height / radius) + 1, 3);

            for (int i = 0; i < ringCount; i++)
            {
                Handles.DrawWireDisc(bottomPosition + height * ((float)i / (ringCount - 1)) * normal, normal, radius);
            }
        }

        /// <summary>
        /// Draws a bounds
        /// </summary>
        /// <param name="bounds"></param>
        public static void DrawBounds(Bounds bounds)
        {
            Handles.DrawWireCube(bounds.center, bounds.size);
        }
    }
}
