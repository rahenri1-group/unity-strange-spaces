﻿namespace Game.Editor.Core
{
    /// <inheritdoc cref="IEditorModule"/>
    public abstract class BaseEditorModule : IEditorModule
    {
        /// <inheritdoc/>
        public virtual void Initialize() { }
    }
}
