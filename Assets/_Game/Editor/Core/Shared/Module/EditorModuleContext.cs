﻿using Game.Core.DependencyInjection;

namespace Game.Editor.Core
{
    /// <summary>
    /// Context for <see cref="IEditorModule"/>s
    /// </summary>
    public static class EditorModuleContext
    {
        /// <summary>
        /// The injection container
        /// </summary>
        public static IInjectionContainer DiContainer
        {
            get => _container;
            set
            {
                if (_container != null)
                {
                    UnityEngine.Debug.LogError($"There was an attempt to set {typeof(EditorModuleContext).Name}.DiContainer after it has already been set!");
                    return;
                }
                _container = value;
            }
        }
        private static IInjectionContainer _container = null;
    }
}
