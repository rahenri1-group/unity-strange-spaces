﻿namespace Game.Editor.Core
{
    /// <summary>
    /// The interface for all editor modules 
    /// </summary>
    public interface IEditorModule
    {

        /// <summary>
        /// The entry point for module initialization
        /// </summary>
        void Initialize();
    }
}
