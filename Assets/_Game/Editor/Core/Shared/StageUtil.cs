﻿using Game.Editor.Core.Entity;
using UnityEditor.SceneManagement;

namespace Game.Editor.Core
{
    /// <summary>
    /// Utility class for <see cref="Stage"/>s
    /// </summary>
    public static class StageUtil
    {
        /// <summary>
        /// Returns true if the stage provided displays a level space
        /// </summary>
        public static bool IsStageForSpace(Stage stage)
        {
            return stage == StageUtility.GetMainStage() || stage.GetType() == typeof(SceneDynamicEntityEditorStage);
        }
    }
}
