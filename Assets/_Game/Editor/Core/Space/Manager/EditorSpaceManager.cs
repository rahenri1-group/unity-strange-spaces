﻿using Game.Core.DependencyInjection;
using Game.Core.Space;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.Core.Space
{
    /// <inheritdoc cref="IEditorSpaceManager"/>
    [Dependency(
        contract: typeof(IEditorSpaceManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class EditorSpaceManager : BaseEditorModule, IEditorSpaceManager
    {
        /// <inheritdoc />
        public SpaceData CurrentSpace { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public EditorSpaceManager()
            : base()
        {
            CurrentSpace = null;
        }

        /// <inheritdoc />
        public override void Initialize()
        {
            base.Initialize();

            if (!string.IsNullOrEmpty(EditorSceneManager.GetActiveScene().name))
            {
                DelayedInitialize();
            }
            else
            {
                // unity defaults to a new empty scene at startup
                EditorApplication.update += DelayInitializeCheck;
            }
        }

        private void DelayInitializeCheck()
        {
            if (!string.IsNullOrEmpty(EditorSceneManager.GetActiveScene().name))
            {
                EditorApplication.update -= DelayInitializeCheck;

                DelayedInitialize();
            }
        }

        private void DelayedInitialize()
        {
            var activeScene = EditorSceneManager.GetActiveScene();

            UpdateSpaceDataForScene(activeScene);

            EditorSceneManager.activeSceneChangedInEditMode += OnActiveSceneChangedInEditMode;
        }

        private void OnActiveSceneChangedInEditMode(UnityEngine.SceneManagement.Scene oldScene, UnityEngine.SceneManagement.Scene nextScene)
        {
            if (EditorSceneManager.sceneCount == 1
                && !string.IsNullOrEmpty(nextScene.path))
            {
                UpdateSpaceDataForScene(nextScene);
            }
            else
            {
                CurrentSpace = null;
            }
        }

        private void UpdateSpaceDataForScene(UnityEngine.SceneManagement.Scene scene)
        {
            CurrentSpace = null;

            var resultGuids = AssetDatabase.FindAssets($"t:{typeof(SpaceData).Name}");
            foreach (var guid in resultGuids)
            {
                var spaceData = AssetDatabase.LoadAssetAtPath<SpaceData>(AssetDatabase.GUIDToAssetPath(guid));

                if (spaceData != null && spaceData.SceneBuildIndex == scene.buildIndex)
                {
                    CurrentSpace = spaceData;
                    return;
                }
            }

            Debug.LogWarning($"No {typeof(SpaceData).Name} found for scene '{scene.name}'");
        }
    }
}
