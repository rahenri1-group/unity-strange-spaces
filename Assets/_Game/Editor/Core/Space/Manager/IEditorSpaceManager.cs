﻿using Game.Core.Space;

namespace Game.Editor.Core.Space
{
    /// <summary>
    /// Manager for editor interaction with <see cref="SpaceData"/>
    /// </summary>
    public interface IEditorSpaceManager : IEditorModule
    {
        /// <summary>
        /// The <see cref="SpaceData"/> for the current open scene. Returns null if there is no data
        /// </summary>
        SpaceData CurrentSpace { get; }
    }
}
