﻿using UnityEditor;
using UnityEditor.SceneManagement;

namespace Game.Editor.Core.Space
{
    public static class SpaceMenuItems
    {
        private static IEditorSpaceManager SpaceManager
        {
            get
            {
                if (_spaceManager == null) _spaceManager = EditorModuleContext.DiContainer.Resolve<IEditorSpaceManager>();
                return _spaceManager;
            }
        }
        private static IEditorSpaceManager _spaceManager = null;

        [MenuItem("Game/Space/Select SpaceData For Current Scene", false, -100)]
        static void SelectSpaceData()
        {
            Selection.activeObject = SpaceManager.CurrentSpace;
            EditorGUIUtility.PingObject(SpaceManager.CurrentSpace);
        }

        [MenuItem("Game/Space/Select SpaceData For Current Scene", true, -100)]
        static bool SelectSpaceDataValidate()
        {
            return !EditorApplication.isPlaying && StageUtility.GetCurrentStage() == StageUtility.GetMainStage() && SpaceManager.CurrentSpace != null;
        }
    }
}
