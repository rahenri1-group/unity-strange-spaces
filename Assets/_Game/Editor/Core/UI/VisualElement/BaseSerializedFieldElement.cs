﻿using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public abstract class BaseSerializedFieldElement : VisualElement
    {
        public string FieldName { get; }

        public abstract Label Label { get; }
        public abstract string Value { get; }

        public BaseSerializedFieldElement(string fieldName)
            : base()
        {
            FieldName = fieldName;
        }
    }
}
