﻿using UnityEditor;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public class SerializedBoolFieldElement : BaseSerializedFieldElement
    {
        public bool BoolValue
        {
            get => _toggle.value;
            set => _toggle.value = value;
        }

        public override string Value => BoolValue.ToString();

        public override Label Label => _toggle.labelElement;

        private Toggle _toggle;

        public SerializedBoolFieldElement(string fieldName)
            : base(fieldName)
        {
            _toggle = new Toggle();
            _toggle.label = ObjectNames.NicifyVariableName(fieldName);
            Add(_toggle);
        }
    }
}
