﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public class SerializedEnumFieldElement : BaseSerializedFieldElement
    {
        public int EnumValue
        {
            get
            {
                return _enumValues[_enumDropdown.index];
            }
            set
            {
                var index = Array.FindIndex(_enumValues, e => e == value);
                _enumDropdown.index = index;
            }
        }

        public override string Value => EnumValue.ToString();

        public override Label Label => _enumDropdown.labelElement;

        private DropdownField _enumDropdown;

        private string[] _enumNames;
        private int[] _enumValues;

        public SerializedEnumFieldElement(string fieldName, Type enumType)
            : base(fieldName)
        {
            _enumDropdown = new DropdownField();
            _enumDropdown.label = ObjectNames.NicifyVariableName(fieldName);

            _enumNames = Enum.GetNames(enumType);
            _enumValues = (int[])Enum.GetValues(enumType);

            _enumDropdown.choices = _enumNames.ToList();

            EnumValue = 0;

            Add(_enumDropdown);
        }
    }
}
