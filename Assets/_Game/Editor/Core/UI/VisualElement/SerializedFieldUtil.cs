﻿using Game.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public static class SerializedFieldUtil
    {
        public static BaseSerializedFieldElement[] CreateSerializedFields(VisualElement container, Type type)
        {
            return CreateSerializedFields(container, type, new Dictionary<string, string>(), false);
        }

        public static BaseSerializedFieldElement[] CreateSerializedFields(VisualElement container, Type type, bool isReadonly)
        {
            return CreateSerializedFields(container, type, new Dictionary<string, string>(), isReadonly);
        }

        public static BaseSerializedFieldElement[] CreateSerializedFields(VisualElement container, Type type, Dictionary<string, string> initialValues)
        {
            return CreateSerializedFields(container, type, initialValues, false);
        }

        public static BaseSerializedFieldElement[] CreateSerializedFields(VisualElement container, Type type, Dictionary<string, string> initialValues, bool isReadonly)
        {
            var serializedFieldElements = new List<BaseSerializedFieldElement>();

            var members = type.GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<SerializeField>().Any())
                .ToArray();

            foreach (var field in members.OfType<FieldInfo>())
            {
                BaseSerializedFieldElement fieldElement = null;

                if (field.FieldType == typeof(float))
                {
                    var floatField = new SerializedFloatFieldElement(field.Name);
                    if (initialValues.ContainsKey(field.Name))
                        floatField.FloatValue = float.Parse(initialValues[field.Name]);

                    fieldElement = floatField;
                }
                else if (field.FieldType == typeof(int))
                {
                    var integerField = new SerializedIntegerFieldElement(field.Name);
                    if (initialValues.ContainsKey(field.Name))
                        integerField.IntegerValue = int.Parse(initialValues[field.Name]);

                    fieldElement = integerField;
                }
                else if (field.FieldType == typeof(bool))
                {
                    var boolField = new SerializedBoolFieldElement(field.Name);
                    if (initialValues.ContainsKey(field.Name))
                        boolField.BoolValue = bool.Parse(initialValues[field.Name]);

                    fieldElement = boolField;
                }
                else if (field.FieldType == typeof(string))
                {
                    var textField = new SerializedStringFieldElement(field.Name);
                    if (initialValues.ContainsKey(field.Name))
                        textField.StringValue = initialValues[field.Name];

                    fieldElement = textField;
                }
                else if (field.FieldType.IsEnum)
                {
                    var enumField = new SerializedEnumFieldElement(field.Name, field.FieldType);
                    if (initialValues.ContainsKey(field.Name))
                        enumField.EnumValue = int.Parse(initialValues[field.Name]);

                    fieldElement = enumField;
                }
                else
                {
                    Debug.LogWarning($"Unable to generate a serialized field for {field.Name} ({field.FieldType})");
                }

                if (fieldElement != null)
                {
                    fieldElement.Label.style.minWidth = 50f;
                    fieldElement.SetEnabled(!isReadonly);
                    serializedFieldElements.Add(fieldElement);
                    container.Add(fieldElement);
                }
            }

            return serializedFieldElements.ToArray();
        }
    }
}
