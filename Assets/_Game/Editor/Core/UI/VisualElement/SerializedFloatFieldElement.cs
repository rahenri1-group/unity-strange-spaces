﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public class SerializedFloatFieldElement : BaseSerializedFieldElement
    {
        public float FloatValue
        {
            get => _floatField.value;
            set => _floatField.value = value;
        }

        public override string Value => FloatValue.ToString();

        public override Label Label => _floatField.labelElement;

        private FloatField _floatField;

        public SerializedFloatFieldElement(string fieldName)
            : base (fieldName)
        {
            _floatField = new FloatField(); 
            _floatField.label = ObjectNames.NicifyVariableName(fieldName);
            Add(_floatField);
        }
    }
}
