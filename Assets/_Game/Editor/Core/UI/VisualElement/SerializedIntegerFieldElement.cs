﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public class SerializedIntegerFieldElement : BaseSerializedFieldElement
    {
        public int IntegerValue
        {
            get => _integerField.value;
            set => _integerField.value = value;
        }

        public override string Value => IntegerValue.ToString();

        public override Label Label => _integerField.labelElement;

        private IntegerField _integerField;

        public SerializedIntegerFieldElement(string fieldName)
            : base(fieldName)
        {
            _integerField = new IntegerField();
            _integerField.label = ObjectNames.NicifyVariableName(fieldName);
            Add(_integerField);
        }
    }
}
