﻿using UnityEditor;
using UnityEngine.UIElements;

namespace Game.Editor.Core.UI
{
    public class SerializedStringFieldElement : BaseSerializedFieldElement
    {
        public string StringValue
        {
            get => _textField.value;
            set => _textField.value = value;
        }

        public override string Value => StringValue;

        public override Label Label => _textField.labelElement;

        private TextField _textField;

        public SerializedStringFieldElement(string fieldName)
            : base(fieldName)
        {
            _textField = new TextField();
            _textField.label = ObjectNames.NicifyVariableName(fieldName);
            Add(_textField);
        }
    }
}
