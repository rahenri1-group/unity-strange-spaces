﻿using Game.Core;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core
{
    [CustomPropertyDrawer(typeof(ColorHdrReference))]
    [CustomPropertyDrawer(typeof(ColorHdrReadonlyReference))]

    public class ColorHdrReferenceDrawer : PropertyDrawer
    {
        private readonly string[] popupOptions = { "Use Inline", "Use Variable" };

        private GUIStyle popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (popupStyle == null)
            {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            SerializedProperty useInline = property.FindPropertyRelative("_useInline");
            SerializedProperty inlineValue = property.FindPropertyRelative("_inlineValue");
            SerializedProperty variable = property.FindPropertyRelative("_variable");

            Rect buttonRect = new Rect(position);
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(
                buttonRect, 
                useInline.boolValue ? 0 : 1, 
                popupOptions, 
                popupStyle);

            useInline.boolValue = result == 0;

            if (useInline.boolValue)
            {
                inlineValue.colorValue = EditorGUI.ColorField(position, GUIContent.none, inlineValue.colorValue, true, false, true);
            }
            else
            {
                EditorGUI.PropertyField(position, variable, GUIContent.none);
            }

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}
