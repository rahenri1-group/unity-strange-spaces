﻿using Game.Core;
using UnityEditor;
using UnityEngine;

namespace Game.Editor.Core
{
    [CustomEditor(typeof(ColorHdrVariable))]
    public class ColorHdrVariableEditor : UnityEditor.Editor
    {
        private SerializedProperty _idProperty;
        private SerializedProperty _isConstantProperty;
        private SerializedProperty _triggerEventsProperty;
        private SerializedProperty _valueProperty;

        private void OnEnable()
        {
            _idProperty = serializedObject.FindProperty("_id");
            _isConstantProperty = serializedObject.FindProperty("_isConstant");
            _triggerEventsProperty = serializedObject.FindProperty("_triggerEvents");
            _valueProperty = serializedObject.FindProperty("_value");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            GUI.enabled = false;
            EditorGUILayout.PropertyField(_idProperty);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(_isConstantProperty);
            EditorGUILayout.PropertyField(_triggerEventsProperty);

            _valueProperty.colorValue = EditorGUILayout.ColorField(new GUIContent(_valueProperty.displayName), _valueProperty.colorValue, true, false, true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
