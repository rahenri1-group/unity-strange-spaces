﻿using Game.Core.DependencyInjection;
using Game.Editor.Core;
using Game.Editor.Core.Entity;
using Game.Editor.Core.Space;
using Game.Editor.Player;
using Game.Editor.Resource.Addressable;
using Game.Editor.World.Haunt;
using Game.Editor.World.Item;
using UnityEditor;

namespace Game.Editor
{
    [InitializeOnLoad]
    public static class EditorApp
    {
        static EditorApp()
        {
            InitializeEditorModules();
        }

        private static void InitializeEditorModules()
        {
            var container = new Container();

            container.RegisterType(typeof(AddressableResourceManager));
            container.RegisterType(typeof(DynamicEntityMetadataManager));
            container.RegisterType(typeof(EditorSpaceManager));
            container.RegisterType(typeof(HauntSpawnDrawer));
            container.RegisterType(typeof(ItemSpawnDrawer));
            container.RegisterType(typeof(MetadataAssetManager));
            container.RegisterType(typeof(PlayerSpawnDrawer));

            EditorModuleContext.DiContainer = container;

            var modules = container.Resolve<IEditorModule[]>();
            foreach (var module in modules)
            {
                module.Initialize();
            }
        }
    }
}
