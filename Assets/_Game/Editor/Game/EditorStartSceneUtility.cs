﻿using UnityEditor;
using UnityEditor.SceneManagement;

namespace Game.Editor
{
    [InitializeOnLoad]

    public static class EditorStartSceneUtility
    {
        private static int SceneIndexToUseOnPlay;

        static EditorStartSceneUtility()
        {
            // force start scene to be 0
            EditorSceneManager.playModeStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(EditorBuildSettings.scenes[0].path);

            SceneIndexToUseOnPlay = EditorSceneManager.GetActiveScene().buildIndex;

            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                SceneIndexToUseOnPlay = EditorSceneManager.GetActiveScene().buildIndex;
            }
            else if (state == PlayModeStateChange.EnteredPlayMode)
            {
                App.Config.PostInitializeSceneIndex = SceneIndexToUseOnPlay;
            }
        }
    }
}
