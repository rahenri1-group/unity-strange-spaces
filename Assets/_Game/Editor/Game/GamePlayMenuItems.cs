﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor
{
    public static class GamePlayMenuItems
    {
        [MenuItem("Game/Play/PC")]
        static void PlayPc()
        {
            PlayerPrefs.SetString("Editor_InjectionConfigFileName", "PC-Config");

            EditorApplication.EnterPlaymode();
        }

        [MenuItem("Game/Play/PC", validate = true)]
        static bool PlayPcValidate()
        {
            return !EditorApplication.isPlaying && StageUtility.GetCurrentStage() == StageUtility.GetMainStage();
        }

        [MenuItem("Game/Play/VR")]
        static void PlayVr()
        {
            PlayerPrefs.SetString("Editor_InjectionConfigFileName", "VR-Config");

            EditorApplication.EnterPlaymode();
        }

        [MenuItem("Game/Play/VR", validate = true)]
        static bool PlayVrValidate()
        {
            return !EditorApplication.isPlaying && StageUtility.GetCurrentStage() == StageUtility.GetMainStage();
        }
    }
}
