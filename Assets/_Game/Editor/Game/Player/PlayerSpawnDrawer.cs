﻿using Game.Core.DependencyInjection;
using Game.Editor.Core;
using Game.Editor.Core.Space;
using Game.Player;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.Player
{
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class PlayerSpawnDrawer : BaseEditorModule
    {
        private const string PLAYER_SPAWN_SHOW = "Editor_PlayerSpawnShow";

        private readonly IEditorSpaceManager _editorSpaceManager;

        public PlayerSpawnDrawer(IEditorSpaceManager editorSpaceManager)
            : base ()
        {
            _editorSpaceManager = editorSpaceManager;
        }

        public override void Initialize()
        {
            base.Initialize();

            SceneView.duringSceneGui += DuringSceneGui;
        }

        private void DuringSceneGui(SceneView view)
        {
            if (_editorSpaceManager.CurrentSpace == null || !StageUtil.IsStageForSpace(StageUtility.GetCurrentStage())) return;

            if (PlayerPrefs.GetInt(PLAYER_SPAWN_SHOW) == 0) return;

            var playerData = _editorSpaceManager.CurrentSpace.GetAdditionalData<IPlayerSpawnData>();

            if (playerData == null) return;

            Handles.color = Color.green;
            HandlesUtil.DrawWireCylinder(playerData.Position, playerData.Rotation * Vector3.up, 0.5f, 1.7f);
            Handles.DrawLine(playerData.Position, playerData.Position + playerData.Rotation * Vector3.forward);
        }

        [MenuItem("Game/Space/Player Spawn/Show")]
        public static void PlayerSpawnShow()
        {
            PlayerPrefs.SetInt(PLAYER_SPAWN_SHOW, 1);
        }

        [MenuItem("Game/Space/Player Spawn/Show", true)]
        public static bool PlayerSpawnShowValidate()
        {
            return PlayerPrefs.GetInt(PLAYER_SPAWN_SHOW) == 0;
        }

        [MenuItem("Game/Space/Player Spawn/Hide")]
        public static void PlayerSpawnHide()
        {
            PlayerPrefs.SetInt(PLAYER_SPAWN_SHOW, 0);
        }

        [MenuItem("Game/Space/Player Spawn/Hide", true)]
        public static bool PlayerSpawnHideValidate()
        {
            return PlayerPrefs.GetInt(PLAYER_SPAWN_SHOW) > 0;
        }
    }
}
