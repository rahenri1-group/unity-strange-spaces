﻿using UnityEditor;
using UnityEngine;

namespace Game.Editor.World.Haunt
{
    public class HauntBuildingWearPreviewer
    {
        public const string BuildingWearShaderProperty = "GlobalWear";

        [MenuItem("Game/Haunt/Wear/0%")]
        public static void HauntWear_0()
        {
            Shader.SetGlobalFloat("GlobalWear", 0f);
        }

        [MenuItem("Game/Haunt/Wear/50%")]
        public static void HauntWear_50()
        {
            Shader.SetGlobalFloat("GlobalWear", 0.5f);
        }

        [MenuItem("Game/Haunt/Wear/100%")]
        public static void HauntWear_100()
        {
            Shader.SetGlobalFloat("GlobalWear", 1f);
        }
    }
}
