﻿using Game.Core.DependencyInjection;
using Game.Editor.Core;
using Game.Editor.Core.Space;
using Game.World.Haunt.Data;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.World.Haunt
{
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class HauntSpawnDrawer : BaseEditorModule
    {
        private const string HAUNT_MONSTER_SPAWN_SHOW = "Editor_HauntMonsterSpawnShow";
        private const string HAUNT_TRAP_SPAWN_SHOW = "Editor_HauntTrapSpawnShow";

        private readonly IEditorSpaceManager _editorSpaceManager;

        public HauntSpawnDrawer(IEditorSpaceManager editorSpaceManager)
            : base()
        {
            _editorSpaceManager = editorSpaceManager;
        }

        public override void Initialize()
        {
            base.Initialize();

            SceneView.duringSceneGui += DuringSceneGui;
        }

        private void DuringSceneGui(SceneView view)
        {
            if (_editorSpaceManager.CurrentSpace == null || !StageUtil.IsStageForSpace(StageUtility.GetCurrentStage())) return;

            var hauntData = _editorSpaceManager.CurrentSpace.GetAdditionalData<IHauntSpaceData>();
            if (hauntData == null) return;

            if (PlayerPrefs.GetInt(HAUNT_MONSTER_SPAWN_SHOW) > 0)
            {
                Handles.color = Color.red;
                foreach (var spawn in hauntData.HauntMonsterSpawns)
                {
                    HandlesUtil.DrawWireCylinder(spawn.Position, spawn.Rotation * Vector3.up, 0.5f, 1.7f);
                    Handles.DrawLine(spawn.Position, spawn.Position + spawn.Rotation * Vector3.forward);
                }
            }

            if (PlayerPrefs.GetInt(HAUNT_TRAP_SPAWN_SHOW) > 0)
            {
                Handles.color = new Color(1f, 0.5f, 0f);
                foreach (var spawn in hauntData.HauntTrapSpawns)
                {
                    HandlesUtil.DrawWireCylinder(spawn.Position, spawn.Rotation * Vector3.up, 0.5f, 0.5f);
                    Handles.DrawLine(spawn.Position, spawn.Position + spawn.Rotation * Vector3.forward);
                }
            }
        }

        #region Monster Spawn

        [MenuItem("Game/Haunt/Monster Spawn/Show")]
        public static void HauntMonsterSpawnShow()
        {
            PlayerPrefs.SetInt(HAUNT_MONSTER_SPAWN_SHOW, 1);
        }

        [MenuItem("Game/Haunt/Monster Spawn/Show", true)]
        public static bool HauntMonsterSpawnShowValidate()
        {
            return PlayerPrefs.GetInt(HAUNT_MONSTER_SPAWN_SHOW) == 0;
        }

        [MenuItem("Game/Haunt/Monster Spawn/Hide")]
        public static void HauntMonsterSpawnHide()
        {
            PlayerPrefs.SetInt(HAUNT_MONSTER_SPAWN_SHOW, 0);
        }

        [MenuItem("Game/Haunt/Monster Spawn/Hide", true)]
        public static bool HauntMonsterSpawnHideValidate()
        {
            return PlayerPrefs.GetInt(HAUNT_MONSTER_SPAWN_SHOW) > 0;
        }

        #endregion

        #region Trap Spawn

        [MenuItem("Game/Haunt/Trap Spawn/Show")]
        public static void HauntTrapSpawnShow()
        {
            PlayerPrefs.SetInt(HAUNT_TRAP_SPAWN_SHOW, 1);
        }

        [MenuItem("Game/Haunt/Trap Spawn/Show", true)]
        public static bool HauntTrapSpawnShowValidate()
        {
            return PlayerPrefs.GetInt(HAUNT_TRAP_SPAWN_SHOW) == 0;
        }

        [MenuItem("Game/Haunt/Trap Spawn/Hide")]
        public static void HauntTrapSpawnHide()
        {
            PlayerPrefs.SetInt(HAUNT_TRAP_SPAWN_SHOW, 0);
        }

        [MenuItem("Game/Haunt/Trap Spawn/Hide", true)]
        public static bool HauntTrapSpawnHideValidate()
        {
            return PlayerPrefs.GetInt(HAUNT_TRAP_SPAWN_SHOW) > 0;
        }

        #endregion
    }
}
