﻿using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Editor.Core;
using Game.Editor.Core.Space;
using Game.Item;
using Game.World.Item.Data;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.World.Item
{
    [Dependency(
        contract: typeof(IEditorModule),
        lifetime: Lifetime.Singleton)]
    public class ItemSpawnDrawer : BaseEditorModule
    {
        private const string ITEM_SPAWN_SHOW = "Editor_ItemSpawnShow";

        private readonly IEditorSpaceManager _editorSpaceManager;

        public ItemSpawnDrawer(IEditorSpaceManager editorSpaceManager)
            : base()
        {
            _editorSpaceManager = editorSpaceManager;
        }

        public override void Initialize()
        {
            base.Initialize();

            SceneView.duringSceneGui += DuringSceneGui;
        }

        private void DuringSceneGui(SceneView view)
        {
            if (_editorSpaceManager.CurrentSpace == null || !StageUtil.IsStageForSpace(StageUtility.GetCurrentStage())) return;          

            if (PlayerPrefs.GetInt(ITEM_SPAWN_SHOW) == 0) return;

            var itemSpawnData = _editorSpaceManager.CurrentSpace.GetAdditionalData<IWorldItemsSpawnData>();

            if (itemSpawnData == null) return;

            Handles.color = Color.cyan;
            for (int i = 0; i < itemSpawnData.ItemSpawns.Length; i++)
            {
                var itemSpawn = itemSpawnData.ItemSpawns[i];

                Handles.DrawWireDisc(itemSpawn.Position, Vector3.up, 0.1f);
                if (itemSpawn.FixedSpawnRotation)
                {
                    Handles.DrawLine(itemSpawn.Position, itemSpawn.Position + itemSpawn.Rotation * (0.1f * Vector3.forward));
                }

                var labelContent = new GUIContent(BuildLabelText(i, itemSpawn));
                var labelStyle = new GUIStyle(EditorStyles.label);
                labelStyle.alignment = TextAnchor.MiddleCenter;

                var size = labelStyle.CalcSize(labelContent);
                labelStyle.fixedWidth = size.x;
                labelStyle.fixedHeight = size.y;
                Handles.Label(itemSpawn.Position, labelContent, labelStyle);
            }
        }

        private string BuildLabelText(int spawnIndex, ItemSpawnData itemSpawnData)
        {
            string labelText = $"Item Spawn {spawnIndex}";

            ItemSpawnGroup[] spawnGroups = itemSpawnData.ItemSpawnGroups;

            for (int i = 0; i < spawnGroups.Length; i++)
            {
                var spawnGroup = spawnGroups[i];
                if (spawnGroup != null)
                {
                    labelText += "\n" + spawnGroup.Name;
                }
            }

            return labelText;
        }

        [MenuItem("Game/Item Spawn/Show")]
        public static void HauntSpawnShow()
        {
            PlayerPrefs.SetInt(ITEM_SPAWN_SHOW, 1);
        }

        [MenuItem("Game/Item Spawn/Show", true)]
        public static bool HauntSpawnShowValidate()
        {
            return PlayerPrefs.GetInt(ITEM_SPAWN_SHOW) == 0;
        }

        [MenuItem("Game/Item Spawn/Hide")]
        public static void HauntSpawnHide()
        {
            PlayerPrefs.SetInt(ITEM_SPAWN_SHOW, 0);
        }

        [MenuItem("Game/Item Spawn/Hide", true)]
        public static bool HauntSpawnHideValidate()
        {
            return PlayerPrefs.GetInt(ITEM_SPAWN_SHOW) > 0;
        }
    }
}
