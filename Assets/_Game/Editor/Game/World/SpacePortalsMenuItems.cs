﻿using Game.Core.Portal;
using Game.Editor.Core;
using Game.Editor.Core.Space;
using Game.World.Builder;
using Game.World.Builder.Data;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game.Editor.World.Builder
{
    public static class SpacePortalsMenuItems
    {
        private static IEditorSpaceManager SpaceManager
        {
            get
            {
                if (_spaceManager == null) _spaceManager = EditorModuleContext.DiContainer.Resolve<IEditorSpaceManager>();
                return _spaceManager;
            }
        }
        private static IEditorSpaceManager _spaceManager = null;

        [MenuItem("Game/Space/Sync Portals")]
        static void SyncPortals()
        {
            var worldBuilderData = SpaceManager.CurrentSpace.GetAdditionalData<WorldBuilderDataComponent>();

            var scenePortals = StageUtility.GetCurrentStage().FindComponentsOfType<DynamicPortalBehaviour>();

            var portalDataList = new List<PortalData>();

            foreach (var portal in scenePortals)
            {
                var portalData = new PortalData(portal.PortalId);
                portalData.Position = portal.transform.position;
                portalData.Rotation = portal.transform.rotation;
                portalData.Scale = portal.transform.lossyScale;

                portalData.WorldEntrance = portal.GetComponent<PortalWorldEntranceDataBehaviour>() != null;

                if (worldBuilderData.RoomType == RoomType.Landing && !portalData.WorldEntrance)
                {
                    var landingData = portal.GetComponent<PortalDoorLandingDataBehaviour>();

                    bool dataError = false;

                    if (landingData == null)
                    {
                        Debug.LogError($"Portal {portal.PortalId} missing LandingData");
                        dataError = true;
                    }

                    if (landingData != null && landingData.ZoneEntrance == WorldZone.None)
                    {
                        Debug.LogError($"Portal {portal.PortalId} has an invalid Zone Entrance");
                        dataError = true;
                    }

                    if (landingData != null && (landingData.AllowedDestinationTypes == null || landingData.AllowedDestinationTypes.Length == 0))
                    {
                        Debug.LogError($"Portal {portal.PortalId} missing destination types");
                        dataError = true;
                    }

                    if (dataError == true)
                    {
                        Selection.activeObject = portal;
                        EditorGUIUtility.PingObject(portal);
                        return;
                    }

                    portalData.ZoneEntrance = landingData.ZoneEntrance;
                    portalData.AllowedDestinationTypes = landingData.AllowedDestinationTypes;
                }
                else
                {
                    portalData.ZoneEntrance = WorldZone.None;
                    portalData.AllowedDestinationTypes = new RoomType[0];
                }

                portalDataList.Add(portalData);
            }

            worldBuilderData.DynamicPortalsEditor = portalDataList.ToArray();

            EditorUtility.SetDirty(SpaceManager.CurrentSpace);
            AssetDatabase.SaveAssets();

            Debug.Log($"Portals synced to manifest for '{SpaceManager.CurrentSpace.name}'");
        }

        [MenuItem("Game/Space/Sync Portals", true)]
        static bool SyncPortalsValidate()
        {
            return !EditorApplication.isPlaying && StageUtility.GetCurrentStage() == StageUtility.GetMainStage() && SpaceManager.CurrentSpace != null && SpaceManager.CurrentSpace.GetAdditionalData<WorldBuilderDataComponent>() != null;
        }
    }
}
