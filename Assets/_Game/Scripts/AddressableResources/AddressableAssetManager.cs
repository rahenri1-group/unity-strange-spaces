﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using System;
using UnityEngine.AddressableAssets;

namespace Game.Resource.Addressable
{
    /// <inheritdoc cref="IAssetResourceManager"/>
    [Dependency(
        contract: typeof(IAssetResourceManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class AddressableAssetManager : BaseAddressableResourceManager, IAssetResourceManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Asset Resource Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        /// <summary>
        /// Injection constructor
        /// </summary>
        public AddressableAssetManager(ILogRouter logger)
            : base(logger) { }

        /// <inheritdoc/>
        public async UniTask<T> LoadAsset<T>(string assetKey)
            where T : class
        {
            T loadedAsset = null;

            try
            {
                loadedAsset = await Addressables.LoadAssetAsync<T>(assetKey).ToUniTask();
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                loadedAsset = null;
            }

            if (loadedAsset == null)
            {
                Logger.LogError($"Could not load asset '{assetKey}'");
                return null;
            }

            return loadedAsset;
        }

        /// <inheritdoc/>
        public void ReleaseAsset<T>(T asset)
            where T : class
        {
            Addressables.Release(asset);
        }
    }
}
