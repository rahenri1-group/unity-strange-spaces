﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Resource.Addressable
{
    /// <inheritdoc cref="IGameObjectResourceManager"/>
    [Dependency(
        contract: typeof(IGameObjectResourceManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class AddressableGameObjectManager : BaseAddressableResourceManager, IGameObjectResourceManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "GameObject Resource Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly ISpaceManager _spaceManager;

        private GameObject _resourceHolderGameObject;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public AddressableGameObjectManager(
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(logger)
        {
            _spaceManager = spaceManager;

            _resourceHolderGameObject = null;
        }

        /// <inheritdoc/>
        public async override UniTask Initialize()
        {
            await base.Initialize();

            _resourceHolderGameObject = new GameObject("@ResourceHolder");
            _resourceHolderGameObject.SetActive(false);
            await _spaceManager.MoveObjectToSpaceAsync(_resourceHolderGameObject, null);
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            UnityEngine.Object.Destroy(_resourceHolderGameObject);
            _resourceHolderGameObject = null;

            return base.Shutdown();
        }

        /// <inheritdoc/>
        public UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position)
            where T : class
        {
            return InstantiateAsync<T>(assetKey, space, position, Quaternion.identity, Vector3.one);
        }

        /// <inheritdoc/>
        public UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
            where T : class
        {
            return InstantiateAsync<T>(assetKey, space, position, rotation, Vector3.one);
        }

        /// <inheritdoc/>
        public async UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale) 
            where T : class
        {
            var gameObject = await InstantiateAsync(assetKey, space, position, rotation, scale);
            if (gameObject == null) return (null, null);

            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                Logger.LogError($"{assetKey} missing type {typeof(T).Name}");
                DestroyObject(gameObject);
                return (null, null);
            }

            return (gameObject, component);
        }

        /// <inheritdoc/>
        public UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position)
        {
            return InstantiateAsync(assetKey, space, position, Quaternion.identity, Vector3.one);
        }

        /// <inheritdoc/>
        public UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
        {
            return InstantiateAsync(assetKey, space, position, rotation, Vector3.one);
        }

        /// <inheritdoc/>
        public async UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            GameObject gameObject = null;
            try
            {
                var handle = Addressables.InstantiateAsync(assetKey, position, rotation, _resourceHolderGameObject.transform);
                if (handle.IsDone) // can happen if asset is already loaded
                {
                    if (handle.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded)
                    {
                        gameObject = handle.Result;
                    }
                }
                else
                {
                    gameObject = await handle.ToUniTask();
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                gameObject = null;
            }

            if (gameObject == null)
            {
                Logger.LogError($"Could not load asset '{assetKey}'");
                return null;
            }

            gameObject.transform.SetParent(null);
            gameObject.transform.localScale = scale;

            await _spaceManager.MoveObjectToSpaceAsync(gameObject, space);

            return gameObject;
        }

        /// <inheritdoc/>
        public void DestroyObject(GameObject gameObject)
        {
            Addressables.ReleaseInstance(gameObject);
        }
    }
}
