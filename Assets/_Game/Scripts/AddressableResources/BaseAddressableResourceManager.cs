﻿using Cysharp.Threading.Tasks;
using Game.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;

namespace Game.Resource.Addressable
{
    public abstract class BaseAddressableResourceManager : BaseModule
    {
        private IResourceLocator _resourceLocator;

        public BaseAddressableResourceManager(ILogRouter logger)
            : base(logger)
        {
            _resourceLocator = null;
        }

        /// <inheritdoc/>
        public async override UniTask Initialize()
        {
            _resourceLocator = await Addressables.InitializeAsync();

            await base.Initialize();
        }

        public bool IsAssetKeyValid(string assetKey)
        {
            foreach (var resourceLocator in Addressables.ResourceLocators)
            {
                if (resourceLocator.Keys.Contains(assetKey)) return true;
            }

            return false;
        }

        public async UniTask<string[]> GetAssetKeysWithLabel(string label)
        {
            var assetKeys = new HashSet<string>();

            var handle = Addressables.LoadResourceLocationsAsync(label);

            var handleResults = await handle;

            foreach (var resourceLocation in handleResults)
            {
                assetKeys.Add(resourceLocation.PrimaryKey);
            }

            Addressables.Release(handle);

            return assetKeys.ToArray();
        }
    }
}
