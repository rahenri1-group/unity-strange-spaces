﻿using Game.Core.Resource;
using System;
using System.Collections.Generic;

namespace Game.Resource.Addressable
{
    public partial class AddressableMetadataManager : BaseAddressableResourceManager
    {
        private class ResouceMetadataCache : IResouceMetadata
        {
            public IResouceMetadata Metadata { get; private set; }

            public string ResourceKey => Metadata.ResourceKey;

            private Dictionary<Type, IResourceMetadataComponent> _metadataComponentCache;

            public ResouceMetadataCache(IResouceMetadata metadata)
            {
                Metadata = metadata;

                _metadataComponentCache = new Dictionary<Type, IResourceMetadataComponent>();
            }

            T IResouceMetadata.GetMetadataComponent<T>()
            {
                return GetCachedMetadataComponent<T>();
            }

            public T GetCachedMetadataComponent<T>() where T : class, IResourceMetadataComponent
            {
                Type componentType = typeof(T);

                if (!_metadataComponentCache.ContainsKey(componentType))
                {
                    _metadataComponentCache[componentType] = Metadata.GetMetadataComponent<T>();
                }

                return (T)_metadataComponentCache[componentType];
            }
        }
    }
}
