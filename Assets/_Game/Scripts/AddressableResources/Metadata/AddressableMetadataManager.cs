﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;

namespace Game.Resource.Addressable
{
    /// <inheritdoc cref="IResourceMetadataManager"/>
    [Dependency(
        contract: typeof(IResourceMetadataManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class AddressableMetadataManager : BaseAddressableResourceManager, IResourceMetadataManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Resource Metadata Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public AddressableMetadataManagerConfig Config = new AddressableMetadataManagerConfig();

        private Dictionary<string, ResouceMetadataCache> _metadataMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public AddressableMetadataManager(ILogRouter logger)
            : base(logger) 
        {
            _metadataMap = new Dictionary<string, ResouceMetadataCache>();
        }

        /// <inheritdoc/>
        public override async UniTask Initialize()
        {
            await base.Initialize();

            var metadataAddresses = await GetAssetKeysWithLabel(Config.MetadataAssetLabel);
            foreach (var address in metadataAddresses)
            {
                IResouceMetadata metadataAsset = null;

                try
                {
                    metadataAsset = await Addressables.LoadAssetAsync<IResouceMetadata>(address).ToUniTask();
                }
                catch (Exception e)
                {
                    Logger.LogError($"Could not load metadata '{address}'");
                    Logger.LogException(e);
                    metadataAsset = null;
                }

                if (metadataAsset != null)
                {
                    _metadataMap.Add(metadataAsset.ResourceKey, new ResouceMetadataCache(metadataAsset));
                }
            }
        }

        /// <inheritdoc/>
        public override async UniTask Shutdown()
        {
            foreach (var pair in _metadataMap)
            {
                Addressables.Release(pair.Value.Metadata);
            }

            _metadataMap.Clear();

            await base.Shutdown();
        }

        /// <inheritdoc/>
        public bool DoesResourceHaveMetadata(string assetKey)
        {
            return _metadataMap.ContainsKey(assetKey);
        }

        /// <inheritdoc/>
        public IResouceMetadata GetResourceMetadata(string assetKey)
        {
            if (DoesResourceHaveMetadata(assetKey))
            {
                return _metadataMap[assetKey];
            }

            return null;
        }

        /// <inheritdoc/>
        public (IResouceMetadata, T)[] FindResourceMetadataOfType<T>() where T : class, IResourceMetadataComponent
        {
            var metadataList = new List<(IResouceMetadata, T)>();

            foreach (var pair in _metadataMap)
            {
                var cache = pair.Value;

                var metadataComponent = cache.GetCachedMetadataComponent<T>();
                if (metadataComponent != null)
                {
                    metadataList.Add(((cache, metadataComponent)));
                }
            }

            return metadataList.ToArray();
        }
    }
}
