﻿using Game.Core;

namespace Game.Resource.Addressable
{
    /// <summary>
    /// Config for <see cref="AddressableMetadataManager"/>
    /// </summary>
    public class AddressableMetadataManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The asset label for metadata
        /// </summary>
        public string MetadataAssetLabel = string.Empty;
    }
}
