﻿using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Attribute to indicate that a value should come from the <see cref="IBehaviorTree"/> blackboard.
    /// This should only be used in classes derived from <see cref="INode"/>.
    /// </summary>
    public class BlackboardVariableAttribute : PropertyAttribute {}
}
