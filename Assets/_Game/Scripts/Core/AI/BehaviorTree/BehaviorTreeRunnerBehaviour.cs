﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="IBehaviorTreeRunner"/>
    public class BehaviorTreeRunnerBehaviour : InjectedBehaviour, IBehaviorTreeRunner
    {
        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public IBehaviorTree BehaviorTree { get; private set; }

        public BehaviorTreeDefinitionObject BehaviorTreeDefinition => _behaviorTreeDefinition;

        [SerializeField] private BehaviorTreeDefinitionObject _behaviorTreeDefinition = null;

        [Inject] private IBehaviorTreeFactory _behaviorTreeFactory = null;
        [Inject] private ILogRouter _logger = null;

        private CancellationTokenSource _runnerTokenSource;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            if (_behaviorTreeDefinition != null)
            {
                BehaviorTree = _behaviorTreeFactory.CreateTreeForRunner(_behaviorTreeDefinition, this);
            }
            else
            {
                _logger.LogWarning($"Runner {name} does not have a tree definition");
                BehaviorTree = null;
            }

            _runnerTokenSource = null;
        }

        private void Start()
        {
            _runnerTokenSource = new CancellationTokenSource();

            if (BehaviorTree != null)
            {
                RunTree(_runnerTokenSource.Token).Forget();
            }
        }

        private void OnDestroy()
        {
            if (BehaviorTree != null)
            {
                BehaviorTree.Teardown();
                BehaviorTree = null;
            }

            if (_runnerTokenSource != null)
            {
                _runnerTokenSource.CancelAndDispose();

                _runnerTokenSource = null;
            }
        }

        private async UniTask RunTree(CancellationToken cancellationToken)
        {
            try
            {
                await UniTask.DelayFrame(1, cancellationToken: cancellationToken);
                await BehaviorTree.ExecuteTree(cancellationToken);
            }
            catch (OperationCanceledException) { }
        }
    }
}
