﻿using System;
using System.Collections.Generic;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Blackboard implementation of <see cref="IVariable"/>
    /// </summary>
    public abstract class BlackboardVariable : IVariable
    {
        /// <inheritdoc/>
        public Guid Id { get; private set; }

        /// <inheritdoc/>
        public string Name { get; private set; }

        /// <inheritdoc/>
        public Type VariableType { get; private set; }

        public bool IsExposed { get; private set; }

        /// <inheritdoc/>
        public abstract string StringValue { get; }

        /// <inheritdoc/>
        public event Action VariableChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        public BlackboardVariable(Guid id, string name, Type variableType, bool isExposed)
        {
            Id = id;
            Name = name;
            VariableType = variableType;
            IsExposed = isExposed;
        }

        protected void InvokeVariableChanged()
        {
            VariableChanged?.Invoke();
        }
    }

    /// <inheritdoc/>
    public class BlackboardVariable<T> : BlackboardVariable, IVariable<T>
    {
        /// <inheritdoc/>
        public T Value 
        {
            get => _value;
            set
            {
                _value = value;

                if (EqualityComparer<T>.Default.Equals(_value, _valueCopy) == false)
                {
                    InvokeVariableChanged();
                }

                _valueCopy = _value;
            }
        }

        /// <inheritdoc/>
        public override string StringValue => Value?.ToString() ?? string.Empty;

        private T _value;
        private T _valueCopy;

        /// <summary>
        /// Constructor
        /// </summary>
        public BlackboardVariable(Guid id, string name, T value, bool isExposed)
            : base(id, name, typeof(T), isExposed)
        {
            _value = value;
            _valueCopy = value;
        }
    }
}
