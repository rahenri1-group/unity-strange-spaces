﻿using Game.Core.Entity;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Script that provides values from a <see cref="IEntityMovement"/> to the blackboard of a <see cref="IBehaviorTree"/>.
    /// </summary>
    [RequireComponent(typeof(IEntityMovement))]
    public class EntityMovementBlackboardProviderBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IBehaviorTreeRunner))] private Component _runnerObj = null;

        [SerializeField] private StringReadonlyReference _isGroundedBlackboard = null;

        private IBehaviorTreeRunner _runner;
        private IEntityMovement _entityMovement;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_runnerObj);

            _runner = _runnerObj.GetComponentAsserted<IBehaviorTreeRunner>();
            _entityMovement = this.GetComponentAsserted<IEntityMovement>();
        }

        /// <inheritdoc/>
        private void Update()
        {
            if (_runner.BehaviorTree == null) return;

            if (!string.IsNullOrEmpty(_isGroundedBlackboard))
            {
                var groundedVariable = _runner.BehaviorTree.GetVariable<bool>(_isGroundedBlackboard);
                if (groundedVariable != null)
                {
                    groundedVariable.Value = _entityMovement.IsGrounded;
                }
            }   
        }
    }
}
