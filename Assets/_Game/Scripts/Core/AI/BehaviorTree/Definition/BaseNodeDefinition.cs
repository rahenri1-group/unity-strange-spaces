﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    [Serializable]
    public abstract class BaseNodeDefinition
    {
        /// <summary>
        /// The unique id for the node
        /// </summary>
        public Guid Id
        {
            get => Guid.Parse(_id);
#if UNITY_EDITOR
            set => _id = value.ToString();
#endif
        }

        /// <summary>
        /// The name of the node
        /// </summary>
        public string Name
        {
            get => _name;
#if UNITY_EDITOR
            set => _name = value;
#endif
        }

        /// <summary>
        /// The priority of the node
        /// </summary>
        public int Priority
        {
            get => _priority;
#if UNITY_EDITOR
            set => _priority = value;
#endif
        }

        /// <summary>
        /// The editor position of the decision node
        /// </summary>
        public Vector2 NodePosition
        {
            get => _nodePosition;
#if UNITY_EDITOR
            set => _nodePosition = value;
#endif
        }

        /// <summary>
        /// The full type name of the node
        /// </summary>
        public string TypeName
        {
            get => _typeName;
#if UNITY_EDITOR
            set => _typeName = value;
#endif
        }

        /// <summary>
        /// The serialized data for the node
        /// </summary>
        public Dictionary<string, string> SerializedFields
        {
            get
            {
                var map = new Dictionary<string, string>();
                for (int i = 0; i < _serializedFieldNames.Length; i++)
                {
                    map.Add(_serializedFieldNames[i], _serializedFieldValues[i]);
                }

                return map;
            }
#if UNITY_EDITOR
            set
            {
                var serializedFieldNames = new List<string>();
                var serializedFieldValues = new List<string>();
                var pairs = value.OrderBy(p => p.Key).ToArray();

                foreach (var pair in pairs)
                {
                    serializedFieldNames.Add(pair.Key);
                    serializedFieldValues.Add(pair.Value);
                }
                _serializedFieldNames = serializedFieldNames.ToArray();
                _serializedFieldValues = serializedFieldValues.ToArray();
            }
#endif
        }

        /// <summary>
        /// The blackboard variables for the node
        /// </summary>
        public Dictionary<string, Guid> BlackboardVariables
        {
            get
            {
                var map = new Dictionary<string, Guid>();
                for (int i = 0; i < _blackboardFieldNames.Length; i++)
                {
                    map.Add(_blackboardFieldNames[i], Guid.Parse(_blackboardFieldVariableIds[i]));
                }

                return map;
            }
#if UNITY_EDITOR
            set
            {
                var blackboardFieldNames = new List<string>();
                var blackboardFieldVariableIds = new List<string>();
                var pairs = value.OrderBy(p => p.Key).ToArray();

                foreach (var pair in pairs)
                {
                    blackboardFieldNames.Add(pair.Key);
                    blackboardFieldVariableIds.Add(pair.Value.ToString());
                }
                _blackboardFieldNames = blackboardFieldNames.ToArray();
                _blackboardFieldVariableIds = blackboardFieldVariableIds.ToArray();
            }
#endif
        }

        [SerializeField] private string _id = string.Empty;
        [SerializeField] private string _name = string.Empty;
        [SerializeField] private int _priority = 0;
        [SerializeField] private Vector2 _nodePosition = new Vector2();

        [SerializeField] private string _typeName = null;

        [SerializeField] private string[] _serializedFieldNames = new string[0];
        [SerializeField] private string[] _serializedFieldValues = new string[0];

        [SerializeField] private string[] _blackboardFieldNames = new string[0];
        [SerializeField] private string[] _blackboardFieldVariableIds = new string[0];
    }
}
