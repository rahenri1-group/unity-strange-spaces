﻿using System;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    [CreateAssetMenu(menuName = "Game/AI/Behavior Tree")]
    public class BehaviorTreeDefinitionObject : ScriptableObject
    {
        /// <summary>
        /// The editor position of the entry node
        /// </summary>
        public Vector2 EntryNodePosition
        {
            get => _entryNodePosition;
#if UNITY_EDITOR
            set => _entryNodePosition = value;
#endif
        }

        /// <summary>
        /// The id of entry node for the finite state machine
        /// </summary>
        public Guid EntryNodeId
        {
            get => (string.IsNullOrEmpty(_entryNodeId)) ? Guid.Empty : Guid.Parse(_entryNodeId);
#if UNITY_EDITOR
            set => _entryNodeId = value.ToString();
#endif
        }

        /// <summary>
        /// The leaf nodes
        /// </summary>
        public LeafNodeDefinition[] LeafNodes
        {
            get => _leafNodes;
#if UNITY_EDITOR
            set => _leafNodes = value;
#endif
        }

        /// <summary>
        /// The decorator nodes
        /// </summary>
        public DecoratorNodeDefinition[] DecoratorNodes
        {
            get => _decoratorNodes;
#if UNITY_EDITOR
            set => _decoratorNodes = value;
#endif
        }

        /// <summary>
        /// The composite nodes
        /// </summary>
        public CompositeNodeDefinition[] CompositeNodes
        {
            get => _compositeNodes;
#if UNITY_EDITOR
            set => _compositeNodes = value;
#endif
        }

        /// <summary>
        /// The blackboard entries
        /// </summary>
        public BlackboardEntryDefinition[] BlackboardEntries
        {
            get => _blackboardEntries;
#if UNITY_EDITOR
            set => _blackboardEntries = value;
#endif
        }

        [HideInInspector]
        [SerializeField] private Vector2 _entryNodePosition = new Vector2();
        [HideInInspector]
        [SerializeField] private string _entryNodeId = string.Empty;
        [HideInInspector]
        [SerializeField] private LeafNodeDefinition[] _leafNodes = new LeafNodeDefinition[0];
        [HideInInspector]
        [SerializeField] private DecoratorNodeDefinition[] _decoratorNodes = new DecoratorNodeDefinition[0];
        [HideInInspector]
        [SerializeField] private CompositeNodeDefinition[] _compositeNodes = new CompositeNodeDefinition[0];
        [HideInInspector]
        [SerializeField] private BlackboardEntryDefinition[] _blackboardEntries = new BlackboardEntryDefinition[0];
    }
}
