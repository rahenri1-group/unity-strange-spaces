﻿using System;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// The variable types a blackboard entry can be
    /// </summary>
    public enum BlackboardType
    {
        String = 0,
        Bool = 1,
        Integer = 2,
        Float = 3,
        Position = 4,
        Entity = 5
    }

    [Serializable]
    public class BlackboardEntryDefinition
    {
        /// <summary>
        /// The unique id for the blackboard entry
        /// </summary>
        public Guid Id
        {
            get => Guid.Parse(_id);
#if UNITY_EDITOR
            set => _id = value.ToString();
#endif
        }

        /// <summary>
        /// The name of the blackboard entry
        /// </summary>
        public string Name
        {
            get => _name;
#if UNITY_EDITOR
            set => _name = value;
#endif
        }

        /// <summary>
        /// The type of the blackboard entry
        /// </summary>
        public BlackboardType BlackboardType
        {
            get => _blackboardType;
#if UNITY_EDITOR
            set => _blackboardType = value;
#endif
        }

        /// <summary>
        /// Is the blackboard entry exposed for use outside of the behavior tree
        /// </summary>
        public bool Exposed
        {
            get => _exposed;
#if UNITY_EDITOR
            set => _exposed = value;
#endif
        }

        [SerializeField] private string _id = string.Empty;
        [SerializeField] private string _name = string.Empty;
        [SerializeField] private BlackboardType _blackboardType = BlackboardType.String;
        [SerializeField] private bool _exposed = false;
    }
}
