﻿using System;
using System.Linq;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A behavior tree node with multiple children
    /// </summary>
    [Serializable]
    public class CompositeNodeDefinition : BaseNodeDefinition
    {
        /// <summary>
        /// The id of the child node of the decorator
        /// </summary>
        public Guid[] ChildNodeIds
        {
            get => Array.ConvertAll(_childNodeIds, s => Guid.Parse(s));
#if UNITY_EDITOR
            set => _childNodeIds = Array.ConvertAll(value, g => g.ToString());
#endif
        }

        [SerializeField] private string[] _childNodeIds = new string[0];
    }
}
