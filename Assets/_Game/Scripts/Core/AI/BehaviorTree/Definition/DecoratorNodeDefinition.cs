﻿using System;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A behavior tree node with a single child
    /// </summary>
    [Serializable]
    public class DecoratorNodeDefinition : BaseNodeDefinition
    {
        /// <summary>
        /// The id of the child node of the decorator
        /// </summary>
        public Guid ChildNodeId
        {
            get => Guid.Parse(_childNodeId);
#if UNITY_EDITOR
            set => _childNodeId = value.ToString();
#endif
        }

        [SerializeField] private string _childNodeId = string.Empty;
    }
}
