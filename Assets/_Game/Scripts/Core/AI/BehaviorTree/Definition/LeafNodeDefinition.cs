﻿using System;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A behavior tree node with no children
    /// </summary>
    [Serializable]
    public class LeafNodeDefinition : BaseNodeDefinition { }
}
