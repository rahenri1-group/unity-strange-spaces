﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Reflection;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="IBehaviorTreeFactory"/>
    [Dependency(
        contract: typeof(IBehaviorTreeFactory),
        lifetime: Lifetime.Singleton)]
    public class BehaviorTreeFactory : IBehaviorTreeFactory
    {
        private Dictionary<string, Type> _typeCache;

        private readonly ILogRouter _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        public BehaviorTreeFactory(ILogRouter logger)
        {
            _logger = logger;

            _typeCache = new Dictionary<string, Type>();
        }

        /// <inheritdoc/>
        public IBehaviorTree CreateTreeForRunner(BehaviorTreeDefinitionObject definition, IBehaviorTreeRunner runner)
        {
            var behaviorTree = new BehaviorTree(runner);

            var blackboard = new List<BlackboardVariable>();
            foreach (var blackboardEntry in definition.BlackboardEntries)
            {
                var blackboardVariable = CreateBlackboardVariable(blackboardEntry);
                if (blackboardVariable != null)
                {
                    blackboard.Add(blackboardVariable);
                }
            }

            var blackboardArray = blackboard.ToArray();

            var nodes = new Dictionary<Guid, INode>();
            var decoratorNodes = new Dictionary<Guid, IDecoratorNode>();
            var compositeNodes = new Dictionary<Guid, ICompositeNode>();

            // create modes
            foreach (var leafDefinition in definition.LeafNodes)
            {
                var leafNode = CreateNodeForTree<ILeafNode>(behaviorTree, leafDefinition, blackboardArray);
                nodes.Add(leafNode.Id, leafNode);
            }

            foreach (var decoratorDefinition in definition.DecoratorNodes)
            {
                var decoratorNode = CreateNodeForTree<IDecoratorNode>(behaviorTree, decoratorDefinition, blackboardArray);
                decoratorNodes.Add(decoratorNode.Id, decoratorNode);
                nodes.Add(decoratorNode.Id, decoratorNode);
            }

            foreach (var compositeDefinition in definition.CompositeNodes)
            {
                var compositeNode = CreateNodeForTree<ICompositeNode>(behaviorTree, compositeDefinition, blackboardArray);
                compositeNodes.Add(compositeNode.Id, compositeNode);
                nodes.Add(compositeNode.Id, compositeNode);
            }

            // link nodes
            foreach (var decoratorDefinition in definition.DecoratorNodes)
            {
                var node = decoratorNodes[decoratorDefinition.Id];
                var childNode = nodes[decoratorDefinition.ChildNodeId];
                childNode.AssignParent(node);
                node.AssignChildNode(childNode);
            }

            foreach (var compositeDefinition in definition.CompositeNodes)
            {
                var node = compositeNodes[compositeDefinition.Id];
                var childNodes = new List<INode>();
                foreach (var childNodeId in compositeDefinition.ChildNodeIds)
                {
                    var childNode = nodes[childNodeId];
                    childNode.AssignParent(node);
                    childNodes.Add(childNode);
                }
                node.AssignChildNodes(childNodes.ToArray());
            }

            behaviorTree.Initialize(nodes[definition.EntryNodeId], nodes.Values.ToArray(), blackboard.ToArray());

            return behaviorTree;
        }

        private BlackboardVariable CreateBlackboardVariable(BlackboardEntryDefinition blackboardEntry)
        {
            switch (blackboardEntry.BlackboardType)
            {
                case BlackboardType.String:
                    return new BlackboardVariable<string>(blackboardEntry.Id, blackboardEntry.Name, string.Empty, blackboardEntry.Exposed);

                case BlackboardType.Bool:
                    return new BlackboardVariable<bool>(blackboardEntry.Id, blackboardEntry.Name, false, blackboardEntry.Exposed);

                case BlackboardType.Integer:
                    return new BlackboardVariable<int>(blackboardEntry.Id, blackboardEntry.Name, 0, blackboardEntry.Exposed);

                case BlackboardType.Float:
                    return new BlackboardVariable<float>(blackboardEntry.Id, blackboardEntry.Name, 0f, blackboardEntry.Exposed);

                case BlackboardType.Position:
                    return new BlackboardVariable<SpacePosition>(blackboardEntry.Id, blackboardEntry.Name, SpacePosition.Empty, blackboardEntry.Exposed);

                case BlackboardType.Entity:
                    return new BlackboardVariable<IEntity>(blackboardEntry.Id, blackboardEntry.Name, null, blackboardEntry.Exposed);

                default:
                    _logger.LogError($"Unsupported variable type {blackboardEntry.BlackboardType} for blackboard variable {blackboardEntry.Name}:{blackboardEntry.Id}");
                    return null;
            }
        }

        private T CreateNodeForTree<T>(BehaviorTree behaviorTree, BaseNodeDefinition definition, BlackboardVariable[] blackboard) where T : INode
        {
            var nodeType = GetTypeFromName(definition.TypeName, typeof(T));
            var node = (T)Activator.CreateInstance(nodeType);

            FillBlackboardFields(blackboard, nodeType, node, definition.BlackboardVariables);

            FillSerializedFields(nodeType, node, definition.SerializedFields);

            ModuleContext.DiContainer.Inject(node);

            node.Initialize(behaviorTree, definition.Id, definition.Name, definition.Priority);

            return node;
        }

        private Type GetTypeFromName(string typeName, Type parentType)
        {
            if (!_typeCache.ContainsKey(typeName))
            {
                _typeCache[typeName] = TypeUtil.GetTypeByName(typeName, parentType);
            }

            return _typeCache[typeName];
        }

        private void FillBlackboardFields(BlackboardVariable[] blackboard, Type type, object obj, Dictionary<string, Guid> blackboardFields)
        {
            var members = type.GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<BlackboardVariableAttribute>().Any())
                .ToArray();

            foreach (var field in members.OfType<FieldInfo>())
            {
                if (blackboardFields.ContainsKey(field.Name))
                {
                    var variableId = blackboardFields[field.Name];

                    object value = blackboard.FirstOrDefault(b => b.Id == variableId);
                    if (value != null)
                    {
                        field.SetValue(obj, value);
                    }
                }
            }
        }

        private void FillSerializedFields(Type type, object obj, Dictionary<string, string> serializedFields)
        {
            var members = type.GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<SerializeField>().Any())
                .ToArray();

            foreach (var field in members.OfType<FieldInfo>())
            {
                if (serializedFields.ContainsKey(field.Name))
                {
                    string strValue = serializedFields[field.Name];

                    object value = strValue;
                    if (field.FieldType == typeof(int))
                    {
                        value = int.Parse(strValue);
                    }
                    else if (field.FieldType == typeof(float))
                    {
                        value = float.Parse(strValue);
                    }
                    else if (field.FieldType == typeof(bool))
                    {
                        value = bool.Parse(strValue);
                    }
                    else if (field.FieldType.IsEnum)
                    {
                        value = int.Parse(strValue);
                    }

                    field.SetValue(obj, value);
                }
            }
        }
    }
}
