﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    ///  Factory for a <see cref="IBehaviorTree"/>
    /// </summary>
    public interface IBehaviorTreeFactory
    {
        /// <summary>
        /// Creates a new behavior tree based on the provided <paramref name="definition"/> for the provided <paramref name="runner"/>
        /// </summary>
        IBehaviorTree CreateTreeForRunner(BehaviorTreeDefinitionObject definition, IBehaviorTreeRunner runner);
    }
}
