﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A GameObject that runs a behavior tree.
    /// </summary>
    public interface IBehaviorTreeRunner : IGameObjectComponent 
    {
        /// <summary>
        /// The tree the runner is currently using
        /// </summary>
        IBehaviorTree BehaviorTree { get; }
    }
}
