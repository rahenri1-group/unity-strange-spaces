﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="INode"/>
    public abstract class BaseNode : INode
    {
        /// <inheritdoc/>
        public Guid Id { get; private set; }

        /// <inheritdoc/>
        public string Name { get; private set; }

        /// <inheritdoc/>
        public string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(Name))
                {
                    return $"{Name} - {GetType().Name}";
                }

                return GetType().Name;
            }
        }

        /// <inheritdoc/>
        public int Priority { get; private set; }

        /// <inheritdoc/>
        public bool HasExecuted { get; private set; }

        /// <inheritdoc/>
        public IBehaviorTree Owner { get; private set; }

        /// <inheritdoc/>
        public INode Parent { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseNode()
        {
            Id = Guid.Empty;
            Name = string.Empty;
            Owner = null;
            Parent = null;
        }

        /// <inheritdoc/>
        public void Initialize(IBehaviorTree owner, Guid id, string name, int priority)
        {
            Owner = owner;
            Id = id;
            Name = name;
            Priority = priority;

            OnInitialize();
        }

        public void Teardown()
        {
            OnTeardown();
        }

        protected virtual void OnInitialize() { }

        protected virtual void OnTeardown() { }

        /// <inheritdoc/>
        public void Reset()
        {
            HasExecuted = false;
        }

        /// <inheritdoc/>
        public void AssignParent(INode parent)
        {
            if (Parent != null)
            {
                throw new InvalidOperationException($"Parent has already been assigned for node {Id}");
            }

            Parent = parent;
        }

        /// <inheritdoc/>
        public async UniTask<bool> ExecuteNode(CancellationToken cancellationToken)
        {
            HasExecuted = true;

            Owner.OnNodeStart(this);

            var result = await Execute(cancellationToken);

            Owner.OnNodeFinish(this);

            return result;
        }

        protected abstract UniTask<bool> Execute(CancellationToken cancellationToken);
    }
}
