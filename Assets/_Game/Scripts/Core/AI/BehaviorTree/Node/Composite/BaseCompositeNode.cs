﻿using System;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="ICompositeNode"/>
    public abstract class BaseCompositeNode : BaseNode, ICompositeNode
    {
        /// <inheritdoc/>
        public INode[] ChildNodes { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseCompositeNode()
            : base()
        {
            ChildNodes = null;
        }

        /// <inheritdoc/>
        public void AssignChildNodes(INode[] childNodes)
        {
            if (ChildNodes != null)
            {
                throw new InvalidOperationException($"Child nodes have already been assigned for node {Id}");
            }

            ChildNodes = childNodes;
        }
    }
}
