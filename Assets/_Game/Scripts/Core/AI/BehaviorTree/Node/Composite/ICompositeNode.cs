﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A node on a behavior tree with multiple children.
    /// </summary>
    public interface ICompositeNode : INode
    {
        /// <summary>
        /// The child nodes of the composite node
        /// </summary>
        INode[] ChildNodes { get; }

        /// <summary>
        /// Assigns the child nodes. Should only be called once by <see cref="IBehaviorTreeFactory"/>
        /// </summary>
        void AssignChildNodes(INode[] childNodes);
    }
}
