﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A composite node that randomly selects a node to run. 
    /// The result of that run is then returned.
    /// </summary>
    public class RandomSelector : BaseCompositeNode, ICompositeNode
    {
        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var randomIndex = Random.Default.Range(0, ChildNodes.Length);

            return ChildNodes[randomIndex].ExecuteNode(cancellationToken);
        }
    }
}
