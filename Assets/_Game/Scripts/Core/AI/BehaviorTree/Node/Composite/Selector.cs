﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A composite node that runs through all nodes until a successful result.
    /// </summary>
    public class Selector : BaseCompositeNode, ICompositeNode
    {
        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            foreach (var node in ChildNodes)
            {
                var result = await node.ExecuteNode(cancellationToken);

                if (result)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
