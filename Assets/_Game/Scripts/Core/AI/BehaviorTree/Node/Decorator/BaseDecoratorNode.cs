﻿using System;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="IDecoratorNode"/>
    public abstract class BaseDecoratorNode : BaseNode, IDecoratorNode
    {
        /// <inheritdoc/>
        public INode ChildNode { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseDecoratorNode()
            : base()
        {
            ChildNode = null;
        }

        /// <inheritdoc/>
        public void AssignChildNode(INode child)
        {
            if (ChildNode != null)
            {
                throw new InvalidOperationException($"Child has already been assigned for node {Id}");
            }

            ChildNode = child;
        }
    }
}
