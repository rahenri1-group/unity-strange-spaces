﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Base class for blackboard based conditionals
    /// </summary>
    public abstract class BaseBlackboardConditionalNode : BaseDecoratorNode, IConditionalNode, IDecoratorNode
    {
        /// <inheritdoc/>
        public InterruptMode InterruptMode => _interruptMode;

        [SerializeField] protected InterruptMode _interruptMode = 0;

        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            if (EvaluateConditional())
            {
                return ChildNode.ExecuteNode(cancellationToken);
            }
            else
            {
                return UniTask.FromResult(false);
            }
        }

        /// <inheritdoc/>
        public abstract bool EvaluateConditional();

        /// <inheritdoc/>
        public abstract bool UsesVariable(IVariable variable);
    }
}
