﻿using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A decorator that will execute it's child if the blackboard conditional is true
    /// </summary>
    public class BlackboardBoolean : BaseBlackboardConditionalNode, IDecoratorNode
    {
        public enum BooleanComparison 
        { 
            IsTrue = 0,
            IsFalse = 1
        }

        [BlackboardVariable] private IVariable<bool> _boolean = null;

        [SerializeField] private BooleanComparison _comparison = 0;

        /// <inheritdoc/>
        public override bool EvaluateConditional()
        {
            if (_boolean != null)
            {
                if (_comparison == BooleanComparison.IsTrue)
                {
                    return _boolean.Value == true;
                }
                else if (_comparison == BooleanComparison.IsFalse)
                {
                    return _boolean.Value == false;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public override bool UsesVariable(IVariable variable)
        {
            return variable.Id == _boolean.Id;
        }
    }
}
