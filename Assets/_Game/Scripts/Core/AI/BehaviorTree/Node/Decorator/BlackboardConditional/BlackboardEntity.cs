﻿using Game.Core.Entity;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    public class BlackboardEntity : BaseBlackboardConditionalNode, IDecoratorNode
    {
        public enum EntityComparison
        {
            IsNotNull = 0,
            IsNull = 1
        }

        [BlackboardVariable] private IVariable<IEntity> _entity = null;

        [SerializeField] private EntityComparison _comparison = 0;

        /// <inheritdoc/>
        public override bool EvaluateConditional()
        {
            if (_entity != null)
            {
                if (_comparison == EntityComparison.IsNotNull)
                {
                    return _entity.Value != null;
                }
                else if (_comparison == EntityComparison.IsNull)
                {
                    return _entity.Value == null;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public override bool UsesVariable(IVariable variable)
        {
            return variable.Id == _entity.Id;
        }
    }
}
