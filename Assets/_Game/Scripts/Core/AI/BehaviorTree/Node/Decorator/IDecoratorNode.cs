﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A node for a behavior tree with only a single child.
    /// </summary>
    public interface IDecoratorNode : INode
    {
        /// <summary>
        /// The child node of the decorator
        /// </summary>
        INode ChildNode { get; }

        /// <summary>
        /// Assigns the child node. Should only be called once by <see cref="IBehaviorTreeFactory"/>
        /// </summary>
        void AssignChildNode(INode child);
    }
}
