﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A decorator that will invert the result of it's child.
    /// </summary>
    public class Invertor : BaseDecoratorNode, IDecoratorNode
    {
        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var childResult = await ChildNode.ExecuteNode(cancellationToken);
            return !childResult;
        }
    }
}
