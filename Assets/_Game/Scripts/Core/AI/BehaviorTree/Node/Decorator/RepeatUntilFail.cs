﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A decorator node that will execute it's child node until a failure result is returned.
    /// If <see cref="_repeatCount"/> is 0 or less, the node will repeat indefinitely.
    /// </summary>
    public class RepeatUntilFail : BaseDecoratorNode, IDecoratorNode
    {
        [SerializeField] private int _repeatCount = 0;

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            int loopCount = 0;
            while (loopCount < _repeatCount || _repeatCount <= 0)
            {
                var result = await ChildNode.ExecuteNode(cancellationToken);

                if (result == false)
                {
                    return false;
                }

                // only increment counter if this is a non-infinite loop
                if (_repeatCount > 0)
                {
                    loopCount += 1;
                }
            }

            return true;
        }
    }
}
