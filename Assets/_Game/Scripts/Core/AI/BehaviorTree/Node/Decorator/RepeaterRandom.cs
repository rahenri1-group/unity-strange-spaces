﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A decorator node that will execute it's child node a random number of times.
    /// </summary>
    public class RepeaterRandom : BaseDecoratorNode, IDecoratorNode
    {
        [SerializeField] private int _repeatMin = 0;
        [SerializeField] private int _repeatMax = 0;

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            int repeatCount = Random.Default.Range(_repeatMin, _repeatMax + 1);

            for (int i = 0; i < repeatCount; i++)
            {
                await ChildNode.ExecuteNode(cancellationToken);
            }

            return true;
        }
    }
}
