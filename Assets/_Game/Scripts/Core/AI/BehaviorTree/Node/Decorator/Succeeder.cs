﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A decorator that will return true regardless of what the child returns.
    /// </summary>
    public class Succeeder : BaseDecoratorNode, IDecoratorNode
    {
        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            await ChildNode.ExecuteNode(cancellationToken);
            return true;
        }
    }
}
