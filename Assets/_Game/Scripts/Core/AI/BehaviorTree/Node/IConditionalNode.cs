﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// How a conditional should interrupt the tree (if at all)
    /// </summary>
    public enum InterruptMode
    {
        /// <summary>
        /// No interrupts
        /// </summary>
        None = 0,

        /// <summary>
        /// Interrupts nodes below the conditional
        /// </summary>
        Self = 1,

        /// <summary>
        /// Interrupts nodes that are a lower priority but not parented to the conditional
        /// </summary>
        LowerPriority = 2,

        /// <summary>
        /// Interrupts nodes that are below the conditional or are lower priority 
        /// </summary>
        Both = 3
    }

    /// <summary>
    /// A node that conditionally executes and can potentially interrupt tree execution
    /// </summary>
    public interface IConditionalNode : INode
    {
        /// <summary>
        /// How the conditional should interrupt tree execution (if at all)
        /// </summary>
        InterruptMode InterruptMode { get; }

        /// <summary>
        /// Evaluates the conditional
        /// </summary>
        bool EvaluateConditional();

        /// <summary>
        /// Does the conditional use the provided variable in <see cref="EvaluateConditional"/>
        /// </summary>
        bool UsesVariable(IVariable variable);
    }
}
