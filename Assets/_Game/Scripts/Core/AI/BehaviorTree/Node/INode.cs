﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Defines a node on a behavior tree
    /// </summary>
    public interface INode
    {
        /// <summary>
        /// The id of the node.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The name of the node.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The name of the node including the type of the node.
        /// </summary>
        string DisplayName { get; }

        /// <summary>
        /// The priority of the node in the tree.
        /// Lower priority nodes will evaluate before higher ones and can potentially interrupt them.
        /// </summary>
        int Priority { get; }

        /// <summary>
        /// Has the node been executed on the current run of the tree
        /// </summary>
        bool HasExecuted { get; }

        /// <summary>
        /// The behavior tree this node is a part of.
        /// </summary>
        IBehaviorTree Owner { get; }

        /// <summary>
        /// The parent node.
        /// </summary>
        INode Parent { get; }

        /// <summary>
        /// Initializes the node. At this point, all serialized fields and injection will have been complete.
        /// However, the nodes parent or children will not yet been linked.
        /// </summary>
        void Initialize(IBehaviorTree owner, Guid id, string name, int priority);

        /// <summary>
        /// Cleans up the node, freeing any used resources
        /// </summary>
        void Teardown();

        /// <summary>
        /// Resets the node, clearing any state it may have. This should not be called while the node is currently executing.
        /// </summary>
        void Reset();

        /// <summary>
        /// Assigns the parent node. Should only be called once by <see cref="IBehaviorTreeFactory"/>
        /// </summary>
        void AssignParent(INode parent);

        /// <summary>
        /// Executes the node and appropriate child nodes. 
        /// Returns true if the node successfully executes, returns false if the node fails.
        /// </summary>
        UniTask<bool> ExecuteNode(CancellationToken cancellationToken);
    }
}
