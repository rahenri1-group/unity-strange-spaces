﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// Triggers an animation on the <see cref="IBehaviorTreeRunner"/>
    /// </summary>
    public class AnimationTrigger : BaseAnimationNode, ILeafNode
    {
        [SerializeField] private string _triggerName = "";

        private int _triggerHash;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _triggerHash = Animator.StringToHash(_triggerName);   
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            AnimationCompleteFlag = false;

            Animator.SetTrigger(_triggerHash);

            await UniTask.WaitUntil(() => AnimationCompleteFlag, cancellationToken:cancellationToken);

            return true;
        }
    }
}
