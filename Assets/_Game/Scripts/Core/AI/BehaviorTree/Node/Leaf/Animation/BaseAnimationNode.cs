﻿using Game.Core.Render;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    public abstract class BaseAnimationNode : BaseNode, ILeafNode
    {
        protected Animator Animator { get; private set; }
        protected bool AnimationCompleteFlag { get; set; }

        private IAnimationCompleteEventRouter _animationCompleteEventRouter;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Animator = Owner.Runner.GetComponentInChildren<Animator>();
            _animationCompleteEventRouter = Owner.Runner.GetComponentInChildren<IAnimationCompleteEventRouter>();

            AnimationCompleteFlag = false;

            _animationCompleteEventRouter.AnimationComplete += OnAnimationComplete;
        }

        protected override void OnTeardown()
        {
            base.OnTeardown();

            _animationCompleteEventRouter.AnimationComplete -= OnAnimationComplete;
        }

        private void OnAnimationComplete()
        {
            AnimationCompleteFlag = true;
        }
    }
}
