﻿using Cysharp.Threading.Tasks;
using Game.Core.Entity.Pathing;
using Game.Core.Space;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that moves the entity to a point defined in the blackboard
    /// </summary>
    public class EntityMoveToPoint : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<SpacePosition> _destinationPosition = null;

        [SerializeField] private float _entitySpeed = 0f;
        [SerializeField] private float _pathingUpdateDelay = 0f;
        [SerializeField] private float _maxOffsetToPosition = 0f;

        private IEntityPathingAgent _pathingAgent;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Assert.IsTrue(_entitySpeed > 0f);

            _pathingAgent = Owner.Runner.GetComponent<IEntityPathingAgent>();
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            try
            {
                while (!_pathingAgent.PathingReady)
                {
                    _pathingAgent.StartAgent();

                    await UniTask.Delay(10, cancellationToken: cancellationToken);
                }

                _pathingAgent.AgentMaxSpeed = _entitySpeed;

                var pathingStartTime = Time.time;
                if (!_pathingAgent.SetDestination(_destinationPosition.Value, _maxOffsetToPosition))
                {
                    return false;
                }

                do
                {
                    if (Time.time - pathingStartTime >= _pathingUpdateDelay && _pathingAgent.PathingReady)
                    {
                        if (!_pathingAgent.SetDestination(_destinationPosition.Value, _maxOffsetToPosition))
                        {
                            return false;
                        }

                        pathingStartTime = Time.time;
                    }
                    
                    await UniTask.Delay(10, delayTiming: PlayerLoopTiming.Update, cancellationToken: cancellationToken);

                } while (!_pathingAgent.IsAgentAtDestination);
            }
            catch (OperationCanceledException ex)
            {
                if (Owner.Runner != null)
                {
                    _pathingAgent.StopAgent();
                }

                throw ex;
            }

            return true;
        }
    }
}
