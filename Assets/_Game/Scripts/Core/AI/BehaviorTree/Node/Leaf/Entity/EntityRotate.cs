﻿using Cysharp.Threading.Tasks;
using Game.Core.Entity;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that rotates an entity
    /// </summary>
    public class EntityRotate : BaseNode, ILeafNode
    {
        [SerializeField] private float _rotateAmount = 0f;

        [SerializeField] private float _rotateDuration = 0f;

        private IEntityMovement _entityMovement;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _entityMovement = Owner.Runner.GetComponent<IEntityMovement>();
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var startRotation = _entityMovement.Rotation;
            var targetRotation = startRotation * Quaternion.AngleAxis(_rotateAmount, Vector3.up);

            var startTime = Time.time;
            while (Time.time - startTime < _rotateDuration)
            {
                var lerpQuaternion = Quaternion.Lerp(startRotation, targetRotation, (Time.time - startTime) / _rotateDuration);

                _entityMovement.RotateTo(lerpQuaternion);

                await UniTask.Yield(PlayerLoopTiming.FixedUpdate, cancellationToken);
            }

            _entityMovement.RotateTo(targetRotation);

            return true;
        }
    }
}
