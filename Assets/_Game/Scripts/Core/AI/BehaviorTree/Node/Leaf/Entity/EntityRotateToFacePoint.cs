﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Space;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that rotates to face a provided point
    /// </summary>
    public class EntityRotateToFacePoint : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<SpacePosition> _targetPosition = null;

        [SerializeField] private float _rotateDuration = 0f;
        [SerializeField] private float _minRotation = 0f;

        [Inject] private ILogRouter _logger = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IDynamicEntity _entity;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _entity = Owner.Runner.GetComponent<IDynamicEntity>();
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var entitySpace = _spaceManager.GetEntitySpace(_entity);
            if (SpaceUtil.SpaceEquals(entitySpace, _targetPosition.Value.Space))
            {
                var startRotation = _entity.EntityMovement.Rotation.eulerAngles.y;
                var targetRotation = CalculateTargetRotation();
                if (_minRotation > 0f && Mathf.Abs(Mathf.DeltaAngle(startRotation, targetRotation)) <= _minRotation)
                {
                    return true;
                }

                var startTime = Time.time;
                while (Time.time - startTime < _rotateDuration)
                {
                    targetRotation = CalculateTargetRotation();
                    
                    var lerpAngle = Mathf.LerpAngle(startRotation, targetRotation, (Time.time - startTime) / _rotateDuration);

                    _entity.EntityMovement.RotateTo(Quaternion.Euler(0f, lerpAngle, 0f));

                    await UniTask.Yield(PlayerLoopTiming.FixedUpdate, cancellationToken);
                }

                _entity.EntityMovement.RotateTo(Quaternion.Euler(0f, CalculateTargetRotation(), 0f));

                return true;
            }
            else
            {
                _logger.LogWarning("Rotating to face a point in a different space is not currently supported");

                return false;
            }
        }

        private float CalculateTargetRotation()
        {
            return Quaternion.LookRotation(_targetPosition.Value.Position - _entity.EntityMovement.Position, Vector3.up).eulerAngles.y;
        }
    }
}
