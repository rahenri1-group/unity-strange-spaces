﻿namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// The lowest level node on a behavior tree. It cannot have children.
    /// </summary>
    public interface ILeafNode : INode { }
}
