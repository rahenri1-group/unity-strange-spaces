﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that does nothing
    /// </summary>
    public class NoOperation : BaseNode, ILeafNode
    {
        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            return UniTask.FromResult(true);
        }
    }
}
