﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that assigns a value to a boolean <see cref="IVariable"/>
    /// </summary>
    public class BooleanAssign : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<bool> _variable = null;

        [SerializeField] private bool _value = false;

        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            _variable.Value = _value;

            return UniTask.FromResult(true);
        }
    }
}
