﻿using Cysharp.Threading.Tasks;
using Game.Core.Entity;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that assigns a value to an entity <see cref="IVariable"/>
    /// </summary>
    public class EntityAssign : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<IEntity> _variable = null;
        [BlackboardVariable] private IVariable<IEntity> _value = null;

        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            _variable.Value = _value?.Value;

            return UniTask.FromResult(true);
        }
    }
}
