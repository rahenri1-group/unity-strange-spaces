﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that waits a set amount of time
    /// </summary>
    public class Wait : BaseNode, ILeafNode
    {
        [SerializeField] private float _waitDuration = 0f;

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            await UniTask.Delay((int) (1000f * _waitDuration), cancellationToken: cancellationToken);

            return true;
        }
    }
}
