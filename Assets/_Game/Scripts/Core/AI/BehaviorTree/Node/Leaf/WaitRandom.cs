﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A leaf node that waits a random amount of time
    /// </summary>
    public class WaitRandom : BaseNode, ILeafNode
    {
        [SerializeField] private float _minWaitDuration = 0f;
        [SerializeField] private float _maxWaitDuration = 0f;

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var duration = Random.Default.Range(_minWaitDuration, _maxWaitDuration);

            await UniTask.Delay((int)(1000f * duration), cancellationToken: cancellationToken);

            return true;
        }
    }
}
