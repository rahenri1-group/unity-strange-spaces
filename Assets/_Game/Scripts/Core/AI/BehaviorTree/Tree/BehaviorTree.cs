﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.BehaviorTree
{
    /// <inheritdoc cref="IBehaviorTree"/>
    public class BehaviorTree : IBehaviorTree
    {
        public BlackboardVariable[] Blackboard { get; private set; }

        /// <inheritdoc/>
        public IBehaviorTreeRunner Runner { get; private set; }

        /// <inheritdoc/>
        public event Action CurrentNodeChanged;

        public INode CurrentNode { get; private set; }

        private INode _entryNode;
        private Dictionary<Guid, INode> _nodeMap;
        private IConditionalNode[] _conditionalNodes;

        private CancellationTokenSource _interruptTokenSource;

        /// <summary>
        /// Constructor
        /// </summary>
        public BehaviorTree(IBehaviorTreeRunner runner)
        {
            Runner = runner;

            CurrentNode = null;
            _nodeMap = new Dictionary<Guid, INode>();
            _interruptTokenSource = null;
        }

        public void Initialize(INode entryNode, INode[] allNodes, BlackboardVariable[] blackboardVariables)
        {
            _entryNode = entryNode;
            _nodeMap = allNodes.ToDictionary(n => n.Id);
            Blackboard = blackboardVariables;
            _conditionalNodes = _nodeMap.Values.OfType<IConditionalNode>().ToArray();

            foreach (var variable in Blackboard)
            {
                variable.VariableChanged += () =>
                {
                    OnBlackboardChanged(variable);
                };
            }
        }

        /// <inheritdoc/>
        public async UniTask ExecuteTree(CancellationToken cancellationToken)
        {
            if (_interruptTokenSource == null)
            {
                _interruptTokenSource = new CancellationTokenSource();
            }

            while (true)
            {
                using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, _interruptTokenSource.Token))
                {
                    int treeFrameStart = Time.frameCount;

                    try
                    {
                        foreach (var pair in _nodeMap)
                        {
                            var node = pair.Value;
                            node.Reset();
                        }

                        await _entryNode.ExecuteNode(linkedCts.Token);
                    }
                    catch (OperationCanceledException e)
                    {
                        // cancellation came from external
                        if (cancellationToken.IsCancellationRequested)
                        {
                            _interruptTokenSource.Dispose();
                            _interruptTokenSource = null;

                            throw e;
                        }
                    }

                    // executing the tree should always take at least one frame to prevent a potential infinite loop
                    if (treeFrameStart == Time.frameCount)
                    {
                        await UniTask.DelayFrame(1, cancellationToken: cancellationToken);
                    }
                }
            }
        }

        private void OnBlackboardChanged(BlackboardVariable variable)
        {
            if (CurrentNode == null) return;

            bool shouldInterrupt = false;

            foreach (var conditional in _conditionalNodes)
            {
                if (conditional.InterruptMode == InterruptMode.None
                    || conditional.HasExecuted == false
                    || conditional.UsesVariable(variable) == false)
                {
                    continue;
                }

                if (IsNodeInSubtree(conditional, CurrentNode))
                {
                    if (conditional.InterruptMode == InterruptMode.Self || conditional.InterruptMode == InterruptMode.Both)
                    {
                        if (conditional.EvaluateConditional() == false) // condition was true and isn't anymore, interrupt
                        {
                            shouldInterrupt = true;
                            break;
                        }
                    }
                }
                else
                {
                    if ((conditional.InterruptMode == InterruptMode.LowerPriority || conditional.InterruptMode == InterruptMode.Both)
                        && conditional.Priority < CurrentNode.Priority)
                    {
                        if (conditional.EvaluateConditional() == true) // condition is now true, interrupt so that we can potentially re-evaluate
                        {
                            shouldInterrupt = true;
                            break;
                        }
                    }
                }
            }

            if (shouldInterrupt)
            {
                _interruptTokenSource.CancelAndDispose();

                _interruptTokenSource = new CancellationTokenSource();
            }
        }

        /// <inheritdoc/>
        public void Teardown()
        {
            Runner = null;

            foreach (var pair in _nodeMap)
            {
                var node = pair.Value;
                node.Teardown();
            }
        }

        /// <inheritdoc/>
        public IVariable<T> GetVariable<T>(string variableName)
        {
            var variable = Blackboard.FirstOrDefault(b => b.Name == variableName && b.IsExposed);
            if (variable == null)
            {
                return null;
            }

            return variable as IVariable<T>;
        }

        public INode GetNode(Guid id)
        {
            if (!_nodeMap.ContainsKey(id)) return null;

            return _nodeMap[id];
        }

        /// <inheritdoc/>
        public string GetPathToCurrentNode()
        {
            string path = string.Empty;

            INode node = CurrentNode;
            while (node != null)
            {
                if (string.IsNullOrEmpty(path))
                {
                    path = node.DisplayName;
                }
                else
                {
                    path = $"{node.DisplayName}/{path}";
                }

                node = node.Parent;
            }

            return path;
        }

        /// <inheritdoc/>
        public void OnNodeStart(INode node)
        {
            CurrentNode = node;

            CurrentNodeChanged?.Invoke();
        }

        /// <inheritdoc/>
        public void OnNodeFinish(INode node)
        {
            if (CurrentNode == node)
            {
                CurrentNode = null;

                CurrentNodeChanged?.Invoke();
            }
        }

        private bool IsNodeInSubtree(INode subtree, INode node)
        {
            if (subtree == node)
            {
                return true;
            }

            if (node.Parent == null)
            {
                return false;
            }

            return IsNodeInSubtree(subtree, node.Parent);
        }
    }
}
