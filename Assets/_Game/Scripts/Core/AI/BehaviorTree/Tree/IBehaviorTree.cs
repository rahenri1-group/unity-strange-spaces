﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;

namespace Game.Core.AI.BehaviorTree
{
    /// <summary>
    /// A simple behavior tree
    /// </summary>
    public interface IBehaviorTree
    {
        /// <summary>
        /// The runner that uses this tree.
        /// </summary>
        IBehaviorTreeRunner Runner { get; }

        /// <summary>
        /// Event for when the current node of the tree changes
        /// </summary>
        event Action CurrentNodeChanged;

        /// <summary>
        /// Executes the behavior tree. 
        /// The tree will run until the topmost node returns a failure or tree execution is cancelled.
        /// </summary>
        UniTask ExecuteTree(CancellationToken cancellationToken);

        /// <summary>
        /// Tearsdown the tree, freeing any used resources
        /// </summary>
        void Teardown();

        /// <summary>
        /// Returns an exposed variable with the provided name and type.
        /// Returns null if there is not an exposed variable with the provided name and type.
        /// </summary>
        IVariable<T> GetVariable<T>(string variableName);

        /// <summary>
        /// Creates a string representation of the path to the current node
        /// </summary>
        string GetPathToCurrentNode();

        /// <summary>
        /// Called when I node starts executing
        /// </summary>
        void OnNodeStart(INode node);

        /// <summary>
        /// Called when I node finishes executing
        /// </summary>
        void OnNodeFinish(INode node);
    }
}
