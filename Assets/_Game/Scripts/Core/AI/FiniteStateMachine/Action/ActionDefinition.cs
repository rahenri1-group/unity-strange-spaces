﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Data to hold the definition for a <see cref="IAction"/>
    /// </summary>
    [Serializable]
    public class ActionDefinition
    {
        /// <summary>
        /// The full type name of the action
        /// </summary>
        public string ActionTypeName 
        {
            get => _actionTypeName;
#if UNITY_EDITOR
            set => _actionTypeName = value;
#endif
        }

        public Dictionary<string, string> SerializedFields
        {
            get
            {
                var map = new Dictionary<string, string>();
                for (int i = 0; i < _serializedFieldNames.Length; i++)
                {
                    map.Add(_serializedFieldNames[i], _serializedFieldValues[i]);
                }

                return map;
            }
#if UNITY_EDITOR
            set
            {
                var serializedFieldNames = new List<string>();
                var serializedFieldValues = new List<string>();
                var pairs = value.OrderBy(p => p.Key).ToArray();

                foreach (var pair in pairs)
                {
                    serializedFieldNames.Add(pair.Key);
                    serializedFieldValues.Add(pair.Value);
                }
                _serializedFieldNames = serializedFieldNames.ToArray();
                _serializedFieldValues = serializedFieldValues.ToArray();
            }
#endif
        }

        [SerializeField] private string _actionTypeName = null;

        [SerializeField] private string[] _serializedFieldNames = new string[0];
        [SerializeField] private string[] _serializedFieldValues = new string[0];
    }
}
