﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Base class for <see cref="IAction"/>s
    /// </summary>
    public abstract class BaseAction : IAction
    {
        protected IState State { get; private set; }

        /// <inheritdoc/>
        public virtual void Initialize(IState state)
        {
            State = state;
        }

        /// <inheritdoc/>
        public virtual void StateEnter() { }

        /// <inheritdoc/>
        public abstract UniTask StateExecute(CancellationToken cancellationToken);

        /// <inheritdoc/>
        public virtual void StateExit() { }
    }
}
