﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// An action that is taken when withing a <see cref="IState"/>
    /// </summary>
    public interface IAction 
    {
        /// <summary>
        /// Called when the action has been created and registered to a state
        /// </summary>
        void Initialize(IState state);
      
        /// <summary>
        /// Called when the state is entered.
        /// </summary>
        void StateEnter();

        /// <summary>
        /// Called for each 'tick' of the state. 
        /// This will only be cancelled due to the <see cref="IStateMachineRunner"/> being disabled / destroyed.
        /// Cancellation is not to be used for control flow between states.
        /// </summary>
        UniTask StateExecute(CancellationToken cancellationToken);

        /// <summary>
        /// Called when the state is exited
        /// </summary>
        void StateExit();
    }
}
