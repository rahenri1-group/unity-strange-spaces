﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// An action that completes without performing any actions
    /// </summary>
    public class YieldAction : BaseAction
    {
        /// <inheritdoc/>
        public override UniTask StateExecute(CancellationToken cancellationToken)
        {
            return UniTask.CompletedTask;
        }
    }
}
