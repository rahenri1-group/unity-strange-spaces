﻿using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A <see cref="IDecision"/> that evaluates to true after the FSM has been in the state past a given duration
    /// </summary>
    public class AfterDurationDecision : BaseDecision
    {
        [SerializeField] private float _duration = 0f;

        /// <inheritdoc/>
        public override bool Decide()
        {
            float timeInState = Time.time - State.StateEnterTime;
            return timeInState >= _duration;
        }
    }
}
