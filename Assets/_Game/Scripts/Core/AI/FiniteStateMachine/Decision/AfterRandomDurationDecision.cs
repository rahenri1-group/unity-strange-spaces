﻿using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A <see cref="IDecision"/> that evaluates to true after the FSM has been in the state past a random duration
    /// </summary>
    public class AfterRandomDurationDecision : BaseDecision
    {
        [SerializeField] private float _minDuration = 0f;
        [SerializeField] private float _maxDuration = 0f;

        private float _currentDuration;

        /// <inheritdoc/>
        public override void Initialize(IState state)
        {
            base.Initialize(state);

            _currentDuration = Random.Default.Range(_minDuration, _maxDuration);
        }

        /// <inheritdoc/>
        public override bool Decide()
        {
            float timeInState = Time.time - State.StateEnterTime;
            return timeInState >= _currentDuration;
        }
    }
}
