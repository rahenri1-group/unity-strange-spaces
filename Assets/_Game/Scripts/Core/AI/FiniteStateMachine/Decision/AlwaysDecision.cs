﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A <see cref="IDecision"/> that always evaluates to true
    /// </summary>
    public class AlwaysDecision : BaseDecision
    {
        /// <inheritdoc/>
        public override bool Decide()
        {
            return true;
        }
    }
}
