﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Base class for <see cref="IDecision"/>s
    /// </summary>
    public abstract class BaseDecision : IDecision
    {
        protected IState State { get; private set; }
        
        /// <inheritdoc/>
        public virtual void Initialize(IState state)
        {
            State = state;
        }

        /// <inheritdoc/>
        public abstract bool Decide();
    }
}
