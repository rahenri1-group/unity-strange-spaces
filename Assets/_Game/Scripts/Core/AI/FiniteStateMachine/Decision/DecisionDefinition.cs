﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Data to hold the definition for a <see cref="IDecision"/> from one state to another
    /// </summary>
    [Serializable]
    public class DecisionDefinition
    {
        /// <summary>
        /// The unique id for the decision
        /// </summary>
        public Guid Id
        {
            get => Guid.Parse(_id);
#if UNITY_EDITOR
            set => _id = value.ToString();
#endif
        }

        /// <summary>
        /// The full type name of the decision
        /// </summary>
        public string DecisionTypeName
        {
            get => _decisionTypeName;
#if UNITY_EDITOR
            set => _decisionTypeName = value;
#endif
        }

        /// <summary>
        /// The editor position of the decision node
        /// </summary>
        public Vector2 NodePosition
        {
            get => _nodePosition;
#if UNITY_EDITOR
            set => _nodePosition = value;
#endif
        }

        /// <summary>
        /// The id of the <see cref="StateDefinition"/> that points into the decision
        /// </summary>
        public Guid EntryStateId
        {
            get => Guid.Parse(_entryStateId);
#if UNITY_EDITOR
            set => _entryStateId = value.ToString();
#endif
        }

        /// <summary>
        /// The id of the <see cref="StateDefinition"/> that the decision points to
        /// </summary>
        public Guid ExitStateId
        {
            get => Guid.Parse(_exitStateId);
#if UNITY_EDITOR
            set => _exitStateId = value.ToString();
#endif
        }

        public Dictionary<string, string> SerializedFields
        {
            get
            {
                var map = new Dictionary<string, string>();
                for (int i = 0; i < _serializedFieldNames.Length; i++)
                {
                    map.Add(_serializedFieldNames[i], _serializedFieldValues[i]);
                }

                return map;
            }
#if UNITY_EDITOR
            set
            {
                var serializedFieldNames = new List<string>();
                var serializedFieldValues = new List<string>();
                var pairs = value.OrderBy(p => p.Key).ToArray();

                foreach (var pair in pairs)
                {
                    serializedFieldNames.Add(pair.Key);
                    serializedFieldValues.Add(pair.Value);
                }
                _serializedFieldNames = serializedFieldNames.ToArray();
                _serializedFieldValues = serializedFieldValues.ToArray();
            }
#endif
        }

        [SerializeField] private string _id = string.Empty;
        [SerializeField] private Vector2 _nodePosition = new Vector2();
        [SerializeField] private string _entryStateId = string.Empty;
        [SerializeField] private string _exitStateId = string.Empty;

        [SerializeField] private string _decisionTypeName = null;
        [SerializeField] private string[] _serializedFieldNames = new string[0];
        [SerializeField] private string[] _serializedFieldValues = new string[0];
    }
}
