﻿using Game.Core.Entity.Pathing;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A <see cref="IDecision"/> that always evaluates to true if the <see cref="IEntityPathingAgent"/> is at it's destination
    /// </summary>
    public class EntityAtPathingDestination : BaseDecision
    {
        private IEntityPathingAgent _pathingAgent = null;

        /// <inheritdoc/>
        public override void Initialize(IState state)
        {
            base.Initialize(state);

            if (_pathingAgent == null)
            {
                _pathingAgent = state.Owner.Runner.GetComponent<IEntityPathingAgent>();
            }
        }

        /// <inheritdoc/>
        public override bool Decide()
        {
            return _pathingAgent.IsAgentAtDestination;
        }
    }
}
