﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Object that decides if the conditions are right for a <see cref="ITransition"/>.
    /// </summary>
    public interface IDecision
    {
        /// <summary>
        /// Called when a state using the decision as for a <see cref="ITransition"/> has been activated.
        /// </summary>
        void Initialize(IState state);

        /// <summary>
        /// Called to evaluate if the conditions are right for a <see cref="ITransition"/> for the controller.
        /// </summary>
        bool Decide();
    }
}
