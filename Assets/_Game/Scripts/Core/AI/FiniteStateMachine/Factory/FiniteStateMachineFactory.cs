﻿using Game.Core.DependencyInjection;
using Game.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <inheritdoc cref="IFiniteStateMachineFactory"/>
    [Dependency(
        contract: typeof(IFiniteStateMachineFactory),
        lifetime: Lifetime.Singleton)]
    public class FiniteStateMachineFactory : IFiniteStateMachineFactory
    {
        private Dictionary<string, Type> _typeCache;

        /// <summary>
        /// Constructor
        /// </summary>
        public FiniteStateMachineFactory()
        {
            _typeCache = new Dictionary<string, Type>();
        }

        /// <inheritdoc />
        public IFiniteStateMachine CreateMachineForRunner(FiniteStateMachineDefinitionObject definition, IStateMachineRunner runner)
        {
            var finiteStateMachine = new FiniteStateMachine(runner);

            var injectionContainer = ModuleContext.DiContainer;

            var states = new List<State>();
            foreach (var stateDefinition in definition.StateDefinitions)
            {
                var actions = new List<IAction>();

                foreach (var actionDefinition in stateDefinition.ActionDefinitions)
                {
                    var actionType = GetTypeFromName(actionDefinition.ActionTypeName, typeof(IAction));
                    var action = (IAction)Activator.CreateInstance(actionType);

                    FillSerializedFields(actionType, action, actionDefinition.SerializedFields);
                    injectionContainer.Inject(action);

                    actions.Add(action);
                }

                states.Add(new State(stateDefinition.Id, stateDefinition.Name, finiteStateMachine, actions.ToArray()));
            }

            foreach (var decisionDefinition in definition.DecisionDefinitions)
            {
                var startStateId = decisionDefinition.EntryStateId;
                var startState = states.First(s => s.Id == startStateId);
                var endStateId = decisionDefinition.ExitStateId;
                var endState = states.First(s => s.Id == endStateId);

                var decisionType = GetTypeFromName(decisionDefinition.DecisionTypeName, typeof(IDecision));
                var decision = (IDecision)Activator.CreateInstance(decisionType);

                FillSerializedFields(decisionType, decision, decisionDefinition.SerializedFields);
                injectionContainer.Inject(decision);

                startState.RegisterTransition(new Transition(decision, endState));
            }

            var entryStateId = definition.EntryStateId;
            finiteStateMachine.EntryState = states.First(s => s.Id == entryStateId);

            return finiteStateMachine;
        }

        private Type GetTypeFromName(string typeName, Type parentType)
        {
            if (!_typeCache.ContainsKey(typeName))
            {
                _typeCache[typeName] = TypeUtil.GetTypeByName(typeName, parentType);
            }

            return _typeCache[typeName];
        }

        private void FillSerializedFields(Type type, object obj, Dictionary<string, string> serializedFields)
        {
            var members = type.GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<SerializeField>().Any())
                .ToArray();

            foreach (var field in members.OfType<FieldInfo>())
            {
                if (serializedFields.ContainsKey(field.Name))
                {
                    string strValue = serializedFields[field.Name];

                    object value = strValue;
                    if (field.FieldType == typeof(int))
                    {
                        value = int.Parse(strValue);
                    }
                    else if (field.FieldType == typeof(float))
                    {
                        value = float.Parse(strValue);
                    }
                    else if (field.FieldType == typeof(bool))
                    {
                        value = bool.Parse(strValue);
                    }

                    field.SetValue(obj, value);
                }
            }
        }
    }
}
