﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Factory for <see cref="IFiniteStateMachine"/>s
    /// </summary>
    public interface IFiniteStateMachineFactory
    {
        /// <summary>
        /// Creates a new finite state machine based on the provided <paramref name="definition"/> for the provided <paramref name="runner"/>
        /// </summary>
        IFiniteStateMachine CreateMachineForRunner(FiniteStateMachineDefinitionObject definition, IStateMachineRunner runner);
    }
}
