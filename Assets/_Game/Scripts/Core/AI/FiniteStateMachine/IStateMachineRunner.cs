﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A GameObject that runs a finite state machine.
    /// </summary>
    public interface IStateMachineRunner : IGameObjectComponent
    {
        /// <summary>
        /// The current state of the controller.
        /// </summary>
        IState CurrentState { get; }
    }
}
