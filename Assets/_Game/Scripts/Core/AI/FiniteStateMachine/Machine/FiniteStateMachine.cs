﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <inheritdoc cref="IFiniteStateMachine"/>
    public class FiniteStateMachine : IFiniteStateMachine
    {
        /// <inheritdoc />
        public IState CurrentState { get; private set; }

        /// <inheritdoc />
        public IStateMachineRunner Runner { get; private set; }


        public IState EntryState { get; set; }

        /// <summary>
        /// Constructs a finite state machine from a definition
        /// </summary>
        public FiniteStateMachine(IStateMachineRunner runner)
        {
            CurrentState = null;
            Runner = runner;

            EntryState = null;
        }

        /// <inheritdoc />
        public void Reset()
        {
            if (CurrentState != null)
            {
                CurrentState.OnStateExit();
            }
            CurrentState = null;
        }

        /// <inheritdoc />
        public async UniTask ExecuteCurrentState(CancellationToken cancellationToken)
        {
            if (CurrentState == null)
            {
                CurrentState = EntryState;
                CurrentState.OnStateEnter();
            }

            await UniTask.WhenAll(
                        UniTask.Yield(cancellationToken),
                        CurrentState.ExecuteActions(cancellationToken));

            var nextState = CurrentState.DecideNextState();
            if (nextState != CurrentState)
            {
                CurrentState.OnStateExit();
                CurrentState = nextState;
                CurrentState.OnStateEnter();
            }
        }
    }
}
