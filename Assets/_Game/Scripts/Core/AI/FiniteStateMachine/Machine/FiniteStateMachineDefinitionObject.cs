﻿using System;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    [CreateAssetMenu(menuName = "Game/AI/FSM")]
    public class FiniteStateMachineDefinitionObject : ScriptableObject
    {
        /// <summary>
        /// The editor position of the entry node
        /// </summary>
        public Vector2 EntryNodePosition
        {
            get => _entryNodePosition;
#if UNITY_EDITOR
            set => _entryNodePosition = value;
#endif
        }

        /// <summary>
        /// The id of entry <see cref="StateDefinition"/> for the finite state machine
        /// </summary>
        public Guid EntryStateId
        {
            get => (string.IsNullOrEmpty(_entryStateId)) ? Guid.Empty : Guid.Parse(_entryStateId);
#if UNITY_EDITOR
            set => _entryStateId = value.ToString();
#endif
        }

        /// <summary>
        /// All of the states in the finite state machine 
        /// </summary>
        public StateDefinition[] StateDefinitions
        {
            get => _stateDefinitions;
#if UNITY_EDITOR
            set => _stateDefinitions = value;
#endif
        }

        /// <summary>
        /// All of the decisions in the finite state machine 
        /// </summary>
        public DecisionDefinition[] DecisionDefinitions
        {
            get => _decisionDefinitions;
#if UNITY_EDITOR
            set => _decisionDefinitions = value;
#endif
        }

        [HideInInspector]
        [SerializeField]  private Vector2 _entryNodePosition = new Vector2();
        [HideInInspector]
        [SerializeField] private string _entryStateId = string.Empty;
        [HideInInspector]
        [SerializeField] private StateDefinition[] _stateDefinitions = new StateDefinition[0];
        [HideInInspector]
        [SerializeField] private DecisionDefinition[] _decisionDefinitions = new DecisionDefinition[0];
    }
}
