﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// A simple Finite State Machine
    /// </summary>
    public interface IFiniteStateMachine
    {
        /// <summary>
        /// The current state of the machine
        /// </summary>
        IState CurrentState { get; }

        /// <summary>
        /// The runner this machine is for
        /// </summary>
        IStateMachineRunner Runner { get; }

        /// <summary>
        /// Resets the finite state machine
        /// </summary>
        void Reset();

        /// <summary>
        /// Executes one 'step' in the finite state machine. Executing all actions for the current state and choosing the next state
        /// </summary>
        UniTask ExecuteCurrentState(CancellationToken cancellationToken);
    }
}
