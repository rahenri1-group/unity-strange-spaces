﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Definies a state for a <see cref="IStateMachineRunner"/>.
    /// Composed of multiple <see cref="IAction"/>s and <see cref="ITransition"/>s to other states.
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// The name of the state
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The finite state machine this state is a part of
        /// </summary>
        IFiniteStateMachine Owner { get; }

        /// <summary>
        /// The time the state was entered. 0 if not currently active
        /// </summary>
        float StateEnterTime { get; }

        /// <summary>
        /// Called controller when the state is entered
        /// </summary>
        void OnStateEnter();

        /// <summary>
        /// Called by the controller to execute all <see cref="IAction"/>s for the state
        /// </summary>
        UniTask ExecuteActions(CancellationToken cancellationToken);

        /// <summary>
        /// Called by the controller when the state is exited
        /// </summary>
        void OnStateExit();

        /// <summary>
        /// Called by the controller after all actions have completed to determine the next state.
        /// Note: The state may transition back to itself.
        /// </summary>
        IState DecideNextState();
    }
}
