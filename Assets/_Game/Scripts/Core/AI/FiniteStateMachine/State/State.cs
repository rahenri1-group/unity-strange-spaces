﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <inheritdoc cref="IState"/>
    public class State : IState
    {
        /// <summary>
        /// The Id of the state
        /// </summary>
        public Guid Id { get; private set; }

        /// <inheritdoc />
        public IFiniteStateMachine Owner { get; private set; }

        /// <inheritdoc />
        public string Name { get; private set; }

        /// <inheritdoc />
        public float StateEnterTime { get; private set; }

        private List<ITransition> _transitions;

        private IAction[] _actions;

        /// <summary>
        /// Constructor
        /// </summary>
        public State(Guid id, string name, IFiniteStateMachine owner, IAction[] actions)
        {
            Id = id;
            Name = name;

            Owner = owner;

            _actions = actions;
            foreach (var action in _actions)
            {
                action.Initialize(this);
            }

            _transitions = new List<ITransition>();

            StateEnterTime = 0f;
        }

        /// <summary>
        /// Registers a new transition from this state to another
        /// </summary>
        public void RegisterTransition(ITransition transition)
        {
            _transitions.Add(transition);
        }

        /// <inheritdoc />
        public void OnStateEnter()
        {
            StateEnterTime = Time.time;

            foreach (var action in _actions)
            {
                action.StateEnter();
            }

            foreach (var transition in _transitions)
            {
                transition.Decision.Initialize(this);
            }
        }

        /// <inheritdoc />
        public UniTask ExecuteActions(CancellationToken cancellationToken)
        {
            var tasks = new List<UniTask>();

            foreach (var action in _actions)
            {
                tasks.Add(action.StateExecute(cancellationToken));
            }

            return UniTask.WhenAll(tasks);
        }

        /// <inheritdoc />
        public IState DecideNextState()
        {
            foreach (var transition in _transitions)
            {
                if (transition.Decision.Decide())
                {
                    return transition.State;
                }
            }

            return this;
        }

        /// <inheritdoc />
        public void OnStateExit()
        {
            foreach (var action in _actions)
            {
                action.StateExit();
            }

            StateEnterTime = 0f;
        }
    }
}
