﻿using System;
using UnityEngine;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Data to hold the definition for a <see cref="IState"/>
    /// </summary>
    [Serializable]
    public class StateDefinition
    {
        /// <summary>
        /// The unique id for the state
        /// </summary>
        public Guid Id
        {
            get => Guid.Parse(_id);
#if UNITY_EDITOR
            set => _id = value.ToString();
#endif
        }

        /// <summary>
        /// The name of the state
        /// </summary>
        public string Name 
        { 
            get => _name;
#if UNITY_EDITOR
            set => _name = value;
#endif
        }

        /// <summary>
        /// The editor position of the state node
        /// </summary>
        public Vector2 NodePosition
        {
            get => _nodePosition;
#if UNITY_EDITOR
            set => _nodePosition = value;
#endif
        }

        /// <summary>
        /// The action definitions for the state
        /// </summary>
        public ActionDefinition[] ActionDefinitions
        {
            get => _actionDefinitions;
#if UNITY_EDITOR
            set => _actionDefinitions = value;
#endif
        }

        [SerializeField] private string _id = string.Empty;
        [SerializeField] private string _name = "";
        [SerializeField] private Vector2 _nodePosition = new Vector2();
        [SerializeField] private ActionDefinition[] _actionDefinitions = new ActionDefinition[0];
    }
}
