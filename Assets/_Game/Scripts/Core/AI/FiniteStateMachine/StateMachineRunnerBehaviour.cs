﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.AI.FiniteStateMachine
{
    /// <inheritdoc cref="IStateMachineRunner"/>
    public class StateMachineRunnerBehaviour : InjectedBehaviour, IStateMachineRunner
    {
        [SerializeField] [ReadOnly] private string _currentStateName;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public IState CurrentState => _finiteStateMachine.CurrentState;

        [SerializeField] private FiniteStateMachineDefinitionObject _finiteStateMachineDefinition = null;

        [Inject] private IFiniteStateMachineFactory _finiteStateMachineFactory = null;

        private IFiniteStateMachine _finiteStateMachine;

        private CancellationTokenSource _stateMachineRunTokenSource;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_finiteStateMachineDefinition);

            _finiteStateMachine = _finiteStateMachineFactory.CreateMachineForRunner(_finiteStateMachineDefinition, this);

            _stateMachineRunTokenSource = null;
        }

        private void Start()
        {
            _stateMachineRunTokenSource = new CancellationTokenSource();

            StateMachineRun(_stateMachineRunTokenSource.Token).Forget();
        }

        private void OnDestroy()
        {
            if (_stateMachineRunTokenSource != null)
            {
                _stateMachineRunTokenSource.CancelAndDispose();
                _stateMachineRunTokenSource = null;
            }
        }

        private async UniTask StateMachineRun(CancellationToken cancellationToken)
        {
            _finiteStateMachine.Reset();
            _currentStateName = string.Empty;

            try
            {
                await UniTask.DelayFrame(1, cancellationToken: cancellationToken);

                while (true)
                {
                    await _finiteStateMachine.ExecuteCurrentState(cancellationToken);

                    _currentStateName = _finiteStateMachine.CurrentState.Name;
                }
            }
            catch (OperationCanceledException) { }
        }
    }
}
