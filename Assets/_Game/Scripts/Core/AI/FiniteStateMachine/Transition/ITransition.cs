﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <summary>
    /// Defines a transition from a state to a new one.
    /// </summary>
    public interface ITransition
    {
        /// <summary>
        /// Decides if the transition should be taken.
        /// </summary>
        IDecision Decision { get; }

        /// <summary>
        /// The new state a <see cref="IStateMachineRunner"/> should enter if <see cref="IDecision.Decide(IStateMachineRunner)"/> returns true.
        /// </summary>
        IState State { get; }
    }
}
