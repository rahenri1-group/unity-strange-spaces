﻿namespace Game.Core.AI.FiniteStateMachine
{
    /// <inheritdoc cref="ITransition"/>
    public class Transition : ITransition
    {
        /// <inheritdoc />
        public IDecision Decision { get; private set; }

        /// <inheritdoc />
        public IState State { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Transition(IDecision decision, IState state)
        {
            Decision = decision;
            State = state;
        }
    }
}
