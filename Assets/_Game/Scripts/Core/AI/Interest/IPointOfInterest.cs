﻿using UnityEngine;

namespace Game.Core.AI.Interest
{
    public interface IPointOfInterest : IGameObjectComponent
    {
        Vector3 Position { get; }
    }
}
