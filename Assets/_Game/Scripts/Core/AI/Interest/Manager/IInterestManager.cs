﻿using Game.Core.Space;

namespace Game.Core.AI.Interest
{ 
    public interface IInterestManager : IModule
    {
        void RegisterPointOfInterest(IPointOfInterest pointOfInterest);

        void UnregisterPointOfInterest(IPointOfInterest pointOfInterest);

        T[] GetPointsOfInterestForSpace<T>(ISpaceData space) where T : IPointOfInterest;
    }
}
