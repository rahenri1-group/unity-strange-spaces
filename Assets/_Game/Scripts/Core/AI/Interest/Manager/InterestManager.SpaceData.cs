﻿using System.Collections.Generic;
using System.Linq;

namespace Game.Core.AI.Interest
{
    public partial class InterestManager : BaseModule
    {
        private class PointsOfInterestCollection
        {
            private HashSet<IPointOfInterest> _pointsOfInterest;

            public PointsOfInterestCollection()
            {
                _pointsOfInterest = new HashSet<IPointOfInterest>();
            }

            public void AddPointOfInterest(IPointOfInterest pointOfInterest)
            {
                if (_pointsOfInterest.Contains(pointOfInterest)) return;

                _pointsOfInterest.Add(pointOfInterest);
            }

            public void RemovePointOfInterest(IPointOfInterest pointOfInterest)
            {
                _pointsOfInterest.Remove(pointOfInterest);
            }

            public T[] GetPointsOfInterest<T>() where T : IPointOfInterest
            {
                // this may need to be cached for performance
                return _pointsOfInterest.OfType<T>().ToArray();
            }
        }
    }
}
