﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections.Generic;

namespace Game.Core.AI.Interest
{
    [Dependency(
        contract: typeof(IInterestManager),
        lifetime: Lifetime.Singleton)]
    public partial class InterestManager : BaseModule, IInterestManager
    {
        public override string ModuleName => "AI Interest Manager";

        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly ISpaceManager _spaceManager;

        private Dictionary<ISpaceData, PointsOfInterestCollection> _poiMap;

        public InterestManager(
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(logger)
        {
            _spaceManager = spaceManager;

            _poiMap = new Dictionary<ISpaceData, PointsOfInterestCollection>();
        }

        public override UniTask Shutdown()
        {
            _poiMap.Clear();

            return base.Shutdown();
        }

        public void RegisterPointOfInterest(IPointOfInterest pointOfInterest)
        {
            var space = _spaceManager.GetObjectSpace(pointOfInterest.GameObject);

            if (!_poiMap.ContainsKey(space))
            {
                _poiMap.Add(space, new PointsOfInterestCollection());
            }

            _poiMap[space].AddPointOfInterest(pointOfInterest);
        }

        public void UnregisterPointOfInterest(IPointOfInterest pointOfInterest)
        {
            var space = _spaceManager.GetObjectSpace(pointOfInterest.GameObject);

            if (_poiMap.ContainsKey(space))
            {
                _poiMap[space].RemovePointOfInterest(pointOfInterest);
            }
        }

        public T[] GetPointsOfInterestForSpace<T>(ISpaceData space) where T : IPointOfInterest
        {
            if (!_poiMap.ContainsKey(space))
                return new T[0];

            return _poiMap[space].GetPointsOfInterest<T>();
        }
    }
}
