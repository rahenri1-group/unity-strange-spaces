﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core.AI.Interest
{
    public class PatrolPointBehaviour : InjectedBehaviour, IPointOfInterest
    {
        public GameObject GameObject => gameObject;

        public Vector3 Position => transform.position;

        [Inject] private IInterestManager _interestManager = null;

        private void OnEnable()
        {
            _interestManager.RegisterPointOfInterest(this);
        }

        private void OnDisable()
        {
            _interestManager.UnregisterPointOfInterest(this);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            GizmosUtil.DrawCylinder(Position, 0.5f, 1.7f);
        }
    }
}
