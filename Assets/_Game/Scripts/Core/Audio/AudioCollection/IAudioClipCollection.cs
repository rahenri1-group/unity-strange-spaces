﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Interface to access a collection of <see cref="AudioClip"/>s
    /// </summary>
    public interface IAudioClipCollection
    {
        /// <summary>
        /// Gets the next audio <see cref="AudioClip"/> in the collection
        /// </summary>
        /// <returns></returns>
        AudioClip GetNextAudioClip();
    }
}
