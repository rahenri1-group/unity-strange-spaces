﻿using System;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Holds a collection of <see cref="AudioClip"/>s and retrieves them randomly
    /// </summary>
    [Serializable]
    public class RandomAudioClipCollection : IAudioClipCollection
    {
        [SerializeField] private AudioClip[] _audioClips;

        /// <inheritdoc cref="IAudioClipCollection.GetNextAudioClip"/>
        public AudioClip GetNextAudioClip()
        {
            int clipIndex = Random.Default.Range(0, _audioClips.Length);
            return _audioClips[clipIndex];
        }
    }
}
