﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// ScriptableObject version of <see cref="RandomAudioClipCollection"/>.
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Audio/Audio Clip Collection")]
    public class RandomAudioClipCollectionObject : ScriptableObject, IAudioClipCollection
    {
        [SerializeField] private AudioClip[] _audioClips;

        /// <inheritdoc cref="IAudioClipCollection.GetNextAudioClip"/>
        public AudioClip GetNextAudioClip()
        {
            int clipIndex = Random.Default.Range(0, _audioClips.Length);
            return _audioClips[clipIndex];
        }
    }
}
