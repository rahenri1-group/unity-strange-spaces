﻿namespace Game.Core.Audio
{
    /// <summary>
    /// The groups all audio can belong to
    /// </summary>
    public enum AudioGroupType
    {
        /// <summary>
        /// The master channel (exists for mixers, audio should not be played directly on this
        /// </summary>
        Master = 0,

        /// <summary>
        /// The music channel
        /// </summary>
        Music = 1,

        /// <summary>
        /// The effects channel
        /// </summary>
        Effects = 2
    }
}
