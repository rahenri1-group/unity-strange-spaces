﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="IAudioListener"/>
    public abstract class BaseAudioListener : InjectedBehaviour, IAudioListener
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public AudioListener UnityListener { get; private set; }

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            UnityListener = gameObject.AddComponent<AudioListener>();
        }
    }
}
