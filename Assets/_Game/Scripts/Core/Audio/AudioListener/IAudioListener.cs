﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Interface for a consumer of audio
    /// </summary>
    public interface IAudioListener : IGameObjectComponent
    {
        /// <summary>
        /// The Unity <see cref="AudioListener"/>
        /// </summary>
        AudioListener UnityListener { get; }
    }
}
