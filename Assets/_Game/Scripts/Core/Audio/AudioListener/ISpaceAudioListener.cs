﻿using Game.Core.Space;

namespace Game.Core.Audio
{
    /// <summary>
    /// An <see cref="IAudioListener"/> that limits only hears the <see cref="ISpaceAudioSource"/>s in the same space or linked by <see cref="Portal.IPortalAudioLink"/>
    /// </summary>
    public interface ISpaceAudioListener: IAudioListener
    {
        /// <summary>
        /// The space the listener is currently listening to
        /// </summary>
        ISpaceData ListeningSpace { get; }
    }
}
