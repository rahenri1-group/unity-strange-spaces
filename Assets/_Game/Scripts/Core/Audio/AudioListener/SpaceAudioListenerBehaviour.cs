﻿using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="ISpaceAudioListener" />
    public class SpaceAudioListenerBehaviour : BaseAudioListener, ISpaceAudioListener
    {
        /// <inheritdoc />
        public ISpaceData ListeningSpace => TargetCamera?.RenderedSpace;

        /// <summary>
        /// The <see cref="ISpaceCamera"/> the listener is currently following
        /// </summary>
        public ISpaceCamera TargetCamera
        {
            get => _targetCamera;
            set
            {
                var oldCamera = _targetCamera;
                _targetCamera = value;

                OnTargetCameraChanged(oldCamera, _targetCamera);
            }
        }
        private ISpaceCamera _targetCamera;

        [Inject] private IEventBus _eventBus = null;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            TargetCamera = null;
        }

        /// <inheritdoc />
        private void OnDestroy()
        {
            if (TargetCamera != null)
            {
                TargetCamera.RenderedSpaceChanged -= OnCameraChangeSpace;
                TargetCamera = null;
            }
        }

        private void OnTargetCameraChanged(ISpaceCamera oldCamera, ISpaceCamera newCamera)
        {
            if (oldCamera != null)
            {
                oldCamera.RenderedSpaceChanged -= OnCameraChangeSpace;
            }

            if (newCamera != null)
            {
                transform.SetParent(newCamera.GameObject.transform);

                newCamera.RenderedSpaceChanged += OnCameraChangeSpace;
            }
            else
            {
                transform.SetParent(null);
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            if (!SpaceUtil.SpaceEquals(oldCamera?.RenderedSpace, newCamera?.RenderedSpace))
            {
                OnCameraChangeSpace(newCamera);
            }
        }

        private void OnCameraChangeSpace(ISpaceCamera camera)
        {
            _eventBus.InvokeEvent(new AudioListenerSpaceChangeEvent
            {
                AudioListener = this,
                NewSpace = camera?.RenderedSpace
            });
        }
    }
}
