﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using UnityEngine;
using UnityEngine.Audio;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="IAudioMixer"/>
    [Dependency(
        contract: typeof(IAudioMixer),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class AudioMixer : BaseModule, IAudioMixer
    {
        /// <inheritdoc/>
        public override string ModuleName => "Audio Mixer";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public AudioMixerConfig Config = new AudioMixerConfig();

        /// <inheritdoc/>
        public float MasterVolume { get => GetVolumeOfGroup(Config.MasterVolumeParameterName); set => SetVolumeOfGroup(Config.MasterVolumeParameterName, value); }
        /// <inheritdoc/>
        public float MusicVolume { get => GetVolumeOfGroup(Config.MusicVolumeParameterName); set => SetVolumeOfGroup(Config.MusicVolumeParameterName, value); }
        /// <inheritdoc/>
        public float EffectsVolume { get => GetVolumeOfGroup(Config.EffectsVolumeParameterName); set => SetVolumeOfGroup(Config.EffectsVolumeParameterName, value); }

        private readonly IAssetResourceManager _assetResourceManager;

        private UnityEngine.Audio.AudioMixer _mainAudioMixer;

        private AudioMixerGroup _musicMixerGroup;
        private AudioMixerGroup _effectsMixerGroup;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public AudioMixer(
            IAssetResourceManager assetResourceManager,
            ILogRouter logger)
            : base(logger)
        {
            _assetResourceManager = assetResourceManager;
        }

        /// <inheritdoc/>
        public override async UniTask Initialize()
        {
            await base.Initialize();

            _mainAudioMixer = await _assetResourceManager.LoadAsset<UnityEngine.Audio.AudioMixer>(Config.AudioMixerAssetKey);

            _musicMixerGroup = _mainAudioMixer.FindMatchingGroups(Config.MusicGroupName)[0];
            _effectsMixerGroup = _mainAudioMixer.FindMatchingGroups(Config.EffectsGroupName)[0];
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _musicMixerGroup = null;
            _effectsMixerGroup = null;

            _assetResourceManager.ReleaseAsset(_mainAudioMixer);
            _mainAudioMixer = null;

            return base.Shutdown();
        }

        private float GetVolumeOfGroup(string volumeParam)
        {
            _mainAudioMixer.GetFloat(volumeParam, out float mixerValue);
            return Mathf.Pow(10f, mixerValue / 20f);
        }

        private void SetVolumeOfGroup(string volumeParam, float volume)
        {
            volume = Mathf.Clamp(volume, 0.0001f, 1f);
            _mainAudioMixer.SetFloat(volumeParam, Mathf.Log10(volume) * 20f);
        }

        /// <inheritdoc/>
        public void ConfigureAudioSource(IAudioSource audioSource)
        {
            if (audioSource.AudioType == AudioGroupType.Master)
            {
                ModuleLogError($"AudioSource '{audioSource.GameObject.name}' marked as master type");
                return;
            }

            switch (audioSource.AudioType)
            {
                case AudioGroupType.Music:
                    audioSource.UnityAudio.outputAudioMixerGroup = _musicMixerGroup;
                    break;
                case AudioGroupType.Effects:
                    audioSource.UnityAudio.outputAudioMixerGroup = _effectsMixerGroup;
                    break;
                default:
                    ModuleLogError($"Unknown audio group '{audioSource.AudioType}'");
                    break;
            }
        }
    }
}
