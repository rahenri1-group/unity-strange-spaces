﻿namespace Game.Core.Audio
{
    /// <summary>
    /// The config for <see cref="AudioMixer"/>
    /// </summary>
    public class AudioMixerConfig : ModuleConfig
    {
        /// <summary>
        /// The asset key of the audio mixer
        /// </summary>
        public string AudioMixerAssetKey = string.Empty;

        /// <summary>
        /// The name of the group for <see cref="AudioGroupType.Music"/>
        /// </summary>
        public string MusicGroupName = string.Empty;
        /// <summary>
        /// The name of the group for <see cref="AudioGroupType.Effects"/>
        /// </summary>
        public string EffectsGroupName = string.Empty;

        /// <summary>
        /// The parameter name for <see cref="IAudioMixer.MasterVolume"/>
        /// </summary>
        public string MasterVolumeParameterName = string.Empty;
        /// <summary>
        /// The parameter name for <see cref="IAudioMixer.MusicVolume"/>
        /// </summary>
        public string MusicVolumeParameterName = string.Empty;
        /// <summary>
        /// The parameter name for <see cref="IAudioMixer.EffectsVolume"/>
        /// </summary>
        public string EffectsVolumeParameterName = string.Empty;
    }
}
