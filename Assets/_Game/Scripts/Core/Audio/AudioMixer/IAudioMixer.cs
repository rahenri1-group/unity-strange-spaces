﻿namespace Game.Core.Audio
{
    /// <summary>
    /// Controls the volumes and assignments of <see cref="IAudioMixer"/>s
    /// </summary>
    public interface IAudioMixer : IModule
    {

        /// <summary>
        /// The volume of <see cref="AudioGroupType.Master"/>, 0 for muted to 1 for max volume.
        /// </summary>
        float MasterVolume { get; set; }

        /// <summary>
        /// The volume of <see cref="AudioGroupType.Music"/>, 0 for muted to 1 for max volume.
        /// </summary>
        float MusicVolume { get; set; }

        /// <summary>
        /// The volume of <see cref="AudioGroupType.Effects"/>, 0 for muted to 1 for max volume.
        /// </summary>
        float EffectsVolume { get; set; }

        /// <summary>
        /// Configures an <see cref="IAudioSource"/> and assigns it to a mixer
        /// </summary>
        void ConfigureAudioSource(IAudioSource audioSource);
    }
}
