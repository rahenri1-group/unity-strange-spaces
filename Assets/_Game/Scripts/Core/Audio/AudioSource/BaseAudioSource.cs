﻿using Game.Core.DependencyInjection;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="IAudioSource"/>
    public abstract class BaseAudioSource : InjectedBehaviour, IAudioSource
    {
        /// <inheritdoc/>
        public event Action<IAudioSource> PlayClipPreStart;
        /// <inheritdoc/>
        public event Action<IAudioSource> PlayClipPostStart;
        /// <inheritdoc/>
        public event Action<IAudioSource> PlayClipStopped;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public abstract AudioSource UnityAudio { get; }

        /// <inheritdoc/>
        public AudioGroupType AudioType => _audioType;
        [SerializeField] private AudioGroupType _audioType = 0;

        /// <inheritdoc/>
        public bool Is3dAudio => UnityAudio.spatialBlend > 0f;

        /// <inheritdoc/>
        public bool IsPlaying { get; private set; } = false;

        /// <inheritdoc/>
        public float MaxDistance => UnityAudio.maxDistance;

        /// <inheritdoc/>
        public float CurrentClipPlayBackTime => UnityAudio.time;

        [Inject] protected IAudioMixer AudioMixer;
        [Inject] protected ILogRouter Logger;

        private IEnumerator _playEndCheckerCoroutine = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(UnityAudio);

            if (AudioType == AudioGroupType.Master)
            {
                Logger.LogError(AudioMixer.ModuleName, $"AudioType should not be set to Master for '{gameObject.name}'");
            }

            if (UnityAudio.playOnAwake)
            {
                Logger.LogError(AudioMixer.ModuleName, $"playOnAwake should not be enabled for UnityEngine.AudioSource '{gameObject.name}'");
            }

            AudioMixer.ConfigureAudioSource(this);
        }

        /// <inheritdoc/>
        protected virtual void OnEnable()
        {
            if (_playEndCheckerCoroutine != null)
            {
                StopCoroutine(_playEndCheckerCoroutine);
                _playEndCheckerCoroutine = null;
            }
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            if (_playEndCheckerCoroutine != null)
            {
                StopCoroutine(_playEndCheckerCoroutine);
                _playEndCheckerCoroutine = null;
            }

            IsPlaying = false;
            if (UnityAudio) // Check for null first. Disable could be a part of destruction
            {
                UnityAudio.Stop();
            }
        }

        /// <inheritdoc/>
        public virtual void PlayCurrentClip()
        {
            PlayClipPreStart?.Invoke(this);

            UnityAudio.Play();

            IsPlaying = true;

            PlayClipPostStart?.Invoke(this);

            if (_playEndCheckerCoroutine != null)
            {
                StopCoroutine(_playEndCheckerCoroutine);
            }
            _playEndCheckerCoroutine = PlayEndChecker();
            StartCoroutine(_playEndCheckerCoroutine);

        }

        /// <inheritdoc/>
        public void PlayCurrentClipAtTime(float playbackTime)
        {
            if (playbackTime < UnityAudio.clip.length)
            {
                UnityAudio.time = playbackTime;
                PlayCurrentClip();
            }
        }

        /// <inheritdoc/>
        public void PlayClip(AudioClip clip)
        {
            UnityAudio.time = 0f;
            UnityAudio.clip = clip;

            PlayCurrentClip();
        }

        /// <inheritdoc/>
        public void StopCurrentClip()
        {
            UnityAudio.Stop();

            IsPlaying = false;
            PlayClipStopped?.Invoke(this);

            if (_playEndCheckerCoroutine != null)
            {
                StopCoroutine(_playEndCheckerCoroutine);
                _playEndCheckerCoroutine = null;
            }
        }

        private IEnumerator PlayEndChecker()
        {
            var waitForClipDuration = new WaitForSeconds(UnityAudio.clip.length);
            do
            {
                yield return waitForClipDuration;
            } while (UnityAudio.loop);
            
            // clip duration may be extended due to audio effects
            while (UnityAudio.isPlaying && UnityAudio.time > 0f)
            {
                yield return null;
            }

            IsPlaying = false;

            _playEndCheckerCoroutine = null;

            PlayClipStopped?.Invoke(this);
        }
    }
}
