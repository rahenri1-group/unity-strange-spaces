﻿using Game.Core.Entity;
using Game.Core.Event;

namespace Game.Core.Audio
{
    /// <summary>
    /// An audio controller that is bound to an entity
    /// </summary>
    public class EntityAudioSpacePositionerBehaviour : AudioSpacePositionerBehaviour
    {
        private IEntity AudioEntity
        {
            get
            {
                if (_audioEntityCached == null)
                {
                    _audioEntityCached = GetComponentInParent<IEntity>();
                }
                return _audioEntityCached;
            }
        }
        private IEntity _audioEntityCached = null;

        private bool _wasPlayingBeforeTeleport;
        private float _resumeTimePostTeleport;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            _wasPlayingBeforeTeleport = false;
            _resumeTimePostTeleport = 0f;

            EventBus.Subscribe<EntityPreTeleportEvent>(OnEntityPreTeleport);
            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityPostTeleport);
        }

        /// <inheritdoc />
        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventBus.Unsubscribe<EntityPreTeleportEvent>(OnEntityPreTeleport);
            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityPostTeleport);
        }

        private void OnEntityPreTeleport(EntityPreTeleportEvent eventArgs)
        {
            if (eventArgs.Entity != AudioEntity) return;

            if (AudioSource.IsPlaying && State != AudioState.Muted)
            {
                _wasPlayingBeforeTeleport = true;
                _resumeTimePostTeleport = AudioSource.CurrentClipPlayBackTime;
            }
            else
            {
                _wasPlayingBeforeTeleport = false;
                _resumeTimePostTeleport = 0f;
            }
        }

        private void OnEntityPostTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (eventArgs.Entity != AudioEntity) return;

            Space = SpaceManager.GetObjectSpace(gameObject);

            if (_wasPlayingBeforeTeleport && State != AudioState.Muted)
            {
                AudioSource.PlayCurrentClipAtTime(_resumeTimePostTeleport);
            }

            UpdateAudioState();
            UpdateAudioPosition();
        }
    }
}
