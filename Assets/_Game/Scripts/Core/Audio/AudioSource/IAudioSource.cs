﻿using System;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// A source of audio
    /// </summary>
    public interface IAudioSource : IGameObjectComponent
    {
        /// <summary>
        /// Raised immediately before the audio source starts playing
        /// </summary>
        event Action<IAudioSource> PlayClipPreStart;

        /// <summary>
        /// Raised immediately after the audio source starts playing
        /// </summary>
        event Action<IAudioSource> PlayClipPostStart;
        /// <summary>
        /// Raised when the audio source stops playing
        /// </summary>
        event Action<IAudioSource> PlayClipStopped;

        /// <summary>
        /// The <see cref="AudioGroupType"/> of the audio.
        /// Avoid directly touching this when possible and use Core Audio system components.
        /// </summary>
        AudioGroupType AudioType { get; }

        /// <summary>
        /// The Unity <see cref="AudioSource"/>
        /// </summary>
        AudioSource UnityAudio { get; }       

        /// <summary>
        /// Is the audio 3d or 2d
        /// </summary>
        bool Is3dAudio { get; }

        /// <summary>
        /// Is the audiosource currently playing
        /// </summary>
        bool IsPlaying { get; }

        /// <summary>
        /// The max distance the audio can be heard
        /// </summary>
        float MaxDistance { get; }

        /// <summary>
        /// The current playback time of the clip (in seconds)
        /// </summary>
        float CurrentClipPlayBackTime { get; }

        /// <summary>
        /// Plays the audio source with it's current clip
        /// </summary>
        void PlayCurrentClip();

        /// <summary>
        /// Plays the audio source with it's current clip at the provided playback time
        /// </summary>
        void PlayCurrentClipAtTime(float playbackTime);

        /// <summary>
        /// Sets the clip of the audio source to <paramref name="clip"/> and plays it
        /// </summary>
        void PlayClip(AudioClip clip);

        /// <summary>
        /// Stops playback of the audio source
        /// </summary>
        void StopCurrentClip();
    }
}
