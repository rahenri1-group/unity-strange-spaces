﻿using Game.Core.Resource;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Poolable version of <see cref="IAudioSource"/>
    /// </summary>
    public class PoolableAudioSourceBehaviour : BaseModifiableAudioSource, IPoolableComponent
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        /// <inheritdoc/>
        public virtual void OnAllocate() { }

        /// <inheritdoc/>
        public virtual void OnDispose() 
        {
            RemoveAllAudioModifierDefinitions();
        }
    }
}
