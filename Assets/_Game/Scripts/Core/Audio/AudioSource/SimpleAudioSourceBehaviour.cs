﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="BaseAudioSource"/>
    [RequireComponent(typeof(AudioSource))]
    public class SimpleAudioSourceBehaviour : BaseAudioSource
    {
        /// <inheritdoc/>
        public override AudioSource UnityAudio => _unityAudio;

        private AudioSource _unityAudio;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _unityAudio = GetComponent<AudioSource>();
        }
    }
}
