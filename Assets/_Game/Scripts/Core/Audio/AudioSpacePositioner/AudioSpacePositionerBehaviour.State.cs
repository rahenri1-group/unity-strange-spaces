﻿namespace Game.Core.Audio
{
    public partial class AudioSpacePositionerBehaviour : InjectedBehaviour
    {
        protected enum AudioState { Unmuted, Muted }

        protected AudioState State { get; private set; }

        private void StateAwake()
        {
            // mute unil we get audio pathing
            AudioSource.UnityAudio.mute = true;
            State = AudioState.Muted;
        }

        private void TransitionToState(AudioState newState)
        {
            AudioSource.UnityAudio.mute = newState == AudioState.Muted;

            if (newState != State)
            {
                State = newState;
            }
        }
    }
}
