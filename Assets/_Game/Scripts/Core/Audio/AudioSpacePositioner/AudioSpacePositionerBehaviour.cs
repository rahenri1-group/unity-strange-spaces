﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Audio
{
    /// <summary>
    /// Manages the position of an <see cref="IModifiableAudioSource"/> across multiple spaces
    /// </summary>
    public partial class AudioSpacePositionerBehaviour : InjectedBehaviour
    {
        [Inject] protected IEventBus EventBus;
        [Inject] protected ISpaceAudioManager SpaceAudioManager;
        [Inject] protected ISpaceManager SpaceManager;

        /// <summary>
        /// The audiosource we are managing the position of
        /// </summary>
        public IModifiableAudioSource AudioSource { get; private set; }
        /// <summary>
        /// The current space of the Audiosource
        /// </summary>
        public ISpaceData Space { get; protected set; }

        private IPortalPath<IPortalAudioLink> _currentAudioPath;
        private List<IAudioModifierDefinition> _currentAudioPathModifiers;

        private IEnumerator _audioUpdateCoroutine;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            AudioSource = this.GetComponentAsserted<IModifiableAudioSource>();

            // audiosource should be on a child gameobject with no other scripts attached
            Assert.AreEqual(AudioSource.UnityAudio.transform.parent, transform); // audiosource is child of this
            Assert.AreEqual(AudioSource.UnityAudio.transform.childCount, 0); // no children on audiosource
            Assert.AreEqual(AudioSource.UnityAudio.GetComponents<Component>().Length, 2); // audiosource only has transform & audiosource components

            // non-3d space based audio isn't currently supported
            Assert.IsTrue(AudioSource.Is3dAudio);

            Space = SpaceManager.GetObjectSpace(gameObject);

            _currentAudioPath = null;
            _currentAudioPathModifiers = null;

            StateAwake();

            EventBus.Subscribe<AudioListenerSpaceChangeEvent>(OnAudioListenerSpaceChange);

            AudioSource.PlayClipPreStart += OnAudioSourcePreStart;
            AudioSource.PlayClipStopped += OnAudioSourceStop;

            _audioUpdateCoroutine = null;
        }

        /// <inheritdoc />
        protected virtual void OnDestroy()
        {
            // ensure audio source has been destroyed as it may have become un-parented
            if (AudioSource.UnityAudio)
            {
                Destroy(AudioSource.UnityAudio);
            }

            EventBus.Unsubscribe<AudioListenerSpaceChangeEvent>(OnAudioListenerSpaceChange);

            AudioSource.PlayClipPreStart -= OnAudioSourcePreStart;
            AudioSource.PlayClipStopped -= OnAudioSourceStop;
        }

        /// <inheritdoc />
        protected virtual void OnEnable()
        {
            Space = SpaceManager.GetObjectSpace(gameObject);

            UpdateAudioState(false);
            UpdateAudioPosition();

            if (_audioUpdateCoroutine == null && AudioSource.IsPlaying)
            {
                _audioUpdateCoroutine = AudioUpdate();
                StartCoroutine(_audioUpdateCoroutine);
            }
        }

        /// <inheritdoc />
        protected virtual void OnDisable()
        {
            if (_audioUpdateCoroutine != null)
            {
                StopCoroutine(_audioUpdateCoroutine);
                _audioUpdateCoroutine = null;
            }
        }

        private void OnAudioSourcePreStart(IAudioSource obj)
        {
            UpdateAudioState(false);
            UpdateAudioPosition();

            if (_audioUpdateCoroutine == null)
            {
                _audioUpdateCoroutine = AudioUpdate();
                StartCoroutine(_audioUpdateCoroutine);
            }
        }

        private void OnAudioSourceStop(IAudioSource obj)
        {
            if (_audioUpdateCoroutine != null)
            {
                StopCoroutine(_audioUpdateCoroutine);
                _audioUpdateCoroutine = null;
            }

            UpdateAudioState();
            UpdateAudioPosition();
        }

        private void OnAudioListenerSpaceChange(AudioListenerSpaceChangeEvent eventArgs)
        {
            UpdateAudioState(false);
            UpdateAudioPosition();
        }

        protected void UpdateAudioState(bool onlyUpdateIfPlaying = true)
        {
            if (_currentAudioPathModifiers != null)
            {
                AudioSource.RemoveAudioModifierDefinitions(_currentAudioPathModifiers);
            }

            if (onlyUpdateIfPlaying && !AudioSource.IsPlaying)
            {
                _currentAudioPath = null;
                _currentAudioPathModifiers = null;
                return;
            }

            _currentAudioPathModifiers = new List<IAudioModifierDefinition>();

            bool pathExists = SpaceAudioManager.GetAudioPathToListener(Space, AudioSource, out _currentAudioPath);
            
            if (pathExists)
            {
                foreach (var node in _currentAudioPath.Path)
                {
                    if (ShouldApplyPathAudioLink(node.Portal))
                    {
                        var audioModifiers = node.Portal.AudioModifiers;

                        _currentAudioPathModifiers.AddRange(audioModifiers);
                        foreach (var audioModifier in audioModifiers)
                        {
                            AudioSource.AddAudioModifierDefinition(audioModifier);
                        }
                    }
                }
            }
            
            TransitionToState((pathExists) ? AudioState.Unmuted : AudioState.Muted);
        }

        protected virtual bool ShouldApplyPathAudioLink(IPortalAudioLink link)
        {
            return link != null;
        }

        protected void UpdateAudioPosition()
        {
            if (!gameObject.activeInHierarchy || AudioSource.UnityAudio.mute || _currentAudioPath == null || _currentAudioPath.Path.Length <= 1)
            {
                AudioSource.UnityAudio.transform.SetParent(transform);
                AudioSource.UnityAudio.transform.localPosition = Vector3.zero;
                AudioSource.UnityAudio.transform.localRotation = Quaternion.identity;
                return;
            }

            if (AudioSource.UnityAudio.transform != null)
            {
                AudioSource.UnityAudio.transform.SetParent(null);
            }

            // get distance from audio to nearest portal. Ignore last node as that is the distance from the listener to the nearest portal
            float distance = 0f;
            for (int i = 0; i < _currentAudioPath.Path.Length - 1; i++)
            {
                distance += _currentAudioPath.Path[i].Distance;
            }

            var lastAudioPathNode = _currentAudioPath.Path[_currentAudioPath.Path.Length - 2];

            if (lastAudioPathNode.Portal.ExitPortal != null) // could be null from portal just closing
            {
                Vector3 audioDirection = (lastAudioPathNode.Portal.EntryPortal.CalculateEndPointPosition(lastAudioPathNode.SpaceStartPosition) - SpaceAudioManager.AudioListener.GameObject.transform.position).normalized;
                Ray portalRay = new Ray(SpaceAudioManager.AudioListener.GameObject.transform.position, audioDirection);

                if (lastAudioPathNode.Portal.ExitPortal.PortalPlane.Raycast(portalRay, out float rayDistance)  // only if we are looking at portal plane
                    && lastAudioPathNode.Portal.ExitPortal.PortalPlane.GetSide(SpaceAudioManager.AudioListener.GameObject.transform.position) == false) // only if we are facing open side of portal
                {
                    Vector3 portalPoint = portalRay.GetPoint(rayDistance);

                    if (PointInPortal(lastAudioPathNode.Portal.ExitPortal, portalPoint)) // only if we can see the audiosource through portal
                    {
                        portalPoint = ClosestPointInPortal(lastAudioPathNode.Portal.ExitPortal, portalPoint);

                        AudioSource.UnityAudio.transform.position = portalPoint + distance * audioDirection;
                        return;
                    }
                }

                // else grab closest point of closest edge of the portal
                Vector3 closestPoint = ClosestPointInPortal(
                    lastAudioPathNode.Portal.ExitPortal,
                    lastAudioPathNode.Portal.ExitPortal.PortalPlane.ClosestPointOnPlane(SpaceAudioManager.AudioListener.GameObject.transform.position));

                var audioPortalDirection = (closestPoint - SpaceAudioManager.AudioListener.GameObject.transform.position).normalized;

                // add distance from portal edge to center
                distance += Vector3.Distance(closestPoint, lastAudioPathNode.Portal.ExitPortal.Transform.position);

                AudioSource.UnityAudio.transform.position = closestPoint + distance * audioPortalDirection;
            }
        }

        /// <summary>
        /// Tests whether a point fits within the bounds of the portal
        /// </summary>
        private bool PointInPortal(IPortal portal, Vector3 testPoint)
        {
            var localPortalPoint = portal.Transform.InverseTransformPointUnscaled(testPoint);

            return -0.5f * portal.PortalSize.x <= localPortalPoint.x && localPortalPoint.x <= 0.5f * portal.PortalSize.x
                && -0.5f * portal.PortalSize.y <= localPortalPoint.y && localPortalPoint.y <= 0.5f * portal.PortalSize.y;
        }

        private Vector3 ClosestPointInPortal(IPortal portal, Vector3 testPoint)
        {
            var localPortalPoint = portal.Transform.InverseTransformPointUnscaled(testPoint);
            localPortalPoint.x = Mathf.Clamp(localPortalPoint.x, -0.5f * portal.PortalSize.x, 0.5f * portal.PortalSize.x);
            localPortalPoint.y = Mathf.Clamp(localPortalPoint.y, -0.5f * portal.PortalSize.y, 0.5f * portal.PortalSize.y);

            return portal.Transform.TransformPointUnscaled(localPortalPoint);
        }

        private IEnumerator AudioUpdate()
        {
            YieldInstruction wait = new WaitForEndOfFrame();

            float lastUpdateTime = Time.time;

            while (true)
            {
                yield return wait;

                if ((_currentAudioPath != null && _currentAudioPath.IsPathStale) || (Time.time - lastUpdateTime >= 0.1f))
                {
                    UpdateAudioState();

                    lastUpdateTime = Time.time;
                }

                UpdateAudioPosition();
            }
        }
    }
}
