﻿using Game.Core.Resource;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Poolable version of <see cref="AudioSpacePositionerBehaviour"/>
    /// </summary>
    public class PoolableAudioSpacePositionerBehaviour : AudioSpacePositionerBehaviour, IPoolableComponent
    {
        /// <inheritdoc/>
        public void OnAllocate() { }

        /// <inheritdoc/>
        public void OnDispose()
        {
            AudioSource.UnityAudio.transform.SetParent(transform);
            AudioSource.UnityAudio.transform.localPosition = Vector3.zero;
            AudioSource.UnityAudio.transform.localRotation = Quaternion.identity;
        }
    }
}