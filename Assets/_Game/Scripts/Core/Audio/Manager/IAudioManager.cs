﻿namespace Game.Core.Audio
{
    /// <summary>
    /// Module to control all audio
    /// </summary>
    public interface IAudioManager : IModule { }
}
