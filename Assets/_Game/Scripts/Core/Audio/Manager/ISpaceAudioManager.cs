﻿using Game.Core.Portal;
using Game.Core.Space;

namespace Game.Core.Audio
{
    /// <summary>
    /// Controls all audio and limits / manipulates the <see cref="IAudioSource"/>s based upon the space of the source and of the <see cref="ISpaceAudioListener"/>
    /// </summary>
    public interface ISpaceAudioManager : IAudioManager
    {
        /// <summary>
        /// The primary listoner for audio
        /// </summary>
        ISpaceAudioListener AudioListener { get; }

        /// <summary>
        /// Returns true if there is a path from the audio source to the listener within the range of the audio source.
        /// Returns false if there is no path or the listener is out of range
        /// </summary>
        bool GetAudioPathToListener(ISpaceData space, IAudioSource audioSource, out IPortalPath<IPortalAudioLink> pathInfo);

        /// <summary>
        /// Returns true if there is a path from <paramref name="startPosition"/> to <paramref name="endPosition"/> 
        /// Returns false if there is no path or the path would be longer than <paramref name="maxDistance"/> 
        /// </summary>
        bool GetAudioPath(SpacePosition startPosition, SpacePosition endPosition, float maxDistance, out IPortalPath<IPortalAudioLink> pathInfo);
    }
}
