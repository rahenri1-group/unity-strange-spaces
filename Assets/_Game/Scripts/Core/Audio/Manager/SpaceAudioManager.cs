﻿using Cysharp.Threading.Tasks;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="ISpaceAudioManager"/>
    [Dependency(
        contract: typeof(ISpaceAudioManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IAudioManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class SpaceAudioManager : BaseModule, IAudioManager, ISpaceAudioManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Audio Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        /// <inheritdoc/>
        public ISpaceAudioListener AudioListener => _audioListener;

        private readonly IEventBus _eventBus;
        private readonly ISpaceManager _spaceManager;

        private SpaceAudioListenerBehaviour _audioListener;

        private IPortalPathSolver<IPortalAudioLink> _portalPathSolver;

        /// <summary>
        /// The injection constructor
        /// </summary>
        public SpaceAudioManager(
            IEventBus eventBus,
            ILogRouter logger,
            ISpaceManager spaceManager)
            : base(logger)
        {
            _eventBus = eventBus;
            _spaceManager = spaceManager;

            _portalPathSolver = new DijkstraPortalPathSolver<IPortalAudioLink>();

            _audioListener = null;
        }

        /// <inheritdoc/>
        public async override UniTask Initialize()
        {
            var audioListenerGameObject = new GameObject("@AudioListener");
            await _spaceManager.MoveObjectToSpaceAsync(audioListenerGameObject, null);

            _audioListener = audioListenerGameObject.AddComponent<SpaceAudioListenerBehaviour>();

            _eventBus.Subscribe<CameraActivateEvent>(OnCameraActivate);
            _eventBus.Subscribe<CameraDeactivateEvent>(OnCameraDeactivate);

            _eventBus.Subscribe<PortalOpenedEvent>(OnPortalOpened);
            _eventBus.Subscribe<PortalClosedEvent>(OnPortalClosed);

            await base.Initialize();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _portalPathSolver.ClearAllPortals();

            _eventBus.Unsubscribe<CameraActivateEvent>(OnCameraActivate);
            _eventBus.Unsubscribe<CameraDeactivateEvent>(OnCameraDeactivate);

            _eventBus.Unsubscribe<PortalOpenedEvent>(OnPortalOpened);
            _eventBus.Unsubscribe<PortalClosedEvent>(OnPortalClosed);

            return base.Shutdown();
        }

        private void OnCameraActivate(CameraActivateEvent eventData)
        {
            if (!eventData.Camera.CameraType.HasFlag(Render.Camera.CameraType.PrimaryCamera)) return;

            var spaceCamera = (ISpaceCamera) eventData.Camera;
            _audioListener.TargetCamera = spaceCamera;
        }

        private void OnCameraDeactivate(CameraDeactivateEvent eventData)
        {
            if (!eventData.Camera.CameraType.HasFlag(Render.Camera.CameraType.PrimaryCamera)) return;

            if (_audioListener != null && eventData.Camera == _audioListener.TargetCamera)
            {
                _audioListener.TargetCamera = null;
            }
        }

        private void OnPortalOpened(PortalOpenedEvent eventData)
        {
            IPortalAudioLink audioLink = eventData.Portal.GetComponent<IPortalAudioLink>();
            if (audioLink == null) return;

            _portalPathSolver.AddPortal(audioLink);
        }

        private void OnPortalClosed(PortalClosedEvent eventData)
        {
            IPortalAudioLink audioLink = eventData.Portal.GetComponent<IPortalAudioLink>();
            if (audioLink == null) return;

            _portalPathSolver.RemovePortal(audioLink);
        }

        /// <inheritdoc/>
        public bool GetAudioPathToListener(ISpaceData space, IAudioSource audioSource, out IPortalPath<IPortalAudioLink> pathInfo)
        {
            if (!ModuleInitialized)
            {
                pathInfo = null;
                return false;
            }

            return _portalPathSolver.GetPath(
                new SpacePosition(space, audioSource.GameObject.transform.position),
                new SpacePosition(AudioListener.ListeningSpace, AudioListener.GameObject.transform.position),
                audioSource.MaxDistance,
                out pathInfo);
        }

        /// <inheritdoc/>
        public bool GetAudioPath(SpacePosition startPosition, SpacePosition endPosition, float maxDistance, out IPortalPath<IPortalAudioLink> pathInfo)
        {
            if (!ModuleInitialized)
            {
                pathInfo = null;
                return false;
            }

            return _portalPathSolver.GetPath(startPosition, endPosition, maxDistance, out pathInfo);
        }
    }
}
