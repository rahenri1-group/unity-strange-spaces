﻿using System;
using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Base class for <see cref="IAudioModifierDefinition"/>
    /// </summary>
    public abstract class BaseAudioModifierDefinition : IAudioModifierDefinition
    {
        /// <inheritdoc />
        public event Action<IAudioModifierDefinition> ModifierChanged;

        /// <inheritdoc />
        public bool Enabled 
        {
            get => _enabled;
            set
            {
                _enabled = value;
                OnModifierChanged();
            }
        }
        private bool _enabled = true;

        /// <inheritdoc />
        public float ModifierLerp 
        {
            get => _modifierLerp;
            set
            {
                _modifierLerp = value;
                OnModifierChanged();
            }
        }
        private float _modifierLerp = 1f;

        /// <inheritdoc />
        public abstract IAudioModifierDefinition Clone();

        protected void OnModifierChanged()
        {
            ModifierChanged?.Invoke(this);
        }

        /// <inheritdoc />
        public void ApplyToAudioSource(IModifiableAudioSource audioSource)
        {
            if (!Enabled || ModifierLerp <= 0f) return;

            if (ModifierLerp >= 1f)
            {
                ApplyToAudioSourceFull(audioSource);
            }
            else
            {
                ApplyToAudioSourceLerped(audioSource, Mathf.Clamp01(ModifierLerp));
            }
        }

        protected abstract void ApplyToAudioSourceFull(IModifiableAudioSource audioSource);

        protected abstract void ApplyToAudioSourceLerped(IModifiableAudioSource audioSource, float lerp);
    }
}
