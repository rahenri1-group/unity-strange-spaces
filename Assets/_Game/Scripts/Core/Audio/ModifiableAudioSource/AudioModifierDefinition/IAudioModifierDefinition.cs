﻿using System;

namespace Game.Core.Audio
{
    /// <summary>
    /// Definition of an effect that can change a <see cref="IModifiableAudioSource"/>
    /// </summary>
    public interface IAudioModifierDefinition
    {
        /// <summary>
        /// Event that is raised when a property on the modifier has changed.
        /// This is to inform <see cref="IModifiableAudioSource"/>s to apply the updated properties.
        /// </summary>
        event Action<IAudioModifierDefinition> ModifierChanged;

        /// <summary>
        /// Enables / disables the <see cref="IAudioModifierDefinition"/>
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Value from 0 to 1 on the amount of the modifier to apply.
        /// 0 is equivalent to being disabled
        /// </summary>
        float ModifierLerp { get; set; }

        /// <summary>
        /// Applies the audio modifier definition
        /// </summary>
        /// <param name="audioSource"></param>
        void ApplyToAudioSource(IModifiableAudioSource audioSource);

        /// <summary>
        /// Creates a duplicate version of the <see cref="IAudioModifierDefinition"/>
        /// </summary>
        /// <returns></returns>
        IAudioModifierDefinition Clone();
    }
}
