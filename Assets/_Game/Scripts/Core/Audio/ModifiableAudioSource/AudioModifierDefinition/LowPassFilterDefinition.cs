﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Definition for a <see cref="IModifiableAudioSource.ILowPassFilter"/>
    /// </summary>
    public class LowPassFilterDefinition : BaseAudioModifierDefinition
    {
        /// <summary>
        /// The cutoff frequency
        /// </summary>
        public float CutoffFrequency
        {
            get => _cutoffFrequency;
            set
            {
                _cutoffFrequency = value;
                OnModifierChanged();
            }
        }

        [SerializeField] private float _cutoffFrequency;

        /// <summary>
        /// Constructor
        /// </summary>
        public LowPassFilterDefinition()
        {
            _cutoffFrequency = 5000f;
        }

        /// <inheritdoc />
        public override IAudioModifierDefinition Clone()
        {
            var definition = new LowPassFilterDefinition();
            definition.Enabled = Enabled;
            definition.ModifierLerp = ModifierLerp;

            definition.CutoffFrequency = CutoffFrequency;

            return definition;
        }

        protected override void ApplyToAudioSourceFull(IModifiableAudioSource audioSource)
        {
            audioSource.LowPassFilter.Enabled = true;
            audioSource.LowPassFilter.CutoffFrequency = Mathf.Min(_cutoffFrequency, audioSource.LowPassFilter.CutoffFrequency);
        }

        protected override void ApplyToAudioSourceLerped(IModifiableAudioSource audioSource, float lerp)
        {
            audioSource.LowPassFilter.Enabled = true;

            float lerpFrequency = Mathf.Lerp(22000, _cutoffFrequency, lerp);
            audioSource.LowPassFilter.CutoffFrequency = Mathf.Min(lerpFrequency, audioSource.LowPassFilter.CutoffFrequency);
        }
    }
}
