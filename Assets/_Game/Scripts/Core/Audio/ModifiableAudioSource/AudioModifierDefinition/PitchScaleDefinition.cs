﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Definition for a <see cref="IModifiableAudioSource.IPitchScale"/>
    /// </summary>
    public class PitchScaleDefinition : BaseAudioModifierDefinition
    {
        /// <summary>
        /// The amount to scale pitch by
        /// </summary>
        public float PitchScale
        {
            get => _pitchScale;
            set
            {
                _pitchScale = value;
                OnModifierChanged();
            }
        }

        [SerializeField] private float _pitchScale;

        /// <summary>
        /// Constructor
        /// </summary>
        public PitchScaleDefinition()
        {
            _pitchScale = 1f;
        }

        /// <inheritdoc />
        public override IAudioModifierDefinition Clone()
        {
            var definition = new PitchScaleDefinition();
            definition.Enabled = Enabled;
            definition.ModifierLerp = ModifierLerp;

            definition.PitchScale = PitchScale;

            return definition;
        }

        protected override void ApplyToAudioSourceFull(IModifiableAudioSource audioSource)
        {
            audioSource.PitchScale.Enabled = true;
            audioSource.PitchScale.PitchScale *= _pitchScale;
        }

        protected override void ApplyToAudioSourceLerped(IModifiableAudioSource audioSource, float lerp)
        {
            audioSource.PitchScale.Enabled = true;
            audioSource.PitchScale.PitchScale *= Mathf.Lerp(1f, _pitchScale, lerp);
        }
    }
}
