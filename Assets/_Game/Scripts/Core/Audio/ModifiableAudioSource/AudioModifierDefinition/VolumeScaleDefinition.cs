﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <summary>
    /// Definition for a <see cref="IModifiableAudioSource.IVolumeScale"/>
    /// </summary>
    public class VolumeScaleDefinition : BaseAudioModifierDefinition
    {
        /// <summary>
        /// The amount to scale volume by
        /// </summary>
        public float VolumeScale
        {
            get => _volumeScale;
            set
            {
                _volumeScale = value;
                OnModifierChanged();
            }
        }

        /// <summary>
        /// The current volume scale with modifier lerp applied
        /// </summary>
        public float CurrentVolumeScale => Mathf.Lerp(1f, VolumeScale, ModifierLerp);

        [SerializeField] private float _volumeScale;

        /// <summary>
        /// Constructor
        /// </summary>
        public VolumeScaleDefinition()
        {
            _volumeScale = 1f;
        }

        /// <inheritdoc />
        public override IAudioModifierDefinition Clone()
        {
            var definition = new VolumeScaleDefinition();
            definition.Enabled = Enabled;
            definition.ModifierLerp = ModifierLerp;

            definition.VolumeScale = VolumeScale;

            return definition;
        }

        protected override void ApplyToAudioSourceFull(IModifiableAudioSource audioSource)
        {
            audioSource.VolumeScale.Enabled = true;
            audioSource.VolumeScale.VolumeScale *= _volumeScale;
        }

        protected override void ApplyToAudioSourceLerped(IModifiableAudioSource audioSource, float lerp)
        {
            audioSource.VolumeScale.Enabled = true;
            audioSource.VolumeScale.VolumeScale *= Mathf.Lerp(1f, _volumeScale, lerp);
        }
    }
}
