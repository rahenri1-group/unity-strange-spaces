﻿using UnityEngine;

namespace Game.Core.Audio
{
    public abstract partial class BaseModifiableAudioSource : BaseAudioSource
    {
        private class LowPassFilterEffect : IModifiableAudioSource.ILowPassFilter
        {
            public bool Enabled 
            {
                get => _enabled;
                set
                {
                    _enabled = value;
                    UpdateAudioSource();
                }
            }

            public float CutoffFrequency
            {
                get => _cutoffFrequency;
                set
                {
                    _cutoffFrequency = value;
                    UpdateAudioSource();
                }
            }

            private BaseModifiableAudioSource _audioSource;

            private bool _enabled;
            private float _cutoffFrequency;

            private AudioLowPassFilter _audioLowPassFilter;

            public LowPassFilterEffect(BaseModifiableAudioSource audioSource)
            {
                _audioSource = audioSource;

                _audioLowPassFilter = null;

                Reset();
            }

            public void Reset()
            {
                _enabled = false;
                _cutoffFrequency = 22000f;

                UpdateAudioSource();
            }

            private void UpdateAudioSource()
            {
                if (!Enabled)
                {
                    if (_audioLowPassFilter != null)
                    {
                        _audioLowPassFilter.enabled = false;
                    }
                }
                else
                {
                    if (_audioLowPassFilter == null)
                    {
                        _audioLowPassFilter = _audioSource.UnityAudio.gameObject.AddComponent<AudioLowPassFilter>();
                    }

                    _audioLowPassFilter.cutoffFrequency = _cutoffFrequency;
                    _audioLowPassFilter.enabled = true;
                }
            }
        }

        private LowPassFilterEffect LowPassFilterImplementation
        {
            get
            {
                if (_lowPassFilter == null) _lowPassFilter = new LowPassFilterEffect(this);
                return _lowPassFilter;
            }
        }
        private LowPassFilterEffect _lowPassFilter;
    }
}
