﻿namespace Game.Core.Audio
{
    public abstract partial class BaseModifiableAudioSource : BaseAudioSource
    {
        private class PitchScaleEffect : IModifiableAudioSource.IPitchScale 
        {
            /// <inheritdoc/>
            public bool Enabled
            {
                get => _enabled;
                set
                {
                    _enabled = value;
                    UpdateAudioSource();
                }
            }

            /// <inheritdoc/>
            public float PitchScale
            {
                get => _pitchScale;
                set
                {
                    _pitchScale = value;
                    UpdateAudioSource();
                }
            }

            private BaseModifiableAudioSource _audioSource;

            private bool _enabled;
            private float _pitchScale;

            public PitchScaleEffect(BaseModifiableAudioSource audioSource)
            {
                _audioSource = audioSource;

                Reset();
            }

            public void Reset()
            {
                _enabled = false;
                _pitchScale = 1f;

                UpdateAudioSource();
            }

            private void UpdateAudioSource()
            {
                _audioSource.UnityAudio.pitch = Enabled ? _pitchScale : 1f;
            }
        }

        private PitchScaleEffect PitchScaleImplementation
        {
            get
            {
                if (_pitchScale == null) _pitchScale = new PitchScaleEffect(this);
                return _pitchScale;
            }
        }
        private PitchScaleEffect _pitchScale;
    }
}
