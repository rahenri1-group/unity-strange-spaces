﻿namespace Game.Core.Audio
{
    public abstract partial class BaseModifiableAudioSource : BaseAudioSource
    {
        private class VolumeScaleEffect : IModifiableAudioSource.IVolumeScale
        {
            /// <inheritdoc/>
            public bool Enabled
            {
                get => _enabled;
                set
                {
                    _enabled = value;
                    UpdateAudioSource();
                }
            }

            /// <inheritdoc/>
            public float VolumeScale
            {
                get => _volumeScale;
                set
                {
                    _volumeScale = value;
                    UpdateAudioSource();
                }
            }

            /// <inheritdoc/>
            public float VolumeBase
            {
                get => _volumeBase;
                set
                {
                    _volumeBase = value;
                    UpdateAudioSource();
                }
            }

            private BaseModifiableAudioSource _audioSource;

            private bool _enabled;
            private float _volumeScale;

            private float _volumeBase;

            public VolumeScaleEffect(BaseModifiableAudioSource audioSource)
            {
                _audioSource = audioSource;

                _volumeBase = _audioSource.UnityAudio.volume;

                Reset();
            }

            public void Reset()
            {
                _enabled = false;
                _volumeScale = 1f;

                UpdateAudioSource();
            }

            private void UpdateAudioSource()
            {
                if (Enabled)
                {
                    _audioSource.UnityAudio.volume = _volumeBase * VolumeScale;
                }
                else
                {
                    _audioSource.UnityAudio.volume = _volumeBase;
                }
            }
        }

        private VolumeScaleEffect VolumeScaleImplementation
        {
            get
            {
                if (_volumeScale == null) _volumeScale = new VolumeScaleEffect(this);
                return _volumeScale;
            }
        }
        private VolumeScaleEffect _volumeScale;
    }
}
