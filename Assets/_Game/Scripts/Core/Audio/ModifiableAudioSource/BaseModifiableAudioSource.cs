﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.Core.Audio
{
    /// <inheritdoc cref="IModifiableAudioSource"/>
    public abstract partial class BaseModifiableAudioSource : BaseAudioSource, IModifiableAudioSource
    {
        /// <inheritdoc/>
        public IModifiableAudioSource.IVolumeScale VolumeScale => VolumeScaleImplementation;
        /// <inheritdoc/>
        public IModifiableAudioSource.IPitchScale PitchScale => PitchScaleImplementation;
        /// <inheritdoc/>
        public IModifiableAudioSource.ILowPassFilter LowPassFilter => LowPassFilterImplementation;

        private ICollection<IAudioModifierDefinition> AppliedAudioModifiers
        {
            get
            {
                if (_appliedAudioModifiers == null) _appliedAudioModifiers = new LinkedList<IAudioModifierDefinition>();
                return _appliedAudioModifiers;
            }
        }
        private ICollection<IAudioModifierDefinition> _appliedAudioModifiers;

        private bool _audioModifiersChanged;
        private IEnumerator _audioModifierApplyCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _audioModifiersChanged = false;
            _audioModifierApplyCoroutine = null;
        }

        /// <inheritdoc/>
        protected virtual void OnDestroy()
        {
            RemoveAllAudioModifierDefinitions();
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            if (_audioModifierApplyCoroutine != null)
            {
                StopCoroutine(_audioModifierApplyCoroutine);
                _audioModifierApplyCoroutine = null;
            }
        }

        private void OnAudioModifierChanged(IAudioModifierDefinition audioModifier)
        {
            _audioModifiersChanged = true;
        }

        public override void PlayCurrentClip()
        {
            ReapplyAllAudioModifierDefinitions();

            base.PlayCurrentClip();

            if (_audioModifierApplyCoroutine == null)
            {
                _audioModifierApplyCoroutine = AudioModifierApplier();
                StartCoroutine(_audioModifierApplyCoroutine);
            }
        }

        /// <inheritdoc/>
        public void AddAudioModifierDefinition(IAudioModifierDefinition audioModifier)
        {
            AppliedAudioModifiers.Add(audioModifier);

            _audioModifiersChanged = true;

            audioModifier.ModifierChanged += OnAudioModifierChanged;
        }

        /// <inheritdoc/>
        public void RemoveAudioModifierDefinition(IAudioModifierDefinition audioModifier)
        {
            if (AppliedAudioModifiers.Remove(audioModifier))
            {
                _audioModifiersChanged = true;

                audioModifier.ModifierChanged -= OnAudioModifierChanged;
            }
        }

        /// <inheritdoc/>
        public void RemoveAudioModifierDefinitions(IEnumerable<IAudioModifierDefinition> audioModifiers)
        {
            foreach (var modifier in audioModifiers)
            {
                RemoveAudioModifierDefinition(modifier);
            }
        }

        /// <inheritdoc/>
        public void RemoveAllAudioModifierDefinitions()
        {
            foreach (var audioModifier in AppliedAudioModifiers)
            {
                audioModifier.ModifierChanged -= OnAudioModifierChanged;
            }

            AppliedAudioModifiers.Clear();

            _audioModifiersChanged = true;
        }

        private void ReapplyAllAudioModifierDefinitions()
        {
            VolumeScaleImplementation.Reset();
            PitchScaleImplementation.Reset();
            LowPassFilterImplementation.Reset();

            foreach (var audioModifer in AppliedAudioModifiers)
            {
                audioModifer.ApplyToAudioSource(this);
            }

            _audioModifiersChanged = false;
        }

        private IEnumerator AudioModifierApplier()
        {
            while (true)
            {
                yield return null;

                if (_audioModifiersChanged)
                {
                    ReapplyAllAudioModifierDefinitions();
                }
            }
        }
    }
}
