﻿using System.Collections.Generic;

namespace Game.Core.Audio
{
    /// <summary>
    /// An <see cref="IAudioSource"/> that can have <see cref="IAudioModifierDefinition"/>s applied to it.
    /// </summary>
    public interface IModifiableAudioSource : IAudioSource
    {
        public interface IAudioEffect
        {
            /// <summary>
            /// Is the audio effect enabled
            /// </summary>
            bool Enabled { get; set; }
        }

        /// <summary>
        /// Interface for modifying the volume of an audio source
        /// </summary>
        public interface IVolumeScale : IAudioEffect
        {
            /// <summary>
            /// The amount to scale the volume of the audio source
            /// </summary>
            float VolumeScale { get; set; }

            /// <summary>
            /// The volume of the audio source before being scaled.
            /// Should only be set implementations of <see cref="IAudioSource"/>
            /// </summary>
            float VolumeBase { get; set; }
        }

        /// <summary>
        /// Interface for modifying the pitch of an audio source
        /// </summary>
        public interface IPitchScale : IAudioEffect
        {
            /// <summary>
            /// The amount to scale the pitch by with 1 being the original pitch.
            /// </summary>
            float PitchScale { get; set; }
        }

        /// <summary>
        /// Interface for low pass filtering on an audio source
        /// </summary>
        public interface ILowPassFilter : IAudioEffect
        {
            /// <summary>
            /// The cutoff frequency
            /// </summary>
            float CutoffFrequency { get; set; }
        }

        /// <summary>
        /// The volume scale for the audio source
        /// </summary>
        IVolumeScale VolumeScale { get; }

        /// <summary>
        /// The pitch scale for the audio source
        /// </summary>
        IPitchScale PitchScale { get; }

        /// <summary>
        /// The low pass filter for the audio source
        /// </summary>
        ILowPassFilter LowPassFilter { get; }

        /// <summary>
        /// Adds a <see cref="IAudioModifierDefinition"/> to the audio source
        /// </summary>
        void AddAudioModifierDefinition(IAudioModifierDefinition audioModifier);

        /// <summary>
        /// Removes a <see cref="IAudioModifierDefinition"/> from the audio source
        /// </summary>
        void RemoveAudioModifierDefinition(IAudioModifierDefinition audioModifier);

        /// <summary>
        /// Removes multiple <see cref="IAudioModifierDefinition"/>s from the audio source
        /// </summary>
        void RemoveAudioModifierDefinitions(IEnumerable<IAudioModifierDefinition> audioModifier);

        /// <summary>
        /// Removes all current <see cref="IAudioModifierDefinition"/>s from the audio source
        /// </summary>
        void RemoveAllAudioModifierDefinitions();
    }
}
