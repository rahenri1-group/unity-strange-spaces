﻿using UnityEngine;

namespace Game.Core.Audio
{
    /// <inheritdoc />
    public class ModifiableAudioSourceBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private BoolReadonlyReference _playOnAwake = null;

        /// <inheritdoc />
        private void Start()
        {
            if (_playOnAwake)
            {
                PlayCurrentClip();
            }
        }
    }
}
