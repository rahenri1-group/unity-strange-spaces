﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Audio
{
    /// <summary>
    /// On enable, randomizes the pitch of a <see cref="IModifiableAudioSource"/>
    /// </summary>
    [RequireComponent(typeof(IModifiableAudioSource))]
    public class AudioPitchRandomizerBehaviour : MonoBehaviour
    {
        [SerializeField] private FloatReadonlyReference _minPitch = null;
        [SerializeField] private FloatReadonlyReference _maxPitch = null;

        private IModifiableAudioSource _audioSource;
        private PitchScaleDefinition _pitchScaleDefinition;

        private void Awake()
        {
            Assert.IsTrue(_maxPitch >= _minPitch);

            _audioSource = this.GetComponentAsserted<IModifiableAudioSource>();

            _pitchScaleDefinition = new PitchScaleDefinition
            {
                PitchScale = Random.Default.Range(_minPitch, _maxPitch)
            };
        }

        private void OnEnable()
        {
            _audioSource.AddAudioModifierDefinition(_pitchScaleDefinition);

            _audioSource.PlayClipPreStart += OnAudioSourcePreStart;
        }

        private void OnDisable()
        {
            _audioSource.RemoveAudioModifierDefinition(_pitchScaleDefinition);

            _audioSource.PlayClipPreStart -= OnAudioSourcePreStart;
        }

        private void OnAudioSourcePreStart(IAudioSource audio)
        {
            _pitchScaleDefinition.PitchScale = Random.Default.Range(_minPitch, _maxPitch);
        }
    }
}
