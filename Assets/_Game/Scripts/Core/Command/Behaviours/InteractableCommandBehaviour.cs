﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using UnityEngine;

namespace Game.Core.Command.Behaviour
{
    /// <summary>
    /// An <see cref="IInteractable"/> that executes an <see cref="ICommand"/>
    /// </summary>
    public class InteractableCommandBehaviour : InjectedBehaviour
    {
        [SerializeReference]
        [SelectType(typeof(ICommand))]
        private ICommand[] _commands = null;

        [Inject] private ICommandManager _commandManager = null;

        private IInteractable _interactable;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _interactable = this.GetComponentAsserted<IInteractable>();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _interactable.InteractBegin += OnInteractBegin;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _interactable.InteractBegin -= OnInteractBegin;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            UniTask.Create(
                async () =>
                {
                    foreach (var command in _commands)
                    {
                        await _commandManager.Execute(command);
                    }
                }
            ).Forget();
        }
    }
}
