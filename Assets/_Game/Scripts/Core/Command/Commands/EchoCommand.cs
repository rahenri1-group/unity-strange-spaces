﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to echo text to the console
    /// </summary>
    [Serializable]
    public class EchoConsoleCommand : ICommand
    {
        /// <summary>
        /// The text to echo
        /// </summary>
        public string Text;
    }

    /// <summary>
    /// Command processor for <see cref="EchoConsoleCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class EchoCommandProcessor : BaseConsoleCommandProcessor<EchoConsoleCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "echo";

        /// <inheritdoc/>
        public override string CommandDescription => "Sends the provided text to the logger";

        private ILogRouter _logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public EchoCommandProcessor(ILogRouter logger)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public override EchoConsoleCommand ParseCommand(string[] args)
        {
            return new EchoConsoleCommand
            {
                Text = string.Join(" ", args)
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(EchoConsoleCommand command)
        {
            _logger.LogInfo(command.Text);

            return UniTask.CompletedTask;
        }
    }
}
