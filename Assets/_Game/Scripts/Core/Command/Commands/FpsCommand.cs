﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to set the max fps
    /// </summary>
    [Serializable]
    public class FpsCommand : ICommand
    {
        /// <summary>
        /// The target fps
        /// </summary>
        public int TargetFps;
    }

    /// <summary>
    /// Command processor for <see cref="FpsCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class FpsCommandProcessor : BaseConsoleCommandProcessor<FpsCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "fps-set";

        /// <inheritdoc/>
        public override string CommandDescription => "Sets the max fps";

        /// <inheritdoc/>
        public override FpsCommand ParseCommand(string[] args)
        {
            if (args.Length == 0) return null;

            int targetFps = ParseInt(args[0], 60);

            return new FpsCommand
            {
                TargetFps = targetFps
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(FpsCommand command)
        {
            Application.targetFrameRate = command.TargetFps;

            QualitySettings.vSyncCount = 0;

            return UniTask.CompletedTask;
        }
    }
}
