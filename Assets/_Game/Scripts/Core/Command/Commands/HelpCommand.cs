﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to list the available commands
    /// </summary>
    [Serializable]
    public class HelpCommand : ICommand
    {
        /// <summary>
        /// Prefix to filter command by
        /// </summary>
        public string Text;
    }

    /// <summary>
    /// Command processor for <see cref="HelpCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class HelpCommandProcessor : BaseConsoleCommandProcessor<HelpCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "help";

        /// <inheritdoc/>
        public override string CommandDescription => "Lists available commands and their descriptions";

        private readonly ILogRouter _logger;
        private readonly List<IConsoleCommandProcessor> _consoleCommands;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public HelpCommandProcessor(
            IConsoleCommandProcessor[] consoleCommandProcessors,
            ILogRouter logger)
        {
            _logger = logger;

            _consoleCommands = new List<IConsoleCommandProcessor>();

            _consoleCommands.AddRange(consoleCommandProcessors);
            _consoleCommands.Add(this);
        }

        /// <inheritdoc/>
        public override HelpCommand ParseCommand(string[] args)
        {
            return new HelpCommand
            {
                Text = args.Length > 0 ? args[0] : string.Empty
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(HelpCommand command)
        {
            var helpResults = _consoleCommands
                .Where(c => string.IsNullOrEmpty(command.Text) || c.CommandName.StartsWith(command.Text))
                .OrderBy(c => c.CommandName);

            foreach (var c in helpResults)
            {
                _logger.LogInfo($"{c.CommandName}: {c.CommandDescription}");
            }

            return UniTask.CompletedTask;
        }
    }
}