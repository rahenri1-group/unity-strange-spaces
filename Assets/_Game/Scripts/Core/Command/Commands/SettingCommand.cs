﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Reflection;
using Game.Core.Storage.Settings;
using System;
using System.Linq;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to set an individual setting
    /// </summary>
    [Serializable]
    public class SettingCommand : ICommand
    {
        /// <summary>
        /// The name of the setting's group
        /// </summary>
        public string SettingGroupName;

        /// <summary>
        /// The name of the setting
        /// </summary>
        public string SettingName;

        /// <summary>
        /// The new setting value
        /// </summary>
        public string SettingValue;
    }

    /// <summary>
    /// Command processor for <see cref="SettingCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class SettingCommandProcessor : BaseConsoleCommandProcessor<SettingCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "setting";

        /// <inheritdoc/>
        public override string CommandDescription => "Changes an individual setting";

        private readonly ILogRouter _logger;
        private readonly ISettingsManager _settingsManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public SettingCommandProcessor(
            ILogRouter logger,
            ISettingsManager settingsManager)
        {
            _logger = logger;
            _settingsManager = settingsManager;
        }

        /// <inheritdoc/>
        public override SettingCommand ParseCommand(string[] args)
        {
            if (args.Length < 2) return null;

            var settingId = args[0];

            int index = settingId.IndexOf('.');
            if (index < 1) return null;

            string groupName = settingId.Substring(0, index);
            string settingName = settingId.Substring(index + 1);

            if (groupName.Length == 0 || settingName.Length == 0) return null;

            return new SettingCommand
            {
                SettingGroupName = groupName,
                SettingName = settingName,
                SettingValue = args[1]
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(SettingCommand command)
        {
            var settingsGroup = _settingsManager.ActiveProfile.GetSettingsGroupByName(command.SettingGroupName);
            if (settingsGroup == null)
            {
                _logger.LogError($"Unknown settings group {command.SettingGroupName}");
                return UniTask.CompletedTask;
            }

            var propertyInfo = settingsGroup
                .GetType()
                .GetPropertiesCached()
                .FirstOrDefault(m => m.GetCustomAttributesCached().OfType<SettingAttribute>().Any() && m.Name.ToLower() == command.SettingName.ToLower());
            if (propertyInfo == null)
            {
                _logger.LogError($"Unknown setting {command.SettingName}");
                return UniTask.CompletedTask;
            }

            if (propertyInfo.PropertyType == typeof(string))
            {
                propertyInfo.SetValue(settingsGroup, command.SettingValue);
            }
            else if (propertyInfo.PropertyType == typeof(bool))
            {
                if (TryParseBool(command.SettingValue, out bool parsedBool))
                    propertyInfo.SetValue(settingsGroup, parsedBool);
                else
                    _logger.LogError($"Invalid value for {command.SettingName}");
            }
            else if (propertyInfo.PropertyType == typeof(int))
            {
                if (int.TryParse(command.SettingValue, out int parsedInt))
                    propertyInfo.SetValue(settingsGroup, parsedInt);
                else
                    _logger.LogError($"Invalid value for {command.SettingName}");
            }
            else if (propertyInfo.PropertyType == typeof(float))
            {
                if (float.TryParse(command.SettingValue, out float parsedFloat))
                    propertyInfo.SetValue(settingsGroup, parsedFloat);
                else
                    _logger.LogError($"Invalid value for {command.SettingName}");
            }
            else
            {
                _logger.LogError($"Unsupported type for {command.SettingName}");
            }

            return UniTask.CompletedTask;
        }
    }
}
