﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Reflection;
using Game.Core.Storage.Settings;
using System;
using System.Linq;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to get the current settings profile
    /// </summary>
    [Serializable]
    public class SettingsProfileGetCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="SettingsProfileGetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class SettingsProfileGetCommandProcessor : BaseConsoleCommandProcessor<SettingsProfileGetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "settings-profile-get";

        /// <inheritdoc/>
        public override string CommandDescription => "Prints the current settings profile";

        private readonly ILogRouter _logger;
        private readonly ISettingsManager _settingsManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public SettingsProfileGetCommandProcessor(ILogRouter logger, ISettingsManager settingsManager)
        {
            _logger = logger;
            _settingsManager = settingsManager;
        }

        /// <inheritdoc/>
        public override UniTask Execute(SettingsProfileGetCommand command)
        {
            var settingsInfo = $"Profile: {_settingsManager.ActiveProfile.ProfileName}";
            foreach (var settingsGroup in _settingsManager.ActiveProfile.SettingsGroups)
            {
                var propertyInfos = settingsGroup
                    .GetType()
                    .GetPropertiesCached()
                    .Where(m => m.GetCustomAttributesCached().OfType<SettingAttribute>().Any());

                foreach (var prop in propertyInfos)
                {
                    settingsInfo += $"\n{settingsGroup.Name}.{prop.Name} = {prop.GetValue(settingsGroup)}";
                }
            }

            _logger.LogInfo(settingsInfo);

            return UniTask.CompletedTask;
        }
    }
}
