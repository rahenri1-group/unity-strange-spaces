﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Storage.Settings;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to set the current settings profile
    /// </summary>
    [Serializable]
    public class SettingsProfileSetCommand : ICommand
    {
        /// <summary>
        /// The settings profile to change to
        /// </summary>
        public string SettingsProfile;
    }

    /// <summary>
    /// Command processor for <see cref="SettingsProfileSetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class SettingsProfileSetCommandProcessor : BaseConsoleCommandProcessor<SettingsProfileSetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "settings-profile-set";

        /// <inheritdoc/>
        public override string CommandDescription => "Changes the current settings profile";

        private readonly ISettingsManager _settingsManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public SettingsProfileSetCommandProcessor(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        /// <inheritdoc/>
        public override SettingsProfileSetCommand ParseCommand(string[] args)
        {
            if (args.Length < 1) return null;

            return new SettingsProfileSetCommand
            {
                SettingsProfile = args[0]
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(SettingsProfileSetCommand command)
        {
            _settingsManager.ChangeProfile(command.SettingsProfile);

            return UniTask.CompletedTask;
        }
    }
}
