﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to shutdown the application
    /// </summary>
    [Serializable]
    public class ShutdownCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="ShutdownCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class ShutdownCommandProcessor : BaseConsoleCommandProcessor<ShutdownCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "shutdown";

        /// <inheritdoc/>
        public override string CommandDescription => "Shuts down the application";

        /// <inheritdoc/>
        public override UniTask Execute(ShutdownCommand command)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif

            return UniTask.CompletedTask;
        }
    }
}
