﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Render.Camera;
using Game.Core.Resource;
using Game.Core.Space;
using System;
using UnityEngine;

namespace Game.Core.Command
{
    /// <summary>
    /// Config for <see cref="SpawnEntityCommandProcessor"/>
    /// </summary>
    [Serializable]
    public class SpawnEntityCommandProcessorConfig
    {
        public string[] IgnoreLayers = new string[0];

        /// <summary>
        /// The minimum clear distance required to spawn
        /// </summary>
        public float MinClearDistance = 0f;

        /// <summary>
        /// The max distance from the main camera to attempt to spawn
        /// </summary>
        public float MaxSpawnDistance = 3f;

        /// <summary>
        /// Padding to use around entity bounds when checking
        /// </summary>
        public float SpawnBoundsPadding = 0f;
    }

    /// <summary>
    /// Command to spawn a new entity
    /// </summary>
    [Serializable]
    public class SpawnEntityCommand : ICommand
    {
        public string AssetKey;
    }

    /// <summary>
    /// Command processor for <see cref="SpawnEntityCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class SpawnEntityCommandProcessor : BaseConsoleCommandProcessor<SpawnEntityCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "spawn";

        /// <inheritdoc/>
        public override string CommandDescription => "Spawns a new entity";

        public SpawnEntityCommandProcessorConfig Config = new SpawnEntityCommandProcessorConfig();

        private readonly ICameraManager _cameraManager;
        private readonly IDynamicEntityManager _entityManager;
        private readonly ILogRouter _logger;
        private readonly IResourceMetadataManager _metadataManager;
        private readonly ISpaceManager _spaceManager;
        private readonly ISpacePhysics _spacePhysics;

        private int _spawnCheckLayerMask = 0x0;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public SpawnEntityCommandProcessor(
            ICameraManager cameraManager,
            IDynamicEntityManager entityManager,
            ILogRouter logger,
            IResourceMetadataManager metadataManager,
            ISpaceManager spaceManager,
            ISpacePhysics spacePhysics)
        {
            _cameraManager = cameraManager;
            _entityManager = entityManager;
            _logger = logger;
            _metadataManager = metadataManager;
            _spaceManager = spaceManager;
            _spacePhysics = spacePhysics;
        }

        /// <inheritdoc/>
        public override SpawnEntityCommand ParseCommand(string[] args)
        {
            if (args.Length == 0) return null;

            string assetKey = args[0];

            if (_entityManager.IsValidEntityAssetKey(assetKey))
            {
                return new SpawnEntityCommand
                {
                    AssetKey = assetKey
                };
            }
            else
            {
                _logger.LogWarning($"Unknown entity asset '{assetKey}'");
                return null;
            }
        }

        /// <inheritdoc/>
        public override async UniTask Execute(SpawnEntityCommand command)
        {
            if (_spawnCheckLayerMask == 0x0)
            {
                _spawnCheckLayerMask = ~LayerUtil.ConstructMaskForLayers(Config.IgnoreLayers);
            }

            var activeCameraTransform = _cameraManager.ActivePrimaryCamera.GameObject.transform;
            var entityMetadata = _metadataManager.GetResourceMetadataComponent<IDynamicEntityMetadata>(command.AssetKey);

            var space = _spaceManager.ActiveSpace;
            var castDirection = activeCameraTransform.forward;
            var castCenter = activeCameraTransform.position + (entityMetadata != null ? entityMetadata.MeshBounds.center : Vector3.zero);
            
            var castExtends = new Vector3(Config.SpawnBoundsPadding, Config.SpawnBoundsPadding, Config.SpawnBoundsPadding) 
                + (entityMetadata != null ? entityMetadata.MeshBounds.extents : 0.5f * Vector3.one);

            bool hit = _spacePhysics.BoxCast(space, castCenter, castExtends, castDirection, out var resultInfo, Quaternion.identity, Config.MaxSpawnDistance, QueryPortalInteraction.CastThroughTransporter, _spawnCheckLayerMask);

            if (hit && resultInfo.Distance < Config.MinClearDistance)
            {
                _logger.LogWarning($"Insuffient space to spawn {command.AssetKey}");
            }
            else
            {
                await _entityManager.SpawnEntity(
                    command.AssetKey,
                    resultInfo.Space,
                    resultInfo.CastShapePosition,
                    resultInfo.CastShapeRotation);
            }
        }
    }
}
