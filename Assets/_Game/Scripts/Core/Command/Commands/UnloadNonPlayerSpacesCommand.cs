﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to unload all spaces that are not active
    /// </summary>
    [Serializable]
    public class UnloadNonActiveSpacesCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="UnloadNonActiveSpacesCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class UnloadNonActiveSpacesCommandProcessor : BaseConsoleCommandProcessor<UnloadNonActiveSpacesCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "unload-nonactive-spaces";

        /// <inheritdoc/>
        public override string CommandDescription => "Unloads all non active spaces";

        private ISpaceLoader _spaceLoader = null;
        private ISpaceManager _spaceManager = null;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public UnloadNonActiveSpacesCommandProcessor(
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager)
        {
            _spaceLoader = spaceLoader;
            _spaceManager = spaceManager;
        }

        /// <inheritdoc/>
        public override async UniTask Execute(UnloadNonActiveSpacesCommand command)
        {
            foreach (var space in _spaceLoader.LoadedSpaces)
            {
                if (SpaceUtil.SpaceEquals(_spaceManager.ActiveSpace, space))
                {
                    continue;
                }

                await _spaceLoader.UnloadSpace(space);
            }
        }
    }
}
