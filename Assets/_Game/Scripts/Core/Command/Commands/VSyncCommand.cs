﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core.Command
{
    /// <summary>
    /// A command to toggle vsync
    /// </summary>
    [Serializable]
    public class VSyncCommand : ICommand
    {
        /// <summary>
        /// Should vsync be enabled
        /// </summary>
        public bool VSyncEnabled;
    }

    /// <summary>
    /// Command processor for <see cref="VSyncCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VSyncCommandProcessor : BaseConsoleCommandProcessor<VSyncCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "vsync";

        /// <inheritdoc/>
        public override string CommandDescription => "Enables/Disables vsync";

        /// <inheritdoc/>
        public override VSyncCommand ParseCommand(string[] args)
        {
            bool vSyncEnabled = true;

            if (args.Length > 0)
            {
                vSyncEnabled = ParseBool(args[0], true);
            }

            return new VSyncCommand
            {
                VSyncEnabled = vSyncEnabled
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(VSyncCommand command)
        {
            QualitySettings.vSyncCount = command.VSyncEnabled ? 1 : 0;

            return UniTask.CompletedTask;
        }
    }
}