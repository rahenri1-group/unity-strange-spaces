﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to get the value of a variable
    /// </summary>
    [Serializable]
    public class VariableGetCommand : ICommand
    {
        /// <summary>
        /// The variable name
        /// </summary>
        public string VariableName;
    }

    /// <summary>
    /// Command processor for <see cref="VariableGetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VariableGetCommandProcessor : BaseConsoleCommandProcessor<VariableGetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "var-get";

        /// <inheritdoc/>
        public override string CommandDescription => "Gets the value of a variable";

        private ILogRouter _logger;
        private IVariableRegistry _variableRegistry;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VariableGetCommandProcessor(
            ILogRouter logger,
            IVariableRegistry variableRegistry)
        {
            _logger = logger;
            _variableRegistry = variableRegistry;
        }

        /// <inheritdoc/>
        public override VariableGetCommand ParseCommand(string[] args)
        {
            if (args.Length > 0)
            {
                return new VariableGetCommand
                {
                    VariableName = args[0]
                };
            }

            return null;
        }

        /// <inheritdoc/>
        public override UniTask Execute(VariableGetCommand command)
        {
            var variable = _variableRegistry.GetVariableByName(command.VariableName);
            if (variable == null)
            {
                _logger.LogWarning($"Unknown variable '{command.VariableName}'");
                return UniTask.CompletedTask;
            }

            _logger.LogInfo($"{variable.Name}: {variable.StringValue}");

            return UniTask.CompletedTask;
        }
    }
}
