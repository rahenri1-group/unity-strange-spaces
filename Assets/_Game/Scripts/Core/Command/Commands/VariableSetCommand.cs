﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to set the value of a variable
    /// </summary>
    [Serializable]
    public class VariableSetCommand : ICommand
    {
        /// <summary>
        /// The variable name
        /// </summary>
        public string VariableName;

        /// <summary>
        /// The new variable value
        /// </summary>
        public string VariableValue;
    }

    /// <summary>
    /// Command processor for <see cref="VariableSetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VariableSetCommandProcessor : BaseConsoleCommandProcessor<VariableSetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "var-set";

        /// <inheritdoc/>
        public override string CommandDescription => "Sets the value of a variable";

        private ILogRouter _logger;
        private IVariableRegistry _variableRegistry;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VariableSetCommandProcessor(
            ILogRouter logger,
            IVariableRegistry variableRegistry)
        {
            _logger = logger;
            _variableRegistry = variableRegistry;
        }

        /// <inheritdoc/>
        public override VariableSetCommand ParseCommand(string[] args)
        {
            if (args.Length > 1)
            {
                return new VariableSetCommand
                {
                    VariableName = args[0],
                    VariableValue = args[1]
                };
            }

            return null;
        }

        /// <inheritdoc/>
        public override UniTask Execute(VariableSetCommand command)
        {
            var variable = _variableRegistry.GetVariableByName(command.VariableName);

            if (variable == null)
            {
                _logger.LogWarning($"Unknown variable '{command.VariableName}'");
                return UniTask.CompletedTask;
            }
            
            if (variable.VariableType == typeof(bool))
            {
                var boolVariable = (IVariable<bool>)variable;
                boolVariable.Value = ParseBool(command.VariableValue, boolVariable.Value);
            }
            else if (variable.VariableType == typeof(float))
            {
                var floatVariable = (IVariable<float>)variable;
                floatVariable.Value = ParseFloat(command.VariableValue, floatVariable.Value);
            }
            else if (variable.VariableType == typeof(int))
            {
                var intVariable = (IVariable<int>)variable;
                intVariable.Value = ParseInt(command.VariableValue, intVariable.Value);
            }
            else if (variable.VariableType == typeof(string))
            {
                var stringVariable = (IVariable<string>)variable;
                stringVariable.Value = command.VariableValue;
            }

            return UniTask.CompletedTask;
        }
    }
}
