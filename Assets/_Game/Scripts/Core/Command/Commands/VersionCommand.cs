﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to retrieve the current version of the build
    /// </summary>
    [Serializable]
    public class VersionCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="VersionCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VersionCommandProcessor : BaseConsoleCommandProcessor<VersionCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "version";

        /// <inheritdoc/>
        public override string CommandDescription => "Prints the application version";

        private ILogRouter _logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VersionCommandProcessor(ILogRouter logger)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public override UniTask Execute(VersionCommand command)
        {
            _logger.LogInfo($"{Application.productName} v{Application.version}");

            return UniTask.CompletedTask;
        }
    }
}
