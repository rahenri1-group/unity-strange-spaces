﻿using Cysharp.Threading.Tasks;
using Game.Core.Audio;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to get the volume of an audio group
    /// </summary>
    [Serializable]
    public class VolumeGetCommand : ICommand
    {
        /// <summary>
        /// The audio group
        /// </summary>
        public AudioGroupType? AudioGroup;
    }

    /// <summary>
    /// Command processor for <see cref="VolumeGetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VolumeGetCommandProcessor : BaseConsoleCommandProcessor<VolumeGetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "volume-get";

        /// <inheritdoc/>
        public override string CommandDescription => "Gets the volume of a volume group";

        private readonly IAudioMixer _audioMixer;
        private readonly ILogRouter _logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VolumeGetCommandProcessor(
            IAudioMixer audioMixer,
            ILogRouter logger)
        {
            _audioMixer = audioMixer;
            _logger = logger;
        }

        /// <inheritdoc/>
        public override VolumeGetCommand ParseCommand(string[] args)
        {
            if (args.Length == 0)
            {
                return new VolumeGetCommand
                {
                    AudioGroup = null
                };
            }

            try
            {
                AudioGroupType groupType = (AudioGroupType)Enum.Parse(typeof(AudioGroupType), args[0], true);

                return new VolumeGetCommand
                {
                    AudioGroup = groupType
                };
            }
            catch (ArgumentException)
            {
                _logger.LogWarning(_audioMixer.ModuleName, $"Unknown volume group '{args[0]}'");
            }

            return null;
        }

        /// <inheritdoc/>
        public override UniTask Execute(VolumeGetCommand command)
        {
            if (command.AudioGroup == null)
            {
                _logger.LogInfo(_audioMixer.ModuleName,
                    "\n" +
                    $"Master {_audioMixer.MasterVolume}\n" +
                    $"Music {_audioMixer.MusicVolume}\n" +
                    $"Effects {_audioMixer.EffectsVolume}");
            }
            else
            {
                switch (command.AudioGroup.Value)
                {
                    case AudioGroupType.Master:
                        _logger.LogInfo(_audioMixer.ModuleName, $"Master {_audioMixer.MasterVolume}");
                        break;
                    case AudioGroupType.Music:
                        _logger.LogInfo(_audioMixer.ModuleName, $"Music {_audioMixer.MusicVolume}");
                        break;
                    case AudioGroupType.Effects:
                        _logger.LogInfo(_audioMixer.ModuleName, $"Effects {_audioMixer.EffectsVolume}");
                        break;
                    default:
                        _logger.LogError(_audioMixer.ModuleName, $"Unknown audio group '{command.AudioGroup.Value}'");
                        break;
                }
            }

            return UniTask.CompletedTask;
        }
    }
}
