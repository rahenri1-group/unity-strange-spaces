﻿using Cysharp.Threading.Tasks;
using Game.Core.Audio;
using Game.Core.DependencyInjection;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Command to set the volume of an audio group
    /// </summary>
    [Serializable]
    public class VolumeSetCommand : ICommand
    {
        /// <summary>
        /// The audio group
        /// </summary>
        public AudioGroupType AudioGroup;

        /// <summary>
        /// The new volume
        /// </summary>
        public float Volume;
    }

    /// <summary>
    /// Command processor for <see cref="VolumeSetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class VolumeSetCommandProcessor : BaseConsoleCommandProcessor<VolumeSetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "volume-set";

        /// <inheritdoc/>
        public override string CommandDescription => "Sets the volume of a volume group";

        private readonly IAudioMixer _audioMixer;
        private readonly ILogRouter _logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VolumeSetCommandProcessor(
            IAudioMixer audioMixer,
            ILogRouter logger)
        {
            _audioMixer = audioMixer;
            _logger = logger;
        }

        /// <inheritdoc/>
        public override VolumeSetCommand ParseCommand(string[] args)
        {
            if (args.Length < 2) return null;

            AudioGroupType groupType = AudioGroupType.Master;

            try
            {
                groupType = (AudioGroupType)Enum.Parse(typeof(AudioGroupType), args[0], true);
            }
            catch (ArgumentException)
            {
                _logger.LogWarning(_audioMixer.ModuleName, $"Unknown volume group '{args[0]}'");
                return null;
            }

            if (!float.TryParse(args[1], out float volume))
            {
                _logger.LogWarning(_audioMixer.ModuleName, $"Invalid volume '{args[1]}'");
                return null;
            }

            return new VolumeSetCommand
            {
                AudioGroup = groupType,
                Volume = volume
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(VolumeSetCommand command)
        {
            switch (command.AudioGroup)
            {
                case AudioGroupType.Master:
                    _audioMixer.MasterVolume = command.Volume;
                    break;
                case AudioGroupType.Music:
                    _audioMixer.MusicVolume = command.Volume;
                    break;
                case AudioGroupType.Effects:
                    _audioMixer.EffectsVolume = command.Volume;
                    break;
                default:
                    _logger.LogError(_audioMixer.ModuleName, $"Unknown audio group '{command.AudioGroup}'");
                    break;
            }

            return UniTask.CompletedTask;
        }
    }
}
