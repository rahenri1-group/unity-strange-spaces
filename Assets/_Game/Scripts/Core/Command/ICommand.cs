﻿namespace Game.Core.Command
{
    /// <summary>
    /// Inteface for an executable command
    /// </summary>
    public interface ICommand { }
}
