﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Game.Core.Command
{
    /// <inheritdoc cref="ICommandManager"/>
    [Dependency(
        contract: typeof(ICommandManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class CommandManager : BaseModule, ICommandManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Command Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private Dictionary<string, IConsoleCommandProcessor> _commandNameProcessorMap;
        private Dictionary<Type, ICommandProcessor> _commandTypeProcessorMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public CommandManager(
            ICommandProcessor[] commandProcessors,
            ILogRouter logger)
            : base(logger)
        {
            _commandNameProcessorMap = new Dictionary<string, IConsoleCommandProcessor>();
            _commandTypeProcessorMap = new Dictionary<Type, ICommandProcessor>();

            foreach (var processor in commandProcessors)
            {
                _commandTypeProcessorMap[processor.CommandType] = processor;

                var consoleProcessor = (IConsoleCommandProcessor) processor;
                if (consoleProcessor != null)
                {
                    _commandNameProcessorMap[consoleProcessor.CommandName.ToLower()] = consoleProcessor;
                }
            }
        }

        /// <inheritdoc/>
        public override UniTask Initialize()
        {
            foreach (var processor in _commandTypeProcessorMap.Values)
            {
                processor.Initialize();
            }

            return base.Initialize();
        }

        /// <inheritdoc/>
        public UniTask Execute(ICommand command)
        {
            var commandType = command.GetType();

            if (!_commandTypeProcessorMap.ContainsKey(commandType))
            {
                ModuleLogWarning($"Unknown command type '{commandType.Name}'");

                return UniTask.CompletedTask;
            }

            return _commandTypeProcessorMap[commandType].Execute(command);
        }

        /// <inheritdoc/>
        public UniTask Execute(string commandText)
        {
            //string[] cmdSplit = commandText.Split(new char[1]{ ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string[] cmdSplit = 
                (
                    from Match m 
                    in Regex.Matches(commandText, @"([\w-/]+)|(""[\w\s-/]+"")|('[\w\s-/]+')")
                    select m.Value
                        .Replace("'", string.Empty)
                        .Replace("\"", string.Empty)
                )
                .ToArray();

            var cmdName = cmdSplit[0].ToLower();

            if (!_commandNameProcessorMap.ContainsKey(cmdName))
            {
                ModuleLogWarning($"Unknown command '{cmdName}'");

                return UniTask.CompletedTask;
            }

            var cmdProcessor = _commandNameProcessorMap[cmdName];
            
            string[] commandArgs = new string[0];
            if (cmdSplit.Length > 1)
            {
                commandArgs = cmdSplit.SubArray(1);
            }            

            var command = cmdProcessor.Parse(commandArgs);
            if (command != null)
            {
                return cmdProcessor.Execute(command);
            }
            else
            {
                ModuleLogWarning($"Invalid args for command '{cmdName}'");
            }

            return UniTask.CompletedTask;
        }
    }
}
