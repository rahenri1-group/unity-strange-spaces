﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Command
{
    /// <summary>
    /// Module that executes <see cref="ICommand"/>s and can parse text into <see cref="ICommand"/>
    /// </summary>
    public interface ICommandManager : IModule
    {
        /// <summary>
        /// Executes the <see cref="ICommand"/> using the matching <see cref="ICommandProcessor"/>
        /// </summary>
        UniTask Execute(ICommand command);

        /// <summary>
        /// Parses the <paramref name="command"/> into an <see cref="ICommand"/> and executes it using the matching <see cref="ICommandProcessor"/>
        /// </summary>
        UniTask Execute(string command);
    }
}
