﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Command
{
    /// <inheritdoc cref="ICommandProcessor"/>
    public abstract class BaseCommandProcessor<T> : ICommandProcessor<T>
        where T : ICommand, new()
    {
        /// <inheritdoc/>
        public Type CommandType => typeof(T);

        /// <inheritdoc/>
        public virtual void Initialize() { }

        /// <inheritdoc/>
        public UniTask Execute(ICommand commandArg)
        {
            return Execute((T)commandArg);
        }

        /// <inheritdoc/>
        public abstract UniTask Execute(T command);
    }
}
