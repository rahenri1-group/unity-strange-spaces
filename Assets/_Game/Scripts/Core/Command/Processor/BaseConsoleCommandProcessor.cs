﻿namespace Game.Core.Command
{
    /// <inheritdoc cref="IConsoleCommandProcessor"/>
    public abstract class BaseConsoleCommandProcessor<T> : BaseCommandProcessor<T>, IConsoleCommandProcessor<T>
        where T : ICommand, new()
    {
        /// <inheritdoc />
        public abstract string CommandName { get; }
        /// <inheritdoc/>
        public abstract string CommandDescription { get; }

        /// <inheritdoc/>
        public ICommand Parse(string[] args)
        {
            return ParseCommand(args);
        }

        /// <inheritdoc/>
        public virtual T ParseCommand(string[] args)
        {
            return new T();
        }

        protected bool ParseBool(string value, bool defaultValue)
        {
            if (TryParseBool(value, out bool parsedBool))
            {
                return parsedBool;
            }

            return defaultValue;
        }

        protected bool TryParseBool(string value, out bool parsedBool)
        {
            parsedBool = false;

            if (bool.TryParse(value, out parsedBool))
            {
                return true;
            }

            if (int.TryParse(value, out int parsedInt))
            {
                parsedBool = parsedInt > 0;
                return true;
            }

            return false;
        }

        protected int ParseInt(string value, int defaultValue)
        {
            if (int.TryParse(value, out int parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        protected float ParseFloat(string value, float defaultValue)
        {
            if (float.TryParse(value, out float parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }
    }
}
