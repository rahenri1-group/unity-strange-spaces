﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Command
{
    /// <summary>
    /// Interface for a processor of a command
    /// </summary>
    public interface ICommandProcessor
    {
        /// <summary>
        /// The command type that this can process
        /// </summary>
        Type CommandType { get; }

        /// <summary>
        /// Initializes the command processor
        /// </summary>
        void Initialize();

        /// <summary>
        /// Executes the command
        /// </summary>
        UniTask Execute(ICommand command);
    }

    public interface ICommandProcessor<T> : ICommandProcessor
        where T: ICommand
    {
        /// <summary>
        /// Executes the command
        /// </summary>
        UniTask Execute(T command);
    }
}
