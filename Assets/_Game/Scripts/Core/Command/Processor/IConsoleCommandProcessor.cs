﻿namespace Game.Core.Command
{
    /// <summary>
    /// Interface for a <see cref="ICommand"/> that can be executed by a console
    /// </summary>
    public interface IConsoleCommandProcessor : ICommandProcessor
    {
        /// <summary>
        /// The name of the command
        /// </summary>
        string CommandName { get; }
        /// <summary>
        /// The description of the command
        /// </summary>
        string CommandDescription { get; }

        /// <summary>
        /// Parses the provided args into a structured command object
        /// </summary>
        ICommand Parse(string[] args);
    }

    /// <summary>
    /// Generic version of <see cref="IConsoleCommandProcessor"/>
    /// </summary>
    public interface IConsoleCommandProcessor<T> : IConsoleCommandProcessor
        where T : ICommand
    {
        /// <summary>
        /// Parses the provided args into a structured command object
        /// </summary>
        T ParseCommand(string[] args);
    }
}
