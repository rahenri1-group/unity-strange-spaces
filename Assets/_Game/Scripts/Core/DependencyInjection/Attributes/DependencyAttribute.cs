﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Marks a class as a candidate for dependency injection
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class DependencyAttribute : Attribute
    {
        /// <summary>
        /// The contract that the class can fulfill
        /// </summary>
        public Type Contract { get; private set; }

        /// <summary>
        /// The lifetime of the instance
        /// </summary>
        public Lifetime Lifetime { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DependencyAttribute(Type contract, Lifetime lifetime = Lifetime.Singleton)
        {
            Contract = contract;
            Lifetime = lifetime;
        }
    }
}
