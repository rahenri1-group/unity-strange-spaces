﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Attribute indicating the property should be fulfilled by dependency injection. 
    /// Only works in <see cref="InjectedBehaviour"/>, <see cref="InjectedScriptableObject"/> based classes.
    /// All other classes must manually call <see cref="IInjectionContainer.Inject(object)"/>
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Constructor |
        AttributeTargets.Field | 
        AttributeTargets.Method |
        AttributeTargets.Property,
        AllowMultiple = false,
        Inherited = false
    )]
    public class InjectAttribute : Attribute { }
}
