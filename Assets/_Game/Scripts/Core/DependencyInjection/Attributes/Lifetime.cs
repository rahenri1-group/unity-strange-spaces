﻿namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// The lifetime of an object instantiated by dependency injection
    /// </summary>
    public enum Lifetime
    {
        /// <summary>
        /// There is only one instance of the object
        /// </summary>
        Singleton,

        /// <summary>
        /// Each injected object injected is unique
        /// </summary>
        Transient
    }
}
