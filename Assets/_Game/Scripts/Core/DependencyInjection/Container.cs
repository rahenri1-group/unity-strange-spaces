using Game.Core.Reflection;
using Game.Core.Serialization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Game.Core.DependencyInjection
{
    /// <inheritdoc cref="IInjectionContainer"/>
    public class Container : IInjectionContainer
    {
        private readonly ConcurrentBag<Registration> _registrations;

        private IJsonDeserializer _jsonDeserializer;

        /// <summary>
        /// Constructor
        /// </summary>
        public Container()
        {
            _registrations = new ConcurrentBag<Registration>();
        }

        #region Registration

        /// <inheritdoc/>
        public Registration[] RegisterJson(string jsonText)
        {
            return RegisterJson(new string[] { jsonText });
        }

        /// <inheritdoc/>
        public Registration[] RegisterJson(string[] jsonTexts)
        {
            if (_jsonDeserializer == null)
            {
                _jsonDeserializer = Resolve<IJsonDeserializer>();
            }

            var assemblies = Discovery.FindDependentAssemblies(typeof(DependencyAttribute).Assembly);

            var availableTypes = assemblies.SelectMany(a => Discovery.FindAttributedTypes<DependencyAttribute>(a)).ToArray();

            var combinedJson = new Dictionary<string, string>();

            foreach (var jsonText in jsonTexts)
            {
                combinedJson.AddOverwrite(_jsonDeserializer.KeyValues(jsonText));
            }

            var missingTypes = combinedJson
                .Where(configPair => !availableTypes.Any(t => t.FullName == configPair.Key))
                .ToArray();

            if (missingTypes.Length > 0)
            {
                throw new DependencyException($"Configured types not found:\n{string.Join("\n", missingTypes)}");
            }

            var registrations = new List<RegistrationConfiguration>();
            foreach (var jsonPair in combinedJson)
            {
                var type = availableTypes.First(t => t.FullName == jsonPair.Key);

                registrations.Add(new RegistrationConfiguration
                {
                    Type = type,
                    JsonConfig = jsonPair.Value
                });
            }

            return Register(registrations.ToArray());
        }

        /// <inheritdoc/>
        public Registration[] Register(RegistrationConfiguration[] registrationConfigurations)
        {
            var registrations = new List<Registration>();
            foreach (var configuration in registrationConfigurations)
            {
                foreach (var registration in RegisterType(configuration.Type))
                {
                    // add interceptor the can apply the config
                    registration.AddInterceptor((contract, instance) =>
                    {
                        _jsonDeserializer.PopulateObject(instance, configuration.JsonConfig);
                        return instance;
                    });

                    registrations.Add(registration);
                }
            }

            return registrations.ToArray();
        }

        /// <inheritdoc/>
        public Registration[] RegisterType(Type configuredType)
        {
            var attributes = configuredType
                .GetCustomAttributesCached()
                .OfType<DependencyAttribute>()
                .GroupBy(a => a.Lifetime);

            var registrations = new List<Registration>();
            foreach (var group in attributes)
            {
                registrations.Add(RegisterType(configuredType, group.Key, group.ToArray()));
            }
            return registrations.ToArray();
        }

        private Registration RegisterType(Type configuredType, Lifetime lifetime, DependencyAttribute[] dependencyAttribute)
        {
            var registration = new Registration(() => Instantiate(configuredType), lifetime);

            foreach (var attr in dependencyAttribute)
            {
                registration.AddContract(attr.Contract);
            }

            _registrations.Add(registration);

            return registration;
        }

        /// <inheritdoc/>
        public Registration RegisterSingleton(object obj)
        {
            var dependencyAttributes = obj.GetType()
                .GetCustomAttributesCached()
                .OfType<DependencyAttribute>()
                .Where(a => a.Lifetime == Lifetime.Singleton);

            var registration = new Registration(() => obj, Lifetime.Singleton);

            foreach (var attr in dependencyAttributes)
            {
                registration.AddContract(attr.Contract);
            }

            _registrations.Add(registration);

            return registration;
        }

        #endregion

        #region Resolving

        /// <inheritdoc/>
        public TContract Resolve<TContract>() where TContract : class
        {
            return (TContract)Resolve(typeof(TContract));
        }

        /// <inheritdoc/>
        public object Resolve(Type contract)
        {
            try
            {
                if (contract.IsArray)
                {
                    return ResolveFromArray(contract);
                }

                var registration = GetRegistrationByContract(contract).FirstOrDefault();
                if (registration != null)
                {
                    return ResolveFromRegistration(contract, registration);
                }

                return Instantiate(contract);

            }
            catch (Exception e)
            {
                throw new DependencyException($"Failed to resolve: {contract.FullName}", e);
            }
        }

        private object ResolveFromRegistration(Type contract, Registration registration)
        {
            return registration.Get(contract);
        }

        private Array ResolveFromArray(Type arrayType)
        {
            var contract = arrayType.GetElementType();

            var instances = GetRegistrationByContract(contract)
                .Select(r => r.Get(contract))
                .ToArray();

            var array = Array.CreateInstance(contract, instances.Length);
            instances.CopyTo(array, 0);

            return array;
        }

        private IEnumerable<Registration> GetRegistrationByContract(Type contract)
        {
            return _registrations.Where(r => r.Contracts.Contains(contract));
        }

        #endregion

        #region Instantiation

        private object Instantiate(Type type)
        {
            if (!type.IsClass || type.IsAbstract || type.IsSubclassOf(typeof(Delegate)))
            {
                throw new DependencyException($"Invalid type for injection: {type.FullName}");
            }

            var constructors = type.GetConstructorsCached().Where(c => !c.IsStatic).ToArray();
            var markedConstructors = constructors.Where(c => c.GetCustomAttributes().OfType<InjectAttribute>().Any()).ToArray();
            var selectedConstructor = (markedConstructors.Any() ? markedConstructors : constructors)
                .OrderBy(constructor => constructor.IsPublic ? 0 : 1)
                .ThenBy(constructor => constructor.GetParametersCached().Length)
                .FirstOrDefault();

            if (selectedConstructor == null)
                return null;

            var args = selectedConstructor.GetParametersCached()
                .Select(p => Resolve(p.ParameterType))
                .ToArray();

            try
            {
                var instance = selectedConstructor.Invoke(args);
                Inject(instance);
                return instance;
            }
            catch
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public void Inject(object injectee)
        {
            InjectValues(injectee);
            InjectMethods(injectee);
        }

        private void InjectValues(object injectee)
        {
            var values = injectee
                .GetType()
                .GetMembersCached()
                .Where(m => m.GetCustomAttributesCached().OfType<InjectAttribute>().Any())
                .ToArray();

            foreach (var prop in values.OfType<PropertyInfo>())
            {
                prop.SetValue(injectee, Resolve(prop.PropertyType));
            }

            foreach (var field in values.OfType<FieldInfo>())
            {
                field.SetValue(injectee, Resolve(field.FieldType));
            }
        }

        private void InjectMethods(object injectee)
        {
            var methods = injectee
                .GetType()
                .GetMethodsCached()
                .Where(m => m.GetCustomAttributesCached().OfType<InjectAttribute>().Any());

            foreach (var method in methods)
            {
                var args = method
                    .GetParameters()
                    .Select(p => Resolve(p.ParameterType))
                    .ToArray();

                method.Invoke(injectee, args);
            }
        }

        #endregion
    }
}
