﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Exception type for dependency injection
    /// </summary>
    public class DependencyException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DependencyException(string message, Exception inner = null)
            : base(message, inner) { }
    }
}
