﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Injection container
    /// </summary>
    public interface IInjectionContainer
    {
        /// <summary>
        /// Registers all <see cref="Type"/>s in a json string
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        Registration[] RegisterJson(string jsonText);

        /// <summary>
        /// Registers all <see cref="Type"/>s in an arry of json strings
        /// </summary>
        /// <param name="jsonTexts"></param>
        /// <returns></returns>
        Registration[] RegisterJson(string[] jsonTexts);

        /// <summary>
        /// Registers all provided <paramref name="registrationConfigurations"/>
        /// </summary>
        /// <param name="registrationConfigurations"></param>
        /// <returns></returns>
        Registration[] Register(RegistrationConfiguration[] registrationConfigurations);

        /// <summary>
        /// Registers a type
        /// </summary>
        /// <param name="configuredType"></param>
        /// <returns></returns>
        Registration[] RegisterType(Type configuredType);

        /// <summary>
        /// Registers a pre-created singleton
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Registration RegisterSingleton(object obj);

        /// <summary>
        /// Returns an instance(s) that match the provided <typeparamref name="TContract"/>
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <returns></returns>
        TContract Resolve<TContract>() where TContract : class;

        /// <summary>
        /// Returns an instance(s) that match the provided <paramref name="contract"/>
        /// </summary>
        /// <param name="contract"></param>
        /// <returns></returns>
        object Resolve(Type contract);

        /// <summary>
        /// Revolves all properties and methods that have the <see cref="InjectAttribute"/>
        /// </summary>
        /// <param name="injectee"></param>
        void Inject(object injectee);
    }
}
