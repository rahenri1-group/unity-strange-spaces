﻿using UnityEngine;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Scriptable object that contains a list of json texts for injection
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Injection/Config")]
    public class InjectionConfig : ScriptableObject
    {
        /// <summary>
        /// The JSON text assets to be injected into a <see cref="IInjectionContainer"/>
        /// </summary>
        public TextAsset[] InjectionJsonTexts => _injectionJsonTexts;

        [SerializeField] private TextAsset[] _injectionJsonTexts = new TextAsset[0];
    }
}
