﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Delegate for when a new instance is created for a contract 
    /// </summary>
    /// <param name="contract"></param>
    /// <param name="instance"></param>
    /// <returns></returns>
    public delegate object Interceptor(Type contract, object instance);
}
