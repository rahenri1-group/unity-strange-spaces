﻿using System;
using System.Collections.Concurrent;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// A registration for a class
    /// </summary>
    public class Registration
    {
        /// <summary>
        /// The contracts this class can fulfill
        /// </summary>
        public ConcurrentBag<Type> Contracts { get; private set; } = new ConcurrentBag<Type>();

        private readonly ConcurrentBag<Interceptor> _interceptors; // generally just the config
        private readonly Lifetime _lifetime;
        private readonly Func<Type, object> _factory;
        private object _instance;

        /// <summary>
        /// Constructor
        /// </summary>
        public Registration(Func<object> factory, Lifetime lifetime)
        {
            _interceptors = new ConcurrentBag<Interceptor>();

            _instance = null;

            _lifetime = lifetime;
            _factory = contract =>
            {
                var instance = factory();
                
                foreach (var interceptor in _interceptors)
                {
                    instance = interceptor(contract, instance);
                }

                return instance;
            };
        }

        /// <summary>
        /// Adds a contract
        /// </summary>
        /// <param name="contract"></param>
        public void AddContract(Type contract)
        {
            Contracts.Add(contract);
        }

        /// <summary>
        /// Adds an interceptor that will be called when an instance of the registered class is instantiated
        /// </summary>
        /// <param name="interceptor"></param>
        public void AddInterceptor(Interceptor interceptor)
        {
            _interceptors.Add(interceptor);
        }

        /// <summary>
        /// Gets an object that matches the contract
        /// </summary>
        /// <param name="contract"></param>
        /// <returns></returns>
        public object Get(Type contract)
        {
            if (_lifetime == Lifetime.Singleton)
            {
                if (_instance == null) _instance = _factory(contract);

                return _instance;
            }
            else
            {
                return _factory(contract);
            }
        }
    }
}
