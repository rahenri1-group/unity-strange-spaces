﻿using System;

namespace Game.Core.DependencyInjection
{
    /// <summary>
    /// Configuration for a registered type and the values to be injected into it when instantiated
    /// </summary>
    public class RegistrationConfiguration
    {
        /// <summary>
        /// The type of the registration
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// The json config to apply to the object when instantiated
        /// </summary>
        public string JsonConfig { get; set; }
    }
}
