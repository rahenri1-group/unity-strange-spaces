﻿using System;

namespace Game.Core.Effect
{
    /// <summary>
    /// Interface for an effect. Effects trigger, do something, and then are destroyed / recycled
    /// </summary>
    public interface IEffect
    {
        /// <summary>
        /// Event called when the effect has completed
        /// </summary>
        event Action EffectCompleted;
    }
}
