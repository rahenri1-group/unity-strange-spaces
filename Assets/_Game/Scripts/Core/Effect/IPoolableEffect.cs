﻿using Game.Core.Resource;

namespace Game.Core.Effect
{
    /// <summary>
    /// Interface for a poolable effect. Effects trigger, do something, and then return themselves to the object pool
    /// </summary>
    public interface IPoolableEffect : IEffect, IPoolableComponent { }
}