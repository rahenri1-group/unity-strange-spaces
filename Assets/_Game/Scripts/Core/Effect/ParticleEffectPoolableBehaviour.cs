﻿using Game.Core.DependencyInjection;
using Game.Core.Resource;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Effect
{
    /// <summary>
    /// Effect that triggers a particle system
    /// </summary>
    public class ParticleEffectPoolableBehaviour : InjectedBehaviour, IPoolableEffect
    {
        /// <inheritdoc/>
        public event Action EffectCompleted;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;

        [SerializeField] private ParticleSystem _particleSystem = null;

        private IEnumerator _effectCompleteCoroutine = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_particleSystem);
        }

        /// <inheritdoc/>
        public void OnAllocate()
        {
            _particleSystem.Play();

            _effectCompleteCoroutine = EffectComplete();
            StartCoroutine(_effectCompleteCoroutine);
        }

        /// <inheritdoc/>
        public void OnDispose()
        {
            if (_effectCompleteCoroutine != null)
            {
                StopCoroutine(_effectCompleteCoroutine);
                _effectCompleteCoroutine = null;
            }
        }

        private IEnumerator EffectComplete()
        {
            yield return new WaitForSeconds(_particleSystem.main.duration + 0.1f);

            if (EffectCompleted != null)
            {
                EffectCompleted.Invoke();
                foreach (Delegate d in EffectCompleted.GetInvocationList())
                {
                    EffectCompleted -= (Action)d;
                }
            }

            _effectCompleteCoroutine = null;

            _poolableGameObjectManager.ReturnPooledObject(gameObject);
        }
    }
}
