﻿using Game.Core.Audio;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Core.Effect
{
    /// <summary>
    /// Effect that plays a random audio clip
    /// </summary>
    public class RandomAudioEffectPoolableBehaviour : PoolableAudioSourceBehaviour, IPoolableEffect
    {
        /// <inheritdoc/>
        public event Action EffectCompleted;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;

        [SerializeField] private RandomAudioClipCollection _audioClips = null;

        private IEnumerator _effectCompleteCoroutine = null;

        /// <inheritdoc/>
        public override void OnAllocate()
        {
            base.OnAllocate();

            PlayClip(_audioClips.GetNextAudioClip());

            _effectCompleteCoroutine = EffectComplete();
            StartCoroutine(_effectCompleteCoroutine);
        }

        /// <inheritdoc/>
        public override void OnDispose()
        {
            base.OnDispose();

            if (_effectCompleteCoroutine != null)
            {
                StopCoroutine(_effectCompleteCoroutine);
                _effectCompleteCoroutine = null;
            }
        }

        private IEnumerator EffectComplete()
        {
            yield return new WaitForSeconds(UnityAudio.clip.length + 0.1f);

            if (EffectCompleted != null)
            {
                EffectCompleted.Invoke();
                foreach (Delegate d in EffectCompleted.GetInvocationList())
                {
                    EffectCompleted -= (Action)d;
                }
            }

            _effectCompleteCoroutine = null;

            _poolableGameObjectManager.ReturnPooledObject(gameObject);
        }        
    }
}
