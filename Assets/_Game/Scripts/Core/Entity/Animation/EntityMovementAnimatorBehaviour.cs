﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Entity
{
    /// <summary>
    /// Component that controls the movement animations for a <see cref="IEntity"/>
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class EntityMovementAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(IEntityMovement))] private Component _entityMovementObj = null;
        private IEntityMovement _entityMovement;

        [SerializeField] private StringReadonlyReference _forwardSpeedAnimatorParameter = null;
        [SerializeField] private StringReadonlyReference _turnAngleAnimatorParameter = null;

        // The speed the forward animation was made for, if the entity is moving faster, the animator will have its speed increased.
        [SerializeField] private FloatReadonlyReference _forwardSpeed = null;
        [SerializeField] private FloatReadonlyReference _maxTurnAngle = null;
        [SerializeField] private FloatReadonlyReference _turnSmoothingDuration = null;

        private Animator _entityAnimator;

        private int _forwardSpeedId;
        private int _turnAngleId;

        private Quaternion _previousRotation;

        private void Awake()
        {
            Assert.IsTrue(!string.IsNullOrEmpty(_forwardSpeedAnimatorParameter));
            Assert.IsTrue(!string.IsNullOrEmpty(_turnAngleAnimatorParameter));

            Assert.IsTrue(_forwardSpeed > 0f);
            Assert.IsTrue(_maxTurnAngle > 0f);
            Assert.IsTrue(_turnSmoothingDuration >= 0f);

            _entityAnimator = GetComponent<Animator>();

            Assert.IsNotNull(_entityMovementObj);
            _entityMovement = _entityMovementObj.GetComponentAsserted<IEntityMovement>();

            _forwardSpeedId = Animator.StringToHash(_forwardSpeedAnimatorParameter);
            _turnAngleId = Animator.StringToHash(_turnAngleAnimatorParameter);
        }

        private void OnEnable()
        {
            _previousRotation = _entityMovement.Rotation;

            _entityMovement.EntityTeleported += OnEntityTeleported;
        }

        private void OnDisable()
        {
            _entityMovement.EntityTeleported -= OnEntityTeleported;
        }

        private void OnEntityTeleported()
        {
            _previousRotation = _entityMovement.Rotation;
        }

        private void LateUpdate()
        {
            var rotationDegrees = Math.MathUtil.NormalizeAngle180((_entityMovement.Rotation * Quaternion.Inverse(_previousRotation)).eulerAngles.y);           
            _entityAnimator.SetFloat(_turnAngleId, Mathf.Clamp(rotationDegrees, -_maxTurnAngle, _maxTurnAngle) / _maxTurnAngle);

            if (_turnSmoothingDuration > 0)
            {
                _previousRotation = Quaternion.Slerp(_previousRotation, _entityMovement.Rotation, Time.deltaTime / _turnSmoothingDuration);
            }
            else
            {
                _previousRotation = _entityMovement.Rotation;
            }

            var groundSpeed = new Vector2(_entityMovement.Velocity.x, _entityMovement.Velocity.z).magnitude;
            _entityAnimator.SetFloat(_forwardSpeedId, Mathf.Clamp01(groundSpeed / _forwardSpeed));
            _entityAnimator.speed = Mathf.Max(1f, groundSpeed / _forwardSpeed);
        }
    }
}
