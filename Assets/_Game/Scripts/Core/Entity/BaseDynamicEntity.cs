﻿using Game.Core.Event;
using System;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IDynamicEntity"/>
    public abstract class BaseDynamicEntity<T> : BaseEntity<T>, IDynamicEntity
        where T : EntitySerializedData, new()
    {
        /// <inheritdoc/>
        public IEntityMovement EntityMovement
        {
            get
            {
                if (_moveComponent == null) _moveComponent = this.GetComponentAsserted<IEntityMovement>();
                return _moveComponent;
            }
        }
        private IEntityMovement _moveComponent = null;

        /// <inheritdoc/>
        public bool IsInitializationComplete { get; private set; } = false;

        /// <inheritdoc/>
        public virtual bool DestroyIfOutOfBounds => true;

        /// <inheritdoc/>
        public event Action InitializationComplete;

        /// <inheritdoc/>
        public virtual void InitializeNew(Guid id)
        {
            EntityId = id;

            IsInitializationComplete = true;

            EventBus.InvokeEvent(new EntityCreateEvent
            {
                Entity = this,
                EntityGameObject = gameObject
            });

            InitializationComplete?.Invoke();
        }

         /// <inheritdoc/>
        public override void InitializeFromCache(Guid id, string serializedJson)
        {
            base.InitializeFromCache(id, serializedJson);

            IsInitializationComplete = true;

            EventBus.InvokeEvent(new EntityCreateEvent
            {
                Entity = this,
                EntityGameObject = gameObject
            });

            InitializationComplete?.Invoke();
        }

#if UNITY_EDITOR
        public void InitializeFromEditorCache(Guid id, string serializedJson)
        {
            base.InitializeFromCache(id, serializedJson);
        }
#endif
    }
}
