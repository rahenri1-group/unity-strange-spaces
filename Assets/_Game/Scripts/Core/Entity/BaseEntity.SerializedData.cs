﻿using Game.Core.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Entity
{
    public abstract partial class BaseEntity<T> : InjectedBehaviour
    {
        protected void DeserializeFromJson(string serializedJson)
        {
            // currently only supported in runtime mode
            if (!Application.isPlaying) return;

            if (string.IsNullOrEmpty(serializedJson)) return;

            var data = JsonDeserializer.Deserialize<T>(serializedJson);

            InitFromSerializedObject(data);
        }

        protected virtual void InitFromSerializedObject(T data)
        {
            var allSerializedComponents = new HashSet<ISerializableComponent>(GetComponentsInChildren<ISerializableComponent>(true));

            if (data.ChildObjectMap != null)
            {
                foreach (var pair in data.ChildObjectMap)
                {
                    var childObject = transform.Find(pair.Key)?.gameObject;
                    if (childObject == null)
                    {
                        Debug.LogWarning($"Unknown child path {pair.Key} in {gameObject.name}");
                        continue;
                    }

                    var objectData = pair.Value;

                    var components = childObject.GetComponents<ISerializableComponent>();
                    foreach (var component in components)
                    {
                        allSerializedComponents.Remove(component);

                        var componentType = component.GetType();

                        if (component.SerializeComponent && objectData.ComponentJsonMap.ContainsKey(componentType))
                        {
                            component.DeserializeFromJson(objectData.ComponentJsonMap[componentType]);
                        }
                    }

                    childObject.SetActive(pair.Value.Enabled);
                }
            }

            // initialize remaining
            foreach (var component in allSerializedComponents)
            {
                component.DeserializeFromJson(string.Empty);
            }
        }

        /// <inheritdoc/>
        public string SerializeToJson()
        {
            // currently only supported in runtime mode
            if (!Application.isPlaying) return string.Empty;

            var data = new T();
            FillSerializedObject(data);

            return JsonSerializer.Serialize(data);
        }

        protected virtual void FillSerializedObject(T data)
        {
            foreach (var component in GetComponentsInChildren<ISerializableComponent>(true))
            {
                if (component.SerializeComponent)
                {
                    var path = component.GameObject.GameObjectRelativePath(gameObject);

                    if (!data.ChildObjectMap.ContainsKey(path))
                    {
                        data.ChildObjectMap[path] = new EntitySerializedChildObjectData
                        {
                            Enabled = component.GameObject.activeSelf,
                            ComponentJsonMap = new Dictionary<Type, string>()
                        };
                    }

                    data.ChildObjectMap[path].ComponentJsonMap[component.GetType()] = component.SerializeToJson();
                }
            }
        }
    }
}
