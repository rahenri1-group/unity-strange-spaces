﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Serialization;
using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IEntity"/>
    public abstract partial class BaseEntity<T> : InjectedBehaviour, IEntity
        where T : EntitySerializedData, new()
    {
        [Inject] protected IEventBus EventBus;
        [Inject] protected IJsonDeserializer JsonDeserializer;
        [Inject] protected IJsonSerializer JsonSerializer;

        [SerializeField] [ReadOnly] protected string _entityId = string.Empty;

        /// <inheritdoc/>
        public Guid EntityId
        {
            get
            {
                if (_parsedEntityId == null) _parsedEntityId = !string.IsNullOrEmpty(_entityId) ? Guid.Parse(_entityId) : Guid.Empty;

                return _parsedEntityId.Value;
            }
            protected set
            {
                _parsedEntityId = value;
                _entityId = value.ToString();
            }
        }
        private Guid? _parsedEntityId = null;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        protected virtual void OnDestroy()
        {
            EventBus.InvokeEvent(new EntityDestroyEvent
            {
                EntityId = EntityId
            });
        }

        /// <inheritdoc/>
        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditorUtil.GenerateGuidForBehaviour(this, _entityId, out _entityId);
            }
#endif
        }

        /// <inheritdoc/>
        public virtual void InitializeFromCache(Guid id, string serializedJson)
        {
            _parsedEntityId = id;
            _entityId = id.ToString();

            DeserializeFromJson(serializedJson);
        }
    }
}
