﻿using Game.Core.DependencyInjection;
using Game.Core.Event;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IStaticEntity"/>
    public abstract class BaseStaticEntity<T> : BaseEntity<T>, IStaticEntity
        where T : EntitySerializedData, new()
    {
        [Inject] protected IStaticEntityManager StaticEntityManager;

        /// <inheritdoc/>
        private void Start()
        {
            StaticEntityManager.OnStaticEntityLoad(this);

            EventBus.InvokeEvent(new EntityCreateEvent
            {
                Entity = this,
                EntityGameObject = gameObject
            });
        }
    }
}
