﻿using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IDynamicEntityMetadata"/>
    [Serializable]
    public class DynamicEntityMetadata : IDynamicEntityMetadata
    {
        /// <inheritdoc />
        public bool FadeInOnSpawn => _fadeInOnSpawn;

        /// <inheritdoc />
        public Bounds MeshBounds
        {
            get => new Bounds(_meshBoundsCenter, _meshBoundsSize);
            set
            {
                _meshBoundsCenter = value.center;
                _meshBoundsSize = value.size;
            }
        }

        [SerializeField] private bool _fadeInOnSpawn = false;
        [SerializeField] [ReadOnly] private Vector3 _meshBoundsCenter = Vector3.zero;
        [SerializeField] [ReadOnly] private Vector3 _meshBoundsSize = Vector3.one;
    }
}
