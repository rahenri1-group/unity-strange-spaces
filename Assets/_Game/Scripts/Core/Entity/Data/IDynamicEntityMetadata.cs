﻿using Game.Core.Resource;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Metadata for <see cref="IDynamicEntity"/>
    /// </summary>
    public interface IDynamicEntityMetadata : IResourceMetadataComponent
    {
        /// <summary>
        /// Should the entity use a fade effect when spawned
        /// </summary>
        bool FadeInOnSpawn { get; }

        /// <summary>
        /// The default bounds of all meshes for the entity. 
        /// </summary>
        Bounds MeshBounds { get; }
    }
}
