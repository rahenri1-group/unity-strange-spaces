﻿using Cysharp.Threading.Tasks;
using Game.Core.Space;
using System;
using System.Linq;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Extensions for <see cref="IDynamicEntityManager"/>
    /// </summary>
    public static class DynamicEntityManagerExtensions
    {
        /// <summary>
        /// Spawns a new entity and creates a new record for it. If the provided space is not loaded, the entity will not be spawned in.
        /// </summary>
        public static UniTask<IDynamicEntity> SpawnEntity(this IDynamicEntityManager dynamicEntityManager, Guid id, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
        {
            return dynamicEntityManager.SpawnEntity(id, assetKey, space, position, rotation, Vector3.one, string.Empty); 
        }

        /// <summary>
        /// Spawns a new entity and creates a new record for it. If the provided space is not loaded, the entity will not be spawned in.
        /// </summary>
        public static UniTask<IDynamicEntity> SpawnEntity(this IDynamicEntityManager dynamicEntityManager, Guid id, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            var name = assetKey.Split('/').Last() + "_" + id.ToString();

            return dynamicEntityManager.SpawnEntity(id, name, assetKey, space, position, rotation, scale, json);
        }

        /// <summary>
        /// Spawns a new entity and creates a new record for it. If the provided space is not loaded, the entity will not be spawned in.
        /// </summary>
        public static UniTask<IDynamicEntity> SpawnEntity(this IDynamicEntityManager dynamicEntityManager, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
        {
            var id = Guid.NewGuid();
            var name = assetKey.Split('/').Last() + "_" + id.ToString();

            return dynamicEntityManager.SpawnEntity(id, name, assetKey, space, position, rotation, Vector3.one, string.Empty);
        }

        /// <summary>
        /// Spawns a new entity and creates a new record for it. If the provided space is not loaded, the entity will not be spawned in.
        /// </summary>
        public static UniTask<IDynamicEntity> SpawnEntity(this IDynamicEntityManager dynamicEntityManager, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            var id = Guid.NewGuid();
            return dynamicEntityManager.SpawnEntity(id, name, assetKey, space, position, rotation, scale, json);
        }

        /// <summary>
        /// Creates a new record for an entity, using the provided information.
        /// </summary>
        public static void CreateDynamicEntityRecord(this IDynamicEntityManager dynamicEntityManager, Guid id, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
        {
            var name = assetKey.Split('/').Last() + "_" + id.ToString();

            dynamicEntityManager.CreateDynamicEntityRecord(id, name, assetKey, space, position, rotation, Vector3.one, string.Empty);
        }

        /// <summary>
        /// Creates a new record for an entity, using the provided information.
        /// </summary>
        public static void CreateDynamicEntityRecord(this IDynamicEntityManager dynamicEntityManager, Guid id, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            var name = assetKey.Split('/').Last() + "_" + id.ToString();

            dynamicEntityManager.CreateDynamicEntityRecord(id, name, assetKey, space, position, rotation, scale, json);
        }
    }
}
