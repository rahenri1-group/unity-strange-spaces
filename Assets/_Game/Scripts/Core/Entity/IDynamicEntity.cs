﻿using System;

namespace Game.Core.Entity
{
    /// <summary>
    /// Interface for a dynamic created entity. 
    /// Dynamic entities are also the only objects that can change spaces with portals.
    /// Dynamic entities should not be directly created, rather they should be registered with the <see cref="IDynamicEntityManager"/>
    /// </summary>
    public interface IDynamicEntity : IEntity
    {
        /// <summary>
        /// The movement component of the entity.
        /// </summary>
        IEntityMovement EntityMovement { get; }

        /// <summary>
        /// Flag to indicate that initialization has completed for the entity. 
        /// This means that either <see cref="InitializeNew()"/> or <see cref="InitializeFromEditorCache(Guid, string)"></see> has completed.
        /// </summary>
        bool IsInitializationComplete { get; }

        /// <summary>
        /// Should the <see cref="IDynamicEntityManager"/> destroy this entity if it is ever out of bounds
        /// </summary>
        bool DestroyIfOutOfBounds { get; }

        /// <summary>
        /// Event raised when <see cref="IsInitializationComplete"/> is true.
        /// </summary>
        event Action InitializationComplete;

        /// <summary>
        /// Called when an new entity has been created with no backing data
        /// </summary>
        void InitializeNew(Guid id);

#if UNITY_EDITOR
        /// <summary>
        /// Called when an entity has been loaded into an editor scene with backing data
        /// </summary>
        void InitializeFromEditorCache(Guid id, string serializedJson);
#endif
    }
}
