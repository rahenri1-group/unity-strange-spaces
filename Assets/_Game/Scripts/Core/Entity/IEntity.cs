﻿using System;

namespace Game.Core.Entity
{
    /// <summary>
    /// An entity is any object that needs to persist data between the space it is in unloading and reloading.
    /// </summary>
    public interface IEntity : IGameObjectComponent
    {
        /// <summary>
        /// The unique id of the entity
        /// </summary>
        Guid EntityId { get; }

        /// <summary>
        /// Called to initialize an entity with data that was stored previously. 
        /// This could be because the entity's space was unloaded or the entity was assigned additional data when created in editor tooling
        /// </summary>
        void InitializeFromCache(Guid id, string serializedJson);

        /// <summary>
        /// Serializes the entity to json
        /// </summary>
        string SerializeToJson();
    }
}
