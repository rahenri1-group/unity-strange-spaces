﻿namespace Game.Core.Entity
{
    /// <summary>
    /// An entity that is baked into a scene. 
    /// A static entity can not be moved or destroyed.
    /// </summary>
    public interface IStaticEntity : IEntity { }
}
