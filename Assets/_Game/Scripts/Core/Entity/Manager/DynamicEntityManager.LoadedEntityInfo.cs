﻿using Game.Core.Space;

namespace Game.Core.Entity
{
    public partial class DynamicEntityManager : BaseModule
    {
        private class LoadedEntityInfo
        {
            public IDynamicEntity Entity { get; private set; }

            public ISpaceData EntitySpace { get; set; }

            public LoadedEntityInfo(IDynamicEntity entity, ISpaceData entitySpace)
            {
                Entity = entity;
                EntitySpace = entitySpace;
            }
        }
    }
}
