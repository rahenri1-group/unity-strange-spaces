﻿using Game.Core.Space;
using System;
using System.Linq;
using UnityEngine;

namespace Game.Core.Entity
{
    public partial class DynamicEntityManager : BaseModule
    {
        /// <inheritdoc cref="IDynamicEntityRecord"/>
        private class PersistentEntityRecord : IDynamicEntityRecord
        {
            /// <inheritdoc />
            public Guid Id { get; private set; }

            public string Name { get; private set; }
            public string AssetKey { get; private set; }

            /// <inheritdoc />
            public ISpaceData EntitySpace { get; protected set; }
            /// <inheritdoc />
            public Vector3 Position { get; private set; }
            /// <inheritdoc />
            public Quaternion Rotation { get; private set; }
            /// <inheritdoc />
            public Vector3 Scale { get; private set; }

            /// <inheritdoc />
            public string SerializedJson { get; set; }

            /// <inheritdoc />
            public bool IsDestroyed { get; set; }

            public PersistentEntityRecord(
                Guid entityId,
                string name,
                string assetKey,
                ISpaceData entitySpace,
                Vector3 position,
                Quaternion rotation,
                Vector3 scale)
            {
                Id = entityId;
                Name = name;
                AssetKey = assetKey;

                EntitySpace = entitySpace;
                Position = position;
                Rotation = rotation;
                Scale = scale;

                SerializedJson = string.Empty;

                IsDestroyed = false;
            }

            public void UpdateTransform(ISpaceData entitySpace, Vector3 position, Quaternion rotation, Vector3 scale)
            {
                EntitySpace = entitySpace;
                Position = position;
                Rotation = rotation;
                Scale = scale;
            }
        }

        /// <inheritdoc />
        public bool HasRecordForDynamicEntity(Guid id)
        {
            return _persistentDynamicEntityRecords.Any(record => record.Id == id);
        }

        /// <inheritdoc />
        public IDynamicEntityRecord GetRecordForDynamicEntity(Guid id)
        {
            return _persistentDynamicEntityRecords.FirstOrDefault(record => record.Id == id);
        }

        /// <inheritdoc />
        public bool IsDynamicEntityLoaded(Guid id)
        {
            return _loadedDynamicEntities.Any(e => e.Entity.EntityId == id);
        }

        /// <inheritdoc />
        public IDynamicEntity GetLoadedDynamicEntity(Guid id)
        {
            var entityInfo = _loadedDynamicEntities.FirstOrDefault(e => e.Entity.EntityId == id);
            if (entityInfo != null)
            {
                return entityInfo.Entity;
            }

            return null;
        }

        /// <inheritdoc />
        public void CreateDynamicEntityRecord(Guid id, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            CreateDynamicEntityRecordInternal(id, name, assetKey, space, position, rotation, scale, json);
        }

        private PersistentEntityRecord CreateDynamicEntityRecordInternal(Guid id, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            if (HasRecordForDynamicEntity(id))
            {
                ModuleLogError($"Record already exists for '{id}'");
                return null;
            }

            if (!IsValidEntityAssetKey(assetKey))
            {
                ModuleLogError($"No entity asset key '{assetKey}'");
                return null;
            }

            var persistentRecord = new PersistentEntityRecord(
                id,
                name,
                assetKey,
                space,
                position,
                rotation,
                scale);

            persistentRecord.SerializedJson = json;

            _persistentDynamicEntityRecords.Add(persistentRecord);

            return persistentRecord;
        }

        /// <inheritdoc />
        public void RemoveDynamicEntityRecord(Guid id)
        {
            if (!HasRecordForDynamicEntity(id))
            {
                ModuleLogWarning($"No entity record for id {id}. No removal done.");
            }

            _persistentDynamicEntityRecords.RemoveWhere(r => r.Id == id);
        }

        /// <inheritdoc />
        public void RemoveDynamicEntityRecordsForSpace(ISpaceData space)
        {
            _persistentDynamicEntityRecords.RemoveWhere(r => SpaceUtil.SpaceEquals(r.EntitySpace, space));
        }

        /// <inheritdoc />
        public bool IsValidEntityAssetKey(string assetKey)
        {
            return _validEntityAddresses.Contains(assetKey);
        }
    }
}
