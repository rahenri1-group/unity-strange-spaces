﻿using Cysharp.Threading.Tasks;
using Game.Core.Serialization;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Game.Core.Entity
{
    public partial class DynamicEntityManager
    {
        private async UniTask RestrictEntityPositionChecker(CancellationToken cancellationToken)
        {
            int delayDurationMillis = (int) (1000f / Config.EntityBoundsCheckFrequency);

            var entityBounds = new Bounds(Vector3.zero, Config.EntityAllowedBoundSize.ToUnityVector3());

            var entitiesToDestroy = new HashSet<IDynamicEntity>();

            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Delay(delayDurationMillis, delayTiming: PlayerLoopTiming.PreLateUpdate, cancellationToken: cancellationToken);

                if (!_isProcessingForSpace)
                {
                    foreach (var entityInfo in _loadedDynamicEntities)
                    {
                        var entity = entityInfo.Entity;
                        if (entity.DestroyIfOutOfBounds && entityBounds.Contains(entity.EntityMovement.Position) == false)
                        {
                            entitiesToDestroy.Add(entity);
                        }
                    }

                    foreach (var entity in entitiesToDestroy)
                    {
                        DestroyEntity(entity);
                    }

                    entitiesToDestroy.Clear();
                }
            }
        }
        
    }
}