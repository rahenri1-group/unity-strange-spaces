﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Render;
using Game.Core.Resource;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IDynamicEntityManager" />
    [Dependency(
        contract: typeof(IDynamicEntityManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class DynamicEntityManager : BaseModule, IDynamicEntityManager, ISpaceLoadProcessor, ISpaceUnloadProcessor
    {
        /// <inheritdoc />
        public override string ModuleName => "Dynamic Entity Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public DynamicEntityManagerConfig Config = new DynamicEntityManagerConfig();

        private readonly IEventBus _eventBus;
        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IResourceMetadataManager _metadataManager;
        private readonly ISpaceLoader _spaceLoader;

        private HashSet<string> _validEntityAddresses;

        private HashSet<LoadedEntityInfo> _loadedDynamicEntities;
        private HashSet<PersistentEntityRecord> _persistentDynamicEntityRecords;

        private bool _isProcessingForSpace;

        private CancellationTokenSource _entityBoundsCheckCts;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public DynamicEntityManager(
            IEventBus eventBus,
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            IResourceMetadataManager metadataManager,
            ISpaceLoader spaceLoader)
            : base(logger)
        {
            _eventBus = eventBus;
            _gameObjectResourceManager = gameObjectResourceManager;
            _metadataManager = metadataManager;
            _spaceLoader = spaceLoader;
        }

        /// <inheritdoc />
        public override async UniTask Initialize()
        {
            _loadedDynamicEntities = new HashSet<LoadedEntityInfo>();
            _persistentDynamicEntityRecords = new HashSet<PersistentEntityRecord>();

            _isProcessingForSpace = false;

            var entityAddresses = await _gameObjectResourceManager.GetAssetKeysWithLabel(Config.EntityAssetLabel);
            _validEntityAddresses = new HashSet<string>(entityAddresses);

            if (Config.RestrictEntityPosition)
            {
                _entityBoundsCheckCts = new CancellationTokenSource();
                RestrictEntityPositionChecker(_entityBoundsCheckCts.Token).Forget();
            }


            _spaceLoader.RegisterSpaceLoadProcessor(this);
            _spaceLoader.RegisterSpaceUnloadProcessor(this);

            _eventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);

            await base.Initialize();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            _loadedDynamicEntities.Clear();
            _persistentDynamicEntityRecords.Clear();

            _spaceLoader.UnregisterSpaceLoadProcessor(this);
            _spaceLoader.UnregisterSpaceUnloadProcessor(this);

            if (_entityBoundsCheckCts != null)
            {
                _entityBoundsCheckCts.CancelAndDispose();
                _entityBoundsCheckCts = null;
            }

            _eventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);

            return base.Shutdown();
        }

        private void OnEntityTeleport(EntityPostTeleportEvent teleportEvent)
        {
            var entityInfo = _loadedDynamicEntities.FirstOrDefault(e => e.Entity == teleportEvent.Entity);
            if (entityInfo != null)
            {
                entityInfo.EntitySpace = teleportEvent.NewSpace;
            }

            UpdateDynamicEntityPersistentRecord(teleportEvent.Entity, teleportEvent.NewSpace);
        }

        /// <inheritdoc/>
        public async UniTask ProcessSpaceForLoad(IRuntimeSpaceData runtimeSpace)
        {
            _isProcessingForSpace = true;

            var space = runtimeSpace.SpaceData;

            var entitiesManifest = space.GetAdditionalData<IEntityManifest>();

            if (entitiesManifest != null)
            {
                AddEntityManifestToPersistentRecords(space, entitiesManifest);
            }

            await LoadEntitiesForSpace(space);

            _isProcessingForSpace = false;
        }

        /// <inheritdoc/>
        public UniTask ProcessSpaceForUnload(IRuntimeSpaceData runtimeSpace)
        {
            _isProcessingForSpace = true;

            var space = runtimeSpace.SpaceData;

            var entityInfos = _loadedDynamicEntities
                .Where(e => SpaceUtil.SpaceEquals(e.EntitySpace, space))
                .ToHashSet();

            foreach (var info in entityInfos)
            {
                UpdateDynamicEntityPersistentRecord(info.Entity, space);

                _loadedDynamicEntities.Remove(info);
            }

            _isProcessingForSpace = false;

            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public async UniTask<IDynamicEntity> SpawnEntity(Guid id, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            var record = CreateDynamicEntityRecordInternal(id, name, assetKey, space, position, rotation, scale, json);
            var metadata = _metadataManager.GetResourceMetadataComponent<IDynamicEntityMetadata>(assetKey);

            if (record != null && _spaceLoader.IsSpaceLoaded(space))
            {
                var entity = await LoadEntity(record);

                if (metadata.FadeInOnSpawn)
                {
                    var fadeIn = entity.AddComponent<FadeInBehaviour>();
                    fadeIn.FadeDuration = 1f;
                }

                return entity;
            }

            return null;
        }

        /// <inheritdoc/>
        public void DestroyEntity(IDynamicEntity entity)
        {
            var record = _persistentDynamicEntityRecords.FirstOrDefault(e => e.Id == entity.EntityId);

            if (record != null)
            {
                record.IsDestroyed = true;
            }

            var entityInfo = _loadedDynamicEntities.FirstOrDefault(e => e.Entity == entity);
            if (entityInfo != null)
            {
                _loadedDynamicEntities.Remove(entityInfo);
            }

            UnityEngine.Object.Destroy(entity.GameObject);
        }

        private void AddEntityManifestToPersistentRecords(ISpaceData space, IEntityManifest entitiesManifest)
        {
            foreach (var initialEntityData in entitiesManifest.Entities)
            {
                if (HasRecordForDynamicEntity(initialEntityData.Id))
                {
                    continue;
                }

                CreateDynamicEntityRecordInternal(
                    initialEntityData.Id,
                    initialEntityData.Name,
                    initialEntityData.EntityAssetKey,
                    space,
                    initialEntityData.Position,
                    initialEntityData.Rotation,
                    initialEntityData.Scale,
                    initialEntityData.SerializedJson);
            }
        }

        private async UniTask LoadEntitiesForSpace(ISpaceData space)
        {
            foreach (var entityRecord in _persistentDynamicEntityRecords.Where(entity => SpaceUtil.SpaceEquals(entity.EntitySpace, space) && !entity.IsDestroyed))
            {
                await LoadEntity(entityRecord);
            }

            return;
        }

        private async UniTask<IDynamicEntity> LoadEntity(PersistentEntityRecord entityRecord)
        {
            
            var entity = await _gameObjectResourceManager.InstantiateAsync<IDynamicEntity>(entityRecord.AssetKey, entityRecord.EntitySpace, entityRecord.Position, entityRecord.Rotation, entityRecord.Scale);
            if (entity.GameObject == null)
            {
                ModuleLogError($"Unable to load entity {entityRecord.Name}:{entityRecord.AssetKey}");
                return null;
            }

            entity.GameObject.name = "@" + entityRecord.Name;
            if (string.IsNullOrEmpty(entityRecord.SerializedJson))
            {
                entity.Component.InitializeNew(entityRecord.Id);
            }
            else
            {
                entity.Component.InitializeFromCache(entityRecord.Id, entityRecord.SerializedJson);
            }

            _loadedDynamicEntities.Add(new LoadedEntityInfo(entity.Component, entityRecord.EntitySpace));

            return entity.Component;
        }

        private void UpdateDynamicEntityPersistentRecord(IDynamicEntity entity, ISpaceData space)
        {
            var record = _persistentDynamicEntityRecords.FirstOrDefault(e => e.Id == entity.EntityId);

            if (record != null)
            {
                record.UpdateTransform(space, entity.EntityMovement.Position, entity.EntityMovement.Rotation, record.Scale);
                record.SerializedJson = entity.SerializeToJson();
            }
        }
    }
}
