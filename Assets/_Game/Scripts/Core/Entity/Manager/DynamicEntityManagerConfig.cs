﻿
using Game.Core.Serialization;

namespace Game.Core.Entity
{
    /// <summary>
    /// Config for <see cref="DynamicEntityManager"/>
    /// </summary>
    public class DynamicEntityManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The label used to identify entities
        /// </summary>
        public string EntityAssetLabel = "Entity";

        /// <summary>
        /// How long should the fade effect be applied on a spawned entity
        /// </summary>
        public float EntitySpawnFadeDuration = 1f;

        /// <summary>
        /// Should entities be restricted to a bounded box (defined by <see cref="EntityAllowedBoundSize"/>)
        /// Entities that move outside of this box will be destroyed.
        /// </summary>
        public bool RestrictEntityPosition = false;

        /// <summary>
        /// The frequency entity positions are checked if <see cref="RestrictEntityPosition"/> is enabled
        /// </summary>
        public float EntityBoundsCheckFrequency = 1f;

        /// <summary>
        /// The bounding box entities are limited to for <see cref="RestrictEntityPosition"/>. 
        /// Note: only the entity center is checked against this box, all colliders and sub-objects on the entity are ignored.
        /// </summary>
        public SerializedVector3 EntityAllowedBoundSize = new SerializedVector3(10f, 10f, 10f);
    }
}
