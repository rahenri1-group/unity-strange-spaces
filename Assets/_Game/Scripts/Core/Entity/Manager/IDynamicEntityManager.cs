﻿using Cysharp.Threading.Tasks;
using Game.Core.Space;
using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Manager for the creation and persistence of <see cref="IDynamicEntity"/>s
    /// </summary>
    public interface IDynamicEntityManager : IModule
    {
        /// <summary>
        /// Spawns a new entity and creates a new record for it. If the provided space is not loaded, the entity will not be spawned in.
        /// </summary>
        UniTask<IDynamicEntity> SpawnEntity(Guid id, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json);

        /// <summary>
        /// Creates a new record for an entity, using the provided information.
        /// Any provided <paramref name="json"/> will be provided to the created entity at initialization.
        /// </summary>
        void CreateDynamicEntityRecord(Guid id, string name, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json);

        /// <summary>
        /// Removes the entity from the gameworld and updates the record to indicate it has been destroyed.
        /// </summary>
        void DestroyEntity(IDynamicEntity entity);

        /// <summary>
        /// Removes the entity record for the given id.
        /// </summary>
        void RemoveDynamicEntityRecord(Guid id);

        /// <summary>
        /// Removes all entity records where the entity is currently in the provided space
        /// </summary>
        void RemoveDynamicEntityRecordsForSpace(ISpaceData space);

        /// <summary>
        /// Checks whether a record for a dynamic entity with the given id exists.
        /// </summary>
        bool HasRecordForDynamicEntity(Guid id);

        /// <summary>
        /// Returns the record for a dynamic entity if it exists. Returns null if one doesn't.
        /// </summary>
        IDynamicEntityRecord GetRecordForDynamicEntity(Guid id);

        /// <summary>
        /// Checks whether a dynamic entity with the given id is loaded.
        /// </summary>
        bool IsDynamicEntityLoaded(Guid id);

        /// <summary>
        /// Gets the dynamic entity with the given id. Returns null if the entity is not loaded. 
        /// </summary>
        IDynamicEntity GetLoadedDynamicEntity(Guid id);

        /// <summary>
        /// Validates that the provided asset key is for a valid entity
        /// </summary>
        bool IsValidEntityAssetKey(string assetKey);
    }
}