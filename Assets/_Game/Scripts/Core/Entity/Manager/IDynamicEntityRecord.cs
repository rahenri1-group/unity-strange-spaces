﻿using Game.Core.Space;
using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// The saved record for a <see cref="IDynamicEntity"/>
    /// </summary>
    public interface IDynamicEntityRecord
    {
        /// <summary>
        /// The id of the entity
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The space the entity resides in
        /// </summary>
        ISpaceData EntitySpace { get; }
        /// <summary>
        /// The world position of the entity
        /// </summary>
        Vector3 Position { get; }
        /// <summary>
        /// The world rotation of the entity
        /// </summary>
        Quaternion Rotation { get; }
        /// <summary>
        /// The world scale of the entity
        /// </summary>
        Vector3 Scale { get; }

        /// <summary>
        /// The any additional serialized or persistent data for the entity
        /// </summary>
        string SerializedJson { get; }

        /// <summary>
        /// Has the entity been destroyed? (a destroyed entity will not be reloaded for the space)
        /// </summary>
        bool IsDestroyed { get; }
    }
}
