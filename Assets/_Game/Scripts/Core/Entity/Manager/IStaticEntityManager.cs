﻿using Game.Core.Space;
using System;

namespace Game.Core.Entity
{
    /// <summary>
    /// Manager for the persistence of <see cref="IStaticEntity"/>s
    /// </summary>
    public interface IStaticEntityManager : IModule
    {
        /// <summary>
        /// Called by a <see cref="IStaticEntity"/> when it is loaded. 
        /// If there is a record for the entity it is applied. If not, a record is created for the entity.
        /// </summary>
        void OnStaticEntityLoad(IStaticEntity entity);

        /// <summary>
        /// Checks whether a record for a static entity with the given id exists.
        /// </summary>
        bool HasRecordForStaticEntity(Guid id);

        /// <summary>
        /// Removes the entity record for the given id.
        /// </summary>
        void RemoveStaticEntityRecord(Guid id);

        /// <summary>
        /// Removes all entity records where the entity is currently in the provided space
        /// </summary>
        void RemoveStaticEntityRecordsForSpace(ISpaceData space);
    }
}
