﻿using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Entity
{
    public partial class StaticEntityManager : BaseModule
    {
        private class PersistentEntityRecord
        {
            public Guid Id { get; private set; }

            public ISpaceData EntitySpace { get; protected set; }

            public string SerializedJson { get; set; }

            public PersistentEntityRecord(Guid entityId, ISpaceData entitySpace)
            {
                Id = entityId;
                EntitySpace = entitySpace;
                SerializedJson = string.Empty;
            }
        }

        /// <inheritdoc />
        public bool HasRecordForStaticEntity(Guid id)
        {
            return _persistentStaticEntityRecords.Any(record => record.Id == id);
        }

        /// <inheritdoc />
        public void OnStaticEntityLoad(IStaticEntity entity)
        {
            var entitySpace = _spaceManager.GetEntitySpace(entity);

            if (!_loadedStaticEntities.ContainsKey(entitySpace))
            {
                _loadedStaticEntities.Add(entitySpace, new HashSet<IStaticEntity>());
            }

            _loadedStaticEntities[entitySpace].Add(entity);

            if (HasRecordForStaticEntity(entity.EntityId))
            {
                var entityData = _persistentStaticEntityRecords.First(record => record.Id == entity.EntityId);

                entity.InitializeFromCache(entityData.Id, entityData.SerializedJson);
            }
            else
            {
                var persistentRecord = new PersistentEntityRecord(
                entity.EntityId,
                entitySpace);

                persistentRecord.SerializedJson = entity.SerializeToJson();

                _persistentStaticEntityRecords.Add(persistentRecord);
            }
        }

        /// <inheritdoc/>
        public void RemoveStaticEntityRecord(Guid id)
        {
            if (!HasRecordForStaticEntity(id))
            {
                ModuleLogWarning($"No entity record for id {id}. No removal done.");
            }

            _persistentStaticEntityRecords.RemoveWhere(r => r.Id == id);
        }

        /// <inheritdoc />
        public void RemoveStaticEntityRecordsForSpace(ISpaceData space)
        {
            _persistentStaticEntityRecords.RemoveWhere(r => SpaceUtil.SpaceEquals(r.EntitySpace, space));
        }
    }
}
