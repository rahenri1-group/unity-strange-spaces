﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IStaticEntityManager" />
    [Dependency(
        contract: typeof(IStaticEntityManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class StaticEntityManager : BaseModule, IStaticEntityManager, ISpaceUnloadProcessor
    {
        /// <inheritdoc />
        public override string ModuleName => "Static Entity Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly ISpaceManager _spaceManager;
        private readonly ISpaceLoader _spaceLoader;

        private Dictionary<ISpaceData, HashSet<IStaticEntity>> _loadedStaticEntities;
        private HashSet<PersistentEntityRecord> _persistentStaticEntityRecords;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public StaticEntityManager(
            ILogRouter logger,
            ISpaceManager spaceManager,
            ISpaceLoader spaceLoader)
            : base(logger)
        {
            _spaceManager = spaceManager;
            _spaceLoader = spaceLoader;
        }

        /// <inheritdoc />
        public override UniTask Initialize()
        {
            _loadedStaticEntities = new Dictionary<ISpaceData, HashSet<IStaticEntity>>();
            _persistentStaticEntityRecords = new HashSet<PersistentEntityRecord>();

            _spaceLoader.RegisterSpaceUnloadProcessor(this);

            return base.Initialize();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            _loadedStaticEntities.Clear();
            _persistentStaticEntityRecords.Clear();

            _spaceLoader.UnregisterSpaceUnloadProcessor(this);

            return base.Shutdown();
        }

        /// <inheritdoc/>
        public UniTask ProcessSpaceForUnload(IRuntimeSpaceData runtimeSpace)
        {
            var space = runtimeSpace.SpaceData;

            if (_loadedStaticEntities.ContainsKey(space))
            {
                foreach (var entity in _loadedStaticEntities[space])
                {
                    UpdateStaticEntityPersistentRecord(entity);
                }

                _loadedStaticEntities.Remove(space);
            }

            return UniTask.CompletedTask;
        }

        private void UpdateStaticEntityPersistentRecord(IStaticEntity entity)
        {
            var record = _persistentStaticEntityRecords.FirstOrDefault(e => e.Id == entity.EntityId);

            if (record != null)
            {
                record.SerializedJson = entity.SerializeToJson();
            }
        }
    }
}
