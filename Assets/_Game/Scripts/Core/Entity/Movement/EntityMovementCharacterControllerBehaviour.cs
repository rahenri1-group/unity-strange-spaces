﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Entity
{
    /// <summary>
    /// Movement for an <see cref="IDynamicEntity"/> that uses a <see cref="UnityEngine.CharacterController"/>
    /// </summary>
    public class EntityMovementCharacterControllerBehaviour : MonoBehaviour, IEntityMovement
    {
        /// <inheritdoc/>
        public event Action EntityTeleported;

        public CharacterController CharacterController
        {
            get
            {
                if (_characterController == null) _characterController = GetComponent<CharacterController>();
                return _characterController;
            }
        }
        [SerializeField] private CharacterController _characterController = null;

        /// <inheritdoc/>
        public virtual Vector3 Position
        {
            get => CharacterController.transform.position;
            protected set => CharacterController.transform.position = value;
        }

        /// <inheritdoc/>
        public virtual Quaternion Rotation
        {
            get => CharacterController.transform.rotation;
            protected set => CharacterController.transform.rotation = value;
        }

        /// <inheritdoc/>
        public Vector3 Velocity
        {
            get => _desiredVelocity + _gravityVelocity;
            set => _desiredVelocity = value;
        }

        /// <inheritdoc/>
        public Vector3 AngularVelocity
        {
            get => Vector3.zero;
            set { }
        }

        /// <inheritdoc/>
        public bool IsGrounded => CharacterController.isGrounded;
        /// <inheritdoc/>
        public bool CanUsePortals => true;
        /// <inheritdoc/>
        public bool PortalSuckInEnabled => _portalSuckInEnabled;

        [SerializeField] private bool _portalSuckInEnabled = false;

        private Vector3 _gravityVelocity = Vector3.zero;
        private Vector3 _desiredVelocity = Vector3.zero;

        protected virtual void Awake()
        {
            Assert.IsNotNull(CharacterController);
        }

        /// <inheritdoc/>
        public void Move(Vector3 moveAmount)
        {
            _desiredVelocity = Vector3.zero;
            _gravityVelocity = Vector3.zero;

            CharacterController.Move(moveAmount);
        }

        /// <inheritdoc/>
        public void MoveTo(Vector3 newPosition)
        {
            _desiredVelocity = Vector3.zero;
            _gravityVelocity = Vector3.zero;

            var moveAmount = newPosition - Position;
            CharacterController.Move(moveAmount);
        }

        /// <inheritdoc/>
        public void Rotate(Quaternion rotationAmount)
        {
            Rotation = Rotation * rotationAmount;
        }

        /// <inheritdoc/>
        public void RotateTo(Quaternion newRotation)
        {
            Rotation = newRotation;
        }

        /// <inheritdoc/>
        public void Teleport(Vector3 position, Quaternion rotation)
        {
            CharacterController.enabled = false;

            Position = position;
            Rotation = rotation;

            _desiredVelocity = Vector3.zero;
            _gravityVelocity = Vector3.zero;

            CharacterController.enabled = true;

            EntityTeleported?.Invoke();
        }

        /// <inheritdoc/>
        protected virtual void FixedUpdate()
        {
            if (IsGrounded)
            {
                _gravityVelocity = Vector3.zero;
            }

            _gravityVelocity += UnityEngine.Physics.gravity * Time.fixedDeltaTime;

            CharacterController.Move(Velocity * Time.fixedDeltaTime);
        }
    }
}
