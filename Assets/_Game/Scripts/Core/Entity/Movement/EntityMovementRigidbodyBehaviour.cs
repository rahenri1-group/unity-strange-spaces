﻿using Game.Core.Math;
using Game.Core.Physics;
using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Movement for an <see cref="IDynamicEntity"/> that uses a <see cref="UnityEngine.Rigidbody"/>
    /// </summary>
    public class EntityMovementRigidbodyBehaviour : InjectedBehaviour, IEntityMovement, IRigidBodyMovement
    {
        /// <inheritdoc/>
        public event Action EntityTeleported;

        /// <inheritdoc/>
        public Rigidbody Rigidbody
        {
            get
            {
                if (_rigidbody == null) _rigidbody = GetComponent<Rigidbody>();
                return _rigidbody;
            }
        }
        private Rigidbody _rigidbody = null;

        /// <inheritdoc/>
        public bool IsKinematic 
        {
            get => Rigidbody.isKinematic;
            set => Rigidbody.isKinematic = value;
        }

        /// <inheritdoc/>
        public bool IsGrounded => (-0.005 <= Rigidbody.velocity.y) && (Rigidbody.velocity.y <= 0.005f);
        /// <inheritdoc/>
        public bool CanUsePortals => true;
        /// <inheritdoc/>
        public bool PortalSuckInEnabled => _portalSuckInEnabled;
        /// <inheritdoc/>
        public Vector3 Position => Rigidbody.transform.position;

        /// <inheritdoc/>
        public Quaternion Rotation => Rigidbody.transform.rotation;

        /// <inheritdoc/>
        public virtual Vector3 Velocity
        {
            get
            {
                if (!Rigidbody.isKinematic)
                    return Rigidbody.velocity;
                else
                    return _calculatedVelocity;
            }
            set => Rigidbody.velocity = value;
        }

        /// <inheritdoc/>
        public virtual Vector3 AngularVelocity
        {
            get => Rigidbody.angularVelocity;
            set => Rigidbody.angularVelocity = value;
        }

        [SerializeField] private bool _portalSuckInEnabled = false;

        private Vector3 _calculatedVelocity;

        private Vector3 _previousPosition;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _calculatedVelocity = Vector3.zero;

            _previousPosition = Position;
        }

        /// <inheritdoc/>
        protected virtual void Update()
        {
            _calculatedVelocity = (Position - _previousPosition) / Time.deltaTime;

            _previousPosition = Position;
        }


        /// <inheritdoc/>
        public virtual void Move(Vector3 moveAmount)
        {
            _rigidbody.MovePosition(Position + moveAmount);
        }

        /// <inheritdoc/>
        public virtual void MoveTo(Vector3 newPosition)
        {
            _rigidbody.MovePosition(newPosition);
        }

        /// <inheritdoc/>
        public virtual void Rotate(Quaternion rotationAmount)
        {
            _rigidbody.MoveRotation(Rotation * rotationAmount);
        }

        /// <inheritdoc/>
        public virtual void RotateTo(Quaternion newRotation)
        {
            _rigidbody.MoveRotation(newRotation);
        }

        /// <inheritdoc/>
        public virtual void Teleport(Vector3 position, Quaternion rotation)
        {
            _rigidbody.position = position;
            _rigidbody.rotation = rotation;

            // necessary to force the object to move IMMEDIATELY instead of on next physics tick
            _rigidbody.transform.position = position;
            _rigidbody.transform.rotation = rotation;

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;

            _previousPosition = position;

            _calculatedVelocity = Vector3.zero;

            EntityTeleported?.Invoke();
        }
    }
}
