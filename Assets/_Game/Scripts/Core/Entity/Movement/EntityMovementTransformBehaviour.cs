﻿using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Movement for an <see cref="IDynamicEntity"/> that directly modifies the <see cref="UnityEngine.Transform"/>
    /// </summary>
    public class EntityMovementTransformBehaviour : MonoBehaviour, IEntityMovement
    {
        /// <inheritdoc/>
        public event Action EntityTeleported;

        /// <inheritdoc/>
        public bool IsGrounded => true;
        /// <inheritdoc/>
        public bool CanUsePortals => _canUsePortals;
        /// <inheritdoc/>
        public bool PortalSuckInEnabled => _portalSuckInEnabled;

        /// <inheritdoc/>
        public Vector3 Position => transform.position;

        /// <inheritdoc/>
        public Vector3 Velocity { get => Vector3.zero; set { } }

        /// <inheritdoc/>
        public Quaternion Rotation => transform.rotation;

        /// <inheritdoc/>
        public Vector3 AngularVelocity { get => Vector3.zero; set { } }

        [SerializeField] private bool _canUsePortals = true;
        [SerializeField] private bool _portalSuckInEnabled = false;

        /// <inheritdoc/>
        public void Move(Vector3 moveAmount)
        {
            transform.position = transform.position + moveAmount;
        }

        /// <inheritdoc/>
        public void MoveTo(Vector3 newPosition)
        {
            transform.position = newPosition;
        }

        /// <inheritdoc/>
        public void Rotate(Quaternion rotationAmount)
        {
            transform.rotation = transform.rotation * rotationAmount;
        }

        /// <inheritdoc/>
        public void RotateTo(Quaternion newRotation)
        {
            transform.rotation = newRotation;
        }

        /// <inheritdoc/>
        public void Teleport(Vector3 position, Quaternion rotation)
        {
            transform.position = position;
            transform.rotation = rotation;

            EntityTeleported?.Invoke();
        }
    }
}
