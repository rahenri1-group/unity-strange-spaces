﻿using Game.Core.Physics;
using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// The movement component for an <see cref="IDynamicEntity"/>
    /// </summary>
    public interface IEntityMovement : IMovement, IRotation
    {
        /// <summary>
        /// Called after an entity has been teleported
        /// </summary>
        event Action EntityTeleported;

        /// <summary>
        /// Is the entity on the ground
        /// </summary>
        bool IsGrounded { get; }

        /// <summary>
        /// Can the entity use portals
        /// </summary>
        bool CanUsePortals { get; }

        /// <summary>
        /// Should this entity be sucked into a portal if very close
        /// Does nothing if <see cref="CanUsePortals"/> is false.
        /// </summary>
        bool PortalSuckInEnabled { get; }

        /// <summary>
        /// Teleports the entity to a new position
        /// </summary>
        void Teleport(Vector3 position, Quaternion rotation);
    }
}
