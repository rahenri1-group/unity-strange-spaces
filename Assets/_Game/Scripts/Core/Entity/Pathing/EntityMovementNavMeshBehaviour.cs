﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

namespace Game.Core.Entity.Pathing
{
    /// <summary>
    /// Movement for an <see cref="IDynamicEntity"/> that uses the pathing system
    /// </summary>
    public class EntityMovementNavMeshBehaviour : EntityMovementRigidbodyBehaviour, IEntityMovement, IRigidBodyMovement, IEntityPathingAgent
    {
        /// <inheritdoc/>
        public event Action<IEntityPathingAgent> PathAcquired;

        /// <inheritdoc/>
        public NavMeshAgent UnityAgent
        {
            get
            {
                if (_unityAgent == null) _unityAgent = GetComponent<NavMeshAgent>();
                return _unityAgent;
            }
        }
        private NavMeshAgent _unityAgent = null;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public bool PathingReady => UnityAgent.enabled && UnityAgent.isOnNavMesh;

        /// <inheritdoc/>
        public bool HasPath => _currentAgentPath != null;

        /// <inheritdoc/>
        public bool IsAgentAtDestination { get; private set; }

        /// <inheritdoc/>
        public float AgentMaxSpeed
        {
            get => UnityAgent.speed;
            set => UnityAgent.speed = value;
        }

        /// <inheritdoc/>
        public override Vector3 Velocity
        {
            get => base.Velocity;
            set
            {
                UnityAgent.enabled = false;
                base.Velocity = value;
            }
        }

        /// <inheritdoc/>
        public override Vector3 AngularVelocity
        {
            get => base.AngularVelocity;
            set
            {
                UnityAgent.enabled = false;
                base.AngularVelocity = value;
            }
        }

        [Inject] private IEntityPathingManager _pathingManager = null;
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IDynamicEntity _entity;
        private float _stoppingDistance;

        private IEntityPath _currentAgentPath;
        private int _agentPathNodeIndex;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsFalse(UnityAgent.enabled);

            _entity = this.GetComponentAsserted<IDynamicEntity>();

            Rigidbody.isKinematic = false;

            _stoppingDistance = UnityAgent.stoppingDistance;

            _currentAgentPath = null;
            _agentPathNodeIndex = 0;

            IsAgentAtDestination = false;
        }

        /// <inheritdoc/>
        private void Start()
        {
            _pathingManager.ConfigurePathingAgent(this);
        }

        private void OnEnable()
        {
            IsAgentAtDestination = false;

            _eventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        private void OnDisable()
        {
            _eventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventData)
        {
            if (eventData.Entity != _entity) return;

            // Better to do all of this here instead of in Teleport method as it guarentees that the entity has fully been moved into the new space.
            _pathingManager.ConfigurePathingAgent(this);

            if (_currentAgentPath != null && _agentPathNodeIndex + 1 < _currentAgentPath.PathNodes.Length)
            {
                UnityAgent.enabled = true;
                _agentPathNodeIndex += 1;

                PathAgentToCurrentPathNode();
            }
        }

        /// <inheritdoc/>
        public override void Move(Vector3 moveAmount)
        {
            UnityAgent.enabled = false;
            StopAgent();

            base.Move(moveAmount);
        }

        /// <inheritdoc/>
        public override void MoveTo(Vector3 newPosition)
        {
            UnityAgent.enabled = false;
            StopAgent();

            base.MoveTo(newPosition);
        }

        /// <inheritdoc/>
        public override void Rotate(Quaternion rotationAmount)
        {
            UnityAgent.enabled = false;
            StopAgent();

            base.Rotate(rotationAmount);
        }

        /// <inheritdoc/>
        public override void RotateTo(Quaternion newRotation)
        {
            UnityAgent.enabled = false;
            StopAgent();

            base.RotateTo(newRotation);
        }

        /// <inheritdoc/>
        public override void Teleport(Vector3 position, Quaternion rotation)
        {
            UnityAgent.enabled = false;

            base.Teleport(position, rotation);
        }

        /// <inheritdoc/>
        protected override void Update()
        {
            base.Update();

            if (!IsGrounded)
            {
                UnityAgent.enabled = false;
                Rigidbody.isKinematic = false;
            }
            else
            {
                Rigidbody.isKinematic = true;

                if (_currentAgentPath != null)
                {
                    UnityAgent.enabled = true;

                    if (!UnityAgent.hasPath || _currentAgentPath.IsPathStale)
                    {
                        // need to repath
                        var destinationNode = _currentAgentPath.PathNodes[_currentAgentPath.PathNodes.Length - 1];
                        SetDestination(new SpacePosition(destinationNode.NodeSpace, destinationNode.EndPosition), 0f);
                    }
                    else if (UnityAgent.hasPath && UnityAgent.remainingDistance <= UnityAgent.stoppingDistance && UnityAgent.desiredVelocity.sqrMagnitude == 0f)
                    {
                        _currentAgentPath = null;

                        UnityAgent.isStopped = true;

                        IsAgentAtDestination = true;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void StartAgent()
        {
            UnityAgent.enabled = true;;
        }

        /// <inheritdoc/>
        public bool SetDestination(SpacePosition spacePosition, float maxOffsetToPosition)
        {
            IsAgentAtDestination = false;

            bool pathExists = _pathingManager.GetPathForAgent(this, spacePosition, out _currentAgentPath, maxOffsetToPosition);
            if (pathExists)
            {
                _agentPathNodeIndex = 0;

                PathAgentToCurrentPathNode();

                PathAcquired?.Invoke(this);
                
                return true;
            }
            else
            {
                StopAgent();

                return false;
            }
        }

        /// <inheritdoc/>
        public void StopAgent()
        {
            _currentAgentPath = null;

            if (UnityAgent.isOnNavMesh)
            {
                UnityAgent.isStopped = true;
                UnityAgent.ResetPath();
            }
        }

        /// <inheritdoc/>
        public bool IsColliderInPath(Collider collider)
        {
            if (_currentAgentPath == null || UnityAgent.isStopped) return false; // no path                

            var colliderSpace = _spaceManager.GetObjectSpace(collider.gameObject);

            for (int nodeIndex = _agentPathNodeIndex; nodeIndex < _currentAgentPath.PathNodes.Length; nodeIndex++)
            {
                var pathNode = _currentAgentPath.PathNodes[nodeIndex];

                if (SpaceUtil.SpaceEquals(colliderSpace, pathNode.NodeSpace))
                {
                    var ray = new Ray();
                    var pathCorners = pathNode.Path.corners;
                    for (int cornerIndex = 1; cornerIndex < pathCorners.Length; cornerIndex++)
                    {
                        var cornerA = pathCorners[cornerIndex - 1];
                        var cornerB = pathCorners[cornerIndex];

                        var distance = Vector3.Distance(cornerA, cornerB);
                        if (distance > 0.01f) // don't compare for small distances, the ray direction normalization can fail
                        {

                            ray.origin = cornerA;
                            ray.direction = (cornerB - cornerA).normalized;

                            if (collider.Raycast(ray, out RaycastHit hitInfo, distance))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            
            return false;
        }

        private void PathAgentToCurrentPathNode()
        {
            var pathNode = _currentAgentPath.PathNodes[_agentPathNodeIndex];
            UnityAgent.isStopped = false;
            //UnityAgent.ResetPath();

            if (pathNode.PortalToNextNode != null) // path involves portals
            {
                UnityAgent.stoppingDistance = 0f;
            }
            else // no portals
            {
                UnityAgent.stoppingDistance = _stoppingDistance;
            }

            UnityAgent.SetPath(pathNode.Path);
        }
    }
}
