﻿namespace Game.Core.Entity.Pathing
{
    /// <summary>
    /// A path calculated from <see cref="IEntityPathingManager"/>
    /// </summary>
    public interface IEntityPath
    {
        /// <summary>
        /// The nodes comprising the path
        /// </summary>
        IEntityPathNode[] PathNodes { get; }

        /// <summary>
        /// If true the path is stale and possibly invalid
        /// </summary>
        bool IsPathStale { get; }
    }
}
