﻿using Game.Core.Portal;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Core.Entity.Pathing
{
    /// <summary>
    /// Part of a path returned from <see cref="IEntityPathingManager"/>
    /// </summary>
    public interface IEntityPathNode
    {
        /// <summary>
        /// The space for this portion of the path
        /// </summary>
        ISpaceData NodeSpace { get; }

        /// <summary>
        /// The portal to the next node in the path. This will be null for the last node
        /// </summary>
        IPortalTransporter PortalToNextNode { get; }

        /// <summary>
        /// The start position of this portion of the path
        /// </summary>
        Vector3 StartPosition { get; }

        /// <summary>
        /// The end position of this portion of the path
        /// </summary>
        Vector3 EndPosition { get; }

        /// <summary>
        /// The pathing for this portion
        /// </summary>
        NavMeshPath Path { get; }
    }
}
