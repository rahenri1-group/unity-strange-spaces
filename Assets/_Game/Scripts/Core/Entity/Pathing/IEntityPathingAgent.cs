﻿using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Core.Entity.Pathing
{
    /// <summary>
    /// Component to control pathing for a <see cref="IDynamicEntity"/>
    /// </summary>
    public interface IEntityPathingAgent : IEntityMovement, IGameObjectComponent
    {
        /// <summary>
        /// Raised when agent acquires a path
        /// </summary>
        event Action<IEntityPathingAgent> PathAcquired;

        /// <summary>
        /// Is the pathing agent ready to path
        /// </summary>
        bool PathingReady { get; }

        /// <summary>
        /// Does the pathing agent currently have a path
        /// </summary>
        bool HasPath { get; }

        /// <summary>
        /// Is the agent at the destination set by <see cref="SetDestination(ISpaceData, Vector3)"/>
        /// </summary>
        bool IsAgentAtDestination { get; }

        /// <summary>
        /// The max speed of the agent while pathing
        /// </summary>
        float AgentMaxSpeed { get; set; }

        /// <summary>
        /// Starts the agent, moving the agent onto a navmesh and preparing for pathing
        /// </summary>
        void StartAgent();

        /// <summary>
        /// Sets the target destination of the pathing agent (or a point within <paramref name="maxOffsetToPosition"/> to it). 
        /// Returns true if there is a valid path to the destination.
        /// </summary>
        bool SetDestination(SpacePosition position, float maxOffsetToPosition = 0f);

        /// <summary>
        /// Stops the pathing agent and clears the current path.
        /// </summary>
        void StopAgent();

        /// <summary>
        /// Tests if a collider is in the way of the current path
        /// </summary>
        bool IsColliderInPath(Collider collider);

        /// <summary>
        /// The <see cref="NavMeshAgent"/> the agent uses
        /// </summary>
        NavMeshAgent UnityAgent { get; }
    }
}
