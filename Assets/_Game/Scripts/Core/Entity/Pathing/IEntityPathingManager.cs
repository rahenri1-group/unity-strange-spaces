﻿using Game.Core.Space;

namespace Game.Core.Entity.Pathing
{
    /// <summary>
    /// Manages pathing <see cref="IDynamicEntity"/>s
    /// </summary>
    public interface IEntityPathingManager
    {
        /// <summary>
        /// Configures a <see cref="IEntityPathingAgent"/> for it's current space
        /// </summary>
        void ConfigurePathingAgent(IEntityPathingAgent agent);

        /// <summary>
        /// Returns true if there is a path from the agent to the destiniation (or a position up to <paramref name="maxOffsetAllowed"/> from it)
        /// Returns false if there is no path.
        /// </summary>
        bool GetPathForAgent(IEntityPathingAgent agent, SpacePosition destinationPosition, out IEntityPath path, float maxOffsetAllowed = 0f);

        /// <summary>
        /// Returns true if there is a path from the agent to the destiniation (or a position up to <paramref name="maxOffsetAllowed"/> from it)
        /// Returns false if there is no path or the path is greather than <paramref name="maxDistance"/>
        /// </summary>
        bool GetPathForAgent(IEntityPathingAgent agent, SpacePosition destinationPosition, float maxDistance, out IEntityPath path, float maxOffsetAllowed = 0f);
    }
}
