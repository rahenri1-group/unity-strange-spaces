﻿using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Data stored to persist an entity 
    /// </summary>
    [Serializable]
    public class EntityData
    {
        [SerializeField][ReadOnly] private string _id = string.Empty;
        [SerializeField] [ReadOnly] private string _name = string.Empty;

        [SerializeField] [ReadOnly] private string _entityAssetKey = string.Empty;
        [SerializeField] [ReadOnly] private Vector3 _position = Vector3.zero;
        [SerializeField] [ReadOnly] private Quaternion _rotation = Quaternion.identity;
        [SerializeField] [ReadOnly] private Vector3 _scale = Vector3.zero;

        [SerializeField] [ReadOnly] private string _serializedJson = string.Empty;

        /// <summary>
        /// The id of the entity
        /// </summary>
        public Guid Id
        {
            get
            {
                if (_parsedId == null) _parsedId = Guid.Parse(_id);
                return _parsedId.Value;
            }
        }
        private Guid? _parsedId = null;

        /// <summary>
        /// The name of the entity
        /// </summary>
        public string Name { get => _name; }

        /// <summary>
        /// The the asset key to use to load the entity
        /// </summary>
        public string EntityAssetKey { get => _entityAssetKey; }

        /// <summary>
        /// The stored position of the entity
        /// </summary>
        public Vector3 Position
        {
            get => _position;
            set => _position = value;
        }

        /// <summary>
        /// The stored rotation of the entity
        /// </summary>
        public Quaternion Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }

        /// <summary>
        /// The stored scale of the entity
        /// </summary>
        public Vector3 Scale
        {
            get => _scale;
            set => _scale = value;
        }

        /// <summary>
        /// Additional serialized json needed to create the entity
        /// </summary>
        public string SerializedJson
        {
            get => _serializedJson;
            set => _serializedJson = value;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="entityId"></param>
        public EntityData(
            Guid entityId, 
            string name, 
            string entityAssetKey)
        {
            _id = entityId.ToString();

            _name = name;

            _entityAssetKey = entityAssetKey;
        }
    }
}
