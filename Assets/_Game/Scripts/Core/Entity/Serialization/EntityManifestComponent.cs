﻿using System;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <inheritdoc cref="IEntityManifest"/>
    [Serializable]
    public class EntityManifestComponent : IEntityManifest
    {
        /// <inheritdoc/>
        public EntityData[] Entities { get => _entities ?? new EntityData[0]; }

#if UNITY_EDITOR
        public EntityData[] EntitiesEditor
        {
            get => _entities;
            set => _entities = value;
        }
#endif

        [SerializeField] private EntityData[] _entities = null;
    }
}
