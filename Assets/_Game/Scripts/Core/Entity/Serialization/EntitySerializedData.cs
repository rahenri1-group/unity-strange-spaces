﻿using System;
using System.Collections.Generic;

namespace Game.Core.Entity
{

    /// <summary>
    /// Serialized data for an <see cref="IEntity"/>
    /// </summary>
    [Serializable]
    public class EntitySerializedData
    {
        public Dictionary<string, EntitySerializedChildObjectData> ChildObjectMap = new Dictionary<string, EntitySerializedChildObjectData>();
    }

    /// <summary>
    /// Serialized data for child objects on a <see cref="IEntity"/>
    /// </summary>
    [Serializable]
    public class EntitySerializedChildObjectData
    {
        public bool Enabled;
        public Dictionary<Type, string> ComponentJsonMap;
    }
}
