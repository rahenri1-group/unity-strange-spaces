﻿using Game.Core.Space;

namespace Game.Core.Entity
{
    /// <summary>
    /// Manifest of <see cref="IDynamicEntity"/>s that should be created the first time a space is loaded
    /// </summary>
    public interface IEntityManifest : ISpaceDataComponent
    {
        /// <summary>
        /// List of <see cref="EntityData"/> entries for each <see cref="IDynamicEntity"/> to create
        /// </summary>
        EntityData[] Entities { get; }
    }
}
