﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;

namespace Game.Core.Event
{
    /// <inheritdoc cref="IEventBus"/>
    [Dependency(
        contract: typeof(IEventBus),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class EventBus : BaseModule, IEventBus
    {
        /// <inheritdoc />
        public override string ModuleName => "Event Bus";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private Dictionary<Type, HashSet<object>> _eventMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public EventBus(ILogRouter logger)
            : base(logger)
        {
            _eventMap = new Dictionary<Type, HashSet<object>>();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            _eventMap.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc />
        public Action<TEvent> Subscribe<TEvent>(Action<TEvent> handle)  where TEvent : struct
        {
            Type eventStruct = typeof(TEvent);

            if (!_eventMap.ContainsKey(eventStruct))
            {
                _eventMap.Add(eventStruct, new HashSet<object>());
            }

            var subscriptions = _eventMap[eventStruct]; 
            if (!subscriptions.Contains(handle))
            {
                subscriptions.Add(handle);
            }

            return handle;
        }

        /// <inheritdoc />
        public void Unsubscribe<TEvent>(Action<TEvent> handle) where TEvent : struct
        {
            Type eventStruct = typeof(TEvent);

            if (!_eventMap.ContainsKey(eventStruct))
            {
                return;
            }

            var subscriptions = _eventMap[eventStruct];
            if (subscriptions.Contains(handle))
            {
                subscriptions.Remove(handle);
            }

            if (subscriptions.Count == 0)
            {
                _eventMap.Remove(eventStruct);
            }
        }

        /// <inheritdoc />
        public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : struct
        {
            Type eventStruct = typeof(TEvent);

            if (!_eventMap.ContainsKey(eventStruct))
            {
                return;
            }

            foreach (var subscriptionObject in _eventMap[eventStruct].ToArray()) // to array incase structure gets modified mid event
            {
                try
                {
                    Action<TEvent> subscription = (Action<TEvent>)subscriptionObject;
                    subscription.Invoke(eventData);
                }
                catch (Exception e)
                {
                    ModuleLogException(e);
                }
            }
        }
    }
}
