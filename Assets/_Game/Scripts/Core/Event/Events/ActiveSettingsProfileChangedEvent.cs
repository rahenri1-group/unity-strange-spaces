﻿using Game.Core.Storage.Settings;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when the active settings config has changed
    /// </summary>
    public struct ActiveSettingsProfileChangedEvent
    {
        /// <summary>
        /// The current settings profile
        /// </summary>
        public ISettingsProfile SettingsProfile;
    }
}
