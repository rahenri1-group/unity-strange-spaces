﻿using Game.Core.Audio;
using Game.Core.Space;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an audio lister changes the space it is listening to
    /// </summary>
    public struct AudioListenerSpaceChangeEvent
    {
        /// <summary>
        /// The audio listener that has changed spaces
        /// </summary>
        public ISpaceAudioListener AudioListener;

        /// <summary>
        /// The space it is currently listening to
        /// </summary>
        public ISpaceData NewSpace;
    }
}
