﻿using Game.Core.Render.Camera;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when a camera is activated
    /// </summary>
    public struct CameraActivateEvent
    {
        /// <summary>
        /// The camera that was activated
        /// </summary>
        public ICamera Camera;
    }
}
