﻿using Game.Core.Render.Camera;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an camera is deactivated
    /// </summary>
    public struct CameraDeactivateEvent
    {
        /// <summary>
        /// The camera that was deactivated
        /// </summary>
        public ICamera Camera;
    }
}
