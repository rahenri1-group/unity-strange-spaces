﻿using Game.Core.Entity;
using UnityEngine;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an <see cref="IEntity"/> is created
    /// </summary>
    public struct EntityCreateEvent
    {
        /// <summary>
        /// The entity that was created
        /// </summary>
        public IEntity Entity;
        
        /// <summary>
        /// The game object of the entity
        /// </summary>
        public GameObject EntityGameObject;
    }
}
