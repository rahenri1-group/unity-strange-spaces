﻿using System;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an entity is destroyed
    /// </summary>
    public struct EntityDestroyEvent
    {
        /// <summary>
        /// The id of the entity that was destroyed
        /// </summary>
        public Guid EntityId;
    }
}
