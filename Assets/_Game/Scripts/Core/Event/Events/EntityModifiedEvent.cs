﻿using Game.Core.Entity;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an entity is modified. 
    /// This should be invoked whenever an <see cref="IEntity"/> has components added or removed at runtime.
    /// It is critical that this invoked for adding/removing <see cref="UnityEngine.Renderer"/>s and <see cref="UnityEngine.Collider"/>s
    /// </summary>
    public struct EntityModifiedEvent
    {
        /// <summary>
        /// The entity that was modifed
        /// </summary>
        public IEntity Entity;
    }
}
