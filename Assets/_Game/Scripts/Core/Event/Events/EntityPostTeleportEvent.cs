﻿using Game.Core.Entity;
using Game.Core.Portal;
using Game.Core.Space;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when after an entity teleports via a portal
    /// </summary>
    public struct EntityPostTeleportEvent
    {
        /// <summary>
        /// The entity
        /// </summary>
        public IDynamicEntity Entity;

        /// <summary>
        /// The portal used to teleport
        /// </summary>
        public IPortal Portal;

        /// <summary>
        /// The entity's previous space
        /// </summary>
        public ISpaceData OldSpace;

        /// <summary>
        /// The entity's new space
        /// </summary>
        public ISpaceData NewSpace;
    }
}
