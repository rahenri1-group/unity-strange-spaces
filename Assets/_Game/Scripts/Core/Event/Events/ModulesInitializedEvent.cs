﻿namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when all modules have finished initialization
    /// </summary>
    public struct ModulesInitializedEvent { }
}
