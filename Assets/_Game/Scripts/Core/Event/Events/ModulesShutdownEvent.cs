﻿namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when module shutdown begins
    /// </summary>
    public struct ModulesShutdownEvent { }
}
