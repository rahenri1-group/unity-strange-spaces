﻿using Game.Core.Portal;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when a portal is closed
    /// </summary>
    public struct PortalClosedEvent
    {
        /// <summary>
        /// The portal that has closed
        /// </summary>
        public IPortal Portal;
    }
}
