﻿using Game.Core.Portal;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when a portal is opened
    /// </summary>
    public struct PortalOpenedEvent
    {
        /// <summary>
        /// The portal that has opened
        /// </summary>
        public IPortal Portal;
    }
}
