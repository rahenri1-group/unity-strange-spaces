﻿using Game.Core.Space;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when the active space changes
    /// </summary>
    public struct SpaceActivateEvent
    {
        /// <summary>
        /// The new active space
        /// </summary>
        public ISpaceData SpaceData;
    }
}
