﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when a space has finished loading
    /// </summary>
    public struct SpaceLoadedEvent
    {
        /// <summary>
        /// The space that was loaded
        /// </summary>
        public ISpaceData SpaceData;

        /// <summary>
        /// The root game objects of the space
        /// </summary>
        public GameObject[] RootLoadedObjects;
    }
}