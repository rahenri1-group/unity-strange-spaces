﻿using Game.Core.Space;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when an space finishes unloading
    /// </summary>
    public struct SpaceUnloadedEvent
    {
        /// <summary>
        /// The space that was unloaded
        /// </summary>
        public ISpaceData SpaceData;
    }
}