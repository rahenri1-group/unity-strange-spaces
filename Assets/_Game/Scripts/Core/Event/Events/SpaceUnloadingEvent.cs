﻿using Game.Core.Space;

namespace Game.Core.Event
{
    /// <summary>
    /// Event raised when a space starts unloading
    /// </summary>
    public struct SpaceUnloadingEvent
    {
        /// <summary>
        /// The space being unloaded
        /// </summary>
        public ISpaceData SpaceData;
    }
}