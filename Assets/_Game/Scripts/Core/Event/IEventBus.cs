﻿using System;

namespace Game.Core.Event
{
    /// <summary>
    /// Bus for global events
    /// </summary>
    public interface IEventBus : IModule
    {
        /// <summary>
        /// Subscribe to an event type
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="handle"></param>
        /// <returns></returns>
        Action<TEvent> Subscribe<TEvent>(Action<TEvent> handle) where TEvent : struct;

        /// <summary>
        /// Unsubscribe to an event type
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="handle"></param>
        void Unsubscribe<TEvent>(Action<TEvent> handle) where TEvent : struct;

        /// <summary>
        /// Raise a global event to all subscribers
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="eventData"></param>
        void InvokeEvent<TEvent>(TEvent eventData) where TEvent : struct;
    }
}