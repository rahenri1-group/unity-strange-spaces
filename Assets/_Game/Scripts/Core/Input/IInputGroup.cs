﻿namespace Game.Core.Input
{
    /// <summary>
    /// A group of available user input
    /// </summary>
    public interface IInputGroup
    {
        /// <summary>
        /// Is the input group enabled
        /// </summary>
        bool InputEnabled { get; set; }
    }
}
