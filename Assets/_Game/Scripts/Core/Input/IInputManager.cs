﻿namespace Game.Core.Input
{
    /// <summary>
    /// Manager for all user input
    /// </summary>
    public interface IInputManager : IModule
    {
        /// <summary>
        /// The various input groups loadded 
        /// </summary>
        IInputGroup[] InputGroups { get; }
    }
}
