﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core.Interaction
{
    /// <inheritdoc cref="IInteractable"/>
    public abstract class BaseInteractable : InjectedBehaviour, IInteractable
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public int InteractionPriority => _interactionPriority;

        /// <inheritdoc />
        public bool IsBeingInteractedUpon => CurrentInteractor != null;

        /// <inheritdoc />
        public IInteractor CurrentInteractor { get; private set; }

        /// <inheritdoc />
        public event InteractableEvent HoverBegin;
        /// <inheritdoc />
        public event InteractableEvent HoverEnd;
        /// <inheritdoc />
        public event InteractableEvent InteractBegin;
        /// <inheritdoc />
        public event InteractableEvent InteractEnd;

        [Inject] protected IInteractionManager InteractionManager;

        [SerializeField] private IntReadonlyReference _interactionPriority = null;
        [SerializeField] [TypeRestriction(typeof(IInteractableDecider))] private Component _interactableDeciderObj = null;
        private IInteractableDecider _interactableDecider;

        protected override void Awake()
        {
            base.Awake();

            _interactableDecider = null;
            if (_interactableDeciderObj != null)
            {
                _interactableDecider = _interactableDeciderObj.GetComponentAsserted<IInteractableDecider>();
            }
        }

        /// <inheritdoc />
        protected virtual void OnDestroy()
        {
            InteractionManager.OnInteractableDisable(this);
        }

        /// <inheritdoc />
        public virtual void OnHoverBegin(IInteractor hoverer)
        {
            HoverBegin?.Invoke(this, hoverer);
        }

        /// <inheritdoc />
        public virtual void OnHoverEnd(IInteractor hoverer)
        {
            HoverEnd?.Invoke(this, hoverer);
        }

        /// <inheritdoc />
        public virtual Interactions AllowedInteractions(IInteractor interactor)
        {
            if (_interactableDecider != null)
            {
                return _interactableDecider.AllowedInteractions(this, interactor);
            }

            return Interactions.All;
        }

        /// <inheritdoc />
        public virtual void OnInteractBegin(IInteractor interactor)
        {
            CurrentInteractor = interactor;
            InteractBegin?.Invoke(this, interactor);
        }

        /// <inheritdoc />
        public virtual void OnInteractEnd(IInteractor interactor)
        {
            CurrentInteractor = null;
            InteractEnd?.Invoke(this, interactor);
        }
    }
}
