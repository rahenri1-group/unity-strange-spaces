﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core.Interaction
{
    /// <summary>
    /// An <see cref="IInteractable"/> that is composed of multiple <see cref="IInteractable"/>s. Treating them as a single unit
    /// </summary>
    public class CompoundInteractable : InjectedBehaviour, IInteractable
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public event InteractableEvent HoverBegin;
        /// <inheritdoc />
        public event InteractableEvent HoverEnd;
        /// <inheritdoc />
        public event InteractableEvent InteractBegin;
        /// <inheritdoc />
        public event InteractableEvent InteractEnd;

        /// <inheritdoc />
        public int InteractionPriority { get; private set; } = 0;

        /// <inheritdoc />
        public bool IsBeingInteractedUpon
        {
            get
            {
                foreach (var interactable in _interactables)
                {
                    if (interactable.IsBeingInteractedUpon)
                        return true;
                }

                return false;
            }
        }

        /// <inheritdoc />
        public IInteractor CurrentInteractor
        {
            get
            {
                foreach (var interactable in _interactables)
                {
                    if (interactable.CurrentInteractor != null)
                        return interactable.CurrentInteractor;
                }

                return null;
            }
        }

        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component[] _interactableObjs = null;
        private IInteractable[] _interactables;

        [Inject] protected IInteractionManager InteractionManager;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            _interactables = _interactableObjs.GetComponentArrayAsserted<IInteractable>();

            foreach (var interactable in _interactables)
            {
                InteractionPriority = Mathf.Min(InteractionPriority, interactable.InteractionPriority);
            }
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            foreach (var interactable in _interactables)
            {
                interactable.HoverBegin += OnChildHoverBegin;
                interactable.HoverEnd += OnChildHoverEnd;
                interactable.InteractBegin += OnChildInteractBegin;
                interactable.InteractEnd += OnChildInteractEnd;
            }
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            foreach (var interactable in _interactables)
            {
                interactable.HoverBegin -= OnChildHoverBegin;
                interactable.HoverEnd -= OnChildHoverEnd;
                interactable.InteractBegin -= OnChildInteractBegin;
                interactable.InteractEnd -= OnChildInteractEnd;
            }
        }

        /// <inheritdoc />
        private void OnDestroy()
        {
            InteractionManager.OnInteractableDisable(this);
        }

        /// <inheritdoc />
        public Interactions AllowedInteractions(IInteractor interactor)
        {
            return Interactions.None;
        }

        /// <inheritdoc />
        public void OnHoverBegin(IInteractor hoverer) { }

        /// <inheritdoc />
        public void OnHoverEnd(IInteractor hoverer) { }

        /// <inheritdoc />
        public void OnInteractBegin(IInteractor interactor) { }

        /// <inheritdoc />
        public void OnInteractEnd(IInteractor interactor) { }

        private void OnChildHoverBegin(IInteractable sender, IInteractor interactor)
        {
            HoverBegin?.Invoke(sender, interactor);
        }

        private void OnChildHoverEnd(IInteractable sender, IInteractor interactor)
        {
            HoverEnd?.Invoke(sender, interactor);
        }

        private void OnChildInteractBegin(IInteractable sender, IInteractor interactor)
        {
            InteractBegin?.Invoke(sender, interactor);
        }

        private void OnChildInteractEnd(IInteractable sender, IInteractor interactor)
        {
            InteractEnd?.Invoke(sender, interactor);
        }
    }
}
