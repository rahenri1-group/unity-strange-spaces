﻿using System;

namespace Game.Core.Interaction
{
    /// <summary>
    /// The allowed interactions between an <see cref="IInteractable"/> and an <see cref="IInteractor"/>
    /// </summary>
    [Flags]
    public enum Interactions
    {
        None = 0,
        Hover = 1,
        Interact = 2,

        All = Hover | Interact
    }

    /// <summary>
    /// Delegate for <see cref="IInteractable"/> events
    /// </summary>
    public delegate void InteractableEvent(IInteractable sender, IInteractor interactor);

    /// <summary>
    /// A component that can be interacted with by a <see cref="IInteractor"/>
    /// </summary>
    public interface IInteractable : IGameObjectComponent
    {
        /// <summary>
        /// Event raised when a hover interaction starts
        /// </summary>
        event InteractableEvent HoverBegin;
        /// <summary>
        /// Event raised when a hover interaction ends
        /// </summary>
        event InteractableEvent HoverEnd;
        /// <summary>
        /// Event raised when a standard interaction begins
        /// </summary>
        event InteractableEvent InteractBegin;
        /// <summary>
        /// Event raised when a standard interaction ends
        /// </summary>
        event InteractableEvent InteractEnd;

        /// <summary>
        /// The priority of this <see cref="IInteractable"/>. 
        /// If multiple <see cref="IInteractable"/>s are available to a <see cref="IInteractor"/>, the one with the lower <see cref="InteractionPriority"/> will be used.
        /// </summary>
        int InteractionPriority { get; }

        /// <summary>
        /// Is this currently being interacted with 
        /// </summary>
        bool IsBeingInteractedUpon { get; }

        /// <summary>
        /// The interactor interacting with this.
        /// </summary>
        IInteractor CurrentInteractor { get; }

        /// <summary>
        /// How can the provided <paramref name="interactor"/> interact with this
        /// </summary>
        Interactions AllowedInteractions(IInteractor interactor);

        /// <summary>
        /// Starts a hover interaction. Should only be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnHoverBegin(IInteractor hoverer);
        /// <summary>
        /// Ends a hover interaction. Should only be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnHoverEnd(IInteractor hoverer);

        /// <summary>
        /// Starts a standard interaction. Should only be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnInteractBegin(IInteractor interactor);
        /// <summary>
        /// Ends a standard interaction. Should only be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnInteractEnd(IInteractor interactor);
    }
}
