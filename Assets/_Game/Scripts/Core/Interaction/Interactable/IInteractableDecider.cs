﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// Interface that decides whether a <see cref="IInteractable"/> can be interacted with
    /// </summary>
    public interface IInteractableDecider
    {
        /// <summary>
        /// The allowed interactions for the provided <paramref name="interactable"/> and <paramref name="interactor"/>
        /// </summary>
        Interactions AllowedInteractions(IInteractable interactable, IInteractor interactor);
    }
}
