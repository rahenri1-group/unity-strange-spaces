﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Resource;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Interaction
{
    /// <summary>
    /// Spawns effects on an interaction begin/end
    /// </summary>
    public class InteractableEffectSpawner : InjectedBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component _interactableObj = null;
        private IInteractable _interactable;

        [SerializeField] private StringReadonlyReference _interactBeginEffectAddress = null;
        [SerializeField] private StringReadonlyReference _interactEndEffectAddress = null;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_interactableObj);
            Assert.IsTrue(string.IsNullOrEmpty(_interactBeginEffectAddress) || _poolableGameObjectManager.IsAssetKeyValid(_interactBeginEffectAddress));
            Assert.IsTrue(string.IsNullOrEmpty(_interactEndEffectAddress) || _poolableGameObjectManager.IsAssetKeyValid(_interactEndEffectAddress));

            _interactable = _interactableObj.GetComponentAsserted<IInteractable>();
        }

        private void OnEnable()
        {
            _interactable.InteractBegin += OnInteractBegin;
            _interactable.InteractEnd += OnInteractEnd;
        }

        private void OnDisable()
        {
            _interactable.InteractBegin -= OnInteractBegin;
            _interactable.InteractEnd -= OnInteractEnd;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            if (!string.IsNullOrEmpty(_interactBeginEffectAddress))
            {
                _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                    _interactBeginEffectAddress, 
                    _spaceManager.GetObjectSpace(gameObject), 
                    transform.position)
                    .Forget();
            }
        }

        private void OnInteractEnd(IInteractable sender, IInteractor interactor)
        {
            if (!string.IsNullOrEmpty(_interactEndEffectAddress))
            {
                _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                    _interactEndEffectAddress, 
                    _spaceManager.GetObjectSpace(gameObject), 
                    transform.position)
                    .Forget();
            }
        }
    }
}
