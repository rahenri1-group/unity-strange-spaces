﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// An <see cref="IInteractable"/> that is interacted with via a laser pointer
    /// </summary>
    public interface ILaserInteractable : IInteractable { }
}