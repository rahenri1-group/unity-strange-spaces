﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// An <see cref="IInteractable"/> that is interacted with via in world interactions
    /// </summary>
    public interface IWorldInteractable : IInteractable { }
}
