﻿namespace Game.Core.Interaction
{
    /// <inheritdoc cref="IWorldInteractable" />
    public class SimpleWorldInteractable : BaseInteractable, IWorldInteractable { }
}
