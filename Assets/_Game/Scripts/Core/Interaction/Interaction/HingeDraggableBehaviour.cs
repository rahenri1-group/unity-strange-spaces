﻿using Game.Core.DependencyInjection;
using Game.Core.Physics;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Interaction
{
    /// <summary>
    /// A type of <see cref="IHinge"/> that has a <see cref="IInteractable"/> component allowing it to be dragged
    /// </summary>
    public class HingeDraggableBehaviour : BaseHinge
    {
        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component _interactableObj = null;
        private IInteractable _interactable;

        [SerializeField] private FloatReadonlyReference _dragMotorForce = null;
        [SerializeField] private FloatReadonlyReference _dragMotorMaxSpeed = null;

        [Inject] private IInteractionManager _interactionManager = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_dragMotorForce > 0f);
            Assert.IsTrue(_dragMotorMaxSpeed > 0f);

            if (_interactableObj != null)
            {
                _interactable = _interactableObj.GetComponentAsserted<IInteractable>();
            }
            else
            {
                _interactable = this.GetComponentAsserted<IInteractable>();
            }
        }

        /// <inheritdoc/>
        protected override void OnEnable()
        {
            base.OnEnable();

            _interactable.InteractBegin += OnInteractBegin;
            _interactable.InteractEnd += OnInteractEnd;
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            _interactable.InteractBegin -= OnInteractBegin;
            _interactable.InteractEnd -= OnInteractEnd;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            StopAllCoroutines();

            Joint.useMotor = true;
            var motor = Joint.motor;
            motor.targetVelocity = 0f;
            motor.force = _dragMotorForce;
            Joint.motor = motor;

            var latched = _isLatched;
            UnlatchHinge();
            _isLatched = latched;

            StartCoroutine(Update_Drag(sender, interactor));
        }

        private void OnInteractEnd(IInteractable sender, IInteractor interactor)
        {
            Joint.useMotor = false;

            StopAllCoroutines();

            StartCoroutine(Update_CheckIfClosed());
        }

        /// <inheritdoc/>
        public override void AttemptOpen(float speed)
        {
            if (_interactable.IsBeingInteractedUpon)
            {
                _interactionManager.InteractEnd(_interactable.CurrentInteractor, _interactable);
            }

            base.AttemptOpen(speed);
        }

        /// <inheritdoc/>
        public override void AttemptClose(float speed)
        {
            if (_interactable.IsBeingInteractedUpon)
            {
                _interactionManager.InteractEnd(_interactable.CurrentInteractor, _interactable);
            }

            base.AttemptClose(speed);
        }

        private YieldInstruction _dragWait = null;
        private IEnumerator Update_Drag(IInteractable interactable, IInteractor interactor)
        {
            if (_dragWait == null) _dragWait = new WaitForFixedUpdate();

            Vector3 grabOffset = interactor.Body.transform.InverseTransformPoint(interactable.GameObject.transform.position);

            float oldTime = Time.time;
            while (true)
            {
                yield return _dragWait;

                if (_isLatched && !IsInLatchRange)
                {
                    _isLatched = false;

                    RaiseOpenEvent(new HingeEventArgs
                    {
                        HingeSpeed = 0f
                    });
                }

                float newTime = Time.time;

                Vector3 currentLocalPosition = transform.InverseTransformPoint(interactable.GameObject.transform.position);
                Vector3 desiredLocalPosition = transform.InverseTransformPoint(interactor.Body.transform.TransformPoint(grabOffset));

                Vector2 currentLocalPosition2D = Vector2.zero;
                Vector2 desiredLocalPosition2D = Vector2.zero;

                if (Joint.axis.x == 1f)
                {
                    currentLocalPosition2D = new Vector2(currentLocalPosition.z, currentLocalPosition.y);
                    desiredLocalPosition2D = new Vector2(desiredLocalPosition.z, desiredLocalPosition.y);
                }
                else if (Joint.axis.y == 1f)
                {
                    currentLocalPosition2D = new Vector2(currentLocalPosition.x, currentLocalPosition.z);
                    desiredLocalPosition2D = new Vector2(desiredLocalPosition.x, desiredLocalPosition.z);
                }
                else if (Joint.axis.z == 1f)
                {
                    currentLocalPosition2D = new Vector2(currentLocalPosition.x, currentLocalPosition.y);
                    desiredLocalPosition2D = new Vector2(desiredLocalPosition.x, desiredLocalPosition.y);
                }

                float signedAngle = Vector2.SignedAngle(desiredLocalPosition2D, currentLocalPosition2D);
                if (signedAngle != 0f)
                {
                    var motor = Joint.motor;
                    motor.targetVelocity = Mathf.Clamp(signedAngle / (newTime - oldTime), -_dragMotorMaxSpeed, _dragMotorMaxSpeed);
                    motor.force = _dragMotorForce;

                    Joint.motor = motor;
                }
                oldTime = newTime;
            }
        }
    }
}
