﻿using Game.Core.Physics;
using System.Collections;
using UnityEngine;

namespace Game.Core.Interaction
{
    /// <summary>
    /// Component on an <see cref="IInteractable"/> that allows it to be held
    /// </summary>
    [RequireComponent(typeof(IRigidBodyMovement))]
    public class HoldableBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component _interactableObj = null;
        private IInteractable _interactable;

        private IRigidBodyMovement _movement;

        private Vector3 _runningAverageVelocity;

        private IEnumerator dragCoroutine;

        private bool _rigidbodyIsKinematic;
        private bool _rigidbodyUseGravity;
        private RigidbodyInterpolation _rigidbodyInterpolation;

        /// <inheritdoc/>
        private void Awake()
        {
            if (_interactableObj != null)
            {
                _interactable = _interactableObj.GetComponentAsserted<IInteractable>();
            }
            else
            {
                _interactable = this.GetComponentAsserted<IInteractable>();
            }

            _movement = this.GetComponentAsserted<IRigidBodyMovement>();

            _rigidbodyIsKinematic = _movement.Rigidbody.isKinematic;
            _rigidbodyUseGravity = _movement.Rigidbody.useGravity;
            _rigidbodyInterpolation = _movement.Rigidbody.interpolation;

            _runningAverageVelocity = Vector3.zero;

            dragCoroutine = null;
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _interactable.InteractBegin += OnInteractBegin;
            _interactable.InteractEnd += OnInteractEnd;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _interactable.InteractBegin -= OnInteractBegin;
            _interactable.InteractEnd -= OnInteractEnd;
        }

        private void OnInteractBegin(IInteractable interactable, IInteractor interactor)
        {
            if (dragCoroutine != null)
            {
                StopCoroutine(dragCoroutine);
            }

            _movement.Rigidbody.isKinematic = false;
            _movement.Rigidbody.useGravity = false;
            _movement.Rigidbody.interpolation = RigidbodyInterpolation.Interpolate;

            _movement.AngularVelocity = Vector3.zero;
            _movement.Velocity = Vector3.zero;

            _runningAverageVelocity = Vector3.zero;

            dragCoroutine = Drag(interactor);
            StartCoroutine(dragCoroutine);
        }


        private void OnInteractEnd(IInteractable interactable, IInteractor interactor)
        {
            if (dragCoroutine != null)
            {
                StopCoroutine(dragCoroutine);
                dragCoroutine = null;
            }

            _movement.Rigidbody.isKinematic = _rigidbodyIsKinematic;
            _movement.Rigidbody.useGravity = _rigidbodyUseGravity;
            _movement.Rigidbody.interpolation = _rigidbodyInterpolation;

            if (_movement.Rigidbody.isKinematic)
            {
                _movement.AngularVelocity = Vector3.zero;
                _movement.Velocity = Vector3.zero;
            }
            else
            {
                _movement.Velocity = _runningAverageVelocity;
            }
        }

        private IEnumerator Drag(IInteractor dragger)
        {
            YieldInstruction wait = new WaitForFixedUpdate();

            Vector3 offsetPosition = dragger.GameObject.transform.InverseTransformPoint(_movement.Position);
            Quaternion offsetRotation = dragger.GameObject.transform.InverseTransformRotation(_movement.Rotation);

            _runningAverageVelocity = Vector3.zero;

            while (true)
            {
                yield return wait;

                var desiredRotation = dragger.GameObject.transform.TransformRotation(offsetRotation);
                _movement.RotateTo(desiredRotation);

                var desiredPosition = dragger.GameObject.transform.TransformPoint(offsetPosition);
                //_movement.MoveTo(desiredPosition); 
                // moveto can ignore collisions
                var velocity = (desiredPosition - _movement.Position) / Time.fixedDeltaTime;
                _movement.Velocity = velocity;

                _runningAverageVelocity = Vector3.Lerp(_runningAverageVelocity, velocity, 0.8f);
            }
        }
    }
}
