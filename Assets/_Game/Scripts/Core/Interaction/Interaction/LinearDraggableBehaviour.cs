﻿using Game.Core.Math;
using Game.Core.Physics;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Interaction
{
    /// <summary>
    /// A type of <see cref="ILinearMover"/> that has a <see cref="IInteractable"/> component allowing it to be dragged
    /// </summary>
    public class LinearDraggableBehaviour : BaseLinear
    {
        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component _interactableObj = null;
        private IInteractable _interactable;


        [SerializeField] private FloatReference _maxDragSpeed = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_maxDragSpeed > 0f);

            if (_interactableObj != null)
            {
                _interactable = _interactableObj.GetComponentAsserted<IInteractable>();
            }
            else
            {
                _interactable = this.GetComponentAsserted<IInteractable>();
            }
        }

        /// <inheritdoc/>
        protected override void OnEnable()
        {
            base.OnEnable();

            _interactable.InteractBegin += OnInteractBegin;
            _interactable.InteractEnd += OnInteractEnd;
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            _interactable.InteractBegin -= OnInteractBegin;
            _interactable.InteractEnd -= OnInteractEnd;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            StopAllCoroutines();

            UnlatchLinear();

            Body.velocity = Vector3.zero;

            StartCoroutine(Drag(sender, interactor));
        }

        private void OnInteractEnd(IInteractable sender, IInteractor interactor)
        {
            StopAllCoroutines();

            StartCoroutine(Update_CheckIfClosed());
        }

        private YieldInstruction _dragWait = null;
        private IEnumerator Drag(IInteractable interactable, IInteractor interactor)
        {
            if (_dragWait == null) _dragWait = new WaitForFixedUpdate();

            Vector3 grabOffset = interactor.Body.transform.InverseTransformPoint(interactable.GameObject.transform.position);

            float oldTime = Time.time;
            while (true)
            {
                yield return _dragWait;

                float newTime = Time.time;

                var oldPosition = Body.position;
                var desiredPosition = interactor.Body.transform.TransformPoint(grabOffset);

                var closestPoint = MathUtil.ClosestPointOnLineSegment(desiredPosition, ClosedPosition, OpenPosition);

                Body.velocity = Vector3.ClampMagnitude((closestPoint - oldPosition) / (newTime - oldTime), _maxDragSpeed);
                oldTime = newTime;
            }
        }
    }
}
