﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using System;
using UnityEngine;

namespace Game.Core.Interaction
{
    /// <inheritdoc cref="IEntityInteractor"/>
    public abstract class BaseEntityInteractorBehaviour : BasePrimaryInteractor, IEntityInteractor
    {
        /// <inheritdoc />
        public event Action<IEntityInteractor> EntityInteractBegin;
        /// <inheritdoc />
        public event Action<IEntityInteractor> EntityInteractEnd;

        /// <inheritdoc />
        public override GameObject InteractorOwner => Entity.GameObject;

        [Inject] protected IEventBus EventBus;

        protected IEntity Entity { get; set; }

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Entity = GetComponentInParent<IEntity>();

            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        /// <inheritdoc />
        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        public override void OnInteractBegin(IInteractable interactable)
        {
            base.OnInteractBegin(interactable);

            EntityInteractBegin?.Invoke(this);
        }

        public override void OnInteractEnd()
        {
            base.OnInteractEnd();

            EntityInteractEnd?.Invoke(this);
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventData)
        {
            if (eventData.Entity != Entity) return;

            if (InteractionTarget != null)
            {
                if (InteractionTarget.GetComponentInParent<IEntity>() == Entity)
                {
                    // No interact stopping or hand-off logic for interactables parented to entity
                    return;
                }

                bool wasInteracting = IsInteracting;
                var handOffInteractor = InteractionTarget;

                if (wasInteracting)
                {
                    InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);
                }
                else
                {
                    InteractionManager.HoverEnd(InteractionTargetInteractor, InteractionTarget);
                }

                AttemptSpaceChangeHandOff(handOffInteractor, wasInteracting);
            }
        }
    }
}