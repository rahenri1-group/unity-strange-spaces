﻿using Game.Core.DependencyInjection;
using Game.Core.Physics;
using Game.Core.Space;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Interaction
{
    /// <inheritdoc cref="IPrimaryWorldInteractor"/>
    public abstract class BasePrimaryInteractor : InjectedBehaviour, IPrimaryWorldInteractor
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;
        /// <inheritdoc />
        public virtual Rigidbody Body { get => _cacheBody; }
        private Rigidbody _cacheBody;

        /// <inheritdoc/>
        public virtual Vector3 Position => Body.position;
        /// <inheritdoc/>
        public virtual Quaternion Rotation => Body.rotation;

        /// <inheritdoc />
        public abstract GameObject InteractorOwner { get; }
        /// <inheritdoc />
        public IInteractable InteractionTarget { get; protected set; }
        /// <inheritdoc />
        public bool IsInteracting { get; private set; }
        /// <inheritdoc />
        public virtual bool IsInteractionEnabled => true;

        /// <inheritdoc />
        public WorldInteractableTriggerProximityVolume NearbyWorldInteractableVolume => _nearbyWorldInteractableVolume;
        public float MaxDistanceBeforeDrop => _maxDistanceBeforeDrop;

        /// <inheritdoc />
        public int InteractionBlockingMask { get; private set; }

        [Inject] protected IInteractionManager InteractionManager = null;
        [Inject] protected ILogRouter Logger = null;
        [Inject] protected ISpaceManager SpaceManager = null;
        [Inject] protected ISpacePhysics SpacePhysics = null;

        [SerializeField] private WorldInteractableTriggerProximityVolume _nearbyWorldInteractableVolume = null;
        [SerializeField] private FloatReadonlyReference _delayBetweenInteractions = null;
        [SerializeField] private FloatReadonlyReference _maxDistanceBeforeDrop = null;

        [SerializeField] [Layer] private int[] _interactionBlockingLayers = new int[0];

        [SerializeField] private LaserInteractableRaycastQuerierBehaviour _laserInteractableQuerier = null;

        protected IInteractor InteractionTargetInteractor { get; private set; }

        private HashSet<IShadowWorldInteractor> _shadowInteractors;

        private float _lastInteractionTime;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_nearbyWorldInteractableVolume);
            Assert.IsTrue(_maxDistanceBeforeDrop > 0f);

            _cacheBody = GetComponent<Rigidbody>();

            _shadowInteractors = new HashSet<IShadowWorldInteractor>();
            InteractionTargetInteractor = null;

            InteractionTarget = null;
            IsInteracting = false;

            InteractionBlockingMask = LayerUtil.ConstructMaskForLayers(_interactionBlockingLayers);

            _lastInteractionTime = 0f;
        }

        /// <inheritdoc />
        protected virtual void OnDestroy()
        {
            InteractionManager.OnInteractorDisable(this);
        }

        /// <inheritdoc />
        public void RegisterShadowInteractor(IShadowWorldInteractor shadowInteractor)
        {
            if (!_shadowInteractors.Contains(shadowInteractor))
            {
                _shadowInteractors.Add(shadowInteractor);
            }
        }

        /// <inheritdoc />
        public void UnregisterShadowInteractor(IShadowWorldInteractor shadowInteractor)
        {
            if (_shadowInteractors.Contains(shadowInteractor))
            {
                _shadowInteractors.Remove(shadowInteractor);
            }

            if (InteractionTargetInteractor == shadowInteractor && InteractionTarget != null)
            {
                bool wasInteracting = shadowInteractor.IsInteracting;

                var handoffInteractor = InteractionTarget;

                if (wasInteracting)
                {
                    InteractionManager.InteractEnd(shadowInteractor, InteractionTarget);
                }
                else
                {
                    InteractionManager.HoverEnd(shadowInteractor, InteractionTarget);
                }

                InteractionTargetInteractor = null;
                AttemptSpaceChangeHandOff(handoffInteractor, wasInteracting);
            }
        }

        protected void InteractBegin()
        {
            if ((Time.time - _lastInteractionTime) >= _delayBetweenInteractions)
            {
                InteractionManager.AttemptInteractBegin(InteractionTargetInteractor, InteractionTarget);
            }
        }

        protected void InteractEnd()
        {
            if (InteractionTargetInteractor != null && InteractionTarget != null)
            {
                InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);
            }
        }

        /// <inheritdoc/>
        public virtual bool CanInteractWith(IInteractable interactble)
        {
            return IsInteractionEnabled;
        }

        /// <inheritdoc />
        public virtual void OnInteractBegin(IInteractable interactable)
        {
            IsInteracting = true;
            InteractionTarget = interactable;

            _lastInteractionTime = Time.time;
        }

        /// <inheritdoc />
        public virtual void OnInteractEnd()
        {
            IsInteracting = false;
            InteractionTarget = null;
        }

        /// <inheritdoc />
        protected virtual void FixedUpdate()
        {
            foreach (var shadowInteractor in _shadowInteractors)
            {
                shadowInteractor.SyncPosition();

                shadowInteractor.UpdateNearbyInteractable();
            }

            if (IsInteractionEnabled)
            {
                if (Body == null)
                {
                    Logger.LogError($"Interactions are enabled for {name} but {nameof(Body)} is null");
                    return;
                }

                if (InteractionTarget != null && IsInteracting)
                {
                    if (InteractionTargetInteractor.CanInteractWith(InteractionTarget) == false
                        || InteractionTarget.AllowedInteractions(InteractionTargetInteractor).HasFlag(Interactions.Interact) == false)
                    {
                        // interactions no longer allowed between interactor and interactable
                        InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);
                    }
                    else
                    {
                        if (InteractionTarget is IWorldInteractable)
                        {
                            bool areInteractorAndTargetInSameSpace = SpaceManager.AreInSameSpace(InteractionTargetInteractor.GameObject, InteractionTarget.GameObject);

                            // check for things that could end interaction
                            if (areInteractorAndTargetInSameSpace
                                && Vector3.Distance(InteractionTarget.GameObject.transform.position, InteractionTargetInteractor.Position) > MaxDistanceBeforeDrop)
                            {
                                // In same space but are too far apart
                                InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);
                            }
                            else if (areInteractorAndTargetInSameSpace == false)
                            {
                                // no longer in same space, check if it can be handed off to another interactor
                                var interactable = InteractionTarget;
                                InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);

                                AttemptSpaceChangeHandOff(interactable, true);
                            }
                        }
                    }

                    return;
                }

                // interactions are enabled but check if has been long enough before looking for a new interactable
                if ((Time.time - _lastInteractionTime) >= _delayBetweenInteractions)
                {
                    GetClosestInteractable(out IInteractor newInteractor, out IInteractable newInteractionTarget);


                    if (InteractionTarget == newInteractionTarget) return;

                    if (InteractionTarget != null)
                    {
                        InteractionManager.HoverEnd(InteractionTargetInteractor, InteractionTarget);
                    }

                    if (newInteractionTarget != null)
                    {
                        InteractionManager.AttemptHoverBegin(newInteractor, newInteractionTarget);
                    }

                    InteractionTarget = newInteractionTarget;
                    InteractionTargetInteractor = newInteractor;
                }
            }
            else
            {
                // interactions no longer enabled. Stop all current interactions
                if (InteractionTarget != null)
                {
                    if (IsInteracting)
                    {
                        InteractionManager.InteractEnd(InteractionTargetInteractor, InteractionTarget);
                    }
                    else
                    {
                        InteractionManager.HoverEnd(InteractionTargetInteractor, InteractionTarget);
                    }
                }

                InteractionTargetInteractor = null;
                InteractionTarget = null;
            }
        }

        // the interactable should already have all hover and interactions ended before calling this
        protected void AttemptSpaceChangeHandOff(IInteractable handoffInteractable, bool interact)
        {
            // check primary first
            if (gameObject.activeInHierarchy && SpaceManager.AreInSameSpace(gameObject, handoffInteractable.GameObject) 
                && Vector3.Distance(handoffInteractable.GameObject.transform.position, Position) <= MaxDistanceBeforeDrop)
            {
                InteractionTargetInteractor = this;
                InteractionTarget = handoffInteractable;

                if (interact)
                    InteractBegin();
                else
                    InteractionManager.AttemptHoverBegin(InteractionTargetInteractor, InteractionTarget);

                return;
            }

            // then the shadows
            foreach (var shadowInteractor in _shadowInteractors)
            {
                if (shadowInteractor.GameObject.activeInHierarchy && SpaceManager.AreInSameSpace(shadowInteractor.GameObject, handoffInteractable.GameObject)
                    && Vector3.Distance(handoffInteractable.GameObject.transform.position, shadowInteractor.Position) <= MaxDistanceBeforeDrop)
                {
                    InteractionTargetInteractor = shadowInteractor;
                    InteractionTarget = handoffInteractable;

                    if (interact)
                        InteractBegin();
                    else
                        InteractionManager.AttemptHoverBegin(InteractionTargetInteractor, InteractionTarget);

                    return;
                }
            }

            // failed
            InteractionTargetInteractor = this;
            InteractionTarget = null;
            IsInteracting = false;
        }

        private void GetClosestInteractable(out IInteractor interactor, out IInteractable interactable)
        {
            interactor = this;

            // laser querier has priority over world interactable detection
            if (_laserInteractableQuerier != null && _laserInteractableQuerier.HasValidResult)
            {
                interactable = _laserInteractableQuerier.CurrentResult;
            }
            else
            {
                float proximityValue;
                interactable = NearbyWorldInteractableVolume.GetClosest(NearbyInteractableVolumeValidator, out proximityValue);

                foreach (var shadowInteractor in _shadowInteractors)
                {
                    if (shadowInteractor.InteractionTarget != null && shadowInteractor.ClosestInteractableProximityValue < proximityValue)
                    {
                        interactor = shadowInteractor;
                        proximityValue = shadowInteractor.ClosestInteractableProximityValue;
                        interactable = shadowInteractor.InteractionTarget;
                    }
                }
            }
        }

        private bool NearbyInteractableVolumeValidator(IInteractable interactable)
        {
            if (CanInteractWith(interactable)
                && interactable.AllowedInteractions(this) != Interactions.None)
            {
                if (SpacePhysics.Raycast(
                    SpaceManager.GetObjectSpace(gameObject),
                    Position,
                    (interactable.GameObject.transform.position - Position).normalized,
                    out var spaceCastResult,
                    Vector3.Distance(interactable.GameObject.transform.position, Position),
                    QueryPortalInteraction.Ignore,
                    InteractionBlockingMask))
                {
                    // verfiy the object we hit is the interactable
                    return spaceCastResult.Collider?.GetComponentInParent<IInteractable>() == interactable;
                }

                return true;
            }

            return false;
        }
    }
}
