﻿using Game.Core.Entity;
using System;

namespace Game.Core.Interaction
{
    /// <summary>
    /// A <see cref="IInteractor"/> controlled by an <see cref="IEntity"/>
    /// </summary>
    public interface IEntityInteractor : IInteractor
    {
        /// <summary>
        /// Event raised when the interactor begins interacting with a object
        /// </summary>
        event Action<IEntityInteractor> EntityInteractBegin;

        /// <summary>
        /// Event raised when the interactor stops interacting with a object
        /// </summary>
        event Action<IEntityInteractor> EntityInteractEnd;
    }
}
