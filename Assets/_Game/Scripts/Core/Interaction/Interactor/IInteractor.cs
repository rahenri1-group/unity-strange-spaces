﻿using UnityEngine;

namespace Game.Core.Interaction
{
    /// <summary>
    /// A gameobject that can interact with <see cref="IInteractable"/>s. This can only interact with a single object at a time.
    /// </summary>
    public interface IInteractor : IRigidbodyComponent
    {
        /// <summary>
        /// The owner of the <see cref="IInteractor"/>
        /// </summary>
        GameObject InteractorOwner { get; }

        /// <summary>
        /// The position of the interactor
        /// </summary>
        Vector3 Position { get; }
        /// <summary>
        /// The rotation of the interactor
        /// </summary>
        Quaternion Rotation { get; }

        /// <summary>
        /// The current target of interaction for the <see cref="IInteractor"/>
        /// </summary>
        IInteractable InteractionTarget { get; }
        /// <summary>
        /// Is this currently interacting with an object
        /// </summary>
        bool IsInteracting { get; }
        /// <summary>
        /// Is interaction current enabled for the <see cref="IInteractor"/>
        /// </summary>
        bool IsInteractionEnabled { get; }

        /// <summary>
        /// Tests whether the <see cref="IInteractor"/> is allowed to interact with the provided <paramref name="interactble"/>
        /// </summary>
        bool CanInteractWith(IInteractable interactble);

        /// <summary>
        /// Starts an interaction. This should mainly be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnInteractBegin(IInteractable interactable);
        /// <summary>
        /// Ends an interaction. This should mainly be called by <see cref="IInteractionManager"/>
        /// </summary>
        void OnInteractEnd();
    }
}
