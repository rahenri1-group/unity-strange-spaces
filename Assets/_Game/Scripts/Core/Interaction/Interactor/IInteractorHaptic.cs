﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// Haptic feedback script for an <see cref="IInteractor"/>
    /// </summary>
    public interface IInteractorHaptic
    {
        /// <summary>
        /// Triggers a haptic pulse
        /// </summary>
        /// <param name="amplitude"></param>
        /// <param name="frequency"></param>
        /// <param name="duration"></param>
        void HapticPulse(float amplitude, float frequency, float duration);
    }
}
