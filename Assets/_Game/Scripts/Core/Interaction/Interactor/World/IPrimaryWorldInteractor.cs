﻿using Game.Core.Physics;

namespace Game.Core.Interaction
{
    /// <summary>
    /// An interactor that can have multiple <see cref="IShadowWorldInteractor"/> of itself. 
    /// This is to support <see cref="IInteractor"/>s and <see cref="IInteractable"/>s that may straddle portals
    /// </summary>
    public interface IPrimaryWorldInteractor : IInteractor
    {
        /// <summary>
        /// The volume used to find <see cref="IWorldInteractable"/>s
        /// </summary>
        WorldInteractableTriggerProximityVolume NearbyWorldInteractableVolume { get; }

        /// <summary>
        /// Mask of of objects that should block interaction
        /// </summary>
        int InteractionBlockingMask { get; }

        /// <summary>
        /// Registers an <see cref="IShadowWorldInteractor"/> that will match this interactor
        /// </summary>

        void RegisterShadowInteractor(IShadowWorldInteractor shadowInteractor);
        /// <summary>
        /// Unregisters a previously registered <see cref="IShadowWorldInteractor"/>
        /// </summary>
        void UnregisterShadowInteractor(IShadowWorldInteractor shadowInteractor);
    }
}
