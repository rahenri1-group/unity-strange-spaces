﻿using Game.Core.Portal;

namespace Game.Core.Interaction
{
    /// <summary>
    /// An interactor that shadows the position and size of a <see cref="IPrimaryWorldInteractor"/> across a portal
    /// </summary>
    public interface IShadowWorldInteractor : IInteractor
    {
        /// <summary>
        /// The portal that necessitated a shadow being created
        /// </summary>
        IPortal Portal { get; }

        /// <summary>
        /// Syncs the position to match the <see cref="IPrimaryWorldInteractor"/> this is shadowing
        /// </summary>
        void SyncPosition();

        /// <summary>
        /// Called by the Update method of the <see cref="IPrimaryWorldInteractor"/> to update the shadow interactor
        /// </summary>
        void UpdateNearbyInteractable();

        /// <summary>
        /// The proximity value (distance) to the nearest <see cref="IInteractable"/>
        /// </summary>
        float ClosestInteractableProximityValue { get; }
    }
}