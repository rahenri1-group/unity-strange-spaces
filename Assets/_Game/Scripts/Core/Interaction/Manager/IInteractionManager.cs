﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// Managers for coordinating interactionss between <see cref="IInteractor"/> and <see cref="IInteractable"/>s
    /// </summary>
    public interface IInteractionManager : IModule
    {
        /// <summary>
        /// Starts a hover interaction
        /// </summary>
        void AttemptHoverBegin(IInteractor interactor, IInteractable interactable);
        /// <summary>
        /// Ends a hover interaction
        /// </summary>
        void HoverEnd(IInteractor interactor, IInteractable interactable);

        /// <summary>
        /// Starts and ends a single interaction
        /// </summary>
        void Interact(IInteractor interactor, IInteractable interactable);
        /// <summary>
        /// Begins a standard interaction
        /// </summary>
        void AttemptInteractBegin(IInteractor interactor, IInteractable interactable);
        /// <summary>
        /// Ends a standard interation
        /// </summary>
        void InteractEnd(IInteractor interactor, IInteractable interactable);

        /// <summary>
        /// Called by the <paramref name="interactor"/> to inform the manager that it is no longer active
        /// </summary>
        void OnInteractorDisable(IInteractor interactor);

        /// <summary>
        /// Called by the <paramref name="interactable"/> to inform the manager that it is no longer active
        /// </summary>
        void OnInteractableDisable(IInteractable interactable);
    }
}