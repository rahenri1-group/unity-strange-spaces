﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Render;
using Game.Core.Resource;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Interaction
{
    /// <inheritdoc cref="IInteractionManager"/>
    [Dependency(
        contract: typeof(IInteractionManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class InteractionManager : BaseModule, IInteractionManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "InteractionManager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public InteractionManagerConfig Config = new InteractionManagerConfig();

        [Inject] private IAssetResourceManager _assetResourceManager = null;

        private HashSet<IInteractor> _knownInteractors;

        private Material _defaultGlowMaterial;
        private Material _hoverOnlyGlowMaterial;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public InteractionManager(ILogRouter logger)
            : base(logger)
        {
            _defaultGlowMaterial = null;
            _knownInteractors = null;
        }

        /// <inheritdoc/>
        public async override UniTask Initialize()
        {
            _knownInteractors = new HashSet<IInteractor>();

            _defaultGlowMaterial = await _assetResourceManager.LoadAsset<Material>(Config.GlowMaterialAssetKey);
            _hoverOnlyGlowMaterial = await _assetResourceManager.LoadAsset<Material>(Config.HoverOnlyGlowMaterialAssetKey);

            await base.Initialize();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _assetResourceManager.ReleaseAsset(_defaultGlowMaterial);
            _defaultGlowMaterial = null;

            _assetResourceManager.ReleaseAsset(_hoverOnlyGlowMaterial);
            _hoverOnlyGlowMaterial = null;

            return base.Shutdown();
        }

        /// <inheritdoc/>
        public void OnInteractorDisable(IInteractor interactor)
        {
            if (interactor.InteractionTarget != null)
            {
                HoverEnd(interactor, interactor.InteractionTarget);
                InteractEnd(interactor, interactor.InteractionTarget);
            }

            if (_knownInteractors.Contains(interactor))
            {
                _knownInteractors.Remove(interactor);
            }
        }

        /// <inheritdoc/>
        public void OnInteractableDisable(IInteractable interactable)
        {
            foreach (var interactor in _knownInteractors)
            {
                if (interactor.InteractionTarget == interactable)
                {
                    HoverEnd(interactor, interactor.InteractionTarget);
                    InteractEnd(interactor, interactor.InteractionTarget);
                }
            }
        }

        /// <inheritdoc/>
        public void AttemptHoverBegin(IInteractor interactor, IInteractable interactable)
        {
            _knownInteractors.Add(interactor);

            var allowedInteractions = interactable.AllowedInteractions(interactor);
            if (interactor.CanInteractWith(interactable) 
                && allowedInteractions.HasFlag(Interactions.Hover))
            {
                // glow system is only for world interactables
                if (interactable is IWorldInteractable)
                {
                    var glowable = interactable.GetComponent<IGlowable>();
                    if (glowable == null)
                    {
                        glowable = interactable.AddComponent<SimpleGlowable>();
                    }

                    if (allowedInteractions.HasFlag(Interactions.Interact))
                    {
                        glowable.SharedGlowMaterial = _defaultGlowMaterial;
                    }
                    else
                    {
                        glowable.SharedGlowMaterial = _hoverOnlyGlowMaterial;
                    }

                    glowable.GlowEnabled = true;
                }

                if (Config.HoverHapticDuration > 0f)
                {
                    var haptic = interactor.GetComponent<IInteractorHaptic>();
                    if (haptic != null)
                        haptic.HapticPulse(Config.HoverHapticAmplitude, Config.HoverHapticFrequency, Config.HoverHapticDuration);
                }

                interactable.OnHoverBegin(interactor);
            }
        }

        /// <inheritdoc/>
        public void HoverEnd(IInteractor interactor, IInteractable interactable)
        {
            _knownInteractors.Add(interactor);

            // glow system is only for world interactables
            if (interactable is IWorldInteractable)
            {
                var glowable = interactable.GetComponent<IGlowable>();
                if (glowable != null)
                {
                    glowable.GlowEnabled = false;
                }
            }

            interactable.OnHoverEnd(interactor);
        }

        /// <inheritdoc/>
        public void Interact(IInteractor interactor, IInteractable interactable)
        {
            AttemptInteractBegin(interactor, interactable);
            InteractEnd(interactor, interactable);
        }

        /// <inheritdoc/>
        public void AttemptInteractBegin(IInteractor interactor, IInteractable interactable)
        {
            _knownInteractors.Add(interactor);

            if (interactor.CanInteractWith(interactable) 
                && interactable.AllowedInteractions(interactor).HasFlag(Interactions.Interact))
            {
                HoverEnd(interactor, interactable);

                if (interactor.IsInteracting)
                {
                    InteractEnd(interactor, interactor.InteractionTarget);
                }

                if (interactable.IsBeingInteractedUpon)
                {
                    InteractEnd(interactable.CurrentInteractor, interactable);
                }

                interactor.OnInteractBegin(interactable);
                interactable.OnInteractBegin(interactor);
            }
        }

        /// <inheritdoc/>
        public void InteractEnd(IInteractor interactor, IInteractable interactable)
        {
            _knownInteractors.Add(interactor);

            interactor.OnInteractEnd();
            interactable.OnInteractEnd(interactor);
        }
    }
}
