﻿namespace Game.Core.Interaction
{
    /// <summary>
    /// Config for <see cref="InteractionManager"/>
    /// </summary>
    public class InteractionManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The material to use for <see cref="IInteractable"/>s that can be hovered and interacted with
        /// </summary>
        public string GlowMaterialAssetKey = string.Empty;

        /// <summary>
        /// The material to use for <see cref="IInteractable"/>s that can only be hovered over
        /// </summary>
        public string HoverOnlyGlowMaterialAssetKey = string.Empty;

        /// <summary>
        /// The amplitude of the haptic on a hover interaction
        /// </summary>
        public float HoverHapticAmplitude = 0.5f;
        /// <summary>
        /// The frequency of the haptic on a hover interaction
        /// </summary>
        public float HoverHapticFrequency = 50f;
        /// <summary>
        /// The duration of the haptic on a hover interaction
        /// </summary>
        public float HoverHapticDuration = 0.1f;
    }
}