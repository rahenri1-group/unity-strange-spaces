﻿using System;
using UnityEngine;

namespace Game.Core.Item
{
    /// <inheritdoc cref="IItemDefinition"/>
    public abstract class BaseItemDefinition : BaseGuidScriptableObject, IItemDefinition
    {
        /// <inheritdoc/>
        public Guid ItemId => Id;

        /// <inheritdoc/>
        public string Name => _name;

        [SerializeField] private string _name = "";

        public override bool Equals(object obj)
        {
            return Equals(obj as BaseItemDefinition);
        }

        public bool Equals(BaseItemDefinition other)
        {
            return ItemId == other.ItemId;
        }

        public bool Equals(IItemDefinition other)
        {
            return ItemId == other.ItemId;
        }

        public override int GetHashCode() => ItemId.GetHashCode();
    }
}
