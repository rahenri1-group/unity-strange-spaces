﻿using System;

namespace Game.Core.Item
{
    /// <summary>
    /// Definition for an item
    /// </summary>
    public interface IItemDefinition : IEquatable<IItemDefinition>
    {
        /// <summary>
        /// The unique id of for the item definition
        /// </summary>
        Guid ItemId { get; }

        /// <summary>
        /// The name used to describe the item
        /// </summary>
        string Name { get; }
    }
}
