﻿using System;

namespace Game.Core.Item
{
    /// <summary>
    /// Manager for <see cref="IItemDefinition"/>s
    /// </summary>
    public interface IItemDefinitionManager : IModule
    {
        /// <summary>
        /// Finds the <see cref="IItemDefinition"/> with the provided <paramref name="definitionId"/>. 
        /// Returns null if one does not exist.
        /// </summary>
        IItemDefinition GetItemDefinition(Guid definitionId);

        /// <summary>
        /// Finds the <see cref="IItemDefinition"/> with the provided <paramref name="definitionId"/>. 
        /// Returns null if one does not exist or it is the wrong type.
        /// </summary>
        T GetItemDefinition<T>(Guid definitionId) where T : class, IItemDefinition;

        /// <summary>
        /// Finds the <see cref="IItemDefinition"/> with the provided <paramref name="name"/>. 
        /// Returns null if one does not exist.
        /// </summary>
        IItemDefinition GetItemDefinition(string name);

        /// <summary>
        /// Finds the <see cref="IItemDefinition"/> with the provided <paramref name="name"/>. 
        /// Returns null if one does not exist or it is the wrong type.
        /// </summary>
        T GetItemDefinition<T>(string name) where T : class, IItemDefinition;

        /// <summary>
        /// Returns all available <see cref="IItemDefinition"/>s
        /// </summary>
        IItemDefinition[] GetAllItemDefinitions();
    }
}
