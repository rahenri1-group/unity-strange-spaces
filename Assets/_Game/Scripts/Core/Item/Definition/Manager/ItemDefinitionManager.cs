﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Item
{
    /// <inheritdoc cref="IItemDefinitionManager"/>
    [Dependency(
        contract: typeof(IItemDefinitionManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class ItemDefinitionManager : BaseModule, IItemDefinitionManager
    {
        /// <inheritdoc />
        public override string ModuleName => "Item Definition Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;

        public ItemDefinitionManagerConfig Config = new ItemDefinitionManagerConfig();

        private readonly IAssetResourceManager _assetResourceManager;

        private HashSet<BaseItemDefinition> _itemDefinitions;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public ItemDefinitionManager(
            IAssetResourceManager assetResourceManager,
            ILogRouter logger)
            : base(logger) 
        {
            _assetResourceManager = assetResourceManager;

            _itemDefinitions = new HashSet<BaseItemDefinition>();
        }

        /// <inheritdoc />
        public override async UniTask Initialize()
        {
            var assetAddresses = await _assetResourceManager.GetAssetKeysWithLabel(Config.ItemDefinitionAssetLabel);
            foreach (var address in assetAddresses)
            {
                var itemDefinition = await _assetResourceManager.LoadAsset<BaseItemDefinition>(address);
                _itemDefinitions.Add(itemDefinition);
            }

            await base.Initialize();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            foreach (var itemDefinition in _itemDefinitions)
            {
                _assetResourceManager.ReleaseAsset(itemDefinition);
            }
            _itemDefinitions.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc />
        public IItemDefinition GetItemDefinition(Guid definitionId)
        {
            return _itemDefinitions.FirstOrDefault(i => i.ItemId == definitionId);
        }

        /// <inheritdoc />
        public T GetItemDefinition<T>(Guid definitionId) where T : class, IItemDefinition
        {
            return GetItemDefinition(definitionId) as T;
        }

        /// <inheritdoc />
        public IItemDefinition GetItemDefinition(string name)
        {
            return _itemDefinitions.FirstOrDefault(i => string.Equals(i.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        /// <inheritdoc />
        public T GetItemDefinition<T>(string name) where T : class, IItemDefinition
        {
            return GetItemDefinition(name) as T;
        }

        /// <inheritdoc />
        public IItemDefinition[] GetAllItemDefinitions()
        {
            return _itemDefinitions.ToArray();
        }
    }
}
