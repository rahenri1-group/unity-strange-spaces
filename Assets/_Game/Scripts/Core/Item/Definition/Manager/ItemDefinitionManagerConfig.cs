﻿namespace Game.Core.Item
{
    /// <summary>
    /// Config for <see cref="ItemDefinitionManager"/>
    /// </summary>
    public class ItemDefinitionManagerConfig : ModuleConfig
    {
        public string ItemDefinitionAssetLabel = "Item Definition";
    }
}
