﻿namespace Game.Core.Item
{
    /// <summary>
    /// Extensions for <see cref="IInventory"/> 
    /// </summary>
    public static class IInventoryExtensions
    {
        /// <summary>
        /// Adds a single instance of <paramref name="itemDefinition"/> to the inventory
        /// </summary>
        public static void AddItem(this IInventory inventory, IItemDefinition itemDefinition)
        {
            inventory.AddItems(itemDefinition, 1);
        }

        /// <summary>
        /// Removes a single instance of <paramref name="itemDefinition"/> from the inventory
        /// </summary>
        public static void RemoveItem(this IInventory inventory, IItemDefinition itemDefinition)
        {
            inventory.RemoveItems(itemDefinition, 1);
        }

        /// <summary>
        /// Does at least a single instance of <paramref name="itemDefinition"/> exist in the inventory
        /// </summary>
        public static bool ContainsItem(this IInventory inventory, IItemDefinition itemDefinition)
        {
            return inventory.DefinitionQuantity(itemDefinition) > 0;
        }
    }
}
