﻿using Game.Core.Entity;

namespace Game.Core.Item
{
    /// <summary>
    /// An instance of an item that exists in world
    /// </summary>
    public interface IItemEntity : IDynamicEntity
    {
        /// <summary>
        /// The definition of the item
        /// </summary>
        IItemDefinition ItemDefinition { get; }
    }
}
