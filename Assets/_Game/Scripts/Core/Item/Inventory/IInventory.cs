﻿using Game.Core.Entity;
using System;

namespace Game.Core.Item
{
    /// <summary>
    /// A collection <see cref="IItemDefinition"/>s that are held by an <see cref="IEntity"/>
    /// </summary>
    public interface IInventory
    {
        /// <summary>
        /// Event raised when the contents of the inventory change
        /// </summary>
        event Action InventoryContentsUpdated;

        /// <summary>
        /// Adds <paramref name="quantity"/> instances of <paramref name="itemDefinition"/> to the inventory
        /// </summary>
        void AddItems(IItemDefinition itemDefinition, int quantity);

        /// <summary>
        /// Removes <paramref name="quantity"/> instances of <paramref name="itemDefinition"/> from the inventory
        /// </summary>
        void RemoveItems(IItemDefinition itemDefinition, int quantity);

        /// <summary>
        /// Removes all items from the inventory
        /// </summary>
        void RemoveAllItems();

        /// <summary>
        /// Returns the number of instances of <paramref name="itemDefinition"/> in the inventory
        /// </summary>
        int DefinitionQuantity(IItemDefinition itemDefinition);

        /// <summary>
        /// Returns all <see cref="IItemDefinitions"/> in the inventory
        /// </summary>
        IItemDefinition[] AllDefinitions();
    }
}
