﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Item
{
    /// <inheritdoc cref="IInventory"/>
    public class Inventory : IInventory
    {
        private class InventoryItem
        {
            public int Quantity { get; set; }

            public InventoryItem()
            {
                Quantity = 0;
            }
        }

        /// <inheritdoc />
        public event Action InventoryContentsUpdated;

        private readonly Dictionary<IItemDefinition, InventoryItem> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public Inventory()
        {
            _items = new Dictionary<IItemDefinition, InventoryItem>();
        }

        /// <inheritdoc />
        public void AddItems(IItemDefinition itemDefinition, int quantity)
        {
            if (!_items.ContainsKey(itemDefinition))
            {
                _items.Add(itemDefinition, new InventoryItem());
            }

            _items[itemDefinition].Quantity += quantity;

            InventoryContentsUpdated?.Invoke();
        }

        /// <inheritdoc />
        public IItemDefinition[] AllDefinitions()
        {
            return _items.Keys.ToArray();
        }

        /// <inheritdoc />
        public int DefinitionQuantity(IItemDefinition itemDefinition)
        {
            if (!_items.ContainsKey(itemDefinition))
            {
                return 0;
            }

            return _items[itemDefinition].Quantity;
        }

        /// <inheritdoc />
        public void RemoveItems(IItemDefinition itemDefinition, int quantity)
        {
            if (!_items.ContainsKey(itemDefinition))
            {
                return;
            }

            var item = _items[itemDefinition];
            item.Quantity -= quantity;
            
            if (item.Quantity <= 0)
            {
                _items.Remove(itemDefinition);
            }

            InventoryContentsUpdated?.Invoke();
        }

        /// <inheritdoc />
        public void RemoveAllItems()
        {
            _items.Clear();

            InventoryContentsUpdated?.Invoke();
        }
    }
}
