﻿using Game.Core.Entity;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Item
{
    /// <inheritdoc cref="IItemEntity"/>
    public class ItemEntity : BaseDynamicEntity<EntitySerializedData>, IItemEntity
    {
        /// <inheritdoc />
        public IItemDefinition ItemDefinition { get; private set; }

        [SerializeField] [TypeRestriction(typeof(IItemDefinition), false)] private UnityEngine.Object _itemDefinitionObj = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_itemDefinitionObj);
            ItemDefinition = (IItemDefinition)_itemDefinitionObj;
        }
    }
}
