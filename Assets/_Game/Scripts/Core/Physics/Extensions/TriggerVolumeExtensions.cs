﻿namespace Game.Core.Physics
{
    /// <summary>
    /// Extensions for <see cref="ITriggerVolume{T}"/>
    /// </summary>
    public static class TriggerVolumeExtensions
    {
        /// <summary>
        /// Is the volume currently empty
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="triggerVolume"></param>
        /// <returns></returns>
        public static bool IsEmpty<T>(this ITriggerVolume<T> triggerVolume)
            where T : class, IGameObjectComponent
        {
            return triggerVolume.VolumeCount == 0;
        }
    }
}
