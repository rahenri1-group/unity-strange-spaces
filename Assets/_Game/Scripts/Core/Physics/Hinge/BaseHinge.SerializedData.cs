﻿using Game.Core.Serialization;
using System;
using UnityEngine;

namespace Game.Core.Physics
{
    public abstract partial class BaseHinge : InjectedBehaviour, ISerializableComponent
    {
        [Serializable]
        private class SerializedData
        {
            public float HingeAngle;
        }

        /// <inheritdoc/>
        public bool SerializeComponent => _serializeComponent;

        /// <inheritdoc/>
        public string SerializeToJson()
        {
            if (Joint == null) Joint = GetComponent<HingeJoint>();

            var data = new SerializedData
            {
                HingeAngle = Joint.angle
            };

            return JsonSerializer.Serialize(data);
        }

        /// <inheritdoc/>
        public void DeserializeFromJson(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                if (Joint == null) Joint = GetComponent<HingeJoint>();

                StopAllCoroutines();

                UnlatchHinge();

                var data = JsonDeserializer.Deserialize<SerializedData>(json);

                transform.localRotation = Quaternion.identity;
                Joint.axis = Joint.axis; // stupid but necessary for hinge to work
                transform.localRotation = Quaternion.AngleAxis(data.HingeAngle, Joint.axis);

                ResetUpdateCoroutine();
            }
        }
    }
}
