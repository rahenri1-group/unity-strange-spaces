﻿using Game.Core.DependencyInjection;
using Game.Core.Math;
using Game.Core.Serialization;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="IHinge"/>
    [RequireComponent(typeof(HingeJoint))]
    public abstract partial class BaseHinge : InjectedBehaviour, IGameObjectComponent, IHinge
    {
        /// <inheritdoc/>
        public float Angle => (_openClockwise ? 1f : -1f) * MathUtil.NormalizeAngle180(RawAngle);

        public float RawAngle
        {
            get
            {
                if (Joint.axis.x == 1f)
                {
                    return transform.localEulerAngles.x;
                }
                else if (Joint.axis.y == 1f)
                {
                    return transform.localEulerAngles.y;
                }
                else
                {
                    return transform.localEulerAngles.z;
                }
                
            }
        }

        /// <inheritdoc/>
        public bool IsClosed => (_latchOnClose) ? _isLatched : IsInLatchRange;

        protected bool IsInLatchRange
        {
            get
            {
                float angle = Angle;
                return _latchedMinAngle <= angle + 0.1f && angle - 0.1f <= _latchedMaxAngle;
            }
        }

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        // Note: events only fire if latchOnClose checked

        /// <inheritdoc/>
        public event HingeEvent Open;
        /// <inheritdoc/>
        public event HingeEvent Close;

        [SerializeField] private BoolReadonlyReference _serializeComponent = null;


        [SerializeField] private BoolReadonlyReference _latchOnClose = null;

        [SerializeField] private FloatReadonlyReference _latchedMinAngle = null;
        [SerializeField] private FloatReadonlyReference _latchedMaxAngle = null;

        [SerializeField] private BoolReadonlyReference _openClockwise = null;
        [SerializeField] private FloatReadonlyReference _openCloseMotorForce = null;

        [Inject] protected IJsonDeserializer JsonDeserializer = null;
        [Inject] protected IJsonSerializer JsonSerializer = null;

        protected HingeJoint Joint { get; private set; }
        private Rigidbody _body;

        private float _unlatchedMinAngle, _unlatchedMaxAngle;

        protected bool _isLatched;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Joint = GetComponent<HingeJoint>();
            _body = GetComponent<Rigidbody>();

            Assert.IsTrue(Joint.axis.magnitude == 1f);
            Assert.IsTrue(Joint.axis.x == 1f || Joint.axis.y == 1f || Joint.axis.z == 1f);

            _isLatched = false;

            _unlatchedMaxAngle = Joint.limits.max;
            _unlatchedMinAngle = Joint.limits.min;
        }

        /// <inheritdoc/>
        protected virtual void OnEnable()
        {
            ResetUpdateCoroutine();
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            StopAllCoroutines();
        }

        /// <inheritdoc/>
        public virtual void AttemptOpen(float speed)
        {
            if (_isLatched)
            {
                UnlatchHinge();

                RaiseOpenEvent(new HingeEventArgs
                {
                    HingeSpeed = 0f
                });
            }

            StopAllCoroutines();

            StartCoroutine(Update_AccelerateMotor((_openClockwise ? 1f : -1f) * Mathf.Abs(speed), 1f));
            StartCoroutine(Update_CheckIfClosed(2f));
        }

        /// <inheritdoc/>
        public virtual void AttemptClose(float speed)
        {
            if (IsClosed) return;

            StopAllCoroutines();

            StartCoroutine(Update_AccelerateMotor((_openClockwise ? -1f : 1f) * Mathf.Abs(speed), 1f));
            StartCoroutine(Update_CheckIfClosed());
        }

        protected void ResetUpdateCoroutine()
        {
            StopAllCoroutines();

            Joint.useMotor = false;

            if (_latchOnClose)
            {
                if (IsInLatchRange)
                {
                    LatchHinge();
                }
                else
                {
                    UnlatchHinge();

                    StartCoroutine(Update_CheckIfClosed());
                }
            }
        }

        protected void LatchHinge()
        {
            _isLatched = true;

            var limits = Joint.limits;
            limits.min = _latchedMinAngle;
            limits.max = _latchedMaxAngle;
            Joint.limits = limits;

            _body.isKinematic = true;
        }

        protected void UnlatchHinge()
        {
            _isLatched = false;

            var limits = Joint.limits;
            limits.max = _unlatchedMaxAngle;
            limits.min = _unlatchedMinAngle;
            Joint.limits = limits;

            _body.isKinematic = false;
        }

        protected void RaiseOpenEvent(HingeEventArgs args)
        {
            Open?.Invoke(this, args);
        }

        protected void RaiseCloseEvent(HingeEventArgs args)
        {
            Close?.Invoke(this, args);
        }

        private YieldInstruction _accelerateMotorWait = null;
        private IEnumerator Update_AccelerateMotor(float targetSpeed, float duration)
        {
            if (_accelerateMotorWait == null) _accelerateMotorWait = new WaitForFixedUpdate();

            float startSpeed = Joint.velocity;

            Joint.useMotor = true;
            var motor = Joint.motor;
            motor.targetVelocity = startSpeed;
            motor.force = _openCloseMotorForce;
            Joint.motor = motor;

            float startTime = Time.time;

            while (Time.time - startTime < duration)
            {
                yield return _accelerateMotorWait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / duration);

                motor = Joint.motor;
                motor.targetVelocity = Mathf.SmoothStep(startSpeed, targetSpeed, lerp);
                Joint.motor = motor;
            }

            motor = Joint.motor;
            motor.targetVelocity = targetSpeed;
            Joint.motor = motor;
        }

        private YieldInstruction _checkIfClosedWait = null;
        protected IEnumerator Update_CheckIfClosed(float delay = 0f)
        {
            if (delay > 0f)
            {
                yield return new WaitForSeconds(delay);
            }

            if (_checkIfClosedWait == null) _checkIfClosedWait = new WaitForFixedUpdate();

            float oldTime = Time.time;
            float oldAngle = RawAngle;
            while (true)
            {
                yield return _checkIfClosedWait;

                float newTime = Time.time;
                float newAngle = RawAngle;

                if (IsInLatchRange)
                {
                    if (_latchOnClose)
                    {
                        float hingeSpeed = Mathf.Abs(Mathf.DeltaAngle(newAngle, oldAngle) / (newTime - oldTime));

                        LatchHinge();

                        RaiseCloseEvent(new HingeEventArgs
                        {
                            HingeSpeed = hingeSpeed
                        });
                    }

                    Joint.useMotor = false;

                    break;
                }

                oldTime = newTime;
                oldAngle = newAngle;
            }

            StopAllCoroutines();
        }
    }
}
