﻿using Game.Core.Audio;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <summary>
    /// Audio component for an instance of <see cref="IHinge". Plays audio on open and close />
    /// </summary>
    public class HingeAudioBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] [TypeRestriction(typeof(IHinge))]  private Component _hingeObj = null;
        private IHinge _hinge;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _openAudioClipsObj;
        private IAudioClipCollection _openAudioClips;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _closeGentleAudioClipsObj;
        private IAudioClipCollection _closeGentleAudioClips;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _closeHarshAudioClipsObj;
        private IAudioClipCollection _closeHarshAudioClips;

        [SerializeField] private FloatReadonlyReference _harshSpeedThreshold = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_hingeObj);

            _hinge = _hingeObj.GetComponentAsserted<IHinge>();
            _openAudioClips = _openAudioClipsObj != null ? (IAudioClipCollection) _openAudioClipsObj : null;
            _closeGentleAudioClips = _closeGentleAudioClipsObj != null ? (IAudioClipCollection) _closeGentleAudioClipsObj : null;
            _closeHarshAudioClips = _closeHarshAudioClipsObj != null ? (IAudioClipCollection) _closeHarshAudioClipsObj : null;
        }

        /// <inheritdoc/>
        protected override void OnEnable()
        {
            base.OnEnable();

            _hinge.Open += OnOpen;
            _hinge.Close += OnClose;
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            _hinge.Open -= OnOpen;
            _hinge.Close -= OnClose;
        }

        private void OnOpen(IHinge sender, HingeEventArgs args)
        {
            if (_openAudioClips != null)
            {
                PlayClip(_openAudioClips.GetNextAudioClip());
            }
        }

        private void OnClose(IHinge sender, HingeEventArgs args)
        {
            if (args.HingeSpeed >= _harshSpeedThreshold && _closeHarshAudioClips != null)
            {
                PlayClip(_closeHarshAudioClips.GetNextAudioClip());
            }
            else if (_closeGentleAudioClips != null)
            {
                PlayClip(_closeGentleAudioClips.GetNextAudioClip());
            }
        }
    }
}