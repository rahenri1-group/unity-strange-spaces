﻿namespace Game.Core.Physics
{
    /// <summary>
    /// Delegate for <see cref="IHinge"/> events
    /// </summary>
    public delegate void HingeEvent(IHinge sender, HingeEventArgs args);

    /// <summary>
    /// The args for <see cref="HingeEvent"/>
    /// </summary>
    public struct HingeEventArgs
    {
        public float HingeSpeed;
    }

    /// <summary>
    /// An interface for a hinge
    /// </summary>
    public interface IHinge
    {
        /// <summary>
        /// The angle of the hinge
        /// </summary>
        float Angle { get; }

        /// <summary>
        /// Is the hinge closed
        /// </summary>
        bool IsClosed { get; }

        /// <summary>
        /// Raised when the hinge is opened
        /// </summary>
        event HingeEvent Open;
        /// <summary>
        /// Raised when the hinge is closed
        /// </summary>
        event HingeEvent Close;

        /// <summary>
        /// Attempts to open the hinge
        /// </summary>
        void AttemptOpen(float speed);

        /// <summary>
        /// Attempts to close the hinge
        /// </summary>
        void AttemptClose(float speed);
    }
}
