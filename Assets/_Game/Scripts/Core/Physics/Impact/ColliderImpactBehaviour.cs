﻿using System.Collections;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Applies physics impact events to <see cref="IImpactable"/>s
    /// </summary>
    [RequireComponent(typeof(IImpactable))]
    public class ColliderImpactBehaviour : MonoBehaviour
    {
        [Tooltip("How long to wait before detecting collisions after enabling")]
        [SerializeField] private FloatReadonlyReference _collisionDetectionDelay = null;

        private IImpactable _impactable;

        private bool _readyToReportCollisions;

        private IEnumerator _collisionDetectionDelayCoroutine;

        private void Awake()
        {
            _impactable = this.GetComponentAsserted<IImpactable>();

            _readyToReportCollisions = false;

            _collisionDetectionDelayCoroutine = null;
        }

        private void OnEnable()
        {
            _readyToReportCollisions = false;

            if (_collisionDetectionDelayCoroutine != null)
            {
                StopCoroutine(_collisionDetectionDelayCoroutine);
            }

            _collisionDetectionDelayCoroutine = CollisionDetectionDelay();
            StartCoroutine(_collisionDetectionDelayCoroutine);
        }

        private void OnDisable()
        {
            _readyToReportCollisions = false;

            if (_collisionDetectionDelayCoroutine != null)
            {
                StopCoroutine(_collisionDetectionDelayCoroutine);
                _collisionDetectionDelayCoroutine = null; 
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (!_readyToReportCollisions) return;

            Vector3 force = -1f * collision.impulse;
            _impactable.OnImpact(force.magnitude, force.normalized);
        }

        private IEnumerator CollisionDetectionDelay()
        {
            if (_collisionDetectionDelay > 0f)
            {
                yield return new WaitForSeconds(_collisionDetectionDelay);
            }

            _readyToReportCollisions = true;

            _collisionDetectionDelayCoroutine = null;
        }
    }
}
