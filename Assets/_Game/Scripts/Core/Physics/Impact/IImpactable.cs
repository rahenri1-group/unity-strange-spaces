﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Interface for interacting with an object that responds to physics impacts
    /// </summary>
    public interface IImpactable
    {
        /// <summary>
        /// Informs an object that an impact has happened
        /// </summary>
        void OnImpact(float impactForce, Vector3 forceDirection);
    }
}
