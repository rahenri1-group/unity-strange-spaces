﻿using Game.Core.Serialization;
using System;
using UnityEngine;

namespace Game.Core.Physics
{
    public abstract partial class BaseLinear : InjectedBehaviour, ISerializableComponent
    {
        [Serializable]
        private class SerializedData
        {
            public float OpenAmount;
        }

        /// <inheritdoc/>
        public bool SerializeComponent => _serializeComponent;

        /// <inheritdoc/>
        public string SerializeToJson()
        {
            var data = new SerializedData
            {
                OpenAmount = OpenAmount
            };

            return JsonSerializer.Serialize(data);
        }

        /// <inheritdoc/>
        public void DeserializeFromJson(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                StopAllCoroutines();

                UnlatchLinear();

                var data = JsonDeserializer.Deserialize<SerializedData>(json);

                Body.position = Vector3.Lerp(ClosedPosition, OpenPosition, data.OpenAmount);

                ResetUpdateCoroutines();
            }
        }
    }
}
