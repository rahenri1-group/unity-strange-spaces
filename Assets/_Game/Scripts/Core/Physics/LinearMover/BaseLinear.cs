﻿using Game.Core.DependencyInjection;
using Game.Core.Serialization;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="ILinearMover"/>
    [RequireComponent(typeof(ConfigurableJoint))]
    [RequireComponent(typeof(Rigidbody))]
    public abstract partial class BaseLinear : InjectedBehaviour, IGameObjectComponent, ILinearMover
    {
        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public bool IsClosed => _isLatched || IsInLatchRange;
        /// <inheritdoc/>
        public float Speed => Body.velocity.magnitude;

        /// <inheritdoc/>
        public float OpenAmount => Mathf.InverseLerp(
            transform.parent.InverseTransformPoint(ClosedPosition).z, 
            transform.parent.InverseTransformPoint(OpenPosition).z, 
            transform.localPosition.z);

        /// <inheritdoc/>
        public Vector3 ClosedPosition { get; private set; }
        /// <inheritdoc/>
        public Vector3 OpenPosition { get; private set; }

        protected bool IsInLatchRange
        {
            get
            {
                return Vector3.Distance(Body.position, ClosedPosition) <= _latchedDistance; 
            }
        }
        [SerializeField] private BoolReadonlyReference _serializeComponent = null;
        [SerializeField] private BoolReadonlyReference _latchOnClose = null;

        [Tooltip("The linear is considered latched if it is with this distance from the closed position")]
        [SerializeField] private FloatReadonlyReference _latchedDistance = null;

        [Inject] protected IJsonDeserializer JsonDeserializer = null;
        [Inject] protected IJsonSerializer JsonSerializer = null;

        protected Rigidbody Body { get; private set; }
        private ConfigurableJoint _configurableJoint;

        private bool _isLatched = false;

        private Vector3 _openJointAnchorPosition;
        private Vector3 _closedJointAnchorPosition;

        protected override void Awake()
        {
            base.Awake();

            Body = GetComponent<Rigidbody>();
            _configurableJoint = GetComponent<ConfigurableJoint>();

            Assert.IsFalse(_configurableJoint.autoConfigureConnectedAnchor);
            Assert.IsTrue(_configurableJoint.xMotion == ConfigurableJointMotion.Locked);
            Assert.IsTrue(_configurableJoint.yMotion == ConfigurableJointMotion.Locked);
            Assert.IsTrue(_configurableJoint.zMotion == ConfigurableJointMotion.Limited);
            Assert.IsTrue(_configurableJoint.angularXMotion == ConfigurableJointMotion.Locked);
            Assert.IsTrue(_configurableJoint.angularYMotion == ConfigurableJointMotion.Locked);
            Assert.IsTrue(_configurableJoint.angularZMotion == ConfigurableJointMotion.Locked);

            _closedJointAnchorPosition = _configurableJoint.anchor;
            _openJointAnchorPosition = _configurableJoint.anchor + (Vector3.back * _configurableJoint.linearLimit.limit);

            ClosedPosition = Body.position;

            OpenPosition = Body.position + transform.TransformDirection(Vector3.forward * 2f * _configurableJoint.linearLimit.limit);
        }

        /// <inheritdoc/>
        protected virtual void OnEnable()
        {
            ResetUpdateCoroutines();
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            StopAllCoroutines();
        }

        protected void LatchLinear()
        {
            _isLatched = true;

            _configurableJoint.anchor = _closedJointAnchorPosition;
            _configurableJoint.zMotion = ConfigurableJointMotion.Locked;
        }

        protected void UnlatchLinear()
        {
            _isLatched = false;

            _configurableJoint.anchor = _openJointAnchorPosition;
            _configurableJoint.zMotion = ConfigurableJointMotion.Limited;
        }

        protected void ResetUpdateCoroutines()
        {
            StopAllCoroutines();

            if (_latchOnClose)
            {
                if (IsInLatchRange)
                {
                    LatchLinear();
                }
                else
                {
                    UnlatchLinear();

                    StartCoroutine(Update_CheckIfClosed());
                }
            }
        }

        private YieldInstruction _checkIfClosedWait = null;
        protected IEnumerator Update_CheckIfClosed()
        {
            if (_checkIfClosedWait == null) _checkIfClosedWait = new WaitForFixedUpdate();

            if (IsInLatchRange)
            {
                if (_latchOnClose)
                {
                    LatchLinear();
                }
            }
            else
            {
                while (true)
                {
                    yield return _checkIfClosedWait;

                    if (Speed > 0f && IsInLatchRange)
                    {
                        if (_latchOnClose)
                        {
                            LatchLinear();
                        }

                        break;
                    }
                }
            }

            StopAllCoroutines();
        }
    }
}
