﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Interface for a compontent that can linearly move forward and back on the local z-axis
    /// </summary>
    public interface ILinearMover
    {
        /// <summary>
        /// Is the linear closed
        /// </summary>
        bool IsClosed { get; }

        /// <summary>
        /// The current speed of the linear
        /// </summary>
        float Speed { get; }

        /// <summary>
        /// Amount the linear is open. 0 is at <see cref="ClosedPosition"/> 1 is at <see cref="OpenPosition"/>
        /// </summary>
        float OpenAmount { get; }

        /// <summary>
        /// The world position for the closed position of the linear mover
        /// </summary>
        Vector3 ClosedPosition { get; }
        /// <summary>
        /// The world position for the open position of the linear mover
        /// </summary>
        Vector3 OpenPosition { get; }
    }
}
