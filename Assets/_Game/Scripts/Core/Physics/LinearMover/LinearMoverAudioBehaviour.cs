﻿using Game.Core.Audio;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <summary>
    /// Audio component for an instance of <see cref="ILinearMover". Plays audio on close and move />
    /// </summary>
    public class LinearMoverAudioBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] [TypeRestriction(typeof(ILinearMover))] private Component _linearMoverObj = null;
        private ILinearMover _linearMover;      

        [Space(10)]
        [SerializeField] private FloatReference _volumeMoveUpperSpeed = null;
        [SerializeField] private FloatReference _volumeMoveLowerSpeed = null;
        [SerializeField] private FloatReference _volumeMax = null;
        [SerializeField] private FloatReference _volumeMin = null;
        [SerializeField] private FloatReference _volumePlayWhileStoppedDuration = null;
        [Space(10)]
        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _openAudioClipsObj;
        private IAudioClipCollection _openAudioClips;
        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _closeAudioClipsObj;
        private IAudioClipCollection _closeAudioClips;
        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _moveAudioClipsObj;
        private IAudioClipCollection _moveAudioClips;

        private IEnumerator _updateCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_linearMoverObj);

            Assert.IsTrue(_volumeMoveUpperSpeed >= 0f);
            Assert.IsTrue(_volumeMoveLowerSpeed >= 0f);
            Assert.IsTrue(_volumeMoveLowerSpeed < _volumeMoveUpperSpeed);
            Assert.IsTrue(_volumeMin < _volumeMax);

            _linearMover = _linearMoverObj.GetComponentAsserted<ILinearMover>();

            _openAudioClips = _openAudioClipsObj != null ? (IAudioClipCollection)_openAudioClipsObj : null;
            _closeAudioClips = _closeAudioClipsObj != null ? (IAudioClipCollection)_closeAudioClipsObj : null;
            _moveAudioClips = _moveAudioClipsObj != null ? (IAudioClipCollection)_moveAudioClipsObj : null;

            _updateCoroutine = null;
        }

        /// <inheritdoc/>
        protected override void OnEnable()
        {
            base.OnEnable();

            if (_updateCoroutine == null)
            {
                _updateCoroutine = UpdateAudio();
                StartCoroutine(_updateCoroutine);
            }
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            if (_updateCoroutine != null)
            {
                StopCoroutine(_updateCoroutine);
                _updateCoroutine = null;
            }
        }

        private void UpdateVolume()
        {
            var speedLerp = Mathf.InverseLerp(_volumeMoveLowerSpeed, _volumeMoveUpperSpeed, _linearMover.Speed);
            VolumeScale.VolumeBase = Mathf.Lerp(_volumeMin, _volumeMax, speedLerp);
        }

        private IEnumerator UpdateAudio()
        {
            YieldInstruction wait = new WaitForFixedUpdate();

            yield return wait;

            bool isPlayingOpenCloseAudio = false;
            bool closedLastFrame = false;
            float stoppedDuration = 0f;

            while (true)
            {
                yield return wait;

                bool isClosed = _linearMover.IsClosed;

                if (isPlayingOpenCloseAudio)
                {
                    if (!IsPlaying)
                    {
                        isPlayingOpenCloseAudio = false;
                    }
                }
                else
                {
                    UpdateVolume();

                    if (isClosed && !closedLastFrame)
                    {
                        StopCurrentClip();

                        if (_closeAudioClips != null)
                        {
                            UpdateVolume();

                            yield return wait; // single frame delay between stopping and playing new clip

                            PlayClip(_closeAudioClips.GetNextAudioClip());

                            isPlayingOpenCloseAudio = true;
                        }
                    }
                    else if (!isClosed && closedLastFrame)
                    {
                        StopCurrentClip();

                        if (_openAudioClips != null)
                        {
                            UpdateVolume();

                            yield return wait; // single frame delay between stopping and playing new clip

                            PlayClip(_openAudioClips.GetNextAudioClip());

                            isPlayingOpenCloseAudio = true;
                        }
                    }
                    else if (_linearMover.Speed >= _volumeMoveLowerSpeed)
                    {
                        stoppedDuration = 0f;

                        if (!IsPlaying && _moveAudioClips != null)
                        {
                            PlayClip(_moveAudioClips.GetNextAudioClip());
                        }
                    }
                    else
                    {
                        stoppedDuration += Time.fixedDeltaTime;
                        if (stoppedDuration >= _volumePlayWhileStoppedDuration)
                        {
                            StopCurrentClip();
                            stoppedDuration = 0f;
                        }
                    }
                }

                closedLastFrame = isClosed;
            }
        }
    }
}
