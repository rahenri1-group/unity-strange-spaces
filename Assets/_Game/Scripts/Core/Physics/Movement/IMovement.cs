﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// An interface for the movement of an object
    /// </summary>
    public interface IMovement
    {
        /// <summary>
        /// The current position
        /// </summary>
        Vector3 Position { get; }
        /// <summary>
        /// The current velocity
        /// </summary>
        Vector3 Velocity { get; set; }
        
        /// <summary>
        /// Moves the object by <paramref name="moveAmount"/>
        /// </summary>
        /// <param name="moveAmount"></param>
        void Move(Vector3 moveAmount);

        /// <summary>
        /// Moves the object to <paramref name="newPosition"/>
        /// </summary>
        /// <param name="newPosition"></param>
        void MoveTo(Vector3 newPosition);
    }
}
