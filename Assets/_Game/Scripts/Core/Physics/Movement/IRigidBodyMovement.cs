﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// An interface controlling the movement and rotation for a <see cref="Rigidbody"/>
    /// </summary>
    public interface IRigidBodyMovement : IMovement, IRotation
    {
        /// <summary>
        /// The objects <see cref="Rigidbody"/>
        /// </summary>
        Rigidbody Rigidbody { get; }
    }
}