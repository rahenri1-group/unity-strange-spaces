﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// An interface for the rotation of an object
    /// </summary>
    public interface IRotation
    {
        /// <summary>
        /// The current rotation
        /// </summary>
        Quaternion Rotation { get; }
        /// <summary>
        /// The current angular velocity
        /// </summary>
        Vector3 AngularVelocity { get; set; } // will always be 0 for character controller movement

        /// <summary>
        /// Rotates the object by <paramref name="rotationAmount"/>
        /// </summary>
        /// <param name="rotationAmount"></param>
        void Rotate(Quaternion rotationAmount);
        /// <summary>
        /// Rotates the object to <paramref name="newRotation"/>
        /// </summary>
        /// <param name="newRotation"></param>
        void RotateTo(Quaternion newRotation);
    }
}
