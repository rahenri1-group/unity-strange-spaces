﻿using System;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="IPhysicsMaterialEventProcessor{T}"/>
    public abstract class BasePhysicsMaterialEventProcessorObject<T> : InjectedScriptableObject, IPhysicsMaterialEventProcessor<T> where T : struct
    {
        /// <inheritdoc />
        public Type PhysicsEvent => typeof(T);

        /// <inheritdoc />
        public abstract void OnPhysicsEvent(IPhysicsMaterial physicsMaterial, T eventData);
    }
}
