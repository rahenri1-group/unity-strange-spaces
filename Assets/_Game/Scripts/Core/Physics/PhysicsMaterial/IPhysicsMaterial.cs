﻿namespace Game.Core.Physics
{
    /// <summary>
    /// A physical material that responses to physics events
    /// </summary>
    public interface IPhysicsMaterial
    {
        /// <summary>
        /// The name of the physics material
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Invokes a physics event for the physics material to respond to
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="eventData"></param>
        void InvokePhysicsEvent<TEvent>(TEvent eventData) where TEvent : struct;
    }
}
