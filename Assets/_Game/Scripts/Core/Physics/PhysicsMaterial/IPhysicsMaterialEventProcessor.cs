﻿using System;

namespace Game.Core.Physics
{
    /// <summary>
    /// An event processor for a single type of physical event for a physics material
    /// </summary>
    public interface IPhysicsMaterialEventProcessor
    {
        /// <summary>
        /// The event type this can process
        /// </summary>
        Type PhysicsEvent { get; }
    }

    /// <summary>
    /// Templated version of <see cref="IPhysicsMaterialEventProcessor"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPhysicsMaterialEventProcessor<T> : IPhysicsMaterialEventProcessor where T : struct
    {
        /// <summary>
        /// Called when a physics event is raised on a physics material this is associated with 
        /// </summary>
        /// <param name="physicsMaterial"></param>
        /// <param name="eventData"></param>
        void OnPhysicsEvent(IPhysicsMaterial physicsMaterial, T eventData);
    }
}
