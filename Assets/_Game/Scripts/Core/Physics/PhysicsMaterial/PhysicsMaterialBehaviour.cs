﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="IPhysicsMaterial"/>
    public class PhysicsMaterialBehaviour : MonoBehaviour, IPhysicsMaterial
    {
        [SerializeField] private PhysicsMaterialDefinition _materialDefintiion = null;

        /// <inheritdoc />
        public string Name => _materialDefintiion.name;

        /// <inheritdoc />
        private void Awake()
        {
            Assert.IsNotNull(_materialDefintiion);
        }

        /// <inheritdoc />
        public void InvokePhysicsEvent<TEvent>(TEvent eventData) where TEvent : struct
        {
            _materialDefintiion.InvokePhysicsEvent<TEvent>(this, eventData);
        }
    }
}
