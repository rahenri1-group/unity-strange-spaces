﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// A scriptable object that defines a physics material
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Physics/Material Definition", order = -50)]
    public class PhysicsMaterialDefinition : BaseGuidScriptableObject
    {
        [SerializeField] [TypeRestriction(typeof(IPhysicsMaterialEventProcessor), false)] private UnityEngine.Object[] _physicsMaterialEventProcessorObjs = null;

        private Dictionary<Type, HashSet<IPhysicsMaterialEventProcessor>> _physicsMaterialEventMap = null;

        private void InitEventMap()
        {
            _physicsMaterialEventMap = new Dictionary<Type, HashSet<IPhysicsMaterialEventProcessor>>();

            foreach (var processorObj in _physicsMaterialEventProcessorObjs)
            {
                var processor = (IPhysicsMaterialEventProcessor)processorObj;

                if (!_physicsMaterialEventMap.ContainsKey(processor.PhysicsEvent))
                {
                    _physicsMaterialEventMap[processor.PhysicsEvent] = new HashSet<IPhysicsMaterialEventProcessor>();
                }

                _physicsMaterialEventMap[processor.PhysicsEvent].Add(processor);
            }
        }

        private IEnumerable<IPhysicsMaterialEventProcessor> GetProcessorsForType(Type type)
        {
            if (_physicsMaterialEventMap == null)
            {
                InitEventMap();
            }

            if (_physicsMaterialEventMap.ContainsKey(type))
            {
                return _physicsMaterialEventMap[type];
            }
            else
            {
                return Enumerable.Empty<IPhysicsMaterialEventProcessor>();
            }
        }

        /// <summary>
        /// Passes the physics event to the relevant <see cref="IPhysicsMaterialEventProcessor">s
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="physicsMaterial"></param>
        /// <param name="eventData"></param>
        public void InvokePhysicsEvent<TEvent>(IPhysicsMaterial physicsMaterial, TEvent eventData) where TEvent : struct
        {
            Type eventStruct = typeof(TEvent);

            foreach (var processor in GetProcessorsForType(eventStruct))
            {
                var typedProcessor = (IPhysicsMaterialEventProcessor<TEvent>)processor;
                typedProcessor.OnPhysicsEvent(physicsMaterial, eventData);
            }
        }
    }
}
