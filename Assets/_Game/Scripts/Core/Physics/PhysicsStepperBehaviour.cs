﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Triggers the simulation of a physics scene. Only one instance of this script should be present in a scene.
    /// </summary>
    public class PhysicsStepperBehaviour : MonoBehaviour
    {
        private PhysicsScene _physicsScene;

        /// <inheritdoc/>
        private void Start()
        {
            _physicsScene = gameObject.scene.GetPhysicsScene();
        }

        /// <inheritdoc/>
        private void FixedUpdate()
        {
            _physicsScene.Simulate(Time.fixedDeltaTime);
        }
    }
}
