﻿using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="IRaycastQuerier{T}"/>
    public abstract class BaseRaycastQuerier<T> : InjectedBehaviour, IRaycastQuerier<T>
        where T : class, IGameObjectComponent
    {
        /// <inheritdoc/>
        public bool HasValidResult => CurrentResult != null;
        /// <inheritdoc/>
        public T CurrentResult { get; private set; }
        /// <inheritdoc/>
        public float CurrentResultDistance { get; private set; }

        [Inject] protected ISpaceManager SpaceManager = null;
        [Inject] protected ISpacePhysics SpacePhysics = null;

        [SerializeField][Layer] private int[] _queryLayers = new int[0];
        [SerializeField] private FloatReadonlyReference _maxRaycastDistance = null;
        [SerializeField] private FloatReadonlyReference _queryFrequency = null;

        private int _queryMask;

        private IEnumerator _queryCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_maxRaycastDistance > 0f);
            Assert.IsTrue(_queryFrequency > 0f);

            _queryMask = LayerUtil.ConstructMaskForLayers(_queryLayers);

            CurrentResult = null;
            CurrentResultDistance = 0f;

            _queryCoroutine = null;
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            CurrentResult = null;
            CurrentResultDistance = 0f;

            _queryCoroutine = Query();
            StartCoroutine(_queryCoroutine);
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            if (_queryCoroutine != null)
            {
                StopCoroutine(_queryCoroutine);
                _queryCoroutine = null;
            }
        }

        private IEnumerator Query()
        {
            float queryDelay = 1f / _queryFrequency;
            YieldInstruction wait = new WaitForSeconds(queryDelay);

            yield return new WaitForSeconds(Random.Default.Range(0.1f * queryDelay, queryDelay));

            while (true)
            {
                if (SpacePhysics.Raycast(
                    SpaceManager.GetObjectSpace(gameObject),
                    transform.position,
                    transform.forward,
                    out var resultInfo,
                    _maxRaycastDistance, 
                    QueryPortalInteraction.CastThroughTransporter,
                    _queryMask))
                {
                    CurrentResultDistance = resultInfo.Distance;
                    CurrentResult = resultInfo.Collider.GetComponentInParent<T>();
                }
                else
                {
                    CurrentResultDistance = 0f;
                    CurrentResult = null;
                }

                yield return wait;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(transform.position, transform.forward);
        }
    }
}