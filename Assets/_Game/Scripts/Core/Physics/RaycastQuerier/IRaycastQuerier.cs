﻿namespace Game.Core.Physics
{
    /// <summary>
    /// Queries for an instance of <typeparamref name="T"/> in a ray the component at regular intervals 
    /// </summary>
    public interface IRaycastQuerier<T> 
        where T : IGameObjectComponent
    {
        /// <summary>
        /// Does the querier have a result to report.
        /// </summary>
        bool HasValidResult { get; }

        /// <summary>
        /// Returns current closest instance of <typeparamref name="T"/> the querier has found
        /// </summary>
        T CurrentResult { get; }

        /// <summary>
        /// Returns the distance of the <see cref="CurrentResult"/> from the raycaster's origin
        /// </summary>
        float CurrentResultDistance { get; }
    }
}