﻿using Game.Core.Interaction;

namespace Game.Core.Physics
{
    /// <summary>
    /// <see cref="ILaserInteractable"/> version of <see cref="IRaycastQuerier{T}"/>
    /// </summary>
    public class LaserInteractableRaycastQuerierBehaviour : BaseRaycastQuerier<ILaserInteractable> { }
}