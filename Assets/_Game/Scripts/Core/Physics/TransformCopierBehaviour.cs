﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <summary>
    /// Copies the transform of of <see cref="_targetTransform"/> onto this behaviour's gameobject
    /// </summary>
    public class TransformCopierBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform _targetTransform = null;

        private void Awake()
        {
            Assert.IsNotNull(_targetTransform);
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            CopyTransform();
        }

        /// <inheritdoc/>
        private void FixedUpdate()
        {
            CopyTransform();
        }

        /// <summary>
        /// Immediately sync the transform of this <see cref="GameObject"/> to <see cref="_targetTransform"/>
        /// </summary>
        public void CopyTransform()
        {
            transform.position = _targetTransform.position;
            transform.rotation = _targetTransform.rotation;
        }
    }
}
