﻿using System;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="ITriggerProximityVolume{T}"/>
    [RequireComponent(typeof(Collider))]
    public abstract class BaseTriggerProximityVolume<T_Component, T_Component_Key> : BaseTriggerVolume<T_Component, T_Component_Key>, ITriggerProximityVolume<T_Component>
        where T_Component : class, IGameObjectComponent
    {
        protected IProximityValueCalculator ProximityValueCalculator { get; private set; }

        /// <inheritdoc/>
        protected override  void Awake()
        {
            base.Awake();
            ProximityValueCalculator = this.GetComponentAsserted<IProximityValueCalculator>();
        }

        /// <inheritdoc/>
        public T_Component GetClosest()
        {
            return GetClosest(null, out float _);
        }

        /// <inheritdoc/>
        public T_Component GetClosest(out float proximityValue)
        {
            return GetClosest(null, out proximityValue);
        }

        /// <inheritdoc/>
        public T_Component GetClosest(Func<T_Component, bool> validator)
        {
            return GetClosest(validator, out float proximityValue);
        }

        /// <inheritdoc/>
        public T_Component GetClosest(Func<T_Component, bool> validator, out float proximityValue)
        {
            T_Component closest = null;
            float closestProximityValue = float.MaxValue;

            foreach (var key in ComponentMap.Keys)
            {
                var componentInfo = ComponentMap[key];

                if (componentInfo.GameObject) // ensure that base gameobject hasn't been destroyed
                {
                    var gameObjectComponent = componentInfo.GameObjectComponent;

                    if (validator == null || validator(gameObjectComponent))
                    {
                        if (closest == null)
                        {
                            closest = gameObjectComponent;
                            closestProximityValue = ProximityValueCalculator.ProximityValue(gameObjectComponent.GameObject.transform);
                        }
                        else
                        {
                            DetermineComponentCloser(closest, closestProximityValue, gameObjectComponent, out closest, out closestProximityValue);
                        }
                    }
                }
            }

            proximityValue = closestProximityValue;

            return closest;
        }

        protected virtual void DetermineComponentCloser(T_Component currentClosest, float currentClosestProximity, T_Component otherToEvaluate, out T_Component newClosest, out float newClosestProximity)
        {
            float otherToEvaluateProximity = ProximityValueCalculator.ProximityValue(otherToEvaluate.GameObject.transform);

            if (otherToEvaluateProximity < currentClosestProximity)
            {
                newClosest = otherToEvaluate;
                newClosestProximity = otherToEvaluateProximity;
            }
            else
            {
                newClosest = currentClosest;
                newClosestProximity = currentClosestProximity;
            }
        }
    }
}