﻿using System;

namespace Game.Core.Physics
{
    /// <summary>
    /// A trigger volume that can test if it contains an item and can return the closest item to a point
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITriggerProximityVolume<T> : ITriggerVolume<T>
        where T : IGameObjectComponent
    {
        /// <summary>
        /// Gets the closest object
        /// </summary>
        /// <returns></returns>
        T GetClosest();

        /// <summary>
        /// Gets the closest object and distance
        /// </summary>
        /// <param name="proximityValue"></param>
        /// <returns></returns>
        T GetClosest(out float proximityValue);

        /// <summary>
        /// Gets the closest object that matches the provided <paramref name="validator"/>
        /// </summary>
        /// <param name="validator"></param>
        /// <returns></returns>
        T GetClosest(Func<T, bool> validator);

        /// <summary>
        /// Gets the closest object and distance that matches the provided <paramref name="validator"/>
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="proximityValue"></param>
        /// <returns></returns>
        T GetClosest(Func<T, bool> validator, out float proximityValue);
    }
}
