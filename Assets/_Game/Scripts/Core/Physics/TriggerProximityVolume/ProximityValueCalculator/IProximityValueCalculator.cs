﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Calculates the proximity of a transform to the object
    /// </summary>
    public interface IProximityValueCalculator
    {
        /// <summary>
        /// The distance of <paramref name="transformToEvaluate"/> to the object
        /// </summary>
        /// <param name="transformToEvaluate"></param>
        /// <returns></returns>
        float ProximityValue(Transform transformToEvaluate);
    }
}