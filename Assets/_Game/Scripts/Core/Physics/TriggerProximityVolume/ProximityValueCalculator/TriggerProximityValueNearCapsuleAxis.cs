﻿using Game.Core.Math;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// A capsule based verion of <see cref="IProximityValueCalculator"/>
    /// </summary>
    [RequireComponent(typeof(CapsuleCollider))]
    public class TriggerProximityValueNearCapsuleAxis : MonoBehaviour, IProximityValueCalculator
    {
        private CapsuleCollider _capsule;

        /// <inheritdoc/>
        private void Awake()
        {
            _capsule = GetComponent<CapsuleCollider>();
        }

        /// <inheritdoc/>
        public float ProximityValue(Transform transformToEvaluate)
        {
            var ray = new Ray(transform.TransformPoint(_capsule.center), CalculateDirection());

            return MathUtil.DistanceToLine(ray, transformToEvaluate.position);
        }

        private Vector3 CalculateDirection()
        {
            switch (_capsule.direction)
            {
                case 0: // x
                    return transform.right;
                case 1: // y
                    return transform.up;
                case 2: // z
                default:
                    return transform.forward;
            }
        }
    }
}
