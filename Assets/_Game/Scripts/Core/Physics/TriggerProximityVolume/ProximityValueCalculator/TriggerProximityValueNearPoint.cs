﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Point based implementaion of <see cref="IProximityValueCalculator"/>
    /// </summary>
    public class TriggerProximityValueNearPoint : MonoBehaviour, IProximityValueCalculator
    {
        [SerializeField] private Transform _point = null;

        /// <inheritdoc/>
        public float ProximityValue(Transform transformToEvaluate)
        {
            var position = _point != null ? _point.position : transform.position;

            return Vector3.Distance(transformToEvaluate.position, position);
        }
    }
}