﻿using Game.Core.Interaction;

namespace Game.Core.Physics
{
    /// <summary>
    /// <see cref="IWorldInteractable"/> version of <see cref="ITriggerProximityVolume{T}"/>
    /// </summary>
    public class WorldInteractableTriggerProximityVolume : BaseTriggerProximityVolume<IWorldInteractable, IWorldInteractable> 
    {
        /// <inheritdoc/>
        protected override IWorldInteractable GetKeyForComponent(IWorldInteractable component)
        {
            return component;
        }

        protected override void DetermineComponentCloser(IWorldInteractable currentClosest, float currentClosestProximity, IWorldInteractable otherToEvaluate, out IWorldInteractable newClosest, out float newClosestProximity)
        {
            if (currentClosest.InteractionPriority != otherToEvaluate.InteractionPriority)
            {
                if (otherToEvaluate.InteractionPriority < currentClosest.InteractionPriority)
                {
                    newClosest = otherToEvaluate;
                    newClosestProximity = ProximityValueCalculator.ProximityValue(otherToEvaluate.GameObject.transform);
                }
                else
                {
                    newClosest = currentClosest;
                    newClosestProximity = currentClosestProximity;
                }
            }
            else
            {
                base.DetermineComponentCloser(currentClosest, currentClosestProximity, otherToEvaluate, out newClosest, out newClosestProximity);
            }
        }
    }
}