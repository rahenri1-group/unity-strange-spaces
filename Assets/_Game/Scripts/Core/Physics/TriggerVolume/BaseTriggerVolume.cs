﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <inheritdoc cref="ITriggerVolume{T}"/>
    [RequireComponent(typeof(Collider))]
    public abstract class BaseTriggerVolume<T_Component, T_Component_Key> : InjectedBehaviour, ITriggerVolume<T_Component>
        where T_Component : class, IGameObjectComponent
    {
        protected class ComponentInfo
        {
            /// <summary>
            /// The key used to identify this component
            /// </summary>
            public T_Component_Key ComponentKey;

            /// <summary>
            /// The tracked component class
            /// </summary>
            public T_Component GameObjectComponent;

            /// <summary>
            /// The gameobject for the main component
            /// </summary>
            public GameObject GameObject;

            /// <summary>
            /// All of the Unity colliders of the gameobject currently within the trigger
            /// </summary>
            public HashSet<Collider> ColliderSet;

            /// <summary>
            /// Was at least one collider of the object within the trigger this physics frame
            /// </summary>
            public bool IsInTriggerThisFrame;
        }

        /// <inheritdoc/>
        public event TriggerVolumeEventHandler<T_Component> GameObjectEnter;
        /// <inheritdoc/>
        public event TriggerVolumeEventHandler<T_Component> GameObjectExit;

        /// <inheritdoc/>
        public int VolumeCount => ComponentMap.Count;

        /// <summary>
        /// Each component will have a single <see cref="ComponentInfo"/>, mapped by their component key
        /// </summary>
        protected Dictionary<T_Component_Key, ComponentInfo> ComponentMap;

        /// <summary>
        /// Each sub collider in a component will be mapped to the components key for easy lookup
        /// </summary>
        protected Dictionary<Collider, T_Component_Key> ColliderMap;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(GetComponent<Collider>().isTrigger);

            ComponentMap = new Dictionary<T_Component_Key, ComponentInfo>();
            ColliderMap = new Dictionary<Collider, T_Component_Key>();
        }

        /// <inheritdoc/>
        private void OnTriggerEnter(Collider other)
        {
            if (other.isTrigger) return;

            T_Component gameObjectComponent = other.GetComponentInParent<T_Component>();
            if (gameObjectComponent == null) return;

            T_Component_Key componentKey = GetKeyForComponent(gameObjectComponent);

            if (ComponentMap.ContainsKey(componentKey) == false)
            {
                ComponentMap.Add(
                    componentKey,
                    new ComponentInfo
                    {
                        ComponentKey = componentKey,
                        GameObject = gameObjectComponent.GameObject,
                        GameObjectComponent = gameObjectComponent,
                        ColliderSet = new HashSet<Collider>(),
                        IsInTriggerThisFrame = true
                    });

                GameObjectEnter?.Invoke(this, gameObjectComponent);
            }

            ComponentMap[componentKey].IsInTriggerThisFrame = true;

            if (ComponentMap[componentKey].ColliderSet.Contains(other) == false)
            {
                ComponentMap[componentKey].ColliderSet.Add(other);
                ColliderMap[other] = componentKey;
            }
        }

        /// <inheritdoc/>
        private void OnTriggerStay(Collider other)
        {
            if (other.isTrigger) return;

            if (!ColliderMap.ContainsKey(other))
            {
                return;
            }

            var componentKey = ColliderMap[other];
            ComponentMap[componentKey].IsInTriggerThisFrame = true;
        }

        /// <inheritdoc/>
        private void FixedUpdate()
        {
            Purge();
        }

        /// <inheritdoc/>
        public void Purge()
        {
            _purgeList.Clear();

            foreach (var pair in ComponentMap)
            {
                var candidateColliderInfo = pair.Value;

                if (!candidateColliderInfo.IsInTriggerThisFrame)
                {
                    _purgeList.Add(candidateColliderInfo);
                }

                // reset flag to be set on next stay
                candidateColliderInfo.IsInTriggerThisFrame = false;
            }

            foreach (var info in _purgeList)
            {
                PurgeInfo(info);
            }
        }
        private List<ComponentInfo> _purgeList = new List<ComponentInfo>();

        /// <summary>
        /// Returns the key to be used with the provided <paramref name="component"/>
        /// </summary>
        protected abstract T_Component_Key GetKeyForComponent(T_Component component);

        protected void PurgeComponent(T_Component_Key componentKey)
        {
            if (ComponentMap.ContainsKey(componentKey) == false) return;
            
            var info = ComponentMap[componentKey];
            PurgeInfo(info);
        }

        private void PurgeInfo(ComponentInfo componentInfo)
        {
            foreach (var collider in componentInfo.ColliderSet)
            {
                ColliderMap.Remove(collider);
            }

            GameObjectExit?.Invoke(this, componentInfo.GameObjectComponent);

            ComponentMap.Remove(componentInfo.ComponentKey);
        }
    }
}
