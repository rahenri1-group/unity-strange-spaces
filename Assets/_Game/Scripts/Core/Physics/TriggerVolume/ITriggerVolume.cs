﻿namespace Game.Core.Physics
{
    /// <summary>
    /// Delegate for <see cref="ITriggerVolume{T}"/> events
    /// </summary>
    public delegate void TriggerVolumeEventHandler<T>(ITriggerVolume<T> sender, T other) where T : IGameObjectComponent;

    /// <summary>
    /// A trigger volume that can test if it contains an item
    /// </summary>
    public interface ITriggerVolume <T>
        where T : IGameObjectComponent
    {
        /// <summary>
        /// Raised when a {T} enters
        /// </summary>
        event TriggerVolumeEventHandler<T> GameObjectEnter;

        /// <summary>
        /// Raised when a {T} exits
        /// </summary>
        event TriggerVolumeEventHandler<T> GameObjectExit;

        /// <summary>
        /// The number of {T}s currently in the volume
        /// </summary>
        int VolumeCount { get; }

        /// <summary>
        /// Cleans up disabled objects or objects with disabled colliders 
        /// </summary>
        void Purge();
    }
}
