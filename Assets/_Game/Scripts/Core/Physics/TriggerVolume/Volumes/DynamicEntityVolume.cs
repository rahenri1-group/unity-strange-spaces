﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using System;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Trigger volume for <see cref="IDynamicEntity"/>
    /// </summary>
    public class DynamicEntityVolume : BaseTriggerVolume<IDynamicEntity, Guid> 
    {
        [Inject] private IEventBus _eventBus = null;

        /// <inheritdoc/>
        private void OnEnable()
        {
            _eventBus.Subscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _eventBus.Unsubscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        /// <inheritdoc/>
        protected override Guid GetKeyForComponent(IDynamicEntity component)
        {
            return component.EntityId;
        }

        private void OnEntityDestroy(EntityDestroyEvent eventData)
        {
            PurgeComponent(eventData.EntityId);
        }
    }
}
