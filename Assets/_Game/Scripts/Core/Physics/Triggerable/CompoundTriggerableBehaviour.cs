﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// A triggerable made of multiple <see cref="ITriggerable"/>
    /// </summary>
    public class CompoundTriggerableBehaviour : MonoBehaviour, ITriggerable
    {
        private class ColliderInfo
        {
            // The triggers the collider is currently in
            public HashSet<ITriggerable> ColliderTriggers { get; private set; }

            public ColliderInfo()
            {
                ColliderTriggers = new HashSet<ITriggerable>();
            }
        }

        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerEnter;
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerExit;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component[] _triggerableObjs = null;
        private ITriggerable[] _triggerables;

        private Dictionary<Collider, ColliderInfo> _colliderMap;

        /// <inheritdoc/>
        private void Awake()
        {
            _triggerables = _triggerableObjs.GetComponentArrayAsserted<ITriggerable>();

            _colliderMap = new Dictionary<Collider, ColliderInfo>();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            foreach (var triggerable in _triggerables)
            {
                triggerable.TriggerEnter += OnTriggerableEnter;
                triggerable.TriggerExit += OnTriggerableExit;
            }
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            foreach (var triggerable in _triggerables)
            {
                triggerable.TriggerEnter -= OnTriggerableEnter;
                triggerable.TriggerExit -= OnTriggerableExit;
            }
        }

        private void OnTriggerableEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            Collider other = args.Collider;

            if (!_colliderMap.ContainsKey(other))
                _colliderMap.Add(other, new ColliderInfo());

            _colliderMap[other].ColliderTriggers.Add(sender);

            if (_colliderMap[other].ColliderTriggers.Count == 1)
                TriggerEnter?.Invoke(sender, args);
        }

        private void OnTriggerableExit(ITriggerable sender, TriggerableEventArgs args)
        {
            Collider other = args.Collider;

            if (_colliderMap.ContainsKey(other))
            {
                _colliderMap[other].ColliderTriggers.Remove(sender);

                if (_colliderMap[other].ColliderTriggers.Count == 0)
                    _colliderMap.Remove(other);
            }

            if (!_colliderMap.ContainsKey(other))
                TriggerExit?.Invoke(sender, args);
        }
    }
}
