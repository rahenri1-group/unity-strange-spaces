﻿using UnityEngine;

namespace Game.Core.Physics
{
    /// <summary>
    /// Delegate for <see cref="ITriggerable"/> events
    /// </summary>
    public delegate void TriggerableEventHandler(ITriggerable sender, TriggerableEventArgs args);

    /// <summary>
    /// Arguements for <see cref="TriggerableEventHandler"/>
    /// </summary>
    public struct TriggerableEventArgs
    {
        /// <summary>
        /// The collider trigger associated with the <see cref="ITriggerable"/>.
        /// </summary>
        public Collider Trigger;

        /// <summary>
        /// The collider that interacted with the <see cref="Trigger"/>.
        /// </summary>
        public Collider Collider;
    }

    /// <summary>
    /// A game object component that raises an event when a collider enters and exits its trigger zone
    /// </summary>
    public interface ITriggerable : IGameObjectComponent
    {
        /// <summary>
        /// Raised when a collider enters
        /// </summary>
        event TriggerableEventHandler TriggerEnter;

        /// <summary>
        /// Raised when a collider exits
        /// </summary>
        event TriggerableEventHandler TriggerExit;
    }
}
