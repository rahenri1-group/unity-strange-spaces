﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <summary>
    /// A simple implementation of <see cref="ITriggerable"/>
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class SimpleTriggerableBehaviour : MonoBehaviour, ITriggerable
    {
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerEnter;
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerExit;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        private Collider _triggerCollider;

        /// <inheritdoc/>
        private void Awake()
        {
            _triggerCollider = GetComponent<Collider>();
            Assert.IsTrue(_triggerCollider.isTrigger);
        }

        /// <inheritdoc/>
        private void OnTriggerEnter(Collider other)
        {
            if (other.isTrigger) return;

            TriggerEnter?.Invoke(this, new TriggerableEventArgs
            {
                Trigger = _triggerCollider,
                Collider = other
            });
        }

        /// <inheritdoc/>
        private void OnTriggerExit(Collider other)
        {
            if (other.isTrigger) return;

            TriggerExit?.Invoke(this, new TriggerableEventArgs
            {
                Trigger = _triggerCollider,
                Collider = other
            });
        }
    }
}
