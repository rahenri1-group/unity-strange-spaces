﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Physics
{
    /// <summary>
    /// An implementation of <see cref="ITriggerable"/> that checks each frame if any colliders have exited (telelport, deactivated, destroyed, etc)
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class StayTestTriggerableBehaviour : MonoBehaviour, ITriggerable
    {
        private class ColliderInfo
        {
            public bool IsInTriggerThisFrame;
        }

        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerEnter;
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerExit;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        private Dictionary<Collider, ColliderInfo> _currentColliders;

        private Collider _triggerCollider;

        /// <inheritdoc/>
        private void Awake()
        {
            _triggerCollider = GetComponent<Collider>();
            Assert.IsTrue(_triggerCollider.isTrigger);

            _currentColliders = new Dictionary<Collider, ColliderInfo>();
        }

        /// <inheritdoc/>
        private void OnTriggerEnter(Collider other)
        {
            if (other.isTrigger) return;

            if (!_currentColliders.ContainsKey(other))
            {
                _currentColliders.Add(other, new ColliderInfo());

                TriggerEnter?.Invoke(this, new TriggerableEventArgs
                {
                    Trigger = _triggerCollider,
                    Collider = other
                });
            }

            _currentColliders[other].IsInTriggerThisFrame = true;
        }

        /// <inheritdoc/>
        private void OnTriggerStay(Collider other)
        {
            if (other.isTrigger) return;

            if (!_currentColliders.ContainsKey(other))
            {
                _currentColliders.Add(other, new ColliderInfo());

                TriggerEnter?.Invoke(this, new TriggerableEventArgs
                {
                    Trigger = _triggerCollider,
                    Collider = other
                });
            }

            _currentColliders[other].IsInTriggerThisFrame = true;
        }

        /// <inheritdoc/>
        private void FixedUpdate()
        {
            Purge();
        }

        private List<Collider> _purgeList = new List<Collider>();
        private void Purge()
        {
            _purgeList.Clear();

            foreach (var pair in _currentColliders)
            {
                var candidateCollider = pair.Key;
                var candidateColliderInfo = pair.Value;

                if (!candidateColliderInfo.IsInTriggerThisFrame)
                {
                    TriggerExit?.Invoke(this,
                        new TriggerableEventArgs
                        {
                            Trigger = _triggerCollider,
                            Collider = candidateCollider
                        });

                    _purgeList.Add(candidateCollider);
                }

                // reset flag to be set on next stay
                candidateColliderInfo.IsInTriggerThisFrame = false;
            }

            foreach (var purge in _purgeList)
            {
                _currentColliders.Remove(purge);
            }
        }
    }
}