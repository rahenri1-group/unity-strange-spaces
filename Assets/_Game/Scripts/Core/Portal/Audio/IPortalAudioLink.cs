﻿using Game.Core.Audio;

namespace Game.Core.Portal
{
    /// <summary>
    /// A portal that can link audio
    /// </summary>
    public interface IPortalAudioLink : IPortalPathLink 
    {
        /// <summary>
        /// The <see cref="IAudioModifierDefinition"/>s applied to the linked audio by this path link
        /// </summary>
        IAudioModifierDefinition[] AudioModifiers { get; set; }
    }
}
