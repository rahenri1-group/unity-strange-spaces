﻿using Game.Core.Audio;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalAudioLink"/>
    public class PortalAudioLinkBehaviour : MonoBehaviour, IPortalAudioLink
    {
        /// <inheritdoc/>
        public IPortal EntryPortal 
        { 
            get
            {
                if (_entryPortal == null) _entryPortal = this.GetComponentAsserted<IPortal>();
                return _entryPortal;
            }
        }
        private IPortal _entryPortal = null;

        /// <inheritdoc/>
        public IPortal ExitPortal => EntryPortal.EndPoint;

        /// <inheritdoc/>
        public IAudioModifierDefinition[] AudioModifiers { get; set; } = new IAudioModifierDefinition[0];
    }
}
