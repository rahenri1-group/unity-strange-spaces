﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Math;
using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortal"/>
    public abstract class BasePortal : SpaceInjectedBehaviour, IPortal
    {
        /// <inheritdoc />
        public event PortalEvent PortalOpened;
        /// <inheritdoc />
        public event PortalEvent PortalClosed;

        /// <inheritdoc />
        public GameObject GameObject => gameObject;
        /// <inheritdoc />
        public Transform Transform => transform;

        /// <inheritdoc />
        public IPortal EndPoint { get; private set; } = null;
        /// <inheritdoc />
        public bool IsOpen { get => EndPoint != null; }
        /// <inheritdoc />
        public abstract bool HasConnection { get; }

        /// <inheritdoc />
        public ISpaceData Space => CurrentSpace;

        /// <inheritdoc />
        public Vector2 PortalSize => transform.localScale;
        /// <inheritdoc />
        public Plane PortalPlane => new Plane(transform.forward, transform.position);

        [Inject] protected ILogRouter Logger = null;

        protected override void OnSpaceLoaded()
        {
            base.OnSpaceLoaded();

            EventBus.Subscribe<ModulesShutdownEvent>(OnModulesShutdown);
        }

        private void OnModulesShutdown(ModulesShutdownEvent eventData)
        {
            if (IsOpen)
            {
                Close();
            }

            EventBus.Unsubscribe<ModulesShutdownEvent>(OnModulesShutdown);
        }

        /// <inheritdoc />
        public abstract void AttemptOpen();
        /// <inheritdoc />
        public abstract void Close();

        /// <inheritdoc />
        public void OnPortalOpened(IPortalConnection portalConnection, IPortal otherEndPoint)
        {
            EndPoint = otherEndPoint;

            PortalOpened?.Invoke(this);

            EventBus.InvokeEvent(new PortalOpenedEvent
            {
                Portal = this
            });
        }

        /// <inheritdoc />
        public void OnPortalClosed()
        {
            EndPoint = null;

            PortalClosed?.Invoke(this);

            EventBus.InvokeEvent(new PortalClosedEvent
            {
                Portal = this
            });
        }

        /// <inheritdoc />
        public void CalculateEndPointTransform(Vector3 inputPosition, Quaternion inputRotation, out Vector3 endPointPosition, out Quaternion endPointRotation)
        {
            endPointPosition = CalculateEndPointPosition(inputPosition);
            endPointRotation = CalculateEndPointRotation(inputRotation);
        }

        /// <inheritdoc />
        public Vector3 CalculateEndPointPosition(Vector3 inputPosition)
        {
            if (!IsOpen)
            {
                return inputPosition;
            }

            var offsetVector = MathUtil.ReverseAxisY * transform.InverseTransformPoint(inputPosition);
            return EndPoint.Transform.TransformPoint(offsetVector);
        }

        /// <inheritdoc />
        public Quaternion CalculateEndPointRotation(Quaternion inputRotation)
        {
            if (!IsOpen)
            {
                return inputRotation;
            }

            return EndPoint.Transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(transform.rotation) * inputRotation;
        }

        /// <inheritdoc />
        public Vector3 CalculateEndPointDirection(Vector3 inputDirection)
        {
            if (!IsOpen)
            {
                return inputDirection;
            }

            return EndPoint.Transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(transform.rotation) * inputDirection;
        }

        /// <inheritdoc/>
        public Vector3 InverseCalculateEndPointPosition(Vector3 inputPosition)
        {
            if (!IsOpen)
            {
                return inputPosition;
            }

            var offsetVector = MathUtil.ReverseAxisY * EndPoint.Transform.InverseTransformPoint(inputPosition);
            return Transform.TransformPoint(offsetVector);
        }

        /// <inheritdoc/>
        public Quaternion InverseCalculateEndPointRotation(Quaternion inputRotation)
        {
            if (!IsOpen)
            {
                return inputRotation;
            }

            return transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(EndPoint.Transform.rotation) * inputRotation;
        }

        /// <inheritdoc/>
        public Vector3 InverseCalculateEndPointDirection(Vector3 inputDirection)
        {
            if (!IsOpen)
            {
                return inputDirection;
            }

            return transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(EndPoint.Transform.rotation) * inputDirection;
        }
    }
}
