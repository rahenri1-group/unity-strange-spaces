﻿using Game.Core.DependencyInjection;
using Game.Core.Render.Camera;
using UnityEngine;

namespace Game.Core.Portal
{
    public abstract class BasePortalCamera : BaseSpaceCamera, IPortalCamera
    {
        private const float _clipPlaneOffset = 0f;

        /// <inheritdoc/>
        public override bool Enabled
        {
            get => base.Enabled;
            set
            {
                base.Enabled = value;

                if (value)
                {
                    RenderedSpace = Portal.EndPoint.Space;
                }
            }
        }

        /// <inheritdoc/>
        public override Game.Core.Render.Camera.CameraType CameraType => Game.Core.Render.Camera.CameraType.EffectCamera;

        /// <inheritdoc/>
        public IPortal Portal { get; private set; } = null;

        [Inject] protected ICameraManager CameraManager = null;

        protected Renderer PortalRenderer { get; private set; }

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            UnityCamera.enabled = false;
        }

        /// <inheritdoc/>
        public virtual void SetupForPortal(IPortal portal, Renderer portalRenderer)
        {
            if (Portal != null) throw new PortalException($"Setup called multiple times for portal camera {GameObject.name}");

            Portal = portal;
            PortalRenderer = portalRenderer;
        }

        public virtual void Render()
        {
            if (Portal == null) throw new PortalException($"Setup not called for portal camera {GameObject.name}");
        }

        protected void CopyCameraSettings(ICamera primaryCamera)
        {
            UnityCamera.farClipPlane = primaryCamera.UnityCamera.farClipPlane;
            UnityCamera.nearClipPlane = primaryCamera.UnityCamera.nearClipPlane;
            UnityCamera.orthographic = primaryCamera.UnityCamera.orthographic;
            UnityCamera.fieldOfView = primaryCamera.UnityCamera.fieldOfView;
            UnityCamera.aspect = primaryCamera.UnityCamera.aspect;
            UnityCamera.orthographicSize = primaryCamera.UnityCamera.orthographicSize;
        }

        protected Vector4 CameraSpacePlane(Matrix4x4 m, Vector3 pos, Vector3 normal, float sideSign)
        {
            Vector3 offsetPos = pos + normal * _clipPlaneOffset;
            Vector3 cpos = m.MultiplyPoint(offsetPos);
            Vector3 cnormal = m.MultiplyVector(normal).normalized * sideSign;
            return new Vector4(cnormal.x, cnormal.y, cnormal.z, -Vector3.Dot(cpos, cnormal));
        }
    }

}
