﻿using Game.Core.Render.Camera;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// A camera for renderering a portal
    /// </summary>
    public interface IPortalCamera : ICamera
    {
        /// <summary>
        /// The portal the camera renders
        /// </summary>
        IPortal Portal { get; }

        /// <summary>
        /// Sets up the portal camera for a <see cref="IPortal"/>
        /// </summary>
        void SetupForPortal(IPortal portal, Renderer portalRenderer);


        /// <summary>
        /// Renderers the portal
        /// </summary>
        void Render();
    }
}
