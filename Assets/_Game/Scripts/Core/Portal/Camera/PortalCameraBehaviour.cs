﻿using Game.Core.Math;
using UnityEngine;

namespace Game.Core.Portal
{
    public class PortalCameraBehaviour : BasePortalCamera
    {
        /// <inheritdoc/>
        public override bool Enabled
        {
            get => base.Enabled;
            set
            {
                base.Enabled = value;

                if (value)
                {
                    UpdateRenderTexture();
                }
            }
        }

        private int _renderTextureShaderId;

        private RenderTexture _portalTexture = null;

        /// <inheritdoc/>
        public override void SetupForPortal(IPortal portal, Renderer portalRenderer)
        {
            base.SetupForPortal(portal, portalRenderer);

            _renderTextureShaderId = Shader.PropertyToID("_BaseMap");
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_portalTexture != null)
            {
                _portalTexture.Release();
                _portalTexture = null;
            }
        }

        /// <inheritdoc/>
        public override void Render()
        {
            base.Render();

            var primaryCamera = CameraManager.ActivePrimaryCamera;

            var offsetVector = Portal.Transform.InverseTransformPoint(primaryCamera.UnityCamera.transform.position);
            offsetVector = MathUtil.ReverseAxisY * offsetVector;

            transform.position = Portal.EndPoint.Transform.TransformPoint(offsetVector);
            transform.rotation = Portal.EndPoint.Transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(Portal.Transform.rotation) * primaryCamera.UnityCamera.transform.rotation;

            Vector4 ClipPlane = CameraSpacePlane(UnityCamera.worldToCameraMatrix, Portal.EndPoint.Transform.position, Portal.EndPoint.Transform.TransformDirection(Vector3.back), 1.0f);
            Matrix4x4 projection = primaryCamera.UnityCamera.CalculateObliqueMatrix(ClipPlane);

            CopyCameraSettings(primaryCamera);
            UnityCamera.projectionMatrix = projection;
            UnityCamera.Render();
        }

        private void UpdateRenderTexture()
        {
            if (_portalTexture != null && _portalTexture.width == Screen.width && _portalTexture.height == Screen.height) return;

            if (_portalTexture != null)
            {
                _portalTexture.Release();
                _portalTexture = null;
            }

            _portalTexture = new RenderTexture(Screen.width, Screen.height, 32);
            _portalTexture.name = Portal.GameObject.name + "_RenderTexture";
            _portalTexture.Create();

            UnityCamera.targetTexture = _portalTexture;

            PortalRenderer.material.SetTexture(_renderTextureShaderId, _portalTexture);
        }
    }
}