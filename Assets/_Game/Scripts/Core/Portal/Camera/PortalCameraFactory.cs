﻿using Cysharp.Threading.Tasks;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using Game.Core.Resource;

namespace Game.Core.Portal
{
    /// <summary>
    /// Factory for <see cref="IPortalCamera"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICameraFactory),
        lifetime: Lifetime.Singleton)]
    public class PortalCameraFactory : BaseCameraFactory<IPortalCamera>
    {
        /// <inheritdoc/>
        public override CameraFactoryConfig FactoryConfig => Config;
        public CameraFactoryConfig Config = new CameraFactoryConfig();

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PortalCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ISpaceManager spaceManager)
            : base(gameObjectResourceManager, spaceManager) { }

        /// <inheritdoc/>
        public async override UniTask<ICamera> CreateCamera()
        {
            var camera = await CreateEmptyCamera();
            return camera.gameObject.AddComponent<PortalCameraBehaviour>();
        }
    }
}
