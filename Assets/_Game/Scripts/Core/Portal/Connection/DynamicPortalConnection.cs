﻿using System;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IDynamicPortalConnection" />
    public class DynamicPortalConnection : IDynamicPortalConnection
    {
        /// <inheritdoc />
        public Guid PortalConnectionId { get; private set; }

        /// <inheritdoc />
        public string Name => PortalConnectionId.ToString();
        
        /// <inheritdoc />
        public Guid PortalIdA { get; private set; }
        /// <inheritdoc />
        public Guid SpaceIdA { get; private set; }

        /// <inheritdoc />
        public Guid PortalIdB { get; private set; }
        /// <inheritdoc />
        public Guid SpaceIdB { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DynamicPortalConnection(Guid portalIdA, Guid spaceIdA, Guid portalIdB, Guid spaceIdB)
        {
            PortalConnectionId = Guid.NewGuid();

            PortalIdA = portalIdA;
            SpaceIdA = spaceIdA;

            PortalIdB = portalIdB;
            SpaceIdB = spaceIdB;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DynamicPortalConnection(IDynamicPortal portalA, Guid portalIdB, Guid spaceIdB)
        {
            PortalConnectionId = Guid.NewGuid();

            PortalIdA = portalA.PortalId;
            SpaceIdA = portalA.Space.SpaceId;

            PortalIdB = portalIdB;
            SpaceIdB = spaceIdB;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DynamicPortalConnection(IDynamicPortal portalA, IDynamicPortal portalB)
        {
            PortalConnectionId = Guid.NewGuid();

            PortalIdA = portalA.PortalId;
            SpaceIdA = portalA.Space.SpaceId;

            PortalIdB = portalB.PortalId;
            SpaceIdB = portalB.Space.SpaceId;
        }
    }
}
