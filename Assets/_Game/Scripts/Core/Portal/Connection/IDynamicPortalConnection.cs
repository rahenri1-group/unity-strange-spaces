﻿using System;

namespace Game.Core.Portal
{
    /// <summary>
    /// Interface for a connection between two <see cref="IDynamicPortal"/>s
    /// </summary>
    public interface IDynamicPortalConnection : IPortalConnection
    {
        /// <summary>
        /// One of the dynamic portal ids
        /// </summary>
        Guid PortalIdA { get; }
        /// <summary>
        /// The other one of the dynamic portal ids
        /// </summary>
        Guid PortalIdB { get; }
    }
}
