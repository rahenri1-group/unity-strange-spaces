﻿using System;

namespace Game.Core.Portal
{
    /// <summary>
    /// Interface describing a connection between two portals
    /// </summary>
    public interface IPortalConnection
    {
        /// <summary>
        /// Unique id for the connection
        /// </summary>
        Guid PortalConnectionId { get; }
        /// <summary>
        /// The name of the connection
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Id of one the spaces the portal connection links
        /// </summary>
        Guid SpaceIdA { get; }
        /// <summary>
        /// Id of one the spaces the portal connection links
        /// </summary>
        Guid SpaceIdB { get; }
    }
}
