﻿namespace Game.Core.Portal
{
    /// <summary>
    /// Interface for a connection between two <see cref="IStaticPortal"/>s
    /// </summary>
    public interface IStaticPortalConnection : IPortalConnection { }
}
