﻿using System;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IStaticPortalConnection"/>
    [CreateAssetMenu(menuName = "Game/Space/Portal/Static Portal Connection")]
    public class StaticPortalConnection : ScriptableObject, IStaticPortalConnection
    {
        /// <inheritdoc />
        public string Name => name + ":" + PortalConnectionId;

        /// <inheritdoc />
        public Guid PortalConnectionId
        {
            get
            {
                if (_parsedPortalConnectionId == Guid.Empty) _parsedPortalConnectionId = Guid.Parse(_portalConnectionId);
                return _parsedPortalConnectionId;
            }
        }
        private Guid _parsedPortalConnectionId = Guid.Empty;

        /// <inheritdoc />
        public Guid SpaceIdA
        {
            get
            {
                if (_parsedSpaceIdA == Guid.Empty) _parsedSpaceIdA = Guid.Parse(_spaceIdA);
                return _parsedSpaceIdA;
            }
        }
        private Guid _parsedSpaceIdA = Guid.Empty;

        /// <inheritdoc />
        public Guid SpaceIdB
        {
            get
            {
                if (_parsedSpaceIdB == Guid.Empty) _parsedSpaceIdB = Guid.Parse(_spaceIdB);
                return _parsedSpaceIdB;
            }
        }
        private Guid _parsedSpaceIdB = Guid.Empty;

        [SerializeField] [ReadOnly] private string _portalConnectionId = string.Empty;

        [SerializeField] private string _spaceIdA = string.Empty;
        [SerializeField] private string _spaceIdB = string.Empty;

        /// <inheritdoc />
        private void OnValidate()
        {
            if (string.IsNullOrEmpty(_portalConnectionId))
            {
                _portalConnectionId = Guid.NewGuid().ToString();
            }
        }
    }
}