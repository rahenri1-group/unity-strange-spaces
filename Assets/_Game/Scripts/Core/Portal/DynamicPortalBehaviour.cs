﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IDynamicPortal" />
    public class DynamicPortalBehaviour : BasePortal, IDynamicPortal
    {
        [SerializeField] [ReadOnly] protected string _portalId = string.Empty;

        /// <inheritdoc />
        public Guid PortalId
        {
            get
            {
                if (_parsedPortalId == null) _parsedPortalId = !string.IsNullOrEmpty(_portalId) ? Guid.Parse(_portalId) : Guid.Empty;

                return _parsedPortalId.Value;
            }
        }
        private Guid? _parsedPortalId = null;

        /// <inheritdoc />
        public IDynamicPortalConnection PortalConnection { get; private set; }

        /// <inheritdoc />
        public override bool HasConnection => PortalConnection != null;

        /// <inheritdoc />
        public event Action<IDynamicPortal> PortalConnectionAssigned;

        [Inject] protected IDynamicPortalManager PortalManager = null;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            PortalConnection = null;
        }

        /// <inheritdoc />
        protected override void OnSpaceLoaded()
        {
            base.OnSpaceLoaded();

            PortalManager.RegisterDynamicPortalEndPoint(this);
        }

        /// <inheritdoc />
        protected override void OnSpaceUnloading()
        {
            base.OnSpaceUnloading();

            PortalManager.UnregisterDynamicPortalEndPoint(this);
        }

        /// <inheritdoc />
        public override void AttemptOpen()
        {
            if (IsOpen)
            {
                Logger.LogWarning($"{PortalId} is already open");
                return;
            }

            if (!HasConnection)
            {
                //Logger.LogWarning($"Portal {PortalId} doesn't have a connection");
                return;
            }

            PortalManager.AttemptOpenDynamicPortalConnection(PortalConnection);
        }

        /// <inheritdoc />
        public override void Close()
        {
            if (!IsOpen)
            {
                Logger.LogWarning($"{PortalId} is already closed");
                return;
            }

            if (!HasConnection)
            {
                Logger.LogWarning($"Portal {PortalId} doesn't have a connection");
                return;
            }

            PortalManager.CloseDynamicPortalConnection(PortalConnection);
        }

        /// <inheritdoc />
        public void OnConnectionAssigned(IDynamicPortalConnection portalConnection)
        {
            if (PortalConnection != null)
            {
                Logger.LogError($"Portal {PortalId} already has a connection");
                return;
            }

            PortalConnection = portalConnection;

            PortalConnectionAssigned?.Invoke(this);
        }

        /// <inheritdoc />
        public void OnConnectionRemoved()
        {
            PortalConnection = null;
        }

        /// <inheritdoc />
        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            UnityEditorUtil.GenerateGuidForBehaviour(this, _portalId, out _portalId);
#endif
        }
    }
}
