﻿using Game.Core.Audio;

namespace Game.Core.Portal
{
    /// <summary>
    /// Extensions for <see cref="IPortalAudioLink"/>
    /// </summary>
    public static class PortalAudioLinkExtensions
    {
        /// <summary>
        /// Returns the first <see cref="IAudioModifierDefinition"/> of type <typeparamref name="T"/>.
        /// Returns null if one does not exist.
        /// </summary>
        public static T GetAudioModifier<T>(this IPortalAudioLink self) where T : class, IAudioModifierDefinition
        {
            foreach (var component in self.AudioModifiers)
            {
                T castedComponent = component as T;
                if (castedComponent != null)
                {
                    return castedComponent;
                }
            }

            return null;
        }
    }
}