﻿using System;

namespace Game.Core.Portal
{
    /// <summary>
    /// A <see cref="IPortal"/> that has a dynamic connection that can be assigned and removed at runtime
    /// </summary>
    public interface IDynamicPortal : IPortal
    {
        /// <summary>
        /// The id of the portal
        /// </summary>
        Guid PortalId { get; }

        /// <summary>
        /// The current dynamic connection
        /// </summary>
        IDynamicPortalConnection PortalConnection { get; }

        /// <summary>
        /// Raised when the portal is assigned a dynamic connection
        /// </summary>
        event Action<IDynamicPortal> PortalConnectionAssigned;

        /// <summary>
        /// Use by <see cref="IDynamicPortalManager"/> to inform the portal it has been assigned a connection
        /// </summary>
        /// <param name="portalConnection"></param>
        void OnConnectionAssigned(IDynamicPortalConnection portalConnection);
        /// <summary>
        /// Used by <see cref="IDynamicPortalManager"/> to inform the portal its connection has been removed
        /// </summary>
        void OnConnectionRemoved();
    }
}
