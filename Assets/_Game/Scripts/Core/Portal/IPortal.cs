﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// Delegate for <see cref="IPortal"/> events
    /// </summary>
    /// <param name="sender"></param>
    public delegate void PortalEvent(IPortal sender);

    /// <summary>
    /// A planar portal between two locations. These locations my be in the same space or on seperate spaces.
    /// </summary>
    public interface IPortal : IGameObjectComponent
    {
        /// <summary>
        /// If the portal is open, a referefence to the paired portal 
        /// </summary>
        IPortal EndPoint { get; }

        /// <summary>
        /// Is the portal open
        /// </summary>
        bool IsOpen { get; }

        /// <summary>
        /// Does the portal have connection data. Just because the portal has connection data does not mean that it is open or ready to use
        /// </summary>
        bool HasConnection { get; }

        /// <summary>
        /// The planar size of the portal
        /// </summary>
        Vector2 PortalSize { get; }

        /// <summary>
        /// The portal plane. 
        /// If a point is on the true side of <see cref="Plane.GetSide(Vector3)"/> it has passed through the portal. 
        /// If false, it is still on the entry side.
        /// </summary>
        Plane PortalPlane { get; }

        /// <summary>
        /// The space the portal is in
        /// </summary>
        ISpaceData Space { get; }

        /// <summary>
        /// The center of the portal
        /// </summary>
        Transform Transform { get; }

        /// <summary>
        /// Raised when the portal opens
        /// </summary>
        event PortalEvent PortalOpened;

        /// <summary>
        /// Raised when the portal closes. This could be for several reasons such as the connection is closed or the end points space was unloaded
        /// </summary>
        event PortalEvent PortalClosed;

        /// <summary>
        /// Attempt to open the portal if it has a connection
        /// </summary>
        void AttemptOpen();

        /// <summary>
        /// Closes the portal if it is open
        /// </summary>
        void Close();

        /// <summary>
        /// Called by the portal manager when a portal has successfully been opened
        /// </summary>
        void OnPortalOpened(IPortalConnection portalConnection, IPortal otherEndPoint);
        /// <summary>
        /// Called by the portal manager when a portal is closed
        /// </summary>
        void OnPortalClosed();

        /// <summary>
        /// Calculates the corresponding position and rotation at the end point <see cref="IPortal"/>
        /// </summary>
        void CalculateEndPointTransform(Vector3 inputPosition, Quaternion inputRotation, out Vector3 endPointPosition, out Quaternion endPointRotation);

        /// <summary>
        /// Calculates the corresponding position at the end point <see cref="IPortal"/>
        /// </summary>
        Vector3 CalculateEndPointPosition(Vector3 inputPosition);

        /// <summary>
        /// Calculates the corresponding rotation at the end point <see cref="IPortal"/>
        /// </summary>
        Quaternion CalculateEndPointRotation(Quaternion inputRotation);

        /// <summary>
        /// Calculates the corresponding direction at the end point <see cref="IPortal"/>
        /// </summary>
        Vector3 CalculateEndPointDirection(Vector3 inputDirection);

        /// <summary>
        /// Calculates a local space position of a point originating from the end point <see cref="IPortal"/>
        /// </summary>
        Vector3 InverseCalculateEndPointPosition(Vector3 inputPosition);

        /// <summary>
        /// Calculates a local space rotation of an input rotation originating from the end point <see cref="IPortal"/>
        /// </summary>
        Quaternion InverseCalculateEndPointRotation(Quaternion inputRotation);

        /// <summary>
        /// Calculates a local space direction of an input direction originating from the end point <see cref="IPortal"/>
        /// </summary>
        Vector3 InverseCalculateEndPointDirection(Vector3 inputDirection);
    }
}