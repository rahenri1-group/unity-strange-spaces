﻿namespace Game.Core.Portal
{
    /// <summary>
    /// A <see cref="IPortal"/> that has a static connection that is assigned at edit time
    /// </summary>
    public interface IStaticPortal : IPortal
    {
        /// <summary>
        /// The static portal connection
        /// </summary>
        IStaticPortalConnection PortalConnection { get; }
    }
}
