﻿using Cysharp.Threading.Tasks;
using Game.Core.Entity;
using Game.Core.Interaction;
using System.Linq;
using UnityEngine;

namespace Game.Core.Portal.Interaction
{
    /// <summary>
    /// Creates shadow interactors for the given <see cref="IPrimaryWorldInteractor"/> whenever a portal is detected
    /// </summary>
    public class PortalShadowInteractorCreaterBehaviour : BasePortalTransporterShadowCreater
    {
        [SerializeField] [TypeRestriction(typeof(IPrimaryWorldInteractor))] private Component _interactorToShadowObj = null;
        private IPrimaryWorldInteractor _interactorToShadow;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _interactorToShadow = _interactorToShadowObj.GetComponentAsserted<IPrimaryWorldInteractor>();
        }

        protected override void OnPortalConnected(IPortal sender)
        {
            base.OnPortalConnected(sender);

            // also create a shadow from starting portal so that we can cleanly hand off objects
            SetupForPortal(sender.EndPoint, sender.EndPoint.GetComponent<IPortalTransporter>());
        }

        protected override GameObject CreateShadowForPortal(IPortal portal)
        {
            var shadowGameObject = new GameObject($"{_interactorToShadow.GameObject.name.PrependIfMissing("@")}-InteractorShadow");

            var shadowInteractor = shadowGameObject.AddComponent<ShadowInteractorBehaviour>();
            shadowInteractor.PrimaryInteractor = _interactorToShadow;
            shadowInteractor.Portal = portal;

            return shadowGameObject;
        }

        protected override void DestroyShadowForPortal(GameObject shadow, IPortal portal)
        {
            var shadowInteractor = shadow.GetComponent<ShadowInteractorBehaviour>();

            // check if holding something
            if (shadowInteractor.IsInteracting && portal.IsOpen)
            {
                var portalTransporter = _portalShadowDataMap[portal].PortalTransporter;

                // is the interactable an entity?
                var interactionEntity = shadowInteractor.InteractionTarget.GameObject.GetComponent<IDynamicEntity>();
                if (interactionEntity != null && portalTransporter.EntitiesInPortalZone.Contains(interactionEntity))
                {
                    portalTransporter.TeleportEntityInPortalZone(interactionEntity).Forget();
                }
            }

            base.DestroyShadowForPortal(shadow, portal);
        }
    }
}