﻿using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Physics;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal.Interaction
{
    /// <summary>
    /// A <see cref="IShadowWorldInteractor"/> created by a <see cref="IPrimaryWorldInteractor"/> being partially in a portal
    /// </summary>
    public class ShadowInteractorBehaviour : InjectedBehaviour, IShadowWorldInteractor
    {
        /// <inheritdoc/>
        public GameObject InteractorOwner => PrimaryInteractor.InteractorOwner;

        /// <inheritdoc/>
        public Vector3 Position => Body.position;
        /// <inheritdoc/>
        public Quaternion Rotation => Body.rotation;

        /// <summary>
        /// The <see cref="IPrimaryWorldInteractor"/> this shadows
        /// </summary>
        public IPrimaryWorldInteractor PrimaryInteractor { get; set; }

        /// <inheritdoc/>
        public IPortal Portal { get; set; }

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public IInteractable InteractionTarget { get; private set; }

        /// <inheritdoc/>
        public bool IsInteracting { get; private set; }
        /// <inheritdoc/>
        public bool IsInteractionEnabled => PrimaryInteractor.IsInteractionEnabled;

        /// <inheritdoc/>
        public Rigidbody Body { get; private set; }

        /// <inheritdoc/>
        public float ClosestInteractableProximityValue { get; private set; }

        [Inject] private IInteractionManager _interactionManager = null;
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] protected ISpacePhysics _spacePhysics = null;

        private bool _isSetup = false;
        private WorldInteractableTriggerProximityVolume _nearbyInteractableVolume;

        /// <inheritdoc/>
        private void Start()
        {
            Assert.IsNotNull(PrimaryInteractor);
            Assert.IsNotNull(Portal);

            Body = gameObject.AddComponent<Rigidbody>();
            Body.isKinematic = true;
            Body.useGravity = false;

            Body.mass = PrimaryInteractor.Body.mass;
            Body.drag = PrimaryInteractor.Body.drag;
            Body.collisionDetectionMode = PrimaryInteractor.Body.collisionDetectionMode;

            ClosestInteractableProximityValue = float.MaxValue;

            GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale);
            Body.position = position;
            Body.rotation = rotation;
            transform.localScale = scale;

            var triggerPosition = Vector3.zero;
            var triggerRotation = Quaternion.identity;
            var triggerScale = Vector3.one;

            if (PrimaryInteractor.NearbyWorldInteractableVolume.gameObject != PrimaryInteractor.GameObject)
            {
                triggerPosition = PrimaryInteractor.GameObject.transform.InverseTransformPoint(PrimaryInteractor.NearbyWorldInteractableVolume.transform.position);
                triggerRotation = PrimaryInteractor.GameObject.transform.InverseTransformRotation(PrimaryInteractor.NearbyWorldInteractableVolume.transform.rotation);
                triggerScale = Vector3.Scale(PrimaryInteractor.GameObject.transform.lossyScale.Invert(), PrimaryInteractor.NearbyWorldInteractableVolume.transform.lossyScale);
            }

            _nearbyInteractableVolume = Instantiate<WorldInteractableTriggerProximityVolume>(
                PrimaryInteractor.NearbyWorldInteractableVolume, 
                triggerPosition,
                triggerRotation,
                transform);
            _nearbyInteractableVolume.transform.localScale = triggerScale;

            _isSetup = true;

            PrimaryInteractor.RegisterShadowInteractor(this);
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (_isSetup)
            {
                PrimaryInteractor.RegisterShadowInteractor(this);
            }
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            if (_isSetup)
            {
                PrimaryInteractor.UnregisterShadowInteractor(this);

                InteractionTarget = null;
                IsInteracting = false;
            }

            _interactionManager.OnInteractorDisable(this);
        }

        /// <inheritdoc/>
        public void SyncPosition()
        {
            GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale);
            Body.position = position;
            Body.rotation = rotation;
            transform.localScale = scale;
        }

        /// <inheritdoc/>
        public void UpdateNearbyInteractable()
        {
            if (IsInteracting) return;

            if (IsInteractionEnabled)
            {
                InteractionTarget = _nearbyInteractableVolume.GetClosest(NearbyInteractableVolumeValidator, out float proximityValue);
                ClosestInteractableProximityValue = proximityValue;
            }
            else
            {
                InteractionTarget = null;
                ClosestInteractableProximityValue = float.MaxValue;
            }
        }

        private bool NearbyInteractableVolumeValidator(IInteractable interactable)
        {
            if (interactable.AllowedInteractions(this) != Interactions.None)
            {
                var direction = Portal.InverseCalculateEndPointDirection((interactable.GameObject.transform.position - Position).normalized);

                if (_spacePhysics.Raycast(
                    _spaceManager.GetObjectSpace(PrimaryInteractor.GameObject),
                    PrimaryInteractor.Position,
                    direction,
                    out var spaceCastResult,
                    Vector3.Distance(interactable.GameObject.transform.position, Position),
                    QueryPortalInteraction.CastThroughTransporter,
                    PrimaryInteractor.InteractionBlockingMask))
                {
                    // verfiy the object we hit is the interactable
                    return spaceCastResult.Collider?.GetComponentInParent<IInteractable>() == interactable;
                }

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool CanInteractWith(IInteractable interactble)
        {
            return PrimaryInteractor.CanInteractWith(interactble);
        }


        /// <inheritdoc/>
        public void OnInteractBegin(IInteractable interactable)
        {
            IsInteracting = true;
            InteractionTarget = interactable; // need to set as it could come fom a handoff

            PrimaryInteractor.OnInteractBegin(interactable);
        }

        /// <inheritdoc/>
        public void OnInteractEnd()
        {
            IsInteracting = false;
            InteractionTarget = null;

            PrimaryInteractor.OnInteractEnd();
        }

        private void GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale)
        {
            if (_spaceManager.GetObjectSpace(gameObject) == _spaceManager.GetObjectSpace(PrimaryInteractor.GameObject))
            {
                position = PrimaryInteractor.Position;
                rotation = PrimaryInteractor.Rotation;
            }
            else
            {
                Portal.CalculateEndPointTransform(
                    PrimaryInteractor.Position, PrimaryInteractor.Rotation,
                    out position, out rotation);
            }

            scale = PrimaryInteractor.GameObject.transform.localScale;
        }
    }
}
