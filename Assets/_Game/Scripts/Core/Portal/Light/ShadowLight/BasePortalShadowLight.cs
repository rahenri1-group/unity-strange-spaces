﻿using Game.Core.DependencyInjection;
using Game.Core.Render.Lighting;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalShadowLight"/>
    public abstract class BasePortalShadowLight : BaseLight, IPortalShadowLight
    {
        /// <inheritdoc/>
        public ILight LightToShadow { get; private set; }

        /// <inheritdoc/>
        public bool ShadowingEnabled 
        {
            get => _shadowingEnabled;
            set
            {
                _shadowingEnabled = value;
                if (_shadowingEnabled)
                {
                    Sync();
                }
                else
                {
                    UnityLight.enabled = false;
                }
            }
        }

        [Inject] private ILogRouter _logger = null;

        protected override Light UnityLight => _light;
        private Light _light;
        
        private IPortal _portal;

        private bool _shadowingEnabled;

        /// <summary>
        /// Initializes the <see cref="BasePortalShadowLight"/>. 
        /// This should be called immediately after creating the shadow
        /// </summary>
        public virtual void Init(IPortal portal, ILight lightToShadow)
        {
            LightToShadow = lightToShadow;
            _portal = portal;

            _light = gameObject.AddComponent<Light>();
            _light.bounceIntensity = 0f;
            _light.type = LightType.Spot;

            // first sync will be trigger by creater after lighting has been setup and we've moved to the appropriate space
            _shadowingEnabled = true;
            LightEnabled = false;
        }

        /// <inheritdoc/>
        protected override void Start()
        {
            base.Start();

            if (LightToShadow == null)
            {
                _logger.LogError(LightManager.ModuleName, $"{gameObject.name} has started without being provided a light to shadow");
            }
        }

        /// <summary>
        /// If <see cref="ShadowingEnabled"/> is on, syncs the shadow light to the <see cref="ILight"/> it is shadowing
        /// </summary>
        public void Sync()
        {
            if (!ShadowingEnabled) return;

            var lightToShadowProperties = LightToShadow.LightProperties;

            LightEnabled = LightToShadow.LightEnabled;

            if (LightEnabled)
            {

                transform.position = _portal.InverseCalculateEndPointPosition(LightToShadow.GameObject.transform.position);

                var newLightProperties = LightToShadow.LightProperties;

                if (LightToShadow.LightType == LightType.Point)
                {
                    transform.rotation = Quaternion.LookRotation((_portal.Transform.position - transform.position).normalized, Vector3.up);

                    float distance = Vector3.Distance(transform.position, _portal.Transform.position);
                    float lightDiameter = 1.5f * Mathf.Max(_portal.PortalSize.x, _portal.PortalSize.y);

                    float angle = Mathf.Atan2(lightDiameter, distance) * Mathf.Rad2Deg;

                    newLightProperties.SpotAngle = angle;
                    newLightProperties.InnerSpotAngle = angle;
                }
                else if (LightToShadow.LightType == LightType.Spot)
                {
                    transform.rotation = _portal.InverseCalculateEndPointRotation(LightToShadow.GameObject.transform.rotation);

                    newLightProperties.Intensity = lightToShadowProperties.Intensity;
                    newLightProperties.SpotAngle = lightToShadowProperties.SpotAngle;
                    newLightProperties.InnerSpotAngle = lightToShadowProperties.InnerSpotAngle;
                }

                LightProperties = newLightProperties;
            }
        }
    }
}
