﻿using Game.Core.Render.Lighting;

namespace Game.Core.Portal
{
    /// <summary>
    /// A <see cref="ILight"/> that shadows another <see cref="ILight"/> that to simulate the light that would be cast through a <see cref="IPortal"/>
    /// </summary>
    public interface IPortalShadowLight : ILight
    {
        /// <summary>
        /// Is shadow simulation enabled? 
        /// If set to false, the light will stop matching <see cref="LightToShadow"/> and will be disabled.
        /// </summary>
        bool ShadowingEnabled { get; set; }

        /// <summary>
        /// The light the shadow is simulating
        /// </summary>
        ILight LightToShadow { get; }
    }
}
