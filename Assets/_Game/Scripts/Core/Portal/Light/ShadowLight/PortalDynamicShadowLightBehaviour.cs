﻿using Game.Core.Render.Lighting;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// A <see cref="IPortalShadowLight"/> for dynamic <see cref="ILight"/>s
    /// </summary>
    public class PortalDynamicShadowLightBehaviour : BasePortalShadowLight
    {
        public override void Init(IPortal portal, ILight lightToShadow)
        {
            base.Init(portal, lightToShadow);

            // hard coded for now
            UnityLight.shadows = LightShadows.None;
        }

        /// <inheritdoc/>
        private void LateUpdate()
        {
            Sync();
        }
    }
}
