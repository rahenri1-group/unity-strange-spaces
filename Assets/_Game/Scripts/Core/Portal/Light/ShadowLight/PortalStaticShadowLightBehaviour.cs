﻿using Game.Core.Render.Lighting;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// A <see cref="IPortalShadowLight"/> for static <see cref="ILight"/>s
    /// </summary>
    public class PortalStaticShadowLightBehaviour : BasePortalShadowLight
    {
        /// <inheritdoc/>
        public override void Init(IPortal portal, ILight lightToShadow)
        {
            base.Init(portal, lightToShadow);

            // shadow settings hard coded for now
            UnityLight.shadows = LightShadows.Hard;

            LightToShadow.LightChanged += OnTargetLightChanged;
        }

        /// <inheritdoc/>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            LightToShadow.LightChanged -= OnTargetLightChanged;
        }

        private void OnTargetLightChanged(ILight obj)
        {
            Sync();
        }
    }
}
