﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Render.Lighting;
using Game.Core.Space;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <summary>
    /// Creates <see cref="PortalDynamicShadowLightBehaviour"/> for a dynamic <see cref="ILight"/>
    /// </summary>
    public class DynamicLightShadowCreatorBehaviour : InjectedBehaviour
    {
        [SerializeField] private FloatReadonlyReference _detectionFrequency = null;

        [SerializeField] private FloatReadonlyReference _detectionAngleFudge = null;
        [SerializeField] private FloatReadonlyReference _detectionDistanceFudge = null;

        [SerializeField] [Layer] private int[] _portalQueryLayers = new int[0];

        [Inject] protected ILogRouter Logger = null;
        [Inject] protected ISpaceManager SpaceManager = null;
        [Inject] protected ISpacePhysics SpacePhysics = null;

        protected int PortalQueryMask { get; private set; }

        private ILight _dynamicLight;

        private IPortal _shadowPortal;
        private PortalDynamicShadowLightBehaviour _shadowLight;

        private IEnumerator _portalSearchCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_detectionFrequency > 0f);

            _dynamicLight = this.GetComponentAsserted<ILight>();

            PortalQueryMask = LayerUtil.ConstructMaskForLayers(_portalQueryLayers);

            _shadowPortal = null;
            _shadowLight = null;

            _portalSearchCoroutine = null;
        }

        private void OnEnable()
        {
            if (_dynamicLight.LightEnabled)
            {
                _portalSearchCoroutine = PortalSearch();
                StartCoroutine(_portalSearchCoroutine);
            }

            _dynamicLight.LightChanged += OnLightChanged;
        }

        private void OnDisable()
        {
            if(_portalSearchCoroutine != null)
            {
                StopCoroutine(_portalSearchCoroutine);
                _portalSearchCoroutine = null;
            }

            _dynamicLight.LightChanged -= OnLightChanged;

            CleanUpForCurrentPortal();
        }

        private void OnLightChanged(ILight light)
        {
            if (_dynamicLight.LightEnabled && _portalSearchCoroutine == null)
            {
                _portalSearchCoroutine = PortalSearch();
                StartCoroutine(_portalSearchCoroutine);
            }
            else if (!_dynamicLight.LightEnabled && _portalSearchCoroutine != null)
            {
                StopCoroutine(_portalSearchCoroutine);
                _portalSearchCoroutine = null;
            }
        }

        private void OnPortalOpened(IPortal sender)
        {
            CreateShadow();
        }

        private void OnPortalClosed(IPortal sender)
        {
            DestroyShadow();
        }

        private void CreateShadow()
        {
            UniTask.Create(async () =>
            {
                var shadowGameObject = new GameObject($"{_dynamicLight.GameObject.name.PrependIfMissing("@")}-LightShadow");
                var shadow = shadowGameObject.AddComponent<PortalDynamicShadowLightBehaviour>();
                shadow.Init(_shadowPortal.EndPoint, _dynamicLight);

                await SpaceManager.MoveObjectToSpaceAsync(shadowGameObject, _shadowPortal.EndPoint.Space);

                // verify that we still have the portal and it is open
                if (_shadowPortal != null && _shadowPortal.IsOpen)
                {
                    shadow.Sync();
                    _shadowLight = shadow;
                }
                else
                {
                    // something happened mid-create
                    Destroy(shadowGameObject);
                }
            }).Forget();
        }

        private void DestroyShadow()
        {
            if (_shadowLight != null)
            {
                Destroy(_shadowLight.gameObject);
                _shadowLight = null;
            }
        }

        private void CleanUpForCurrentPortal()
        {
            if (_shadowPortal != null)
            {
                _shadowPortal.PortalOpened -= OnPortalOpened;
                _shadowPortal.PortalClosed -= OnPortalClosed;

                _shadowPortal = null;
            }

            DestroyShadow();
        }

        protected virtual IPortal DetectPortal()
        {
            if (_dynamicLight.LightType == LightType.Spot)
            {
                var lightSpace = SpaceManager.GetObjectSpace(gameObject);

                var lightCone = _dynamicLight.ConstructSpotCone();
                lightCone.Angle += _detectionAngleFudge;
                lightCone.MaxDistance += _detectionDistanceFudge;
                if (SpacePhysics.FindClosestInCone<IPortal>(
                    lightSpace,
                    lightCone,
                    out var resultInfo,
                    QueryPortalInteraction.Block,
                    PortalQueryMask)
                    && resultInfo.Result.PortalPlane.GetSide(transform.position) == false)
                {
                    return resultInfo.Result;
                }
            }
            else
            {
                Logger.LogError($"Unsupported light type {_dynamicLight.LightType} for {nameof(DynamicLightShadowCreatorBehaviour)}");
            }

            return null;
        }

        private IEnumerator PortalSearch()
        {
            float detectionDelay = 1f / _detectionFrequency;
            YieldInstruction wait = new WaitForSeconds(detectionDelay);

            PortalSearchLogic();

            yield return new WaitForSeconds(Random.Default.Range(0.1f * detectionDelay, detectionDelay));

            while (true)
            {
                PortalSearchLogic();

                yield return wait;
            }
        }

        private void PortalSearchLogic()
        {
            var detectedPortal = DetectPortal();

            if (detectedPortal != null)
            {
                if (_shadowPortal != null && _shadowPortal != detectedPortal) // new portal found, forget old one
                {
                    CleanUpForCurrentPortal();
                }

                // may need to 'forget' old portal
                if (_shadowPortal == null) // if we haven't seen the portal before create a shadow
                {
                    _shadowPortal = detectedPortal;

                    _shadowPortal.PortalOpened += OnPortalOpened;
                    _shadowPortal.PortalClosed += OnPortalClosed;

                    if (_shadowPortal.IsOpen)
                    {
                        CreateShadow();
                    }
                }
            }
            else // no portal found
            {
                CleanUpForCurrentPortal();
            }
        }
    }
}
