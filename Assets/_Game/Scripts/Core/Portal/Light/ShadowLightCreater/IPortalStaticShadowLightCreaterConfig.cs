﻿namespace Game.Core.Portal
{
    /// <summary>
    /// Config for a <see cref="PortalStaticShadowLightCreaterBehaviour"/>
    /// </summary>
    public interface IPortalStaticShadowLightCreaterConfig
    {
        /// <summary>
        /// The maximum number of lights per portal.
        /// </summary>
        int MaxLightsPerPortal { get; }

        /// <summary>
        /// The maximum angle between the portal the light allowed. Any light beyond this angle will not generate a shadow.
        /// </summary>
        float MaxLightAngle { get; }

        /// <summary>
        /// Any only lights with a measured intensity value at the portal entrance will be shadowed.
        /// </summary>
        float MinLightIntensity { get; }
    }
}
