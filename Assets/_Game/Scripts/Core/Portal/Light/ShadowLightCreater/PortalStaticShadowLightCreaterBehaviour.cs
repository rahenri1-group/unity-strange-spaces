﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Render.Lighting;
using Game.Core.Space;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// Creates <see cref="PortalStaticShadowLightBehaviour"/>s for a static lights that touch a <see cref="IPortal"/>
    /// </summary>
    public class PortalStaticShadowLightCreaterBehaviour : InjectedBehaviour, IPortalLighting
    {
        /// <inheritdoc/>
        public IPortalShadowLight[] PortalLights => _shadows.ToArray();

        [Inject] private IPortalStaticShadowLightCreaterConfig _config = null;

        [Inject] private ILightManager _lightManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IPortal _portal;

        private List<PortalStaticShadowLightBehaviour> _shadows;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _portal = this.GetComponentAsserted<IPortal>();

            if (_portal.IsOpen)
            {
                OnPortalConnected(_portal);
            }

            _shadows = new List<PortalStaticShadowLightBehaviour>();

            _portal.PortalOpened += OnPortalConnected;
            _portal.PortalClosed += OnPortalDisconnected;
        }

        /// <inheritdoc/>
        private void OnDestroy()
        {
            CleanUpShadows();

            _portal.PortalOpened -= OnPortalConnected;
            _portal.PortalClosed -= OnPortalDisconnected;
        }

        private void OnPortalConnected(IPortal sender)
        {
            var lights = _lightManager.StaticLightsThatTouchPoint(
                _portal.EndPoint.Space, 
                _portal.EndPoint.Transform.position, 
                (light) => light.LightIntensityAtPoint(_portal.EndPoint.Transform.position));

            int createdLightsCount = 0;

            foreach (var light in lights)
            {
                var lightVector = (light.GameObject.transform.position - _portal.EndPoint.Transform.position).normalized;
                float angle = Vector3.Angle(-1f * _portal.EndPoint.Transform.forward, lightVector);

                if (light.LightIntensityAtPoint(_portal.EndPoint.Transform.position) < _config.MinLightIntensity)
                {
                    break;
                }

                if (angle <= _config.MaxLightAngle)
                {
                    createdLightsCount += 1;

                    UniTask.Create(async () =>
                    {
                        var shadowGameObject = new GameObject($"{light.GameObject.name.PrependIfMissing("@")}-LightShadow");
                        var shadow = shadowGameObject.AddComponent<PortalStaticShadowLightBehaviour>();
                        shadow.Init(_portal, light);

                        await _spaceManager.MoveObjectToSpaceAsync(shadowGameObject, _portal.Space); 

                        // verify portal is still open
                        if (_portal.IsOpen)
                        {
                            shadow.Sync();

                            _shadows.Add(shadow);
                        }
                        else
                        {
                            // something happened mid-create
                            Destroy(shadowGameObject);
                        }
                    }).Forget();
                }

                if (createdLightsCount >= _config.MaxLightsPerPortal)
                {
                    break;
                }
            }
        }

        private void OnPortalDisconnected(IPortal sender)
        {
            CleanUpShadows();
        }

        private void CleanUpShadows()
        {
            foreach (var shadow in _shadows)
            {
                Destroy(shadow.gameObject);
            }

            _shadows.Clear();
        }
    }
}
