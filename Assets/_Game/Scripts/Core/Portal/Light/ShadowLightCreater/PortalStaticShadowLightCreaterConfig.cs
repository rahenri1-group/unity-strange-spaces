﻿using Game.Core.DependencyInjection;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalStaticShadowLightCreaterConfig"/>
    [Dependency(
        contract: typeof(IPortalStaticShadowLightCreaterConfig),
        lifetime: Lifetime.Singleton)]
    public class PortalStaticShadowLightCreaterConfig : IPortalStaticShadowLightCreaterConfig
    {
        /// <inheritdoc/>
        public int MaxLightsPerPortal { get; set; }

        /// <inheritdoc/>
        public float MaxLightAngle { get; set; }

        /// <inheritdoc/>
        public float MinLightIntensity { get; set; }
    }
}
