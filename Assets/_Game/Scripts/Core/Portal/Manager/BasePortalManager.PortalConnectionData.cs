﻿using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Portal
{
    public abstract partial class BasePortalManager<T> : BaseModule where T : IPortal
    {
        protected class PortalConnectionData
        {
            public IPortalConnection PortalConnection { get; private set; }

            /// <summary>
            /// A portal is considered tunnelled if both of it's end points are loaded, registered, and active
            /// </summary>
            public bool PortalTunnelled => _portalEndPoints.Count == 2;
            public bool PortalOpen { get; set; }
            public bool AttemptPortalOpen { get; set; }
            public T[] EndPoints => _portalEndPoints.ToArray();

            private readonly HashSet<T> _portalEndPoints;

            public PortalConnectionData(IPortalConnection portalConnection)
            {
                PortalConnection = portalConnection;

                PortalOpen = false;
                AttemptPortalOpen = false;

                _portalEndPoints = new HashSet<T>();
            }

            public void AddEndPoint(T portal)
            {
                if (ContainsEndPoint(portal))
                {
                    throw new PortalException($"Attempt to add duplicate portal to portal connection {PortalConnection.PortalConnectionId}");
                }

                if (PortalTunnelled)
                {
                    throw new PortalException($"Attempt to add portal to already completed portal connection {PortalConnection.PortalConnectionId}");
                }

                _portalEndPoints.Add(portal);
            }

            public void RemoveEndPoint(T portal)
            {
                if (ContainsEndPoint(portal))
                {
                    _portalEndPoints.Remove(portal);

                    PortalOpen = false;
                }
            }

            public bool ContainsEndPoint(T portal)
            {
                return _portalEndPoints.Contains(portal);
            }

            public bool HasEndPoints()
            {
                return _portalEndPoints.Count > 0;
            }
        }
    }
}
