using Cysharp.Threading.Tasks;
using Game.Core.Math;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Portal
{
    /// <summary>
    /// Base portal manager
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract partial class BasePortalManager<T> : BaseModule where T : IPortal
    {
        private readonly ISpaceLoader SpaceLoader;
        private readonly ISpaceManager SpaceManager;

        protected Dictionary<IPortalConnection, PortalConnectionData> PortalConnectionMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public BasePortalManager(
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(logger)
        {
            SpaceLoader = spaceLoader;
            SpaceManager = spaceManager;

            PortalConnectionMap = new Dictionary<IPortalConnection, PortalConnectionData>();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            PortalConnectionMap.Clear();

            return base.Shutdown();
        }

        protected void SetupAndOpenPortalConnections(IPortalConnection[] portalConnections)
        {
            var connectionSpaceIds = new HashSet<Guid>();

            foreach (var connection in portalConnections)
            {
                if (!connectionSpaceIds.Contains(connection.SpaceIdA))
                {
                    connectionSpaceIds.Add(connection.SpaceIdA);
                }

                if (!connectionSpaceIds.Contains(connection.SpaceIdB))
                {
                    connectionSpaceIds.Add(connection.SpaceIdB);
                }

                var portalConnectionData = PortalConnectionMap[connection];

                if (portalConnectionData.PortalTunnelled)
                {
                    OpenTunnelledPortal(portalConnectionData);
                }
                else
                {
                    portalConnectionData.AttemptPortalOpen = true;
                }
            }

            SpaceLoader.LoadSpaces(connectionSpaceIds.ToArray()).Forget();
        }

        protected void ClosePortalConnection(IPortalConnection portalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(portalConnection))
            {
                ModuleLogError($"Attempt to close unknown portal connection {portalConnection.PortalConnectionId}");
                return;
            }

            var portalConnectionData = PortalConnectionMap[portalConnection];

            portalConnectionData.AttemptPortalOpen = false;

            if (portalConnectionData.PortalOpen)
            {
                portalConnectionData.PortalOpen = false;

                ModuleLogInfo($"Portal connection {portalConnectionData.PortalConnection.Name} closed");

                var endPoints = portalConnectionData.EndPoints;

                endPoints[0].OnPortalClosed();
                endPoints[1].OnPortalClosed();
            }
        }

        protected void RegisterPortalForConnection(T portal, IPortalConnection portalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(portalConnection))
            {
                PortalConnectionMap[portalConnection] = new PortalConnectionData(portalConnection);
            }

            var portalConnectionData = PortalConnectionMap[portalConnection];

            portalConnectionData.AddEndPoint(portal);

            if (portalConnectionData.PortalTunnelled && portalConnectionData.AttemptPortalOpen)
            {
                OpenTunnelledPortal(portalConnectionData);
            }
        }

        protected void UnregisterPortalForConnection(T portal, IPortalConnection portalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(portalConnection)) return;

            var portalConnectionData = PortalConnectionMap[portalConnection];

            if (!portalConnectionData.ContainsEndPoint(portal))
            {
                ModuleLogWarning($"Attempt to unregister portal that is not a part of portal connection {portalConnection.PortalConnectionId}");
                return;
            }

            if (portalConnectionData.PortalTunnelled && portalConnectionData.PortalOpen)
            {
                ModuleLogInfo($"Portal {portalConnectionData.PortalConnection.PortalConnectionId} closed");

                var endPoints = portalConnectionData.EndPoints;
                endPoints[0].OnPortalClosed();
                endPoints[1].OnPortalClosed();

                portalConnectionData.PortalOpen = false;
                portalConnectionData.AttemptPortalOpen = true;
            }

            portalConnectionData.RemoveEndPoint(portal);
        }

        protected IEnumerable<T> AllOpenPortalsToSpace(ISpaceData spaceData)
        {
            var portalList = new List<T>();

            foreach (var pair in PortalConnectionMap)
            {
                var connection = pair.Key;
                var portalData = pair.Value;

                if (portalData.PortalOpen 
                    && (connection.SpaceIdA == spaceData.SpaceId || connection.SpaceIdB == spaceData.SpaceId))
                {
                    foreach (var endpoint in portalData.EndPoints)
                    {
                        if (!SpaceUtil.SpaceEquals(endpoint.Space, spaceData))
                        {
                            portalList.Add(endpoint);
                        }
                    }
                }
            }

            return portalList;
        }

        protected IEnumerable<T> AllOpenPortalsFromSpace(ISpaceData spaceData)
        {
            var portalList = new List<T>();

            foreach (var pair in PortalConnectionMap)
            {
                var connection = pair.Key;
                var portalData = pair.Value;

                if (portalData.PortalOpen 
                    && (connection.SpaceIdA == spaceData.SpaceId || connection.SpaceIdB == spaceData.SpaceId))
                {
                    foreach (var endpoint in portalData.EndPoints)
                    {
                        if (SpaceUtil.SpaceEquals(endpoint.Space, spaceData))
                        {
                            portalList.Add(endpoint);
                        }
                    }
                }
            }

            return portalList;
        }

        private void OpenTunnelledPortal(PortalConnectionData portalData)
        {
            if (portalData.PortalOpen) return;

            portalData.AttemptPortalOpen = false;
            portalData.PortalOpen = true;

            ModuleLogInfo($"Portal connection {portalData.PortalConnection.Name} opened");

            var endPoints = portalData.EndPoints;

            // verify portals are same ratio
            if (!MathUtil.ApproximatelyEqual(endPoints[0].PortalSize.y / endPoints[0].PortalSize.x, endPoints[1].PortalSize.y / endPoints[1].PortalSize.x))
            {
                ModuleLogWarning($"Portals for {portalData.PortalConnection.PortalConnectionId} have different size ratios");
            }

            endPoints[0].OnPortalOpened(portalData.PortalConnection, endPoints[1]);
            endPoints[1].OnPortalOpened(portalData.PortalConnection, endPoints[0]);
        }
    }
}