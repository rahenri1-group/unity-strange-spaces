﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System;
using System.Collections.Generic;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IDynamicPortalManager"/>
    [Dependency(
        contract: typeof(IDynamicPortalManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class DynamicPortalManager : BasePortalManager<IDynamicPortal>, IDynamicPortalManager
    {
        private class DynamicPortalData
        {
            public IDynamicPortal DynamicPortal { get; set; }
            public IDynamicPortalConnection PortalConnection { get; set; }

            public DynamicPortalData()
            {
                DynamicPortal = null;
                PortalConnection = null;
            }
        }

        /// <inheritdoc/>
        public override string ModuleName => "Dynamic Portal Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private Dictionary<Guid, DynamicPortalData> _dynamicPortalIdMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public DynamicPortalManager(
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(spaceLoader, 
                  spaceManager, 
                  logger)
        {
            _dynamicPortalIdMap = new Dictionary<Guid, DynamicPortalData>();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _dynamicPortalIdMap.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc/>
        public void AttemptOpenDynamicPortalConnection(IDynamicPortalConnection dynamicPortalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(dynamicPortalConnection))
            {
                RegisterDynamicPortalConnection(dynamicPortalConnection);
            }

            SetupAndOpenPortalConnections(new IPortalConnection[] { dynamicPortalConnection });
        }

        /// <inheritdoc/>
        public void AttemptOpenDynamicPortalConnections(IDynamicPortalConnection[] portalConnections)
        {
            foreach (var connention in portalConnections)
            {
                if (!PortalConnectionMap.ContainsKey(connention))
                {
                    RegisterDynamicPortalConnection(connention);
                }
            }

            SetupAndOpenPortalConnections(portalConnections);
        }

        /// <inheritdoc/>
        public void CloseDynamicPortalConnection(IDynamicPortalConnection portalConnection)
        {
            ClosePortalConnection(portalConnection);
        }

        /// <inheritdoc/>
        public void RegisterDynamicPortalConnection(IDynamicPortalConnection dynamicPortalConnection)
        {
            if (PortalConnectionMap.ContainsKey(dynamicPortalConnection))
            {
                ModuleLogWarning($"Portal {dynamicPortalConnection.Name} already registered");
                return;
            }

            PortalConnectionMap[dynamicPortalConnection] = new PortalConnectionData(dynamicPortalConnection);

            SetupDynamicPortalForConnection(dynamicPortalConnection.PortalIdA, dynamicPortalConnection);
            SetupDynamicPortalForConnection(dynamicPortalConnection.PortalIdB, dynamicPortalConnection);
        }

        /// <inheritdoc/>
        public void UnregisterDynamicPortalConnection(IDynamicPortalConnection dynamicPortalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(dynamicPortalConnection))
            {
                ModuleLogError($"Attempt to unregister unknown portal connection {dynamicPortalConnection.PortalConnectionId}");
                return;
            }

            ClosePortalConnection(dynamicPortalConnection);

            var connectionData = PortalConnectionMap[dynamicPortalConnection];

            foreach (var portal in connectionData.EndPoints)
            {
                portal.OnConnectionRemoved();
            }

            if (_dynamicPortalIdMap.ContainsKey(dynamicPortalConnection.PortalIdA))
            {
                _dynamicPortalIdMap[dynamicPortalConnection.PortalIdA].PortalConnection = null;
            }

            if (_dynamicPortalIdMap.ContainsKey(dynamicPortalConnection.PortalIdB))
            {
                _dynamicPortalIdMap[dynamicPortalConnection.PortalIdB].PortalConnection = null;
            }

            PortalConnectionMap.Remove(dynamicPortalConnection);
        }

        private void SetupDynamicPortalForConnection(Guid portalId, IDynamicPortalConnection portalConnection)
        {
            if (!_dynamicPortalIdMap.ContainsKey(portalId))
            {
                _dynamicPortalIdMap[portalId] = new DynamicPortalData();
            }

            var portalData = _dynamicPortalIdMap[portalId];
            if (portalData.PortalConnection != null && portalData.PortalConnection != portalConnection)
            {
                throw new PortalException($"Portal {portalId} already has a connection");
            }

            portalData.PortalConnection = portalConnection;
            if (portalData.DynamicPortal != null && !PortalConnectionMap[portalConnection].ContainsEndPoint(portalData.DynamicPortal))
            {
                portalData.DynamicPortal.OnConnectionAssigned(portalData.PortalConnection);

                PortalConnectionMap[portalConnection].AddEndPoint(portalData.DynamicPortal);
            }
        }

        /// <inheritdoc/>
        public bool IsPortalConnectionTunnelled(IDynamicPortalConnection portalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(portalConnection)) return false;

            return PortalConnectionMap[portalConnection].PortalTunnelled;
        }

        /// <inheritdoc/>
        public bool IsPortalConnectionOpen(IDynamicPortalConnection portalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(portalConnection)) return false;

            return PortalConnectionMap[portalConnection].AttemptPortalOpen || PortalConnectionMap[portalConnection].PortalOpen;
        }

        /// <inheritdoc/>
        public void RegisterDynamicPortalEndPoint(IDynamicPortal portal)
        {
            if (!_dynamicPortalIdMap.ContainsKey(portal.PortalId))
            {
                _dynamicPortalIdMap[portal.PortalId] = new DynamicPortalData();
            }

            var portalData = _dynamicPortalIdMap[portal.PortalId];
            portalData.DynamicPortal = portal;

            if (portalData.PortalConnection != null)
            {
                portal.OnConnectionAssigned(portalData.PortalConnection);
                RegisterPortalForConnection(portal, portalData.PortalConnection);
            }
        }

        /// <inheritdoc/>
        public void UnregisterDynamicPortalEndPoint(IDynamicPortal portal)
        {
            if (!_dynamicPortalIdMap.ContainsKey(portal.PortalId)) return;

            var portalData = _dynamicPortalIdMap[portal.PortalId];
            portalData.DynamicPortal = null;

            if (portalData.PortalConnection != null)
            {
                UnregisterPortalForConnection(portal, portalData.PortalConnection);
            }
        }

        /// <inheritdoc/>
        public IEnumerable<IDynamicPortal> AllOpenDynamicPortalsToSpace(ISpaceData spaceData)
        {
            return AllOpenPortalsToSpace(spaceData);
        }

        /// <inheritdoc/>
        public IEnumerable<IDynamicPortal> AllOpenDynamicPortalsFromSpace(ISpaceData spaceData)
        {
            return AllOpenPortalsFromSpace(spaceData);
        }
    }
}
