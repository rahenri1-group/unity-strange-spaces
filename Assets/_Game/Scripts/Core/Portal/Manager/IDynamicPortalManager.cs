﻿using Game.Core.Space;
using System.Collections.Generic;

namespace Game.Core.Portal
{
    /// <summary>
    /// Manager for <see cref="IDynamicPortal"/>s
    /// </summary>
    public interface IDynamicPortalManager
    {
        /// <summary>
        /// Attempts to open a dynamic portal connection, loading additional spaces if necessary. 
        /// Also registers the connection if it hasn't been registered
        /// </summary>
        /// <param name="portalConnection"></param>
        void AttemptOpenDynamicPortalConnection(IDynamicPortalConnection portalConnection);
        /// <summary>
        /// Attempts to open multiple dynamic portal connections, loading additional spaces if necessary. 
        /// Also registers the connections if they haven't been registered
        /// </summary>
        /// <param name="portalConnections"></param>
        void AttemptOpenDynamicPortalConnections(IDynamicPortalConnection[] portalConnections);
        /// <summary>
        /// Closes an opened dynamic portal connection
        /// </summary>
        /// <param name="portalConnection"></param>
        void CloseDynamicPortalConnection(IDynamicPortalConnection portalConnection);

        /// <summary>
        /// Registers a dynamic portal connection
        /// </summary>
        /// <param name="portalConnection"></param>
        void RegisterDynamicPortalConnection(IDynamicPortalConnection portalConnection);
        /// <summary>
        /// Unregisters a dynamic portal connection. 
        /// Also closes the connection if it is still open
        /// </summary>
        /// <param name="portalConnection"></param>
        void UnregisterDynamicPortalConnection(IDynamicPortalConnection portalConnection);

        /// <summary>
        /// A portal is considered tunnelled if both of it's end points are loaded, registered, and active.
        /// </summary>
        /// <param name="portalConnection"></param>
        /// <returns></returns>
        bool IsPortalConnectionTunnelled(IDynamicPortalConnection portalConnection);
        /// <summary>
        /// Is the portal connection open. 
        /// Note: it is possible for a connection to be open but not yet tunneled if one or both of the endpoints hasn't yet finished loading
        /// </summary>
        /// <param name="portalConnection"></param>
        /// <returns></returns>
        bool IsPortalConnectionOpen(IDynamicPortalConnection portalConnection);

        /// <summary>
        /// Registers a dynamic portal
        /// </summary>
        /// <param name="portal"></param>
        void RegisterDynamicPortalEndPoint(IDynamicPortal portal);
        /// <summary>
        /// Unregisters a dynamic portal
        /// </summary>
        /// <param name="portal"></param>
        void UnregisterDynamicPortalEndPoint(IDynamicPortal portal);

        /// <summary>
        /// All open dynamic portals that have a <see cref="IPortal.EndPoint"/> in <paramref name="spaceData"/>
        /// </summary>
        /// <param name="spaceData"></param>
        /// <returns></returns>
        IEnumerable<IDynamicPortal> AllOpenDynamicPortalsToSpace(ISpaceData spaceData);
        /// <summary>
        /// All open dynamic portals in <paramref name="spaceData"/>
        /// </summary>
        /// <param name="spaceData"></param>
        /// <returns></returns>
        IEnumerable<IDynamicPortal> AllOpenDynamicPortalsFromSpace(ISpaceData spaceData);
    }
}
