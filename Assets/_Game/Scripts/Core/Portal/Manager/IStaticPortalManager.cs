﻿using Game.Core.Space;
using System.Collections.Generic;

namespace Game.Core.Portal
{
    /// <summary>
    /// Manager for <see cref="IStaticPortal"/>s
    /// </summary>
    public interface IStaticPortalManager
    {
        /// <summary>
        /// Attempts to open a static portal connection, loading additional spaces if necessary
        /// </summary>
        /// <param name="portalConnection"></param>
        void AttemptOpenStaticPortalConnection(IStaticPortalConnection portalConnection);
        /// <summary>
        /// Closes an opened static portal connection
        /// </summary>
        /// <param name="portalConnection"></param>
        void CloseStaticPortalConnection(IStaticPortalConnection portalConnection);

        /// <summary>
        /// Registers a static portal.
        /// </summary>
        /// <param name="portal"></param>
        void RegisterStaticPortalEndPoint(IStaticPortal portal);
        /// <summary>
        /// Unregisters a static portal
        /// </summary>
        /// <param name="portal"></param>
        void UnregisterStaticPortalEndPoint(IStaticPortal portal);

        /// <summary>
        /// All open static portals that have a <see cref="IPortal.EndPoint"/> in <paramref name="spaceData"/>
        /// </summary>
        /// <param name="spaceData"></param>
        /// <returns></returns>
        IEnumerable<IStaticPortal> AllOpenStaticPortalsToSpace(ISpaceData spaceData);
        /// <summary>
        /// All open static portals in <paramref name="spaceData"/>
        /// </summary>
        /// <param name="spaceData"></param>
        /// <returns></returns>
        IEnumerable<IStaticPortal> AllOpenStaticPortalsFromSpace(ISpaceData spaceData);
    }
}
