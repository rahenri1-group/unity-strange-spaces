﻿using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections.Generic;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IStaticPortalManager"/>
    [Dependency(
        contract: typeof(IStaticPortalManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class StaticPortalManager : BasePortalManager<IStaticPortal>, IStaticPortalManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Static Portal Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        /// <summary>
        /// Injection constructor
        /// </summary>
        public StaticPortalManager(
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(spaceLoader,
                  spaceManager,
                  logger) { }

        /// <inheritdoc/>
        public void AttemptOpenStaticPortalConnection(IStaticPortalConnection staticPortalConnection)
        {
            if (!PortalConnectionMap.ContainsKey(staticPortalConnection))
            {
                PortalConnectionMap[staticPortalConnection] = new PortalConnectionData(staticPortalConnection);
            }

            SetupAndOpenPortalConnections(new IPortalConnection[]{ staticPortalConnection});
        }

        /// <inheritdoc/>
        public void CloseStaticPortalConnection(IStaticPortalConnection portalConnection)
        {
            ClosePortalConnection(portalConnection);
        }

        /// <inheritdoc/>
        public void RegisterStaticPortalEndPoint(IStaticPortal portal)
        {
            RegisterPortalForConnection(portal, portal.PortalConnection);
        }

        /// <inheritdoc/>
        public void UnregisterStaticPortalEndPoint(IStaticPortal portal)
        {
            UnregisterPortalForConnection(portal, portal.PortalConnection);
        }

        /// <inheritdoc/>
        public IEnumerable<IStaticPortal> AllOpenStaticPortalsToSpace(ISpaceData spaceData)
        {
            return AllOpenPortalsToSpace(spaceData);
        }

        /// <inheritdoc/>
        public IEnumerable<IStaticPortal> AllOpenStaticPortalsFromSpace(ISpaceData spaceData)
        {
            return AllOpenPortalsFromSpace(spaceData);
        }
    }
}
