﻿using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// A particle system that shadows another <see cref="ParticleSystem"/> through a <see cref="IPortal"/>.
    /// </summary>
    public class PortalShadowParticleBehaviour : InjectedBehaviour
    {
        private IPortal _portal;

        private ParticleSystem _particleSystemToShadow;
        private ParticleSystem _shadowParticles;

        private ParticleSystem.Particle[] _particleCache;

        public void Init(IPortal portal, ParticleSystem particleSystemToShadow)
        {
            _portal = portal;
            _particleSystemToShadow = particleSystemToShadow;

            _shadowParticles = gameObject.AddComponent<ParticleSystem>();

            var particleMain = _particleSystemToShadow.main;
            var shadowMain = _shadowParticles.main;
            _particleCache = new ParticleSystem.Particle[particleMain.maxParticles];

            shadowMain.cullingMode = ParticleSystemCullingMode.AlwaysSimulate;
            particleMain.cullingMode = ParticleSystemCullingMode.AlwaysSimulate;
            shadowMain.maxParticles = particleMain.maxParticles;
            shadowMain.simulationSpace = ParticleSystemSimulationSpace.Local;
            shadowMain.simulationSpeed = 0f;

            var shadowEmission = _shadowParticles.emission;
            shadowEmission.enabled = false;

            var shadowRenderer = _shadowParticles.GetComponent<ParticleSystemRenderer>();
            var particleRenderer = _particleSystemToShadow.GetComponent<ParticleSystemRenderer>();
            shadowRenderer.enabled = particleRenderer.enabled;
            if (shadowRenderer.enabled)
            {
                shadowRenderer.sharedMaterial = particleRenderer.sharedMaterial;
                shadowRenderer.trailMaterial = particleRenderer.trailMaterial;
                shadowRenderer.normalDirection = particleRenderer.normalDirection;
                shadowRenderer.alignment = particleRenderer.alignment;
                shadowRenderer.allowRoll = particleRenderer.allowRoll;
                shadowRenderer.renderMode = particleRenderer.renderMode;
                shadowRenderer.shadowCastingMode = particleRenderer.shadowCastingMode;
                shadowRenderer.rotateWithStretchDirection = particleRenderer.rotateWithStretchDirection;
                shadowRenderer.sortMode = particleRenderer.sortMode;
                shadowRenderer.flip = particleRenderer.flip;
                shadowRenderer.pivot = particleRenderer.pivot;
            }

            var shadowTextureAnimation = _shadowParticles.textureSheetAnimation;
            shadowTextureAnimation.enabled = _particleSystemToShadow.textureSheetAnimation.enabled;
            if (shadowTextureAnimation.enabled)
            {
                shadowTextureAnimation.mode = _particleSystemToShadow.textureSheetAnimation.mode;
                shadowTextureAnimation.numTilesX = _particleSystemToShadow.textureSheetAnimation.numTilesX;
                shadowTextureAnimation.numTilesY = _particleSystemToShadow.textureSheetAnimation.numTilesY;
                shadowTextureAnimation.animation = _particleSystemToShadow.textureSheetAnimation.animation;
                shadowTextureAnimation.frameOverTime = _particleSystemToShadow.textureSheetAnimation.frameOverTime;
                shadowTextureAnimation.frameOverTimeMultiplier = _particleSystemToShadow.textureSheetAnimation.frameOverTimeMultiplier;
                shadowTextureAnimation.startFrame = _particleSystemToShadow.textureSheetAnimation.startFrame;
                shadowTextureAnimation.cycleCount = _particleSystemToShadow.textureSheetAnimation.cycleCount;
                shadowTextureAnimation.uvChannelMask = _particleSystemToShadow.textureSheetAnimation.uvChannelMask;
                shadowTextureAnimation.timeMode = _particleSystemToShadow.textureSheetAnimation.timeMode;
            }
            

            var shadowTrails = _shadowParticles.trails;
            shadowTrails.enabled = _particleSystemToShadow.trails.enabled;
            if (shadowTrails.enabled)
            {
                shadowTrails.worldSpace = _particleSystemToShadow.trails.worldSpace;
                shadowTrails.textureMode = _particleSystemToShadow.trails.textureMode;
                shadowTrails.ratio = _particleSystemToShadow.trails.ratio;
                shadowTrails.lifetime = _particleSystemToShadow.trails.lifetime;
                shadowTrails.minVertexDistance = _particleSystemToShadow.trails.minVertexDistance;
                shadowTrails.dieWithParticles = _particleSystemToShadow.trails.dieWithParticles;
                shadowTrails.sizeAffectsWidth = _particleSystemToShadow.trails.sizeAffectsWidth;
                shadowTrails.sizeAffectsLifetime = _particleSystemToShadow.trails.sizeAffectsLifetime;
                shadowTrails.inheritParticleColor = _particleSystemToShadow.trails.inheritParticleColor;
                shadowTrails.colorOverLifetime = _particleSystemToShadow.trails.colorOverLifetime;
                shadowTrails.widthOverTrail = _particleSystemToShadow.trails.widthOverTrail;
                shadowTrails.colorOverTrail = _particleSystemToShadow.trails.colorOverTrail;
            }

            Sync();
        }

        private void LateUpdate()
        {
            Sync();
        }

        public void Sync()
        {
            

            if (_particleSystemToShadow.main.simulationSpace == ParticleSystemSimulationSpace.World)
            {
                _portal.CalculateEndPointTransform(Vector3.zero, Quaternion.identity,
                out var shadowPosition, out var shadowRotation);

                transform.position = shadowPosition;
                transform.rotation = shadowRotation;
            }
            else
            {
                _portal.CalculateEndPointTransform(_particleSystemToShadow.transform.position, _particleSystemToShadow.transform.rotation,
                out var shadowPosition, out var shadowRotation);

                transform.position = shadowPosition;
                transform.rotation = shadowRotation;
            }

            var particleCount = _particleSystemToShadow.GetParticles(_particleCache);
            _shadowParticles.SetParticles(_particleCache, particleCount);

            _shadowParticles.SetTrails(_particleSystemToShadow.GetTrails());
        }
    }
}
