﻿using UnityEngine;

namespace Game.Core.Portal
{
    public abstract partial class BaseShadowParticleCreator
    {
        private class PortalData 
        {
            public GameObject ShadowParticleGameObject { get; set; } = null;
            public PortalShadowParticleBehaviour ShadowParticle { get; set; } = null;

            public void DestroyShadow()
            {
                if (ShadowParticleGameObject)
                {
                    Destroy(ShadowParticleGameObject);
                    
                    ShadowParticle = null;
                    ShadowParticleGameObject = null;
                }
            }
        }
    }
}
