﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <summary>
    /// Base class for particle systems that can detect <see cref="IPortal"/>s and create shadows
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public abstract partial class BaseShadowParticleCreator : InjectedBehaviour
    {
        [Inject] protected ISpaceManager SpaceManager = null;
        [Inject] protected ISpacePhysics SpacePhysics = null;
        [Inject] protected ILogRouter Logger = null;

        [SerializeField][Layer] private int[] _portalQueryLayers = new int[0];
        [SerializeField] private FloatReadonlyReference _portalSearchRadius = null;

        private ParticleSystem _particleSystem;

        private Dictionary<IPortal, PortalData> _portalMap;

        private int _portalQueryMask;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_portalSearchRadius > 0f);

            _particleSystem = GetComponent<ParticleSystem>();

            _portalMap = new Dictionary<IPortal, PortalData>();

            _portalQueryMask = LayerUtil.ConstructMaskForLayers(_portalQueryLayers);
        }

        protected void UpdateNearbyPortals()
        {
            var detectedPortals = DetectPortals();

            var portalsToAdd = detectedPortals
                .Where(p => !_portalMap.ContainsKey(p))
                .ToArray();

            var portalsToRemove = _portalMap
                .Keys
                .Where(p => !detectedPortals.Contains(p))
                .ToArray();

            foreach (var portal in portalsToRemove)
            {
                var portalData = _portalMap[portal];
                portalData.DestroyShadow();

                _portalMap.Remove(portal);

                portal.PortalOpened -= OnPortalOpen;
                portal.PortalClosed -= OnPortalClose;
            }

            foreach (var portal in portalsToAdd)
            {
                _portalMap.Add(portal, new PortalData());

                portal.PortalOpened += OnPortalOpen;
                portal.PortalClosed += OnPortalClose;

                if (portal.IsOpen && portal.GetComponent<IPortalTransporter>() != null)
                {
                    CreateShadowForPortal(portal);
                }
            }
        }

        protected void PurgeAllPortals()
        {
            foreach (var pair in _portalMap)
            {
                var portal = pair.Key;
                var portalData = pair.Value;

                portalData.DestroyShadow();

                portal.PortalOpened -= OnPortalOpen;
                portal.PortalClosed -= OnPortalClose;
            }

            _portalMap.Clear();
        }

        private IPortal[] DetectPortals()
        {
            if (_particleSystem.emission.enabled)
            {
                var particleSpace = SpaceManager.GetObjectSpace(gameObject);

                if (SpacePhysics.FindInSphere<IPortal>(
                    particleSpace,
                    transform.position,
                    _portalSearchRadius,
                    out var resultInfo,
                    portalInteraction: QueryPortalInteraction.Block,
                    layerMask: _portalQueryMask))
                {
                    return new IPortal[] { resultInfo.Result };
                }
            }

            return new IPortal[0];
        }

        private void OnPortalOpen(IPortal sender)
        {
            if (sender.GetComponent<IPortalTransporter>() != null)
            {
                CreateShadowForPortal(sender);
            }
        }

        private void OnPortalClose(IPortal sender)
        {
            if (_portalMap.ContainsKey(sender))
            {
                var portalData = _portalMap[sender];

                portalData.DestroyShadow();
            }
        }

        private void CreateShadowForPortal(IPortal portal)
        {
            UniTask.Create(async () =>
            {
                var portalData = _portalMap[portal];

                portalData.ShadowParticleGameObject = new GameObject($"{_particleSystem.gameObject.name.PrependIfMissing("@")}-ParticleShadow");
                portalData.ShadowParticle = portalData.ShadowParticleGameObject.AddComponent<PortalShadowParticleBehaviour>();

                portalData.ShadowParticle.Init(portal, _particleSystem);

                await SpaceManager.MoveObjectToSpaceAsync(portalData.ShadowParticleGameObject, portal.EndPoint.Space);

                // verify that we still have the portal and it is open
                if (portal.IsOpen && _portalMap.ContainsKey(portal))
                {
                    portalData.ShadowParticle.Sync();
                }
                else
                {
                    // something happened mid-create
                    portalData.DestroyShadow();
                }

            }).Forget();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, _portalSearchRadius);
        }
    }
}
