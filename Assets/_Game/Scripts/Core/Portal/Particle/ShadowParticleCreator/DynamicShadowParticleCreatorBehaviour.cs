﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <summary>
    /// Portal detection system for a particle system that checks periodically (generally due to movement).
    /// </summary>
    public class DynamicShadowParticleCreatorBehaviour : BaseShadowParticleCreator
    {
        [SerializeField] private FloatReadonlyReference _detectionFrequency = null;

        private IEnumerator _portalDetectionCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_detectionFrequency > 0f);

            _portalDetectionCoroutine = null;
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            UpdateNearbyPortals();

            _portalDetectionCoroutine = PortalDetection();
            StartCoroutine(_portalDetectionCoroutine);
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            PurgeAllPortals();

            if (_portalDetectionCoroutine != null)
            {
                StopCoroutine(_portalDetectionCoroutine);
                _portalDetectionCoroutine = null;
            }
        }

        private IEnumerator PortalDetection()
        {
            float detectionDelay = 1f / _detectionFrequency;
            YieldInstruction wait = new WaitForSeconds(detectionDelay);

            yield return new WaitForSeconds(Random.Default.Range(0.1f * detectionDelay, detectionDelay));

            while (true)
            {
                UpdateNearbyPortals();

                yield return wait;
            }
        }
    }
}
