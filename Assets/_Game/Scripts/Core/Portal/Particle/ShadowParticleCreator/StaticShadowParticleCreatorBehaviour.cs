﻿namespace Game.Core.Portal
{
    /// <summary>
    /// Portal detection system for a particle system that only checks on enable and disable
    /// </summary>
    public class StaticShadowParticleCreatorBehaviour : BaseShadowParticleCreator
    {     
        /// <inheritdoc/>
        private void OnEnable()
        {
            UpdateNearbyPortals();
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            PurgeAllPortals();
        }
    }
}
