﻿using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Portal
{
    public partial class DijkstraPortalPathSolver<T_Portal>
    {
        public class DijkstraPathNode : IPortalPathNode<T_Portal>
        {
            /// <inheritdoc/>
            public T_Portal Portal { get; set; }
            /// <inheritdoc/>
            public ISpaceData NodeSpace { get; set; }
            /// <inheritdoc/>
            public Vector3 SpaceStartPosition { get; set; }
            /// <inheritdoc/>
            public Vector3 SpaceEndPosition { get; set; }
            /// <inheritdoc/>
            public float Distance { get; set; }
        }

        private class DijkstraPath : IPortalPath<T_Portal>, IComparable
        {
            /// <inheritdoc/>
            public float TotalDistance { get; private set; }

            /// <inheritdoc/>
            public IPortalPathNode<T_Portal>[] Path { get => PathArray; }

            /// <inheritdoc/>
            public bool IsPathStale => _pathSolverStateIndex != _solver.CurrentSolverStateIndex;

            public DijkstraPathNode[] PathArray
            {
                get
                {
                    if (_cachedArray == null) _cachedArray = _path.ToArray();
                    return _cachedArray;
                }
            }

            public DijkstraPathNode EndNode => _path.Last.Value;

            private readonly DijkstraPortalPathSolver<T_Portal> _solver;
            private readonly int _pathSolverStateIndex;

            private LinkedList<DijkstraPathNode> _path;
            private DijkstraPathNode[] _cachedArray;

            public DijkstraPath(DijkstraPortalPathSolver<T_Portal> solver)
            {
                _solver = solver;
                _pathSolverStateIndex = _solver.CurrentSolverStateIndex;

                _cachedArray = null;
                _path = new LinkedList<DijkstraPathNode>();
                TotalDistance = 0f;
            }

            public DijkstraPath(
                DijkstraPortalPathSolver<T_Portal> solver,
                DijkstraPathNode startNode)
            {
                _solver = solver;
                _pathSolverStateIndex = _solver.CurrentSolverStateIndex;

                _cachedArray = null;
                _path = new LinkedList<DijkstraPathNode>();
                TotalDistance = 0f;

                AppendNode(startNode);
            }

            public DijkstraPath(
                DijkstraPortalPathSolver<T_Portal> solver,
                DijkstraPathNode[] path)
            {
                _solver = solver;
                _pathSolverStateIndex = _solver.CurrentSolverStateIndex;

                _cachedArray = null;

                _path = new LinkedList<DijkstraPathNode>();
                TotalDistance = 0f;

                foreach (var node in path)
                {
                    AppendNode(node);
                }
            }

            public void AppendNode(DijkstraPathNode node)
            {
                _cachedArray = null;

                _path.AddLast(node);
                TotalDistance += node.Distance;
            }

            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                var other = obj as DijkstraPath;
                if (other != null)
                    return this.TotalDistance.CompareTo(other.TotalDistance);
                else
                    throw new ArgumentException($"Object is not a {nameof(DijkstraPath)}");
            }
        }   
    }
}
