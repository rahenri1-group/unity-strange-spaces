﻿using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// Dijkstra's algorithm implementation of a <see cref="IPortalPathSolver{T_Portal}"/>
    /// </summary>
    /// <typeparam name="T_Portal"></typeparam>
    public partial class DijkstraPortalPathSolver<T_Portal> : IPortalPathSolver<T_Portal>
        where T_Portal : class, IPortalPathLink
    {
        private class InterSpaceLink : IComparable
        {
            public T_Portal PortalA { get; set; }
            public Vector3 PositionA { get; set; }

            public T_Portal PortalB { get; set; }
            public Vector3 PositionB { get; set; }

            public float Distance { get; set; }

            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                var other = obj as InterSpaceLink;
                if (other != null)
                    return this.Distance.CompareTo(other.Distance);
                else
                    throw new ArgumentException($"Object is not a {nameof(InterSpaceLink)}");
            }
        }

        /// <summary>
        /// The current index that changes whenever a portal is added or removed. 
        /// Used to check if a generated path is still valid.
        /// </summary>
        public int CurrentSolverStateIndex { get; private set; }

        private HashSet<T_Portal> _portals;

        // map of portal.ExitPortal to the portals in the same space
        // PortalA.Exit -> PortalB.Entry
        private Dictionary<T_Portal, HashSet<InterSpaceLink>> _cachedInterSpaceLinks; 
        
        /// <summary>
        /// Constructor
        /// </summary>
        public DijkstraPortalPathSolver()
        {
            CurrentSolverStateIndex = 0;

            _portals = new HashSet<T_Portal>();
            _cachedInterSpaceLinks = null;
        }

        /// <inheritdoc/>
        public void AddPortal(T_Portal portal)
        {
            if (_portals.Contains(portal)) return;

            _portals.Add(portal);

            _cachedInterSpaceLinks = null;
            CurrentSolverStateIndex += 1;
        }

        /// <inheritdoc/>
        public void RemovePortal(T_Portal portal)
        {
            if (!_portals.Contains(portal)) return;

            _portals.Remove(portal);

            _cachedInterSpaceLinks = null;
            CurrentSolverStateIndex += 1;
        }

        /// <inheritdoc/>
        public void ClearAllPortals()
        {
            _portals.Clear();

            _cachedInterSpaceLinks = null;
            CurrentSolverStateIndex += 1;
        }

        /// <inheritdoc/>
        public bool GetPath(SpacePosition startPosition, SpacePosition targetPosition, out IPortalPath<T_Portal> resultPath)
        {
            return GetPath(startPosition, targetPosition, float.MaxValue, out resultPath);
        }

        /// <inheritdoc/>
        public bool GetPath(SpacePosition startPosition, SpacePosition targetPosition, float maxCost, out IPortalPath<T_Portal> resultPath)
        {
            resultPath = new DijkstraPath(this);

            // if in same space as the listener, that is the path
            if (SpaceUtil.SpaceEquals(startPosition.Space, targetPosition.Space))
            {
                var distance = Vector3.Distance(startPosition.Position, targetPosition.Position);

                if (distance <= maxCost)
                {
                    resultPath = new DijkstraPath(
                        this,
                        new DijkstraPathNode
                        {
                            Portal = null,
                            NodeSpace = startPosition.Space,
                            SpaceStartPosition = startPosition.Position,
                            SpaceEndPosition = targetPosition.Position,
                            Distance = distance
                        });

                    return true;
                }
                else
                {
                    return false;
                }
            }


            if (_cachedInterSpaceLinks == null)
            {
                BuildInterSpaceLinkCache();
            }

            // copy cached links
            var availableInterSpaceLinks = new Dictionary<T_Portal, HashSet<InterSpaceLink>>();
            foreach (var pair in _cachedInterSpaceLinks)
            {
                availableInterSpaceLinks[pair.Key] = new HashSet<InterSpaceLink>();
                availableInterSpaceLinks[pair.Key].UnionWith(pair.Value);
            }

            // add end links
            _portals.Where(p => SpaceUtil.SpaceEquals(targetPosition.Space, p.ExitPortal.Space)).ToList()
                .ForEach(portal =>
                {
                    availableInterSpaceLinks[portal].Add(new InterSpaceLink
                    {
                        PortalA = portal,
                        PositionA = portal.ExitPortal.Transform.position,

                        PortalB = null,
                        PositionB = targetPosition.Position,

                        Distance = Vector3.Distance(targetPosition.Position, portal.ExitPortal.Transform.position)
                    });
                });


            var avaiablePaths = new HashSet<DijkstraPath>();

            // add first paths
            _portals.Where(p => SpaceUtil.SpaceEquals(startPosition.Space, p.EntryPortal.Space)).ToList()
                .ForEach(portal =>
                {
                    avaiablePaths.Add(new DijkstraPath(
                        this,
                        new DijkstraPathNode
                        {
                            Portal = portal,
                            NodeSpace = startPosition.Space,
                            SpaceStartPosition = startPosition.Position,
                            SpaceEndPosition = portal.EntryPortal.Transform.position,
                            Distance = Vector3.Distance(startPosition.Position, portal.EntryPortal.Transform.position)
                        }
                    ));
                });

            // find shortest path
            while (avaiablePaths.Count > 0)
            {
                var possiblePath = avaiablePaths.Min();
                avaiablePaths.Remove(possiblePath);

                if (possiblePath.TotalDistance >= maxCost)
                {
                    // even the cheapest possible path is too long
                    break;
                }

                if (possiblePath.EndNode.Portal == null)
                {
                    // found a path
                    resultPath = possiblePath;

                    return true;
                }

                if (availableInterSpaceLinks.ContainsKey(possiblePath.EndNode.Portal))
                {
                    var links = availableInterSpaceLinks[possiblePath.EndNode.Portal];
                    availableInterSpaceLinks.Remove(possiblePath.EndNode.Portal);

                    foreach (var link in links)
                    {
                        var newPath = new DijkstraPath(this, possiblePath.PathArray);
                        newPath.AppendNode(new DijkstraPathNode
                        {
                            Portal = link.PortalB,
                            NodeSpace = possiblePath.EndNode.Portal.ExitPortal.Space,
                            SpaceStartPosition = link.PositionA,
                            SpaceEndPosition = link.PositionB,
                            Distance = link.Distance
                        });

                        avaiablePaths.Add(newPath);
                    }
                }
            }

            // didn't find a path
            return false;
        }

        private void BuildInterSpaceLinkCache()
        {
            _cachedInterSpaceLinks = new Dictionary<T_Portal, HashSet<InterSpaceLink>>();

            foreach (var portal in _portals)
            {
                _cachedInterSpaceLinks[portal] = new HashSet<InterSpaceLink>();

                _portals.Where(p => p != portal && p.ExitPortal != portal.EntryPortal && SpaceUtil.SpaceEquals(portal.ExitPortal.Space, p.EntryPortal.Space)).ToList()
                    .ForEach(interspacePortal =>
                    {
                        _cachedInterSpaceLinks[portal].Add(new InterSpaceLink
                        {
                            PortalA = portal,
                            PositionA = portal.ExitPortal.Transform.position,

                            PortalB = interspacePortal,
                            PositionB = interspacePortal.EntryPortal.Transform.position,

                            Distance = Vector3.Distance(portal.ExitPortal.Transform.position, interspacePortal.EntryPortal.Transform.position)
                        });
                    });
            }
        }
    }
}
