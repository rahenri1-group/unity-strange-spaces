﻿namespace Game.Core.Portal
{
    public interface IPortalPath<T_Portal>
        where T_Portal : IPortalPathLink
    {
        /// <summary>
        /// The spacial path from the beginning to the end
        /// </summary>
        public IPortalPathNode<T_Portal>[] Path { get; }

        /// <summary>
        /// The total distance of the path
        /// </summary>
        float TotalDistance { get; }

        /// <summary>
        /// If true the path is stale and possibly invalid
        /// </summary>
        bool IsPathStale { get; }
    }
}
