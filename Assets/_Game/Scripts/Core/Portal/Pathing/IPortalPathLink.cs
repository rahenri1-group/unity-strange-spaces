﻿namespace Game.Core.Portal
{
    /// <summary>
    /// Interface for a portal link used by <see cref="IPortalPathSolver{T_Portal}"/>
    /// </summary>
    public interface IPortalPathLink
    {
        /// <summary>
        /// The entry portal
        /// </summary>
        IPortal EntryPortal { get; }

        /// <summary>
        /// The exit portal
        /// </summary>
        IPortal ExitPortal { get; }
    }
}
