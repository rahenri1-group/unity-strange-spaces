﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// A portion of the path, describes the portion between two locations in the same <see cref="ISpaceData"/>
    /// </summary>
    public interface IPortalPathNode<T> where T : IPortalPathLink
    {
        /// <summary>
        /// The portal to the next node in the path. This will be null for the last node
        /// </summary>
        public T Portal { get; }

        /// <summary>
        /// The space of the node
        /// </summary>
        public ISpaceData NodeSpace { get; }

        /// <summary>
        /// The start position within the space
        /// </summary>
        public Vector3 SpaceStartPosition { get; }

        /// <summary>
        /// The end position within the space
        /// </summary>
        public Vector3 SpaceEndPosition { get; }

        /// <summary>
        /// The distance from the <see cref="SpaceStartPosition"/> to <see cref="SpaceEndPosition"/>
        /// </summary>
        public float Distance { get; }
    }
}
