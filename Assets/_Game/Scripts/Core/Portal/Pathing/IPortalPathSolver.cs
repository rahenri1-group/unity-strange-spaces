﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// Solver that generates a path between spaces using portal links
    /// </summary>
    /// <typeparam name="T_Portal"></typeparam>
    public interface IPortalPathSolver<T_Portal>
        where T_Portal : IPortalPathLink
    {
        /// <summary>
        /// Adds a portal to the solver
        /// </summary>
        /// <param name="portal"></param>
        void AddPortal(T_Portal portal);
        /// <summary>
        /// Removes a portal from the solver
        /// </summary>
        /// <param name="portal"></param>
        void RemovePortal(T_Portal portal);
        /// <summary>
        /// Removes all portals from the solver
        /// </summary>
        void ClearAllPortals();

        /// <summary>
        /// Looks for a path between two locations using portals.
        /// Returns true if a path exists
        /// </summary>
        bool GetPath(SpacePosition startPosition, SpacePosition targetPosition, out IPortalPath<T_Portal> path);

        /// <summary>
        /// Looks for a path between two locations using portals up to the a maximum tranversal cost.
        /// Returns true if a path exists
        /// </summary>
        bool GetPath(SpacePosition startPosition, SpacePosition targetPosition, float maxCost, out IPortalPath<T_Portal> path);
    }
}
