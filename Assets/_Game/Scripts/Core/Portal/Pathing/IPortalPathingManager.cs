﻿using Game.Core.Space;

namespace Game.Core.Portal
{
    /// <summary>
    /// Finds path between two points using <see cref="IPortalTransporter"/>s
    /// </summary>
    public interface IPortalPathingManager
    {
        /// <summary>
        /// Returns true if there is a path from <paramref name="startPosition"/> to <paramref name="destinationPosition"/>
        /// Returns false if there is no path.
        /// </summary>
        bool GetPath(SpacePosition startPosition, SpacePosition destinationPosition, out IPortalPath<IPortalTransporter> path);

        /// <summary>
        /// Returns true if there is a path from <paramref name="startPosition"/> to <paramref name="destinationPosition"/>
        /// Returns false if there is no path or the path is greather than <paramref name="maxDistance"/>
        /// </summary>
        bool GetPath(SpacePosition startPosition, SpacePosition destinationPosition, float maxDistance, out IPortalPath<IPortalTransporter> path);
    }
}