﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Space;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalPathingManager"/>
    [Dependency(
        contract: typeof(IPortalPathingManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class PortalPathingManager : BaseModule, IPortalPathingManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Portal Pathing Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly IEventBus _eventBus;
        private readonly ISpaceManager _spaceManager;

        private IPortalPathSolver<IPortalTransporter> _portalPathSolver;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PortalPathingManager(
            IEventBus eventBus,
            ILogRouter logger,
            ISpaceManager spaceManager)
            : base(logger) 
        {
            _eventBus = eventBus;
            _spaceManager = spaceManager;

            _portalPathSolver = new DijkstraPortalPathSolver<IPortalTransporter>();
        }

        /// <inheritdoc/>
        public override UniTask Initialize()
        {
            _eventBus.Subscribe<PortalOpenedEvent>(OnPortalOpened);
            _eventBus.Subscribe<PortalClosedEvent>(OnPortalClosed);

            return base.Initialize();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _portalPathSolver.ClearAllPortals();

            _eventBus.Unsubscribe<PortalOpenedEvent>(OnPortalOpened);
            _eventBus.Unsubscribe<PortalClosedEvent>(OnPortalClosed);

            return base.Shutdown();
        }

        private void OnPortalOpened(PortalOpenedEvent eventData)
        {
            IPortalTransporter portalTransporter = eventData.Portal.GetComponent<IPortalTransporter>();
            if (portalTransporter == null) return;

            _portalPathSolver.AddPortal(portalTransporter);
        }

        private void OnPortalClosed(PortalClosedEvent eventData)
        {
            IPortalTransporter portalTransporter = eventData.Portal.GetComponent<IPortalTransporter>();
            if (portalTransporter == null) return;

            _portalPathSolver.RemovePortal(portalTransporter);
        }

        /// <inheritdoc/>
        public bool GetPath(SpacePosition startPosition, SpacePosition destinationPosition, out IPortalPath<IPortalTransporter> pathInfo)
        {
            return GetPath(startPosition, destinationPosition, float.MaxValue, out pathInfo);
        }

        /// <inheritdoc/>
        public bool GetPath(SpacePosition startPosition, SpacePosition destinationPosition, float maxDistance, out IPortalPath<IPortalTransporter> pathInfo)
        {
            return _portalPathSolver.GetPath(
                startPosition,
                destinationPosition,
                maxDistance,
                out pathInfo);
        }
    }
}
