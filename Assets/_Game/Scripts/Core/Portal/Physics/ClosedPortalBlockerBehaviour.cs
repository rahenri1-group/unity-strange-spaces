﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal.Physics
{
    [RequireComponent(typeof(Collider))]
    public class ClosedPortalBlockerBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IPortal))] private Component _portalObj = null;
        private IPortal _portal;

        private Collider _portalCollider;

        private void Awake()
        {
            Assert.IsNotNull(_portalObj);
            _portal = _portalObj.GetComponentAsserted<IPortal>();

            _portalCollider = this.GetComponentAsserted<Collider>();

            _portalCollider.enabled = !_portal.IsOpen;
        }

        private void OnEnable()
        {
            _portal.PortalOpened += OnPortalOpenedOrClosed;
            _portal.PortalClosed += OnPortalOpenedOrClosed;
        }

        private void OnDisable()
        {
            _portal.PortalOpened -= OnPortalOpenedOrClosed;
            _portal.PortalClosed -= OnPortalOpenedOrClosed;
        }

        private void OnPortalOpenedOrClosed(IPortal sender)
        {
            _portalCollider.enabled = !_portal.IsOpen;
        }
    }
}
