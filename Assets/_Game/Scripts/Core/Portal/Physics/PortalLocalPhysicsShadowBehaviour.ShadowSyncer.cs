﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Portal.Physics
{
    public partial class PortalLocalPhysicsShadowBehaviour
    {
        private class ShadowSyncer
        {
            public readonly GameObject EntityGameObject;
            public readonly GameObject ColliderGameObject;
            public readonly GameObject ShadowColliderGameObject;          

            private Dictionary<Collider, Collider> _colliderMap;

            public ShadowSyncer(GameObject entityGameObject, GameObject originalGameObject, GameObject shadowGameObject)
            {
                EntityGameObject = entityGameObject;
                ColliderGameObject = originalGameObject;
                ShadowColliderGameObject = shadowGameObject;

                _colliderMap = new Dictionary<Collider, Collider>();
            }

            public void RegisterCollider(Collider originalCollider, Collider shadowCollider)
            {
                _colliderMap[originalCollider] = shadowCollider;
            }

            public void SyncShadow()
            {
                // colliders in root already handled
                if (ColliderGameObject != EntityGameObject)
                {
                    ShadowColliderGameObject.transform.localPosition = EntityGameObject.transform.InverseTransformPoint(ColliderGameObject.transform.position);
                    ShadowColliderGameObject.transform.localRotation = EntityGameObject.transform.InverseTransformRotation(ColliderGameObject.transform.rotation);
                    ShadowColliderGameObject.transform.localScale = Vector3.Scale(EntityGameObject.transform.lossyScale.Invert(), ColliderGameObject.transform.lossyScale);

                    ShadowColliderGameObject.SetActive(ColliderGameObject.activeInHierarchy);
                }

                foreach (var pair in _colliderMap)
                {
                    var originalCollider = pair.Key;
                    var shadowCollider = pair.Value;

                    shadowCollider.enabled = originalCollider.enabled;
                }
            }
        }
    }
}
