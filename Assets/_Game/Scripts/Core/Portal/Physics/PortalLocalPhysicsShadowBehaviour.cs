﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Interaction;
using Game.Core.Physics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal.Physics
{
    /// <summary>
    /// A portal shadow of the physics portions of a <see cref="IDynamicEntity"/>s
    /// </summary>
    public partial class PortalLocalPhysicsShadowBehaviour : InjectedBehaviour
    {
        /// <summary>
        /// The portal that necessitated a shadow being created
        /// </summary>
        public IPortal Portal { get; set; }

        /// <summary>
        /// The <see cref="IDynamicEntity"/> that is being shadowed
        /// </summary>
        public IDynamicEntity EntityToShadow
        {
            get => _entityToShadow;
            set
            {
                _entityToShadow = value;
                if (_entityToShadow != null)
                {
                    _entityToShadowMovement = _entityToShadow.GetComponent<IRigidBodyMovement>();
                    _entityToShadowInteractable = _entityToShadow.GetComponent<IInteractable>();
                }
                else
                {
                    _entityToShadowMovement = null;
                    _entityToShadowInteractable = null;
                }
            }
        }

        [Inject] private IEventBus _eventBus = null;
        [Inject] private ILogRouter _logger = null;

        private Rigidbody _localBody;

        private IDynamicEntity _entityToShadow = null;
        private IInteractable _entityToShadowInteractable = null;
        private IRigidBodyMovement _entityToShadowMovement = null;

        // map of original GameObjects to their shadow sync data
        private Dictionary<GameObject, ShadowSyncer> _shadowSyncMap;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _shadowSyncMap = new Dictionary<GameObject, ShadowSyncer>();
        }

        /// <inheritdoc/>
        private void Start()
        {
            Assert.IsNotNull(EntityToShadow);

            _localBody = gameObject.AddComponent<Rigidbody>();
            _localBody.useGravity = false;
            if (_entityToShadowMovement != null)
            {
                _localBody.isKinematic = false;
                _localBody.mass = _entityToShadowMovement.Rigidbody.mass;
                _localBody.drag = _entityToShadowMovement.Rigidbody.drag;
                _localBody.collisionDetectionMode = _entityToShadowMovement.Rigidbody.collisionDetectionMode;
            }
            else
            {
                _localBody.isKinematic = true;
                _localBody.mass = 100;
                _localBody.drag = 0;
                _localBody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            }

            GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale);
            _localBody.position = position;
            _localBody.rotation = rotation;
            transform.localScale = scale;

            RebuildColliders();
            Sync();

            _eventBus.Subscribe<EntityModifiedEvent>(OnEntityModified);
        }

        /// <inheritdoc/>
        private void OnDestroy()
        {
            _eventBus.Unsubscribe<EntityModifiedEvent>(OnEntityModified);
        }

        /// <inheritdoc/>
        private void FixedUpdate()
        {
            Sync();

            GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale);
            _localBody.MovePosition(position);
            _localBody.MoveRotation(rotation);
            transform.localScale = scale;
        }

        private void OnEntityModified(EntityModifiedEvent eventArgs)
        {
            if (eventArgs.Entity == EntityToShadow)
            {
                RebuildColliders();
                Sync();
            }
        }

        /// <inheritdoc/>
        private void OnCollisionEnter(Collision collisionInfo)
        {
            ApplyCollisionToEntity(collisionInfo);
        }

        /// <inheritdoc/>
        private void OnCollisionStay(Collision collisionInfo)
        {
            ApplyCollisionToEntity(collisionInfo);
        }

        private void RebuildColliders()
        {
            foreach (var pair in _shadowSyncMap)
            {
                var shadowSyncer = pair.Value;
                Destroy(shadowSyncer.ShadowColliderGameObject);
            }

            _shadowSyncMap.Clear();

            if (EntityToShadow != null)
            {
                foreach (var collider in EntityToShadow.GetComponentsInChildren<Collider>(true))
                {
                    RegisterGameObjectCollider(collider.gameObject, collider);
                }
            }
        }

        private void ApplyCollisionToEntity(Collision collisionInfo)
        {
            if (_entityToShadowMovement == null || _entityToShadowMovement.Rigidbody.isKinematic || collisionInfo.contactCount == 0) return;

            // hacky fix
            // don't apply forces to interacted upon entities
            // this prevents kinematic shadow colliders from moving held entities
            if (_entityToShadowInteractable != null && _entityToShadowInteractable.IsBeingInteractedUpon) return;

            var collisionForce = (collisionInfo.impulse) / collisionInfo.contactCount;
            for (int i = 0; i < collisionInfo.contactCount; i++)
            {
                var contact = collisionInfo.GetContact(i);
                _entityToShadowMovement.Rigidbody.AddForceAtPosition(collisionForce, contact.point, ForceMode.Impulse);
            }
        }

        private void RegisterGameObjectCollider(GameObject colliderGameObject, Collider collider)
        {
            if (!_shadowSyncMap.ContainsKey(colliderGameObject))
            {
                var shadowGo = new GameObject(colliderGameObject.name);
                shadowGo.layer = collider.gameObject.layer;
                shadowGo.transform.SetParent(transform);
                shadowGo.transform.localPosition = Vector3.zero;
                shadowGo.transform.localRotation = Quaternion.identity;
                shadowGo.transform.localScale = Vector3.one;

                _shadowSyncMap[colliderGameObject] = new ShadowSyncer(EntityToShadow.GameObject, colliderGameObject, shadowGo);
            }

            if (collider.isTrigger)
            {
                return;
            }

            var shadowSyncer = _shadowSyncMap[colliderGameObject];
            Collider shadowCollider = null;

            if (collider is SphereCollider)
            {
                shadowCollider = PhysicsUtil.CopySphereCollider(collider as SphereCollider, shadowSyncer.ShadowColliderGameObject.AddComponent<SphereCollider>());
            }
            else if(collider is BoxCollider)
            {
                shadowCollider = PhysicsUtil.CopyBoxCollider(collider as BoxCollider, shadowSyncer.ShadowColliderGameObject.AddComponent<BoxCollider>());
            }
            else if (collider is CapsuleCollider)
            {
                shadowCollider = PhysicsUtil.CopyCapsuleCollider(collider as CapsuleCollider, shadowSyncer.ShadowColliderGameObject.AddComponent<CapsuleCollider>());
            }
            else if (collider is CharacterController)
            {
                shadowCollider = PhysicsUtil.CopyCharacterControllerAsCapsuleCollider(collider as CharacterController, shadowSyncer.ShadowColliderGameObject.AddComponent<CapsuleCollider>());
            }
            else if (collider is MeshCollider)
            {
                shadowCollider = PhysicsUtil.CopyMeshCollider(collider as MeshCollider, shadowSyncer.ShadowColliderGameObject.AddComponent<MeshCollider>());
            }
            else
            {
                _logger.LogWarning($"Unknown collider {collider.GetType()} on gameobject {colliderGameObject.name}");
            }

            if (shadowCollider != null)
            {
                shadowSyncer.RegisterCollider(collider, shadowCollider);
            }
        }

        private void GetNextPositionRotationScale(out Vector3 position, out Quaternion rotation, out Vector3 scale)
        {
            Portal.CalculateEndPointTransform(
                EntityToShadow.EntityMovement.Position, EntityToShadow.EntityMovement.Rotation, 
                out position, out rotation);

            scale = EntityToShadow.GameObject.transform.localScale;
        }

        private void Sync()
        {
            foreach (var pair in _shadowSyncMap)
            {
                //var entityColliderGameObject = pair.Key;
                var shadowSyncData = pair.Value;
                shadowSyncData.SyncShadow();
            }
        }
    }
}
