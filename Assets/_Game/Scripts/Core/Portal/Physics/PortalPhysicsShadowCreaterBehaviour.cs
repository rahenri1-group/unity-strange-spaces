﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Portal.Physics
{
    /// <summary>
    /// Component of a portal that creates shadows for the physics portion of an <see cref="IDynamicEntity"/>
    /// </summary>
    public class PortalPhysicsShadowCreaterBehaviour : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus;
        [Inject] private ILogRouter _logger;
        [Inject] private ISpaceManager _spaceManager;

        private IPortal _parentPortal;

        private IPortalTransporter _pairedPortalTransporter;

        private Dictionary<Guid, PortalLocalPhysicsShadowBehaviour> _entityPhysicsShadowMap;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _parentPortal = this.GetComponentAsserted<IPortal>();
            _pairedPortalTransporter = null;

            _entityPhysicsShadowMap = new Dictionary<Guid, PortalLocalPhysicsShadowBehaviour>();

            _parentPortal.PortalOpened += OnPortalConnected;
            _parentPortal.PortalClosed += OnPortalDisconnected;

            _eventBus.Subscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        /// <inheritdoc/>
        private void OnDestroy()
        {
            if (_pairedPortalTransporter != null)
            {
                RemovePhysicsShadows();

                _pairedPortalTransporter.EntityEnterPortalZone -= OnEntityEnterConnectedPortal;
                _pairedPortalTransporter.EntityExitPortalZone -= OnEntityExitConnectedPortal;

                _pairedPortalTransporter = null;
            }

            _parentPortal.PortalOpened -= OnPortalConnected;
            _parentPortal.PortalClosed -= OnPortalDisconnected;

            _eventBus.Unsubscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        private void OnEntityDestroy(EntityDestroyEvent eventArgs)
        {
            if (_entityPhysicsShadowMap.ContainsKey(eventArgs.EntityId))
            {
                var shadow = _entityPhysicsShadowMap[eventArgs.EntityId];
                _entityPhysicsShadowMap.Remove(eventArgs.EntityId);

                shadow.gameObject.SetActive(false);
                Destroy(shadow.gameObject);
            }
        }

        private void OnPortalConnected(IPortal portal)
        {
            _pairedPortalTransporter = _parentPortal.EndPoint.GetComponent<IPortalTransporter>();
            if (_pairedPortalTransporter != null)
            {
                _pairedPortalTransporter.EntityEnterPortalZone += OnEntityEnterConnectedPortal;
                _pairedPortalTransporter.EntityExitPortalZone += OnEntityExitConnectedPortal;

                foreach (var entity in _pairedPortalTransporter.EntitiesInPortalZone)
                {
                    OnEntityEnterConnectedPortal(_pairedPortalTransporter, entity);
                }
            }
        }

        private void OnPortalDisconnected(IPortal portal)
        {
            if (_pairedPortalTransporter != null)
            {
                RemovePhysicsShadows();

                _pairedPortalTransporter.EntityEnterPortalZone -= OnEntityEnterConnectedPortal;
                _pairedPortalTransporter.EntityExitPortalZone -= OnEntityExitConnectedPortal;

                _pairedPortalTransporter = null;
            }
        }

        private void OnEntityEnterConnectedPortal(IPortalTransporter portalTransporter, IDynamicEntity entity)
        {
            if (_entityPhysicsShadowMap.ContainsKey(entity.EntityId))
            {
                //_logger.LogWarning($"{entity.GameObject.name} already has entered portal area it was already in");
                return;
            }

            // add null to hold place until async completes
            _entityPhysicsShadowMap.Add(entity.EntityId, null);

            UniTask.Create(async () =>
            {
                var shadowGameObject = new GameObject($"{entity.GameObject.name.PrependIfMissing("@")}-PhysicsShadow");
                await _spaceManager.MoveObjectToSpaceAsync(shadowGameObject, _spaceManager.GetObjectSpace(gameObject));

                shadowGameObject.layer = entity.GameObject.layer;
                var shadow = shadowGameObject.AddComponent<PortalLocalPhysicsShadowBehaviour>();
                shadow.Portal = _pairedPortalTransporter.EntryPortal;
                shadow.EntityToShadow = entity;

                _entityPhysicsShadowMap[entity.EntityId] = shadow;
            }).Forget();
        }

        private void OnEntityExitConnectedPortal(IPortalTransporter portalTransporter, IDynamicEntity entity)
        {
            if (!_entityPhysicsShadowMap.ContainsKey(entity.EntityId))
            {
                //_logger.LogWarning($"{entity.GameObject.name} has already left portal area");
                return;
            }

            UniTask.Create(async () =>
            {
                // wait until the entity has been removed or the shadow has finished being created
                await UniTask.WaitUntil(() => !_entityPhysicsShadowMap.ContainsKey(entity.EntityId) || _entityPhysicsShadowMap[entity.EntityId] != null, PlayerLoopTiming.FixedUpdate);

                // Remove shadow if entity is still mapped
                if (_entityPhysicsShadowMap.ContainsKey(entity.EntityId))
                {
                    var shadow = _entityPhysicsShadowMap[entity.EntityId];
                    _entityPhysicsShadowMap.Remove(entity.EntityId);

                    shadow.gameObject.SetActive(false);
                    Destroy(shadow.gameObject);
                }
            }).Forget();
        }

        private void RemovePhysicsShadows()
        {
            foreach (var pair in _entityPhysicsShadowMap)
            {
                var shadow = pair.Value;
                shadow.gameObject.SetActive(false);
                Destroy(shadow.gameObject);
            }

            _entityPhysicsShadowMap.Clear();
        }
    }
}