﻿namespace Game.Core.Portal.Physics
{
    /// <summary>
    /// A shadow of an <see cref="ITriggerable"/> that updates it's position every frame
    /// </summary>
    public class DynamicShadowTriggerableBehaviour : ShadowTriggerableBehaviour
    {
        private void FixedUpdate()
        {
            if (Portal.IsOpen)
                SyncPosition();
        }
    }
}
