﻿using Game.Core.Physics;
using UnityEngine;

namespace Game.Core.Portal.Physics
{
    public class PortalTriggerableShadowCreaterBehaviour : BasePortalTransporterShadowCreater, ITriggerable
    {
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerEnter;
        /// <inheritdoc/>
        public event TriggerableEventHandler TriggerExit;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _triggerableToShadowObj = null;
        private ITriggerable _triggerableToShadow;

        [SerializeField] private BoolReadonlyReference _syncShadowPositionOnUpdate = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _triggerableToShadow = _triggerableToShadowObj.GetComponent<ITriggerable>();
        }

        protected override GameObject CreateShadowForPortal(IPortal portal)
        {
            var shadowGameObject = Instantiate(_triggerableToShadow.GameObject);
            shadowGameObject.name = $"{_triggerableToShadow.GameObject.name.PrependIfMissing("@")}-TriggerableShadow";

            ShadowTriggerableBehaviour shadowTrigger = null;
            if (_syncShadowPositionOnUpdate)
                shadowTrigger = shadowGameObject.AddComponent<DynamicShadowTriggerableBehaviour>();
            else
                shadowTrigger = shadowGameObject.AddComponent<ShadowTriggerableBehaviour>();

            shadowTrigger.PrimaryTrigger = _triggerableToShadow;
            shadowTrigger.Portal = portal;

            var shadowTriggerable = shadowGameObject.GetComponent<ITriggerable>();
            shadowTriggerable.TriggerEnter += OnTriggerableEnter;
            shadowTriggerable.TriggerExit += OnTriggerableExit;

            return shadowGameObject;
        }

        protected override void DestroyShadowForPortal(GameObject shadow, IPortal portal)
        {
            var triggerable = shadow.GetComponent<ITriggerable>();
            triggerable.TriggerEnter -= OnTriggerableEnter;
            triggerable.TriggerExit -= OnTriggerableExit;

            base.DestroyShadowForPortal(shadow, portal);
        }

        private void OnTriggerableEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            TriggerEnter?.Invoke(sender, args);
        }

        private void OnTriggerableExit(ITriggerable sender, TriggerableEventArgs args)
        {
            TriggerExit?.Invoke(sender, args);
        }
    }
}
