﻿using Game.Core.Physics;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal.Physics
{
    /// <summary>
    /// A shadow of an <see cref="ITriggerable"/>
    /// </summary>
    public class ShadowTriggerableBehaviour : InjectedBehaviour
    {
        /// <summary>
        /// The <see cref="ITriggerable"/> that is being shadowed
        /// </summary>
        public ITriggerable PrimaryTrigger { get; set; }
        /// <summary>
        /// The portal that necessitated a shadow being created
        /// </summary>
        public IPortal Portal { get; set; }

        private Rigidbody _body;

        /// <inheritdoc/>
        private void Start()
        {
            Assert.IsNotNull(PrimaryTrigger);
            Assert.IsNotNull(Portal);

            _body = gameObject.AddComponent<Rigidbody>();
            _body.isKinematic = true;
            _body.useGravity = false;

            _body.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

            SyncPosition();
        }

        protected void SyncPosition()
        {
            Portal.CalculateEndPointTransform(
                PrimaryTrigger.GameObject.transform.position, PrimaryTrigger.GameObject.transform.rotation,
                out Vector3 position, out Quaternion rotation);

            _body.position = position;
            _body.rotation = rotation;

            transform.localScale = PrimaryTrigger.GameObject.transform.localScale;
        }
    }
}
