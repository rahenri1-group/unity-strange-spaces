﻿using System;

namespace Game.Core.Portal
{
    /// <summary>
    /// Exception class for portals
    /// </summary>
    public class PortalException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PortalException(string message)
            : base(message) { }
    }
}
