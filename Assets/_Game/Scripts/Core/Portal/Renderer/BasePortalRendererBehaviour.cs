﻿using Cysharp.Threading.Tasks;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalRenderer"/>
    public abstract class BasePortalRendererBehaviour : InjectedBehaviour, IPortalRenderer
    {
        public IPortal Portal { get; private set; }

        /// <inheritdoc/>
        public bool RenderingEnabled 
        { 
            get => _portalRenderingEnabled; 
            set
            {
                _portalRenderingEnabled = value;
                _portalRenderer.enabled = value;
            }
        }

        [Inject] protected ICameraManager CameraManager = null;

        [SerializeField] protected Renderer _portalRenderer = null;

        private IPortalCamera _portalCamera;

        private bool _portalRenderingEnabled;
        private bool _didPortalRenderLastFrame;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_portalRenderer);

            Portal = this.GetComponentAsserted<IPortal>();
            _portalCamera = null;
            _didPortalRenderLastFrame = false;
            _portalRenderingEnabled = true;
        }

        /// <inheritdoc/>
        protected virtual void OnDestroy()
        {
            if (_portalCamera != null)
            {
                CameraManager.DestroyCamera(_portalCamera);
                _portalCamera = null;
            }
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (Portal.EndPoint != null)
            {
                OnPortalConnected(Portal);
            }
            else
            {
                OnPortalDisconnected(Portal);
            }

            Portal.PortalOpened += OnPortalConnected;
            Portal.PortalClosed += OnPortalDisconnected;
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            Portal.PortalOpened -= OnPortalConnected;
            Portal.PortalClosed -= OnPortalDisconnected;
        }

        /// <inheritdoc/>
        private void LateUpdate()
        {
            bool portalRendered = false;

            if (RenderingEnabled && _portalCamera != null && Portal.EndPoint != null)
            {
                var primaryCamera = CameraManager.ActivePrimaryCamera;
                if (primaryCamera != null && IsPortalVisibleToCamera(primaryCamera))
                {
                    _portalCamera.Render();
                    portalRendered = true;
                }
            }

            if (portalRendered != _didPortalRenderLastFrame)
            {
                if (portalRendered)
                    OnPortalRenderStart();
                else
                    OnPortalRenderStop();

                _didPortalRenderLastFrame = portalRendered;
            }
        }

        private void OnPortalConnected(IPortal portal)
        {
            OnPortalConnectedAsync().Forget();
        }

        protected virtual async UniTask OnPortalConnectedAsync()
        {
            if (_portalCamera == null)
            {
                _portalCamera = await CameraManager.CreateCamera<IPortalCamera>();
                _portalCamera.SetupForPortal(Portal, _portalRenderer);
            }

            _portalCamera.Enabled = true;
        }

        protected virtual void OnPortalDisconnected(IPortal portal)
        {
            if (_portalCamera != null)
            {
                _portalCamera.Enabled = false;
            }
        }

        protected virtual void OnPortalRenderStart() { }

        protected virtual void OnPortalRenderStop() { }

        /// <inheritdoc/>
        public bool IsPortalVisibleToCamera(ISpaceCamera camera)
        {
            // is the portal surface in the same space as the camera?
            if (!SpaceUtil.SpaceEquals(Portal.Space, camera.RenderedSpace))
            {
                return false;
            }

            // Only run position based tests if portal is not right next to camera
            var maxPortalDimension = Mathf.Max(Portal.PortalSize.x, Portal.PortalSize.y);
            if (Vector3.SqrMagnitude(camera.UnityCamera.transform.position - Portal.Transform.position) > maxPortalDimension * maxPortalDimension)
            {
                // is the portal facing same direction as the camera?
                bool isFacingCamera = false;
                var frustumCorners = new Vector3[4];
                camera.UnityCamera.CalculateFrustumCorners(new Rect(0f, 0f, 1f, 1f), camera.UnityCamera.farClipPlane, UnityEngine.Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
                foreach (var corner in frustumCorners)
                {
                    var worldSpaceDirection = camera.UnityCamera.transform.TransformVector(corner).normalized;
                    if (Vector3.Dot(worldSpaceDirection, Portal.Transform.forward) > 0)
                    {
                        isFacingCamera = true;
                        break;
                    }
                }
                if (!isFacingCamera)
                {
                    return false;
                }

                // check if portal is within camera frustum
                var bounds = new Bounds(Portal.Transform.position, Portal.Transform.lossyScale);
                var frustumPlanes = GeometryUtility.CalculateFrustumPlanes(camera.UnityCamera);
                if (!GeometryUtility.TestPlanesAABB(frustumPlanes, bounds))
                {
                    return false;
                }
            }

            // all tests passed (or portal is on top of camera)
            return true;
        }
    }
}
