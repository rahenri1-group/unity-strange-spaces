﻿using Game.Core.Render.Camera;

namespace Game.Core.Portal
{
    /// <summary>
    /// Render component for a <see cref="IPortal"/>
    /// </summary>
    public interface IPortalRenderer
    {
        /// <summary>
        /// Is rendering enabled
        /// </summary>
        bool RenderingEnabled { get; set; }

        /// <summary>
        /// Is the portal visible to <paramref name="camera"/>
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        bool IsPortalVisibleToCamera(ISpaceCamera camera);
    }
}
