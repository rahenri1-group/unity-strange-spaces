﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Portal
{
    /// <summary>
    /// Simple implementation of <see cref="IPortalRenderer"/>
    /// </summary>
    public class PortalRendererBehaviour : BasePortalRendererBehaviour
    {
        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _portalRenderer.enabled = false;
        }

        protected override void OnPortalRenderStart()
        {
            base.OnPortalRenderStart();

            _portalRenderer.enabled = true;
        }

        protected override void OnPortalRenderStop()
        {
            base.OnPortalRenderStop();

            _portalRenderer.enabled = false;
        }
    }
}
