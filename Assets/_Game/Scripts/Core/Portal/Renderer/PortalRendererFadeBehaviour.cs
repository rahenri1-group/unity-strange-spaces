﻿using Cysharp.Threading.Tasks;
using Game.Core.Space;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <summary>
    /// <see cref="IPortalRenderer"/> that fades in when the portal connects and out when the portal disconnects
    /// </summary>
    public class PortalRendererFadeBehaviour : BasePortalRendererBehaviour
    {
        private float Alpha
        {
            get
            {
                if (_portalRenderer.material.HasProperty(_portalAlphaShaderId))
                {
                    return _portalRenderer.material.GetFloat(_portalAlphaShaderId);
                }

                return 1f;
            }
            set
            {
                if (_portalRenderer.material.HasProperty(_portalAlphaShaderId))
                {
                    _portalRenderer.material.SetFloat(_portalAlphaShaderId, value);
                }
            }
        }

        [SerializeField] private FloatReadonlyReference _fadeConnectDuration = null;

        private int _portalAlphaShaderId;

        private IEnumerator _fadeCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_fadeConnectDuration > 0f);

            _portalAlphaShaderId = Shader.PropertyToID("_Alpha");

            Alpha = 0f;

            _fadeCoroutine = null;
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            if (_fadeCoroutine != null)
            {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;
            }
        }

        protected override async UniTask OnPortalConnectedAsync()
        {
            await base.OnPortalConnectedAsync();

            // only fade in if we are in the active camera space
            if (CameraManager.ActivePrimaryCamera != null && SpaceUtil.SpaceEquals(Portal.Space, CameraManager.ActivePrimaryCamera.RenderedSpace))
            {
                if (_fadeCoroutine != null) StopCoroutine(_fadeCoroutine);

                _fadeCoroutine = fadeTo(1f);
                StartCoroutine(_fadeCoroutine);
            }
        }

        protected override void OnPortalDisconnected(IPortal portal)
        {
            base.OnPortalDisconnected(portal);

            if (gameObject.activeInHierarchy)
            {
                if (_fadeCoroutine != null) StopCoroutine(_fadeCoroutine);

                _fadeCoroutine = fadeTo(0f);
                StartCoroutine(_fadeCoroutine);
            }
        }

        protected override void OnPortalRenderStart()
        {
            base.OnPortalRenderStart();

            if (_fadeCoroutine == null)
            {
                Alpha = 1f;
            }
        }

        protected override void OnPortalRenderStop()
        {
            base.OnPortalRenderStop();

            if (_fadeCoroutine == null)
            {
                Alpha = 0f;
            }
        }

        private IEnumerator fadeTo(float endAlpha)
        {
            YieldInstruction wait = null;

            float startAlpha = Alpha;

            float startFadeTime = Time.time;
            while (Time.time - startFadeTime < _fadeConnectDuration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startFadeTime) / _fadeConnectDuration);

                Alpha = Mathf.SmoothStep(startAlpha, endAlpha, lerp);
            }

            Alpha = endAlpha;

            _fadeCoroutine = null;
        }
    }
}