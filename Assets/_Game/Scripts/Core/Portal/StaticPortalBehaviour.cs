﻿using Game.Core.DependencyInjection;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IStaticPortal"/>
    public class StaticPortalBehaviour : BasePortal, IStaticPortal
    {
        /// <inheritdoc />
        public IStaticPortalConnection PortalConnection => _portalConnection;
        /// <inheritdoc />
        public override bool HasConnection => true;

        [Inject] protected IStaticPortalManager PortalManager = null;

        [SerializeField] private StaticPortalConnection _portalConnection = null;
        [SerializeField] private bool _openOnAwake = false;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_portalConnection);
        }

        /// <inheritdoc />
        protected override void OnSpaceLoaded()
        {
            base.OnSpaceLoaded();

            PortalManager.RegisterStaticPortalEndPoint(this);

            if (_openOnAwake)
            {
                PortalManager.AttemptOpenStaticPortalConnection(PortalConnection);
            }
        }

        /// <inheritdoc />
        protected override void OnSpaceUnloading()
        {
            base.OnSpaceUnloading();

            PortalManager.UnregisterStaticPortalEndPoint(this);
        }

        /// <inheritdoc />
        public override void AttemptOpen()
        {
            if (IsOpen)
            {
                Logger.LogWarning($"{PortalConnection.Name} is already open");
                return;
            }

            PortalManager.AttemptOpenStaticPortalConnection(PortalConnection);
        }

        /// <inheritdoc />
        public override void Close()
        {
            if (!IsOpen)
            {
                Logger.LogWarning($"{PortalConnection.Name} is already closed");
                return;
            }

            PortalManager.CloseStaticPortalConnection(PortalConnection);
        }
    }
}
