﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Physics;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <summary>
    /// Base class that creates shadows whenever a portal is detected
    /// </summary>
    public abstract class BasePortalTransporterShadowCreater : InjectedBehaviour
    {
        protected class PortalShadowData
        {
            public IPortalTransporter PortalTransporter;
            public GameObject Shadow;
            public bool IsInTrigger;
        }

        [Inject] protected ISpaceManager SpaceManager = null;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalTriggerableObj = null;
        private ITriggerable _portalTriggerable;

        protected Dictionary<IPortal, PortalShadowData> _portalShadowDataMap;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _portalTriggerable = _portalTriggerableObj.GetComponentAsserted<ITriggerable>();

            _portalShadowDataMap = new Dictionary<IPortal, PortalShadowData>();

            _portalTriggerable.TriggerEnter += OnPortalDetected;
            _portalTriggerable.TriggerExit += OnPortalLost;
        }

        /// <inheritdoc/>
        private void OnDestroy()
        {
            _portalTriggerable.TriggerEnter -= OnPortalDetected;
            _portalTriggerable.TriggerExit -= OnPortalLost;

            while (_portalShadowDataMap.Count > 0)
            {
                TeardownForPortal(_portalShadowDataMap.Keys.First());
            }
        }

        private void OnPortalDetected(ITriggerable sender, TriggerableEventArgs args)
        {
            var portalTransporter = args.Collider.GetComponentInParent<IPortalTransporter>();
            if (portalTransporter == null) return;

            var portal = portalTransporter.EntryPortal;

            SetupForPortal(portal, portalTransporter);

            if (portal.IsOpen)
            {
                SetupForPortal(portal.EndPoint, portal.EndPoint.GetComponent<IPortalTransporter>());
            }

            _portalShadowDataMap[portal].IsInTrigger = true;
        }

        private void OnPortalLost(ITriggerable sender, TriggerableEventArgs args)
        {
            if (args.Collider == null) return;

            var portalTransporter = args.Collider.GetComponentInParent<IPortalTransporter>();
            if (portalTransporter == null) return;

            var portal = portalTransporter.EntryPortal;

            if (!_portalShadowDataMap.ContainsKey(portal)) return;

            _portalShadowDataMap[portal].IsInTrigger = false;

            if (!portal.IsOpen
                || (_portalShadowDataMap.ContainsKey(portal.EndPoint) && !_portalShadowDataMap[portal.EndPoint].IsInTrigger))
            {
                TeardownForPortal(portal);

                if (portal.IsOpen)
                {
                    TeardownForPortal(portal.EndPoint);
                }
            }
        }

        protected virtual void OnPortalConnected(IPortal sender)
        {
            SetupForPortal(sender, sender.GetComponent<IPortalTransporter>());
        }

        private void OnPortalDisconnected(IPortal sender)
        {
            if (_portalShadowDataMap[sender].Shadow != null)
            {
                DestroyShadowForPortal(_portalShadowDataMap[sender].Shadow, sender);
                _portalShadowDataMap[sender].Shadow = null;
            }
        }

        protected void SetupForPortal(IPortal portal, IPortalTransporter portalTransporter)
        {
            if (portal == null || portalTransporter == null) return;

            if (!_portalShadowDataMap.ContainsKey(portal))
            {
                _portalShadowDataMap[portal] = new PortalShadowData
                {
                    PortalTransporter = portalTransporter,
                    Shadow = null,
                    IsInTrigger = false
                };

                portal.PortalOpened += OnPortalConnected;
                portal.PortalClosed += OnPortalDisconnected;
            }

            if (portal.IsOpen && _portalShadowDataMap[portal].Shadow == null)
            {
                UniTask.Create(async () =>
                {
                    var shadow = CreateShadowForPortal(portal);
                    await SpaceManager.MoveObjectToSpaceAsync(shadow, portal.EndPoint.Space);

                    if (_portalShadowDataMap.ContainsKey(portal)
                        && portal.IsOpen
                        && _portalShadowDataMap[portal].Shadow == null)
                    {
                        _portalShadowDataMap[portal].Shadow = shadow;
                    }
                    else
                    {
                        // something happened mid-create
                        DestroyShadowForPortal(shadow, portal);
                    }
                }).Forget();
            }
        }

        private void TeardownForPortal(IPortal portal)
        {
            if (!_portalShadowDataMap.ContainsKey(portal)) return;

            if (_portalShadowDataMap[portal].Shadow != null)
            {
                DestroyShadowForPortal(_portalShadowDataMap[portal].Shadow, portal);
                _portalShadowDataMap[portal].Shadow = null;
            }

            portal.PortalOpened -= OnPortalConnected;
            portal.PortalClosed -= OnPortalDisconnected;

            _portalShadowDataMap.Remove(portal);
        }

        protected abstract GameObject CreateShadowForPortal(IPortal portal);

        protected virtual void DestroyShadowForPortal(GameObject shadow, IPortal portal)
        {
            Destroy(shadow);
        }
    }
}
