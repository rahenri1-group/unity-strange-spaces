﻿using Cysharp.Threading.Tasks;
using Game.Core.Entity;
using System.Collections.Generic;

namespace Game.Core.Portal
{
    /// <summary>
    /// Delegate for events relating to when an entity interacts with a portal
    /// </summary>
    public delegate void PortalTransporterEntityEvent(IPortalTransporter sender, IDynamicEntity entity);

    /// <summary>
    /// Component can transport <see cref="IDynamicEntity"/>s through a portal
    /// </summary>
    public interface IPortalTransporter : IPortalPathLink
    {
        /// <summary>
        /// If an <see cref="IEntity"/> with <see cref="IEntityMovement.PortalSuckInEnabled"/> is less than this distance from the portal, 
        /// it will be sucked in an transported as if it had crossed through it
        /// </summary>
        float EntitySuckInDistance { get; }

        /// <summary>
        /// All <see cref="IDynamicEntity"/>s that are partially in the portal
        /// </summary>
        IEnumerable<IDynamicEntity> EntitiesInPortalZone { get; }

        /// <summary>
        /// Event raised when an <see cref="IDynamicEntity"/> partially enters the portal
        /// </summary>
        event PortalTransporterEntityEvent EntityEnterPortalZone;
        /// <summary>
        /// Event raised when an <see cref="IDynamicEntity"/> fully exits the portal
        /// </summary>
        event PortalTransporterEntityEvent EntityExitPortalZone;

        /// <summary>
        /// Teleports an <paramref name="entity"/> through the portal
        /// </summary>
        UniTask TeleportEntityInPortalZone(IDynamicEntity entity);
    }
}