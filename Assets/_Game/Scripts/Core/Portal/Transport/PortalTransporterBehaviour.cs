﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Math;
using Game.Core.Physics;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Portal
{
    /// <inheritdoc cref="IPortalTransporter"/>
    public class PortalTransporterBehaviour : InjectedBehaviour, IPortalTransporter
    {
        /// <inheritdoc/>
        public IPortal EntryPortal { get; private set; }
        /// <inheritdoc/>
        public IPortal ExitPortal => EntryPortal.EndPoint;

        /// <inheritdoc/>
        public float EntitySuckInDistance => _entitySuckInDistance;

        /// <inheritdoc/>
        public IEnumerable<IDynamicEntity> EntitiesInPortalZone => _entitiesInPortalZone.Concat(_entitesBeingTeleported);

        /// <inheritdoc/>
        public event PortalTransporterEntityEvent EntityEnterPortalZone;
        /// <inheritdoc/>
        public event PortalTransporterEntityEvent EntityExitPortalZone;

        [SerializeField] [TypeRestriction(typeof(ITriggerVolume<IDynamicEntity>))] private Component _portalTriggerVolumeObj = null;
        private ITriggerVolume<IDynamicEntity> _portalTriggerVolume;

        [Inject] private ILogRouter Logger = null;
        [Inject] private IEventBus EventBus = null;
        [Inject] private ISpaceManager SpaceManager = null;

        private HashSet<IDynamicEntity> _entitiesInPortalZone;
        private HashSet<IDynamicEntity> _entitesBeingTeleported;

        [Tooltip("If an entity's center is this close to the portal but not through it, it will be sucked into the portal")]
        [SerializeField] private FloatReadonlyReference _entitySuckInDistance = null;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _portalTriggerVolume = _portalTriggerVolumeObj.GetComponentAsserted<ITriggerVolume<IDynamicEntity>>();

            EntryPortal = this.GetComponentAsserted<IPortal>();

            _entitiesInPortalZone = new HashSet<IDynamicEntity>();
            _entitesBeingTeleported = new HashSet<IDynamicEntity>();

            _portalTriggerVolume.GameObjectEnter += OnEnterPortalZone;
            _portalTriggerVolume.GameObjectExit += OnExitPortalZone;

            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        /// <inheritdoc/>
        private void OnDestroy()
        {
            _portalTriggerVolume.GameObjectEnter -= OnEnterPortalZone;
            _portalTriggerVolume.GameObjectExit -= OnExitPortalZone;

            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            // entity teleported to this location
            if (EntryPortal.IsOpen && ExitPortal == eventArgs.Portal)
            {
                OnEnterPortalZone(_portalTriggerVolume, eventArgs.Entity);
            }
        }

        private void OnEnterPortalZone(ITriggerVolume<IDynamicEntity> sender, IDynamicEntity entity)
        {
            if (!_entitiesInPortalZone.Contains(entity))
            {
                _entitiesInPortalZone.Add(entity);

                EntityEnterPortalZone?.Invoke(this, entity);
            }
        }

        private void OnExitPortalZone(ITriggerVolume<IDynamicEntity> sender, IDynamicEntity entity)
        {
            if (_entitiesInPortalZone.Contains(entity))
            {
                _entitiesInPortalZone.Remove(entity);

                EntityExitPortalZone?.Invoke(this, entity);
            }
        }

        /// <inheritdoc/>
        private void Update()
        {
            // don't do anything if portal isn't connected yet
            if (!EntryPortal.IsOpen) return;

            if (_entitiesInPortalZone.Count > 0)
            {
                var entitiesToTeleport = new HashSet<IDynamicEntity>();

                foreach (var entity in _entitiesInPortalZone)
                {
                    var entityMovement = entity.EntityMovement;
                    if (entityMovement.CanUsePortals)
                    {
                        // don't trigger until the object has fully crossed into portal
                        if (EntryPortal.PortalPlane.GetSide(entityMovement.Position))
                        {
                            entitiesToTeleport.Add(entity);
                        }
                        else if (entityMovement.PortalSuckInEnabled)
                        {
                            var portalDistance = Mathf.Abs(EntryPortal.PortalPlane.GetDistanceToPoint(entityMovement.Position));
                            if (portalDistance < _entitySuckInDistance)
                            {
                                var moveDistance = portalDistance + 1.1f * _entitySuckInDistance;
                                var moveVector = moveDistance * EntryPortal.PortalPlane.normal;
                                entityMovement.Teleport(entityMovement.Position + moveVector, entityMovement.Rotation);
                                entitiesToTeleport.Add(entity);
                            }
                        }
                    }
                }

                foreach (var entity in entitiesToTeleport)
                {
                    TeleportEntityInPortalZone(entity).Forget();
                }
            }
        }

        /// <inheritdoc/>
        public async UniTask TeleportEntityInPortalZone(IDynamicEntity entity)
        {
            if (!_entitiesInPortalZone.Remove(entity))
            {
                Logger.LogWarning($"Entity {entity.GameObject.name} is not a valid entity to teleport");
                return;
            }

            _entitesBeingTeleported.Add(entity);

            EntryPortal.CalculateEndPointTransform(
                        entity.EntityMovement.Position, entity.EntityMovement.Rotation,
                        out Vector3 destinationPosition, out Quaternion destinationRotation);

            Vector3 destinationVelocity = ExitPortal.Transform.TransformVector(MathUtil.ReverseAxisY * transform.InverseTransformVector(entity.EntityMovement.Velocity));
            Vector3 destinationAnglularVelocity = ExitPortal.Transform.TransformVector(MathUtil.ReverseAxisY * transform.InverseTransformVector(entity.EntityMovement.AngularVelocity));

            var startSpace = EntryPortal.Space;
            var destinationSpace = ExitPortal.Space;

            // may need to push them slightly farther so they don't get sucked back in
            if (entity.EntityMovement.PortalSuckInEnabled) 
            {
                var exitTransporter = ExitPortal.GetComponent<IPortalTransporter>();
                if (exitTransporter != null)
                {
                    var portalDistance = Mathf.Abs(ExitPortal.PortalPlane.GetDistanceToPoint(destinationPosition));
                    if (portalDistance < exitTransporter.EntitySuckInDistance)
                    {
                        var moveDistance = portalDistance + 1.1f * exitTransporter.EntitySuckInDistance;
                        var moveVector = moveDistance * -1f * ExitPortal.PortalPlane.normal;
                        destinationPosition += moveVector;
                    }
                }
            }

            EventBus.InvokeEvent(new EntityPreTeleportEvent
            {
                Entity = entity,
                Portal = EntryPortal,
                OldSpace = startSpace,
                NewSpace = destinationSpace
            });

            entity.EntityMovement.Teleport(destinationPosition, destinationRotation);
            await SpaceManager.MoveObjectToSpaceAsync(entity.GameObject, destinationSpace);

            entity.EntityMovement.Teleport(destinationPosition, destinationRotation);
            entity.EntityMovement.Velocity = destinationVelocity;
            entity.EntityMovement.AngularVelocity = destinationAnglularVelocity;

            _entitesBeingTeleported.Remove(entity);

            // manually need to invoke as trigger will not occur
            EntityExitPortalZone?.Invoke(this, entity);

            EventBus.InvokeEvent(new EntityPostTeleportEvent
            {
                Entity = entity,
                Portal = EntryPortal,
                OldSpace = startSpace,
                NewSpace = destinationSpace
            });
        }
    }
}
