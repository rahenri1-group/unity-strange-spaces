﻿using System;
using System.Linq;
using System.Reflection;

namespace Game.Core.Reflection
{
    /// <summary>
    /// Utility class for reflection discovery 
    /// </summary>
    public static class Discovery
    {
        /// <summary>
        /// Returns all <see cref="Assembly"/>s dependend on <paramref name="assembly"/>
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static Assembly[] FindDependentAssemblies(Assembly assembly)
        {
            var name = assembly.GetName();

            return AppDomain.CurrentDomain
                .GetAssembliesCached()
                .Where(a =>
                   a == assembly
                   || a.GetReferencedAssembliesCached().Any(n => n.Name == name.Name)
                ).ToArray();
        }

        /// <summary>
        /// Finds all class types with attribute <typeparamref name="TAttribute"/> in <paramref name="assembly"/>
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static Type[] FindAttributedTypes<TAttribute>(Assembly assembly)
        {
            return assembly
                .GetTypesCached()
                .Where(t => t.GetCustomAttributesCached().OfType<TAttribute>().Any())
                .ToArray();
        }
    }
}
