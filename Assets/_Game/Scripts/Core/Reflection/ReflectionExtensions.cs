﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace Game.Core.Reflection
{
    /// <summary>
    /// Reflection extensions
    /// </summary>
    public static class ReflectionExtensions
    {
        #region AppDomain

        /// <summary>
        /// Cached versions of <see cref="AppDomain.GetAssemblies"/>
        /// </summary>
        /// <param name="appDomain"></param>
        /// <returns></returns>
        public static Assembly[] GetAssembliesCached(this AppDomain appDomain)
        {
            return DomainAssemblies.GetOrAdd(appDomain, d => d.GetAssemblies());
        }
        private static ConcurrentDictionary<AppDomain, Assembly[]> DomainAssemblies = new ConcurrentDictionary<AppDomain, Assembly[]>();

        #endregion

        #region Assembly

        /// <summary>
        /// Cached version of <see cref="Assembly.GetReferencedAssemblies"/>
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static AssemblyName[] GetReferencedAssembliesCached(this Assembly assembly)
        {
            return ReferencedAssemblies.GetOrAdd(assembly, a => a.GetReferencedAssemblies());
        }
        private static ConcurrentDictionary<Assembly, AssemblyName[]> ReferencedAssemblies = new ConcurrentDictionary<Assembly, AssemblyName[]>();

        /// <summary>
        /// Cached version of <see cref="Assembly.GetTypes"/>
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static Type[] GetTypesCached(this Assembly assembly)
        {
            return AssemblyTypes.GetOrAdd(assembly, a => a.GetTypes());
        }
        private static ConcurrentDictionary<Assembly, Type[]> AssemblyTypes = new ConcurrentDictionary<Assembly, Type[]>();

        #endregion

        #region Type

        /// <summary>
        /// Cached version of <see cref="Type.GetConstructors"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ConstructorInfo[] GetConstructorsCached(this Type type)
        {
            return TypeConstructors.GetOrAdd(type, t => t.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy));
        }
        private static ConcurrentDictionary<Type, ConstructorInfo[]> TypeConstructors = new ConcurrentDictionary<Type, ConstructorInfo[]>();

        /// <summary>
        /// Cached version of <see cref="CustomAttributeExtensions.GetCustomAttributes{T}(MemberInfo)"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Attribute[] GetCustomAttributesCached(this Type type)
        {
            return CustomAttributes.GetOrAdd(type, t => t.GetCustomAttributes().ToArray());
        }
        private static ConcurrentDictionary<Type, Attribute[]> CustomAttributes = new ConcurrentDictionary<Type, Attribute[]>();

        /// <summary>
        /// Cached version of <see cref="Type.GetMembers"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static MemberInfo[] GetMembersCached(this Type type)
        {
            return TypeMembers.GetOrAdd(type, t => t.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy));
        }
        private static ConcurrentDictionary<Type, MemberInfo[]> TypeMembers = new ConcurrentDictionary<Type, MemberInfo[]>();

        /// <summary>
        /// Cached version of <see cref=" Type.GetProperties"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static PropertyInfo[] GetPropertiesCached(this Type type)
        {
            return TypeProperties.GetOrAdd(type, t => t.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy));
        }
        private static ConcurrentDictionary<Type, PropertyInfo[]> TypeProperties = new ConcurrentDictionary<Type, PropertyInfo[]>();

        /// <summary>
        /// Returns the cached <see cref="MethodInfo"/>s of <paramref name="type"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static MethodInfo[] GetMethodsCached(this Type type)
        {
            return GetMembersCached(type).OfType<MethodInfo>().ToArray();
        }

        #endregion

        #region ConstructorInfo

        /// <summary>
        /// Cached version of <see cref="MethodBase.GetParameters"/>
        /// </summary>
        /// <param name="constructor"></param>
        /// <returns></returns>
        public static ParameterInfo[] GetParametersCached(this ConstructorInfo constructor)
        {
            return ConstructorParameters.GetOrAdd(constructor, c => c.GetParameters());
        }
        private static ConcurrentDictionary<ConstructorInfo, ParameterInfo[]> ConstructorParameters = new ConcurrentDictionary<ConstructorInfo, ParameterInfo[]>();

        #endregion

        #region MemberInfo

        /// <summary>
        /// Cached version of <see cref="CustomAttributeExtensions.GetCustomAttribute{Attribute}(MemberInfo)"/>
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static Attribute[] GetCustomAttributesCached(this MemberInfo memberInfo)
        {
            return MemberInfoAttributes.GetOrAdd(memberInfo, m => m.GetCustomAttributes().ToArray());
        }
        private static ConcurrentDictionary<MemberInfo, Attribute[]> MemberInfoAttributes = new ConcurrentDictionary<MemberInfo, Attribute[]>();

        #endregion
    }
}