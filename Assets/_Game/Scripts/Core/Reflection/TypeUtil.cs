﻿using System;
using System.Collections.Concurrent;

namespace Game.Core.Reflection
{
    /// <summary>
    /// Utility class for <see cref="Type"/>
    /// </summary>
    public static class TypeUtil
    {
        /// <summary>
        /// Returns a type that has the provided <paramref name="typeFullName"/>
        /// </summary>
        public static Type GetTypeByName(string typeFullName)
        {
            return GetTypeByName(typeFullName, typeof(object));
        }

        /// <summary>
        /// Returns a type that has the provided <paramref name="typeFullName"/> and is assignable from <paramref name="parentType"/>.
        /// </summary>
        public static Type GetTypeByName(string typeFullName, Type parentType)
        {
            if (TypeNameCache.TryGetValue(typeFullName, out var cachedType))
            {
                if (!cachedType.IsAbstract && parentType.IsAssignableFrom(cachedType))
                {
                    return cachedType;
                }
                else
                {
                    return null;
                }
            }

            foreach (var assembly in AppDomain.CurrentDomain.GetAssembliesCached())
            {
                foreach (var type in assembly.GetTypesCached())
                {
                    if (!type.IsAbstract && parentType.IsAssignableFrom(type) && type.FullName.Equals(typeFullName))
                    {
                        TypeNameCache.TryAdd(typeFullName, type);
                        return type;
                    }
                }
            }

            return null;
        }

        private static ConcurrentDictionary<string, Type> TypeNameCache = new ConcurrentDictionary<string, Type>();
    }
}
