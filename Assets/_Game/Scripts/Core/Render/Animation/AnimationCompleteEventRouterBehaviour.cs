﻿using System;
using UnityEngine;

namespace Game.Core.Render
{
    /// <inheritdoc cref="IAnimationCompleteEventRouter"/>
    public class AnimationCompleteEventRouterBehaviour : MonoBehaviour, IAnimationCompleteEventRouter
    {
        /// <inheritdoc/>
        public event Action AnimationComplete;

        public void OnAnimationComplete()
        {
            AnimationComplete?.Invoke();
        }
    }
}
