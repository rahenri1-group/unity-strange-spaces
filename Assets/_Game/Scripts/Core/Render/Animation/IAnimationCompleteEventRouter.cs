﻿using System;
using UnityEngine;

namespace Game.Core.Render
{
    /// <summary>
    /// Event router for the animation complete AnimationEvent
    /// </summary>
    public interface IAnimationCompleteEventRouter
    {
        /// <summary>
        /// Event for when the current animation for an <see cref="Animator"/> has completed
        /// </summary>
        public event Action AnimationComplete;
    }
}
