﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="ICamera"/>
    public abstract class BaseCamera : InjectedBehaviour, ICamera
    {
        /// <summary>
        /// The logger
        /// </summary>
        [Inject] public ILogRouter Logger;

        /// <inheritdoc/>
        public string Name => gameObject.name;

        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        /// <inheritdoc/>
        public abstract CameraType CameraType { get; }

        /// <inheritdoc/>
        public UnityEngine.Camera UnityCamera { get; protected set; }

        /// <inheritdoc/>
        public virtual bool Enabled
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();
            UnityCamera = this.GetComponentAsserted<UnityEngine.Camera>();

            if (CameraType.HasFlag(CameraType.PrimaryCamera) && !(this is ISpaceCamera))
            {
                Logger.LogError($"Primary Camera created that doesn't implement {nameof(ISpaceCamera)}");
            }
        }

        /// <inheritdoc/>
        public T GetCameraEffect<T>() where T : ICameraEffect 
        {
            return GetComponentInChildren<T>();
        }
    }
}