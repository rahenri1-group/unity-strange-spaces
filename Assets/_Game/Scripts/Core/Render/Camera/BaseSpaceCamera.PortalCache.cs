﻿using Game.Core.Entity;
using Game.Core.Portal;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Render.Camera
{
    public abstract partial class BaseSpaceCamera : BaseCamera, ISpaceCamera
    {
        #region Entity Cache

        private abstract class EntityRendererCache
        {
            public readonly Renderer Renderer;

            private Material[] _cachedMaterials;
            private Material[] _instancedMaterials;

            public EntityRendererCache(Renderer renderer)
            {
                Renderer = renderer;
            }

            public void CacheMaterials()
            {
                _cachedMaterials = Renderer.sharedMaterials;
                _instancedMaterials = new Material[_cachedMaterials.Length];
                for (int i = 0; i < _instancedMaterials.Length; i++)
                {
                    _instancedMaterials[i] = Instantiate(_cachedMaterials[i]);
                }
                Renderer.sharedMaterials = _instancedMaterials;
            }

            public void RevertMaterials()
            {
                Renderer.sharedMaterials = _cachedMaterials;

                for (int i = 0; i < _instancedMaterials.Length; i++)
                {
                    Destroy(_instancedMaterials[i]);
                }

                _instancedMaterials = null;
            }
        }

        private class LocalEntityRendererCache : EntityRendererCache
        {
            public LocalEntityRendererCache(Renderer renderer)
                : base(renderer) { }
        }

        private class RemoteEntityRendererCache : EntityRendererCache
        {
            public readonly ISpaceData CachedSpace;

            public Transform Transform => _skinnedMeshRenderer != null ? _skinnedMeshRenderer.rootBone.parent : Renderer.transform;
            public bool IsParentRenderer { get; private set; }

            private SkinnedMeshRenderer _skinnedMeshRenderer;
            private Vector3 _cachedPosition;
            private Quaternion _cachedRotation;
            private Vector3 _cachedScale;

            public RemoteEntityRendererCache(Renderer renderer, ISpaceData space)
                : base(renderer)
            {
                CachedSpace = space;

                _skinnedMeshRenderer = Renderer as SkinnedMeshRenderer;

                // skinned meshes are always treated as the parent renderer
                IsParentRenderer = _skinnedMeshRenderer != null || renderer.transform.GetComponentInParentExcludeSelf<Renderer>(true) == null;

                CacheTransform();
            }

            public void CacheTransform()
            {
                var rendererTransform = Transform;

                _cachedPosition = rendererTransform.localPosition;
                _cachedRotation = rendererTransform.localRotation;
                _cachedScale = rendererTransform.localScale;
            }

            public void RevertTransform()
            {
                var rendererTransform = Transform;

                rendererTransform.localPosition = _cachedPosition;
                rendererTransform.localRotation = _cachedRotation;
                rendererTransform.localScale = _cachedScale;
            }
        }

        #endregion

        #region Portal Cache

        private abstract class PortalCache
        {
            public readonly IPortalRenderer PortalRenderer;
            public readonly IPortalTransporter PortalTransporter;

            protected readonly int EnablePortalPlaneShaderId;
            protected readonly int PortalPlaneShaderId;

            public PortalCache(IPortalRenderer portalRenderer, IPortalTransporter portalTransporter)
            {
                PortalRenderer = portalRenderer;
                PortalTransporter = portalTransporter;

                EnablePortalPlaneShaderId = Shader.PropertyToID("_Enable_Portal_Plane");
                PortalPlaneShaderId = Shader.PropertyToID("_Portal_Plane");
            }
        }

        private class LocalPortalCache : PortalCache
        {
            private Dictionary<IDynamicEntity, LocalEntityRendererCache[]> _localEntityRenderers;

            public LocalPortalCache(IPortalRenderer portalRenderer, IPortalTransporter portalTransporter)
                : base(portalRenderer, portalTransporter)
            {
                _localEntityRenderers = new Dictionary<IDynamicEntity, LocalEntityRendererCache[]>();
            }

            public void UpdateAndCacheRenderers()
            {
                var entitiesToRemove = _localEntityRenderers.Keys.Except(PortalTransporter.EntitiesInPortalZone).ToArray();
                var entitiesToAdd = PortalTransporter.EntitiesInPortalZone.Except(_localEntityRenderers.Keys).ToArray();

                // remove old cache entries
                foreach (var entity in entitiesToRemove)
                {
                    RemoveEntity(entity);
                }

                // create new cache entries
                foreach (var entity in entitiesToAdd)
                {
                    var renderers = entity.GetComponentsInChildren<Renderer>(true);

                    var rendererCaches = new LocalEntityRendererCache[renderers.Length];
                    for (int i = 0; i < renderers.Length; i++)
                    {
                        rendererCaches[i] = new LocalEntityRendererCache(renderers[i]);
                    }

                    _localEntityRenderers.Add(entity, rendererCaches);
                }

                Plane portalPlane = PortalTransporter.EntryPortal.PortalPlane;
                Vector4 portalPlaneVector = new Vector4(portalPlane.normal.x, portalPlane.normal.y, portalPlane.normal.z, portalPlane.distance);

                // cache renderer material
                if (PortalRenderer.RenderingEnabled)
                {
                    foreach (var entity in _localEntityRenderers.Keys)
                    {
                        foreach (var rendererCache in _localEntityRenderers[entity])
                        {
                            rendererCache.CacheMaterials();

                            foreach (var mat in rendererCache.Renderer.materials)
                            {
                                if (mat.HasProperty(EnablePortalPlaneShaderId))
                                {
                                    mat.SetFloat(EnablePortalPlaneShaderId, 1f);
                                    mat.SetVector(PortalPlaneShaderId, portalPlaneVector);
                                }
                            }
                        }
                    }
                }
            }

            public void RestoreRenderers()
            {
                if (PortalRenderer.RenderingEnabled)
                {
                    // restore cached renderer material
                    foreach (var entity in _localEntityRenderers.Keys)
                    {
                        foreach (var rendererCache in _localEntityRenderers[entity])
                        {
                            rendererCache.RevertMaterials();
                        }
                    }
                }
            }

            public void RemoveEntity(IDynamicEntity entity)
            {
                _localEntityRenderers.Remove(entity);
            }
        }

        private class RemotePortalCache : PortalCache
        {
            private readonly ISpaceManager _spaceManager;
            private readonly ISpaceRenderer _spaceRenderer;

            private Dictionary<IDynamicEntity, RemoteEntityRendererCache[]> _remoteEntityRenderers;

            public RemotePortalCache(ISpaceManager spaceManager, ISpaceRenderer spaceRenderer, IPortalRenderer portalRenderer, IPortalTransporter portalTransporter)
                : base(portalRenderer, portalTransporter)
            {
                _spaceManager = spaceManager;
                _spaceRenderer = spaceRenderer;

                _remoteEntityRenderers = new Dictionary<IDynamicEntity, RemoteEntityRendererCache[]>();
            }

            public void UpdateAndCacheRenderers(ISpaceData renderedSpace)
            {
                var entitiesToRemove = _remoteEntityRenderers.Keys.Except(PortalTransporter.EntitiesInPortalZone).ToArray();
                var entitiesToAdd = PortalTransporter.EntitiesInPortalZone.Except(_remoteEntityRenderers.Keys).ToArray();

                // remove old cache entries
                foreach (var entity in entitiesToRemove)
                {
                    RemoveEntity(entity);
                }

                // create new cache entries
                foreach (var entity in entitiesToAdd)
                {
                    var renderers = entity.GetComponentsInChildren<Renderer>(true);
                    var entitySpace = _spaceManager.GetEntitySpace(entity);

                    var rendererCaches = new RemoteEntityRendererCache[renderers.Length];
                    for (int i = 0; i < renderers.Length; i++)
                    {
                        rendererCaches[i] = new RemoteEntityRendererCache(renderers[i], entitySpace);
                    }

                    _remoteEntityRenderers.Add(entity, rendererCaches);
                }

                Plane portalPlane = PortalTransporter.ExitPortal.PortalPlane;
                Vector4 portalPlaneVector = new Vector4(portalPlane.normal.x, portalPlane.normal.y, portalPlane.normal.z, portalPlane.distance);
                // cache renderer positions
                foreach (var entity in _remoteEntityRenderers.Keys)
                {
                    foreach (var rendererCache in _remoteEntityRenderers[entity])
                    {
                        if (PortalRenderer.RenderingEnabled)
                        {
                            rendererCache.CacheMaterials();

                            foreach (var mat in rendererCache.Renderer.materials)
                            {
                                if (mat.HasProperty(EnablePortalPlaneShaderId))
                                {
                                    mat.SetFloat(EnablePortalPlaneShaderId, 1f);
                                    mat.SetVector(PortalPlaneShaderId, portalPlaneVector);
                                }
                            }
                        }   

                        _spaceRenderer.ConfigureRendererForSpace(rendererCache.Renderer, renderedSpace);

                        // only move the topmost render components
                        if (rendererCache.IsParentRenderer)
                        {
                            rendererCache.CacheTransform();

                            var rendererTransform = rendererCache.Transform;

                            PortalTransporter.EntryPortal.CalculateEndPointTransform(
                                rendererTransform.position, rendererTransform.rotation,
                                out Vector3 destinationPosition, out Quaternion destinationRotation);

                            rendererTransform.position = destinationPosition;
                            rendererTransform.rotation = destinationRotation;
                        }
                    }
                }
            }

            public void RestoreRenderers()
            {
                // restore cached renderer positions and material
                foreach (var entity in _remoteEntityRenderers.Keys)
                {
                    foreach (var rendererCache in _remoteEntityRenderers[entity])
                    {
                        _spaceRenderer.ConfigureRendererForSpace(rendererCache.Renderer, rendererCache.CachedSpace);

                        if (PortalRenderer.RenderingEnabled)
                        {
                            rendererCache.RevertMaterials();
                        }

                        if (rendererCache.IsParentRenderer)
                        {
                            rendererCache.RevertTransform();
                        }
                    }
                }
            }

            public void RemoveEntity(IDynamicEntity entity)
            {
                _remoteEntityRenderers.Remove(entity);
            }
        }

        #endregion

        private void UpdateAndCacheLocalPortalEntities()
        {
            var portals = DynamicPortalManager.AllOpenDynamicPortalsFromSpace(RenderedSpace)
               .Concat<IPortal>(StaticPortalManager.AllOpenStaticPortalsFromSpace(RenderedSpace));

            var portalsToRemove = _localPortalCache.Keys.Except(portals).ToArray();
            var portalToAdd = portals.Except(_localPortalCache.Keys).ToArray();

            // remove old cache entries
            foreach (var portal in portalsToRemove)
            {
                _localPortalCache.Remove(portal);
            }

            // create new cache entries
            foreach (var portal in portalToAdd)
            {
                _localPortalCache.Add(portal, new LocalPortalCache(portal.GetComponent<IPortalRenderer>(), portal.GetComponent<IPortalTransporter>()));
            }

            foreach (var portal in _localPortalCache.Keys)
            {
                var cache = _localPortalCache[portal];

                if (cache.PortalTransporter != null)
                {
                    cache.UpdateAndCacheRenderers();
                }
            }
        }

        private void UpdateAndCacheRemotePortalEntities()
        {
            var portals = DynamicPortalManager.AllOpenDynamicPortalsToSpace(RenderedSpace)
                .Concat<IPortal>(StaticPortalManager.AllOpenStaticPortalsToSpace(RenderedSpace));

            var portalsToRemove = _remotePortalCache.Keys.Except(portals).ToArray();
            var portalToAdd = portals.Except(_remotePortalCache.Keys).ToArray();

            // remove old cache entries
            foreach (var portal in portalsToRemove)
            {
                _remotePortalCache.Remove(portal);
            }

            // create new cache entries
            foreach (var portal in portalToAdd)
            {
                _remotePortalCache.Add(portal, new RemotePortalCache(SpaceManager, SpaceRenderer, portal.GetComponent<IPortalRenderer>(), portal.GetComponent<IPortalTransporter>()));
            }

            foreach (var portal in _remotePortalCache.Keys)
            {
                var cache = _remotePortalCache[portal];

                if (cache.PortalTransporter != null)
                {
                    cache.UpdateAndCacheRenderers(RenderedSpace);
                }
            }
        }

        private void RevertCachedEntityValues()
        {
            foreach (var portal in _localPortalCache.Keys)
            {
                _localPortalCache[portal].RestoreRenderers();
            }

            foreach (var portal in _remotePortalCache.Keys)
            {
                _remotePortalCache[portal].RestoreRenderers();
            }
        }
    }
}