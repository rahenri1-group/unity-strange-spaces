﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using UnityEngine.Rendering;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="ISpaceCamera"/>
    public abstract partial class BaseSpaceCamera : BaseCamera, ISpaceCamera
    {
        /// <inheritdoc/>
        public event Action<ISpaceCamera> RenderedSpaceChanged;

        /// <inheritdoc/>
        public ISpaceData RenderedSpace
        {
            get => _rendereredSpace;
            set
            {
                _rendereredSpace = value;
                SpaceRenderer.ConfigureCameraForSpace(this, _rendereredSpace);
                RenderedSpaceChanged?.Invoke(this);
            }
        }
        private ISpaceData _rendereredSpace = null;

        /// <inheritdoc/>
        public ISpaceRenderSettings RenderSettings { get; set; }

        [Inject] protected IDynamicPortalManager DynamicPortalManager;
        [Inject] protected IEventBus EventBus;
        [Inject] protected IStaticPortalManager StaticPortalManager;
        [Inject] protected ISpaceManager SpaceManager;
        [Inject] protected ISpaceRenderer SpaceRenderer;

        private ISpaceRenderSettings _stashedRenderSettings;

        private Dictionary<IPortal, LocalPortalCache> _localPortalCache;
        private Dictionary<IPortal, RemotePortalCache> _remotePortalCache;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _localPortalCache = new Dictionary<IPortal, LocalPortalCache>();
            _remotePortalCache = new Dictionary<IPortal, RemotePortalCache>();

            EventBus.Subscribe<EntityModifiedEvent>(OnEntityModified);

            RenderPipelineManager.beginCameraRendering += OnBeginCameraRenderingInternal;
            RenderPipelineManager.endCameraRendering += OnEndCameraRenderingInternal;
        }

        /// <inheritdoc/>
        protected virtual void OnDestroy()
        {
            EventBus.Unsubscribe<EntityModifiedEvent>(OnEntityModified);

            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRenderingInternal;
            RenderPipelineManager.endCameraRendering -= OnEndCameraRenderingInternal;
        }

        private void OnEntityModified(EntityModifiedEvent eventArgs)
        {
            var dynamicEntity = eventArgs.Entity as IDynamicEntity;

            if (dynamicEntity != null)
            {
                // Remove entities from cache. Cache will be rebuilt at next render

                foreach (var pair in _localPortalCache)
                {
                    var localPortalCache = pair.Value;
                    localPortalCache.RemoveEntity(dynamicEntity);
                }

                foreach (var pair in _remotePortalCache)
                {
                    var remotePortalCache = pair.Value;

                    remotePortalCache.RemoveEntity(dynamicEntity);
                }
            }
        }

        private void OnBeginCameraRenderingInternal(ScriptableRenderContext context, UnityEngine.Camera cam)
        {
            if (cam != UnityCamera || !Enabled || RenderedSpace == null) return;

            OnBeginCameraRendering(context);
        }

        private void OnEndCameraRenderingInternal(ScriptableRenderContext context, UnityEngine.Camera cam)
        {
            if (cam != UnityCamera || !Enabled || RenderedSpace == null) return;

            OnEndCameraRendering(context);
        }

        protected virtual void OnBeginCameraRendering(ScriptableRenderContext context)
        {
            _stashedRenderSettings = SpaceRenderer.CurrentRenderSettings;
            SpaceRenderer.CurrentRenderSettings = RenderSettings;

            UpdateAndCacheLocalPortalEntities();
            UpdateAndCacheRemotePortalEntities();
        }

        protected virtual void OnEndCameraRendering(ScriptableRenderContext context)
        {
            SpaceRenderer.CurrentRenderSettings = _stashedRenderSettings;

            RevertCachedEntityValues();
        }
    }
}
