﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="ICameraFade"/>
    [RequireComponent(typeof(Renderer))]
    public class CameraFadeRendererBehaviour : MonoBehaviour, ICameraFade, ICameraEffect
    {
        private static int _lerpID = -1;

        /// <inheritdoc/>
        public bool IsFading => _fadeCoroutine != null;

        /// <inheritdoc/>
        public float FadeLerp
        {
            get => _fadeRenderer.material.GetFloat(_lerpID);
            private set
            {
                value = Mathf.Clamp01(value);
                _fadeRenderer.enabled = value > 0f;
                _fadeRenderer.material.SetFloat(_lerpID, value);
            }
        }

        private Renderer _fadeRenderer;

        private IEnumerator _fadeCoroutine;
        private UniTaskCompletionSource _fadeCompletionSource;

        /// <inheritdoc/>
        private void Awake()
        {
            if (_lerpID < 0) _lerpID = Shader.PropertyToID("_Lerp");

            _fadeRenderer = GetComponent<Renderer>();

            FadeLerp = 0f;
            _fadeCoroutine = null;
            _fadeCompletionSource = null;
        }

        /// <inheritdoc/>
        public void SetBlack()
        {
            StopCurrentFade();

            FadeLerp = 1f;
        }

        /// <inheritdoc/>
        public void FadeIn(float duration)
        {
            StopCurrentFade();

            _fadeCoroutine = FadeCoroutine(duration, 0f);
            StartCoroutine(_fadeCoroutine);
        }

        /// <inheritdoc/>
        public UniTask FadeInAsync(float duration)
        {
            StopCurrentFade();

            _fadeCompletionSource = new UniTaskCompletionSource();

            _fadeCoroutine = FadeCoroutine(duration, 0f);
            StartCoroutine(_fadeCoroutine);

            return _fadeCompletionSource.Task;
        }

        /// <inheritdoc/>
        public void FadeOut(float duration)
        {
            StopCurrentFade();

            _fadeCoroutine = FadeCoroutine(duration, 1f);
            StartCoroutine(_fadeCoroutine);
        }

        /// <inheritdoc/>
        public UniTask FadeOutAsync(float duration)
        {
            StopCurrentFade();

            _fadeCompletionSource = new UniTaskCompletionSource();

            _fadeCoroutine = FadeCoroutine(duration, 1f);
            StartCoroutine(_fadeCoroutine);

            return _fadeCompletionSource.Task;
        }

        /// <inheritdoc/>
        public void Blink(float duration, Action blinkAction = null)
        {
            StopCurrentFade();

            _fadeCoroutine = BlinkCoroutine(duration, blinkAction);
            StartCoroutine(_fadeCoroutine);
        }

        private void StopCurrentFade()
        {
            if (_fadeCoroutine != null)
            {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;
            }

            if (_fadeCompletionSource != null)
            {
                _fadeCompletionSource.TrySetResult();
                _fadeCompletionSource = null;
            }
        }

        private IEnumerator FadeCoroutine(float duration, float targetLerp)
        {
            YieldInstruction wait = null;

            float startLerp = FadeLerp;
            float startTime = Time.time;
            while ((Time.time - startTime) <= duration)
            {
                yield return wait;

                FadeLerp = Mathf.SmoothStep(startLerp, targetLerp, (Time.time - startTime) / duration);
            }

            FadeLerp = targetLerp;

            _fadeCoroutine = null;

            if (_fadeCompletionSource != null)
            {
                var completionSource = _fadeCompletionSource;
                _fadeCompletionSource = null;
                completionSource.TrySetResult();
            }
        }

        private IEnumerator BlinkCoroutine(float duration, Action blinkAction)
        {
            YieldInstruction wait = null;

            float halfDuration = 0.5f * duration;

            float startLerp = FadeLerp;
            float startTime = Time.time;
            while ((Time.time - startTime) <= halfDuration)
            {
                yield return wait;

                FadeLerp = Mathf.SmoothStep(startLerp, 1f, (Time.time - startTime) / halfDuration);
            }

            FadeLerp = 1f;

            blinkAction?.Invoke();

            startTime = Time.time;
            while ((Time.time - startTime) <= halfDuration)
            {
                yield return wait;

                FadeLerp = Mathf.SmoothStep(1f, 0f, (Time.time - startTime) / halfDuration);
            }

            FadeLerp = 0f;

            _fadeCoroutine = null;
        }
    }
}
