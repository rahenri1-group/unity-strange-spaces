﻿namespace Game.Core.Render.Camera
{
    /// <summary>
    /// An effect that is applied to a <see cref="ICamera"/>.
    /// </summary>
    public interface ICameraEffect { }
}
