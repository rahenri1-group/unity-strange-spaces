﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// A camera effect to fade the camera in and out.
    /// </summary>
    public interface ICameraFade : ICameraEffect
    {
        /// <summary>
        /// Is the fade effect currently active.
        /// </summary>
        bool IsFading { get; }

        /// <summary>
        /// The current fade status of the camera. 
        /// 0 is no fade, 1 is completely black.
        /// </summary>
        float FadeLerp { get; }

        /// <summary>
        /// Sets the camera fade to black.
        /// </summary>
        void SetBlack();

        /// <summary>
        /// Fades the camera in.
        /// </summary>
        void FadeIn(float duration);

        /// <summary>
        /// Asynchronously fades the camera in.
        /// </summary>
        UniTask FadeInAsync(float duration);

        /// <summary>
        /// Fades the camera out.
        /// </summary>
        void FadeOut(float duration);

        /// <summary>
        /// Asynchronously fades the camera out.
        /// </summary>
        UniTask FadeOutAsync(float duration);

        /// <summary>
        /// Fades the camera to black and then back in.
        /// </summary>
        void Blink(float duration, Action blinkAction = null);
    }
}
