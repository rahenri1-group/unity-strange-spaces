﻿using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// The flags describing a camera
    /// </summary>
    [Flags]
    public enum CameraType
    {
        PrimaryCamera = 1,
        UiCamera = 2,
        EffectCamera = 4
    }
}
