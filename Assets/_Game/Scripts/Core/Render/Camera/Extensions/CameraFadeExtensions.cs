﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Extensions for <see cref="ICameraFade"/>
    /// </summary>
    public static class CameraFadeExtensions
    {
        /// <summary>
        /// Calls <see cref="ICameraFade.Blink(float, System.Action)"/> as a task
        /// </summary>
        public static UniTask BlinkAsync(this ICameraFade cameraFade, float duration)
        {
            var utcs = new UniTaskCompletionSource();

            cameraFade.Blink(duration, () =>
            {
                utcs.TrySetResult();
            });

            return utcs.Task;
        }
    }
}
