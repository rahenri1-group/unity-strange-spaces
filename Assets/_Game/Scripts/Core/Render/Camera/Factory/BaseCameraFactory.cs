﻿using Cysharp.Threading.Tasks;
using Game.Core.Resource;
using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="ICamera"/>
    public abstract class BaseCameraFactory<T> : ICameraFactory where T : ICamera
    {
        /// <inheritdoc />
        public Type CameraType => typeof(T);

        public abstract CameraFactoryConfig FactoryConfig { get; }

        protected readonly IGameObjectResourceManager GameObjectResourceManager;
        protected readonly ISpaceManager SpaceManager;

        private string _cameraNamePrefix;
        private int _cameraCount;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public BaseCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ISpaceManager spaceManager)
        {
            GameObjectResourceManager = gameObjectResourceManager;
            SpaceManager = spaceManager;

            _cameraCount = 0;
            _cameraNamePrefix = $"@{CameraType.Name}";
        }

        /// <inheritdoc />
        public virtual UniTask InitializeCameraFactory()
        {
            foreach (var key in FactoryConfig.CameraChildAssetKeys)
            {
                Assert.IsTrue(GameObjectResourceManager.IsAssetKeyValid(key));
            }

            return UniTask.CompletedTask;
        }

        /// <inheritdoc />
        public virtual UniTask ShutdownCameraFactory()
        {
            return UniTask.CompletedTask;
        }


        /// <inheritdoc />
        protected async UniTask<UnityEngine.Camera> CreateEmptyCamera()
        {
            _cameraCount += 1;

            var cameraObject = new GameObject($"{_cameraNamePrefix}_{_cameraCount}");
            var camera = cameraObject.AddComponent<UnityEngine.Camera>();

            camera.cullingMask = LayerUtil.ConstructMaskForLayers(FactoryConfig.CameraLayers);
            camera.nearClipPlane = FactoryConfig.NearClipPlane;
            camera.farClipPlane = FactoryConfig.FarClipPlane;

            cameraObject.SetActive(false);
            await SpaceManager.MoveObjectToSpaceAsync(cameraObject, null);

            foreach (var assetKey in FactoryConfig.CameraChildAssetKeys)
            {
                var child = await GameObjectResourceManager.InstantiateAsync(assetKey, null, Vector3.zero);
                child.transform.SetParent(camera.transform);
                child.transform.localPosition = Vector3.zero;
                child.transform.localRotation = Quaternion.identity;
                child.transform.localScale = Vector3.one;
            }

            cameraObject.SetActive(true);

            return camera;
        }

        /// <inheritdoc />
        public abstract UniTask<ICamera> CreateCamera();
    }
}
