﻿using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Config for <see cref="BaseCameraFactory{T}"/>
    /// </summary>
    [Serializable]
    public class CameraFactoryConfig
    {
        /// <summary>
        /// The default near clip plane for the camera
        /// </summary>
        public float NearClipPlane = 0.3f;
        /// <summary>
        /// The default far clip plane for the camera
        /// </summary>
        public float FarClipPlane = 1000f;

        /// <summary>
        /// The layers the camera will render
        /// </summary>
        public string[] CameraLayers = new string[0];

        /// <summary>
        /// Child object to be spawned on the camera
        /// </summary>
        public string[] CameraChildAssetKeys = new string[0];
    }
}