﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.Space;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Camera factory for <see cref="IFirstPersonCamera"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICameraFactory),
        lifetime: Lifetime.Singleton)]
    public class FirstPersonCameraFactory : BaseCameraFactory<IFirstPersonCamera>
    {
        public override CameraFactoryConfig FactoryConfig => Config;
        public CameraFactoryConfig Config = new CameraFactoryConfig();

        /// <inheritdoc />
        public FirstPersonCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ISpaceManager spaceManager)
            : base(gameObjectResourceManager, spaceManager) { }

        /// <inheritdoc />
        public async override UniTask<ICamera> CreateCamera()
        {
            var camera = await CreateEmptyCamera();
            return camera.gameObject.AddComponent<FirstPersonCameraBehaviour>();
        }
    }
}