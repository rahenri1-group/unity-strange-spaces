﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Factory that creates a single type of camera
    /// </summary>
    public interface ICameraFactory
    {
        /// <summary>
        /// The camera type the factory will create
        /// </summary>
        Type CameraType { get; }

        /// <summary>
        /// Sets up the camera factory
        /// </summary>
        /// <returns></returns>
        UniTask InitializeCameraFactory();

        /// <summary>
        /// Shuts down the camera factory
        /// </summary>
        /// <returns></returns>
        UniTask ShutdownCameraFactory();


        /// <summary>
        /// Creates a new camera. The Type of this camera will be <see cref="CameraType"/>
        /// </summary>
        /// <returns></returns>
        UniTask<ICamera> CreateCamera();
    }
}