﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.Space;
using UnityEngine.Rendering.Universal;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Camera factory for <see cref="IUiCamera"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICameraFactory),
        lifetime: Lifetime.Singleton)]
    public class UiCameraFactory : BaseCameraFactory<IUiCamera>
    {
        public override CameraFactoryConfig FactoryConfig => Config;
        public CameraFactoryConfig Config = new CameraFactoryConfig();

        /// <inheritdoc />
        public UiCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ISpaceManager spaceManager)
            : base(gameObjectResourceManager, spaceManager) { }

        /// <inheritdoc />
        public override async UniTask<ICamera> CreateCamera()
        {
            var camera = await CreateEmptyCamera();
            var uiCamera =  camera.gameObject.AddComponent<UiCameraBehaviour>();

            uiCamera.UnityCamera.GetUniversalAdditionalCameraData().renderType = CameraRenderType.Overlay;

            return uiCamera;
        }
    }
}
