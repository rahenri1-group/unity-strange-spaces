﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="IFirstPersonCamera"/>
    public class FirstPersonCameraBehaviour : BaseSpaceCamera, IFirstPersonCamera
    {
        /// <inheritdoc/>
        public override CameraType CameraType => CameraType.PrimaryCamera;

        /// <inheritdoc/>
        public Transform ViewTransform { get; set; }

        /// <inheritdoc/>
        private void LateUpdate()
        {
            if (ViewTransform == null) return;

            transform.position = ViewTransform.position;
            transform.rotation = ViewTransform.rotation;

            var currentViewTransformSpace = SpaceManager.GetObjectSpace(ViewTransform.gameObject);
            if (!SpaceUtil.SpaceEquals(RenderedSpace, currentViewTransformSpace))
            {
                RenderedSpace = currentViewTransformSpace;
            }
        }
    }
}
