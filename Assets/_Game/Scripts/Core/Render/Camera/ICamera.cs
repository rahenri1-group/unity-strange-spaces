﻿namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Game object component that displays renderered items
    /// </summary>
    public interface ICamera : IGameObjectComponent
    {
        /// <summary>
        /// The name of the camera
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The type of the camera
        /// </summary>
        CameraType CameraType { get; }

        /// <summary>
        /// Is the camera enabled
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// The <see cref="UnityEngine.Camera"/> the camera uses
        /// </summary>
        UnityEngine.Camera UnityCamera { get; }

        /// <summary>
        /// Gets the <see cref="ICameraEffect"/> of <typeparamref name="T"/>. 
        /// Returns null if the effect is not attached to the camera.
        /// </summary>
        T GetCameraEffect<T>() where T : ICameraEffect;
    }
}
