﻿using UnityEngine;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// A <see cref="ICamera"/> that moves to match a transform
    /// </summary>
    public interface IFirstPersonCamera : ICamera
    {
        /// <summary>
        /// The transform the camera is following
        /// </summary>
        Transform ViewTransform { get; set; }
    }
}