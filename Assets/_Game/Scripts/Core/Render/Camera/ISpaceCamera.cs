﻿using Game.Core.Space;
using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// A <see cref="ICamera"/> that only renders a single space
    /// </summary>
    public interface ISpaceCamera : ICamera
    {
        /// <summary>
        /// Raised when <see cref="RenderedSpace changes"/>
        /// </summary>
        event Action<ISpaceCamera> RenderedSpaceChanged;

        /// <summary>
        /// The space the camera is rendering
        /// </summary>
        ISpaceData RenderedSpace { get; }

        /// <summary>
        /// Assigned from <see cref="ISpaceRenderer"/> to be used when renderering
        /// </summary>
        ISpaceRenderSettings RenderSettings { get; set; }
    }
}