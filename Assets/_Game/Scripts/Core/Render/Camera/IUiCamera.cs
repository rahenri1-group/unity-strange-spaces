﻿namespace Game.Core.Render.Camera
{
    /// <summary>
    /// A <see cref="ICamera"/> that is used for rendering UI elements
    /// </summary>
    public interface IUiCamera : ICamera { }
}
