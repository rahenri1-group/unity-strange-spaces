﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="ICameraManager"/>
    [Dependency(
        contract: typeof(ICameraManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class CameraManager : BaseModule, ICameraManager
    {
        /// <inheritdoc />
        public override string ModuleName => "Camera Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public CameraManagerConfig Config = new CameraManagerConfig();

        /// <inheritdoc />
        public ISpaceCamera ActivePrimaryCamera { get; private set; }
        /// <inheritdoc />
        public IUiCamera ActiveUiCamera { get; private set; }

        private readonly ICameraFactory[] _cameraFactories;
        private readonly IEventBus _eventBus;

        private Dictionary<Type, ICameraFactory> _cameraFactoryMap;

        private Dictionary<ICamera, GameObject> _activeCameras;
        private Dictionary<UnityEngine.Camera, ICamera> _unityCameraMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public CameraManager(
            ICameraFactory[] cameraFactories,
            IEventBus eventBus,
            ILogRouter logger)
            : base(logger)
        {
            _cameraFactories = cameraFactories;
            _eventBus = eventBus;

            _cameraFactoryMap = new Dictionary<Type, ICameraFactory>();
            _activeCameras = new Dictionary<ICamera, GameObject>();
            _unityCameraMap = new Dictionary<UnityEngine.Camera, ICamera>();
        }

        /// <inheritdoc />
        public override async UniTask Initialize()
        {
            foreach (var factory in _cameraFactories)
            {
                _cameraFactoryMap[factory.CameraType] = factory;

                await factory.InitializeCameraFactory();
            }

            foreach (var cameraType in Config.InitialCameras)
            {
                await CreateCamera(cameraType);
            }

            await base.Initialize();
        }

        /// <inheritdoc />
        public override async UniTask Shutdown()
        {
            while (_activeCameras.Count > 0)
            {
                DestroyCamera(_activeCameras.First().Key);
            }
            _activeCameras.Clear();
            _unityCameraMap.Clear();

            foreach (var pair in _cameraFactoryMap)
            {
                var factory = pair.Value;
                await factory.ShutdownCameraFactory();
            }
            _cameraFactoryMap.Clear();

            await base.Shutdown();
        }

        /// <inheritdoc />
        public async UniTask<T> CreateCamera<T>() where T : class, ICamera
        {
            var cameraType = typeof(T);

            return (T) await CreateCamera(cameraType);
        }

        /// <inheritdoc />
        public async UniTask<ICamera> CreateCamera(Type cameraType)
        {
            if (!_cameraFactoryMap.ContainsKey(cameraType))
            {
                ModuleLogWarning($"Requested camera type '{cameraType.Name}' has no factory");
                return null;
            }

            var camera = await _cameraFactoryMap[cameraType].CreateCamera();

            OnActivateCamera(camera);

            return camera;
        }

        /// <inheritdoc />
        public ICamera GetCameraFromUnity(UnityEngine.Camera camera)
        {
            if (_unityCameraMap.ContainsKey(camera))
            {
                return _unityCameraMap[camera];
            }

            return null;
        }

        /// <inheritdoc />
        public void DestroyCamera(ICamera camera)
        {
            if (!_activeCameras.ContainsKey(camera)) return;

            var cameraObject = _activeCameras[camera];

            OnDeactivateCamera(camera);

            _activeCameras.Remove(camera);
            _unityCameraMap.Remove(camera.UnityCamera);

            if (cameraObject)
            {
                UnityEngine.Object.Destroy(cameraObject);
            }
        }

        private void OnActivateCamera(ICamera camera)
        {
            if (_activeCameras.ContainsKey(camera)) return;

            _activeCameras.Add(camera, camera.UnityCamera.gameObject);
            _unityCameraMap.Add(camera.UnityCamera, camera);

            bool addedPrimaryOrUiCamera = false; 

            if (camera.CameraType.HasFlag(CameraType.PrimaryCamera))
            {
                if (ActivePrimaryCamera == null)
                {
                    ActivePrimaryCamera = (ISpaceCamera)camera;

                    addedPrimaryOrUiCamera = true;
                }
                else
                {
                    ModuleLogError($"Invalid attempt to activate primary camera {camera.Name} when {ActivePrimaryCamera.Name} is active");
                    return;
                }
            }

            if (camera.CameraType.HasFlag(CameraType.UiCamera))
            {
                if (ActiveUiCamera == null)
                {
                    ActiveUiCamera = (IUiCamera)camera;

                    addedPrimaryOrUiCamera = true;
                }
            }

            if (addedPrimaryOrUiCamera && ActivePrimaryCamera != null && ActiveUiCamera != null && ActivePrimaryCamera != ActiveUiCamera)
            {
                ActivePrimaryCamera.UnityCamera.GetUniversalAdditionalCameraData().cameraStack.Add(ActiveUiCamera.UnityCamera);
            }

            _eventBus.InvokeEvent(new CameraActivateEvent
            {
                Camera = camera
            });
        }

        private void OnDeactivateCamera(ICamera camera)
        {
            if (ActivePrimaryCamera == camera)
            {
                ActivePrimaryCamera = null;
            }

            if (ActiveUiCamera == camera)
            {
                ActiveUiCamera = null;
            }

            _eventBus.InvokeEvent(new CameraDeactivateEvent
            {
                Camera = camera
            });
        }
    }
}