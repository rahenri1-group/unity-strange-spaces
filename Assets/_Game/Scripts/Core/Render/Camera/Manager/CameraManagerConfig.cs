﻿using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Config for <see cref="CameraManager"/>
    /// </summary>
    public class CameraManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The cameras that should be created as a part of <see cref="IModule.Initialize"/>
        /// </summary>
        public Type[] InitialCameras = new Type[0];
    }
}
