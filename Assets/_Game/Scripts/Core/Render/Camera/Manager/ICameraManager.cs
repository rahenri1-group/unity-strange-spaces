﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Render.Camera
{
    /// <summary>
    /// Module controlling the creation, destruction, and management of all <see cref="ICamera"/>s
    /// </summary>
    public interface ICameraManager : IModule
    {
        /// <summary>
        /// The active primary camera. This is the camera rendering to the screen
        /// </summary>
        ISpaceCamera ActivePrimaryCamera { get; }
        /// <summary>
        /// The active UI camera
        /// </summary>
        IUiCamera ActiveUiCamera { get; }

        /// <summary>
        /// Creates a new instance of the requested camera
        /// </summary>
        UniTask<T> CreateCamera<T>() where T : class, ICamera;

        /// <summary>
        /// Creates a new instance of the requested camera
        /// </summary>
        UniTask<ICamera> CreateCamera(Type cameraType);

        /// <summary>
        /// Gets the <see cref="ICamera"/> associated with the provided unity camera.
        /// Returns null if the manager didn't create the camera.
        /// </summary>
        ICamera GetCameraFromUnity(UnityEngine.Camera camera);

        /// <summary>
        /// Destroys the provided camera
        /// </summary>
        void DestroyCamera(ICamera camera);
    }
}