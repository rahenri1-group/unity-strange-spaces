﻿namespace Game.Core.Render.Camera
{
    /// <inheritdoc/>
    public class SimpleCameraBehaviour : BaseCamera
    {
        /// <inheritdoc/>
        public override CameraType CameraType => CameraType.PrimaryCamera;
    }
}