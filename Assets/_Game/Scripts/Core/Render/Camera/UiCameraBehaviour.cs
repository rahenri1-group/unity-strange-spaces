﻿namespace Game.Core.Render.Camera
{
    /// <inheritdoc cref="IUiCamera"/>
    public class UiCameraBehaviour : BaseCamera, IUiCamera
    {
        /// <inheritdoc/>
        public override CameraType CameraType => CameraType.UiCamera;
    }
}
