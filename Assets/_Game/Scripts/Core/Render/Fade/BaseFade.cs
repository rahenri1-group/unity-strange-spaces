﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Render
{
    /// <summary>
    /// Base class for behaviours that fade in and out
    /// </summary>
    public abstract class BaseFade : MonoBehaviour
    {
        private class RendererData
        {
            public Renderer Renderer { get; set; }
            public Material[] InstancedMaterials { get; set; }
            public Material[] SharedMaterials { get; set; }
        }

        private List<RendererData> _rendererDatas;

        protected virtual void Awake()
        {
            _rendererDatas = new List<RendererData>();

            var renderers = GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
            {
                var rendererData = new RendererData();
                rendererData.Renderer = renderer;
                rendererData.SharedMaterials = renderer.sharedMaterials;
                rendererData.InstancedMaterials = renderer.materials;

                _rendererDatas.Add(rendererData);
            }
        }

        private void OnDestroy()
        {
            foreach (var rendererData in _rendererDatas)
            {
                var renderer = rendererData.Renderer;

                if (renderer) // don't restore destroyed renderers
                {
                    renderer.sharedMaterials = rendererData.SharedMaterials;
                    renderer.materials = rendererData.SharedMaterials;
                }

                for (int i = 0; i < rendererData.InstancedMaterials.Length; i++)
                {
                    Destroy(rendererData.InstancedMaterials[i]);
                }
            }

            _rendererDatas.Clear();
        }

        protected void SetAlpha(float alpha)
        {
            foreach (var rendererData in _rendererDatas)
            {
                var renderer = rendererData.Renderer;
                if (renderer) // don't update renderers that have been destroyed
                {
                    for (int i = 0; i < rendererData.InstancedMaterials.Length; i++)
                    {
                        var mat = rendererData.InstancedMaterials[i];
                        var sharedMat = rendererData.SharedMaterials[i];

                        var color = sharedMat.color;
                        color.a = Mathf.Lerp(0f, color.a, alpha);
                        mat.color = color;

                        renderer.materials[i] = mat;

                        renderer.enabled = alpha > 0f;
                    }
                }
            }
        }
    }
}
