﻿using System.Collections;
using UnityEngine;

namespace Game.Core.Render
{
    /// <summary>
    /// A behaviour that fades in all renderers and then destroys itself
    /// </summary>
    public class FadeInBehaviour : BaseFade
    {
        public float FadeDuration = 1f;

        private IEnumerator _fadeCoroutine;

        protected override void Awake()
        {
            base.Awake();

            SetAlpha(0f);
        }

        private void OnEnable()
        {
            _fadeCoroutine = FadeIn();
            StartCoroutine(_fadeCoroutine);
        }

        private void OnDisable()
        {
            if (_fadeCoroutine != null)
            {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;
            }
        }

        private IEnumerator FadeIn()
        {
            YieldInstruction wait = null;

            float startTime = Time.time;

            while ((Time.time - startTime) <= FadeDuration)
            {
                yield return wait;

                float alpha = Mathf.SmoothStep(0f, 1f, (Time.time - startTime) / FadeDuration);
                SetAlpha(alpha);
            }

            SetAlpha(1f);

            Destroy(this);

            _fadeCoroutine = null;
        }
    }
}
