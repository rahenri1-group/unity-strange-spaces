﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Core.Render
{
    /// <summary>
    /// A behaviour that fades out all renderers and then destroys itself
    /// </summary>
    public class FadeOutBehaviour : BaseFade
    {
        public float FadeDuration = 1f;

        public event Action FadeComplete;

        public UniTask FadeCompleteTask => _fadeCompletedSource.Task;

        private UniTaskCompletionSource _fadeCompletedSource = new UniTaskCompletionSource();

        private IEnumerator _fadeCoroutine;

        protected override void Awake()
        {
            base.Awake();

            SetAlpha(1f);
        }

        private void OnEnable()
        {
            _fadeCoroutine = FadeOut();
            StartCoroutine(_fadeCoroutine);
        }

        private void OnDisable()
        {
            if (_fadeCoroutine != null)
            {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;

                _fadeCompletedSource.TrySetResult();
            }
        }

        private IEnumerator FadeOut()
        {
            YieldInstruction wait = null;

            float startTime = Time.time;

            while ((Time.time - startTime) <= FadeDuration)
            {
                yield return wait;

                float alpha = Mathf.SmoothStep(1f, 0f, (Time.time - startTime) / FadeDuration);
                SetAlpha(alpha);
            }

            SetAlpha(0f);

            Destroy(this);

            _fadeCoroutine = null;

            FadeComplete?.Invoke();
            _fadeCompletedSource.TrySetResult();
        }
    }
}
