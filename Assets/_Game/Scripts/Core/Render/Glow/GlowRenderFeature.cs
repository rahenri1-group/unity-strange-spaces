﻿using Game.Core.DependencyInjection;
using Game.Core.Portal;
using Game.Core.Render.Camera;
using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Game.Core.Render
{
    public class GlowRenderFeature : ScriptableRendererFeature
    {
        private class GlowPass : ScriptableRenderPass
        {
            private ICameraManager CameraManager => _cameraManager.Value;
            private Lazy<ICameraManager> _cameraManager = new Lazy<ICameraManager>(() => ModuleContext.DiContainer.Resolve<ICameraManager>());

            private IGlowManager GlowManager => _glowManager.Value;
            private Lazy<IGlowManager> _glowManager = new Lazy<IGlowManager>(() => ModuleContext.DiContainer.Resolve<IGlowManager>());

            private readonly int _enablePortalPlaneShaderId;
            private readonly int _portalPlaneShaderId;

            private Vector3 _scaleFudge;

            public GlowPass()
                : base()
            {
                _scaleFudge = new Vector3(0.001f, 0.001f, 0.001f);

                _enablePortalPlaneShaderId = Shader.PropertyToID("_Enable_Portal_Plane");
                _portalPlaneShaderId = Shader.PropertyToID("_Portal_Plane");
            }

            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                var spaceCamera = CameraManager.GetCameraFromUnity(renderingData.cameraData.camera) as ISpaceCamera;
                if (spaceCamera == null) return;

                CommandBuffer command = CommandBufferPool.Get(name: "GlowPass");

                foreach (var glowable in GlowManager.AllGlowablesForSpace(spaceCamera.RenderedSpace))
                {
                    AddGlowableToCommand(glowable.Glowable, spaceCamera.RenderedSpace, glowable.Portal, ref command);
                }

                context.ExecuteCommandBuffer(command);
                CommandBufferPool.Release(command);
            }

            private void AddGlowableToCommand(IGlowable glowable, ISpaceData space, IPortal portal, ref CommandBuffer command)
            {
                if (!glowable.GlowEnabled) return;

                foreach (var meshFilter in glowable.Meshes)
                {
                    if (meshFilter && meshFilter.gameObject.activeInHierarchy)
                    {
                        var matrix = Matrix4x4.TRS(
                            meshFilter.transform.position,
                            meshFilter.transform.rotation,
                            meshFilter.transform.lossyScale + _scaleFudge);

                        var material = glowable.GetInstancedGlowMaterialForSpace(space);

                        if (portal != null && material.HasProperty(_enablePortalPlaneShaderId))
                        {
                            var portalRenderer = portal.GetComponent<IPortalRenderer>();
                            if (portalRenderer != null && portalRenderer.RenderingEnabled)
                            {
                                var portalPlane = portal.PortalPlane;
                                var portalPlaneVector = new Vector4(portalPlane.normal.x, portalPlane.normal.y, portalPlane.normal.z, portalPlane.distance);

                                material.SetFloat(_enablePortalPlaneShaderId, 1f);
                                material.SetVector(_portalPlaneShaderId, portalPlaneVector);
                            }
                        }

                        if (meshFilter.sharedMesh != null)
                        {
                            for (int i = 0; i < meshFilter.sharedMesh.subMeshCount; i++)
                            {
                                command.DrawMesh(meshFilter.sharedMesh, matrix, material, i, 0);
                            }
                        }
                    }
                }
            }
        }

        private GlowPass _glowPass;

        public override void Create()
        {
            if (!Application.isPlaying) return;

            _glowPass = new GlowPass();
            _glowPass.renderPassEvent = RenderPassEvent.AfterRenderingSkybox;
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            if (!Application.isPlaying) return;

            renderer.EnqueuePass(_glowPass);
        }
    }
}
