﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Render
{
    /// <summary>
    /// Interface for an object that can glow
    /// </summary>
    public interface IGlowable : IGameObjectComponent
    {
        /// <summary>
        /// Is glowing currently enabled
        /// </summary>
        bool GlowEnabled { get; set; }

        /// <summary>
        /// The shared material used to make the renderer glow
        /// </summary>
        Material SharedGlowMaterial { get; set; }

        /// <summary>
        /// All possible meshes in an object that will glow
        /// </summary>
        MeshFilter[] Meshes { get; }

        /// <summary>
        /// Gets the instanced glow material for a given space.
        /// Each space will have it's own material.
        /// </summary>
        Material GetInstancedGlowMaterialForSpace(ISpaceData space);
    }
}