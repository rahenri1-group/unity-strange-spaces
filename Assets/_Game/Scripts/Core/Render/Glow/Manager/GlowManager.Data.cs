﻿using Game.Core.Entity;
using System.Collections.Generic;

namespace Game.Core.Render
{
    public partial class GlowManager : BaseModule
    {
        private class GlowableData
        {
            public IGlowable Glowable { get; }

            public IDynamicEntity Entity { get; }

            public GlowableData(IGlowable glowable, IDynamicEntity entity)
            {
                Glowable = glowable;
                Entity = entity;
            }
        }

        private class EntityGlowable
        {
            public IDynamicEntity Entity { get; }

            public IEnumerable<IGlowable> Glowables => _glowables;

            public bool HasGlowables => _glowables.Count > 0;

            private HashSet<IGlowable> _glowables;

            public EntityGlowable(IDynamicEntity entity)
            {
                Entity = entity;

                _glowables = new HashSet<IGlowable>();
            }

            public void AddGlowable(IGlowable glowable)
            {
                _glowables.Add(glowable);
            }

            public void RemoveGlowable(IGlowable glowable)
            {
                _glowables.Remove(glowable);
            }
        }

    }
}
