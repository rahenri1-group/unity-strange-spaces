﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Portal;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Render
{
    /// <inheritdoc cref="IGlowManager"/>
    [Dependency(
        contract: typeof(IGlowManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class GlowManager : BaseModule, IGlowManager
    {
        /// <inheritdoc />
        public override string ModuleName => "Glow Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly IDynamicPortalManager _dynamicPortalManager;
        private readonly ISpaceManager _spaceManager;
        private readonly IStaticPortalManager _staticPortalManager;

        private Dictionary<IGlowable, GlowableData> _allGlowables;
        private Dictionary<IDynamicEntity, EntityGlowable> _glowableEntities;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public GlowManager(
            IDynamicPortalManager dynamicPortalManager,
            ILogRouter logger,
            ISpaceManager spaceManager,
            IStaticPortalManager staticPortalManager)
            : base(logger)
        {
            _dynamicPortalManager = dynamicPortalManager;
            _spaceManager = spaceManager;
            _staticPortalManager = staticPortalManager;

            _allGlowables = new Dictionary<IGlowable, GlowableData>();
            _glowableEntities = new Dictionary<IDynamicEntity, EntityGlowable>();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            _allGlowables.Clear();
            _glowableEntities.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc />
        public IEnumerable<IGlowable> AllGlowables()
        {
            return _allGlowables.Keys;
        }

        /// <inheritdoc />
        public IEnumerable<(IGlowable Glowable, IPortal Portal)> AllGlowablesForSpace(ISpaceData space)
        {
            var glowables = new HashSet<(IGlowable, IPortal)>();

            foreach (var glowable in _allGlowables.Keys)
            {
                if (SpaceUtil.SpaceEquals(space, _spaceManager.GetObjectSpace(glowable.GameObject)))
                {
                    glowables.Add((glowable, null));
                }
            }

            var portals = _dynamicPortalManager.AllOpenDynamicPortalsToSpace(space)
                .Concat<IPortal>(_staticPortalManager.AllOpenStaticPortalsToSpace(space));

            foreach (var portal in portals)
            {
                var transporter = portal.GetComponent<IPortalTransporter>();
                if (transporter != null)
                {
                    foreach (var entity in transporter.EntitiesInPortalZone)
                    {
                        if (_glowableEntities.ContainsKey(entity))
                        {
                            var entityData = _glowableEntities[entity];
                            foreach (var glowable in entityData.Glowables)
                            {
                                glowables.Add((glowable, portal.EndPoint));
                            }
                        }
                    }
                }
            }

            return glowables;
        }

        /// <inheritdoc />
        public bool IsGlowableRegistered(IGlowable glowable)
        {
            return _allGlowables.ContainsKey(glowable);
        }

        /// <inheritdoc />
        public void RegisterGlowable(IGlowable glowable)
        {
            if (!IsGlowableRegistered(glowable))
            {
                var entity = glowable.GetComponentInParent<IDynamicEntity>();
                _allGlowables.Add(glowable, new GlowableData(glowable, entity));

                if (entity != null)
                {
                    if (!_glowableEntities.ContainsKey(entity))
                    {
                        _glowableEntities.Add(entity, new EntityGlowable(entity));
                    }

                    _glowableEntities[entity].AddGlowable(glowable);
                }
            }
        }

        /// <inheritdoc />
        public void UnregisterGlowable(IGlowable glowable)
        {
            if (IsGlowableRegistered(glowable))
            {
                var data = _allGlowables[glowable];

                if (data.Entity != null)
                {
                    var entityData = _glowableEntities[data.Entity];
                    entityData.RemoveGlowable(glowable);
                    if (!entityData.HasGlowables)
                    {
                        _glowableEntities.Remove(data.Entity);
                    }
                }

                _allGlowables.Remove(glowable);
            }
        }
    }
}
