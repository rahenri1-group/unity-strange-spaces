﻿using Game.Core.Portal;
using Game.Core.Space;
using System.Collections.Generic;

namespace Game.Core.Render
{
    /// <summary>
    /// Manager for <see cref="IGlowable"/>s
    /// </summary>
    public interface IGlowManager : IModule
    {
        /// <summary>
        /// Retrieves all <see cref="IGlowable"/>s
        /// </summary>
        IEnumerable<IGlowable> AllGlowables();

        /// <summary>
        /// Retrieves all <see cref="IGlowable"/>s that render in the provided <paramref name="space"/>. 
        /// If the glowables is partially in a portal, the end that is in the desired space will be provided.
        /// </summary>
        IEnumerable<(IGlowable Glowable, IPortal Portal)> AllGlowablesForSpace(ISpaceData space);

        /// <summary>
        /// Returns is a <paramref name="glowable"/> has been registered already or now
        /// </summary>
        bool IsGlowableRegistered(IGlowable glowable);

        /// <summary>
        /// Registers a new <see cref="IGlowable"/>. Used when it is created.
        /// </summary>
        void RegisterGlowable(IGlowable glowable);

        /// <summary>
        /// Unregisters a <see cref="IGlowable"/>. Used when it is destroyed.
        /// </summary>
        void UnregisterGlowable(IGlowable glowable);
    }
}
