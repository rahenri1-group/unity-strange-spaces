﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Core.Render
{
    /// <inheritdoc cref="IGlowable"/>
    public class SimpleGlowable : InjectedBehaviour, IGlowable
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public bool GlowEnabled { get => enabled; set => enabled = value; }

        /// <inheritdoc />
        public Material SharedGlowMaterial
        {
            get => _glowMaterial;
            set
            {
                _glowMaterial = value;

                foreach (var pair in _instancedMaterials)
                {
                    Destroy(pair.Value);
                }
                _instancedMaterials.Clear();
            }
        }

        /// <inheritdoc />
        public MeshFilter[] Meshes
        { 
            get => _meshes;
            private set
            {
                _meshes = value;
            }
        }

        [SerializeField] private Material _glowMaterial = null;
        [SerializeField] private MeshFilter[] _meshes = new MeshFilter[0];

        [Inject] private IEventBus _eventBus = null;
        [Inject] private IGlowManager _glowManager = null;

        private Dictionary<ISpaceData, Material> _instancedMaterials;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            if (Meshes.Length == 0)
            {
                Meshes = GetComponentsInChildren<MeshFilter>(true)
                    .Where(m => m.GetComponent<ICanvasElement>() == null) // don't make any UI elements glow
                    .ToArray();
            }

            _instancedMaterials = new Dictionary<ISpaceData, Material>();

            _glowManager.RegisterGlowable(this);
        }

        /// <inheritdoc />
        private void OnDestroy()
        {
            SharedGlowMaterial = null;

            _glowManager.UnregisterGlowable(this);
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            if (SharedGlowMaterial == null)
            {
                GlowEnabled = false;
            }

            _eventBus.Subscribe<EntityModifiedEvent>(OnEntityModified);
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            _eventBus.Unsubscribe<EntityModifiedEvent>(OnEntityModified);
        }

        private void OnEntityModified(EntityModifiedEvent eventArgs)
        {
            if (_glowManager.IsGlowableRegistered(this))
            {
                // re-register if we are a part of an entity that changed
                var parentEntity = GetComponentInParent<IEntity>();
                if (parentEntity == eventArgs.Entity)
                {
                    _glowManager.UnregisterGlowable(this);
                    _glowManager.RegisterGlowable(this);
                }
            }
        }

        /// <inheritdoc />
        public Material GetInstancedGlowMaterialForSpace(ISpaceData space)
        {
            if (!_instancedMaterials.ContainsKey(space))
            {
                _instancedMaterials[space] = new Material(SharedGlowMaterial);
            }

            return _instancedMaterials[space];
        }
    }
}