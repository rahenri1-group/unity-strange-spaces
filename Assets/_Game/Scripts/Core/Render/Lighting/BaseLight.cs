﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core.Render.Lighting 
{
    /// <inheritdoc cref="ILight"/>
    public abstract class BaseLight : InjectedBehaviour, ILight
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public virtual bool LightEnabled
        {
            get => UnityLight.enabled && GameObject.activeInHierarchy;
            set
            {
                UnityLight.enabled = value;

                if (_cachedLightEnabled != value)
                {
                    _cachedLightEnabled = value;
                    InvokeLightChanged();
                }
            }
        }

        /// <inheritdoc />
        public bool IsLightDynamic => _isLightDynamic;

        /// <inheritdoc />
        public LightType LightType => UnityLight.type;

        /// <inheritdoc />
        public LightProperties LightProperties
        {
            get => new LightProperties
            {
                Intensity = UnityLight.intensity,
                Range = UnityLight.range,
                Color = UnityLight.color,
                ColorTemperature = UnityLight.colorTemperature,
                UseColorTemperature = UnityLight.useColorTemperature,
                InnerSpotAngle = UnityLight.innerSpotAngle,
                SpotAngle = UnityLight.spotAngle,
                Cookie = UnityLight.cookie
            };
            set
            {
                UnityLight.intensity = value.Intensity;
                UnityLight.range = value.Range;
                UnityLight.color = value.Color;
                UnityLight.colorTemperature = value.ColorTemperature;
                UnityLight.useColorTemperature = value.UseColorTemperature;
                UnityLight.spotAngle = value.SpotAngle;
                UnityLight.innerSpotAngle = value.InnerSpotAngle;
                UnityLight.cookie = value.Cookie;

                if (_cachedLightProperties != value)
                {
                    _cachedLightProperties = value;
                    InvokeLightChanged();
                }
            }
        }

        /// <inheritdoc />
        public event Action<ILight> LightChanged;

        [Inject] protected ILightManager LightManager;

        [Tooltip("A dynamic light is one that can change position or space")]
        [SerializeField] private bool _isLightDynamic;

        protected abstract Light UnityLight { get; }

        private bool _cachedLightEnabled;
        private LightProperties _cachedLightProperties;

        private bool _lightRegistered;

        protected override void Awake()
        {
            base.Awake();

            _lightRegistered = false;
        }

        /// <inheritdoc />
        protected virtual void Start()
        {
            LightManager.RegisterLight(this);
            _lightRegistered = true;
        }

        /// <inheritdoc />
        protected virtual void OnDestroy()
        {
            if (_lightRegistered)
            {
                LightManager.UnregisterLight(this);
                _lightRegistered = false;
            }
        }

        /// <inheritdoc />
        protected virtual void OnValidate()
        {
            if (!Application.isPlaying) return;

            if (UnityLight == null) return; // UnityLight may not have been created yet in the first frame
           
            if (_cachedLightEnabled != LightEnabled)
            {
                _cachedLightEnabled = LightEnabled;
                InvokeLightChanged();
            }

            var newLightingProperties = LightProperties;
            if (_cachedLightProperties != newLightingProperties)
            {
                _cachedLightProperties = newLightingProperties;
                InvokeLightChanged();
            }
        }

        protected void InvokeLightChanged()
        {
            LightChanged?.Invoke(this);
        }
    }
}
