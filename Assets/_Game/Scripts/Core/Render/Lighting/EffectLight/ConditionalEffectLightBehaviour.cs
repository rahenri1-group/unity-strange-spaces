﻿using Game.Core.Conditional;
using System;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A light that applies a <see cref="ILightEffect"/> to the light using <see cref="IConditional"/>s to pick which effect to use.
    /// </summary>
    public class ConditionalEffectLightBehaviour : BaseLight
    {
        [Serializable]
        private class ConditionalEffect
        {
            public IConditional Condition => _condition;

            public ILightEffect LightEffect => _lightEffect;

            [SerializeReference] [SelectType(typeof(IConditional))] private IConditional _condition = null;

            [SerializeReference] [SelectType(typeof(ILightEffect))] private ILightEffect _lightEffect = null;
        }

        /// <summary>
        /// The current <see cref="ILightEffect"/>
        /// </summary>
        public ILightEffect LightEffect { get; private set; }

        [SerializeReference] [SelectType(typeof(ILightEffect))] private ILightEffect _defaultLightEffect = null;

        [SerializeField] private ConditionalEffect[] _conditionalEffects = new ConditionalEffect[0];

        protected override Light UnityLight => _light;
        private Light _light;

        /// <inheritdoc />
        protected override void Awake()
        {
            _light = GetComponent<Light>();

            base.Awake();

            foreach (var effects in _conditionalEffects)
            {
                effects.Condition.OnAwake();
            }
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            LightEffect = GetNextEffect();

            LightEffect?.OnEffectStart(this);

            foreach (var effects in _conditionalEffects)
            {
                effects.Condition.ConditionalChanged += OnConditionalChanged;
            }
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            LightEffect?.OnEffectStop();

            LightEffect = null;

            foreach (var effects in _conditionalEffects)
            {
                effects.Condition.ConditionalChanged -= OnConditionalChanged;
            }
        }

        /// <inheritdoc />
        protected override void OnDestroy()
        {
            base.OnDestroy();

            foreach (var effects in _conditionalEffects)
            {
                effects.Condition.OnDestroy();
            }
        }

        private void OnConditionalChanged(IConditional obj)
        {
            var nextEffect = GetNextEffect();
            if (nextEffect != LightEffect)
            {
                LightEffect?.OnEffectStop();
                LightEffect = nextEffect;
                LightEffect?.OnEffectStart(this);
            }
        }

        private ILightEffect GetNextEffect()
        {
            foreach (var conditionalEffect in _conditionalEffects)
            {
                if (conditionalEffect.Condition.Evaluate())
                {
                    return conditionalEffect.LightEffect;
                }
            }

            return _defaultLightEffect;
        }
    }
}
