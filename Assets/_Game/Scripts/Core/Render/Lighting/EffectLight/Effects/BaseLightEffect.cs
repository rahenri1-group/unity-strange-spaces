﻿namespace Game.Core.Render.Lighting
{
    /// <inheritdoc cref="ILightEffect>" />
    public abstract class BaseLightEffect : ILightEffect
    {
        protected ILight Light { get; private set; }

        private bool _startLightEnabled;
        private LightProperties _startLightProperties;

        /// <inheritdoc />
        public virtual void OnEffectStart(ILight light)
        {
            Light = light;

            _startLightEnabled = Light.LightEnabled;
            _startLightProperties = Light.LightProperties;
        }

        /// <inheritdoc />
        public virtual void OnEffectStop()
        {
            Light.LightEnabled = _startLightEnabled;
            Light.LightProperties = _startLightProperties;

            Light = null;
        }
    }
}
