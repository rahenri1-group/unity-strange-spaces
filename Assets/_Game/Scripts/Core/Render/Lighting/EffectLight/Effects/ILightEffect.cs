﻿namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Interface for a <see cref="ILight"/> effect
    /// </summary>
    public interface ILightEffect
    {
        /// <summary>
        /// Called when the effects starts
        /// </summary>
        /// <param name="light">The light to apply the effect to</param>
        void OnEffectStart(ILight light);

        /// <summary>
        /// Called when the light should stop.
        /// </summary>
        void OnEffectStop();
    }
}
