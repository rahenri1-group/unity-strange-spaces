﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A light effect composed of a sequence of <see cref="ILightEffect"/>s
    /// </summary>
    public class LightEffectSequence : BaseLightEffect
    {
        [Serializable]
        private class EffectData
        {
            public ILightEffect LightEffect => _lightEffect;
            public int MinDurationMillis => _minDurationMillis;
            public int MaxDurationMillis => _maxDurationMillis;

            [SerializeReference] [SelectType(typeof(ILightEffect))] private ILightEffect _lightEffect = null;

            [SerializeField] private IntReadonlyReference _minDurationMillis = null;
            [SerializeField] private IntReadonlyReference _maxDurationMillis = null;
        }

        [SerializeField] private EffectData[] _effects = new EffectData[0];

        private CancellationTokenSource _sequenceCts;

        private ILightEffect _currentEffect;

        /// <inheritdoc />
        public override void OnEffectStart(ILight light)
        {
            base.OnEffectStart(light);

            _currentEffect = null;

            _sequenceCts = new CancellationTokenSource();
            Sequence(_sequenceCts.Token).Forget();
        }

        /// <inheritdoc />
        public override void OnEffectStop()
        {
            if (_sequenceCts != null)
            {
                _sequenceCts.CancelAndDispose();
                _sequenceCts = null;
            }

            _currentEffect?.OnEffectStop();
            _currentEffect = null;

            base.OnEffectStop();
        }

        private async UniTask Sequence(CancellationToken cancellationToken)
        {
            try
            {
                Light.LightEnabled = true;

                while (true)
                {
                    foreach (var effect in _effects)
                    {
                        int delay = Random.Default.Range(effect.MinDurationMillis, effect.MaxDurationMillis);

                        _currentEffect?.OnEffectStop();
                        _currentEffect = effect.LightEffect;
                        _currentEffect?.OnEffectStart(Light);

                        await UniTask.Delay(delay, false, PlayerLoopTiming.Update, cancellationToken);
                    }
                }
            }
            catch (OperationCanceledException)
            {
                return;
            }
        }
    }
}
