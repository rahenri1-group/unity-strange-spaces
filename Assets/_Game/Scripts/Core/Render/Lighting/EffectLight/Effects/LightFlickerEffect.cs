﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    ///  A simple flicker effect
    /// </summary>
    public class LightFlickerEffect : BaseLightEffect
    {
        [SerializeField] private FloatReadonlyReference _minIntensity = null;
        [SerializeField] private FloatReadonlyReference _maxIntensity = null;
        [SerializeField] private IntReadonlyReference _smoothing = null;

        private CancellationTokenSource _flickerCts;

        /// <inheritdoc/>
        public override void OnEffectStart(ILight light)
        {
            base.OnEffectStart(light);

            Assert.IsTrue(_smoothing > 0);

            _flickerCts = new CancellationTokenSource();
            Flicker(_flickerCts.Token).Forget();
        }

        /// <inheritdoc/>
        public override void OnEffectStop()
        {
            if (_flickerCts != null)
            {
                _flickerCts.CancelAndDispose();
                _flickerCts = null;
            }

            base.OnEffectStop();
        }

        private async UniTask Flicker(CancellationToken cancellationToken)
        {
            var intensityQueue = new Queue<float>();
            float intensitySum = 0;

            // prefill queue
            for (int i = 0; i < _smoothing; i++)
            {
                float intensity = Random.Default.Range(_minIntensity, _maxIntensity);

                intensityQueue.Enqueue(intensity);
                intensitySum += intensity;
            }

            try
            {
                Light.LightEnabled = true;

                while (true)
                {
                    while (intensityQueue.Count >= _smoothing)
                    {
                        intensitySum -= intensityQueue.Dequeue();
                    }

                    float intensity = Random.Default.Range(_minIntensity, _maxIntensity);

                    intensityQueue.Enqueue(intensity);
                    intensitySum += intensity;

                    var lightProperties = Light.LightProperties;
                    lightProperties.Intensity = intensitySum / (float)intensityQueue.Count; 
                    Light.LightProperties = lightProperties;

                    await UniTask.Delay(10, false, PlayerLoopTiming.Update, cancellationToken);
                }
            }
            catch (OperationCanceledException)
            {
                return;
            }
        }
    }
}
