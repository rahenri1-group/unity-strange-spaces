﻿namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A simple constant off effect
    /// </summary>
    public class LightOffEffect : BaseLightEffect
    {
        /// <inheritdoc/>
        public override void OnEffectStart(ILight light)
        {
            base.OnEffectStart(light);

            light.LightEnabled = false;
        }
    }
}
