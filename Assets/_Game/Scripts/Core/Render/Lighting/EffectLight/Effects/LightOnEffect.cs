﻿namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A simple constant on effect
    /// </summary>
    public class LightOnEffect : BaseLightEffect
    {
        /// <inheritdoc/>
        public override void OnEffectStart(ILight light)
        {
            base.OnEffectStart(light);

            light.LightEnabled = true;
        }
    }
}
