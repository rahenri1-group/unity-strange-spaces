﻿using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A light that applies a <see cref="ILightEffect"/> to the light
    /// </summary>
    public class SimpleEffectLightBehaviour : BaseLight
    {
        /// <summary>
        /// The current <see cref="ILightEffect"/>
        /// </summary>
        public ILightEffect LightEffect 
        { 
            get => _lightEffect;
            set
            {
                if (_lightEffect != value)
                {
                    _lightEffect?.OnEffectStop();
                    _lightEffect = value;
                    _lightEffect?.OnEffectStart(this);

                    InvokeLightChanged();
                }
            }
        }

        [SerializeReference] [SelectType(typeof(ILightEffect))] private ILightEffect _lightEffect = null;

        protected override Light UnityLight => _light;
        private Light _light;

        /// <inheritdoc />
        protected override void Awake()
        {
            _light = GetComponent<Light>();

            base.Awake();
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            LightEffect?.OnEffectStart(this);
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            LightEffect?.OnEffectStop();
        }
    }
}
