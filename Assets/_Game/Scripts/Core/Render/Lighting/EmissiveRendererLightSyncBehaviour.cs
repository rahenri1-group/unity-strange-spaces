﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Syncs the <see cref="Material"/> of a <see cref="Renderer"/> to the current value of <see cref="ILight.LightEnabled"/>.
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class EmissiveRendererLightSyncBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(ILight))] private Component _lightObj = null;
        private ILight _light;

        [SerializeField] private Material _lightOffMaterial = null;
        private Material _lightOnMaterial;

        private Renderer _renderer;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_lightObj);
            Assert.IsNotNull(_lightOffMaterial);

            _light = _lightObj.GetComponent<ILight>();

            _renderer = GetComponent<Renderer>();
            _lightOnMaterial = _renderer.sharedMaterial;
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _light.LightChanged += OnLightChanged;

            if (!_light.LightEnabled)
            {
                OnLightChanged(_light);
            }
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _light.LightChanged -= OnLightChanged;
        }

        private void OnLightChanged(ILight obj)
        {
            _renderer.sharedMaterial = obj.LightEnabled ? _lightOnMaterial : _lightOffMaterial;
        }
    }
}
