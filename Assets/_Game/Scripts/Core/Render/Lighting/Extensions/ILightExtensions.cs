﻿using Game.Core.Math;
using System;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Collection of extensions for <see cref="ILight"/>s
    /// </summary>
    public static class ILightExtensions
    {
        /// <summary>
        /// Constructs a <see cref="Cone"/> that fits a spot light
        /// </summary>
        public static Cone ConstructSpotCone(this ILight light)
        {
            if (light.LightType != LightType.Spot)
            {
                throw new InvalidOperationException($"Light {light.GameObject.name} is not a spot light");
            }

            return new Cone(light.GameObject.transform.position,
                        light.GameObject.transform.forward,
                        light.LightProperties.SpotAngle,
                        light.LightProperties.Range);
        }

        /// <summary>
        /// Calculates the approximate intensity of a <see cref="ILight"/> at a point.
        /// Does not take into account if there is a shadow blocking the light or in the case of <see cref="LightType.Spot"/> if the light is aimed at the point.
        /// </summary>
        public static float LightIntensityAtPoint(this ILight light, Vector3 point)
        {
            var lightProperties = light.LightProperties;

            var distance = Vector3.Distance(point, light.GameObject.transform.position);

            return lightProperties.Intensity * (1f - Mathf.Clamp01(distance / lightProperties.Range));
        }
    }
}
