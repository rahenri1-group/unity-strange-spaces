﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Syncs the <see cref="GameObject.activeInHierarchy"/> to the current value of <see cref="ILight.LightEnabled"/>.
    /// </summary>
    [RequireComponent(typeof(ILight))]
    public class GameObjectLightSyncBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject _syncedGameObject;

        private ILight _light;

        private void Awake()
        {
            Assert.IsNotNull(_syncedGameObject);

            _light = this.GetComponentAsserted<ILight>();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _light.LightChanged += OnLightChanged;

            OnLightChanged(_light);
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _light.LightChanged -= OnLightChanged;
        }

        private void OnLightChanged(ILight light)
        {
            _syncedGameObject.SetActive(_light.LightEnabled);
        }
    }
}