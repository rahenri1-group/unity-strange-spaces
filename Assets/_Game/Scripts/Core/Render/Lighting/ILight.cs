﻿using System;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Interface for interacting with <see cref="Light"/>s
    /// </summary>
    public interface ILight : IGameObjectComponent 
    {
        /// <summary>
        /// Gets and sets if the light is currently enabled
        /// </summary>
        bool LightEnabled { get; set; }

        /// <summary>
        /// Is the light dynamic (able to change positions?)
        /// </summary>
        bool IsLightDynamic { get; }

        /// <summary>
        /// The light type of the light
        /// </summary>
        LightType LightType { get; }

        /// <summary>
        /// The properties of the light
        /// </summary>
        LightProperties LightProperties { get; set; }

        /// <summary>
        /// Raised when a lighting property is changed
        /// </summary>
        event Action<ILight> LightChanged;
    }
}
