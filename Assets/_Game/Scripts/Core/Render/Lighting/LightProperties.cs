﻿using Game.Core.Math;
using System;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// The properties of a <see cref="ILight"/>
    /// </summary>
    public struct LightProperties : IEquatable<LightProperties>
    {
        /// <summary>
        /// The intensity of the light
        /// </summary>
        public float Intensity { get; set; }

        /// <summary>
        /// The range of the light
        /// </summary>
        public float Range { get; set; }

        /// <summary>
        /// The color of the light
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// The color temperature of the light
        /// </summary>
        public float ColorTemperature { get; set; }

        /// <summary>
        /// Should <see cref="ColorTemperature"/> be used
        /// </summary>
        public bool UseColorTemperature { get; set; }

        /// <summary>
        /// The inner spot angle of the light.
        /// Only used by <see cref="LightType.Spot"/> lights
        /// </summary>
        public float InnerSpotAngle { get; set; }

        /// <summary>
        /// RGB texture that this light projects into the space
        /// </summary>
        public Texture Cookie { get; set; }

        /// <summary>
        /// The spot angle of the light.
        /// Only used by <see cref="LightType.Spot"/> lights
        /// </summary>
        public float SpotAngle { get; set; }
        
        public override bool Equals(object obj) => obj is LightProperties other && this.Equals(other);

        public bool Equals(LightProperties other)
        {
            return MathUtil.ApproximatelyEqual(Intensity, other.Intensity)
                && MathUtil.ApproximatelyEqual(Range, other.Range)
                && Color == other.Color
                && MathUtil.ApproximatelyEqual(ColorTemperature, other.ColorTemperature)
                && UseColorTemperature == other.UseColorTemperature
                && MathUtil.ApproximatelyEqual(InnerSpotAngle, other.InnerSpotAngle)
                && MathUtil.ApproximatelyEqual(SpotAngle, other.SpotAngle);
        }

        public override int GetHashCode()
        {
            return 
            (
                Intensity,
                Range,
                Color,
                ColorTemperature,
                UseColorTemperature,
                InnerSpotAngle,
                SpotAngle
            ).GetHashCode();
        }

        public static bool operator ==(LightProperties lhs, LightProperties rhs) => lhs.Equals(rhs);

        public static bool operator !=(LightProperties lhs, LightProperties rhs) => !(lhs == rhs);
    }
}
