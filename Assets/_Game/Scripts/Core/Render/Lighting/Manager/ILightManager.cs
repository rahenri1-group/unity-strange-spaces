﻿using Game.Core.Space;
using System;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// Manager of all lights in game
    /// </summary>
    public interface ILightManager : IModule
    {
        /// <summary>
        /// Registers a light to the manager
        /// </summary>
        void RegisterLight(ILight light);

        /// <summary>
        /// Unregisters a light, this should be called when a <see cref="ILight"/> is destroyed
        /// </summary>
        void UnregisterLight(ILight light);

        /// <summary>
        /// Returns all enabled static lights whose lights touch the provided <paramref name="point"/> in the provided <paramref name="space"/>.
        /// If a <paramref name="sortFunction"/> is provided, the results will be sorted by that (descending). If not the result will be unsorted.
        /// </summary>
        ILight[] StaticLightsThatTouchPoint(ISpaceData space, Vector3 point, Func<ILight, float> sortFunction = null);

        /// <summary>
        /// Returns all enabled dynamic lights whose lights touch the provided <paramref name="point"/> in the provided <paramref name="space"/>.
        /// If a <paramref name="sortFunction"/> is provided, the results will be sorted by that (descending). If not the result will be unsorted.
        /// </summary>
        ILight[] DynamicLightsThatTouchPoint(ISpaceData space, Vector3 point, Func<ILight, float> sortFunction = null);

        /// <summary>
        /// Calculates an approximation of the realtime lighting at a point in space
        /// </summary>
        void CalculateLightingAtPoint(ISpaceData space, Vector3 point, out Color color, out float intensity);
    }
}
