﻿using Game.Core.DependencyInjection;
using Game.Core.Math;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <inheritdoc cref="ILightManager" />
    [Dependency(
        contract: typeof(ILightManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class LightManager : BaseModule, ILightManager
    {
        /// <inheritdoc />
        public override string ModuleName => "Light Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly ISpaceManager _spaceManager;

        private HashSet<ILight> _registeredLights;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public LightManager (
            ILogRouter logger,
            ISpaceManager spaceManager)
            : base(logger) 
        {
            _spaceManager = spaceManager;

            _registeredLights = new HashSet<ILight>();
        }

        /// <inheritdoc />
        public void RegisterLight(ILight light)
        {
            if (_registeredLights.Contains(light))
            {
                ModuleLogWarning($"Light {light.GameObject.name} already registered");
                return;
            }

            _registeredLights.Add(light);
        }

        /// <inheritdoc />
        public void UnregisterLight(ILight light)
        {
            if (!_registeredLights.Contains(light))
            {
                ModuleLogWarning($"Attempt to unregister unknown light {light.GameObject.name}");
                return;
            }

            _registeredLights.Remove(light);
        }

        /// <inheritdoc />
        public ILight[] StaticLightsThatTouchPoint(ISpaceData space, Vector3 point, Func<ILight, float> sortFunction)
        {
            return LightsThatTouchPoint(
                space,
                point,
                (light => light.IsLightDynamic == false),
                sortFunction);
        }

        /// <inheritdoc />
        public ILight[] DynamicLightsThatTouchPoint(ISpaceData space, Vector3 point, Func<ILight, float> sortFunction)
        {
            return LightsThatTouchPoint(
                space,
                point,
                (light => light.IsLightDynamic == true),
                sortFunction);
        }

        /// <inheritdoc />
        public void CalculateLightingAtPoint(ISpaceData space, Vector3 point, out Color color, out float intensity)
        {
            color = Color.black;
            intensity = 0f;

            foreach (var light in LightsThatTouchPoint(space, point, null, null))
            {
                float lightIntensity = light.LightIntensityAtPoint(point);

                color += lightIntensity * light.LightProperties.Color;
                intensity += lightIntensity;
            }
        }

        private ILight[] LightsThatTouchPoint(ISpaceData space, Vector3 point, Func<ILight, bool> filterFunction, Func<ILight, float> sortFunction)
        {
            var lights = _registeredLights.Where(light =>
            {
                var lightSpace = _spaceManager.GetObjectSpace(light.GameObject);

            if (!light.LightEnabled
                || !SpaceUtil.SpaceEquals(lightSpace, space)
                || (filterFunction != null && filterFunction(light) == false))
                {
                    return false;
                }

                if (light.LightType == LightType.Point)
                {
                    var distance = Vector3.Distance(point, light.GameObject.transform.position);
                    return distance <= light.LightProperties.Range;
                }
                else if (light.LightType == LightType.Spot)
                {
                    var lightCone = light.ConstructSpotCone();
                    return lightCone.IsPointInCone(point);
                }

                return false;
            });

            if (sortFunction != null)
            {
                lights = lights.OrderByDescending(sortFunction);
            }

            return lights.ToArray();
        }
    }
}
