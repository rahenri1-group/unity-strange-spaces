﻿using UnityEngine;

namespace Game.Core.Render.Lighting
{
    /// <inheritdoc cref="BaseLight"/>
    [RequireComponent(typeof(Light))]
    public class SimpleLightBehaviour : BaseLight 
    {
        protected override Light UnityLight => _light;
        private Light _light;

        /// <inheritdoc />
        protected override void Awake()
        {
            _light = GetComponent<Light>();

            base.Awake();
        }
    }
}
