﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.Render.Lighting
{
    /// <summary>
    /// A <see cref="ILight"/> that is synchronized to another <see cref="ILight"/>
    /// </summary>
    public class SynchronizedLightBehaviour : BaseLight
    {
        [SerializeField] [TypeRestriction(typeof(ILight))] private Component _syncLightObj = null;
        private ILight _syncLight;

        [SerializeField] private BoolReadonlyReference _syncEnabled = null;
        [SerializeField] private BoolReadonlyReference _syncIntensity = null;
        [SerializeField] private BoolReadonlyReference _syncRange = null;
        [SerializeField] private BoolReadonlyReference _syncColor = null;
        [SerializeField] private BoolReadonlyReference _syncInnerSpotAngle = null;
        [SerializeField] private BoolReadonlyReference _syncSpotAngle = null;

        protected override Light UnityLight => _light;
        private Light _light;

        /// <inheritdoc />
        protected override void Awake()
        {
            Assert.IsNotNull(_syncLightObj);
            _syncLight = _syncLightObj.GetComponentAsserted<ILight>();

            _light = GetComponent<Light>();

            base.Awake();
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            _syncLight.LightChanged += OnSyncLightChanged;
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            _syncLight.LightChanged -= OnSyncLightChanged;
        }

        private void OnSyncLightChanged(ILight syncLight)
        {
            if (_syncEnabled) LightEnabled = syncLight.LightEnabled;

            var newLightProperties = LightProperties;
            var syncLightProperties = syncLight.LightProperties;

            if (_syncIntensity) newLightProperties.Intensity = syncLightProperties.Intensity;
            if (_syncRange) newLightProperties.Range = syncLightProperties.Range;
            if (_syncColor) newLightProperties.Color = syncLightProperties.Color;
            if (_syncInnerSpotAngle) newLightProperties.InnerSpotAngle = syncLightProperties.InnerSpotAngle;
            if (_syncSpotAngle) newLightProperties.SpotAngle = syncLightProperties.SpotAngle;

            LightProperties = newLightProperties;
        }
    }
}
