﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Extensions for <see cref=" IResourceMetadataManager"/>
    /// </summary>
    public static class IResourceMetadataManagerExtensions
    {
        public static T GetResourceMetadataComponent<T>(this IResourceMetadataManager resourceMetadataManager, string assetKey)
            where T : class, IResourceMetadataComponent
        {
            var metadata = resourceMetadataManager.GetResourceMetadata(assetKey);

            if (metadata != null)
            {
                return metadata.GetMetadataComponent<T>();
            }

            return null;
        }
    }
}
