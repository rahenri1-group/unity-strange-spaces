﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Resource
{
    /// <summary>
    /// Manages the loading and unloading of non-gameobject assets
    /// </summary>
    public interface IAssetResourceManager : IModule
    {
        /// <summary>
        /// Returns the asset with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetKey"></param>
        /// <returns></returns>
        UniTask<T> LoadAsset<T>(string assetKey) where T : class;

        /// <summary>
        /// Releases the previously loaded asset
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="asset"></param>
        void ReleaseAsset<T>(T asset) where T : class;

        /// <summary>
        /// Does the provided key point to a valid asset
        /// </summary>
        /// <param name="assetKey"></param>
        /// <returns></returns>
        bool IsAssetKeyValid(string assetKey);

        /// <summary>
        /// Returns all asset keys that match the provided label
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        UniTask<string[]> GetAssetKeysWithLabel(string label);
    }
}
