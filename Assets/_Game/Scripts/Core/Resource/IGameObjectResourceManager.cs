﻿using Cysharp.Threading.Tasks;
using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Resource
{
    /// <summary>
    /// Manages the loading and unloading of <see cref="GameObject"/>s for a given asset key
    /// </summary>
    public interface IGameObjectResourceManager : IModule
    {
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position);
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation);
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> InstantiateAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale);

        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position) where T : class;
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation) where T : class;
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> InstantiateAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale) where T : class;

        /// <summary>
        /// Destroys and unloads the provided <see cref="GameObject"/>. This only should be used on <see cref="GameObject"/>s initialized by this module.
        /// </summary>
        void DestroyObject(GameObject gameObject);

        /// <summary>
        /// Does the provided key point to a valid asset
        /// </summary>
        bool IsAssetKeyValid(string assetKey);

        /// <summary>
        /// Returns all asset keys that match the provided label
        /// </summary>
        UniTask<string[]> GetAssetKeysWithLabel(string label);
    }
}
