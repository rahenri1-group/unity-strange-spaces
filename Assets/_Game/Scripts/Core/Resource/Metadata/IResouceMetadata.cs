﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Metadata for a resource
    /// </summary>
    public interface IResouceMetadata
    {
        /// <summary>
        /// The key of the resource the metadata is for
        /// </summary>
        string ResourceKey { get; }

        /// <summary>
        /// Retrieves a metadata component of the provided type. 
        /// If this metadata does not have the provided component, null will be returned.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetMetadataComponent<T>() where T : class, IResourceMetadataComponent;
    }
}
