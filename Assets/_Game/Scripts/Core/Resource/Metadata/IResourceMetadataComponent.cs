﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Component of metadata for <see cref="IResouceMetadata"/>
    /// </summary>
    public interface IResourceMetadataComponent { }
}
