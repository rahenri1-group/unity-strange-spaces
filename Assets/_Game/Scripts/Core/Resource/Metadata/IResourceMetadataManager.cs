﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Manages the <see cref="IResouceMetadata"/> for resources
    /// </summary>
    public interface IResourceMetadataManager : IModule
    {
        /// <summary>
        /// Checks if there exists metadata for a resource
        /// </summary>
        /// <param name="assetKey"></param>
        /// <returns></returns>
        bool DoesResourceHaveMetadata(string assetKey);

        /// <summary>
        /// Retrieves the metadata for a resource.
        /// Returns null if the resource does not have metadata.
        /// </summary>
        /// <param name="assetKey"></param>
        /// <returns></returns>
        IResouceMetadata GetResourceMetadata(string assetKey);

        /// <summary>
        /// Retrieves all metadata of type <typeparamref name="T"/>.
        /// If none are found, an empty array will be returned.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        (IResouceMetadata, T)[] FindResourceMetadataOfType<T>() where T : class, IResourceMetadataComponent;
    }
}
