﻿using UnityEngine;

namespace Game.Core.Resource
{
    /// <inheritdoc cref="IResouceMetadata"/>
    [CreateAssetMenu(menuName = "Game/Resource/Metadata")]
    public class MetadataObject : ScriptableObject, IResouceMetadata
    {
        /// <inheritdoc/>
        public string ResourceKey
        {
            get => _resourceKey;
            set => _resourceKey = value;
        }

        [SerializeField] private string _resourceKey = string.Empty;

        [SerializeReference] [SelectType(typeof(IResourceMetadataComponent))] private IResourceMetadataComponent[] _metadataComponents = new IResourceMetadataComponent[0];

        /// <inheritdoc/>
        public T GetMetadataComponent<T>() where T : class, IResourceMetadataComponent
        {
            foreach (var component in _metadataComponents)
            {
                T typedComponent = component as T;
                if (typedComponent != null)
                {
                    return typedComponent;
                }
            }

            return null;
        }
    }
}
