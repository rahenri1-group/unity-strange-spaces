﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Metadata for a <see cref="IPoolableComponent"/>
    /// </summary>
    public interface IPoolableComponentMetadata : IResourceMetadataComponent
    {
        /// <summary>
        /// The minimum amount of items to keep in the pool
        /// </summary>
        int PoolSizeMin { get; }

        /// <summary>
        /// The max amount of items allowed in the pool
        /// </summary>
        int PoolSizeMax{ get; }
    }
}
