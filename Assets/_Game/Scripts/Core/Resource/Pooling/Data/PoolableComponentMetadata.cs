﻿using System;
using UnityEngine;

namespace Game.Core.Resource
{
    /// <inheritdoc cref="IPoolableComponentMetadata"/>
    [Serializable]
    public class PoolableComponentMetadata : IPoolableComponentMetadata
    {
        /// <inheritdoc />
        public int PoolSizeMin => _poolSizeMin;

        /// <inheritdoc />
        public int PoolSizeMax => _poolSizeMax;

        [SerializeField] private int _poolSizeMin = 0;
        [SerializeField] private int _poolSizeMax = 5;
    }
}
