﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Interface for components of a pooled gameobject that need to reset or restart state
    /// </summary>
    public interface IPoolableComponent
    {
        /// <summary>
        /// Called when an object has been retrieved from a pool and the gameobject has been enabled.
        /// </summary>
        void OnAllocate();

        /// <summary>
        /// Called before an object has been disabled and returned to the pool.
        /// </summary>
        void OnDispose();
    }
}
