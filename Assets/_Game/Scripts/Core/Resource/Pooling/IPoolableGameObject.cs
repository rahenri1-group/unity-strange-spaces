﻿namespace Game.Core.Resource
{
    /// <summary>
    /// Interface for an object created by a pool. 
    /// This contains all of the information the the manager will need to return it to a pool and reset the objects state.
    /// This will be added by the <see cref="IPoolableGameObjectManager"/> and should not be on the prefab.
    /// </summary>
    public interface IPoolableGameObject : IGameObjectComponent
    {
        /// <summary>
        /// The id of the pool this gameobject belongs to.
        /// </summary>
        string PoolId { get; }

        /// <summary>
        /// All of the components on the object or subobjects that need to update their internal state when returned or retrieved from a pool.
        /// </summary>
        IPoolableComponent[] PoolableComponents { get; }
    }
}
