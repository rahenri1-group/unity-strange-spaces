﻿using Cysharp.Threading.Tasks;
using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Resource
{
    /// <summary>
    /// Manages the loading and unloading of gameobjects from a precreated pool
    /// </summary>
    public interface IPoolableGameObjectManager : IModule
    {
        /// <summary>
        /// Returns if the provided asset key is valid and can be used with this module.
        /// </summary>
        bool IsAssetKeyValid(string assetKey);

        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position);
        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation);
        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> with the given <paramref name="assetKey"/>
        /// </summary>
        UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale);

        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position) where T : class;
        /// <summary>
        /// Instantiates the <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation) where T : class;
        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> with the given <paramref name="assetKey"/> of type <typeparamref name="T"/>
        /// </summary>
        UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale) where T : class;

        /// <summary>
        /// Returns a pooled <see cref="GameObject"/> back to the object pool. This only should be used on <see cref="GameObject"/>s retrieved by this module.
        /// </summary>
        void ReturnPooledObject(GameObject gameObject);
    }
}
