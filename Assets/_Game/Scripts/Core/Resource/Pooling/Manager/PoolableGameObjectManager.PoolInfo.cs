﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Resource
{
    public partial class PoolableGameObjectManager : BaseModule
    {
        private class PoolInfo
        {
            public IPoolableComponentMetadata Metadata { get; private set; }

            public GameObject PoolableHolderGameObject { get; private set; }

            public Queue<IPoolableGameObject> AvailablePoolables { get; private set; }

            public PoolInfo(IPoolableComponentMetadata metadata, GameObject poolableHolderGameObject)
            {
                Metadata = metadata;
                PoolableHolderGameObject = poolableHolderGameObject;

                AvailablePoolables = new Queue<IPoolableGameObject>();
            }
        }
    }
}
