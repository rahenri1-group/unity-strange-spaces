﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Resource
{
    /// <inheritdoc cref="IPoolableGameObjectManager"/>
    [Dependency(
        contract: typeof(IPoolableGameObjectManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class PoolableGameObjectManager : BaseModule, IPoolableGameObjectManager
    {
        /// <inheritdoc />
        public override string ModuleName => "Poolable GameObject Manager";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IResourceMetadataManager _resourceMetadataManager;
        private readonly ISpaceManager _spaceManager;

        private Dictionary<string, PoolInfo> _poolInfoMap;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PoolableGameObjectManager(
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            IResourceMetadataManager resourceMetadataManager,
            ISpaceManager spaceManager)
            : base(logger) 
        {
            _gameObjectResourceManager = gameObjectResourceManager;
            _resourceMetadataManager = resourceMetadataManager;
            _spaceManager = spaceManager;

            _poolInfoMap = new Dictionary<string, PoolInfo>();
        }

        public async override UniTask Initialize()
        {
            var metadatas = _resourceMetadataManager.FindResourceMetadataOfType<IPoolableComponentMetadata>();
            foreach (var metadata in metadatas)
            {
                var assetKey = metadata.Item1.ResourceKey;
                var poolableMetadata = metadata.Item2;

                await CreatePoolForAsset(assetKey, poolableMetadata);
            }

            await base.Initialize();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            // destroy all pooled objects
            foreach (var pair in _poolInfoMap)
            {
                var poolInfo = pair.Value;

                foreach (var poolable in poolInfo.AvailablePoolables)
                {
                    _gameObjectResourceManager.DestroyObject(poolable.GameObject);
                }

                UnityEngine.Object.Destroy(poolInfo.PoolableHolderGameObject);
            }

            _poolInfoMap.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc />
        public bool IsAssetKeyValid(string assetKey)
        {
            return _poolInfoMap.ContainsKey(assetKey);
        }

        /// <inheritdoc />
        public UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position)
        {
            return GetPooledObjectAsync(assetKey, space, position, Quaternion.identity, Vector3.one);
        }

        /// <inheritdoc />
        public UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation)
        {
            return GetPooledObjectAsync(assetKey, space, position, rotation, Vector3.one);
        }

        /// <inheritdoc />
        public async UniTask<GameObject> GetPooledObjectAsync(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            if (!IsAssetKeyValid(assetKey))
            {
                ModuleLogError($"Asset '{assetKey}' is not a known poolable object");
                return null;
            }

            var poolInfo = _poolInfoMap[assetKey];

            IPoolableGameObject poolable;
            if (poolInfo.AvailablePoolables.Count > 0)
            {
                // retrieve pooled version
                poolable = poolInfo.AvailablePoolables.Dequeue();
                
                var poolableGameObjectTransfrom = poolable.GameObject.transform;
                poolableGameObjectTransfrom.SetParent(null);

                await _spaceManager.MoveObjectToSpaceAsync(poolable.GameObject, space);

                poolableGameObjectTransfrom.localPosition = position;
                poolableGameObjectTransfrom.localRotation = rotation;
                poolableGameObjectTransfrom.localScale = scale;

                poolable.GameObject.SetActive(true);
            }
            else
            {
                // create new version
                var poolableGameObject = await _gameObjectResourceManager.InstantiateAsync(assetKey, space, position, rotation, scale);
                var poolableBehaviour = poolableGameObject.AddComponent<PoolableGameObjectBehaviour>();
                poolableBehaviour.PoolId = assetKey;

                poolable = poolableBehaviour;
            }

            foreach (var poolableComponent in poolable.PoolableComponents)
            {
                poolableComponent.OnAllocate();
            }

            return poolable.GameObject;
        }

        /// <inheritdoc />
        public UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position) where T : class
        {
            return GetPooledObjectAsync<T>(assetKey, space, position, Quaternion.identity, Vector3.one);
        }

        /// <inheritdoc />
        public UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation) where T : class
        {
            return GetPooledObjectAsync<T>(assetKey, space, position, rotation, Vector3.one);
        }

        /// <inheritdoc />
        public async UniTask<(GameObject GameObject, T Component)> GetPooledObjectAsync<T>(string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale) where T : class
        {
            var gameObject = await GetPooledObjectAsync(assetKey, space, position, rotation, scale);
            if (gameObject == null) return (null, null);

            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                Logger.LogError($"{assetKey} missing type {typeof(T).Name}");
                ReturnPooledObject(gameObject);
                return (null, null);
            }

            return (gameObject, component);
        }

        /// <inheritdoc />
        public void ReturnPooledObject(GameObject gameObject)
        {
            var poolableGameObject = gameObject.GetComponent<IPoolableGameObject>();

            if (poolableGameObject == null)
            {
                ModuleLogWarning($"{gameObject.name} is not poolable, destroying immediately");

                _gameObjectResourceManager.DestroyObject(gameObject);
                return;
            }

            ReturnPoolableObject(poolableGameObject);
        }

        private void ReturnPoolableObject(IPoolableGameObject poolable)
        {
            foreach (var poolableComponent in poolable.PoolableComponents)
            {
                poolableComponent.OnDispose();
            }

            poolable.GameObject.SetActive(false);

            var poolInfo = _poolInfoMap[poolable.PoolId];

            var poolableTransform = poolable.GameObject.transform;

            poolableTransform.SetParent(poolInfo.PoolableHolderGameObject.transform);
            poolableTransform.localPosition = Vector3.zero;
            poolableTransform.localRotation = Quaternion.identity;
            poolableTransform.localScale = Vector3.one;

            poolInfo.AvailablePoolables.Enqueue(poolable);
        }

        private async UniTask CreatePoolForAsset(string assetKey, IPoolableComponentMetadata poolableMetadata)
        {
            var poolHolder = new GameObject($"@{assetKey} pool");

            await _spaceManager.MoveObjectToSpaceAsync(poolHolder, null);
            poolHolder.transform.position = Vector3.zero;
            poolHolder.transform.rotation = Quaternion.identity;
            poolHolder.transform.localScale = Vector3.zero;

            _poolInfoMap.Add(assetKey, new PoolInfo(poolableMetadata, poolHolder));

            if (poolableMetadata != null && poolableMetadata.PoolSizeMin > 0)
            {
                for (int i = 0; i < poolableMetadata.PoolSizeMin; i++)
                {
                    var poolableGameObject = await _gameObjectResourceManager.InstantiateAsync(assetKey, null, Vector3.zero);
                    var poolableBehaviour = poolableGameObject.AddComponent<PoolableGameObjectBehaviour>();
                    poolableBehaviour.PoolId = assetKey;

                    ReturnPoolableObject(poolableBehaviour);
                }
            }
        }
    }
}
