﻿using UnityEngine;

namespace Game.Core.Resource
{
    /// <inheritdoc cref="IPoolableGameObject"/>
    public class PoolableGameObjectBehaviour : MonoBehaviour, IPoolableGameObject
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public string PoolId { get; set; }

        /// <inheritdoc />
        public IPoolableComponent[] PoolableComponents { get; private set; }

        /// <inheritdoc />
        private void Awake()
        {
            PoolableComponents = GetComponentsInChildren<IPoolableComponent>();
        }
    }
}
