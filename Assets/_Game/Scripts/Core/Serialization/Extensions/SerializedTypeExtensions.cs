﻿using UnityEngine;

namespace Game.Core.Serialization
{
    public static class SerializedTypeExtensions
    {
        /// <summary>
        /// Converts a <see cref="Vector3"/> to a <see cref="SerializedVector3"/>
        /// </summary>
        public static SerializedVector3 ToSerializedVector3(this Vector3 vector)
        {
            return new SerializedVector3(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Converts a <see cref="SerializedVector3"/> to a <see cref="Vector3"/>
        /// </summary>
        public static Vector3 ToUnityVector3(this SerializedVector3 vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        /// <summary>
        /// Converts a <see cref="Quaternion"/> to a <see cref="SerializedQuaternion"/>
        /// </summary>
        public static SerializedQuaternion ToSerializedQuaternion(this Quaternion quaternion)
        {
            return new SerializedQuaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }

        /// <summary>
        /// Converts a <see cref="SerializedQuaternion"/> to a <see cref="Quaternion"/>
        /// </summary>
        public static Quaternion ToUnityQuaternion(this SerializedQuaternion quaternion)
        {
            return new Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
        }
    }
}
