﻿using System;
using System.Collections.Generic;

namespace Game.Core.Serialization
{
    /// <summary>
    /// Module that deserializes json
    /// </summary>
    public interface IJsonDeserializer : IModule
    {
        /// <summary>
        /// Deserializes <paramref name="json"/> into a new instance of <typeparamref name="T"/>
        /// </summary>
        T Deserialize<T>(string json);

        /// <summary>
        /// Deserializes <paramref name="json"/> into a new instance of <paramref name="type"/>
        /// </summary>
        object Deserialize(string json, Type type);

        /// <summary>
        /// Returns the keys and values of <paramref name="json"/>
        /// </summary>
        Dictionary<string, string> KeyValues(string json);

        /// <summary>
        /// Populates <paramref name="obj"/> with matching properties in <paramref name="json"/>
        /// </summary>
        void PopulateObject(object obj, string json);
    }
}
