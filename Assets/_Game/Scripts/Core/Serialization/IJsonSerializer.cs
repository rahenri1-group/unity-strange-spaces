﻿namespace Game.Core.Serialization
{
    /// <summary>
    /// Module that serializes objects to json
    /// </summary>
    public interface IJsonSerializer : IModule
    {
        /// <summary>
        /// Serializes <paramref name="obj"/> to json text
        /// </summary>
        string Serialize(object obj);

        /// <summary>
        /// Serializes <paramref name="obj"/> to json text in easily readable format 
        /// </summary>
        string SerializeHumanReadable(object obj);
    }
}
