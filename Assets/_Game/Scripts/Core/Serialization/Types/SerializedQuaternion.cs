﻿using System;

namespace Game.Core.Serialization
{
    /// <summary>
    /// A Quaternion for easy serialization
    /// </summary>
    [Serializable]
    public class SerializedQuaternion
    {
        public float X;
        public float Y;
        public float Z;
        public float W;

        public SerializedQuaternion()
        {
            X = 0f;
            Y = 0f;
            Z = 0f;
            W = 1f;
        }

        public SerializedQuaternion(float x, float y, float z, float w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }
    }
}
