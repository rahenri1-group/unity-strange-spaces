﻿using System;

namespace Game.Core.Serialization
{
    /// <summary>
    /// A Vector3 for easy serialization
    /// </summary>
    [Serializable]
    public class SerializedVector3
    {
        public float X;
        public float Y;
        public float Z;

        public SerializedVector3()
        {
            X = 0f;
            Y = 0f;
            Z = 0f;
        }

        public SerializedVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
