﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Hooks up an array of ints to the layer selector in the editor.
    /// </summary>
    public class LayerAttribute : PropertyAttribute { }
}
