﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Marks a property as read only in the editor.
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute { }
}