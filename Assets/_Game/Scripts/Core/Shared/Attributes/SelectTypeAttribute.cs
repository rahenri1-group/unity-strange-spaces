﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Allows a property to be wired up with non-unity classes. <see cref="SerializeReference"/>.
    /// </summary>
    public class SelectTypeAttribute : PropertyAttribute
    {
        /// <summary>
        /// The types allowed to wired up. Cannot inherit from <see cref="UnityEngine.Object"/>.
        /// </summary>
        public Type FieldType { get; private set; }
        
        /// <summary>
        /// Contructor
        /// </summary>
        public SelectTypeAttribute(Type fieldType)
        {
            FieldType = fieldType;
        }
    }
}
