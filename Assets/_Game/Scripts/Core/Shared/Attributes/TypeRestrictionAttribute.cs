﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Restricts a propery to only allowing the editor to wireup objects that match <see cref="TypeRestrictionAttribute.Contract"/>.
    /// </summary>
    public class TypeRestrictionAttribute : PropertyAttribute
    {
        /// <summary>
        /// The types allowed to be wired up to the property.
        /// </summary>
        public Type Contract { get; private set; }

        /// <summary>
        /// Are scene objects allowed to be wired up to the property.
        /// </summary>
        public bool AllowSceneObjects { get; private set; }

        /// <summary>
        /// Contructor
        /// </summary>
        public TypeRestrictionAttribute(Type contract, bool allowSceneObjects = true)
        {
            Contract = contract;
            AllowSceneObjects = allowSceneObjects;
        }
    }
}
