﻿using System;

namespace Game.Core.Conditional
{
    /// <summary>
    /// Conditional that evaluates a boolean value
    /// </summary>
    public class BoolEvaluate : IConditional
    {
        public BoolReadonlyReference _boolean;

        /// <inheritdoc/>
        public event Action<IConditional> ConditionalChanged;

        /// <inheritdoc/>
        public bool Evaluate() => _boolean;

        /// <inheritdoc/>
        public void OnAwake()
        {
            _boolean.ValueChanged += OnBooleanChanged;
        }

        /// <inheritdoc/>
        public void OnDestroy()
        {
            _boolean.ValueChanged -= OnBooleanChanged;
        }

        private void OnBooleanChanged()
        {
            ConditionalChanged?.Invoke(this);
        }
    }
}
