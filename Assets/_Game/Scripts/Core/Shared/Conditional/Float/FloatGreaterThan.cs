﻿using System;

namespace Game.Core.Conditional
{
    /// <summary>
    /// Conditional that evaluates float > float
    /// </summary>
    [Serializable]
    public class FloatGreaterThan : IConditional
    {
        public FloatReadonlyReference _floatLeftSide;
        public FloatReadonlyReference _floatRightSide;

        /// <inheritdoc/>
        public event Action<IConditional> ConditionalChanged;

        /// <inheritdoc/>
        public bool Evaluate() => _floatLeftSide > _floatRightSide;

        /// <inheritdoc/>
        public void OnAwake()
        {
            _floatLeftSide.ValueChanged += OnFloatChanged;
            _floatRightSide.ValueChanged += OnFloatChanged;
        }

        /// <inheritdoc/>
        public void OnDestroy()
        {
            _floatLeftSide.ValueChanged -= OnFloatChanged;
            _floatRightSide.ValueChanged -= OnFloatChanged;
        }

        private void OnFloatChanged()
        {
            ConditionalChanged?.Invoke(this);
        }
    }
}
