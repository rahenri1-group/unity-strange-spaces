﻿using System;

namespace Game.Core.Conditional
{
    /// <summary>
    /// Interface for conditional logic
    /// </summary>
    public interface IConditional
    {
        /// <summary>
        /// Raised when one of the backing properties of the conditional changes
        /// </summary>
        event Action<IConditional> ConditionalChanged;

        /// <summary>
        /// Evaluates the conditional
        /// </summary>
        /// <returns></returns>
        bool Evaluate();

        /// <summary>
        /// Must be called by the behaviour that contains this when it wakes up
        /// </summary>
        void OnAwake();

        /// <summary>
        /// Must be called by the behaviour that contains this when it is destroyed
        /// </summary>
        void OnDestroy();
    }
}
