﻿using System;

namespace Game.Core.Conditional
{
    /// <summary>
    /// Conditional that evaluates int < int
    /// </summary>
    [Serializable]
    public class IntLessThan : IConditional
    {
        public IntReadonlyReference _intLeftSide;
        public IntReadonlyReference _intRightSide;

        /// <inheritdoc/>
        public event Action<IConditional> ConditionalChanged;

        /// <inheritdoc/>
        public bool Evaluate() => _intLeftSide < _intRightSide;

        /// <inheritdoc/>
        public void OnAwake()
        {
            _intLeftSide.ValueChanged += OnIntChanged;
            _intRightSide.ValueChanged += OnIntChanged;
        }

        /// <inheritdoc/>
        public void OnDestroy()
        {
            _intLeftSide.ValueChanged -= OnIntChanged;
            _intRightSide.ValueChanged -= OnIntChanged;
        }

        private void OnIntChanged()
        {
            ConditionalChanged?.Invoke(this);
        }
    }
}
