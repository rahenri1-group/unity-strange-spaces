﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="Array"/>s.
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// Returns a new array starting from <paramref name="offset"/> going to the end.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static T[] SubArray<T>(this T[] array, int offset)
        {
            return array.SubArray(offset, array.Length - offset);
        }

        /// <summary>
        /// Returns a new array starting from <paramref name="offset"/> with a length of <paramref name="length"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static T[] SubArray<T>(this T[] array, int offset, int length)
        {
            T[] result = new T[length];
            Array.Copy(array, offset, result, 0, length);
            return result;
        }
    }
}
