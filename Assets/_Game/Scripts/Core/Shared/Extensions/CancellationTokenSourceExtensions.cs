﻿using System.Threading;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="CancellationTokenSource"/>s.
    /// </summary>
    public static class CancellationTokenSourceExtensions
    {
        /// <summary>
        /// Helper methods that calls both <see cref="CancellationTokenSource.Cancel"/> and <see cref="CancellationTokenSource.Dispose"/>
        /// </summary>
        /// <param name="cancellationTokenSource"></param>
        public static void CancelAndDispose(this CancellationTokenSource cancellationTokenSource)
        {
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();
        }
    }
}
