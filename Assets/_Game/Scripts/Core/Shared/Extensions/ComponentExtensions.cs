﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="Component"/>s.
    /// </summary>
    public static class ComponentExtensions
    {
        /// <summary>
        /// Version of <see cref="Component.GetComponent{T}"/> that asserts that the value is not null.
        /// </summary>
        public static T GetComponentAsserted<T>(this Component component) where T : class
        {
            T retValue = component.GetComponent<T>();
            Assert.IsNotNull(retValue);
            return retValue;
        }

        /// <summary>
        /// Version of <see cref="GetComponentAsserted{T}(Component)"/> for an array of components
        /// </summary>
        public static T[] GetComponentArrayAsserted<T>(this Component[] components) where T : class
        {
            var array = new T[components.Length];
            for (int i = 0; i < components.Length; i++)
            {
                array[i] = components[i].GetComponent<T>();
            }

            return array;
        }

        /// <summary>
        /// Version of <see cref="Component.GetComponentInParent{T}"/> that asserts that the value is not null.
        /// </summary>
        public static T GetComponentInParentAsserted<T>(this Component component) where T : class
        {
            T retValue = component.GetComponentInParent<T>();
            Assert.IsNotNull(retValue);
            return retValue;
        }

        /// <summary>
        /// Version of <see cref="Component.GetComponentInParent{T}"/> that ONLY searches the parent objects of a component.
        /// </summary>
        public static T GetComponentInParentExcludeSelf<T>(this Component component) where T : class
        {
            if (component.transform.parent != null) 
            {
                return component.transform.parent.GetComponentInParent<T>();
            }

            return null;
        }

        /// <summary>
        /// Version of <see cref="Component.GetComponentInParent{T,bool}"/> that ONLY searches the parent objects of a component.
        /// </summary>
        public static T GetComponentInParentExcludeSelf<T>(this Component component, bool includeInactive) where T : class
        {
            if (component.transform.parent != null)
            {
                return component.transform.parent.GetComponentInParent<T>(includeInactive);
            }

            return null;
        }
    }
}