﻿using System.Collections.Generic;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="Dictionary{TKey, TValue}"/>s.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        ///  Adds the contents of <paramref name="otherDictionary"/> to the dictionary, overwriting any values that share a key.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="otherDictionary"></param>
        public static void AddOverwrite<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, Dictionary<TKey, TValue> otherDictionary)
        {
            foreach (var keyValue in otherDictionary)
            {
                dictionary[keyValue.Key] = keyValue.Value;
            }
        }
    }
}
