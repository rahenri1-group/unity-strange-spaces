﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="GameObject"/>s.
    /// </summary>
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Gets the requests <see cref="Component"/>. If one is not present on the <see cref="GameObject"/>, a new one is added.
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            T retValue = gameObject.GetComponent<T>();
            if (retValue == null)
            {
                retValue = (T)gameObject.AddComponent(typeof(T));
            }

            return retValue;
        }

        /// <summary>
        /// Returns the scene path of the <see cref="GameObject"/>.
        /// </summary>
        public static string GameObjectPath(this GameObject gameObject)
        {
            string path = gameObject.name;
            while (gameObject.transform.parent != null)
            {
                gameObject = gameObject.transform.parent.gameObject;
                path = gameObject.name + "/" + path;
            }
            return path;
        }

        /// <summary>
        /// Returns the scene path of the <see cref="GameObject"/> relative to the provided parent. 
        /// Returns the full scene path if <paramref name="parent"/> is not actually the parent to the object.
        /// </summary>
        public static string GameObjectRelativePath(this GameObject gameObject, GameObject parent)
        {
            string path = gameObject.name;
            while (gameObject.transform.parent != null && gameObject.transform.parent != parent.transform)
            {
                gameObject = gameObject.transform.parent.gameObject;
                path = gameObject.name + "/" + path;
            }
            return path;
        }
    }
}