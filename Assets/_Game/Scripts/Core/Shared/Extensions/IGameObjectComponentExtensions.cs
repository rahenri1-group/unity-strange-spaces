﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="IGameObjectComponent"/>s.
    /// </summary>
    public static class IGameObjectComponentExtensions
    {
        /// <summary>
        /// Interface version of <see cref="GameObject.GetComponent{T}"/>.
        /// </summary>
        public static T GetComponent<T>(this IGameObjectComponent gameObject) where T : class
        {
            return gameObject.GameObject.GetComponent<T>();
        }

        /// <summary>
        /// Interface version of <see cref="GameObject.AddComponent{T}"/>.
        /// </summary>
        public static T AddComponent<T>(this IGameObjectComponent gameObject) where T : Component
        {
            return gameObject.GameObject.AddComponent<T>();
        }

        /// <summary>
        /// Interface version of <see cref="GameObject.GetComponentInParent{T}"/>.
        /// </summary>
        public static T GetComponentInParent<T>(this IGameObjectComponent gameObject)
        {
            return gameObject.GameObject.GetComponentInParent<T>();
        }

        /// <summary>
        /// Interface version of <see cref="GameObject.GetComponentInChildren{T}"/>.
        /// </summary>
        public static T GetComponentInChildren<T>(this IGameObjectComponent gameObject)
        {
            return gameObject.GameObject.GetComponentInChildren<T>();
        }

        /// <summary>
        /// Interface version of <see cref="GameObject.GetComponentsInChildren{T}"/>.
        /// </summary>
        public static T[] GetComponentsInChildren<T>(this IGameObjectComponent gameObject)
        {
            return gameObject.GameObject.GetComponentsInChildren<T>();
        }

        /// <summary>
        /// Interface version of <see cref="GameObject.GetComponentInChildren(System.Type, bool)"/>.
        /// </summary>
        public static T[] GetComponentsInChildren<T>(this IGameObjectComponent gameObject, bool includeInactive)
        {
            return gameObject.GameObject.GetComponentsInChildren<T>(includeInactive);
        }
    }
}
