﻿using System.Collections.Generic;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="List{T}"/>s.
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Returns a random index in the <see cref="List{T}"/>.
        /// </summary>
        public static int RandomIndex<T>(this List<T> list, IRandom random = null)
        {
            if (random == null) random = Random.Default;
            return random.Range(0, list.Count);
        }

        /// <summary>
        /// Returns a random element in the <see cref="List{T}"/>.
        /// </summary>
        public static T RandomElement<T>(this List<T> list, IRandom random = null)
        {
            return list[list.RandomIndex(random)];
        }

        /// <summary>
        /// Removes an the item at <paramref name="index"/> from the list. The removed item is replaced with the item at the end of the list.
        /// </summary>
        public static void RemoveAtSwap<T>(this List<T> list, int index)
        {
            list.SwapAt(index, list.Count - 1);
            list.RemoveAt(list.Count - 1);
        }

        /// <summary>
        /// Swaps the elements at <paramref name="index1"/> and <paramref name="index2"/>.
        /// </summary>
        public static void SwapAt<T>(this List<T> list, int index1, int index2)
        {
            var element1 = list[index1];
            var element2 = list[index2];

            list[index1] = element2;
            list[index2] = element1;
        }

        /// <summary>
        /// Removes and returns a random element in the list. The removed items is replaced with the item at the end of the list.
        /// </summary>
        public static T RemoveRandomElement<T>(this List<T> list, IRandom random = null)
        {
            int index = list.RandomIndex(random);
            T randomElement = list[index];
            list.RemoveAtSwap(index);
            return randomElement;
        }

        /// <summary>
        /// Shuffles a list using the <see href="http://en.wikipedia.org/wiki/Fisher-Yates_shuffle">Fisher-Yates shuffle</see>
        /// </summary>
        public static void Shuffle<T>(this List<T> list, IRandom random = null)
        {
            if (random == null) random = Random.Default;

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
