﻿namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="string"/>s.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Prepends the <see cref="string"/> with <paramref name="prepend"/> if it is not already present.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="prepend"></param>
        /// <returns></returns>
        public static string PrependIfMissing(this string str, string prepend)
        {
            if (!str.StartsWith(prepend))
            {
                str = prepend + str;
            }

            return str;
        }

    }
}