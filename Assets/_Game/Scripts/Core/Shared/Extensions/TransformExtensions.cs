﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="Transform"/>s.
    /// </summary>
    public static class TransformExtensions 
    {
        /// <summary>
        /// Transforms rotation from local space to world space.
        /// </summary>
        public static Quaternion TransformRotation(this Transform transform, Quaternion localRotation)
        {
            return transform.rotation * localRotation;
        }

        /// <summary>
        /// Transforms rotation from world space to local space.
        /// </summary>
        public static Quaternion InverseTransformRotation(this Transform transform, Quaternion worldSpaceRotation)
        {
            return Quaternion.Inverse(transform.rotation) * worldSpaceRotation;
        }

        /// <summary>
        /// Transforms a position from local space to world space without using scale
        /// </summary>
        public static Vector3 TransformPointUnscaled(this Transform transform, Vector3 position)
        {
            var localToWorldMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            return localToWorldMatrix.MultiplyPoint3x4(position);
        }

        /// <summary>
        /// Transforms a position from world space to local space without using scale
        /// </summary>
        public static Vector3 InverseTransformPointUnscaled(this Transform transform, Vector3 position)
        {
            var worldToLocalMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
            return worldToLocalMatrix.MultiplyPoint3x4(position);
        }

    }
}