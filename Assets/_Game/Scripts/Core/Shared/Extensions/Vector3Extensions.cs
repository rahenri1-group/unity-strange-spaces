﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Collection of extensions for <see cref="Vector3"/>s.
    /// </summary>
    public static class Vector3Extensions
    {
        /// <summary>
        /// Constructs a new <see cref="Vector3"/> with a value of 1 / the value of <paramref name="vector"/>.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector3 Invert(this Vector3 vector)
        {
            return new Vector3(1 / vector.x, 1 / vector.y, 1 / vector.z);
        }
    }
}
