﻿using UnityEngine;

namespace Game.Core.Gizmo
{
    /// <summary>
    /// Renders a ray for debug purposes
    /// </summary>
    public class RayGizmoBehaviour : MonoBehaviour
    {
        [SerializeField] private ColorReadonlyReference _color = null;
        [SerializeField] private FloatReadonlyReference _length = null;

        private void OnDrawGizmos()
        {
            Gizmos.color = _color;
            Gizmos.DrawLine(transform.position, transform.position + _length * transform.forward);
        }
    }
}
