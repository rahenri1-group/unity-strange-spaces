﻿namespace Game.Core
{
    /// <summary>
    /// The level of a log
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Level for extra debugging information
        /// </summary>
        Debug = 0,

        /// <summary>
        /// Level for non-error information
        /// </summary>
        Info = 1,
        
        /// <summary>
        /// Level for warnings
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Level for errors
        /// </summary>
        Error = 3,

        /// <summary>
        /// Level for exceptions
        /// </summary>
        Exception = 4
    }
}