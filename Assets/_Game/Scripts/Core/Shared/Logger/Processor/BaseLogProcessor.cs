﻿using System.Collections.Generic;

namespace Game.Core
{
    /// <summary>
    /// Base <see cref="ILogProcessor"/> that will cache logs until the class is ready to begin processing
    /// </summary>
    public abstract class BaseLogProcessor : ILogProcessor
    {
        protected abstract bool ReadyToProcess { get; }
        
        private LinkedList<LogData> _logCache;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseLogProcessor()
        {
            _logCache = new LinkedList<LogData>();
        }

        /// <inheritdoc/>
        public void ProcessLog(LogData log)
        {
            if (ReadyToProcess)
            {
                ProcessLogData(log);
            }
            else
            {
                _logCache.AddLast(log);
            }
        }

        protected void ProcessCachedLogs()
        {
            foreach (var log in _logCache)
            {
                ProcessLogData(log);
            }

            _logCache.Clear();
        }

        protected abstract void ProcessLogData(LogData log);
    }
}
