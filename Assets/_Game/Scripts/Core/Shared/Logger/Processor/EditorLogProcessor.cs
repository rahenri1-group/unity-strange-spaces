﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Prints logs to the Unity debug window.
    /// </summary>
    [Dependency(
        contract: typeof(ILogProcessor),
        lifetime: Lifetime.Singleton)]
    public class EditorLogProcessor : BaseLogProcessor
    {
        protected override bool ReadyToProcess => true;

        protected override void ProcessLogData(LogData log)
        {
            if (log.Category == "Unity") return;

            if (!string.IsNullOrEmpty(log.Message))
            {
                string logMessage = !string.IsNullOrEmpty(log.Category) ? $"{log.Category}: {log.Message}" : log.Message;

                switch (log.Level)
                {
                    case LogLevel.Exception:
                    case LogLevel.Error:
                        Debug.LogError(logMessage);
                        break;
                    case LogLevel.Warning:
                        Debug.LogWarning(logMessage);
                        break;
                    case LogLevel.Debug:
                    case LogLevel.Info:
                    default:
                        Debug.Log(logMessage);
                        break;
                }
            }

            if (log.Exception != null)
            {
                Debug.LogException(log.Exception);
            }
        }
    }
}