﻿namespace Game.Core
{
    /// <summary>
    /// Processes logs
    /// </summary>
    public interface ILogProcessor
    {
        /// <summary>
        /// Processes a log
        /// </summary>
        /// <param name="logData"></param>
        void ProcessLog(LogData logData);
    }
}
