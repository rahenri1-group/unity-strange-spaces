﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Data structure for holding a log
    /// </summary>
    public struct LogData
    {
        /// <summary>
        /// The category of the log
        /// </summary>
        public string Category { get; }

        /// <summary>
        /// The log level
        /// </summary>
        public LogLevel Level { get; }

        /// <summary>
        ///  The log message
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// The exception for the log if the log source was from an error
        /// </summary>
        public Exception Exception { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        public LogData(string category, LogLevel level, string message)
        {
            Category = category;
            Level = level;
            Message = message;
            Exception = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public LogData(string category, Exception exception, string message)
        {
            Category = category;
            Level = LogLevel.Exception;
            Message = message;
            Exception = exception;
        }
    }
}
