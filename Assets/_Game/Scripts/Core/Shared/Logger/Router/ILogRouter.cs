﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Routes logs to the various <see cref="ILogProcessor"/>s
    /// </summary>
    public interface ILogRouter : IModule
    {
        /// <summary>
        /// Logs a <paramref name="message"/> with level <paramref name="level"/> and category <paramref name="logCategory"/>.
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="level"></param>
        /// <param name="message"></param>
        void Log(string logCategory, LogLevel level, string message);

        /// <summary>
        /// Logs at <see cref="LogLevel.Debug"/>
        /// </summary>
        /// <param name="message"></param>
        void LogDebug(string message);
        /// <summary>
        /// Logs at <see cref="LogLevel.Debug"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="message"></param>
        void LogDebug(string logCategory, string message);

        /// <summary>
        /// Logs at <see cref="LogLevel.Info"/>
        /// </summary>
        /// <param name="message"></param>
        void LogInfo(string message);
        /// <summary>
        /// Logs at <see cref="LogLevel.Info"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="message"></param>
        void LogInfo(string logCategory, string message);

        /// <summary>
        /// Logs at <see cref="LogLevel.Warning"/>
        /// </summary>
        /// <param name="message"></param>
        void LogWarning(string message);
        /// <summary>
        /// Logs at <see cref="LogLevel.Warning"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="message"></param>
        void LogWarning(string logCategory, string message);

        /// <summary>
        /// Logs at <see cref="LogLevel.Error"/>
        /// </summary>
        /// <param name="message"></param>
        void LogError(string message);
        /// <summary>
        /// Logs at <see cref="LogLevel.Error"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="message"></param>
        void LogError(string logCategory, string message);

        /// <summary>
        /// Logs at <see cref="LogLevel.Exception"/>
        /// </summary>
        /// <param name="exception"></param>
        void LogException(Exception exception);
        /// <summary>
        /// Logs at <see cref="LogLevel.Exception"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="exception"></param>
        void LogException(string logCategory, Exception exception);
        /// <summary>
        /// Logs at <see cref="LogLevel.Exception"/>
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="message"></param>
        void LogException(Exception exception, string message);
        /// <summary>
        /// Logs at <see cref="LogLevel.Exception"/>
        /// </summary>
        /// <param name="logCategory"></param>
        /// <param name="exception"></param>
        /// <param name="message"></param>
        void LogException(string logCategory, Exception exception, string message);
    }
}
