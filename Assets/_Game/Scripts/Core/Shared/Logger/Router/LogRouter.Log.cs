﻿using System;
using UnityEngine;

namespace Game.Core
{
    public partial class LogRouter : BaseModule
    {
        /// <inheritdoc/>
        public void Log(string logCategory, LogLevel level, string message)
        {
            if (level < Config.MinLogLevel) return;

            var log = new LogData(logCategory, level, message);

            // don't want to process unity logs 2x
            Application.logMessageReceived -= _onUnityLogReceivedHandler;

            foreach (var logProcessor in _logProcessors)
            {
                logProcessor.ProcessLog(log);
            }

            Application.logMessageReceived += _onUnityLogReceivedHandler;
        }

        /// <inheritdoc/>
        public void LogException(string logCategory, Exception exception, string message)
        {
            var log = new LogData(logCategory, exception, message);

            // don't want to process unity logs 2x
            Application.logMessageReceived -= _onUnityLogReceivedHandler;

            foreach (var logProcessor in _logProcessors)
            {
                logProcessor.ProcessLog(log);
            }

            Application.logMessageReceived += _onUnityLogReceivedHandler;
        }

        #region log aliases

        /// <inheritdoc/>
        public void LogDebug(string message)
        {
            LogDebug(string.Empty, message);
        }

        /// <inheritdoc/>
        public void LogDebug(string logCategory, string message)
        {
            Log(logCategory, LogLevel.Debug, message);
        }

        /// <inheritdoc/>
        public void LogInfo(string message)
        {
            LogInfo(string.Empty, message);
        }

        /// <inheritdoc/>
        public void LogInfo(string logCategory, string message)
        {
            Log(logCategory, LogLevel.Info, message);
        }

        /// <inheritdoc/>
        public void LogWarning(string message)
        {
            LogWarning(string.Empty, message);
        }

        /// <inheritdoc/>
        public void LogWarning(string logCategory, string message)
        {
            Log(logCategory, LogLevel.Warning, message);
        }

        /// <inheritdoc/>
        public void LogError(string message)
        {
            LogError(string.Empty, message);
        }

        /// <inheritdoc/>
        public void LogError(string logCategory, string message)
        {
            Log(logCategory, LogLevel.Error, message);
        }

        /// <inheritdoc/>
        public void LogException(Exception exception)
        {
            LogException(string.Empty, exception, string.Empty);
        }

        /// <inheritdoc/>
        public void LogException(string logCategory, Exception exception)
        {
            LogException(logCategory, exception, string.Empty);
        }

        /// <inheritdoc/>
        public void LogException(Exception exception, string message)
        {
            LogException(string.Empty, exception, message);
        }

        #endregion
    }
}