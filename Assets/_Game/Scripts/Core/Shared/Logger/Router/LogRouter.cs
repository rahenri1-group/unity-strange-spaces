﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core
{
    /// <inheritdoc cref="ILogRouter" />
    [Dependency(
        contract:typeof(ILogRouter),
        lifetime:Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class LogRouter : BaseModule, ILogRouter
    {
        /// <inheritdoc/>
        public override string ModuleName => "Log Router";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public LogRouterConfig Config = new LogRouterConfig();

        private readonly Application.LogCallback _onUnityLogReceivedHandler;

        private HashSet<ILogProcessor> _logProcessors;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public LogRouter(ILogProcessor[] logProcessors)
        : base (null)
        {
            Logger = this;

            _onUnityLogReceivedHandler = OnUnityLogReceived;
            Application.logMessageReceived += _onUnityLogReceivedHandler;

            _logProcessors = new HashSet<ILogProcessor>();
            foreach (var processor in logProcessors)
            {
                RegisterLogProcessor(processor);
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~LogRouter()
        {
            Application.logMessageReceived -= _onUnityLogReceivedHandler;
        }

        private void OnUnityLogReceived(string logString, string stackTrace, LogType unityLogType)
        {
            switch (unityLogType)
            {
                case LogType.Exception:
                case LogType.Error:
                case LogType.Assert:
                    Log("Unity", LogLevel.Error, logString);
                    break;
                case LogType.Warning:
                    Log("Unity", LogLevel.Warning, logString);
                    break;
                default:
                    Log("Unity", LogLevel.Info, logString);
                    break;
            }
        }

        private void RegisterLogProcessor(ILogProcessor processor)
        {
            _logProcessors.Add(processor);
        }
    }
}
