﻿namespace Game.Core
{ 
    /// <summary>
    /// The config for <see cref="LogRouter"/>
    /// </summary>
    public class LogRouterConfig : ModuleConfig
    {
        /// <summary>
        /// The current minimum log level
        /// </summary>
        public LogLevel MinLogLevel = LogLevel.Debug;
    }
}
