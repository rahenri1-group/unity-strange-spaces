﻿using System;
using UnityEngine;

namespace Game.Core.Math
{
    /// <summary>
    /// A structure the contains the definition of a cone shape
    /// </summary>
    public struct Cone : IEquatable<Cone>
    {
        /// <summary>
        /// The origin of the cone
        /// </summary>
        public Vector3 Origin { get; set; }

        /// <summary>
        /// The direction of the cone
        /// </summary>
        public Vector3 Direction { get; set; }

        /// <summary>
        /// The angle of the cone in degrees from edge to edge
        /// </summary>
        public float Angle { get; set; }

        /// <summary>
        /// The distance from <see cref="Origin"/> that the cone extends
        /// </summary>
        public float MaxDistance { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Cone(Vector3 origin, Vector3 direction, float angle, float maxDistance)
        {
            Origin = origin;
            Direction = direction;
            Angle = angle;
            MaxDistance = maxDistance;
        }

        /// <summary>
        /// Tests if a point is within the cone
        /// </summary>
        public bool IsPointInCone(Vector3 point)
        {
            var distance = Vector3.Distance(point, Origin);
            if (distance <= MaxDistance)
            {
                var pointDirection = point - Origin;
                var angle = Vector3.Angle(Direction, pointDirection);
                return angle < (0.5f * Angle);
            }

            return false;
        }

        /// <summary>
        /// Returns the closest direction vector within the cone to the provided <paramref name="point"/>.
        /// </summary>
        public Vector3 ClosestDirectionToPoint(Vector3 point)
        {
            var angleRadian = Mathf.Deg2Rad * 0.5f * Angle;

            return Vector3.RotateTowards(Direction, (point - Origin).normalized, angleRadian, 0f);
        }

        public override bool Equals(object obj) => obj is Cone other && this.Equals(other);

        public bool Equals(Cone other)
        {
            return Origin == other.Origin
                && Direction == other.Direction
                && MathUtil.ApproximatelyEqual(Angle, other.Angle)
                && MathUtil.ApproximatelyEqual(MaxDistance, other.MaxDistance);
        }

        public override int GetHashCode()
        {
            return
            (
                Origin,
                Direction,
                Angle,
                MaxDistance
            ).GetHashCode();
        }

        public static bool operator ==(Cone lhs, Cone rhs) => lhs.Equals(rhs);

        public static bool operator !=(Cone lhs, Cone rhs) => !(lhs == rhs);
    }
}
