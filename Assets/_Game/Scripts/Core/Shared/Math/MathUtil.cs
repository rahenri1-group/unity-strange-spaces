﻿using UnityEngine;

namespace Game.Core.Math
{
    /// <summary>
    /// Static class of math utilities
    /// </summary>
    public static class MathUtil
    {
        /// <summary>
        /// Reverses a <see cref="Quaternion"/> on the Y axis
        /// </summary>
        public static Quaternion ReverseAxisY => Quaternion.Euler(0f, 180f, 0f);

        /// <summary>
        /// Normalizes an angle so that it is between -180 and 180
        /// </summary>
        public static float NormalizeAngle180(float angle)
        {
            return OverflowValue(angle, -180f, 180f);
        }

        /// <summary>
        /// Normalizes an angle so that it is between 0 and 360
        /// </summary>
        public static float NormalizeAngle360(float angle)
        {
            return OverflowValue(angle, 0f, 360f);
        }

        /// <summary>
        /// Wraps a value so that it is between <paramref name="min"/> and <paramref name="max"/>.
        /// </summary>
        public static float OverflowValue(float value, float min, float max)
        {
            float width = max - min;
            float offsetValue = value - min;

            return (offsetValue - (Mathf.Floor(offsetValue / width) * width)) + min;
        }

        /// <summary>
        /// Returns true if <paramref name="a"/> and <paramref name="b"/> are mostly equal. Useful for floating point math.
        /// </summary>
        public static bool ApproximatelyEqual(float a, float b, float fudge = 0.0001f)
        {
            return Mathf.Abs(a - b) <= fudge;
        }

        /// <summary>
        /// Distance from <paramref name="point"/> to the closest point on <paramref name="ray"/>.
        /// </summary>
        public static float DistanceToLine(Ray ray, Vector3 point)
        {
            return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
        }

        /// <summary>
        /// Returns true if the <paramref name="bounds"/> intersects or contains the provided <paramref name="bounds"/>
        /// </summary>
        public static bool DoesConeIntersectBounds(Bounds bounds, Cone cone)
        {
            var closestDirectionInCone = cone.ClosestDirectionToPoint(bounds.center);

            if (bounds.IntersectRay(new Ray(cone.Origin, closestDirectionInCone), out float distance))
            {
                return distance <= cone.MaxDistance;
            }

            return false;
        }

        /// <summary>
        /// Finds the closes point on a line segment to the provided <paramref name="point"/>
        /// </summary>
        public static Vector3 ClosestPointOnLineSegment(Vector3 point, Vector3 segmentStart, Vector3 segmentEnd)
        {
            Vector3 heading = (segmentEnd - segmentStart);
            float magnitudeMax = heading.magnitude;
            heading.Normalize();

            Vector3 lhs = point - segmentStart;
            float dotP = Vector3.Dot(lhs, heading);
            dotP = Mathf.Clamp(dotP, 0f, magnitudeMax);
            return segmentStart + heading * dotP;
        }
    }
}