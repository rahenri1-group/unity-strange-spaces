﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core
{
    /// <inheritdoc cref="IModule"/>
    public abstract class BaseModule : IModule
    {
        /// <inheritdoc/>
        public abstract string ModuleName { get; }

        /// <inheritdoc/>
        public abstract ModuleConfig ModuleConfig { get; }

        /// <inheritdoc/>
        public bool ModuleInitialized { get; private set; } = false;

        /// <summary>
        /// The <see cref="ILogRouter"/>
        /// </summary>
        protected ILogRouter Logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public BaseModule(ILogRouter logger)
        {
            Logger = logger;
        }

        /// <inheritdoc/>
        public virtual UniTask Initialize()
        {
            ModuleInitialized = true;

            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public virtual UniTask Shutdown()
        {
            ModuleInitialized = false;

            return UniTask.CompletedTask;
        }

        /// <summary>
        /// Logs <paramref name="message"/> to <see cref="Logger"/> at <see cref="LogLevel.Info"/>.
        /// </summary>
        protected void ModuleLogInfo(string message)
        {
            Logger.LogInfo(ModuleName, message);
        }

        /// <summary>
        /// Logs <paramref name="message"/> to <see cref="Logger"/> at <see cref="LogLevel.Debug"/>.
        /// </summary>
        protected void ModuleLogDebug(string message)
        {
            Logger.LogDebug(ModuleName, message);
        }

        /// <summary>
        /// Logs <paramref name="message"/> to <see cref="Logger"/> at <see cref="LogLevel.Warning"/>.
        /// </summary>
        protected void ModuleLogWarning(string message)
        {
            Logger.LogWarning(ModuleName, message);
        }

        /// <summary>
        /// Logs <paramref name="message"/> to <see cref="Logger"/> at <see cref="LogLevel.Error"/>.
        /// </summary>
        protected void ModuleLogError(string message)
        {
            Logger.LogError(ModuleName, message);
        }

        /// <summary>
        /// Logs <paramref name="exception"/> to <see cref="Logger"/> at <see cref="LogLevel.Exception"/>.
        /// </summary>
        protected void ModuleLogException(Exception exception)
        {
            Logger.LogException(ModuleName, exception);
        }

        /// <summary>
        /// Logs <paramref name="message"/> and <paramref name="exception"/> to <see cref="Logger"/> at <see cref="LogLevel.Exception"/>.
        /// </summary>
        protected void ModuleLogException(Exception exception, string message)
        {
            Logger.LogException(ModuleName, exception, message);
        }
    }
}
