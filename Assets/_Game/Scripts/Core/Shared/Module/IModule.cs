﻿using Cysharp.Threading.Tasks;

namespace Game.Core
{
    /// <summary>
    /// The interface for all game modules.
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// The name of the module.
        /// </summary>
        string ModuleName { get; }

        /// <summary>
        /// The config file for the module.
        /// </summary>
        ModuleConfig ModuleConfig { get; }

        /// <summary>
        /// Has the module successfully completed initialization.
        /// </summary>
        bool ModuleInitialized { get; }

        /// <summary>
        /// The entry point for module initialization.
        /// </summary>
        UniTask Initialize();

        /// <summary>
        /// The entry point for module shutdown. All resources should be released at this step.
        /// </summary>
        UniTask Shutdown();
    }
}
