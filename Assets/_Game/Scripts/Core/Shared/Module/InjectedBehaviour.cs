﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Base class for all <see cref="MonoBehaviour"/>s that use the <see cref="InjectAttribute"/>.
    /// </summary>
    public class InjectedBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Has injection completed
        /// </summary>
        protected bool InjectionComplete { get; private set; } = false;

        /// <inheritdoc/>
        protected virtual void Awake()
        {
            Inject();
        }

        /// <summary>
        /// Resolves all <see cref="InjectAttribute"/>s. 
        /// Called automatically in Awake but will need to be manually called if the object is not active.
        /// </summary>
        protected void Inject()
        {
            if (InjectionComplete) return;

            ModuleContext.DiContainer.Inject(this);
            InjectionComplete = true;
        }
    }
}
