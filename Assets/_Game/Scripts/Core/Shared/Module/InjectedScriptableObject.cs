﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Base class for all <see cref="ScriptableObject"/>s that use the <see cref="InjectAttribute"/>.
    /// </summary>
    public class InjectedScriptableObject : ScriptableObject
    {
        /// <summary>
        /// The injection container
        /// </summary>
        protected IInjectionContainer Container { get; private set; }

        [System.NonSerialized]
        private bool _hasbeenInitialized = false;

        /// <summary>
        /// Injects the object and runs <see cref="Initialize"/> exactly once
        /// </summary>
        protected void InitializeIfNecessary()
        {
            if (_hasbeenInitialized) return;

            _hasbeenInitialized = true;

            Container = ModuleContext.DiContainer;
            Container.Inject(this);

            Initialize();
        }

        protected virtual void Initialize() { }
    }
}
