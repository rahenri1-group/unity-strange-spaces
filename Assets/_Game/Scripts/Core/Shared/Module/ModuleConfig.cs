﻿using System;

namespace Game.Core
{
    /// <summary>
    /// The config for a <see cref="IModule"/>
    /// </summary>
    [Serializable]
    public class ModuleConfig
    {
        /// <summary>
        /// The relative order of initialization of a module. Lower values initialize first.
        /// </summary>
        public int InitializeOrder = 0;

        /// <summary>
        /// The relative order of shutdown of a module. Lower values shutdown first.
        /// </summary>
        public int ShutdownOrder = 0;
    }
}