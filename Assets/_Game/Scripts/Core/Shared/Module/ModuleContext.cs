﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using System;
using System.Linq;

namespace Game.Core
{
    /// <summary>
    /// Holds the main injection container.
    /// </summary>
    public static class ModuleContext
    {
        private const string LogCategory = "Initialization";

        private static IInjectionContainer _container = null;

        /// <summary>
        /// The injection context
        /// </summary>
        public static IInjectionContainer DiContainer
        {
            get => _container;
            set
            {
                if (_container != null)
                {
                    _logger.LogError(LogCategory, $"There was an attempt to set {typeof(ModuleContext).Name}.DiContainer after it has already been set!");
                    return;
                }
                _container = value;
            }
        }

        /// <summary>
        /// Has <see cref="IModule"/> initialization completed
        /// </summary>
        public static bool ModulesInitialized { get; private set; } = false;

        private static IEventBus _eventBus = null;
        private static ILogRouter _logger = null;
        private static IModule[] _modules = null;

        /// <summary>
        /// Initializes all <see cref="IModule"/>s
        /// </summary>
        public static async UniTask InitializeModules()
        {
            ModulesInitialized = false;

            _logger = DiContainer.Resolve<ILogRouter>();
            _modules = DiContainer.Resolve<IModule[]>();
            _eventBus = DiContainer.Resolve<IEventBus>();

            var sortedModules = _modules.OrderBy(f => f.ModuleConfig.InitializeOrder);
            foreach (var module in sortedModules)
            {
                try
                {
                    if (!module.ModuleInitialized)
                    {
                        await module.Initialize();

                        _logger.LogInfo(LogCategory, $"Initialized {module.ModuleName}");
                    }
                }
                catch (Exception e)
                {
                    _logger.LogException(LogCategory, e, $"Failed to initialize {module.ModuleName}");
                }
            }

            ModulesInitialized = true;

            _eventBus.InvokeEvent(new ModulesInitializedEvent());
        }

        /// <summary>
        /// Shutsdown all <see cref="IModule"/>s.
        /// </summary>
        public static async UniTask ShutdownModules()
        {
            _eventBus.InvokeEvent(new ModulesShutdownEvent());

            var sortedModules = _modules.OrderBy(f => f.ModuleConfig.ShutdownOrder);
            foreach (var module in sortedModules)
            {
                try
                {
                    if (module.ModuleInitialized)
                        await module.Shutdown();
                }
                catch (Exception e)
                {
                    _logger.LogException(LogCategory, e, $"Failed to shutdown {module.ModuleName}");
                }
            }

            ModulesInitialized = false;
        }
    }
}
