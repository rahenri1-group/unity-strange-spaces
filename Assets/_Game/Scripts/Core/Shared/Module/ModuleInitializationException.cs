﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Exception for <see cref="IModule"/> initialization
    /// </summary>
    public class ModuleInitializationException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ModuleInitializationException(string message)
            : base(message) { }
    }
}