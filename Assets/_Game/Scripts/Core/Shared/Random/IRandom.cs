﻿namespace Game.Core
{
     /// <summary>
     /// Interface for generating random data
     /// </summary>
    public interface IRandom
    {
        /// <summary>
        /// Returns a random <paramref name="minInclusive"/> &lt;= int &lt; <paramref name="maxExclusive"/>
        /// </summary>
        int Range(int minInclusive, int maxExclusive);

        /// <summary>
        /// Returns a random <paramref name="minInclusive"/> &lt;= float &lt; <paramref name="maxExclusive"/>
        /// </summary>
        float Range(float minInclusive, float maxExclusive);
    }
}
