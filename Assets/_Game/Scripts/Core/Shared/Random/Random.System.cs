﻿namespace Game.Core
{
    public static partial class Random
    {
        /// <summary>
        /// Generates random numbers using Microsoft's implementation of Random
        /// </summary>
        private class SystemRandom : IRandom
        {
            private System.Random _systemRandom;

            /// <summary>
            /// Constructor
            /// </summary>
            public SystemRandom(int seed)
            {
                _systemRandom = new System.Random(seed);
            }

            /// <inheritdoc/>
            public int Range(int minInclusive, int maxExclusive)
            {
                return _systemRandom.Next(minInclusive, maxExclusive);
            }

            /// <inheritdoc/>
            public float Range(float minInclusive, float maxExclusive)
            {
                float val = (float)_systemRandom.NextDouble();
                return val * (maxExclusive - minInclusive) + minInclusive;
            }
        }
    }
}
