﻿namespace Game.Core
{
    public static partial class Random
    {
        /// <summary>
        /// Generates random numbers using Unity's implementation of Random
        /// </summary>
        private class UnityRandom : IRandom
        {
            /// <inheritdoc/>
            public int Range(int minInclusive, int maxExclusive)
            {
                return UnityEngine.Random.Range(minInclusive, maxExclusive);
            }

            /// <inheritdoc/>
            public float Range(float minInclusive, float maxExclusive)
            {
                return UnityEngine.Random.Range(minInclusive, maxExclusive);
            }
        }
    }
}
