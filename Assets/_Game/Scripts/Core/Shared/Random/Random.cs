﻿namespace Game.Core
{
    /// <summary>
    /// Utility for accessing random number generators
    /// </summary>
    public static partial class Random
    {
        /// <summary>
        /// The default random number generator
        /// </summary>
        public static IRandom Default
        {
            get
            {
                if (_default == null) _default = new UnityRandom();
                return _default;
            }
        }
        private static IRandom _default = null;

        /// <summary>
        /// Creates a new random number generator with a given seed
        /// </summary>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static IRandom Create(int seed)
        {
            return new SystemRandom(seed);
        }
    }
}
