﻿namespace Game.Core.Serialization
{
    /// <summary>
    /// Interface for serializable <see cref="IGameObjectComponent"/>s
    /// </summary>
    public interface ISerializableComponent : IGameObjectComponent
    {
        /// <summary>
        /// Should the component be serialized / deserialized
        /// </summary>
        bool SerializeComponent { get; }

        /// <summary>
        /// Serializes the component to json
        /// </summary>
        /// <returns></returns>
        string SerializeToJson();

        /// <summary>
        /// Applies the serialized json to the component
        /// </summary>
        /// <param name="json"></param>
        void DeserializeFromJson(string json);
    }
}
