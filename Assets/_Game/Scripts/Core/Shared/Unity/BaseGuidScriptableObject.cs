﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Base class for <see cref="ScriptableObject"/>s that have a unique <see cref="Guid"/> for an id.
    /// </summary>
    public abstract class BaseGuidScriptableObject : InjectedScriptableObject
    {
        private Guid _parsedId = Guid.Empty;
        /// <summary>
        /// The id of the object
        /// </summary>
        public Guid Id
        {
            get
            {
                if (_parsedId == Guid.Empty && !string.IsNullOrEmpty(_id)) _parsedId = Guid.Parse(_id);
                return _parsedId;
            }
        }

        [SerializeField] [ReadOnly] private string _id = string.Empty;

        /// <inheritdoc />
        public override int GetHashCode() => Id.GetHashCode();

        protected virtual void Awake()
        {
            EnsureIdNotEmpty();
        }

        /// <inheritdoc />
        protected virtual void OnValidate()
        {
            EnsureIdNotEmpty();
        }

        private void EnsureIdNotEmpty()
        {
            if (string.IsNullOrEmpty(_id))
            {
                _id = Guid.NewGuid().ToString();

#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(this);

                if (!UnityEditor.EditorApplication.isUpdating)
                {    
                    UnityEditor.AssetDatabase.SaveAssets();
                }
#endif
            }
        }
    }
}
