﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Prevents the <see cref="GameObject"/> from being destroyed on scene unload
    /// </summary>
    public class DontDestroyBehaviour : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
