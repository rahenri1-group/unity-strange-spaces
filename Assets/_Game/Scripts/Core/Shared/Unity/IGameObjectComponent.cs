﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Interface for a component on a <see cref="UnityEngine.GameObject"/>
    /// </summary>
    public interface IGameObjectComponent
    {
        /// <summary>
        /// The component <see cref="UnityEngine.GameObject"/>
        /// </summary>
        GameObject GameObject { get; }
    }
}
