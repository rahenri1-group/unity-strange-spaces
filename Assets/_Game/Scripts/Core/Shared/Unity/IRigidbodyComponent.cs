﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Interface for components that control a <see cref="UnityEngine.Rigidbody"/>
    /// </summary>
    public interface IRigidbodyComponent : IGameObjectComponent
    {
        /// <summary>
        /// The component <see cref="UnityEngine.Rigidbody"/>
        /// </summary>
        Rigidbody Body { get; }
    }
}
