﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Hides an object by fading in and out, also disables colliders
    /// </summary>
    public class FadeColliderObjectHiderBehaviour : FadeObjectHiderBehaviour
    {
        private Collider[] _colliders;

        /// <inheritdoc/>
        protected override void Awake()
        {
            _colliders = GetComponentsInChildren<Collider>();

            base.Awake();
        }

        /// <inheritdoc/>
        public override void ShowImmediate()
        {
            base.ShowImmediate();

            SetCollidersEnabled(true);
        }

        /// <inheritdoc/>
        public override UniTask ShowAnimated()
        {
            SetCollidersEnabled(true);

            return base.ShowAnimated();
        }

        /// <inheritdoc/>
        public override void HideImmediate()
        {
            base.HideImmediate();

            SetCollidersEnabled(false);
        }

        /// <inheritdoc/>
        public override async UniTask HideAnimated()
        {
            await base.HideAnimated();

            SetCollidersEnabled(false);
        }

        private void SetCollidersEnabled(bool collidersEnabled)
        {
            foreach (var collider in _colliders)
            {
                collider.enabled = collidersEnabled;
            }
        }
    }
}
