﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core
{
    /// <summary>
    /// Hides an object by fading in and out
    /// </summary>
    public class FadeObjectHiderBehaviour : MonoBehaviour, IObjectHider
    {
        [SerializeField] private BoolReadonlyReference _hideOnAwake = null;
        [SerializeField] private FloatReadonlyReference _fadeInDuration = null;
        [SerializeField] private FloatReadonlyReference _fadeOutDuration = null;

        private Renderer[] _renderers;

        /// <inheritdoc/>
        protected virtual void Awake()
        {
            Assert.IsTrue(_fadeInDuration > 0f);
            Assert.IsTrue(_fadeOutDuration > 0f);

            _renderers = GetComponentsInChildren<Renderer>();

            Assert.IsTrue(_renderers.Length > 0f);

            if (_hideOnAwake)
            {
                HideImmediate();
            }
        }

        /// <inheritdoc/>
        public virtual void ShowImmediate()
        {
            foreach (var renderer in _renderers)
            {
                renderer.enabled = true;
            }

            SetRendererAlpha(1f);
        }

        /// <inheritdoc/>
        public virtual async UniTask ShowAnimated()
        {
            foreach (var renderer in _renderers)
            {
                renderer.enabled = true;
            }

            await Fade(1f, _fadeInDuration);
        }

        /// <inheritdoc/>
        public virtual void HideImmediate()
        {
            SetRendererAlpha(0f);

            foreach(var renderer in _renderers)
            {
                renderer.enabled = false;
            }
        }

        /// <inheritdoc/>
        public virtual async UniTask HideAnimated()
        {
            await Fade(0f, _fadeOutDuration);

            foreach(var renderer in _renderers)
            {
                renderer.enabled = false;
            }
        }

        private async UniTask Fade(float targetAlpha, float duration)
        {
            float startAlpha = _renderers[0].material.color.a;
            
            float startTime = Time.time;
            while (Time.time - startTime <= duration && gameObject.activeInHierarchy)
            {
                await UniTask.Yield();

                float lerp = Mathf.Clamp01((Time.time - startTime) / duration);

                SetRendererAlpha( Mathf.SmoothStep(startAlpha, targetAlpha, lerp));
            }

            SetRendererAlpha(targetAlpha);
        }

        private void SetRendererAlpha(float alpha)
        {
            foreach (var renderer in _renderers)
            {
                foreach (var mat in renderer.materials)
                {
                    var color = mat.color;
                    color.a = alpha;
                    mat.color = color;
                }
            }
        }
    }
}
