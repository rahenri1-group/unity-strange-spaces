﻿using Cysharp.Threading.Tasks;

namespace Game.Core
{
    /// <summary>
    /// Utility for showing / hiding an object
    /// </summary>
    public interface IObjectHider
    {
        /// <summary>
        /// Immediatetly shows the object
        /// </summary>
        void ShowImmediate();

        /// <summary>
        /// Shows the object over multiple frames
        /// </summary>
        /// <returns></returns>
        UniTask ShowAnimated();

        /// <summary>
        /// Immediatetly hides the object
        /// </summary>
        void HideImmediate();

        /// <summary>
        /// Hides the object over multiple frames
        /// </summary>
        /// <returns></returns>
        UniTask HideAnimated();
    }
}
