﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Utility class for <see cref="Gizmos"/>
    /// </summary>
    public static class GizmosUtil
    {
        /// <summary>
        /// Draws a circle
        /// </summary>
        public static void DrawCircle(Vector3 position, float radius)
        {
            Vector3[] points = new Vector3[16];
            for (int i = 0; i < points.Length; i++)
            {
                float angle = (2f * Mathf.PI * i / points.Length);
                points[i] = radius * new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle));
            }

            for (int i = 0; i < points.Length; i++)
            {
                int a = i;
                int b = (i + 1) % points.Length;

                Gizmos.DrawLine(position + points[a], position + points[b]);
            }
        }

        /// <summary>
        /// Draws a cylinder of rings
        /// </summary>
        public static void DrawCylinder(Vector3 bottomPosition, float radius, float height)
        {
            int ringCount = Mathf.Max((int)(3f * height / radius) + 1, 3);

            for (int i = 0; i < ringCount; i++)
            {
                DrawCircle(bottomPosition + height * ((float)i / (ringCount - 1)) * Vector3.up, radius);
            }
        }

        /// <summary>
        /// Draws a cone
        /// </summary>
        public static void DrawCone(Vector3 coneOrigin, Vector3 coneDirection, float coneAngle, float maxDistance)
        {
            Quaternion forwardRotation = Quaternion.LookRotation(coneDirection, Vector3.up);

            Vector3[] points = new Vector3[16];
            for (int i = 0; i < points.Length; i++)
            {
                float circleAngle = (2f * Mathf.PI * i / points.Length);

                float x = Mathf.Sin(circleAngle) * (maxDistance * Mathf.Tan(0.5f * coneAngle * Mathf.Deg2Rad));
                float y = Mathf.Cos(circleAngle) * (maxDistance * Mathf.Tan(0.5f * coneAngle * Mathf.Deg2Rad));
                float z = maxDistance;
                Vector3 basePoint = forwardRotation * new Vector3(x, y, z);

                points[i] = coneOrigin + basePoint;
            }

            Vector3 coneBaseCenter = coneOrigin + coneDirection * maxDistance;

            Gizmos.DrawLine(coneOrigin, coneBaseCenter);

            foreach (var point in points)
            {
                Gizmos.DrawLine(coneOrigin, point);
                Gizmos.DrawLine(coneBaseCenter, point);
            }

            for (int i = 0; i < points.Length; i++)
            {
                int a = i;
                int b = (i + 1) % points.Length;

                Gizmos.DrawLine(points[a], points[b]);
            }
        }
    }
}
