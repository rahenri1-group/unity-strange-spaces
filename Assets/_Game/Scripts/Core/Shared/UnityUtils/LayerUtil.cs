﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Exception class for Unity layers
    /// </summary>
    public class LayerException : Exception
    {
        public LayerException(string message)
            : base(message) { }
    }

    /// <summary>
    /// Static utility class for layers
    /// </summary>
    public static class LayerUtil
    {
        private static Dictionary<string, int> _layerNameMaskCache = new Dictionary<string, int>();
        
        /// <summary>
        /// Converts a layer name to a layer id
        /// </summary>
        public static int GetLayerId(string layerName)
        {
            if (!_layerNameMaskCache.ContainsKey(layerName))
            {
                int layer = LayerMask.NameToLayer(layerName);

                if (layer == -1) throw new LayerException($"Layer '{layerName}' not found.");

                _layerNameMaskCache[layerName] = layer;
            }

            return _layerNameMaskCache[layerName];
        }

        /// <summary>
        /// Gets the layer name for a layer id
        /// </summary>
        public static string GetLayerName(int layerId)
        {
            return LayerMask.LayerToName(layerId);
        }

        /// <summary>
        /// Constructs a mask for the provided layer ids
        /// </summary>
        public static int ConstructMaskForLayers(int[] layerIds)
        {
            int layerMask = 0;
            foreach (var layer in layerIds)
            {
                layerMask = IncludeLayerInMask(layerMask, layer);
            }

            return layerMask;
        }

        /// <summary>
        /// Constructs a mask for the provided layer names
        /// </summary>
        public static int ConstructMaskForLayers(string[] layers)
        {
            int[] layerIds = new int[layers.Length];
            for (int i = 0; i < layers.Length; i++)
            {
                layerIds[i] = GetLayerId(layers[i]);
            }

            return ConstructMaskForLayers(layerIds);
        }

        /// <summary>
        /// Adds a layer to a layer mask
        /// </summary>
        public static int IncludeLayerInMask(int mask, int layerId)
        {
            return mask | (1 << layerId); ;
        }
    }
}