﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Static utility class for Unity physics
    /// </summary>
    public static class PhysicsUtil
    {
        /// <summary>
        /// Copies a <see cref="SphereCollider"/>
        /// </summary>
        /// <param name="originalCollider"></param>
        /// <param name="copyCollider"></param>
        /// <returns></returns>
        public static SphereCollider CopySphereCollider(SphereCollider originalCollider, SphereCollider copyCollider)
        {
            copyCollider.center = originalCollider.center;
            copyCollider.radius = originalCollider.radius;
            return copyCollider;
        }

        /// <summary>
        /// Copies a <see cref="BoxCollider"/>
        /// </summary>
        /// <param name="originalCollider"></param>
        /// <param name="copyCollider"></param>
        /// <returns></returns>
        public static BoxCollider CopyBoxCollider(BoxCollider originalCollider, BoxCollider copyCollider)
        {
            copyCollider.center = originalCollider.center;
            copyCollider.size = originalCollider.size;

            return copyCollider;
        }

        /// <summary>
        /// Copies a <see cref="CapsuleCollider"/>
        /// </summary>
        /// <param name="originalCollider"></param>
        /// <param name="copyCollider"></param>
        /// <returns></returns>
        public static CapsuleCollider CopyCapsuleCollider(CapsuleCollider originalCollider, CapsuleCollider copyCollider)
        {
            copyCollider.center = originalCollider.center;
            copyCollider.direction = originalCollider.direction;
            copyCollider.height = originalCollider.height;
            copyCollider.radius = originalCollider.radius;

            return copyCollider;
        }

        /// <summary>
        /// Copies a <see cref="MeshCollider"/>
        /// </summary>
        /// <param name="originalCollider"></param>
        /// <param name="copyCollider"></param>
        /// <returns></returns>
        public static MeshCollider CopyMeshCollider(MeshCollider originalCollider, MeshCollider copyCollider)
        {
            copyCollider.sharedMesh = originalCollider.sharedMesh;
            copyCollider.convex = originalCollider.convex;
            copyCollider.cookingOptions = originalCollider.cookingOptions;

            return copyCollider;
        }

        /// <summary>
        /// Copies a <see cref="CharacterController"/> as a <see cref="CapsuleCollider"/>
        /// </summary>
        /// <param name="originalCollider"></param>
        /// <param name="copyCollider"></param>
        /// <returns></returns>
        public static CapsuleCollider CopyCharacterControllerAsCapsuleCollider(CharacterController originalCollider, CapsuleCollider copyCollider)
        {
            copyCollider.center = originalCollider.center;
            copyCollider.direction = 1; // y
            copyCollider.height = originalCollider.height;
            copyCollider.radius = originalCollider.radius;

            return copyCollider;
        }
    }
}
