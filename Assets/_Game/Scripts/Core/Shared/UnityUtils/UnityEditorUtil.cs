﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Static utility class for the Unity Editor
    /// </summary>
    public static class UnityEditorUtil 
    {
#if UNITY_EDITOR
        /// <summary>
        /// Generates a <see cref="Guid"/> for a <see cref="MonoBehaviour"/>
        /// </summary>
        /// <param name="monoBehaviour"></param>
        /// <param name="savedId"></param>
        /// <param name="generatedId"></param>
        public static void GenerateGuidForBehaviour(MonoBehaviour monoBehaviour, string savedId, out string generatedId)
        {
            generatedId = savedId;

            var gameObject = monoBehaviour.gameObject;
            var gameObjectStage = UnityEditor.SceneManagement.StageUtility.GetStage(gameObject);

            if (!gameObject.scene.IsValid() || gameObjectStage == null) // viewed in inspector
            {
                if (!string.IsNullOrEmpty(generatedId))
                {
                    generatedId = string.Empty;
                    UnityEditor.EditorUtility.SetDirty(monoBehaviour);
                    UnityEditor.AssetDatabase.SaveAssets();
                }
            }
            else if (gameObjectStage.GetType() == typeof(UnityEditor.SceneManagement.PrefabStage))
            {
                generatedId = string.Empty;
            }
            else
            {
                if (string.IsNullOrEmpty(generatedId))
                {
                    generatedId = Guid.NewGuid().ToString();
                    UnityEditor.Undo.RecordObject(monoBehaviour, "Created id");
                }
            }
        }
#endif
    }
}
