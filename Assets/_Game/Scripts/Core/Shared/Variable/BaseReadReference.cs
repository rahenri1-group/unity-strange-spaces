﻿namespace Game.Core
{
    /// <summary>
    /// A readonly version of <see cref="BaseReference{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseReadReference<T> : BaseReference<T>
    {
        /// <summary>
        /// The referenced value
        /// </summary>
        public T Value => InternalValue;

        /// <inheritdoc/>
        public BaseReadReference()
            : base() { }
    }
}
