﻿namespace Game.Core
{
    /// <summary>
    /// A read and write version of <see cref="BaseReference{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseReadWriteReference<T> : BaseReference<T>
    {
        /// <summary>
        /// The referenced value
        /// </summary>
        public T Value
        {
            get => InternalValue;
            set => InternalValue = value;
        }

        /// <inheritdoc/>
        public BaseReadWriteReference()
            : base() { }
    }
}
