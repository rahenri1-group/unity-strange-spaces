﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Class that references a typed value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseReference<T>
    {
        [SerializeField] private bool _useInline = true;

        [SerializeField] private T _inlineValue;

        protected abstract BaseVariable<T> Variable { get; }

        // for inlineChangeEvent comparision
        private T _inlineValueCopy;

        private event Action _inlineChanged;

        protected T InternalValue
        {
            get => _useInline ? _inlineValue : Variable.Value;
            set
            {
                if (_useInline)
                {
                    _inlineValue = value;

                    OnInlineAssigned();
                }
                else
                {
                    Variable.Value = value;
                }
            }
        }

        /// <summary>
        /// Action called when the referenced value changes
        /// </summary>
        public event Action ValueChanged
        {
            add
            {
                if (_useInline)
                    _inlineChanged += value;
                else
                    Variable.VariableChanged += value;
            }
            remove
            {
                if (_useInline)
                    _inlineChanged -= value;
                else
                    Variable.VariableChanged -= value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseReference() { }

        private void OnValidate()
        {
            OnInlineAssigned();
        }

        private void OnInlineAssigned()
        {
            if (EqualityComparer<T>.Default.Equals(_inlineValue, _inlineValueCopy) == false)
            {
                _inlineChanged?.Invoke();
            }
            _inlineValueCopy = _inlineValue;
        }
    }
}
