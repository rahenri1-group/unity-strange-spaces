﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core
{
    /// <inheritdoc cref="IVariable{T}"/>
    public abstract class BaseVariable<T> : BaseGuidScriptableObject, IVariable<T>
    {
        /// <inheritdoc/>
        public event Action VariableChanged;

        /// <inheritdoc/>
        public string Name => name;

        /// <inheritdoc/>
        public Type VariableType => typeof(T);

        /// <inheritdoc/>
        public T Value
        {
            get => _value;
            set
            {
                if (_isConstant)
                {
                    Debug.LogWarning("Attempt to write to constant: " + name);
                }
                else
                {
                    this._value = value;

                    OnVariableAssigned();
                }
            }
        }

        /// <inheritdoc/>
        public string StringValue
        {
            get => Value.ToString();
        }

        [SerializeField] private bool _isConstant = false;

        [SerializeField] private bool _triggerEvents = false;

        [SerializeField] private T _value;

        // for variableChangeEvent comparision
        private T _valueCopy;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _valueCopy = _value;
        }

        /// <inheritdoc/>
        protected override void OnValidate()
        {
            base.OnValidate();

            OnVariableAssigned();
        }

        private void OnVariableAssigned()
        {
            if (!_triggerEvents)
            {
                return;
            }

            if (EqualityComparer<T>.Default.Equals(_value, _valueCopy) == false)
            {
                VariableChanged?.Invoke();
            }

            _valueCopy = _value;
        }
    }
}
