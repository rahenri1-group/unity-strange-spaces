﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Interface for a variable
    /// </summary>
    public interface IVariable
    {
        /// <summary>
        /// The name of the variable
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The id of the variable
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The <see cref="Type"/> of the variable
        /// </summary>
        Type VariableType { get; }

        /// <summary>
        /// The string value of variable
        /// </summary>
        string StringValue { get; }

        /// <summary>
        /// Event called whenever the variable is changed.
        /// </summary>
        event Action VariableChanged;
    }

    /// <summary>
    /// A typed readonly version of <see cref="IVariable"/>
    /// </summary>
    public interface IReadonlyVariable<T> : IVariable 
    {
        T Value { get; }
    }

    /// <summary>
    /// A typed version of <see cref="IVariable"/>
    /// </summary>
    public interface IVariable<T> : IReadonlyVariable<T>
    {
        /// <summary>
        /// The value of the variable
        /// </summary>
        new T Value { get; set; }
    }
}
