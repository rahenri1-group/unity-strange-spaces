﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="bool"/> readonly reference
    /// </summary>
    [Serializable]
    public class BoolReadonlyReference : BaseReadReference<bool>
    {
        [SerializeField] private BoolVariable _variable = null;

        protected override BaseVariable<bool> Variable => _variable;

        public static implicit operator bool(BoolReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
