﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Hdr <see cref="Color"/> readonly reference
    /// </summary>
    [Serializable]
    public class ColorHdrReadonlyReference : BaseReadReference<Color>
    {
        [SerializeField] private ColorHdrVariable _variable = null;

        protected override BaseVariable<Color> Variable => _variable;

        public static implicit operator Color(ColorHdrReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
