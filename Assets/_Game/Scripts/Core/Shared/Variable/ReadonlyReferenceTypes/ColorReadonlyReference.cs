﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="Color"/> readonly reference
    /// </summary>
    [Serializable]
    public class ColorReadonlyReference : BaseReadReference<Color>
    {
        [SerializeField] private ColorVariable _variable = null;

        protected override BaseVariable<Color> Variable => _variable;

        public static implicit operator Color(ColorReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
