﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="float"/> readonly reference
    /// </summary>
    [Serializable]
    public class FloatReadonlyReference : BaseReadReference<float>
    {
        [SerializeField] private FloatVariable _variable = null;

        protected override BaseVariable<float> Variable => _variable;

        public static implicit operator float(FloatReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
