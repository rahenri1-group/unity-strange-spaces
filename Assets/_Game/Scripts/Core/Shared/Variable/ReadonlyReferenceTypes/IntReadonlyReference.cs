﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="int"/> readonly reference
    /// </summary>
    [Serializable]
    public class IntReadonlyReference : BaseReadReference<int>
    {
        [SerializeField] private IntVariable _variable = null;

        protected override BaseVariable<int> Variable => _variable;

        public static implicit operator int(IntReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
