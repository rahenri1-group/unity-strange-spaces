﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="string"/> readonly reference
    /// </summary>
    [Serializable]
    public class StringReadonlyReference : BaseReadReference<string>
    {
        [SerializeField] private StringVariable _variable = null;

        protected override BaseVariable<string> Variable => _variable;

        public static implicit operator string(StringReadonlyReference reference)
        {
            return reference.Value;
        }
    }
}
