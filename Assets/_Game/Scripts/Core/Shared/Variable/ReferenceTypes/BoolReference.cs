﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="bool"/> reference
    /// </summary>
    [Serializable]
    public class BoolReference : BaseReadWriteReference<bool>
    {
        [SerializeField] private BoolVariable _variable = null;

        protected override BaseVariable<bool> Variable => _variable;

        public static implicit operator bool(BoolReference reference)
        {
            return reference.Value;
        }
    }
}
