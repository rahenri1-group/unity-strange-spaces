﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// Hdr <see cref="Color"/> reference
    /// </summary>
    [Serializable]
    public class ColorHdrReference : BaseReadWriteReference<Color>
    {
        [SerializeField] private ColorHdrVariable _variable = null;

        protected override BaseVariable<Color> Variable => _variable;

        public static implicit operator Color(ColorHdrReference reference)
        {
            return reference.Value;
        }
    }
}
