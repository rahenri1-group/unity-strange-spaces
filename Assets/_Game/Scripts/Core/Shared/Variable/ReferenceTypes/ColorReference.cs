﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="Color"/> reference
    /// </summary>
    [Serializable]
    public class ColorReference : BaseReadWriteReference<Color>
    {
        [SerializeField] private ColorVariable _variable = null;

        protected override BaseVariable<Color> Variable => _variable;

        public static implicit operator Color(ColorReference reference)
        {
            return reference.Value;
        }
    }
}
