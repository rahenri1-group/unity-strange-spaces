﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="float"/> reference
    /// </summary>
    [Serializable]
    public class FloatReference : BaseReadWriteReference<float>
    {
        [SerializeField] private FloatVariable _variable = null;

        protected override BaseVariable<float> Variable => _variable;

        public static implicit operator float(FloatReference reference)
        {
            return reference.Value;
        }
    }
}
