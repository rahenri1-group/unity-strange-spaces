﻿using System;
using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="int"/> reference
    /// </summary>
    [Serializable]
    public class IntReference : BaseReadWriteReference<int>
    {
        [SerializeField] private IntVariable _variable = null;

        protected override BaseVariable<int> Variable => _variable;

        public static implicit operator int(IntReference reference)
        {
            return reference.Value;
        }
    }
}
