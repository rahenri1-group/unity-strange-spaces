﻿namespace Game.Core
{
    /// <summary>
    /// Contains a list of <see cref="IVariable"/>s for <see cref="IVariableRegistry"/> to register.
    /// This script should only be in the initial scene.
    /// </summary>
    public interface IVariableManifest
    {
        /// <summary>
        /// The variables that will be registered
        /// </summary>
        IVariable[] Variables { get; }
    }
}
