﻿using System;

namespace Game.Core
{
    /// <summary>
    /// Module for managing <see cref="IVariable"/>s
    /// </summary>
    public interface IVariableRegistry : IModule
    {
        /// <summary>
        /// Registers a <see cref="IVariable"/>
        /// </summary>
        void RegisterVariable(IVariable variable);
        /// <summary>
        /// Registers a <see cref="IVariable{T}"/>
        /// </summary>
        void RegisterVariable<T>(IVariable<T> variable);

        /// <summary>
        /// Gets an <see cref="IVariable"/> by its id
        /// </summary>
        IVariable GetVariableById(Guid variableId);
        /// <summary>
        /// Gets a <see cref="IVariable"/> by its object name
        /// </summary>
        IVariable GetVariableByName(string variableName);

        /// <summary>
        /// Gets a <see cref="IVariable{T}"/> by its id
        /// </summary>
        IVariable<T> GetVariableById<T>(Guid variableId);
        /// <summary>
        /// Gets a <see cref="IVariable{T}"/> by its object name
        /// </summary>
        IVariable<T> GetVariableByName<T>(string variableName);
    }
}
