﻿using Game.Core.DependencyInjection;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core
{
    /// <inheritdoc cref="IVariableManifest"/>
    public class VariableManifestBehaviour : MonoBehaviour, IVariableManifest
    {
        /// <inheritdoc />
        public IVariable[] Variables 
        { 
            get
            {
                if (_variables == null)
                {
                    var varList = new List<IVariable>();
                    foreach (var varObj in _variableObjs)
                    {
                        varList.Add((IVariable)varObj);
                    }
                    _variables = varList.ToArray();
                }

                return _variables;
            } 
        }
        private IVariable[] _variables = null;

        [SerializeField] [TypeRestriction(typeof(IVariable), false)] private Object[] _variableObjs = null;
    }
}
