﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Space;

namespace Game.Core
{
    /// <inheritdoc cref="IVariableRegistry"/>
    [Dependency(
        contract: typeof(IVariableRegistry),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class VariableRegistry : BaseModule, IVariableRegistry
    {
        /// <inheritdoc />
        public override string ModuleName => "Variable Registry";

        /// <inheritdoc />
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly ISpaceManager _spaceManager;

        private Dictionary<Guid, IVariable> _variablesById;
        private Dictionary<string, IVariable> _variablesByName;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public VariableRegistry(
            ISpaceManager spaceManager,
            ILogRouter logger)
            : base(logger)
        {
            _spaceManager = spaceManager;

            _variablesById = new Dictionary<Guid, IVariable>();
            _variablesByName = new Dictionary<string, IVariable>();
        }

        /// <inheritdoc />
        public override UniTask Initialize()
        {
            var manifests = _spaceManager.FindLoaded<IVariableManifest>();
            foreach (var manifest in manifests)
            {
                foreach (var variable in manifest.Variables)
                {
                    RegisterVariable(variable);
                }
            }

            return base.Initialize();
        }

        /// <inheritdoc />
        public override UniTask Shutdown()
        {
            _variablesById.Clear();
            _variablesByName.Clear();

            return base.Shutdown();
        }

        /// <inheritdoc />
        public void RegisterVariable(IVariable variable)
        {
            _variablesByName[variable.Name.ToLower()] = variable;
            _variablesById[variable.Id] = variable;
        }

        /// <inheritdoc />
        public void RegisterVariable<T>(IVariable<T> variable)
        {
            RegisterVariable(variable);
        }

        /// <inheritdoc />
        public IVariable GetVariableById(Guid variableId)
        {
            if (_variablesById.ContainsKey(variableId))
            {
                return _variablesById[variableId];
            }

            return null;
        }

        /// <inheritdoc />
        public IVariable GetVariableByName(string variableName)
        {
            variableName = variableName.ToLower();

            if (_variablesByName.ContainsKey(variableName))
            {
                return _variablesByName[variableName];
            }

            return null;
        }

        /// <inheritdoc />
        public IVariable<T> GetVariableById<T>(Guid variableId)
        {
            return GetVariableById(variableId) as IVariable<T>;
        }

        /// <inheritdoc />
        public IVariable<T> GetVariableByName<T>(string variableName)
        {
            return GetVariableByName(variableName) as IVariable<T>;
        }
    }
}
