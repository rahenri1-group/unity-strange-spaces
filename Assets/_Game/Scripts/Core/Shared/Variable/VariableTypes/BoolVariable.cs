﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="bool"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/Bool")]
    public class BoolVariable : BaseVariable<bool>
    {
        public static implicit operator bool(BoolVariable reference)
        {
            return reference.Value;
        }
    }
}

