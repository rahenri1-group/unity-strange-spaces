﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// HDR <see cref="Color"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/Color HDR")]
    public class ColorHdrVariable : BaseVariable<Color>
    {
        public static implicit operator Color(ColorHdrVariable reference)
        {
            return reference.Value;
        }
    }
}
