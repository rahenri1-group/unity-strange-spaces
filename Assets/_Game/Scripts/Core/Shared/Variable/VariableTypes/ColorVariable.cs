﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="Color"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/Color")]
    public class ColorVariable : BaseVariable<Color>
    {
        public static implicit operator Color(ColorVariable reference)
        {
            return reference.Value;
        }
    }
}
