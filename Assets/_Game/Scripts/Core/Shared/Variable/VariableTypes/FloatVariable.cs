﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="float"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/Float")]
    public class FloatVariable : BaseVariable<float>
    {
        public static implicit operator float(FloatVariable reference)
        {
            return reference.Value;
        }
    }
}
