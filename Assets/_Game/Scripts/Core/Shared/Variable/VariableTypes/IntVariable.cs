﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="int"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/Int")]
    public class IntVariable : BaseVariable<int>
    {
        public static implicit operator int(IntVariable reference)
        {
            return reference.Value;
        }
    }
}
