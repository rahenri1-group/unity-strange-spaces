﻿using UnityEngine;

namespace Game.Core
{
    /// <summary>
    /// <see cref="string"/> version of <see cref="IVariable{T}"/>
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Variable/String")]
    public class StringVariable : BaseVariable<string>
    {
        public static implicit operator string(StringVariable reference)
        {
            return reference.Value;
        }
    }
}

