﻿using Game.Core.Entity;

namespace Game.Core.Space
{
    /// <summary>
    /// Extensions for <see cref="ISpaceManager"/>
    /// </summary>
    public static class SpaceManagerExtensions
    {
        /// <summary>
        /// Gets the space for a <see cref="IEntity"/>
        /// </summary>
        /// <param name="spaceManager"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static ISpaceData GetEntitySpace(this ISpaceManager spaceManager, IEntity entity)
        {
            return spaceManager.GetObjectSpace(entity.GameObject);
        }
    }
}
