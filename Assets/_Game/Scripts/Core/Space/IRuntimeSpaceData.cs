﻿using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <summary>
    /// Runtime data generated for a loaded <see cref="ISpaceData"/>
    /// </summary>
    public interface IRuntimeSpaceData
    {
        /// <summary>
        /// The <see cref="ISpaceData"/> this is for
        /// </summary>
        ISpaceData SpaceData { get; }
        
        /// <summary>
        /// The scene the space has been loaded into
        /// </summary>
        Scene Scene { get; }
        
        /// <summary>
        /// The last time (seconds from game start) that the space was the active space
        /// </summary>
        float LastActivationTime { get; }

        /// <summary>
        /// The layer assigned to all renderers in the space
        /// </summary>
        int SpaceRenderLayer { get; }
    }
}
