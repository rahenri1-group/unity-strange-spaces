﻿using System;

namespace Game.Core.Space
{
    /// <summary>
    /// Interface for the data relating to a space
    /// </summary>
    public interface ISpaceData : IEquatable<ISpaceData>
    {
        /// <summary>
        /// The name of the space
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The id of the space
        /// </summary>
        Guid SpaceId { get; }

        /// <summary>
        /// Gets additional data of type <typeparamref name="T"/> for the space
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetAdditionalData<T>() where T : class, ISpaceDataComponent;
    }
}