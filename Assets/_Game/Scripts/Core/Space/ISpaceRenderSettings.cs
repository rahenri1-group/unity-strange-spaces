﻿using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Controls the rendering of a space
    /// </summary>
    public interface ISpaceRenderSettings
    {
        /// <summary>
        /// The skybox material
        /// </summary>
        public Material Skybox { get; set; }

        /// <summary>
        /// The light representing the sun
        /// </summary>
        public Light Sun { get; set; }

        /// <summary>
        /// The color of ambient lighting in the space
        /// </summary>
        public Color AmbientLightColor { get; set; }

        /// <summary>
        /// The color to use for realtime shadows
        /// </summary>
        public Color RealtimeShadowsColor { get; set; }
    }
}
