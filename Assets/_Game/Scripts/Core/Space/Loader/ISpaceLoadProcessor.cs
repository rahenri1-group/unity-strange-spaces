﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Space
{
    /// <summary>
    /// A processor for a space that has just loaded
    /// </summary>
    public interface ISpaceLoadProcessor
    {
        /// <summary>
        /// Processes a <paramref name="space"/> that has just loaded but before <see cref="Event.SpaceLoadedEvent"/> has been invoked
        /// </summary>
        UniTask ProcessSpaceForLoad(IRuntimeSpaceData space);
    }
}
