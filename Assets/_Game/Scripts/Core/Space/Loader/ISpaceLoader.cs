﻿using Cysharp.Threading.Tasks;
using System;

namespace Game.Core.Space
{
    /// <summary>
    /// Interface for loading a space
    /// </summary>
    public interface ISpaceLoader : IModule
    {
        /// <summary>
        /// All loaded spaces
        /// </summary>
        ISpaceData[] LoadedSpaces { get; }

        /// <summary>
        /// Is a space loaded
        /// </summary>
        bool IsSpaceLoaded(ISpaceData space);
        /// <summary>
        /// Is a space loaded
        /// </summary>
        bool IsSpaceLoaded(Guid spaceId);

        /// <summary>
        /// Is a space currently loading
        /// </summary>
        bool IsSpaceLoading(ISpaceData space);
        /// <summary>
        /// Is a space currently loading
        /// </summary>
        bool IsSpaceLoading(Guid spaceId);

        /// <summary>
        /// is a space currently loaded or loading
        /// </summary>
        bool IsSpaceLoadedOrLoading(ISpaceData space);
        /// <summary>
        /// is a space currently loaded or loading
        /// </summary>
        bool IsSpaceLoadedOrLoading(Guid spaceId);

        /// <summary>
        /// Registers a new <paramref name="processor"/>
        /// </summary>
        void RegisterSpaceLoadProcessor(ISpaceLoadProcessor processor);
        /// <summary>
        /// Unregisters a <paramref name="processor"/>
        /// </summary>
        void UnregisterSpaceLoadProcessor(ISpaceLoadProcessor processor);

        /// <summary>
        /// Registers a new <paramref name="processor"/>
        /// </summary>
        void RegisterSpaceUnloadProcessor(ISpaceUnloadProcessor processor);
        /// <summary>
        /// Unregisters a <paramref name="processor"/>
        /// </summary>
        void UnregisterSpaceUnloadProcessor(ISpaceUnloadProcessor processor);

        /// <summary>
        /// Loads the space defined by <paramref name="space"/>
        /// </summary>
        UniTask LoadSpace(ISpaceData space);
        /// <summary>
        /// Loads the space with id <paramref name="spaceId"/>
        /// </summary>
        UniTask LoadSpace(Guid spaceId);

        /// <summary>
        /// Loads the spaces defined by <paramref name="spaces"/>
        /// </summary>
        UniTask LoadSpaces(ISpaceData[] spaces);
        /// <summary>
        /// Loads the spaces with ids <paramref name="spaceIds"/>
        /// </summary>
        UniTask LoadSpaces(Guid[] spaceIds);

        /// <summary>
        /// Unloads the space defined by <paramref name="space"/>
        /// </summary>
        UniTask UnloadSpace(ISpaceData space);
        /// <summary>
        /// Unloads the space with id <paramref name="spaceId"/>
        /// </summary>
        UniTask UnloadSpace(Guid spaceId);
    }
}