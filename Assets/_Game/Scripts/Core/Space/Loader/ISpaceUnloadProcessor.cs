﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Space
{
    /// <summary>
    /// A processor for a space that will be unloaded
    /// </summary>
    public interface ISpaceUnloadProcessor
    {
        /// <summary>
        /// Processes a <paramref name="space"/> before it has been unloaded from memory
        /// </summary>
        UniTask ProcessSpaceForUnload(IRuntimeSpaceData space);
    }
}
