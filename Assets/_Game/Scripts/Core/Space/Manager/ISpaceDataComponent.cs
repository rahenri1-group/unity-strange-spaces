﻿namespace Game.Core.Space
{
    /// <summary>
    /// Additional data for a <see cref="ISpaceData"/>
    /// </summary>
    public interface ISpaceDataComponent { }
}
