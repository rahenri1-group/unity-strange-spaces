﻿using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Delegate for space related events
    /// </summary>
    /// <param name="space"></param>
    /// <returns></returns>
    public delegate UniTask SpaceEventHandler(IRuntimeSpaceData space);

    /// <summary>
    /// Interface managing spaces
    /// </summary>
    public interface ISpaceManager : IModule
    {
        /// <summary>
        /// The active space
        /// </summary>
        ISpaceData ActiveSpace { get; }

        /// <summary>
        /// Gets the space data for a space with id <paramref name="spaceId"/>
        /// </summary>
        ISpaceData SpaceDataFromId(Guid spaceId);

        /// <summary>
        /// Gets the space of the given object
        /// </summary>
        ISpaceData GetObjectSpace(GameObject gameObject);

        /// <summary>
        /// Tests if two objects are in the same space
        /// </summary>
        bool AreInSameSpace(GameObject gameObject1, GameObject gameObject2);

        /// <summary>
        /// Sets the active space to the space defined by <paramref name="space"/>
        /// </summary>
        void SetActiveSpace(ISpaceData space);
        /// <summary>
        /// Sets the active space to the space with id <paramref name="spaceId"/>
        /// </summary>
        void SetActiveSpace(Guid spaceId);

        /// <summary>
        /// Starts the operation to move an object to a new space. The object will have completed movement at the end of the current frame.
        /// This should only be used for when moving multiple objects at once
        /// </summary>
        void StartMoveObjectToSpaceOperation(GameObject gameObject, ISpaceData space);

        /// <summary>
        /// Returns when the object has completed moving
        /// </summary>
        UniTask MoveObjectToSpaceAsync(GameObject gameObject, ISpaceData space);

        /// <summary>
        /// Finds all components of type <typeparamref name="T"/> in <paramref name="space"/>
        /// </summary>
        T[] FindLoadedInSpace<T>(ISpaceData space, bool includeInactive = false) where T : class;

        /// <summary>
        /// Finds all currently loaded components of type <typeparamref name="T"/>.
        /// </summary>
        T[] FindLoaded<T>(bool includeInactive = false) where T : class;
    }
}