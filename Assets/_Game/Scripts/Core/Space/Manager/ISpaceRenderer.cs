﻿using Game.Core.Render.Camera;
using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Interface for controlling the rendering of a space
    /// </summary>
    public interface ISpaceRenderer : IModule
    {
        /// <summary>
        /// The current <see cref="ISpaceRenderSettings"/>
        /// </summary>
        ISpaceRenderSettings CurrentRenderSettings { get; set; }

        /// <summary>
        /// Gets the render settings for a space.
        /// Note: this will return a null value for a space that is not currently loaded.
        /// </summary>
        ISpaceRenderSettings GetSpaceRenderSettings(ISpaceData spaceData);

        /// <summary>
        /// Configures a <see cref="ICamera"/> to render a space
        /// </summary>
        void ConfigureCameraForSpace(ICamera camera, ISpaceData space);

        /// <summary>
        /// Configures a <see cref="Renderer"/> to only renderer for a given space
        /// </summary>
        void ConfigureRendererForSpace(Renderer renderer, ISpaceData space);

        /// <summary>
        /// Configures a <see cref="Canvas"/> to only render for a given space
        /// </summary>
        void ConfigureCanvasForSpace(Canvas canvas, ISpaceData space);
    }
}
