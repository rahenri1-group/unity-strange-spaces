﻿using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <summary>
    /// Interface for matching scenes to spaces
    /// </summary>
    public interface ISpaceSceneHelper
    {
        /// <summary>
        /// Gets the space data for a given scene
        /// </summary>
        /// <param name="scene"></param>
        /// <returns></returns>
        ISpaceData SpaceDataFromScene(Scene scene);

        /// <summary>
        /// Get the space data for a scene with a given build index
        /// </summary>
        /// <param name="sceneBuildIndex"></param>
        /// <returns></returns>
        ISpaceData SpaceDataFromScene(int sceneBuildIndex);
    }
}
