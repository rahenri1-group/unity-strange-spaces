﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Physics;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    [Dependency(
    contract: typeof(ISpaceLoader),
    lifetime: Lifetime.Singleton)]
    public partial class SpaceManager : BaseModule, ISpaceLoader
    {
        protected int LoadedAndLoadingSpacesCount => _loadingSpaces.Count + _loadedSpaces.Count;

        /// <inheritdoc/>
        public ISpaceData[] LoadedSpaces => _loadedSpaces.Values.Select(s => s.SpaceData).ToArray();

        private readonly HashSet<Guid> _loadingSpaces;

        private readonly HashSet<ISpaceLoadProcessor> _spaceLoadProcessors;
        private readonly HashSet<ISpaceUnloadProcessor> _spaceUnloadProcessors;

        private bool _spacesLoadingFlag;

        /// <inheritdoc/>
        public bool IsSpaceLoaded(Guid spaceId)
        {
            var space = SpaceDataFromIdHelper(spaceId);
            if (space == null)
            {
                ModuleLogError($"Attempt check load status of unknown space '{spaceId}'");
                return false;
            }

            return IsSpaceLoaded(space);
        }

        /// <inheritdoc/>
        public bool IsSpaceLoaded(ISpaceData space)
        {
            return _loadedSpaces.ContainsKey(space.SpaceId);
        }

        /// <inheritdoc/>
        public bool IsSpaceLoading(Guid spaceId)
        {
            var space = SpaceDataFromIdHelper(spaceId);
            if (space == null)
            {
                ModuleLogError($"Attempt check load status of unknown space '{spaceId}'");
                return false;
            }

            return IsSpaceLoading(space);
        }

        /// <inheritdoc/>
        public bool IsSpaceLoading(ISpaceData space)
        {
            return _loadingSpaces.Contains(space.SpaceId);
        }

        /// <inheritdoc/>
        public bool IsSpaceLoadedOrLoading(Guid spaceId)
        {
            return IsSpaceLoaded(spaceId) || IsSpaceLoading(spaceId);
        }

        /// <inheritdoc/>
        public bool IsSpaceLoadedOrLoading(ISpaceData space)
        {
            return IsSpaceLoaded(space) || IsSpaceLoading(space);
        }

        /// <inheritdoc/>
        public void RegisterSpaceLoadProcessor(ISpaceLoadProcessor processor)
        {
            _spaceLoadProcessors.Add(processor);
        }
        /// <inheritdoc/>
        public void UnregisterSpaceLoadProcessor(ISpaceLoadProcessor processor)
        {
            _spaceLoadProcessors.Remove(processor);
        }

        /// <inheritdoc/>
        public void RegisterSpaceUnloadProcessor(ISpaceUnloadProcessor processor)
        {
            _spaceUnloadProcessors.Add(processor);
        }
        /// <inheritdoc/>
        public void UnregisterSpaceUnloadProcessor(ISpaceUnloadProcessor processor)
        {
            _spaceUnloadProcessors.Remove(processor);
        }

        /// <inheritdoc/>
        public async UniTask LoadSpace(ISpaceData space)
        {
            await LoadSpace(space.SpaceId);
        }

        /// <inheritdoc/>
        public async UniTask LoadSpace(Guid spaceId)
        {
            var space = SpaceDataFromIdHelper(spaceId);
            if (space == null)
            {
                ModuleLogError($"Attempt to load unknown space '{spaceId}'");
                return;
            }

            if (IsSpaceLoading(space))
            {
                //
                //LogWarning($"Attempt to load already loading space '{space.Name}'");
                return;
            }

            if (IsSpaceLoaded(space))
            {
                //ModuleLogWarning($"Attempt to load already loaded space '{space.Name}'");
                return;
            }

            if (_spacesLoadingFlag)
            {
                ModuleLogWarning($"A previous load hasn't finished. Loading space '{spaceId}' will delay until the previous load finishes.");
                while (_spacesLoadingFlag)
                {
                    await UniTask.Delay(10, delayTiming: PlayerLoopTiming.Update);
                }
            }

            _spacesLoadingFlag = true;

            await LoadSpaceHelper(space, new Guid[] { space.Id });

            _spacesLoadingFlag = false;
        }

        /// <inheritdoc/>
        public async UniTask LoadSpaces(ISpaceData[] spaces)
        {
            var spaceIds = spaces.Select(s => s.SpaceId).ToArray();
            await LoadSpaces(spaceIds);
        }

        /// <inheritdoc/>
        public async UniTask LoadSpaces(Guid[] spaceIds)
        {
            if (_spacesLoadingFlag)
            {
                ModuleLogWarning($"A previous load hasn't finished. Loading spaces '{string.Join(", ", spaceIds)}' will delay until the previous load finishes.");
                while (_spacesLoadingFlag)
                {
                    await UniTask.Delay(10, delayTiming: PlayerLoopTiming.Update);
                }
            }

            _spacesLoadingFlag = true;

            foreach (var id in spaceIds)
            {
                var space = SpaceDataFromIdHelper(id);

                if (space == null)
                {
                    ModuleLogError($"Attempt to load unknown space '{id}'");
                    continue;
                }

                if (IsSpaceLoading(space))
                {
                    //ModuleLogWarning($"Attempt to load already loading space '{space.Name}'");
                    continue;
                }

                if (IsSpaceLoaded(space))
                {
                    //ModuleLogWarning($"Attempt to load already loaded space '{space.Name}'");
                    continue;
                }

                await LoadSpaceHelper(space, spaceIds);
            }

            _spacesLoadingFlag = false;
        }

        private async UniTask LoadSpaceHelper(SpaceData space, Guid[] spacesBeingLoaded)
        {
            _loadingSpaces.Add(space.SpaceId);

            if (Config.MaxLoadedSpaces > 0)
            {
                while (LoadedAndLoadingSpacesCount >= Config.MaxLoadedSpaces)
                {
                    await RemoveLeastUsedSpace(spacesBeingLoaded);
                }
            }

            int sceneBuildIndex = space.SceneBuildIndex;

            await SceneManager.LoadSceneAsync(
                sceneBuildIndex,
                new LoadSceneParameters
                {
                    loadSceneMode = LoadSceneMode.Additive,
                    localPhysicsMode = LocalPhysicsMode.Physics3D
                });

            var scene = SceneManager.GetSceneByBuildIndex(sceneBuildIndex);

            var physicsStepper = new GameObject("@Physics Stepper", typeof(PhysicsStepperBehaviour));
            SceneManager.MoveGameObjectToScene(physicsStepper.gameObject, scene);

            await SetupLoadedSpace(space, scene);

            ModuleLogInfo($"Space {space.Name} loaded");

            _loadingSpaces.Remove(space.SpaceId);
        }

        protected async UniTask SetupLoadedSpace(SpaceData space, Scene scene)
        {
            var runtimeSpaceData = new RuntimeSpaceData
            {
                SpaceDataAsset = space,
                Scene = scene,
                LastActivationTime = 0
            };

            _loadedSpaces[space.SpaceId] = runtimeSpaceData;

            SetupSpaceRendering(runtimeSpaceData);

            foreach (var processor in _spaceLoadProcessors)
            {
                await processor.ProcessSpaceForLoad(runtimeSpaceData);
            }

            await UniTask.DelayFrame(1); // wait 1 frame to make sure everything has finished processing

            _eventBus.InvokeEvent(new SpaceLoadedEvent
            {
                SpaceData = space,
                RootLoadedObjects = scene.GetRootGameObjects()
            });

            if (ActiveSpace == null)
            {
                SetActiveSpace(space);
            }
        }

        private async UniTask RemoveLeastUsedSpace(Guid[] spacesToKeep)
        {
            if (_loadedSpaces.Count == 0)
            {
                // wait a single frame to avoid infinite loop
                await UniTask.DelayFrame(1);
            }
            else
            {
                RuntimeSpaceData spaceToUnload = null;
                foreach (var pair in _loadedSpaces)
                {
                    var spaceData = pair.Value;

                    if (!spacesToKeep.Contains(spaceData.SpaceDataAsset.Id))
                    {
                        if (spaceToUnload == null || spaceData.LastActivationTime < spaceToUnload.LastActivationTime)
                        {
                            spaceToUnload = spaceData;
                        }
                    }
                }

                await UnloadSpace(spaceToUnload.SpaceData);
            }
        }

        /// <inheritdoc/>
        public async UniTask UnloadSpace(Guid spaceId)
        {
            var space = SpaceDataFromIdHelper(spaceId);
            if (space == null)
            {
                ModuleLogError($"Attempt to unload unknown space '{spaceId}'");
                return;
            }

            await UnloadSpace(space);
        }

        /// <inheritdoc/>
        public async UniTask UnloadSpace(ISpaceData space)
        {
            if (IsSpaceLoading(space))
            {
                ModuleLogWarning($"Space '{space.Name}' hasn't finished loading. Delaying unload until load finishes.");

                await UniTask.WaitUntil(() => IsSpaceLoading(space), PlayerLoopTiming.LastTimeUpdate);
            }

            if (!IsSpaceLoaded(space))
            {
                ModuleLogWarning($"Attempt to unload already unloaded space '{space.Name}'");
                return;
            }

            var spaceData = _loadedSpaces[space.SpaceId];
            _loadedSpaces.Remove(space.SpaceId);

            foreach (var processor in _spaceUnloadProcessors)
            {
                await processor.ProcessSpaceForUnload(spaceData);
            }

            _eventBus.InvokeEvent(new SpaceUnloadingEvent
            {
                SpaceData = space
            });

            await SceneManager.UnloadSceneAsync(spaceData.Scene);

            TeardownSpaceRendering(spaceData);

            ModuleLogInfo($"Space {space.Name} unloaded");

            _eventBus.InvokeEvent(new SpaceUnloadedEvent
            {
                SpaceData = space
            });

            if (SpaceUtil.SpaceEquals(ActiveSpace, space))
            {
                SetActiveSpace(null);
            }
        }
    }
}