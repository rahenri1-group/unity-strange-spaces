﻿using Game.Core.DependencyInjection;
using Game.Core.Math;
using Game.Core.Portal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Space
{
    [Dependency(
    contract: typeof(ISpacePhysics),
    lifetime: Lifetime.Singleton)]
    public partial class SpaceManager : BaseModule, ISpacePhysics
    {
        private RaycastHit[] _unityCastHitResultCache = new RaycastHit[32];
        private Collider[] _colliderResultCache = new Collider[32];

        private int _portalLayerId;

        /// <inheritdoc />
        public bool Raycast(
            ISpaceData space, 
            Vector3 origin, 
            Vector3 direction, 
            out SpaceRayCastResult resultInfo, 
            float maxDistance = float.PositiveInfinity, 
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll, 
            int layerMask = -5, 
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
        {
            resultInfo = new SpaceRayCastResult();
            resultInfo.Distance = 0f;

            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                return false;
            }

            if (portalInteraction != QueryPortalInteraction.Ignore)
            {
                layerMask = LayerUtil.IncludeLayerInMask(layerMask, _portalLayerId);
            }

            return RaycastInternal(0, null, space, origin, direction, ref resultInfo, maxDistance, portalInteraction, layerMask, queryTriggerInteraction);
        }

        private bool RaycastInternal(
            int portalDepth, 
            IPortal senderPortal, 
            ISpaceData space, 
            Vector3 origin, 
            Vector3 direction, 
            ref SpaceRayCastResult resultInfo, 
            float maxDistance, 
            QueryPortalInteraction portalInteraction, 
            int layerMask, 
            QueryTriggerInteraction queryTriggerInteraction)
        {
            if (portalDepth >= Config.MaxPhysicsCastsPortalDepth)
            {
                return false;
            }

            var physicsScene = _loadedSpaces[space.SpaceId].Scene.GetPhysicsScene();
            int hitCount = physicsScene.Raycast(origin, direction, _unityCastHitResultCache, maxDistance, layerMask, queryTriggerInteraction);
            if (hitCount > 0 
                && ProcessRaycastHits(
                    _unityCastHitResultCache, 
                    hitCount, 
                    senderPortal, 
                    portalInteraction, 
                    out IPortal hitPortal, 
                    out Vector3 hitPoint, 
                    out Vector3 hitNormal, 
                    out float hitDistance, 
                    out Collider hitCollider))
            {

                if (hitPortal != null)
                {
                    resultInfo.Distance += hitDistance;

                    return RaycastInternal(
                        portalDepth + 1,
                        hitPortal,
                        hitPortal.EndPoint.Space,
                        hitPortal.CalculateEndPointPosition(hitPoint),
                        hitPortal.CalculateEndPointDirection(direction),
                        ref resultInfo,
                        maxDistance - hitDistance,
                        portalInteraction,
                        layerMask,
                        queryTriggerInteraction);
                }
                else
                {
                    resultInfo.Space = space;
                    resultInfo.HitPosition = hitPoint;
                    resultInfo.Collider = hitCollider;
                    resultInfo.Distance += hitDistance;
                    resultInfo.HitDirection = direction;
                    resultInfo.HitNormal = hitNormal;

                    return true;
                }
            }

            resultInfo.Space = space;

            return false;
        }


        /// <inheritdoc />
        public bool BoxCast(
            ISpaceData space, 
            Vector3 center, 
            Vector3 extends, 
            Vector3 direction, 
            out SpaceShapeCastResult resultInfo, 
            Quaternion orientation, 
            float maxDistance = float.PositiveInfinity, 
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll, 
            int layerMask = -5, 
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
        {
            resultInfo = new SpaceShapeCastResult();
            resultInfo.Distance = 0f;
            resultInfo.Space = space;
            resultInfo.CastShapePosition = center;
            resultInfo.CastShapeRotation = orientation;

            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                return false;
            }

            if (portalInteraction != QueryPortalInteraction.Ignore)
            {
                layerMask = LayerUtil.IncludeLayerInMask(layerMask, _portalLayerId);
            }

            return BoxCastInternal(0, null, space, center, extends, direction, ref resultInfo, orientation, maxDistance, portalInteraction, layerMask, queryTriggerInteraction);
        }

        private bool BoxCastInternal(
            int portalDepth, 
            IPortal senderPortal, 
            ISpaceData space, 
            Vector3 center, 
            Vector3 extends, 
            Vector3 direction, 
            ref SpaceShapeCastResult resultInfo, 
            Quaternion orientation,
            float maxDistance, 
            QueryPortalInteraction portalInteraction, 
            int layerMask, 
            QueryTriggerInteraction queryTriggerInteraction)
        {
            if (portalDepth >= Config.MaxPhysicsCastsPortalDepth)
            {
                return false;
            }

            var physicsScene = _loadedSpaces[space.SpaceId].Scene.GetPhysicsScene();
            int hitCount = physicsScene.BoxCast(center, extends, direction, _unityCastHitResultCache, orientation, maxDistance, layerMask, queryTriggerInteraction);
            if (hitCount > 0
                && ProcessRaycastHits(
                    _unityCastHitResultCache, 
                    hitCount, 
                    senderPortal, 
                    portalInteraction, 
                    out IPortal hitPortal, 
                    out Vector3 hitPoint, 
                    out Vector3 hitNormal, 
                    out float hitDistance, 
                    out Collider hitCollider))
            {

                var castPosition = center + direction * hitDistance;

                if (hitPortal != null)
                {
                    while (!hitPortal.PortalPlane.GetSide(castPosition)) // bump point until is fully on other side of portal
                    {
                        float bumpDistance = 0.05f;
                        hitDistance += bumpDistance;
                        castPosition += direction * bumpDistance;
                    }

                    resultInfo.Distance += hitDistance;

                    return BoxCastInternal(
                        portalDepth + 1,
                        hitPortal,
                        hitPortal.EndPoint.Space,
                        hitPortal.CalculateEndPointPosition(castPosition), 
                        extends,
                        hitPortal.CalculateEndPointDirection(direction),
                        ref resultInfo,
                        hitPortal.CalculateEndPointRotation(orientation), 
                        maxDistance - hitDistance, 
                        portalInteraction, 
                        layerMask, 
                        queryTriggerInteraction);
                }
                else
                {
                    resultInfo.Space = space;
                    resultInfo.HitPosition = hitPoint;
                    resultInfo.CastShapePosition = castPosition;
                    resultInfo.CastShapeRotation = orientation;
                    resultInfo.Collider = hitCollider;
                    resultInfo.Distance += hitDistance;

                    return true;
                }
            }

            resultInfo.Space = space;
            if (maxDistance != float.MaxValue) resultInfo.CastShapePosition = center + direction * maxDistance;
            resultInfo.CastShapeRotation = orientation;

            return false;
        }

        /// <inheritdoc />
        public bool FindInSphere<T>(
            ISpaceData space, 
            Vector3 position, 
            float radius,
            out SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll, 
            int layerMask = -5, 
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal,
            Func<IPortal, float, float> onPortalRadiusModifier = null) 
            where T : class
        {
            resultInfo = new SpaceFindResult<T>();

            if (space == null || !_loadedSpaces.ContainsKey(space.SpaceId))
            {
                return false;
            }

            if (portalInteraction != QueryPortalInteraction.Ignore)
            {
                layerMask = LayerUtil.IncludeLayerInMask(layerMask, _portalLayerId);
            }

            return FindInSphereInternal<T>(new List<IPortal>(), space, position, radius, ref resultInfo, portalInteraction, layerMask, queryTriggerInteraction, onPortalRadiusModifier);
        }

        private bool FindInSphereInternal<T>(
            List<IPortal> portalPath, 
            ISpaceData space, 
            Vector3 position, 
            float radius,
            ref SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction, 
            int layerMask, 
            QueryTriggerInteraction queryTriggerInteraction,
            Func<IPortal, float, float> onPortalRadiusModifier) 
            where T : class
        {
            if (portalPath.Count >= Config.MaxPhysicsCastsPortalDepth || radius <= 0f)
            {
                return false;
            }

            var physicsScene = _loadedSpaces[space.SpaceId].Scene.GetPhysicsScene();

            var portals = new List<IPortal>();
            var hitCount = physicsScene.OverlapSphere(position, radius, _colliderResultCache, layerMask, queryTriggerInteraction);
            for (int i = 0; i < hitCount; i++)
            {
                var collider = _colliderResultCache[i];

                var component = collider.GetComponentInParent<T>();
                if (component != null)
                {
                    resultInfo.Result = component;
                    resultInfo.Space = space;
                    resultInfo.Portals = portalPath.ToArray();

                    return true;
                }
                else if (IsColliderInteractablePortal(collider, portalInteraction, out var portal))
                {
                    portals.Add(portal);
                }
            }

            foreach (var portal in portals)
            {
                if (!portalPath.Contains(portal))
                {
                    var testPath = new List<IPortal>(portalPath);
                    testPath.Add(portal);

                    float remainingRadius = radius - Vector3.Distance(position, portal.Transform.position);

                    var found = FindInSphereInternal<T>(
                        testPath,
                        portal.EndPoint.Space,
                        portal.EndPoint.Transform.position,
                        onPortalRadiusModifier != null ? onPortalRadiusModifier(portal, remainingRadius) : remainingRadius,
                        ref resultInfo,
                        portalInteraction,
                        layerMask,
                        queryTriggerInteraction,
                        onPortalRadiusModifier);

                    if (found)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <inheritdoc />
        public bool FindClosestInCone<T>(
            ISpaceData space,
            Cone cone,
            out SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll,
            int layerMask = -5,  // everything except IgnoreRaycast layer
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) 
            where T : class
        {
            resultInfo = new SpaceFindResult<T>();

            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                return false;
            }

            if (portalInteraction != QueryPortalInteraction.Ignore)
            {
                layerMask = LayerUtil.IncludeLayerInMask(layerMask, _portalLayerId);
            }

            return FindInConeInternal<T>(new List<IPortal>(), space, cone, ref resultInfo, portalInteraction, layerMask, queryTriggerInteraction);
        }

        private bool FindInConeInternal<T>(
            List<IPortal> portalPath,
            ISpaceData space,
            Cone cone,
            ref SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction,
            int layerMask,
            QueryTriggerInteraction queryTriggerInteraction) 
            where T : class
        {
            if (portalPath.Count >= Config.MaxPhysicsCastsPortalDepth)
            {
                return false;
            }

            var physicsScene = _loadedSpaces[space.SpaceId].Scene.GetPhysicsScene();

            var portals = new List<IPortal>();
            var hitCount = physicsScene.OverlapSphere(cone.Origin, cone.MaxDistance, _colliderResultCache, layerMask, queryTriggerInteraction);

            // in the case of multiple, return the closest one in the current space
            T closestComponent = null;
            float closestComponentDistance = float.MaxValue;
            for (int i = 0; i < hitCount; i++)
            {
                var collider = _colliderResultCache[i];
                var colliderBoundsCenter = collider.bounds.center;

                IPortal senderPortal = portalPath.Count > 0 ? portalPath[portalPath.Count - 1] : null;
                if ((senderPortal == null || !senderPortal.EndPoint.PortalPlane.GetSide(colliderBoundsCenter))
                    && MathUtil.DoesConeIntersectBounds(collider.bounds, cone))
                {
                    var component = collider.GetComponentInParent<T>();
                    if (component != null)
                    {
                        float distance = Vector3.Distance(cone.Origin, colliderBoundsCenter);
                        if (distance < closestComponentDistance)
                        {
                            closestComponent = component;
                            closestComponentDistance = distance;
                        }
                    }
                    else if (IsColliderInteractablePortal(collider, portalInteraction, out var portal))
                    {
                        portals.Add(portal);
                    }
                }
            }

            if (closestComponent != null)
            {
                resultInfo.Result = closestComponent;
                resultInfo.Space = space;
                resultInfo.Portals = portalPath.ToArray();

                return true;
            }

            foreach (var portal in portals)
            {
                if (!portalPath.Contains(portal))
                {
                    var testPath = new List<IPortal>(portalPath);
                    testPath.Add(portal);

                    var testCone = new Cone(portal.CalculateEndPointPosition(cone.Origin),
                        portal.CalculateEndPointDirection(cone.Direction),
                        cone.Angle,
                        cone.MaxDistance);

                    var found = FindInConeInternal<T>(
                        testPath,
                        portal.EndPoint.Space,
                        testCone,
                        ref resultInfo,
                        portalInteraction,
                        layerMask,
                        queryTriggerInteraction);

                    if (found)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool ProcessRaycastHits(RaycastHit[] hitCache, int hitCount, IPortal senderPortal, QueryPortalInteraction portalInteraction, out IPortal hitPortal, out Vector3 hitPoint, out Vector3 hitNormal, out float hitDistance, out Collider hitCollider)
        {
            bool hitProcessed = false;

            hitPortal = null;
            hitPoint = Vector3.zero;
            hitNormal = Vector3.up;
            hitDistance = float.MaxValue;
            hitCollider = null;
            for (int i = 0; i < hitCount; i++)
            {
                var testHitInfo = hitCache[i];

                bool isPortal = IsColliderInteractablePortal(testHitInfo.collider, portalInteraction, out var testPortal);
                
                if (isPortal && senderPortal != null && senderPortal.EndPoint == testPortal) continue; // don't collide with portal we passed through

                //if (!isPortal && hitPortal != null) continue; // once a portal has been found, only compare against portals

                if (testHitInfo.distance < hitDistance)
                {
                    hitPortal = (isPortal) ? testPortal : null;
                    hitPoint = testHitInfo.point;
                    hitDistance = testHitInfo.distance;
                    hitCollider = testHitInfo.collider;
                    hitNormal = testHitInfo.normal;

                    hitProcessed = true;
                }
            }

            return hitProcessed;
        }

        private bool IsColliderInteractablePortal(Collider collider, QueryPortalInteraction portalInteraction, out IPortal portal)
        {
            if (portalInteraction == QueryPortalInteraction.Block || portalInteraction == QueryPortalInteraction.Ignore) 
            {
                portal = null;
                return false;
            }

            portal = collider.GetComponentInParent<IPortal>();

            if (portal == null || !portal.IsOpen)
            {
                return false;
            }

            if (portalInteraction == QueryPortalInteraction.CastThroughRenderer)
            {
                var portalRenderer = portal.GetComponent<IPortalRenderer>();
                return portalRenderer != null && portalRenderer.RenderingEnabled;
            }
            else if (portalInteraction == QueryPortalInteraction.CastThroughTransporter)
            {
                var portalTransporter = portal.GetComponent<IPortalTransporter>();
                return portalTransporter != null;
            }

            // CastThroughAll
            return true;
        }
    }
}
