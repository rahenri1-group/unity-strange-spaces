﻿using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    [Dependency(
        contract: typeof(ISpaceRenderer),
        lifetime: Lifetime.Singleton)]
    public partial class SpaceManager : BaseModule, ISpaceRenderer
    {
        /// <inheritdoc/>
        public ISpaceRenderSettings CurrentRenderSettings
        {
            get => _currentRenderSettings;
            set
            {
                if (_currentRenderSettings != value)
                {
                    _currentRenderSettings = value;
                    (_currentRenderSettings as SpaceRenderSettings)?.ApplySettings();
                }
            }
        }
        private ISpaceRenderSettings _currentRenderSettings = null;

        private Queue<int> _availableLayers;

        private int _renderLayersMask;

        private void InitializeRenderer()
        {
            _renderLayersMask = 0x0;

            _availableLayers = new Queue<int>();
            foreach (var layerName in Config.SpaceRenderLayers)
            {
                int layer = LayerUtil.GetLayerId(layerName);
                _renderLayersMask |= 1 << layer;
                _availableLayers.Enqueue(layer);
            }

            _renderLayersMask = ~_renderLayersMask;
        }

        private void ShutdownRenderer()
        {
            _availableLayers.Clear();
        }

        /// <inheritdoc/>
        public ISpaceRenderSettings GetSpaceRenderSettings(ISpaceData spaceData)
        {
            if (_loadedSpaces.ContainsKey(spaceData.SpaceId))
            {
                return _loadedSpaces[spaceData.SpaceId].RenderSettings;
            }

            return null;
        }

        /// <inheritdoc/>
        public void ConfigureCameraForSpace(ICamera camera, ISpaceData space)
        {
            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                ModuleLogWarning($"Can not set view of {camera.Name} to {space.Name} as it is not loaded");
                return;
            }

            var runtimeSpaceData = _loadedSpaces[space.SpaceId];

            int spaceLayer = 1 << runtimeSpaceData.SpaceRenderLayer;
            camera.UnityCamera.cullingMask = (camera.UnityCamera.cullingMask & _renderLayersMask) | spaceLayer;

            var spaceCamera = (ISpaceCamera)camera;
            if (spaceCamera != null)
            {
                spaceCamera.RenderSettings = runtimeSpaceData.RenderSettings;
            }
        }

        /// <inheritdoc/>
        public void ConfigureRendererForSpace(Renderer renderer, ISpaceData space)
        {
            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                ModuleLogWarning($"Can not configure {renderer.name} for {space.Name} as it is not loaded");
                return;
            }

            var runtimeSpaceData = _loadedSpaces[space.SpaceId];
            renderer.gameObject.layer = runtimeSpaceData.SpaceRenderLayer;
        }

        /// <inheritdoc/>
        public void ConfigureCanvasForSpace(Canvas canvas, ISpaceData space)
        {
            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                ModuleLogWarning($"Can not configure {canvas.name} for {space.Name} as it is not loaded");
                return;
            }

            var runtimeSpaceData = _loadedSpaces[space.SpaceId];
            canvas.gameObject.layer = runtimeSpaceData.SpaceRenderLayer;
        }

        private void SetupSpaceRendering(RuntimeSpaceData runtimeSpaceData)
        {
            var spaceLayer = _availableLayers.Dequeue();

            ModuleLogInfo($"'{runtimeSpaceData.SpaceData.Name}' assigned layer '{LayerUtil.GetLayerName(spaceLayer)} ({spaceLayer})'");

            runtimeSpaceData.SpaceRenderLayer = spaceLayer;

            foreach (var rootObject in runtimeSpaceData.Scene.GetRootGameObjects())
            {
                UpdateObjectRenderingLayers(rootObject, spaceLayer);
            }

            // temp switch space to get scene render settings
            var activeSpace = SceneManager.GetActiveScene();
            SceneManager.SetActiveScene(runtimeSpaceData.Scene);

            runtimeSpaceData.RenderSettings = new SpaceRenderSettings
            (
                this,
                RenderSettings.skybox,
                RenderSettings.sun,
                RenderSettings.ambientLight,
                RenderSettings.subtractiveShadowColor
            );

            SceneManager.SetActiveScene(activeSpace);
        }

        private void TeardownSpaceRendering(RuntimeSpaceData runtimeSpaceData)
        {
            _availableLayers.Enqueue(runtimeSpaceData.SpaceRenderLayer);
        }

        private void UpdateObjectRenderingLayers(GameObject gameObject, int spaceLayer)
        {
            foreach (var renderer in gameObject.GetComponentsInChildren<Renderer>(true))
            {
                renderer.gameObject.layer = spaceLayer;
            }

            foreach (var canvas in gameObject.GetComponentsInChildren<Canvas>(true))
            {
                canvas.gameObject.layer = spaceLayer;
            }

            var spaceLayerMask = 1 << spaceLayer;
            foreach (var light in gameObject.GetComponentsInChildren<Light>(true))
            {
                light.gameObject.layer = spaceLayer;
                light.cullingMask = spaceLayerMask;
            }
        }
    }
}
