﻿using Game.Core.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    [Dependency(
    contract: typeof(ISpaceSceneHelper),
    lifetime: Lifetime.Singleton)]
    public partial class SpaceManager : BaseModule, ISpaceSceneHelper
    {
        /// <inheritdoc cref="IRuntimeSpaceData"/>
        private class RuntimeSpaceData : IRuntimeSpaceData
        {
            /// <inheritdoc/>
            public ISpaceData SpaceData => SpaceDataAsset;
            public SpaceData SpaceDataAsset { get; set; }

            /// <inheritdoc/>
            public Scene Scene { get; set; }
            /// <inheritdoc/>
            public float LastActivationTime { get; set; } = 0f;
            /// <inheritdoc/>
            public int SpaceRenderLayer { get; set; } = 0;

            public ISpaceRenderSettings RenderSettings { get; set; } = null;
        }

        private List<SpaceData> _avaliableSpaces;

        /// <inheritdoc/>
        public ISpaceData SpaceDataFromScene(Scene scene)
        {
            return SpaceDataFromSceneHelper(scene);
        }

        private SpaceData SpaceDataFromSceneHelper(Scene scene)
        {
            return _avaliableSpaces.FirstOrDefault(s => s.SceneBuildIndex == scene.buildIndex);
        }

        /// <inheritdoc/>
        public ISpaceData SpaceDataFromId(Guid spaceId)
        {
            return SpaceDataFromIdHelper(spaceId);
        }

        /// <inheritdoc/>
        public ISpaceData SpaceDataFromScene(int sceneBuildIndex)
        {
            return _avaliableSpaces.FirstOrDefault(s => s.SceneBuildIndex == sceneBuildIndex);
        }

        private SpaceData SpaceDataFromIdHelper(Guid spaceId)
        {
            return _avaliableSpaces.FirstOrDefault(s => s.SpaceId == spaceId);
        }

        /// <inheritdoc/>
        public ISpaceData GetObjectSpace(GameObject gameObject)
        {
            return SpaceDataFromSceneHelper(gameObject.scene);
        }

        /// <inheritdoc/>
        public bool AreInSameSpace(GameObject gameObject1, GameObject gameObject2)
        {
            return gameObject1.scene == gameObject2.scene;
        }
    }
}