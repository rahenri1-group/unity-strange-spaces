﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <inheritdoc cref="ISpaceManager"/>
    [Dependency(
        contract: typeof(ISpaceManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class SpaceManager : BaseModule, ISpaceManager
    {
        /// <inheritdoc/>
        public override string ModuleName => "Space Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public SpaceManagerConfig Config = new SpaceManagerConfig();

        /// <inheritdoc/>
        public ISpaceData ActiveSpace { get; private set; }

        private readonly IAssetResourceManager _assetResourceManager;
        private readonly IEventBus _eventBus;

        private readonly Dictionary<Guid, RuntimeSpaceData> _loadedSpaces;
        
        private Scene _rootScene;
        
        /// <summary>
        /// Injection constructor
        /// </summary>
        public SpaceManager(
            IAssetResourceManager assetResourceManager,
            IEventBus eventBus,
            ILogRouter logger)
            : base(logger)
        {
            _assetResourceManager = assetResourceManager;
            _eventBus = eventBus;

            _avaliableSpaces = null;

            _spacesLoadingFlag = false;
            _loadingSpaces = new HashSet<Guid>();
            _loadedSpaces = new Dictionary<Guid, RuntimeSpaceData>();
            ActiveSpace = null;

            _spaceLoadProcessors = new HashSet<ISpaceLoadProcessor>();
            _spaceUnloadProcessors = new HashSet<ISpaceUnloadProcessor>();
        }

        /// <inheritdoc/>
        public async override UniTask Initialize()
        {
            InitializeRenderer();

            _portalLayerId = LayerUtil.GetLayerId(Config.PortalLayer);

            _avaliableSpaces = new List<SpaceData>();

            var availableSpaceKeys = await _assetResourceManager.GetAssetKeysWithLabel(Config.AvailableSpacesLabel);
            foreach (var spaceKey in availableSpaceKeys)
            {
                var spaceData = await _assetResourceManager.LoadAsset<SpaceData>(spaceKey);
                _avaliableSpaces.Add(spaceData);
            }

            _rootScene = SceneManager.CreateScene("SpaceManager Root");

            await base.Initialize();
        }

        /// <inheritdoc/>
        public async override UniTask Shutdown()
        {
            SetActiveSpace(null);

            while (_loadingSpaces.Count > 0)
            {
                // finish loading spaces, then unload them
                await UniTask.DelayFrame(1);
            }

            while (_loadedSpaces.Count > 0)
            {
                await UnloadSpace(_loadedSpaces.First().Key);
            }

            ActiveSpace = null;

            ShutdownRenderer();

            foreach (var spaceData in _avaliableSpaces)
            {
                _assetResourceManager.ReleaseAsset(spaceData);
            }
            
            _avaliableSpaces = null;

            await base.Shutdown();
        }

        /// <inheritdoc/>
        public void SetActiveSpace(Guid spaceId)
        {
            var space = SpaceDataFromIdHelper(spaceId);
            if (space == null)
            {
                ModuleLogError($"Attempt to make unknown space '{spaceId}' the active space");
                return;
            }

            SetActiveSpace(space);
        }

        /// <inheritdoc/>
        public void SetActiveSpace(ISpaceData space)
        {
            ActiveSpace = space;

            if (space == null)
            {
                SceneManager.SetActiveScene(_rootScene);
                CurrentRenderSettings = null;

                _eventBus.InvokeEvent(new SpaceActivateEvent
                {
                    SpaceData = null
                });
            }
            else if (_loadedSpaces.ContainsKey(space.SpaceId))
            {
                var runtimeSpaceData = _loadedSpaces[space.SpaceId];
                runtimeSpaceData.LastActivationTime = Time.time;
                SceneManager.SetActiveScene(runtimeSpaceData.Scene);

                CurrentRenderSettings = runtimeSpaceData.RenderSettings;
                // may need to call DynamicGI.UpdateEnvironment()

                _eventBus.InvokeEvent(new SpaceActivateEvent
                {
                    SpaceData = space
                });
            }
            else
            {
                ModuleLogError($"Attempt to set active space to non-loaded space '{space.Name}'");
            }
        }

        /// <inheritdoc/>
        public void StartMoveObjectToSpaceOperation(GameObject gameObject, ISpaceData space)
        {
            if (space == null)
            {
                SceneManager.MoveGameObjectToScene(gameObject, _rootScene);
            }
            else if (_loadedSpaces.ContainsKey(space.SpaceId))
            {
                var runtimeSpaceData = _loadedSpaces[space.SpaceId];
                SceneManager.MoveGameObjectToScene(gameObject, runtimeSpaceData.Scene);
                UpdateObjectRenderingLayers(gameObject, runtimeSpaceData.SpaceRenderLayer);
            }
            else
            {
                ModuleLogError($"Attempt to move object '{gameObject.name}' to non-active space '{space.Name}'");
            }
        }

        /// <inheritdoc/>
        public UniTask MoveObjectToSpaceAsync(GameObject gameObject, ISpaceData space)
        {
            if (space != null && !_loadedSpaces.ContainsKey(space.SpaceId))
            {
                ModuleLogError($"Attempt to move object '{gameObject.name}' to non-active space '{space.Name}'");
                return UniTask.CompletedTask;
            }

            // Animator states must be set to keep on disable before changing scene
            // Changing scene briefly disables and re-enables the object.
            // If keepAnimatorControllerStateOnDisable is not set to true, animations can break
            var animatorStates = new Dictionary<Animator, bool>();
            foreach (var animator in gameObject.GetComponentsInChildren<Animator>(true))
            {
                animatorStates.Add(animator, animator.keepAnimatorControllerStateOnDisable);
                animator.keepAnimatorControllerStateOnDisable = true;
            }

            if (space == null)
            {
                SceneManager.MoveGameObjectToScene(gameObject, _rootScene);
            }
            else
            {
                var runtimeSpaceData = _loadedSpaces[space.SpaceId];
                SceneManager.MoveGameObjectToScene(gameObject, runtimeSpaceData.Scene);
                UpdateObjectRenderingLayers(gameObject, runtimeSpaceData.SpaceRenderLayer);
            }

            foreach (var pair in animatorStates)
            {
                pair.Key.keepAnimatorControllerStateOnDisable = pair.Value;
            }

            return UniTask.CompletedTask;
        }

        public T[] FindLoadedInSpace<T>(ISpaceData space, bool includeInactive = false) 
            where T : class
        {
            if (!_loadedSpaces.ContainsKey(space.SpaceId))
            {
                ModuleLogError($"FindLoadedInSpace called non-active space {space.Name}");
                return new T[0];
            }

            var returnList = new List<T>();

            foreach (var root in _loadedSpaces[space.SpaceId].Scene.GetRootGameObjects())
            {
                returnList.AddRange(root.GetComponentsInChildren<T>(includeInactive));
            }

            return returnList.ToArray();
        }

        /// <inheritdoc/>
        public T[] FindLoaded<T>(bool includeInactive = false) 
            where T : class
        {
            var returnList = new List<T>();

            // case of first scene before the scenemanager has loaded a scene
            if (ActiveSpace == null)
            {
                foreach (var root in SceneManager.GetActiveScene().GetRootGameObjects())
                {
                    returnList.AddRange(root.GetComponentsInChildren<T>(includeInactive));
                }
            }

            foreach (var pair in _loadedSpaces)
            {
                foreach (var root in pair.Value.Scene.GetRootGameObjects())
                {
                    returnList.AddRange(root.GetComponentsInChildren<T>(includeInactive));
                }
            }

            return returnList.ToArray();
        }
    }
}
