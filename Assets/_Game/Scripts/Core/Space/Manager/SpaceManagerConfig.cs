﻿namespace Game.Core.Space
{
    /// <summary>
    /// Config for <see cref="SpaceManager"/>
    /// </summary>
    public class SpaceManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The maximum number of spaces allowed to be loaded at once. 
        /// If a load would go over this amount, the oldest space will be unloaded.
        /// If this value is 0 or less, no space will be unloaded
        /// </summary>
        public int MaxLoadedSpaces = 0;

        /// <summary>
        /// The maximum number of portals to go through when performing a physics cast
        /// </summary>
        public int MaxPhysicsCastsPortalDepth = 1;

        /// <summary>
        /// The asset label for all available spaces
        /// </summary>
        public string AvailableSpacesLabel = string.Empty;

        /// <summary>
        /// The layer used to detect portals. Used for space physics code.
        /// </summary>
        public string PortalLayer = string.Empty;

        /// <summary>
        /// The layers available to be used for spaces
        /// </summary>
        public string[] SpaceRenderLayers = new string[0];
    }
}