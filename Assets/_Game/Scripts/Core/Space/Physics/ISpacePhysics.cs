﻿using Game.Core.Math;
using Game.Core.Portal;
using System;
using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Enum defining how a physics query should interact with portals
    /// </summary>
    public enum QueryPortalInteraction
    {
        /// <summary>
        /// Stopped by all portals
        /// </summary>
        Block = 0,

        /// <summary>
        /// Ignores portals
        /// </summary>
        Ignore = 1,

        /// <summary>
        /// Go through active portals
        /// </summary>
        CastThroughAll = 2,

        /// <summary>
        /// Go through active portals with a render component
        /// </summary>
        CastThroughRenderer = 3,

        // Go through active portals with a transporter component
        CastThroughTransporter = 4
    }

    /// <summary>
    /// Physics functionality for testing against objects in a <see cref="ISpaceData"/>
    /// </summary>
    public interface ISpacePhysics : IModule
    {
        /// <summary>
        /// Performs raycast check
        /// </summary>
        bool Raycast(
            ISpaceData space, 
            Vector3 origin, 
            Vector3 direction, 
            out SpaceRayCastResult resultInfo, 
            float maxDistance = float.PositiveInfinity,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll,
            int layerMask = -5,  // everything except IgnoreRaycast layer
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal);

        /// <summary>
        /// Performs boxcast check
        /// </summary>
        bool BoxCast(
            ISpaceData space,
            Vector3 center, 
            Vector3 halfExtents, 
            Vector3 direction,
            out SpaceShapeCastResult resultInfo,
            Quaternion orientation,
            float maxDistance = float.PositiveInfinity,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll,
            int layerMask = -5,  // everything except IgnoreRaycast layer
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal);

        /// <summary>
        /// Searches for an instance of <typeparamref name="T"/> in a given sphere. 
        /// </summary>
        bool FindInSphere<T>(
            ISpaceData space,
            Vector3 position,
            float radius,
            out SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll,
            int layerMask = -5,  // everything except IgnoreRaycast layer
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal,
            Func<IPortal, float, float> onPortalRadiusModifier = null) where T : class;

        /// <summary>
        /// Searches for an instance of <typeparamref name="T"/> in a given cone. 
        /// </summary>
        bool FindClosestInCone<T>(
            ISpaceData space,
            Cone cone,
            out SpaceFindResult<T> resultInfo,
            QueryPortalInteraction portalInteraction = QueryPortalInteraction.CastThroughAll,
            int layerMask = -5,  // everything except IgnoreRaycast layer
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) where T : class;
    }
}
