﻿using Game.Core.Portal;

namespace Game.Core.Space
{
    /// <summary>
    /// Result struct for search queries to <see cref="ISpacePhysics"/>
    /// </summary>
    public struct SpaceFindResult<T> where T : class
    {
        /// <summary>
        /// The found behaviour
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// The space the behaviour was found in
        /// </summary>
        public ISpaceData Space { get; set; }

        /// <summary>
        /// Any portals used to get to the end behaviour from the start. 
        /// The order indicates the order they were traversed.
        /// </summary>
        public IPortal[] Portals { get; set; }
    }
}
