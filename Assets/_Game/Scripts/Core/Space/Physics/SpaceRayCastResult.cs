﻿using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Result struct for <see cref="ISpacePhysics"/> ray casts
    /// </summary>
    public struct SpaceRayCastResult
    {
        /// <summary>
        /// The space the cast ended in.
        /// </summary>
        public ISpaceData Space { get; set; }

        /// <summary>
        /// The position in space of the hit.
        /// </summary>
        public Vector3 HitPosition { get; set; }

        /// <summary>
        /// The collider the raycast hit
        /// </summary>
        public Collider Collider { get; set; }

        /// <summary>
        /// The distance from the ray's origin to the end point
        /// </summary>
        public float Distance { get; set; }

        /// <summary>
        /// The direction the ray hit the <see cref="Collider"/>. 
        /// This will be different than the original cast direction if the ray goes through a portal
        /// </summary>
        public Vector3 HitDirection { get; set; }

        /// <summary>
        /// The normal vector of the surface the ray hit
        /// </summary>
        public Vector3 HitNormal { get; set; }
    }
}
