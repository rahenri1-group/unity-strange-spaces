﻿using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Result struct for <see cref="ISpacePhysics"/> shape casts
    /// </summary>
    public struct SpaceShapeCastResult
    {
        /// <summary>
        /// The space the cast ended in.
        /// </summary>
        public ISpaceData Space { get; set; }

        /// <summary>
        /// The position in space of the hit.
        /// </summary>
        public Vector3 HitPosition { get; set; }

        /// <summary>
        /// The position of the cast shape at the end of the cast, either because we hit something or we hit the max range.
        /// </summary>
        public Vector3 CastShapePosition { get; set; }

        /// <summary>
        /// The rotation of the cast shape at the end of the cast, either because we hit something or we hit the max range.
        /// </summary>
        public Quaternion CastShapeRotation { get; set; }

        /// <summary>
        /// The collider the raycast hit
        /// </summary>
        public Collider Collider { get; set; }

        /// <summary>
        /// The distance from the ray's origin to the end point
        /// </summary>
        public float Distance { get; set; }
    }
}
