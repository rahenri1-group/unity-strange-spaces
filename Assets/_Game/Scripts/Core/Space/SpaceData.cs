﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <inheritdoc cref="ISpaceData"/>
    [CreateAssetMenu(menuName = "Game/Space/Space Data", order = -100)]
    public class SpaceData : BaseGuidScriptableObject, ISpaceData, IEquatable<SpaceData>
    {
        /// <inheritdoc/>
        public Guid SpaceId => Id;

        /// <inheritdoc/>
        public string Name => name;

        /// <summary>
        /// The scene build index of the space
        /// </summary>
        public int SceneBuildIndex => _sceneBuildIndex;
        /// <summary>
        /// The description of the space
        /// </summary>
        public string Description => _description;

        [SerializeField] private int _sceneBuildIndex = 0;
        [SerializeField] private string _description = string.Empty;
        
        [SerializeReference] [SelectType(typeof(ISpaceDataComponent))] private ISpaceDataComponent[] _spaceDataComponents = new ISpaceDataComponent[0];

        /// <inheritdoc/>
        protected override void OnValidate()
        {
            base.OnValidate();

            if (_sceneBuildIndex >= SceneManager.sceneCountInBuildSettings)
            {
                Debug.LogWarning($"'{name}' has invalid build index: {_sceneBuildIndex}");
            }
        }

        /// <inheritdoc/>
        public T GetAdditionalData<T>() where T : class, ISpaceDataComponent
        {
            foreach (var component in _spaceDataComponents)
            {
                T componentComponent = component as T;
                if (componentComponent != null)
                {
                    return componentComponent;
                }
            }

            return null;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as SpaceData);
        }

        public bool Equals(SpaceData other)
        {
            return SpaceId == other.SpaceId;
        }

        public bool Equals(ISpaceData other)
        {
            return SpaceId == other.SpaceId;
        }

        public override int GetHashCode() => SpaceId.GetHashCode();
    }
}