﻿using Game.Core.Event;
using Game.Core.DependencyInjection;

namespace Game.Core.Space
{
    /// <summary>
    /// A behaviour the needs to be aware of of when a space has finished loading or started unloading
    /// </summary>
    public class SpaceInjectedBehaviour : InjectedBehaviour
    {
        /// <summary>
        /// The <see cref="IEventBus"/>
        /// </summary>
        [Inject] public IEventBus EventBus = null;
        /// <summary>
        /// The <see cref="ISpaceLoader"/>
        /// </summary>
        [Inject] public ISpaceLoader SpaceLoader = null;
        /// <summary>
        /// The <see cref="ISpaceManager"/>
        /// </summary>
        [Inject] public ISpaceManager SpaceManager = null;

        protected ISpaceData CurrentSpace;

        /// <inheritdoc/>
        protected virtual void OnEnable()
        {
            CurrentSpace = SpaceManager.GetObjectSpace(gameObject);

            if (SpaceLoader.IsSpaceLoaded(CurrentSpace))
            {
                OnSpaceLoaded();
            }
            else
            {
                EventBus.Subscribe<SpaceLoadedEvent>(OnSpaceLoadEvent);
            }

            EventBus.Subscribe<SpaceUnloadingEvent>(OnSpaceUnloadingEvent);
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            EventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoadEvent);
            EventBus.Unsubscribe<SpaceUnloadingEvent>(OnSpaceUnloadingEvent);
        }

        private void OnSpaceLoadEvent(SpaceLoadedEvent spaceLoadEvent)
        {
            if (spaceLoadEvent.SpaceData == CurrentSpace)
            {
                OnSpaceLoaded();
            }
        }

        private void OnSpaceUnloadingEvent(SpaceUnloadingEvent spaceUnloadingEvent)
        {
            if (spaceUnloadingEvent.SpaceData == CurrentSpace)
            {
                OnSpaceUnloading();
            }
        }

        /// <summary>
        /// Called when a space has finished loading
        /// </summary>
        /// <returns></returns>
        protected virtual void OnSpaceLoaded() { }

        /// <summary>
        /// Called when a space has started unloading
        /// </summary>
        protected virtual void OnSpaceUnloading() { }
    }
}
