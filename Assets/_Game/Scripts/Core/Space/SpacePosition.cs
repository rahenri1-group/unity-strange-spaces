﻿using System;
using UnityEngine;

namespace Game.Core.Space
{
    /// <summary>
    /// Representation of a position in a <see cref="ISpaceData"/>
    /// </summary>
    public struct SpacePosition : IEquatable<SpacePosition>
    {
        public static readonly SpacePosition Empty = new SpacePosition(null, Vector3.zero);

        /// <summary>
        /// The space
        /// </summary>
        public ISpaceData Space { get; set; }

        /// <summary>
        /// The position
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SpacePosition(ISpaceData space, Vector3 position)
        {
            Space = space;
            Position = position;
        }

        public override string ToString()
        {
            return $"{Space?.Name ?? "None"} : {Position}";
        }

        public override bool Equals(object obj) => obj is SpacePosition other && this.Equals(other);

        public bool Equals(SpacePosition other)
        {
            return SpaceUtil.SpaceEquals(Space, other.Space) && Position.Equals(other.Position);
        }

        public override int GetHashCode()
        {
            return
            (
                Space,
                Position
            ).GetHashCode();
        }

        public static bool operator ==(SpacePosition lhs, SpacePosition rhs) => lhs.Equals(rhs);

        public static bool operator !=(SpacePosition lhs, SpacePosition rhs) => !(lhs == rhs);
    }
}
