﻿using UnityEngine;

namespace Game.Core.Space
{
    /// <inheritdoc cref="ISpaceRenderSettings"/>
    public class SpaceRenderSettings : ISpaceRenderSettings
    {
        /// <inheritdoc />
        public Material Skybox 
        {
            get => _skybox;
            set
            {
                _skybox = value; 
                ApplySettingsIfActive();
            }
        }

        /// <inheritdoc />
        public Light Sun 
        { 
            get => _sun;
            set
            {
                _sun = value;
                ApplySettingsIfActive();
            }
        }

        /// <inheritdoc />
        public Color AmbientLightColor 
        { 
            get => _ambientLightColor;
            set
            {
                _ambientLightColor = value;
                ApplySettingsIfActive();
            }
        }

        /// <inheritdoc />
        public Color RealtimeShadowsColor 
        { 
            get => _realtimeShadowsColor;
            set
            {
                _realtimeShadowsColor = value;
                ApplySettingsIfActive();
            }
        }

        private readonly ISpaceRenderer _spaceRenderer;

        private Material _skybox;
        private Light _sun;
        private Color _ambientLightColor;
        private Color _realtimeShadowsColor;

        /// <summary>
        /// Constructor
        /// </summary>
        public SpaceRenderSettings(
            ISpaceRenderer spaceRenderer,
            Material skybox,
            Light sun,
            Color ambientLightColor,
            Color realtimeShadowsColor)
        {
            _spaceRenderer = spaceRenderer;

            _skybox = skybox;
            _sun = sun;
            _ambientLightColor = ambientLightColor;
            _realtimeShadowsColor = realtimeShadowsColor;
        }

        /// <summary>
        /// Applies the stored settings to the current active scene
        /// </summary>
        public void ApplySettings()
        {
            RenderSettings.skybox = _skybox;
            RenderSettings.sun = _sun;
            RenderSettings.ambientLight = _ambientLightColor;
            RenderSettings.subtractiveShadowColor = _realtimeShadowsColor;
        }

        private void ApplySettingsIfActive()
        {
            if (_spaceRenderer.CurrentRenderSettings == this)
            {
                ApplySettings();
            }
        }
    }
}
