﻿namespace Game.Core.Space
{
    /// <summary>
    /// Utility class for spaces
    /// </summary>
    public static class SpaceUtil
    {
        /// <summary>
        /// Are two spaces the same
        /// </summary>
        /// <param name="space1"></param>
        /// <param name="space2"></param>
        /// <returns></returns>
        public static bool SpaceEquals(ISpaceData space1, ISpaceData space2)
        {
            if (space1 != null && space2 != null)
            {
                return space1.Equals(space2);
            }
            else if (space1 == null && space2 == null)
            {
                return true;
            }

            return false;
        }
        
    }
}
