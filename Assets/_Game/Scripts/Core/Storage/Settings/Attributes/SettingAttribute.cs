﻿using System;

namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// Attribute indicating the property is a setting. Only works in <see cref="ISettingsGroup"/> based classes.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property,
        AllowMultiple = false,
        Inherited = false
    )]
    public class SettingAttribute : Attribute { }
}
