﻿using Cysharp.Threading.Tasks;
using Game.Core.Event;
using Game.Core.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Game.Core.Storage.Settings
{
    /// <inheritdoc cref="ISettingsManager"/>
    public abstract class BaseSettingsManager<T> : BaseModule, ISettingsManager 
        where T : BaseSettingsManagerConfig, new()
    {
        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public T Config = new T();

        /// <inheritdoc/>
        public ISettingsProfile ActiveProfile => _activeProfile;
        /// <inheritdoc/>
        public string[] AvailableProfiles => _availableProfileNames.ToArray();

        protected readonly IEventBus EventBus;
        protected readonly IJsonDeserializer JsonDeserializer;
        protected readonly IJsonSerializer JsonSerializer;

        protected abstract Dictionary<string, Type> AvailableSettingsGroups { get; }

        private SettingsProfile _activeProfile;
        private Dictionary<string, SettingsProfile> _allProfiles;
        private List<string> _availableProfileNames;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseSettingsManager(
            IEventBus eventBus,
            IJsonDeserializer jsonDeserializer,
            IJsonSerializer jsonSerializer,
            ILogRouter logger)
            : base(logger)
        {
            EventBus = eventBus;
            JsonDeserializer = jsonDeserializer;
            JsonSerializer = jsonSerializer;

            _activeProfile = null;

            _allProfiles = new Dictionary<string, SettingsProfile>();
            _availableProfileNames = new List<string>();
        }

        /// <inheritdoc/>
        public override async UniTask Initialize()
        {
            await LoadAllProfiles();

            if (!_allProfiles.ContainsKey(Config.DefaultProfileName.ToLower()))
            {
                await CreateProfile(Config.DefaultProfileName);
            }

            ChangeProfile(Config.DefaultProfileName);

            await base.Initialize();
        }

        /// <inheritdoc/>
        public override async UniTask Shutdown()
        {
            if (_activeProfile != null)
            {
                foreach (var group in _activeProfile.SettingsGroups)
                    group.SettingsGroupUpdated -= OnActiveProfileSettingsGroupUpdate;

                _activeProfile = null;
            }

            await base.Shutdown();
        }

        /// <inheritdoc/>
        public UniTask CreateProfile(string profileName)
        {
            var profileKey = profileName.ToLower();

            if (_allProfiles.ContainsKey(profileKey))
            {
                throw new StorageException($"There already exists a profile named {profileName}");
            }

            var profile = new SettingsProfile(profileName);
            foreach (var pair in AvailableSettingsGroups)
            {
                var settingsGroupType = pair.Value;
                profile.AddSettingsGroup((ISettingsGroup)Activator.CreateInstance(settingsGroupType));
            }

            SaveProfile(profile);

            _allProfiles.Add(profileKey, profile);
            _availableProfileNames.Add(profileName);

            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public void ChangeProfile(string profileName)
        {
            var profileKey = profileName.ToLower();
            if (!_allProfiles.ContainsKey(profileKey))
            {
                throw new StorageException($"Profile {profileName} does not exist");
            }

            if (_activeProfile != null)
            {
                foreach (var group in _activeProfile.SettingsGroups)
                    group.SettingsGroupUpdated -= OnActiveProfileSettingsGroupUpdate;
            }

            _activeProfile = _allProfiles[profileKey];

            ModuleLogInfo($"Using profile '{profileName}'");

            foreach (var group in _activeProfile.SettingsGroups)
                group.SettingsGroupUpdated += OnActiveProfileSettingsGroupUpdate;

            EventBus.InvokeEvent(new ActiveSettingsProfileChangedEvent
            {
                SettingsProfile = _activeProfile
            });
        }

        private void OnActiveProfileSettingsGroupUpdate(ISettingsGroup settingsGroup)
        {
            EventBus.InvokeEvent(new ActiveSettingsProfileChangedEvent
            {
                SettingsProfile = _activeProfile
            });

            SaveProfile(_activeProfile);
        }

        private async UniTask LoadAllProfiles()
        {
            var directoryPath = Path.Combine(Application.persistentDataPath, Config.SettingsDirectory);
            if (!Directory.Exists(directoryPath)) return;

            try
            {
                foreach (var filePath in Directory.EnumerateFiles(directoryPath, "*.json"))
                {
                    var json = await File.ReadAllTextAsync(filePath, Encoding.UTF8);
                    var rootKeys = JsonDeserializer.KeyValues(json);

                    if (rootKeys["Version"] != "1")
                    {
                        throw new StorageException($"Unknown settings version for {filePath}");
                    }

                    var profile = new SettingsProfile(rootKeys["ProfileName"]);
                    var settingsGroupsJson = JsonDeserializer.KeyValues(rootKeys["SettingsGroups"]);
                    foreach (var pair in settingsGroupsJson)
                    {
                        var name = pair.Key;
                        var settingsJson = pair.Value;
                        if (AvailableSettingsGroups.ContainsKey(name))
                        {
                            var settingsGroupType = AvailableSettingsGroups[name];
                            var settingsGroup = (ISettingsGroup)JsonDeserializer.Deserialize(settingsJson, settingsGroupType);
                            profile.AddSettingsGroup(settingsGroup);
                        }
                        else
                        {
                            throw new StorageException($"Unknown settings group {name}");
                        }   
                    }

                    _allProfiles.Add(profile.ProfileName.ToLower(), profile);
                    _availableProfileNames.Add(profile.ProfileName);
                }
            }
            catch (StorageException e)
            {
                ModuleLogException(e);
            }
        }

        private void SaveProfile(SettingsProfile profile)
        {
            var json = JsonSerializer.SerializeHumanReadable(profile);

            var directoryPath = Path.Combine(Application.persistentDataPath, Config.SettingsDirectory);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            var fullPath = Path.Combine(directoryPath, profile.ProfileName.ToLower() + ".json");

            File.WriteAllText(fullPath, json, Encoding.UTF8);
        }
    }
}
