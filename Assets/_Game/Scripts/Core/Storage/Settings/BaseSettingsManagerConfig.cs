﻿namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// Config for <see cref="BaseSettingsManager{T}"/>
    /// </summary>
    public abstract class BaseSettingsManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The default directory to save settings to
        /// </summary>
        public string SettingsDirectory = "Settings";
        
        /// <summary>
        /// The default profile to use
        /// </summary>
        public string DefaultProfileName = "default";
    }
}
