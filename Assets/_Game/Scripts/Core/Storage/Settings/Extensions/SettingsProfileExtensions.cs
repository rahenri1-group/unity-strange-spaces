﻿using System.Linq;

namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// Extensions for <see cref="ISettingsProfile"/>
    /// </summary>
    public static class SettingsProfileExtensions
    {
        /// <summary>
        /// Gets a settings group of type <typeparamref name="T"/>. 
        /// Returns null if one is not present.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetSettingsGroup<T>(this ISettingsProfile profile) where T : class, ISettingsGroup
        {
            foreach (var settingsGroup in profile.SettingsGroups)
            {
                T typedSettingsGroup = settingsGroup as T;
                if (typedSettingsGroup != null)
                {
                    return typedSettingsGroup;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a settings group with name <paramref name="groupName"/>.
        /// Not case sensitive.
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static ISettingsGroup GetSettingsGroupByName(this ISettingsProfile profile, string groupName)
        {
            groupName = groupName.ToLower();

            return profile.SettingsGroups.FirstOrDefault(g => g.Name.ToLower() == groupName);
        }
    }
}
