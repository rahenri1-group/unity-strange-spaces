﻿using System;

namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// A group of settings
    /// </summary>
    public interface ISettingsGroup 
    { 
        /// <summary>
        /// The name of the settings group
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Event raised when a property has changed
        /// </summary>
        event Action<ISettingsGroup> SettingsGroupUpdated;
    }
}
