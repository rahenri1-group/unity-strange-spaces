﻿using Cysharp.Threading.Tasks;

namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// Manager class for the creation and loading of settings
    /// </summary>
    public interface ISettingsManager : IModule
    {
        /// <summary>
        /// The active setting profile
        /// </summary>
        ISettingsProfile ActiveProfile { get; }

        /// <summary>
        /// The names of all available profiles
        /// </summary>
        string[] AvailableProfiles { get; }

        /// <summary>
        /// Creates a new profile and adds it to <see cref="AvailableProfiles"/>
        /// </summary>
        UniTask CreateProfile(string profileName);

        /// <summary>
        /// Changes the current <see cref="ActiveProfile"/>
        /// </summary>
        void ChangeProfile(string profileName);
    }
}
