﻿namespace Game.Core.Storage.Settings
{
    /// <summary>
    /// Profile containing multiple settings to apply
    /// </summary>
    public interface ISettingsProfile
    {
        /// <summary>
        /// The name of the settings profile
        /// </summary>
        string ProfileName { get; }

        /// <summary>
        /// Gets all settings groups
        /// </summary>
        ISettingsGroup[] SettingsGroups { get; }
    }
}
