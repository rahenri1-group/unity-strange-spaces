﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Game.Core.Storage.Settings
{
    /// <inheritdoc cref="ISettingsProfile"/>
    [Serializable]
    public class SettingsProfile : ISettingsProfile, ISerializable
    {
        /// <inheritdoc/>
        public string ProfileName { get; }

        /// <inheritdoc/>
        public ISettingsGroup[] SettingsGroups => _settingsGroups.ToArray();

        private readonly int _version;
        private List<ISettingsGroup> _settingsGroups;

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsProfile(string profileName)
        {
            _version = 1;

            ProfileName = profileName;

            _settingsGroups = new List<ISettingsGroup>();
        }

        /// <summary>
        /// Adds a setting group to the profile
        /// </summary>
        /// <param name="settingsGroup"></param>
        public void AddSettingsGroup(ISettingsGroup settingsGroup)
        {
            _settingsGroups.Add(settingsGroup);
        }

        //Deserialize
        protected SettingsProfile(SerializationInfo info, StreamingContext context)
        {
            _version = info.GetInt32("Version");
            ProfileName = info.GetString("ProfileName");
            _settingsGroups = new List<ISettingsGroup>();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Version", _version);
            info.AddValue("ProfileName", ProfileName);

            var groupMap = new Dictionary<string, ISettingsGroup>();
            foreach (var settingGroup in _settingsGroups)
            {
                groupMap.Add(settingGroup.Name, settingGroup);
            }

            info.AddValue("SettingsGroups", groupMap);
        }
    }
}
