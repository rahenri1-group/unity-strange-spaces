﻿using System;

namespace Game.Core.Storage
{
    /// <summary>
    /// Exception related to data storage
    /// </summary>
    public class StorageException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StorageException(string message)
            : base(message) { }
    }
}
