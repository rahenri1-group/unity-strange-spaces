﻿using Game.Core.Interaction;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.UI
{
    /// <summary>
    /// Implementation of a UI carousel using two <see cref="ILaserInteractable"/>s
    /// </summary>
    public class LaserUiCarouselComponent : MonoBehaviour
    {
        [SerializeField] private BoolReadonlyReference _allowWrapAround = null;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _rightButtonObj = null;
        private ILaserInteractable _rightButton;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _leftButtonObj = null;
        private ILaserInteractable _leftButton;

        [SerializeField] private TMP_Text _carouselText = null;

        private Func<int> _dataReadFunc;
        private Action<int> _dataWriteAction;
        private Func<int, string> _dataDisplayFunc;

        private int _carouselValue;
        private int _carouselCount;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_rightButtonObj);
            Assert.IsNotNull(_leftButtonObj);
            Assert.IsNotNull(_carouselText);

            _rightButton = _rightButtonObj.GetComponentAsserted<ILaserInteractable>();
            _leftButton = _leftButtonObj.GetComponentAsserted<ILaserInteractable>();

            _carouselValue = 0;
            _carouselCount = 1;
        }

        /// <summary>
        /// Initializes the carousel with data bindings
        /// </summary>
        public void Initialize(int carouselCount, Func<int> dataRead, Action<int> dataWrite, Func<int, string> dataDisplay)
        {
            _carouselCount = carouselCount;

            _dataReadFunc = dataRead;
            _dataWriteAction = dataWrite;
            _dataDisplayFunc = dataDisplay;

            Refresh();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _rightButton.InteractBegin += OnRightButton;
            _leftButton.InteractBegin += OnLeftButton;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _rightButton.InteractBegin -= OnRightButton;
            _leftButton.InteractBegin -= OnLeftButton;
        }

        private void OnRightButton(IInteractable sender, IInteractor interactor)
        {
            if (_dataWriteAction != null)
            {
                if (!_allowWrapAround && _carouselValue + 1 >= _carouselCount)
                {
                    return;
                }

                _carouselValue = (_carouselValue + 1) % _carouselCount;
                _dataWriteAction.Invoke(_carouselValue);

                Refresh();
            }
        }

        private void OnLeftButton(IInteractable sender, IInteractor interactor)
        {
            if (_dataWriteAction != null)
            {
                if (!_allowWrapAround && _carouselValue - 1 < 0)
                {
                    return;
                }

                _carouselValue = (_carouselValue + _carouselCount - 1) % _carouselCount;
                _dataWriteAction.Invoke(_carouselValue);

                Refresh();
            }
        }

        public void Refresh()
        {
            if (_dataReadFunc != null && _dataDisplayFunc != null)
            {
                _carouselValue = _dataReadFunc.Invoke();

                _carouselText.text = _dataDisplayFunc.Invoke(_carouselValue);
            }
        }
    }
}