﻿using Game.Core.Interaction;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Core.UI
{
    /// <summary>
    /// Implementation of a UI toggle using a <see cref="ILaserInteractable"/>
    /// </summary>
    public class LaserUiToggleBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _toggleButtonObj = null;
        private ILaserInteractable _toggleButton;

        [SerializeField] private TMP_Text _toggleText = null;

        private Func<bool> _dataReadFunc;
        private Action<bool> _dataWriteAction;
        private Func<bool, string> _dataDisplayFunc;

        private bool _value;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_toggleButtonObj);
            Assert.IsNotNull(_toggleText);

            _toggleButton = _toggleButtonObj.GetComponentAsserted<ILaserInteractable>();
        }

        /// <summary>
        /// Initializes the toggle with data bindings
        /// </summary>
        public void Initialize(Func<bool> dataRead, Action<bool> dataWrite, Func<bool, string> dataDisplay)
        {
            _dataReadFunc = dataRead;
            _dataWriteAction = dataWrite;
            _dataDisplayFunc = dataDisplay;

            Refresh();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            _toggleButton.InteractBegin += OnToggle;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _toggleButton.InteractBegin -= OnToggle;
        }

        private void OnToggle(IInteractable sender, IInteractor interactor)
        {
            if (_dataWriteAction != null)
            {
                _value = !_value;
                _dataWriteAction.Invoke(_value);

                Refresh();
            }
        }

        public void Refresh()
        {
            _value = _dataReadFunc.Invoke();

            _toggleText.text = _dataDisplayFunc.Invoke(_value);
        }
    }
}