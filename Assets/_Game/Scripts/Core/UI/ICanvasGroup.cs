﻿namespace Game.Core.UI
{
    /// <summary>
    /// Interface for a canvas group
    /// </summary>
    public interface ICanvasGroup
    {
        /// <summary>
        /// Is the canvas group visible
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// Displays the canvas immediately
        /// </summary>
        void Show();
        /// <summary>
        /// Displays the canvas with an animated transition
        /// </summary>
        void Show(float transitionDuration);

        /// <summary>
        /// HIdes the canvas immediately
        /// </summary>
        void Hide();
        /// <summary>
        /// Hides the canvas with an animated transition
        /// </summary>
        void Hide(float transitionDuration);
    }
}