﻿using System;

namespace Game.Core.UI
{
    /// <summary>
    /// A UI element that can trigger <see cref="CursorEnter"/> and <see cref="CursorExit"/> events.
    /// </summary>
    public interface ICursorHoverable : IGameObjectComponent
    {
        /// <summary>
        /// Event fired when the cursor hovers over the UI element
        /// </summary>
        event Action<ICursorHoverable> CursorEnter;

        /// <summary>
        /// Event fired when the cursor is no longer hovering over the UI element
        /// </summary>
        event Action<ICursorHoverable> CursorExit;
    }
}
