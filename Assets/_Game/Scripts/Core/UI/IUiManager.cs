﻿namespace Game.Core.UI
{
    /// <summary>
    /// The manager of all UI screens
    /// </summary>
    public interface IUiManager : IModule
    {
        /// <summary>
        /// Displays a ui screen
        /// </summary>
        /// <param name="screen"></param>
        void ShowScreenSpaceUiScreen(IUiScreen screen);

        /// <summary>
        /// Hides a ui screen
        /// </summary>
        /// <param name="screen"></param>
        void HideScreenSpaceUiScreen(IUiScreen screen);
    }
}
