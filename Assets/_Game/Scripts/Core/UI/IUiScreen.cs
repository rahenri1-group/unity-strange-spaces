﻿namespace Game.Core.UI
{
    /// <summary>
    /// Interface for a UI screen
    /// </summary>
    public interface IUiScreen : IGameObjectComponent
    {
        /// <summary>
        /// Is the screen currently visible
        /// </summary>
        bool Visible { get; }
        /// <summary>
        /// Does the screen use the cursor?
        /// </summary>
        bool UsesCursor { get; }

        /// <summary>
        /// Shows the screen
        /// </summary>
        void Show();
        /// <summary>
        /// Hides the screen
        /// </summary>
        void Hide();
    }
}