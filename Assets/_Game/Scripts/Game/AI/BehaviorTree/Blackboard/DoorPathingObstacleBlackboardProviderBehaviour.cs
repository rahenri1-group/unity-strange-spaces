﻿using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.Entity;
using Game.Core.Entity.Pathing;
using Game.Core.Physics;
using Game.Entity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.BehaviorTree
{
    public class DoorPathingObstacleBlackboardProviderBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IBehaviorTreeRunner))] private Component _runnerObj = null;
        private IBehaviorTreeRunner _runner;

        [SerializeField] private StringReadonlyReference _doorObstacleBlackboard = null;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _doorTriggerableObj = null;
        private ITriggerable _doorTriggerable;

        [SerializeField] [TypeRestriction(typeof(IEntityPathingAgent))] private Component _pathingAgentObj = null;
        private IEntityPathingAgent _pathingAgent;

        private DoorEntityBehaviour _door;
        private HashSet<Collider> _doorColliders;

        private void Awake()
        {
            Assert.IsNotNull(_runnerObj);
            Assert.IsNotNull(_doorTriggerableObj);
            Assert.IsNotNull(_pathingAgentObj);

            _runner = _runnerObj.GetComponentAsserted<IBehaviorTreeRunner>();
            _doorTriggerable = _doorTriggerableObj.GetComponentAsserted<ITriggerable>();
            _pathingAgent = _pathingAgentObj.GetComponentAsserted<IEntityPathingAgent>();

            _door = null;
            _doorColliders = new HashSet<Collider>();
        }

        private void OnEnable()
        {
            _pathingAgent.PathAcquired += OnPathAcquired;
            _doorTriggerable.TriggerEnter += OnDoorTriggerEnter;
            _doorTriggerable.TriggerExit += OnDoorTriggerExit;
        }

        private void OnDisable()
        {
            _pathingAgent.PathAcquired -= OnPathAcquired;
            _doorTriggerable.TriggerEnter -= OnDoorTriggerEnter;
            _doorTriggerable.TriggerExit -= OnDoorTriggerExit;
        }

        private void OnPathAcquired(IEntityPathingAgent agent)
        {
            if (_runner.BehaviorTree == null || string.IsNullOrEmpty(_doorObstacleBlackboard)) return;

            var doorVariable = _runner.BehaviorTree.GetVariable<IEntity>(_doorObstacleBlackboard);
            if (doorVariable == null) return;

            bool isDoorBlocking = false;

            if (_door != null)
            {
                foreach (var collider in _doorColliders)
                {
                    if (_pathingAgent.IsColliderInPath(collider))
                    {
                        isDoorBlocking = true;
                        break;
                    }
                }
            }

            doorVariable.Value = isDoorBlocking ? _door : null;
        }

        private void OnDoorTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            if (_runner.BehaviorTree == null || string.IsNullOrEmpty(_doorObstacleBlackboard)) return;

            Collider other = args.Collider;

            var detectedDoor = other.GetComponentInParent<DoorEntityBehaviour>();
            if (detectedDoor == null) return;

            if (_door != null && detectedDoor != _door) return;

            if (_door == null)
            {
                _door = detectedDoor;
            }

            _doorColliders.Add(other);

            if (_pathingAgent.HasPath && _pathingAgent.IsColliderInPath(other)) // check if new collider is in the way
            {
                var doorVariable = _runner.BehaviorTree.GetVariable<IEntity>(_doorObstacleBlackboard);
                if (doorVariable != null)
                {
                    doorVariable.Value = _door;
                }
            }
        }

        private void OnDoorTriggerExit(ITriggerable sender, TriggerableEventArgs args)
        {
            if (args.Collider == null || _runner.BehaviorTree == null || string.IsNullOrEmpty(_doorObstacleBlackboard)) return;

            Collider other = args.Collider;

            var detectedDoor = other.GetComponentInParent<DoorEntityBehaviour>();
            if (detectedDoor == null) return;

            if (detectedDoor != _door) return;

            _doorColliders.Remove(other);

            if (_doorColliders.Count == 0)
            {
                _door = null;
            }
        }
    }
}
