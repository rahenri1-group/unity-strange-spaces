﻿using Game.Core.AI.BehaviorTree;
using Game.Core;
using UnityEngine;
using Game.Entity;
using UnityEngine.Assertions;

namespace Game.AI.BehaviorTree
{
    /// <summary>
    /// Provides the health information of a <see cref="IMortalEntity"/> to a <see cref="IBehaviorTreeRunner"/>'s blackboard
    /// </summary>
    [RequireComponent(typeof(IMortalEntity))]
    public class EntityHealthBlackboardProviderBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(IBehaviorTreeRunner))] private Component _runnerObj = null;
        private IBehaviorTreeRunner _runner;

        [SerializeField] private StringReadonlyReference _entityAliveBlackboard = null;

        private IMortalEntity _entity;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_runnerObj);
            _runner = _runnerObj.GetComponentAsserted<IBehaviorTreeRunner>();

            _entity = this.GetComponentAsserted<IMortalEntity>();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (_entity.IsInitializationComplete)
            {
                UpdateBlackboard();
            }

            _entity.InitializationComplete += OnEntityInitialized;
            _entity.EntityDamaged += OnEntityDamaged;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _entity.InitializationComplete -= OnEntityInitialized;
            _entity.EntityDamaged -= OnEntityDamaged;
        }

        private void OnEntityInitialized()
        {
            UpdateBlackboard();
        }

        private void OnEntityDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            UpdateBlackboard();
        }

        private void UpdateBlackboard()
        {
            if (_runner.BehaviorTree == null) return;

            if (!string.IsNullOrEmpty(_entityAliveBlackboard))
            {
                var isAliveVariable = _runner.BehaviorTree.GetVariable<bool>(_entityAliveBlackboard);
                if (isAliveVariable != null)
                {
                    isAliveVariable.Value = _entity.IsAlive();
                }
            }
        }
    }
}