﻿using Game.AI.Sensor;
using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.BehaviorTree
{
    [RequireComponent(typeof(IPlayerSensor))]
    public class SensorBlackboardProviderBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(IBehaviorTreeRunner))] private Component _runnerObj = null;

        [SerializeField] private StringReadonlyReference _detectPlayerBlackboard = null;
        [SerializeField] private StringReadonlyReference _lastPlayerPositionBlackboard = null;

        private IBehaviorTreeRunner _runner;
        private IPlayerSensor _playerSensor;

        private void Awake()
        {
            Assert.IsNotNull(_runnerObj);

            _runner = _runnerObj.GetComponentAsserted<IBehaviorTreeRunner>();
            _playerSensor = this.GetComponentAsserted<IPlayerSensor>();
        }

        private void OnEnable()
        {
            _playerSensor.DetectionChanged += OnDetectionChanged;
            _playerSensor.PositionChanged += OnPositionChanged;
        }

        private void OnDisable()
        {
            _playerSensor.DetectionChanged -= OnDetectionChanged;
            _playerSensor.PositionChanged -= OnPositionChanged;
        }

        private void OnDetectionChanged()
        {
            if (_runner.BehaviorTree == null || string.IsNullOrEmpty(_detectPlayerBlackboard)) return;

            var detectionVariable = _runner.BehaviorTree.GetVariable<bool>(_detectPlayerBlackboard);
            if (detectionVariable != null)
            {
                detectionVariable.Value = _playerSensor.CanDetectPlayer;
            }
        }

        private void OnPositionChanged()
        {
            if (_runner.BehaviorTree == null || string.IsNullOrEmpty(_lastPlayerPositionBlackboard)) return;

            var positionVariable = _runner.BehaviorTree.GetVariable<SpacePosition>(_lastPlayerPositionBlackboard);
            if (positionVariable != null)
            {
                positionVariable.Value = _playerSensor.MostRecentPlayerPosition;
            }
        }
    }
}
