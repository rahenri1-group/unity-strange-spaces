﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Entity.Enemy;
using Game.Physics;
using Game.Player;
using System;
using System.Threading;
using UnityEngine;

namespace Game.AI.BehaviorTree
{
    public class EnemyAttackPlayer : BaseAnimationNode, ILeafNode
    {
        [SerializeField] private string _triggerName = "";
        [SerializeField] private int _attackDamage = 0;

        private IEnemyAttackTrigger _attackTrigger;

        private int _triggerHash;

        private bool _playerHitFlag;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _attackTrigger = Owner.Runner.GetComponentInChildren<IEnemyAttackTrigger>();

            _triggerHash = Animator.StringToHash(_triggerName);
            
            _playerHitFlag = false;
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            AnimationCompleteFlag = false;
            _playerHitFlag = false;

            _attackTrigger.PlayerHit += OnPlayerHit;

            Animator.SetTrigger(_triggerHash);

            await UniTask.WaitUntil(() => AnimationCompleteFlag, PlayerLoopTiming.Update);

            _attackTrigger.PlayerHit -= OnPlayerHit;

            // attacks can't be interrupted, don't throw until animation completes
            cancellationToken.ThrowIfCancellationRequested();

            if (_playerHitFlag)
                return true;
            else
                return false;
        }

        private void OnPlayerHit(IEnemyAttackTrigger sender, PlayerHitArgs args)
        {
            _playerHitFlag = true;

            args.Player.ApplyDamage(_attackDamage, args.HitLocation, DamageType.Generic);
        }
    }
}
