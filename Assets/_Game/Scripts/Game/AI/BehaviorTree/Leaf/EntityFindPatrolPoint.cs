﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.AI.Interest;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Space;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Game.AI.BehaviorTree
{
    /// <summary>
    /// Finds a random patrol point for an entity and stores it to the blackboard
    /// </summary>
    public class EntityFindPatrolPoint : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<SpacePosition> _nextPatrolPosition = null;

        [SerializeField] private float _minDistanceToPatrolPoint = 0f;

        [Inject] IInterestManager _interestManager = null;
        [Inject] ISpaceManager _spaceManager = null;

        private IDynamicEntity _entity;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _entity = Owner.Runner.GetComponent<IDynamicEntity>();
        }

        protected override UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var currentSpace = _spaceManager.GetEntitySpace(_entity);
            var patrolPoints = _interestManager.GetPointsOfInterestForSpace<PatrolPointBehaviour>(currentSpace)
                .Where(p => Vector3.Distance(_entity.EntityMovement.Position, p.Position) >= _minDistanceToPatrolPoint)
                .ToList();

            if (patrolPoints.Count > 0)
            {
                var patrolPoint = patrolPoints.RandomElement();

                _nextPatrolPosition.Value = new SpacePosition(_spaceManager.GetObjectSpace(patrolPoint.GameObject), patrolPoint.Position);

                return UniTask.FromResult(true);
            }
            else
            {
                return UniTask.FromResult(false);
            }
        }
    }
}
