﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.DependencyInjection;
using Game.Core.Entity.Pathing;
using Game.Core.Space;
using Game.Player;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.BehaviorTree
{
    public class EntityMoveToPlayer : BaseNode, ILeafNode
    {
        [SerializeField] private float _entitySpeed = 0f;
        [SerializeField] private float _pathingUpdateDelay = 0f;

        [Inject] private IPlayerManager _playerManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IEntityPathingAgent _pathingAgent;

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Assert.IsTrue(_entitySpeed > 0f);

            _pathingAgent = Owner.Runner.GetComponent<IEntityPathingAgent>();
        }

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            try
            {
                while (!_pathingAgent.PathingReady)
                {
                    _pathingAgent.StartAgent();

                    await UniTask.Delay(10, cancellationToken: cancellationToken);
                }

                _pathingAgent.AgentMaxSpeed = _entitySpeed;

                var pathingStartTime = Time.time;
                if (!_pathingAgent.SetDestination(new SpacePosition(_spaceManager.GetEntitySpace(_playerManager.Player), _playerManager.Player.EntityMovement.Position)))
                {
                    return false;
                }

                do
                {
                    if(Time.time - pathingStartTime >= _pathingUpdateDelay && _pathingAgent.PathingReady)
                    {
                        if (!_pathingAgent.SetDestination(new SpacePosition(_spaceManager.GetEntitySpace(_playerManager.Player), _playerManager.Player.EntityMovement.Position)))
                        {
                            return false;
                        }

                        pathingStartTime = Time.time;
                    }

                    await UniTask.Delay(10, delayTiming:PlayerLoopTiming.Update, cancellationToken: cancellationToken);

                } while (!_pathingAgent.IsAgentAtDestination);
            }
            catch (OperationCanceledException ex)
            {
                if (Owner.Runner != null)
                {
                    _pathingAgent.StopAgent();
                }

                throw ex;
            }

            return true;
        }
    }
}
