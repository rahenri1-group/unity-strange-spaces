﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.AI.BehaviorTree;
using Game.Core.Entity;
using Game.Entity;
using System.Threading;
using UnityEngine;

namespace Game.AI.BehaviorTree
{
    public class OpenDoor : BaseNode, ILeafNode
    {
        [BlackboardVariable] private IVariable<IEntity> _doorEntity = null;

        [SerializeField] private float _delayAfterOpen = 0f;

        protected override async UniTask<bool> Execute(CancellationToken cancellationToken)
        {
            var door = _doorEntity.Value as DoorEntityBehaviour;
            if (door == null)
            {
                return false;
            }

            door.Open();

            if (_delayAfterOpen > 0f)
            {
                await UniTask.Delay((int) (1000f * _delayAfterOpen), delayTiming: PlayerLoopTiming.Update, cancellationToken: cancellationToken);
            }

            return true;
        }
    }
}
