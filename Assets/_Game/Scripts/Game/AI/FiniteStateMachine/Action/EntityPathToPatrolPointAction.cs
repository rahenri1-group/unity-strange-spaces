﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.AI.FiniteStateMachine;
using Game.Core.AI.Interest;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Entity.Pathing;
using Game.Core.Space;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Game.AI.FiniteStateMachine
{
    public class EntityPathToPatrolPointAction : BaseAction
    {
        [SerializeField] private float _minPatrolPointDistance = 0f;

        [Inject] IInterestManager _interestManager = null;
        [Inject] ISpaceManager _spaceManager = null;

        private IEntity _entity;
        private IEntityPathingAgent _pathingAgent;

        private PatrolPointBehaviour _currentPatrolPoint;
        private bool _pathingStarted;

        public override void Initialize(IState state)
        {
            base.Initialize(state);

            _entity = State.Owner.Runner.GetComponent<IEntity>();
            _pathingAgent = State.Owner.Runner.GetComponent<IEntityPathingAgent>();

            _currentPatrolPoint = null;
            _pathingStarted = false;
        }

        public override void StateEnter()
        {
            base.StateEnter();

            _pathingStarted = false;
            _currentPatrolPoint = FindNewPatrolPoint();
        }

        public override UniTask StateExecute(CancellationToken cancellationToken)
        {
            if (_pathingAgent.PathingReady) 
            {
                if (!_pathingStarted && _currentPatrolPoint != null)
                {
                    _pathingAgent.SetDestination(new SpacePosition(_spaceManager.GetObjectSpace(_currentPatrolPoint.GameObject), _currentPatrolPoint.Position));
                    
                    _pathingStarted = true;
                }
            }

            return UniTask.CompletedTask;
        }

        public override void StateExit()
        {
            base.StateExit();

            _pathingAgent.StopAgent();
            _currentPatrolPoint = null;
        }

        private PatrolPointBehaviour FindNewPatrolPoint()
        {
            var currentSpace = _spaceManager.GetEntitySpace(_entity);
            var patrolPoints = _interestManager.GetPointsOfInterestForSpace<PatrolPointBehaviour>(currentSpace)
                .Where(p => Vector3.Distance(_pathingAgent.Position, p.Position) >= _minPatrolPointDistance)
                .ToList();

            if (patrolPoints.Count > 0)
            {
                return patrolPoints.RandomElement();
            }
            else
            {
                return null;
            }
        }
    }
}
