﻿using Cysharp.Threading.Tasks;
using Game.Core.AI.FiniteStateMachine;
using Game.Core.DependencyInjection;
using Game.Core.Entity.Pathing;
using Game.Core.Space;
using Game.Player;
using System.Threading;
using UnityEngine;

namespace Game.AI.FiniteStateMachine
{
    public class EntityPathToPlayerAction : BaseAction
    {
        [SerializeField] private float _pathingUpdateDelay = 0f;

        [Inject] private IPlayerManager _playerManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IEntityPathingAgent _pathingAgent;
        private float _pathingStartTime;

        public override void Initialize(IState state)
        {
            base.Initialize(state);

            _pathingAgent = State.Owner.Runner.GameObject.GetComponent<IEntityPathingAgent>();
            _pathingStartTime = 0f;
        }

        public override void StateEnter()
        {
            base.StateEnter();

            _pathingStartTime = 0f;
        }

        public override UniTask StateExecute(CancellationToken cancellationToken)
        {
            if (_pathingAgent.PathingReady)
            {
                if (Time.time - _pathingStartTime >= _pathingUpdateDelay)
                {
                    _pathingAgent.SetDestination(new SpacePosition(_spaceManager.GetEntitySpace(_playerManager.Player), _playerManager.Player.EntityMovement.Position));

                    _pathingStartTime = Time.time;
                }
            }

            return UniTask.CompletedTask;
        }

        public override void StateExit()
        {
            base.StateExit();

            _pathingAgent.StopAgent();
        }
    }
}
