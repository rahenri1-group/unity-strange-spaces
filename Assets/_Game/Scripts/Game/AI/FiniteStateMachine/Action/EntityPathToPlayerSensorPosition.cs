﻿using Cysharp.Threading.Tasks;
using Game.AI.Sensor;
using Game.Core;
using Game.Core.AI.FiniteStateMachine;
using Game.Core.Entity.Pathing;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Game.AI.FiniteStateMachine
{
    public class EntityPathToPlayerSensorPosition : BaseAction
    {
        [SerializeField] private float _pathingUpdateDelay = 0f;

        private IEntityPathingAgent _pathingAgent;
        private IPlayerSensor[] _playerSensors;

        private float _pathingStartTime;

        public override void Initialize(IState state)
        {
            base.Initialize(state);

            _pathingAgent = State.Owner.Runner.GetComponent<IEntityPathingAgent>();
            _playerSensors = State.Owner.Runner.GetComponentsInChildren<IPlayerSensor>();

            _pathingStartTime = 0f;
        }

        public override void StateEnter()
        {
            base.StateEnter();

            _pathingStartTime = 0f;
        }

        public override UniTask StateExecute(CancellationToken cancellationToken)
        {
            if (_pathingAgent.PathingReady)
            {
                if (Time.time - _pathingStartTime >= _pathingUpdateDelay)
                {
                    var sensor = _playerSensors
                        .OrderByDescending(s => s.DetectTime)
                        .First();

                    if (sensor.DetectTime > 0f)
                    {
                        _pathingAgent.SetDestination(sensor.MostRecentPlayerPosition);
                    }

                    _pathingStartTime = Time.time;
                }
            }

            return UniTask.CompletedTask;
        }

        public override void StateExit()
        {
            base.StateExit();

            _pathingAgent.StopAgent();
        }
    }
}
