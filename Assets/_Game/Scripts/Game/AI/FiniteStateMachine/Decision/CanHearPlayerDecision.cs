﻿using Game.AI.Sensor;
using Game.Core;
using Game.Core.AI.FiniteStateMachine;

namespace Game.AI.FiniteStateMachine
{
    public class CanHearPlayerDecision : BaseDecision
    {
        private IAudioPlayerSensor _playerSensor = null;

        public override void Initialize(IState state)
        {
            base.Initialize(state);

            if (_playerSensor == null)
            {
                _playerSensor = state.Owner.Runner.GetComponentInChildren<IAudioPlayerSensor>();
            }
        }

        public override bool Decide()
        {
            return _playerSensor.CanDetectPlayer;
        }
    }
}
