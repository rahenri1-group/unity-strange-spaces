﻿using Game.AI.Sensor;
using Game.Core;
using Game.Core.AI.FiniteStateMachine;

namespace Game.AI.FiniteStateMachine
{
    public class CanSeePlayerDecision : BaseDecision
    {
        private IVisualPlayerSensor _visualSensor = null;

        public override void Initialize(IState state)
        {
            base.Initialize(state);

            if (_visualSensor == null)
            {
                _visualSensor = state.Owner.Runner.GetComponentInChildren<IVisualPlayerSensor>();
            }
        }

        public override bool Decide()
        {
            return _visualSensor.CanDetectPlayer;
        }
    }
}
