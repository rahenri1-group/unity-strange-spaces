﻿using Game.Core.AI.FiniteStateMachine;
using Game.Core.DependencyInjection;
using Game.Player;
using UnityEngine;

namespace Game.AI.FiniteStateMachine
{
    public class PlayerFartherDecision : BaseDecision
    {
        [SerializeField] private float _playerDistance = 0f;

        [Inject] private IPlayerManager _playerManager = null;

        public override bool Decide()
        {
            return Vector3.Distance(_playerManager.Player.FeetPosition, State.Owner.Runner.GameObject.transform.position) > _playerDistance;
        }
    }
}
