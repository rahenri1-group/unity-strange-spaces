﻿using Game.Core;
using Game.Core.Audio;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Events;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.Sensor
{
    /// <inheritdoc cref="IAudioPlayerSensor"/>
    public class AudioPlayerSensorBehaviour : InjectedBehaviour, IAudioPlayerSensor
    {

        /// <inheritdoc/>
        public bool CanDetectPlayer
        {
            get => _canDetectPlayer;
            private set
            {
                if (_canDetectPlayer != value)
                {
                    _canDetectPlayer = value;
                    DetectionChanged?.Invoke();
                }
            }
        }
        [SerializeField][ReadOnly] private bool _canDetectPlayer;

        /// <inheritdoc/>
        public float DetectTime { get; private set; }

        /// <inheritdoc/>
        public SpacePosition MostRecentPlayerPosition
        {
            get => _mostRecentPlayerPosition;
            protected set
            {
                if (_mostRecentPlayerPosition != value)
                {
                    _mostRecentPlayerPosition = value;
                    PositionChanged?.Invoke();
                }
            }
        }
        private SpacePosition _mostRecentPlayerPosition;

        /// <inheritdoc/>
        public event Action DetectionChanged;
        /// <inheritdoc/>
        public event Action PositionChanged;

        [Inject] private ISpaceAudioManager _spaceAudioManager = null;
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] private IEventBus _eventBus = null;
        
        [SerializeField] private FloatReadonlyReference _detectionHoldDuration = null;

        [SerializeField] private FloatReadonlyReference _noiseLowRadius = null;
        [SerializeField] private FloatReadonlyReference _noiseMediumRadius = null;
        [SerializeField] private FloatReadonlyReference _noiseHighRadius = null;

        private IEnumerator _detectionHoldCoroutine;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_detectionHoldDuration > 0f);
            Assert.IsTrue(_noiseLowRadius > 0f);
            Assert.IsTrue(_noiseMediumRadius > _noiseLowRadius);
            Assert.IsTrue(_noiseHighRadius > _noiseMediumRadius);

            DetectTime = 0f;
            _canDetectPlayer = false;
            _mostRecentPlayerPosition = SpacePosition.Empty;

            _detectionHoldCoroutine = null;
        }

        private void OnEnable()
        {
            _eventBus.Subscribe<PlayerNoiseEvent>(OnPlayerNoise);
            
            if (CanDetectPlayer && Time.time - DetectTime < _detectionHoldDuration)
            {
                _detectionHoldCoroutine = DelayedClearPlayerDetection();
                StartCoroutine(_detectionHoldCoroutine);
            }
        }

        private void OnDisable()
        {
            _eventBus.Unsubscribe<PlayerNoiseEvent>(OnPlayerNoise);

            if (_detectionHoldCoroutine != null)
            {
                StopCoroutine(_detectionHoldCoroutine);
                _detectionHoldCoroutine = null;
            }
        }

        private void OnPlayerNoise(PlayerNoiseEvent eventArgs)
        {
            float maxDistance;
            switch (eventArgs.NoiseIntensity)
            {
                case Player.PlayerNoiseIntensity.High:
                    maxDistance = _noiseHighRadius;
                    break;    
                case Player.PlayerNoiseIntensity.Medium:
                    maxDistance = _noiseMediumRadius;
                    break;
                case Player.PlayerNoiseIntensity.Low:
                default:
                    maxDistance = _noiseLowRadius;
                    break;
            }
            
            if (_spaceAudioManager.GetAudioPath(
                new SpacePosition(_spaceManager.GetObjectSpace(gameObject), transform.position),
                eventArgs.NoisePosition,
                maxDistance, 
                out var pathInfo)
                && CalculateModifiedPathDistance(pathInfo) <= maxDistance)
            {
                DetectTime = Time.time;
                CanDetectPlayer = true;
                MostRecentPlayerPosition = new SpacePosition(_spaceManager.GetEntitySpace(eventArgs.Player), eventArgs.Player.FeetPosition);

                if (_detectionHoldCoroutine != null)
                {
                    StopCoroutine(_detectionHoldCoroutine);
                }
                _detectionHoldCoroutine = DelayedClearPlayerDetection();
                StartCoroutine(_detectionHoldCoroutine);
            }
        }

        private float CalculateModifiedPathDistance(IPortalPath<IPortalAudioLink> path)
        {
            float distance = 0;

            float runningScale = 1f;
            foreach (var step in path.Path)
            {
                distance += step.Distance / runningScale;

                // scale will only be applied to all subsequent steps
                var scaleDefinition = step.Portal?.GetAudioModifier<VolumeScaleDefinition>();
                if (scaleDefinition != null)
                {
                    runningScale = runningScale * scaleDefinition.CurrentVolumeScale;

                    if (runningScale == 0f)
                    {
                        return float.MaxValue;
                    }
                }
            }

            return distance;
        }

        private IEnumerator DelayedClearPlayerDetection()
        {
            yield return new WaitForSeconds(_detectionHoldDuration - (Time.time - DetectTime));
            
            CanDetectPlayer = false;

            _detectionHoldCoroutine = null;
        }

        /// <inheritdoc/>
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _noiseLowRadius);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, _noiseMediumRadius);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _noiseHighRadius);
        }
    }
}