﻿namespace Game.AI.Sensor
{
    public interface IAudioSensorRangeMultiplier
    {
        float AudioRangeMultiplier { get; }
    }
}
