﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using Game.Entity;
using Game.Player;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.Sensor
{
    public abstract class BasePlayerSensor : InjectedBehaviour, IPlayerSensor
    {
        /// <inheritdoc/>
        public bool CanDetectPlayer
        {
            get => _canDetectPlayer;
            private set
            {
                if (_canDetectPlayer != value)
                {
                    _canDetectPlayer = value;
                    DetectionChanged?.Invoke();
                }
            }
        }

        /// <inheritdoc/>
        public float DetectTime { get; protected set; }

        /// <inheritdoc/>
        public SpacePosition MostRecentPlayerPosition
        {
            get => _mostRecentPlayerPosition;
            protected set
            {
                if (_mostRecentPlayerPosition != value)
                {
                    _mostRecentPlayerPosition = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        /// <inheritdoc/>
        public event Action DetectionChanged;
        /// <inheritdoc/>
        public event Action PositionChanged;

        [Inject] protected ISpaceManager SpaceManager = null;

        protected IPlayer ObservedPlayer { get; private set; }

        [SerializeField] [ReadOnly] private bool _canDetectPlayer;
        private SpacePosition _mostRecentPlayerPosition;

        [SerializeField] private FloatReadonlyReference _detectionDelay = null;
        [SerializeField] private FloatReadonlyReference _detectionFrequency = null;

        private IEnumerator _scanCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_detectionDelay >= 0f);
            Assert.IsTrue(_detectionFrequency > 0f);

            CanDetectPlayer = false;

            DetectTime = 0f;
            MostRecentPlayerPosition = SpacePosition.Empty;
            ObservedPlayer = null;

            _scanCoroutine = null;
        }

        protected virtual void OnEnable()
        {
            ObservedPlayer = null;

            _scanCoroutine = Scan();
            StartCoroutine(_scanCoroutine);
        }

        protected virtual void OnDisable()
        {
            if (_scanCoroutine != null)
            {
                StopCoroutine(_scanCoroutine);
                _scanCoroutine = null;
            }
        }

        protected abstract IPlayer ScanForPlayer();

        private IEnumerator Scan()
        {
            float scanDelay = 1f / _detectionFrequency;
            YieldInstruction wait = new WaitForSeconds(scanDelay);

            yield return new WaitForSeconds(Core.Random.Default.Range(0.1f * scanDelay, scanDelay)); ;

            float initialDetectTime = 0f;
            while (true)
            {
                ObservedPlayer = ScanForPlayer();
                if (ObservedPlayer != null && !ObservedPlayer.IsAlive())
                {
                    ObservedPlayer = null;
                }

                if (ObservedPlayer != null)
                {
                    if (initialDetectTime == 0f)
                    {
                        initialDetectTime = Time.time;
                    }

                    if (_detectionDelay == 0f || Time.time - initialDetectTime >= _detectionDelay)
                    {
                        DetectTime = Time.time;                      
                        MostRecentPlayerPosition = new SpacePosition(SpaceManager.GetEntitySpace(ObservedPlayer), ObservedPlayer.FeetPosition);
                        CanDetectPlayer = true;
                    }
                }
                else
                {
                    initialDetectTime = 0f;
                    CanDetectPlayer = false;
                }

                yield return wait;
            }
        }
    }
}
