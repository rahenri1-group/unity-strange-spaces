﻿using Game.Core.Space;
using System;

namespace Game.AI.Sensor
{
    public interface IPlayerSensor
    {
        /// <summary>
        /// Can the sensor currently detect the player
        /// </summary>
        bool CanDetectPlayer { get; }

        /// <summary>
        /// The most recent time the sensor detected the player
        /// </summary>
        float DetectTime { get; }

        /// <summary>
        /// The most recent position the player was detected
        /// </summary>
        SpacePosition MostRecentPlayerPosition { get; }

        /// <summary>
        /// Event for when <see cref="CanDetectPlayer"/> has changed
        /// </summary>
        event Action DetectionChanged;

        /// <summary>
        /// Event for when <see cref="MostRecentPlayerPosition"/> has changed
        /// </summary>
        event Action PositionChanged;
    }
}
