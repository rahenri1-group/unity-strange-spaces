﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Math;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.AI.Sensor
{
    public class VisualPlayerSensorBehaviour : BasePlayerSensor, IVisualPlayerSensor
    {
        [SerializeField] private FloatReadonlyReference _coneAngle = null;
        [SerializeField] private FloatReadonlyReference _coneDistance = null;

        [SerializeField] [Layer] private int[] _queryLayers = new int[0];
        [SerializeField] [Layer] private int[] _blockingLayers = new int[0];

        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISpacePhysics _spacePhysics = null;

        private int _queryMask, _blockingMask;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_coneAngle > 0f && _coneAngle < 180f);
            Assert.IsTrue(_coneDistance > 0f);

            _queryMask = LayerUtil.ConstructMaskForLayers(_queryLayers);
            _blockingMask = LayerUtil.ConstructMaskForLayers(_blockingLayers);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _eventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleported);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _eventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleported);
        }

        private void OnEntityTeleported(EntityPostTeleportEvent eventArgs)
        {
            if (!CanDetectPlayer || eventArgs.Entity != ObservedPlayer)
            {
                return;
            }

            DetectTime = Time.time;
            MostRecentPlayerPosition = new SpacePosition(SpaceManager.GetEntitySpace(ObservedPlayer), ObservedPlayer.FeetPosition);
        }

        protected override IPlayer ScanForPlayer()
        {
            var space = SpaceManager.GetObjectSpace(gameObject);

            var sensorCone = new Cone(transform.position, transform.forward, _coneAngle, _coneDistance);
            bool playerFound = _spacePhysics.FindClosestInCone<IPlayer>(
                space,
                sensorCone,
                out var resultInfo,
                QueryPortalInteraction.CastThroughTransporter,
                _queryMask);

            if (playerFound)
            {
                // verify there are no blocking obstacles

                // first check from portal to portal
                for (int i = 0; i < resultInfo.Portals.Length; i++)
                {
                    Vector3 startPortalPosition = i == 0 ? transform.position : resultInfo.Portals[i - 1].EndPoint.Transform.position;
                    IPortal portal = resultInfo.Portals[i];

                    var portalCheckHit =_spacePhysics.Raycast(
                        portal.Space,
                        startPortalPosition,
                        (portal.Transform.position - startPortalPosition).normalized,
                        out var portalCheckResult,
                        Vector3.Distance(startPortalPosition, portal.Transform.position),
                        QueryPortalInteraction.Block,
                        _blockingMask);

                    if (portalCheckHit)
                    {
                        // if we hit anything other than a portal we have a blocker
                        if (portalCheckResult.Collider.GetComponentInParent<IPortal>() == null)
                        {
                            return null;
                        }
                    }
                }

                // then check player
                Vector3 origin = resultInfo.Portals.Length == 0 ? transform.position : resultInfo.Portals[resultInfo.Portals.Length - 1].EndPoint.Transform.position;
                IPlayer player = resultInfo.Result;

                var playerCheckHit = _spacePhysics.Raycast(
                    SpaceManager.GetEntitySpace(player),
                    origin,
                    (player.EntityMovement.Position - origin).normalized,
                    out var playerCheckResult,
                    Vector3.Distance(origin, player.EntityMovement.Position),
                    QueryPortalInteraction.Block,
                    _blockingMask);

                if (playerCheckHit)
                {
                    // if we hit anything other than a player we have a blocker
                    if (playerCheckResult.Collider.GetComponentInParent<IPlayer>() == null)
                    {
                        return null;
                    }
                }

                return player;
            }

            return null;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;

            GizmosUtil.DrawCone(transform.position, transform.forward, _coneAngle, _coneDistance);
        }
    }
}
