﻿using UnityEngine;

namespace Game
{
    public partial class App
    {
        public class AppConfig
        {
            public int PostInitializeSceneIndex { get; set; } = 1; // Home scene
            public string InjectionConfigFileName { get; set; } = "PC-Config";
        }

        private static void ApplyAllConfigOverrides()
        {
#if UNITY_EDITOR
            ApplyPlayerPrefConfigOverrides();
#endif

            ApplyCommandLineConfigOverrides();
        }

#if UNITY_EDITOR
        private static void ApplyPlayerPrefConfigOverrides()
        {
            Config.InjectionConfigFileName = PlayerPrefs.GetString("Editor_InjectionConfigFileName", Config.InjectionConfigFileName);
        }
#endif

        private static void ApplyCommandLineConfigOverrides()
        {
            string[] args = System.Environment.GetCommandLineArgs();

            foreach (var arg in args)
            {
                var commandArg = arg
                    .ToLower()
                    .Split('=');

                if ("--pc_mode".Equals(commandArg[0]))
                {
                    Config.InjectionConfigFileName = "PC-Config";
                }
                else if ("--vr_mode".Equals(commandArg[0]))
                {
                    Config.InjectionConfigFileName = "VR-Config";
                }
                else if ("--start_scene".Equals(commandArg[0]) && commandArg.Length == 2)
                {
                    if (int.TryParse(commandArg[1], out int startScene))
                    {
                        Config.PostInitializeSceneIndex = startScene;
                    }
                }
            }
        }
    }
}
