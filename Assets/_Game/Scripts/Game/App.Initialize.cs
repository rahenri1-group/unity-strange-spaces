﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Space;
using Game.Serialization.Json;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public partial class App
    {
        private async UniTask LoadInjectionConfig()
        {
            var jsonModule = new NewtonsoftJson();
            await jsonModule.Initialize();

            var injectionConfig = (InjectionConfig) await Resources.LoadAsync<InjectionConfig>(Config.InjectionConfigFileName);

            var configTexts = new List<string>();
            foreach (var text in injectionConfig.InjectionJsonTexts)
            {
                configTexts.Add(text.text);
            }

            _dependencyContainer = new Container();
            _dependencyContainer.RegisterSingleton(jsonModule);

            _dependencyContainer.RegisterJson(configTexts.ToArray());

            ModuleContext.DiContainer = _dependencyContainer;

            _logger = _dependencyContainer.Resolve<ILogRouter>();
            _logger.LogInfo("Application", $"Using config {Config.InjectionConfigFileName}");
        }

        private async UniTaskVoid Initialize()
        {
            await LoadInjectionConfig();

            await ModuleContext.InitializeModules();

            // always spend at least 1 frame initializing
            await UniTask.Yield();

            await SceneManager.UnloadSceneAsync(0);

            var spaceLoader = _dependencyContainer.Resolve<ISpaceLoader>();
            var spaceHelper = _dependencyContainer.Resolve<ISpaceSceneHelper>();

            var spaceData = spaceHelper.SpaceDataFromScene(Config.PostInitializeSceneIndex);
            await spaceLoader.LoadSpace(spaceData);

#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged += OnEditorPlayModeStateChanged;
#else
            UnityEngine.Application.wantsToQuit += OnUnityWantsToQuit;
#endif
        }
    }
}
