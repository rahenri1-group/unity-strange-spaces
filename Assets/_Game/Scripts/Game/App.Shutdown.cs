﻿using Cysharp.Threading.Tasks;
using Game.Core;

namespace Game
{
    public partial class App
    {
        private async UniTaskVoid ShutdownApplication()
        {
            _logger.LogInfo("Shutdown initiated");

            await ModuleContext.ShutdownModules();

            _logger.LogInfo("Shutdown completed");

#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged -= OnEditorPlayModeStateChanged;
            UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.wantsToQuit -= OnUnityWantsToQuit;
            UnityEngine.Application.Quit();
#endif
        }

        private bool OnUnityWantsToQuit()
        {
            if (ModuleContext.ModulesInitialized)
            {
                ShutdownApplication().Forget();

                return false;
            }
            else
            {
                return true;
            }
        }

#if UNITY_EDITOR
        private void OnEditorPlayModeStateChanged(UnityEditor.PlayModeStateChange state)
        {
            if (state == UnityEditor.PlayModeStateChange.ExitingPlayMode)
            {
                // forces unity to use our shutdown method when exiting playmode
                if (ModuleContext.ModulesInitialized)
                {
                    UnityEditor.EditorApplication.isPlaying = true;

                    ShutdownApplication().Forget();
                }
            }
        }
#endif
    }
}
