using Game.Core;
using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game
{
    public partial class App
    {
        private static App Instance = null;
        public static AppConfig Config { get; } = new AppConfig();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void InitUniTaskLoop()
        {
            var loop = UnityEngine.LowLevel.PlayerLoop.GetCurrentPlayerLoop();
            Cysharp.Threading.Tasks.PlayerLoopHelper.Initialize(ref loop);
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void OnAppStart()
        {
            if (Instance != null) return;

            ApplyAllConfigOverrides();

            Instance = new App();
            Instance.Initialize().Forget();
        }

        private IInjectionContainer _dependencyContainer;

        private ILogRouter _logger;

        private App() 
        {
            _dependencyContainer = null;
            _logger = null;
        }
    }
}
