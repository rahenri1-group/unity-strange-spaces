﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Audio;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Storage.Settings;

namespace Game.Audio
{
    /// <summary>
    /// Applies the settings in <see cref="IIAudioSettingsGroup"/> to <see cref="IAudioMixer"/>
    /// </summary>
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class AudioMixerSettingApplier : BaseModule
    {
        /// <inheritdoc/>
        public override string ModuleName => "Audio Mixer Settings Applier";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        private readonly IAudioMixer _audioMixer;
        private readonly IEventBus _eventBus;
        private readonly ISettingsManager _settingsManager;

        private IAudioSettingsGroup _audioSettingsGroup;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public AudioMixerSettingApplier(
            IAudioMixer audioMixer,
            IEventBus eventBus,
            ILogRouter logger,
            ISettingsManager settingsManager)
            : base(logger)
        {
            _audioMixer = audioMixer;
            _eventBus = eventBus;
            _settingsManager = settingsManager;
        }

        /// <inheritdoc/>
        public override UniTask Initialize()
        {
            _audioSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IAudioSettingsGroup>();

            ApplyAudioSettingsToMixer();

            _audioSettingsGroup.SettingsGroupUpdated += OnAudioSettingsUpdated;
            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);

            return base.Initialize();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _audioSettingsGroup.SettingsGroupUpdated -= OnAudioSettingsUpdated;
            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);

            return base.Shutdown();
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventData)
        {
            _audioSettingsGroup.SettingsGroupUpdated -= OnAudioSettingsUpdated;
            _audioSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IAudioSettingsGroup>();
            _audioSettingsGroup.SettingsGroupUpdated += OnAudioSettingsUpdated;
        }

        private void OnAudioSettingsUpdated(ISettingsGroup group)
        {
            ApplyAudioSettingsToMixer();
        }

        private void ApplyAudioSettingsToMixer()
        {
            _audioMixer.MasterVolume = _audioSettingsGroup.MasterVolume / 100f;
            _audioMixer.MusicVolume = _audioSettingsGroup.MusicVolume / 100f;
            _audioMixer.EffectsVolume = _audioSettingsGroup.EffectsVolume / 100f;
        }
    }
}