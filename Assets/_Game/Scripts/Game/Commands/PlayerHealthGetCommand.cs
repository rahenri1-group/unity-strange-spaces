﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Player;
using System;

namespace Game.Command
{
    /// <summary>
    /// Command to get the current health of the player
    /// </summary>
    [Serializable]
    public class PlayerHealthGetCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="PlayerHealthGetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class PlayerHealthGetCommandProcessor : BaseConsoleCommandProcessor<PlayerHealthGetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "player-health-get";

        /// <inheritdoc/>
        public override string CommandDescription => "Gets the current health of the player";

        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PlayerHealthGetCommandProcessor(
            ILogRouter logger,
            IPlayerManager playerManager)
        {
            _logger = logger;
            _playerManager = playerManager;
        }

        /// <inheritdoc/>
        public override UniTask Execute(PlayerHealthGetCommand command)
        {
            _logger.LogInfo(_playerManager.ModuleName, $"{_playerManager.Player.HealthCurrent} / {_playerManager.Player.HealthMax}");

            return UniTask.CompletedTask;
        }
    }
}
