﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Player;
using System;

namespace Game.Command
{
    /// <summary>
    /// Command to set the players health
    /// </summary>
    [Serializable]
    public class PlayerHealthSetCommand : ICommand
    {
        /// <summary>
        /// The new health value for the player
        /// </summary>
        public int Health;
    }

    /// <summary>
    /// Command processor for <see cref="PlayerHealthSetCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class PlayerHealthSetCommandProcessor : BaseConsoleCommandProcessor<PlayerHealthSetCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "player-health-set";

        /// <inheritdoc/>
        public override string CommandDescription => "Sets the current health of the player";

        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PlayerHealthSetCommandProcessor(
            ILogRouter logger,
            IPlayerManager playerManager)
        {
            _logger = logger;
            _playerManager = playerManager;
        }

        /// <inheritdoc/>
        public override PlayerHealthSetCommand ParseCommand(string[] args)
        {
            if (args.Length < 1) return null;

            if (!int.TryParse(args[0], out int health))
            {
                _logger.LogWarning(_playerManager.ModuleName, $"Invalid health value '{args[0]}'");
                return null;
            }

            return new PlayerHealthSetCommand
            {
                Health = health
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(PlayerHealthSetCommand command)
        {
            _playerManager.Player.SetPlayerHealth(command.Health);

            return UniTask.CompletedTask;
        }
    }
}
