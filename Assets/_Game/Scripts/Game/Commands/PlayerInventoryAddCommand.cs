﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Player;
using System;

namespace Game.Command
{
    [Serializable]
    public class PlayerInventoryAddCommand : ICommand
    {
        /// <summary>
        /// The name of the item to add
        /// </summary>
        public string ItemName;

        /// <summary>
        /// The amount of the item to add
        /// </summary>
        public int Quantity;
    }

    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class PlayerInventoryAddCommandProcessor : BaseConsoleCommandProcessor<PlayerInventoryAddCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "inventory-add";

        /// <inheritdoc/>
        public override string CommandDescription => "Adds an item to the player's inventory";

        private readonly IItemDefinitionManager _itemDefinitionManager;
        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PlayerInventoryAddCommandProcessor(
            IItemDefinitionManager itemDefinitionManager,
            ILogRouter logger,
            IPlayerManager playerManager)
        {
            _itemDefinitionManager = itemDefinitionManager;
            _logger = logger;
            _playerManager = playerManager;
        }

        /// <inheritdoc/>
        public override PlayerInventoryAddCommand ParseCommand(string[] args)
        {
            if (args.Length > 0)
            {
                int quantity = 1;
                if (args.Length > 1)
                {
                    if (!int.TryParse(args[1], out quantity))
                    {
                        quantity = 1;
                    }
                }

                return new PlayerInventoryAddCommand
                {
                    ItemName = args[0],
                    Quantity = quantity
                };
            }

            return null;
        }

        /// <inheritdoc/>
        public override UniTask Execute(PlayerInventoryAddCommand command)
        {
            var itemDefinition = _itemDefinitionManager.GetItemDefinition(command.ItemName);
            if (itemDefinition == null)
            {
                _logger.LogWarning($"Invalid item name {command.ItemName}");
                return UniTask.CompletedTask;
            }

            _playerManager.Player.Inventory.AddItems(itemDefinition, command.Quantity);

            return UniTask.CompletedTask;
        }
    }
}
