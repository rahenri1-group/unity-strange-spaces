﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Player;
using System;
using System.Linq;

namespace Game.Command
{
    [Serializable]
    public class PlayerInventoryShowCommand : ICommand { }

    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class PlayerInventoryShowCommandProcessor : BaseConsoleCommandProcessor<PlayerInventoryShowCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "inventory-show";

        /// <inheritdoc/>
        public override string CommandDescription => "Displays the player's inventory";

        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PlayerInventoryShowCommandProcessor(
            ILogRouter logger,
            IPlayerManager playerManager)
        {
            _logger = logger;
            _playerManager = playerManager;
        }

        /// <inheritdoc/>
        public override UniTask Execute(PlayerInventoryShowCommand command)
        {
            var playerInventory = _playerManager.Player?.Inventory;
            
            var itemDefinitions = playerInventory
                .AllDefinitions()
                .OrderBy(i => i.Name)
                .ToArray();

            string inventoryString = string.Empty;
            foreach (var itemDefinition in itemDefinitions)
            {
                inventoryString += $"{itemDefinition.Name} x {playerInventory.DefinitionQuantity(itemDefinition)}\n"; 
            }

            _logger.LogInfo(inventoryString);

            return UniTask.CompletedTask;
        }
    }
}
