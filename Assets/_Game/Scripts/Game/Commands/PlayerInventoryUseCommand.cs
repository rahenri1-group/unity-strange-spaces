﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Item;
using Game.Player;
using System;

namespace Game.Command
{
    [Serializable]
    public class PlayerInventoryUseCommand : ICommand 
    {
        /// <summary>
        /// The name of the item to add
        /// </summary>
        public string ItemName;
    }

    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class PlayerInventoryUseCommandProcessor : BaseConsoleCommandProcessor<PlayerInventoryUseCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "inventory-use";

        /// <inheritdoc/>
        public override string CommandDescription => "Uses an item in the player's inventory";

        private readonly IItemDefinitionManager _itemDefinitionManager;
        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public PlayerInventoryUseCommandProcessor(
            IItemDefinitionManager itemDefinitionManager,
            ILogRouter logger,
            IPlayerManager playerManager)
        {
            _itemDefinitionManager = itemDefinitionManager;
            _logger = logger;
            _playerManager = playerManager;
        }

        /// <inheritdoc/>
        public override PlayerInventoryUseCommand ParseCommand(string[] args)
        {
            if (args.Length > 0)
            {
                return new PlayerInventoryUseCommand
                {
                    ItemName = args[0]
                };
            }

            return null;
        }

        /// <inheritdoc/>
        public override UniTask Execute(PlayerInventoryUseCommand command)
        {
            var itemDefinition = _itemDefinitionManager.GetItemDefinition(command.ItemName) as IPlayerConsumableItemDefinition;
            if (itemDefinition == null)
            {
                _logger.LogWarning($"Invalid item name {command.ItemName}");
                return UniTask.CompletedTask;
            }

            var playerInventory = _playerManager.Player?.Inventory;
            if (!playerInventory.ContainsItem(itemDefinition))
            {
                _logger.LogWarning($"Player inventory does not contain {itemDefinition.Name}");
                return UniTask.CompletedTask;
            }

            itemDefinition.Consume(_playerManager.Player);

            return UniTask.CompletedTask;
        }
    }
}
