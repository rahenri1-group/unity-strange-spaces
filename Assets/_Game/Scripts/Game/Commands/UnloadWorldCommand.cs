﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Player;
using Game.World;
using System;

namespace Game.Command
{
    [Serializable]
    public class UnloadWorldCommand : ICommand { }

    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class UnloadWorldCommandProcessor : BaseConsoleCommandProcessor<UnloadWorldCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "unloadworld";

        /// <inheritdoc/>
        public override string CommandDescription => "Unloads the current world and kicks the player to the home scene";

        private readonly ILogRouter _logger;
        private readonly IPlayerManager _playerManager;
        private readonly IWorldManager _worldManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public UnloadWorldCommandProcessor(
            ILogRouter logger,
            IPlayerManager playerManager,
            IWorldManager worldManager)
        {
            _logger = logger;
            _playerManager = playerManager;
            _worldManager = worldManager;
        }

        /// <inheritdoc/>
        public override async UniTask Execute(UnloadWorldCommand command)
        {
            if (!_worldManager.IsWorldLoaded)
            {
                _logger.LogWarning("No world currently loaded");
                return;
            }

            await _playerManager.MovePlayerToHome(
                async () =>
                {
                    // unload the world
                    await _worldManager.DestroyCurrentWorld();
                });
        }
    }
}
