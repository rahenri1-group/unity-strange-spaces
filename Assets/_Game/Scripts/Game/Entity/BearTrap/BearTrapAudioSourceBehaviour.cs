﻿using Game.Core;
using Game.Core.Audio;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public class BearTrapAudioSourceBehaviour : BaseModifiableAudioSource
    {
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private BearTrapEntityBehaviour _bearTrap = null;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _trapAudioClipsObj;
        private IAudioClipCollection _trapAudioClips;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_bearTrap);

            _trapAudioClips = _trapAudioClipsObj != null ? (IAudioClipCollection)_trapAudioClipsObj : null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _bearTrap.TrapSprung += OnTrapSprung;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _bearTrap.TrapSprung -= OnTrapSprung;
        }

        private void OnTrapSprung()
        {
            if (_trapAudioClips != null)
            {
                PlayClip(_trapAudioClips.GetNextAudioClip());
            }
        }
    }
}
