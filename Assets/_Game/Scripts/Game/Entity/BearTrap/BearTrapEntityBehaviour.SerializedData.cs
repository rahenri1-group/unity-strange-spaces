﻿using Game.Core.Entity;
using System;

namespace Game.Entity
{
    public partial class BearTrapEntityBehaviour : BaseDynamicEntity<BearTrapEntityBehaviour.BearTrapEntityData>
    {
        [Serializable]
        public class BearTrapEntityData : EntitySerializedData
        {
            public bool HasTrapSprung = false;
        }

        protected override void InitFromSerializedObject(BearTrapEntityData data)
        {
            base.InitFromSerializedObject(data);

            HasTrapSprung = data.HasTrapSprung;

            if (HasTrapSprung)
            {
                UpdateTrapJawAngle(_trapJawAngle);
            }
        }

        protected override void FillSerializedObject(BearTrapEntityData data)
        {
            base.FillSerializedObject(data);

            data.HasTrapSprung = HasTrapSprung;
        }
    }
}
