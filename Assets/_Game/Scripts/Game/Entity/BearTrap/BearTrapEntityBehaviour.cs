﻿using Game.Core;
using Game.Core.Entity;
using Game.Core.Physics;
using Game.Physics;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public partial class BearTrapEntityBehaviour : BaseDynamicEntity<BearTrapEntityBehaviour.BearTrapEntityData>, IImpactable
    {
        public event Action TrapSprung;

        public bool HasTrapSprung { get; private set; }

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _trapTriggerObj = null;
        private ITriggerable _trapTrigger;

        [SerializeField] private IntReadonlyReference _damageAmount = null;
        [SerializeField] private FloatReadonlyReference _impactAmount = null;

        [SerializeField] private Transform[] _trapJaws = new Transform[0];
        [SerializeField] private FloatReadonlyReference _trapJawAngle = null;
        [SerializeField] private FloatReadonlyReference _trapJawDuration = null;

        private IEnumerator _jawAnimationCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_trapTriggerObj);
            Assert.IsTrue(_damageAmount > 0);

            _trapTrigger = _trapTriggerObj.GetComponentAsserted<ITriggerable>();

            UpdateTrapJawAngle(0f);

            _jawAnimationCoroutine = null;
        }

        private void OnEnable()
        {
            _trapTrigger.TriggerEnter += OnTrapTrigger;
        }

        private void OnDisable()
        {
            _trapTrigger.TriggerEnter -= OnTrapTrigger;

            if (_jawAnimationCoroutine != null)
            {
                StopCoroutine(_jawAnimationCoroutine);
                _jawAnimationCoroutine = null;
            }
        }

        private void OnTrapTrigger(ITriggerable sender, TriggerableEventArgs args)
        {
            if (HasTrapSprung) return;

            var entity = args.Collider.GetComponentInParent<IEntity>();
            if (entity != null)
            {
                var damageable = args.Collider.GetComponentInParent<IDamageable>(); ;
                if (damageable != null)
                {
                    var hitLocation = args.Collider.ClosestPoint(args.Trigger.bounds.center);
                    damageable.ApplyDamage(_damageAmount, hitLocation, DamageType.Generic);
                }

                var impactable = args.Collider.GetComponentInParent<IImpactable>();
                if (impactable != null && _impactAmount > 0f)
                {
                    impactable.OnImpact(_impactAmount, transform.up);
                }

                TriggerTrap();
            }
        }

        public void OnImpact(float impactForce, Vector3 forceDirection)
        {
            TriggerTrap();
        }

        private void TriggerTrap()
        {
            if (HasTrapSprung) return;

            HasTrapSprung = true;

            TrapSprung?.Invoke();

            _jawAnimationCoroutine = JawAnimation();
            StartCoroutine(_jawAnimationCoroutine);
        }

        private void UpdateTrapJawAngle(float angle)
        {
            foreach (var jaw in _trapJaws)
            {
                var euler = jaw.localEulerAngles;
                euler.z = angle;
                jaw.localEulerAngles = euler;
            }
        }

        private IEnumerator JawAnimation()
        {
            YieldInstruction wait = null;

            float startTime = Time.time;
            while (Time.time - startTime <= _trapJawDuration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / _trapJawDuration);
                float angle = Mathf.LerpAngle(0f, _trapJawAngle, lerp);
                UpdateTrapJawAngle(angle);
            }

            UpdateTrapJawAngle(_trapJawAngle);

            _jawAnimationCoroutine = null;
        }
    }
}
