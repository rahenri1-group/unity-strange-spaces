﻿using Game.Core;
using Game.Core.Entity;
using Game.Core.Physics;
using UnityEngine;

namespace Game.Entity
{
    [RequireComponent(typeof(IRigidBodyMovement))]
    public class BottleChunkEntityBehaviour : BaseDynamicEntity<EntitySerializedData>
    {
        [SerializeField] private Transform _breakDirection = null;

        private IRigidBodyMovement _movement;

        protected override void Awake()
        {
            base.Awake();

            _movement = this.GetComponentAsserted<IRigidBodyMovement>();
        }

        public void OnBreak(float impactForce, Vector3 forceDirection)
        {
            var force = forceDirection * impactForce;

            if (_breakDirection != null)
            {
                float breakDirectionRatio = Core.Random.Default.Range(0.1f, 0.3f);
                force = (_breakDirection.forward * breakDirectionRatio + forceDirection * (1f - breakDirectionRatio)) * impactForce;
            }

            _movement.Rigidbody.AddForce(force, ForceMode.Impulse);

            if (_breakDirection != null)
            {
                _movement.Rigidbody.AddTorque(_breakDirection.forward * impactForce * Core.Random.Default.Range(-5f, 5f), ForceMode.Impulse);
                _movement.Rigidbody.AddTorque(_breakDirection.right * impactForce * Core.Random.Default.Range(-5f, 5f), ForceMode.Impulse);
            }
            else
            {
                _movement.Rigidbody.AddTorque(transform.forward * impactForce * Core.Random.Default.Range(-5f, 5f), ForceMode.Impulse);
                _movement.Rigidbody.AddTorque(transform.right * impactForce * Core.Random.Default.Range(-5f, 5f), ForceMode.Impulse);
            }
        }
    }
}
