﻿using System;
using UnityEngine;

namespace Game.Entity
{
    public partial class BottleEntityBehaviour
    {
        [Serializable]
        public class BottleChunkSpawnData 
        {
            public string BottleChunkAddress;
            public Vector3 BottleChunkSpawnPosition;
        }
    }
}