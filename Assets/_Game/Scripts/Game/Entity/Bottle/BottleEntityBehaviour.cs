﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Entity;
using Game.Core.Interaction;
using Game.Core.Physics;
using Game.Core.Resource;
using Game.Core.Space;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    [RequireComponent(typeof(IRigidBodyMovement))]
    public partial class BottleEntityBehaviour : BaseDynamicEntity<EntitySerializedData>, IImpactable, IInteractableDecider
    {
        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] private IDynamicEntityManager _dynamicEntityManager = null;
        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        [SerializeField] private FloatReference _breakForceRequired = null;
        [SerializeField] private FloatReference _maxForce = null;

        [SerializeField] private StringReadonlyReference _bottleBreakEffectAddress = null;
        [SerializeField] private BottleChunkSpawnData[] _bottleChunkDatas = new BottleChunkSpawnData[0];

        private bool _isBroken = false;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_breakForceRequired >= 0f);
            Assert.IsTrue(_maxForce >= _breakForceRequired);

            Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(_bottleBreakEffectAddress));

            foreach (var data in _bottleChunkDatas)
            {
                Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(data.BottleChunkAddress));
            }
        }

        public Interactions AllowedInteractions(IInteractable interactable, IInteractor interactor)
        {
            if (_isBroken)
            {
                return Interactions.None;
            }

            return Interactions.All;
        }

        public void OnImpact(float impactForce, Vector3 forceDirection)
        {
            if (_isBroken || impactForce < _breakForceRequired)
            {
                return;
            }

            _isBroken = true;

            if (impactForce > _maxForce)
            {
                impactForce = _maxForce;
            }

            Break(impactForce, forceDirection).Forget();
        }

        private async UniTask Break(float impactForce, Vector3 forceDirection)
        {
            var space = _spaceManager.GetEntitySpace(this);

            await _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(_bottleBreakEffectAddress, space, EntityMovement.Position);
            

            var chunkSpawnTasks = new List<UniTask<IDynamicEntity>>();
            foreach (var data in _bottleChunkDatas)
            {
                chunkSpawnTasks.Add(_dynamicEntityManager.SpawnEntity(
                    data.BottleChunkAddress,
                    space,
                    transform.TransformPoint(data.BottleChunkSpawnPosition),
                    EntityMovement.Rotation));
            }

            var spawnedChunks = await UniTask.WhenAll(chunkSpawnTasks);

            var chunkForce = (impactForce - _breakForceRequired) / spawnedChunks.Length;
            foreach (var spawnedEntity in spawnedChunks)
            {
                var bottleChunk = spawnedEntity as BottleChunkEntityBehaviour;
                if (bottleChunk != null)
                {
                    bottleChunk.OnBreak(chunkForce, forceDirection);
                }
            }

            _dynamicEntityManager.DestroyEntity(this);
        }
    }
}
