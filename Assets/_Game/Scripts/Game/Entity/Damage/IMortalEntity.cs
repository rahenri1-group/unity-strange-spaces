﻿using Game.Core.Entity;
using Game.Physics;
using UnityEngine;

namespace Game.Entity
{
    public delegate void EntityDamagedEvent(IMortalEntity sender, EntityDamagedEventArgs args);

    public struct EntityDamagedEventArgs
    {
        public int DamageAmount;
        public Vector3 DamagePosition;
        public DamageType DamageType;
    }

    /// <summary>
    /// An <see cref="IDynamicEntity"/> that has health and can be damaged
    /// </summary>
    public interface IMortalEntity : IDynamicEntity, IDamageable
    {
        /// <summary>
        /// The maximum health of the entity
        /// </summary>
        int HealthMax { get; }
        
        /// <summary>
        /// The current health of the entity
        /// </summary>
        int HealthCurrent { get; }

        /// <summary>
        /// Event raised when the entity is damaged
        /// </summary>
        event EntityDamagedEvent EntityDamaged;
    }
}