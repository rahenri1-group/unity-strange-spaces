﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    /// <summary>
    /// Component that controls the damage animations for a <see cref="IMortalEntity"/>
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class MortalEntityAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(IMortalEntity))] private Component _entityObj = null;
        private IMortalEntity _entity;

        [SerializeField] private StringReadonlyReference _isAliveAnimatorParameter = null;

        private Animator _entityAnimator;

        private int _isAliveId;

        /// <inheritdoc/>
        private void Awake()
        {
            _entityAnimator = GetComponent<Animator>();

            Assert.IsNotNull(_entityObj);
            _entity = _entityObj.GetComponentAsserted<IMortalEntity>();

            _isAliveId = Animator.StringToHash(_isAliveAnimatorParameter);
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (_entity.IsInitializationComplete)
            {
                UpdateAnimator();
            }

            _entity.InitializationComplete += OnEntityInitialized;
            _entity.EntityDamaged += OnEntityDamaged;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _entity.InitializationComplete -= OnEntityInitialized;
            _entity.EntityDamaged -= OnEntityDamaged;
        }

        private void OnEntityInitialized()
        {
            UpdateAnimator();
        }

        private void OnEntityDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            UpdateAnimator();
        }

        private void UpdateAnimator()
        {
            _entityAnimator.SetBool(_isAliveId, _entity.IsAlive());
        }
    }
}