﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    /// <summary>
    /// Toggles components on a <see cref="IMortalEntity"/> depending on if it is alive or dead
    /// </summary>
    public class MortalEntityTogglerBehaviour : MonoBehaviour
    {
        [SerializeField][TypeRestriction(typeof(IMortalEntity))] private Component _entityObj = null;
        private IMortalEntity _entity;

        /// <summary>
        /// Objects that should only be active when entity is alive
        /// </summary>
        [SerializeField] private GameObject[] _aliveObjects = new GameObject[0];
        /// <summary>
        /// Objects that should only be active when entity is dead
        /// </summary>
        [SerializeField] private GameObject[] _deadObjects = new GameObject[0];

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_entityObj);
            _entity = _entityObj.GetComponentAsserted<IMortalEntity>();
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (_entity.IsInitializationComplete)
            {
                UpdateGameObjects();
            }

            _entity.InitializationComplete += OnEntityInitialized;
            _entity.EntityDamaged += OnEntityDamaged;
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _entity.InitializationComplete -= OnEntityInitialized;
            _entity.EntityDamaged -= OnEntityDamaged;
        }

        private void OnEntityInitialized()
        {
            UpdateGameObjects();
        }

        private void OnEntityDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            UpdateGameObjects();
        }

        private void UpdateGameObjects()
        {
            bool isAlive = _entity.IsAlive();

            foreach (var entity in _aliveObjects)
            {
                entity.SetActive(isAlive);
            }

            foreach (var entity in _deadObjects)
            {
                entity.SetActive(!isAlive);
            }
        }
    }
}