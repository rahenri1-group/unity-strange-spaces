﻿using Game.Core.Entity;
using Game.Item;
using System;

namespace Game.Entity
{
    public partial class DoorEntityBehaviour : BaseDynamicEntity<DoorEntityBehaviour.DoorEntityData>
    {
        [Serializable]
        public class DoorEntityData : EntitySerializedData
        {
            public string DoorKeyId = string.Empty;
            public bool IsLocked = false;
        }

        protected override void InitFromSerializedObject(DoorEntityData data)
        {
            _doorKeyItemId = data.DoorKeyId;

            if (!string.IsNullOrEmpty(_doorKeyItemId))
            {
                DoorKeyItemDefinition = _itemDefinitionManager.GetItemDefinition<IKeyItemDefinition>(Guid.Parse(_doorKeyItemId));
            }
            else
            {
                DoorKeyItemDefinition = null;
            }

            IsLocked = data.IsLocked;

            base.InitFromSerializedObject(data);
        }

        protected override void FillSerializedObject(DoorEntityData data)
        {
            base.FillSerializedObject(data);

            data.DoorKeyId = _doorKeyItemId;
            data.IsLocked = IsLocked;
        }
    }
}
