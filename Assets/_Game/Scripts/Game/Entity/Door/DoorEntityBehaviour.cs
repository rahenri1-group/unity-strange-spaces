﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Interaction;
using Game.Core.Item;
using Game.Core.Physics;
using Game.Item;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public partial class DoorEntityBehaviour : BaseDynamicEntity<DoorEntityBehaviour.DoorEntityData>, IInteractableDecider
    {
        public IKeyItemDefinition DoorKeyItemDefinition { get; private set; }
        [SerializeField] [ReadOnly] private string _doorKeyItemId = string.Empty;

        public bool IsLocked { get; set; } = false;

        [Inject] private IItemDefinitionManager _itemDefinitionManager = null;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _characterTriggerableObj = null;
        private ITriggerable _characterTriggerable;

        [SerializeField] [TypeRestriction(typeof(IHinge))] private Component[] _hingeObjs = null;
        private IHinge[] _hinges;

        [SerializeField] private FloatReadonlyReference _doorOpenSpeed = null;
        [SerializeField] private FloatReadonlyReference _doorCloseSpeed = null;
        [SerializeField] private FloatReadonlyReference _doorCloseDelay = null;

        private HashSet<Collider> _characterCollidersInTrigger;

        private IEnumerator _delayedCloseCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_characterTriggerableObj);
            Assert.IsNotNull(_hingeObjs);
            Assert.IsTrue(_hingeObjs.Length > 0);
            
            _characterTriggerable = _characterTriggerableObj.GetComponentAsserted<ITriggerable>();

            _hinges = new IHinge[_hingeObjs.Length];
            for (int i = 0; i < _hingeObjs.Length; i++)
            {
                _hinges[i] = _hingeObjs[i].GetComponentAsserted<IHinge>();
            }

            _characterCollidersInTrigger = new HashSet<Collider>();

            _delayedCloseCoroutine = null;
        }

        private void OnEnable()
        {
            _characterTriggerable.TriggerEnter += OnCharacterEnter;
            _characterTriggerable.TriggerExit += OnCharacterExit;
        }

        private void OnDisable()
        {
            _characterTriggerable.TriggerEnter -= OnCharacterEnter;
            _characterTriggerable.TriggerExit -= OnCharacterExit;

            if (_delayedCloseCoroutine != null)
            {
                StopCoroutine(_delayedCloseCoroutine);
                _delayedCloseCoroutine = null;
            }
        }

        private void OnCharacterEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            if (args.Collider.GetComponentInParent<ICharacterEntity>() != null)
            {
                if (!_characterCollidersInTrigger.Contains(args.Collider))
                {
                    _characterCollidersInTrigger.Add(args.Collider);
                }

                if (_delayedCloseCoroutine != null)
                {
                    StopCoroutine(_delayedCloseCoroutine);
                    _delayedCloseCoroutine = null;
                }
            }
        }

        private void OnCharacterExit(ITriggerable sender, TriggerableEventArgs args)
        {
            if (_characterCollidersInTrigger.Contains(args.Collider))
            {
                _characterCollidersInTrigger.Remove(args.Collider);

                if (_characterCollidersInTrigger.Count == 0 && _delayedCloseCoroutine == null)
                {
                    _delayedCloseCoroutine = DelayedClose();
                    StartCoroutine(_delayedCloseCoroutine);
                }
            }
        }

        public void Open()
        {
            if (_delayedCloseCoroutine != null)
            {
                StopCoroutine(_delayedCloseCoroutine);
                _delayedCloseCoroutine = null;
            }

            foreach (var hinge in _hinges)
                hinge.AttemptOpen(_doorOpenSpeed);
        }

        public Interactions AllowedInteractions(IInteractable interactable, IInteractor interactor)
        {
            if (DoorKeyItemDefinition == null || !IsLocked)
            {
                return Interactions.All;
            }
            else
            {
                return Interactions.Hover;
            }
        }

        private IEnumerator DelayedClose()
        {
            if (_doorCloseDelay > 0)
                yield return new WaitForSeconds(_doorCloseDelay);

            foreach (var hinge in _hinges)
                hinge.AttemptClose(_doorCloseSpeed);

            _delayedCloseCoroutine = null;
        }
    }
}
