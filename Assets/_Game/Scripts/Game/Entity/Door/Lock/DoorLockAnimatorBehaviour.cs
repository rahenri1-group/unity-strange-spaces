﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Resource;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public class DoorLockAnimatorBehaviour : InjectedBehaviour
    {
        [Inject] IEventBus _eventBus = null;
        [Inject] IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] ISpaceManager _spaceManager = null;

        [SerializeField] private DoorLockBehaviour _doorLock = null;
        [SerializeField] private FloatReadonlyReference _keyHoldDuration = null;
        [SerializeField] private FloatReadonlyReference _keyAnimateDuration = null;
        [SerializeField] private Transform _keyStartPosition = null;
        [SerializeField] private Transform _keyEndPosition = null;
        [SerializeField] private Renderer[] _unlockDisableRenderers = new Renderer[0];

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_doorLock);
            Assert.IsTrue(_keyAnimateDuration > 0f);
            Assert.IsNotNull(_keyStartPosition);
            Assert.IsNotNull(_keyEndPosition);
        }

        public async UniTask AnimateUnlock()
        {
            var parentEntity = GetComponentInParent<IEntity>();

            var keyModel = await _gameObjectResourceManager.InstantiateAsync(
                _doorLock.KeyItemDefinition.UnlockModelAddress,
                _spaceManager.GetObjectSpace(gameObject),
                _keyStartPosition.position,
                _keyStartPosition.rotation);

            keyModel.transform.SetParent(transform);

            if (parentEntity != null)
            {
                _eventBus.InvokeEvent(new EntityModifiedEvent 
                { 
                    Entity = parentEntity 
                });
            }

            foreach (var renderer in _unlockDisableRenderers)
            {
                renderer.enabled = false;
            }

            if (_keyHoldDuration > 0f)
            {
                await UniTask.Delay((int)(1000 * _keyHoldDuration));
            }

            float startTime = Time.time;
            while (Time.time - startTime <= _keyAnimateDuration)
            {
                await UniTask.DelayFrame(1);

                float lerp = Mathf.Clamp01((Time.time - startTime) / _keyAnimateDuration);

                keyModel.transform.SetPositionAndRotation(
                    Vector3.Lerp(_keyStartPosition.position, _keyEndPosition.position, lerp),
                    Quaternion.Lerp(_keyStartPosition.rotation, _keyEndPosition.rotation, lerp));

                await UniTask.DelayFrame(1);
            }
            keyModel.transform.SetPositionAndRotation(_keyEndPosition.position, _keyEndPosition.rotation);

            _gameObjectResourceManager.DestroyObject(keyModel);

            if (parentEntity != null)
            {
                _eventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = parentEntity
                });
            }

            return;
        }
    }
}
