﻿using Game.Core;
using Game.Core.Audio;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public class DoorLockAudioBehaviour : BaseModifiableAudioSource
    {
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private DoorLockBehaviour _doorLock = null;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _interactAudioClipsObj;
        private IAudioClipCollection _interactAudioClips;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _unlockAudioClipsObj;
        private IAudioClipCollection _unlockAudioClips;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_doorLock);

            _interactAudioClips = _interactAudioClipsObj != null ? (IAudioClipCollection)_interactAudioClipsObj : null;
            _unlockAudioClips = _unlockAudioClipsObj != null ? (IAudioClipCollection)_unlockAudioClipsObj : null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _doorLock.AttemptUnlock += OnAttemptUnlock;
            _doorLock.Unlocked += OnUnlocked;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _doorLock.AttemptUnlock -= OnAttemptUnlock;
            _doorLock.Unlocked -= OnUnlocked;
        }

        private void OnAttemptUnlock()
        {
            if (_interactAudioClips != null)
            {
                PlayClip(_interactAudioClips.GetNextAudioClip());
            }
        }

        private void OnUnlocked()
        {
            if (_unlockAudioClips != null)
            {
                PlayClip(_unlockAudioClips.GetNextAudioClip());
            }
        }
    }
}
