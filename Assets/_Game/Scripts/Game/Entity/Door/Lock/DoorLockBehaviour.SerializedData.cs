﻿using Game.Core.Serialization;
using System;

namespace Game.Entity
{
    public partial class DoorLockBehaviour : ISerializableComponent
    {
        [Serializable]
        private class SerializedData { }

        public bool SerializeComponent => true;

        public string SerializeToJson()
        {
            var data = new SerializedData { };

            return _jsonSerializer.Serialize(data);
        }

        public void DeserializeFromJson(string json)
        {
            // Deserialize is called after parent object has deserialized. At this point, we can check if the door is locked.

            UpdateLockState();
        }
    }
}
