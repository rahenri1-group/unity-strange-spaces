﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Render;
using Game.Core.Serialization;
using Game.Interaction;
using Game.Item;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Entity
{
    /// <summary>
    /// An <see cref="IInteractableLock"/> for <see cref="DoorEntityBehaviour"/>
    /// </summary>
    public partial class DoorLockBehaviour : BaseInteractable, IInteractableLock, IWorldInteractable
    {
        /// <summary>
        /// Event raised when an <see cref="IInteractor"/> interacts with the lock but does not have the required key
        /// </summary>
        public event Action AttemptUnlock;
        /// <summary>
        /// Event raised when the lock has been unlocked
        /// </summary>
        public event Action Unlocked;

        /// <inheritdoc/>
        public IKeyItemDefinition KeyItemDefinition => _door.DoorKeyItemDefinition;

        [Inject] private IJsonSerializer _jsonSerializer = null;

        [SerializeField] private FloatReadonlyReference _unlockFadeDuration = null;

        private DoorEntityBehaviour _door;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            _door = GetComponentInParent<DoorEntityBehaviour>();

            UpdateLockState();
        }

        /// <inheritdoc />
        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            if (KeyItemDefinition == null || !_door.IsLocked) return;

            var keyProvider = interactor.InteractorOwner.GetComponent<IKeyItemDefinitionProvider>();
            if (keyProvider != null && keyProvider.ContainsKeyItem(KeyItemDefinition))
            {
                _door.IsLocked = false;

                UniTask.Create(async () =>
                {
                    var unlockAnimationTasks = new List<UniTask>();
                    foreach (var unlockAnimator in GetComponentsInChildren<DoorLockAnimatorBehaviour>())
                    {
                        unlockAnimationTasks.Add(unlockAnimator.AnimateUnlock());
                    }
                    await UniTask.WhenAll(unlockAnimationTasks);

                    Unlocked?.Invoke();

                    if (_unlockFadeDuration > 0f)
                    {
                        var fade = gameObject.AddComponent<FadeOutBehaviour>();
                        fade.FadeDuration = _unlockFadeDuration;
                        await fade.FadeCompleteTask;
                    }

                    UpdateLockState();
                }).Forget();
            }
            else
            {
                AttemptUnlock?.Invoke();
            }
        }

        private void UpdateLockState()
        {
            GameObject.SetActive(_door.IsLocked);
        }
    }
}
