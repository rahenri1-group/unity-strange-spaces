﻿using Game.Core.Entity;
using System;

namespace Game.Entity

{
    public partial class DoorShadowEntityBehaviour : BaseDynamicEntity<DoorShadowEntityBehaviour.ShadowEntityData>
    {
        [Serializable]
        public class ShadowEntityData : EntitySerializedData
        {
            public string DoorToShadowId = string.Empty;
        }

        protected override void InitFromSerializedObject(ShadowEntityData data)
        {
            base.InitFromSerializedObject(data);

            _entityToShadowId = data.DoorToShadowId;

            if (!_entityManager.HasRecordForDynamicEntity(EntityToShadowId))
            {
                _logger.LogError($"Unable to shadow entity {EntityToShadowId} as it does not exist");

                Destroy(gameObject);
                return;
            }

            if (_entityManager.IsDynamicEntityLoaded(EntityToShadowId))
            {
                gameObject.SetActive(false);
            }
            else
            {
                var doorEntityRecord = _entityManager.GetRecordForDynamicEntity(EntityToShadowId);
                var doorData = JsonDeserializer.Deserialize<DoorEntityBehaviour.DoorEntityData>(doorEntityRecord.SerializedJson);

                _lockObject.SetActive(doorData.IsLocked);
            }
        }

        protected override void FillSerializedObject(ShadowEntityData data)
        {
            base.FillSerializedObject(data);

            data.DoorToShadowId = _entityToShadowId;
        }
    }
}
