﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Entity
{
    public partial class DoorShadowEntityBehaviour : BaseDynamicEntity<DoorShadowEntityBehaviour.ShadowEntityData>
    {
        public Guid EntityToShadowId
        {
            get
            {
                if (_parsedEntityToShadowId == null) _parsedEntityToShadowId = !string.IsNullOrEmpty(_entityToShadowId) ? Guid.Parse(_entityToShadowId) : Guid.Empty;

                return _parsedEntityToShadowId.Value;
            }
        }
        private Guid? _parsedEntityToShadowId = null;
        [SerializeField] [ReadOnly] private string _entityToShadowId = string.Empty;

        [SerializeField] private GameObject _lockObject = null;
        

        [Inject] private IDynamicEntityManager _entityManager = null;
        [Inject] private ILogRouter _logger = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_lockObject);

            EventBus.Subscribe<SpaceLoadedEvent>(OnSpaceLoaded);
            EventBus.Subscribe<SpaceUnloadingEvent>(OnSpaceUnloading);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoaded);
            EventBus.Unsubscribe<SpaceUnloadingEvent>(OnSpaceUnloading);
        }

        private void OnSpaceLoaded(SpaceLoadedEvent eventObj)
        {
            if (_entityManager.IsDynamicEntityLoaded(EntityToShadowId))
            {
                gameObject.SetActive(false);
            }
        }

        private void OnSpaceUnloading(SpaceUnloadingEvent eventObj)
        {
            var doorEntityRecord = _entityManager.GetRecordForDynamicEntity(EntityToShadowId);

            if (doorEntityRecord != null && SpaceUtil.SpaceEquals(doorEntityRecord.EntitySpace, eventObj.SpaceData))
            {
                gameObject.SetActive(true);
                
                var doorData = JsonDeserializer.Deserialize<DoorEntityBehaviour.DoorEntityData>(doorEntityRecord.SerializedJson);
                _lockObject.SetActive(doorData.IsLocked);
            }
        }
    }
}
