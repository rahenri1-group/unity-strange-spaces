﻿using Game.Core;
using Game.Core.Physics;
using Game.Player;
using System;
using UnityEngine;

namespace Game.Entity.Enemy
{
    public class EnemyAttackTriggerBehaviour : MonoBehaviour, IEnemyAttackTrigger
    {
        public event EnemyAttackHitEvent PlayerHit;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component[] _triggerObjs = null;
        private ITriggerable[] _triggerables;

        private bool _attackEnabled;

        private void Awake()
        {
            _triggerables = _triggerObjs.GetComponentArrayAsserted<ITriggerable>();

            _attackEnabled = false;
        }

        private void OnEnable()
        {
            _attackEnabled = false;

            foreach (var trigger in _triggerables)
            {
                trigger.TriggerEnter += OnPlayerTriggerEnter;
            }
        }

        private void OnDisable()
        {
            foreach (var trigger in _triggerables)
            {
                trigger.TriggerEnter -= OnPlayerTriggerEnter;
            }
        }

        private void OnPlayerTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            if (!_attackEnabled) return;

            var player = args.Collider.GetComponentInParent<IPlayer>();
            if (player != null)
            {
                _attackEnabled = false;

                var hitLocation = args.Collider.ClosestPoint(args.Trigger.bounds.center);

                PlayerHit?.Invoke(this, new PlayerHitArgs
                {
                    Player = player,
                    HitLocation = hitLocation
                });
            }
        }

        /// <summary>
        /// Called by animation event to enable attack triggers
        /// </summary>
        public void EnableAttackTriggers()
        {
            _attackEnabled = true;
        }

        /// <summary>
        /// Called by animation event to disable attack triggers
        /// </summary>
        public void DisableAttackTriggers()
        {
            _attackEnabled = false;
        }
    }
}
