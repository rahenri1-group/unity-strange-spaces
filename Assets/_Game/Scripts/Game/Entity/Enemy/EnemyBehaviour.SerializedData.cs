﻿using Game.Core.Entity;
using System;
using UnityEngine;

namespace Game.Entity.Enemy
{
    public partial class EnemyBehaviour
    {
        [Serializable]
        public class EnemyEntityData : EntitySerializedData
        {
            public int Health = 999;
        }

        /// <inheritdoc/>
        public override void InitializeNew(Guid id)
        {
            HealthCurrent = HealthMax;

            base.InitializeNew(id);
        }

        protected override void InitFromSerializedObject(EnemyEntityData data)
        {
            base.InitFromSerializedObject(data);

            HealthCurrent = Mathf.Clamp(data.Health, 0, HealthMax);
        }

        protected override void FillSerializedObject(EnemyEntityData data)
        {
            base.FillSerializedObject(data);

            data.Health = HealthCurrent;
        }
    }
}