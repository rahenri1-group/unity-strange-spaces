﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Events;
using Game.Physics;
using UnityEngine;

namespace Game.Entity.Enemy
{
    public partial class EnemyBehaviour : BaseDynamicEntity<EnemyBehaviour.EnemyEntityData>, IMortalEntity, ICharacterEntity
    {
        /// <inheritdoc/>
        public int HealthMax => _enemyHealthMax;
        /// <inheritdoc/>
        public int HealthCurrent 
        { 
            get => _enemyHealthCurrent; 
            private set => _enemyHealthCurrent = value;
        }

        /// <inheritdoc/>
        public event EntityDamagedEvent EntityDamaged;

        [Inject] protected ILogRouter Logger = null;

        [SerializeField] private IntReadonlyReference _enemyHealthMax = null;
        [SerializeField] [ReadOnly] private int _enemyHealthCurrent = 0;
        

        /// <inheritdoc/>
        public void ApplyDamage(int damage, Vector3 damagePosition, DamageType damageType)
        {
            if (damage <= 0)
            {
                Logger.LogWarning($"Invalid damage amount '{damage}'");
                return;
            }

            var oldHealth = HealthCurrent;
            HealthCurrent = Mathf.Clamp(HealthCurrent - damage, 0, HealthMax);
            if (HealthCurrent != oldHealth)
            {
                EntityDamaged?.Invoke(this, new EntityDamagedEventArgs
                {
                    DamageAmount = HealthCurrent - oldHealth,
                    DamagePosition = damagePosition,
                    DamageType = damageType
                });

                if (!this.IsAlive())
                {
                    EventBus.InvokeEvent(new EntityKilledEvent
                    {
                        Entity = this
                    });
                }
            }
        }
    }
}
