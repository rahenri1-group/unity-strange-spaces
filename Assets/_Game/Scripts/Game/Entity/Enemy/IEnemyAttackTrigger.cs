﻿using Game.Player;
using System;
using UnityEngine;

namespace Game.Entity.Enemy
{
    public delegate void EnemyAttackHitEvent(IEnemyAttackTrigger sender, PlayerHitArgs args);

    public struct PlayerHitArgs
    {
        public IPlayer Player;
        public Vector3 HitLocation;
    }

    public interface IEnemyAttackTrigger
    {
        event EnemyAttackHitEvent PlayerHit;
    }
}
