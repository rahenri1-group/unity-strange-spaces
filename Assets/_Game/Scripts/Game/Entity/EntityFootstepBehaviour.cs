﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Physics;
using Game.Core.Space;
using Game.Physics.PhysicsMaterial.Event;
using UnityEngine;

namespace Game.Entity
{
    public class EntityFootstepBehaviour : InjectedBehaviour
    {
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] private ISpacePhysics _spacePhysics = null;

        [SerializeField] private FloatReadonlyReference _stepTriggerHeight = null;
        [SerializeField] private FloatReadonlyReference _stepResetHeight = null;
        [SerializeField] [Layer] private int[] _queryLayers = new int[0];

        private IDynamicEntity _entity;
        private ISpaceData _entitySpace;
        private bool _stepTriggered;
        private int _queryMask;

        protected override void Awake()
        {
            base.Awake();

            _entity = this.GetComponentInParentAsserted<IDynamicEntity>();

            _queryMask = LayerUtil.ConstructMaskForLayers(_queryLayers);

            _stepTriggered = true;
        }

        private void OnEnable()
        {
            _entitySpace = _spaceManager.GetEntitySpace(_entity);

            _entity.EntityMovement.EntityTeleported += OnEntityTeleported;
        }

        private void OnDisable()
        {
            _entity.EntityMovement.EntityTeleported -= OnEntityTeleported;
        }

        private void OnEntityTeleported()
        {
            _entitySpace = _spaceManager.GetEntitySpace(_entity);
        }

        private void LateUpdate()
        {
            float stepHeight = CalculateStepHeight();
            Vector3 footPosition = transform.position - stepHeight * Vector3.up;

            if (_stepTriggered)
            {
                if (stepHeight >= _stepResetHeight)
                {
                    _stepTriggered = false;
                }
            }
            else
            {
                if (stepHeight <= _stepTriggerHeight)
                {
                    if (_spacePhysics.Raycast(_entitySpace, footPosition + 0.1f * Vector3.up, -Vector3.up, out var stepRaycastHit, 0.2f, QueryPortalInteraction.CastThroughTransporter, _queryMask))
                    {
                        var physicsMaterial = stepRaycastHit.Collider.gameObject.GetComponentInParent<IPhysicsMaterial>();
                        if (physicsMaterial != null)
                        {
                            physicsMaterial.InvokePhysicsEvent(new FootstepWalkEvent
                            {
                                FootstepPosition = new SpacePosition(_entitySpace, footPosition)
                            });

                            _stepTriggered = true;
                        }
                    }
                }
            }
        }

        private float CalculateStepHeight()
        {
            return transform.localPosition.y;
        }
    }
}
