﻿namespace Game.Entity
{
    /// <summary>
    /// Extensions for <see cref="IMortalEntity"/>
    /// </summary>
    public static class DamageableEntityExtensions
    {
        /// <summary>
        /// Does the <see cref="IMortalEntity"/> have health greater than 0
        /// </summary>
        public static bool IsAlive(this IMortalEntity entity)
        {
            return entity.HealthCurrent > 0;
        }
    }
}
