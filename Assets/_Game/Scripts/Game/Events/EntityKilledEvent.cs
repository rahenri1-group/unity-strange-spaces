﻿using Game.Entity;

namespace Game.Events
{
    /// <summary>
    /// Event raised when a <see cref="IMortalEntity"/> has its health reduced to zero
    /// </summary>
    public struct EntityKilledEvent
    {
        public IMortalEntity Entity;
    }
}