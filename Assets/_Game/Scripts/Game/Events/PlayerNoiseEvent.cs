﻿using Game.Core.Space;
using Game.Player;

namespace Game.Events
{
    // Event raised when a player makes a noise
    public struct PlayerNoiseEvent
    {
        /// <summary>
        /// The player that generated the noise
        /// </summary>
        public IPlayer Player;

        /// <summary>
        /// The position of the noise
        /// </summary>
        public SpacePosition NoisePosition;

        /// <summary>
        /// The intensity of the noise
        /// </summary>
        public PlayerNoiseIntensity NoiseIntensity;

        /// <summary>
        /// A description of the noise. FOR DEBUG PURPOSES ONLY
        /// </summary>
        public string Description;
    }
}