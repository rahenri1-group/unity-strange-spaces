﻿using Game.Core.Interaction;
using Game.Item;

namespace Game.Interaction
{
    /// <summary>
    /// An interactable that is intended to be used only if the player has a <see cref="IKeyItemDefinition"/>
    /// </summary>
    public interface IInteractableLock : IInteractable
    {
        /// <summary>
        /// The required item to use this interactable
        /// </summary>
        IKeyItemDefinition KeyItemDefinition { get; }
    }
}
