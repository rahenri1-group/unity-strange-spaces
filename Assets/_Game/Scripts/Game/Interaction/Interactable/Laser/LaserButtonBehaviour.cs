﻿using Game.Core;
using Game.Core.Interaction;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Interaction
{
    /// <summary>
    /// A button that is interactable with a laser
    /// </summary>
    public class LaserButtonBehaviour : BaseInteractable, ILaserInteractable
    {
        [SerializeField] private TMP_Text _buttonText = null;
        [SerializeField] private ColorReadonlyReference _textHoverColor = null;

        [SerializeField] private Material _hoverMaterial = null;
        [SerializeField] private Renderer[] _backgroundRenderers = new Renderer[0];
        

        private Material _defaultMaterial;
        private Color _textDefaultColor;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_buttonText);
            Assert.IsTrue(_backgroundRenderers.Length > 0);
            Assert.IsNotNull(_hoverMaterial);

            _textDefaultColor = _buttonText.color;
            _defaultMaterial = _backgroundRenderers[0].sharedMaterial;
        }

        /// <inheritdoc/>
        public override void OnHoverBegin(IInteractor hoverer)
        {
            base.OnHoverBegin(hoverer);

            _buttonText.color = _textHoverColor;

            foreach (var renderer in _backgroundRenderers)
            {
                renderer.sharedMaterial = _hoverMaterial;
            }
        }

        /// <inheritdoc/>
        public override void OnHoverEnd(IInteractor hoverer)
        {
            base.OnHoverEnd(hoverer);

            _buttonText.color = _textDefaultColor;

            foreach (var renderer in _backgroundRenderers)
            {
                renderer.sharedMaterial = _defaultMaterial;
            }
        }
    }
}