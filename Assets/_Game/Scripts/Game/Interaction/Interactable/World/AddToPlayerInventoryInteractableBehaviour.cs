﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Entity;
using Game.Core.Interaction;
using Game.Core.Item;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Item;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Interaction
{
    public class AddToPlayerInventoryInteractableBehaviour : BaseInteractable, IWorldInteractable
    {
        [Inject] private IDynamicEntityManager _dynamicEntityManager = null;
        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        [SerializeField] [TypeRestriction(typeof(IItemEntity))] private Component _itemEntityObj = null;
        private IItemEntity _itemEntity;

        [SerializeField] private IntReadonlyReference _itemQuantity = null;

        private bool _hasBeenAddedToPlayerInventory;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_itemEntityObj);
            _itemEntity = _itemEntityObj.GetComponentAsserted<IItemEntity>();

            _hasBeenAddedToPlayerInventory = false;
        }

        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            if (_hasBeenAddedToPlayerInventory)
            {
                return Interactions.None;
            }

            return base.AllowedInteractions(interactor);
        }

        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            var player = interactor.GetComponentInParent<IPlayer>();
            if (player == null) return;

            var itemEquiptor = interactor.GetComponent<IItemEquiptor>();

            _hasBeenAddedToPlayerInventory = true;

            var itemDefinition = _itemEntity.ItemDefinition;
            var playerItemDefinition = (IPlayerItemDefinition) itemDefinition;
            
            if (_itemQuantity < 1)
            {
                player.Inventory.AddItem(itemDefinition);
            }
            else 
            {
                player.Inventory.AddItems(itemDefinition, _itemQuantity);
            }

            InteractionManager.OnInteractableDisable(this);

            if (itemDefinition is IPlayerEquiptableItemDefinition && itemEquiptor != null)
            {
                (itemDefinition as IPlayerEquiptableItemDefinition).EquiptInstance(itemEquiptor);
            }
            else
            {
                if (!string.IsNullOrEmpty(playerItemDefinition.StashEffectAddress))
                {
                    _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                        playerItemDefinition.StashEffectAddress,
                        _spaceManager.GetEntitySpace(_itemEntity),
                        _itemEntity.EntityMovement.Position)
                        .Forget();
                }
            }

            _dynamicEntityManager.DestroyEntity(_itemEntity);
        }
    }
}
