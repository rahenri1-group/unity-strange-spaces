﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Core.Space;
using UnityEngine;

namespace Game.Item
{
    public abstract class BaseEquiptableItemDefinition : BasePlayerItemDefinition, IPlayerEquiptableItemDefinition
    {
        public string EquiptableAddress => _equiptableAddress;

        [Inject] protected IGameObjectResourceManager GameObjectResourceManager = null;
        [Inject] protected IInteractionManager InteractionManager = null;
        [Inject] protected ISpaceManager SpaceManager = null;

        [SerializeField] private string _equiptableAddress = "";

        /// <inheritdoc/>
        public void EquiptInstance(IItemEquiptor itemEquiptor)
        {
            InitializeIfNecessary();

            UniTask.Create(async () =>
            {
                var equiptorSpace = SpaceManager.GetObjectSpace(itemEquiptor.GameObject);
                var equitableItem = await GameObjectResourceManager.InstantiateAsync<IEquiptableItem>(
                    EquiptableAddress,
                    equiptorSpace,
                    itemEquiptor.GameObject.transform.position,
                    itemEquiptor.GameObject.transform.rotation);

                InteractionManager.AttemptInteractBegin(itemEquiptor, equitableItem.Component);
            }).Forget();
        }
    }
}
