﻿using Game.Core.Item;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Base class for player items
    /// </summary>
    public abstract class BasePlayerItemDefinition : BaseItemDefinition, IPlayerItemDefinition
    {
        /// <inheritdoc/>
        public string InventorySpriteIconAddress => _inventorySpriteIconAddress;
        /// <inheritdoc/>
        public string InventoryPreviewModelAddress => _inventoryPreviewModelAddress;

        /// <inheritdoc/>
        public string PickupEffectAddress => _playerPickupEffectAddress;
        /// <inheritdoc/>
        public string StashEffectAddress => _playerStashEffectAddress;
                
        [SerializeField] private string _inventorySpriteIconAddress = "";
        [SerializeField] private string _inventoryPreviewModelAddress = "";

        [SerializeField] private string _playerPickupEffectAddress = "";
        [SerializeField] private string _playerStashEffectAddress = "";
    }
}
