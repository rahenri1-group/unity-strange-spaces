﻿using Game.Core.Item;

namespace Game.Item
{
    /// <summary>
    /// Definition for an item that is used as a key
    /// </summary>
    public interface IKeyItemDefinition : IItemDefinition 
    {
        /// <summary>
        /// The address to get the model to be used for unlock animations
        /// </summary>
        string UnlockModelAddress { get; }
    }
}
