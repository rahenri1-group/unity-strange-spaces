﻿using Game.Core.Item;
using Game.Player;

namespace Game.Item
{
    /// <summary>
    /// An <see cref="IItemDefinition"/> that can consumed by the player to perform an action
    /// </summary>
    public interface IPlayerConsumableItemDefinition : IPlayerItemDefinition
    {
        /// <summary>
        /// Performs the items action on the provided <see cref="IPlayer"/> and removes it from their inventory
        /// </summary>
        void Consume(IPlayer player);
    }
}
