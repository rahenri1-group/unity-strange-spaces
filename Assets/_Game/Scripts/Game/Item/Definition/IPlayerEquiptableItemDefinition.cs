﻿using Game.Core.Item;

namespace Game.Item
{
    /// <summary>
    /// An <see cref="IItemDefinition"/> that can be equipped by the player
    /// </summary>
    public interface IPlayerEquiptableItemDefinition : IPlayerItemDefinition
    {
        /// <summary>
        /// Equipts a new instance of the item to <paramref name="itemEquiptor"/>
        /// </summary>
        void EquiptInstance(IItemEquiptor itemEquiptor);
    }
}
