﻿using Game.Core.Item;

namespace Game.Item
{
    /// <summary>
    /// An <see cref="IItemDefinition"/> that can be added to a player's inventory
    /// </summary>
    public interface IPlayerItemDefinition : IItemDefinition
    {
        /// <summary>
        /// The address to get the ui sprite representation of an item
        /// </summary>
        string InventorySpriteIconAddress { get; }

        /// <summary>
        /// The address to get the preview model of an item
        /// </summary>
        string InventoryPreviewModelAddress { get; }

        /// <summary>
        /// The address of the effect to be triggered when the player picks up this item
        /// </summary>
        string PickupEffectAddress { get; }
        /// <summary>
        /// The address of the effect to be triggered when the player stashes this item
        /// </summary>
        string StashEffectAddress { get; }
    }
}
