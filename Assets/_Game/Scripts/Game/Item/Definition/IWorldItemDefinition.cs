﻿using Game.Core.Item;

namespace Game.Item
{
    /// <summary>
    /// An <see cref="IItemDefinition"/> that has an in world representation
    /// </summary>
    public interface IWorldItemDefinition : IItemDefinition
    {
        /// <summary>
        /// The address to get the world representation of an item
        /// </summary>
        string ItemEntityAddress { get; }
    }
}
