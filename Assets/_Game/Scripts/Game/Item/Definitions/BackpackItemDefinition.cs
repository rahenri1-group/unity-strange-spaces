﻿using UnityEngine;

namespace Game.Item
{
    [CreateAssetMenu(menuName = "Game/Item/Backpack Item")]
    public class BackpackItemDefinition : BaseEquiptableItemDefinition { }
}
