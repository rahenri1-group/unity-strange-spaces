﻿using Game.Core;
using UnityEngine;

namespace Game.Item
{
    /// <inheritdoc cref="IPlayerEquiptableItemDefinition"/>
    [CreateAssetMenu(menuName = "Game/Item/Equitable Item")]
    public class EquiptableItemDefinition : BaseEquiptableItemDefinition, IWorldItemDefinition
    {
        /// <inheritdoc/>
        public string ItemEntityAddress => _itemEntityAddress;

        [SerializeField] private string _itemEntityAddress = "";
    }
}
