﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Item;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Player;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// An item definition for a healing item
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Item/Health Item")]
    public class HealthItemDefinition : BaseEquiptableItemDefinition, IPlayerConsumableItemDefinition, IWorldItemDefinition
    {
        /// <inheritdoc/>
        public string ItemEntityAddress => _itemEntityAddress;

        public int HealAmount => _healAmount;
        public string HealEffectAddress => _healEffectAddress;

        [SerializeField] private string _itemEntityAddress = "";

        [SerializeField] private IntReadonlyReference _healAmount = null;
        [SerializeField] private StringReadonlyReference _healEffectAddress = null;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;

        /// <inheritdoc/>
        public void Consume(IPlayer player)
        {
            InitializeIfNecessary();

            player.ApplyHealingToPlayer(_healAmount);

            player.Inventory.RemoveItem(this);

            if (!string.IsNullOrEmpty(_healEffectAddress))
            {
                _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                    _healEffectAddress, 
                    SpaceManager.GetEntitySpace(player), 
                    player.HeadPosition)
                    .Forget();
            }
        }
    }
}
