﻿using UnityEngine;

namespace Game.Item
{
    /// <inheritdoc cref="IKeyItemDefinition"/>
    [CreateAssetMenu(menuName = "Game/Item/Key Item")]
    public class KeyItemDefinition : BaseEquiptableItemDefinition, IWorldItemDefinition, IKeyItemDefinition
    {
        /// <inheritdoc/>
        public string ItemEntityAddress => _itemEntityAddress;

        /// <inheritdoc/>
        public string UnlockModelAddress => _unlockModelAddress;

        [SerializeField] private string _itemEntityAddress = "";

        [SerializeField] private string _unlockModelAddress = "";
    }
}
