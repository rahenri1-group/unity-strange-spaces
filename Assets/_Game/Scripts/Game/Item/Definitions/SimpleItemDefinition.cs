﻿using Game.Core;
using Game.Core.Item;
using UnityEngine;

namespace Game.Item
{
    /// <inheritdoc cref="IItemDefinition"/>
    [CreateAssetMenu(menuName = "Game/Item/Simple Item")]
    public class SimpleItemDefinition : BasePlayerItemDefinition, IWorldItemDefinition
    {
        /// <inheritdoc/>
        public string ItemEntityAddress => _itemEntityAddress;

        [SerializeField] private string _itemEntityAddress = "";
    }
}
