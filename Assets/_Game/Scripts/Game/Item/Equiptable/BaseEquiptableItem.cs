﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Space;
using Game.Input;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item
{
    /// <inheritdoc cref="IEquiptableItem"/>
    public abstract class BaseEquiptableItem : BaseInteractable, IEquiptableItem, IWorldInteractable
    {
        /// <inheritdoc/>
        public event Action<IEquiptableItem> ItemEquip;
        /// <inheritdoc/>
        public event Action<IEquiptableItem> ItemUnequip;

        /// <inheritdoc/>
        public IItemEquiptor CurrentEquipter { get; private set; }

        /// <inheritdoc/>
        public IPlayerEquiptableItemDefinition ItemDefinition { get; private set; }

        [Inject] protected ILogRouter Logger;
        [Inject] protected ISpaceManager SpaceManager = null;
        [Inject] protected ISpacePhysics SpacePhysics = null;

        [SerializeField] [TypeRestriction(typeof(IPlayerEquiptableItemDefinition))] UnityEngine.Object _itemDefinition = null;

        [SerializeField] private FloatReadonlyReference _itemReturnDetectionRadius = null;
        [SerializeField] [Layer] private int[] _itemReturnDetectionLayers = new int[0];

        private int _itemReturnDetectionMask;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_itemDefinition);
            ItemDefinition = (IPlayerEquiptableItemDefinition)_itemDefinition;

            Assert.IsTrue(_itemReturnDetectionRadius > 0f);

            _itemReturnDetectionMask = LayerUtil.ConstructMaskForLayers(_itemReturnDetectionLayers);

            CurrentEquipter = null;
        }

        /// <inheritdoc/>
        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            CurrentEquipter = interactor as IItemEquiptor;
            if (CurrentEquipter != null)
            {
                OnItemEquip(CurrentEquipter);
            }
            else
            {
                Logger.LogWarning($"{GameObject.name} has been interacted with a non {typeof(IItemEquiptor).Name} ({interactor.GameObject.name})");
            }
        }

        /// <inheritdoc/>
        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            if (interactor is IShadowWorldInteractor)
            {
                return Interactions.None;
            }

            return base.AllowedInteractions(interactor);
        }

        protected virtual void OnItemEquip(IItemEquiptor equiptor)
        {
            equiptor.ItemInput.ReleaseItemActionTriggered += OnAttemptReleaseItem;
            
            ItemEquip?.Invoke(this);
        }

        /// <inheritdoc/>
        public override void OnInteractEnd(IInteractor interactor)
        {
            base.OnInteractEnd(interactor);

            CurrentEquipter = interactor as IItemEquiptor;
            if (CurrentEquipter != null)
            {
                OnItemUnequip(CurrentEquipter);
            }
        }

        protected virtual void OnItemUnequip(IItemEquiptor equiptor) 
        {
            equiptor.ItemInput.ReleaseItemActionTriggered -= OnAttemptReleaseItem;

            ItemUnequip?.Invoke(this);
        }

        /// <inheritdoc/>
        public virtual void PreDestroyItem() { }

        private void OnAttemptReleaseItem(InputContext inputContext)
        {
            var itemSpace = SpaceManager.GetObjectSpace(gameObject);
            
            if (SpacePhysics.FindInSphere<IEquiptableItemReleasor>(
                itemSpace, 
                transform.position, 
                _itemReturnDetectionRadius, 
                out var resultInfo, 
                QueryPortalInteraction.Ignore, 
                _itemReturnDetectionMask))
            {
                resultInfo.Result.OnPlayerAttemptReleaseItem(this);
            }
        }
    }
}
