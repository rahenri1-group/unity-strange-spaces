﻿using UnityEngine.Assertions;

namespace Game.Item
{
    public class ConsumableEquiptableBehaviour : BaseEquiptableItem, IConsumableEquiptableItem
    {
        public IPlayerConsumableItemDefinition PlayerConsumableItemDefinition => (IPlayerConsumableItemDefinition)ItemDefinition;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(ItemDefinition is IPlayerConsumableItemDefinition);
        }
    }
}
