﻿using Game.Core;
using Game.Core.Audio;
using UnityEngine;

namespace Game.Item.Flashlight
{
    public class FlashlightAudioBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] [TypeRestriction(typeof(IFlashlight))] private Component _flashlightObj = null;
        private IFlashlight _flashlight;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            _flashlight = _flashlightObj.GetComponentAsserted<IFlashlight>();
        }

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();

            _flashlight.FlashlightActivated += OnFlashlightActivated;
        }

        /// <inheritdoc />
        protected override void OnDisable()
        {
            base.OnDisable();

            _flashlight.FlashlightActivated -= OnFlashlightActivated;
        }

        private void OnFlashlightActivated(IFlashlight sender)
        {
            PlayCurrentClip();
        }
    }
}
