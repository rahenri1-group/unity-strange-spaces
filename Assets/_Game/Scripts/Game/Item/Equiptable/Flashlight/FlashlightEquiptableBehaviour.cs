﻿using Game.Core;
using Game.Core.Render.Lighting;
using Game.Input;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Flashlight
{
    public class FlashlightEquiptableBehaviour : BaseEquiptableItem, IFlashlight
    {
        /// <inheritdoc/>
        public event Action<IFlashlight> FlashlightActivated;

        [SerializeField] [TypeRestriction(typeof(ILight))] private Component _lightObj = null;
        private ILight _light;

        [SerializeField] private Renderer _lightLensRenderer = null;

        [SerializeField] private FloatReadonlyReference _flipDuration = null;

        [SerializeField] private Quaternion _defaultRotation = Quaternion.identity;
        [SerializeField] private Quaternion _flipRotation = Quaternion.identity;

        private bool _isFlipped;

        private IEnumerator _flipCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_lightObj);
            _light = _lightObj.GetComponent<ILight>();

            Assert.IsNotNull(_lightLensRenderer);
            Assert.IsTrue(_flipDuration > 0f);

            _isFlipped = false;

            _flipCoroutine = null;
        }

        private void OnEnable()
        {
            transform.localRotation = _isFlipped ? _flipRotation : _defaultRotation;
        }

        private void OnDisable()
        {
            if (_flipCoroutine != null)
            {
                StopCoroutine(_flipCoroutine);
                _flipCoroutine = null;
            }
        }

        protected override void OnItemEquip(IItemEquiptor equiptor)
        {
            base.OnItemEquip(equiptor);

            equiptor.ItemInput.UseItemActionTriggered += OnUseItemAction;
            equiptor.ItemInput.ItemAlternateActionTriggered += OnItemAlternateAction;
        }

        protected override void OnItemUnequip(IItemEquiptor equiptor)
        {
            base.OnItemUnequip(equiptor);

            _light.LightEnabled = false;
            _lightLensRenderer.enabled = false;

            equiptor.ItemInput.UseItemActionTriggered -= OnUseItemAction;
            equiptor.ItemInput.ItemAlternateActionTriggered -= OnItemAlternateAction;
        }

        private void OnUseItemAction(InputContext context, bool useItem)
        {
            if (useItem)
            {
                bool lightEnabled = !_light.LightEnabled;

                _light.LightEnabled = lightEnabled;
                _lightLensRenderer.enabled = lightEnabled;

                FlashlightActivated?.Invoke(this);
            }
        }

        private void OnItemAlternateAction(InputContext context, bool alternateAction)
        {
            if (alternateAction)
            {
                if (_flipCoroutine != null)
                {
                    StopCoroutine(_flipCoroutine);
                }

                _flipCoroutine = Flip(_isFlipped ? _defaultRotation : _flipRotation, _flipDuration, () =>
                {
                    _isFlipped = !_isFlipped;
                });
                StartCoroutine(_flipCoroutine);
            }
        }

        private IEnumerator Flip(Quaternion targetRotation, float duration, Action onComplete = null)
        {
            YieldInstruction wait = null;

            var startRotation = transform.localRotation;
            float startTime = Time.time;

            while (Time.time - startTime < duration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / duration);
                transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, lerp);
            }
            transform.localRotation = targetRotation;

            _flipCoroutine = null;

            onComplete?.Invoke();
        }
    }
}
