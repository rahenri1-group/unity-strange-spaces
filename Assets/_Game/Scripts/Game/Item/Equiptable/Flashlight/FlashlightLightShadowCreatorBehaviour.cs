﻿using Game.Core.DependencyInjection;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Player;
using UnityEngine;

namespace Game.Item.Flashlight
{
    public class FlashlightLightShadowCreatorBehaviour : DynamicLightShadowCreatorBehaviour                                                         
    {
        [Inject] private IPlayerManager _playerManager;

        protected override IPortal DetectPortal()
        {
            var detectedPortal = base.DetectPortal();
            if (detectedPortal == null)
            {
                if (SpaceManager.AreInSameSpace(_playerManager.Player.GameObject, gameObject))
                {
                    var lightSpace = SpaceManager.GetObjectSpace(gameObject);

                    if (SpacePhysics.Raycast(
                        lightSpace,
                        _playerManager.Player.HeadPosition,
                        (transform.position - _playerManager.Player.HeadPosition).normalized,
                        out var hitInfo,
                        Vector3.Distance(_playerManager.Player.HeadPosition, transform.position) * 1.1f,
                        QueryPortalInteraction.Block,
                        PortalQueryMask))
                    {
                        detectedPortal = hitInfo.Collider.GetComponentInParent<IPortal>();
                    }
                }
            }

            return detectedPortal;
        }
    }
}
