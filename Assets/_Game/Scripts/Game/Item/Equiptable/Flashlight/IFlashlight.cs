﻿using System;

namespace Game.Item.Flashlight
{
    public interface IFlashlight
    {
        /// <summary>
        /// Event raised when the flashlight is activated
        /// </summary>
        event Action<IFlashlight> FlashlightActivated;
    }
}
