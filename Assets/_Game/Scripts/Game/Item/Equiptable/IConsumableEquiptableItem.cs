﻿namespace Game.Item
{
    public interface IConsumableEquiptableItem : IEquiptableItem
    {
        IPlayerConsumableItemDefinition PlayerConsumableItemDefinition { get; }
    }
}
