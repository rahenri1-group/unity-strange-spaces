﻿using Game.Core;
using Game.Core.Interaction;
using System;

namespace Game.Item
{
    /// <summary>
    /// An item the can be equipped by an <see cref="IVrItemEquiptor"/>
    /// </summary>
    public interface IEquiptableItem : IInteractable, IGameObjectComponent 
    {
        /// <summary>
        /// Event raised when the item is equipped
        /// </summary>
        event Action<IEquiptableItem> ItemEquip;
        /// <summary>
        /// Event raised when the item is unequipped
        /// </summary>
        event Action<IEquiptableItem> ItemUnequip;

        /// <summary>
        /// The equipter holding this item.
        /// </summary>
        IItemEquiptor CurrentEquipter { get; }

        /// <summary>
        /// The definition for the item
        /// </summary>
        IPlayerEquiptableItemDefinition ItemDefinition { get; }

        /// <summary>
        /// Called before the item is stashed and destroyed. The <see cref="CurrentEquipter"/> will still reflect the holder at this time.
        /// </summary>
        void PreDestroyItem();
    }
}
