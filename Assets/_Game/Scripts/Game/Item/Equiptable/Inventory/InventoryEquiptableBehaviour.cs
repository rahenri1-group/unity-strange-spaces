﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Item;
using Game.Player;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Inventory
{
    public class InventoryEquiptableBehaviour : BaseEquiptableItem
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private IPlayerManager _playerManager = null;

        [SerializeField] private Transform _pivotTransform = null;
        [Space]
        [SerializeField] private Vector3 _leftHandPivotPosition = Vector3.zero;
        [SerializeField] private Quaternion _leftHandPivotRotation = Quaternion.identity;
        [Space]
        [SerializeField] private Vector3 _rightHandPivotPosition = Vector3.zero;
        [SerializeField] private Quaternion _rightHandPivotRotation = Quaternion.identity;
        [Space]
        [SerializeField] private InventorySlotBehaviour[] _inventorySlots = new InventorySlotBehaviour[0];

        private IItemEquiptor[] _playerEquiptors;
        private IInventory _displayedInventory;

        private CancellationTokenSource _inventorySlotsShowCts;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_pivotTransform);
            Assert.IsTrue(_inventorySlots.Length > 0);

            _playerEquiptors = null;
            _displayedInventory = null;
            _inventorySlotsShowCts = null;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_inventorySlotsShowCts != null)
            {
                _inventorySlotsShowCts.CancelAndDispose();
                _inventorySlotsShowCts = null;
            }
        }

        protected override void OnItemEquip(IItemEquiptor equiptor)
        {
            base.OnItemEquip(equiptor);

            var vrEquiptor = equiptor as IVrItemEquiptor;
            if (vrEquiptor != null)
            {
                if (vrEquiptor.IsLeftHand)
                {
                    _pivotTransform.localPosition = _leftHandPivotPosition;
                    _pivotTransform.localRotation = _leftHandPivotRotation;
                }
                else
                {
                    _pivotTransform.localPosition = _rightHandPivotPosition;
                    _pivotTransform.localRotation = _rightHandPivotRotation;
                }
            }

            _displayedInventory = _playerManager.Player.Inventory;

            _playerEquiptors = _playerManager.Player.GetComponentsInChildren<IItemEquiptor>(true);
            if (_playerEquiptors != null)
            {
                foreach (var playerEquiptor in _playerEquiptors)
                {
                    playerEquiptor.EquiptedItemChanged += OnPlayerEquiptorsEquipt;
                }
            }

            if (_inventorySlotsShowCts != null)
            {
                _inventorySlotsShowCts.CancelAndDispose();
            }

            _inventorySlotsShowCts = new CancellationTokenSource();

            RefreshInventorySlots(_inventorySlotsShowCts.Token).Forget();

            _displayedInventory.InventoryContentsUpdated += OnInventoryContentsUpdated;
        }

        protected override void OnItemUnequip(IItemEquiptor equiptor)
        {
            base.OnItemUnequip(equiptor);

            _pivotTransform.localPosition = Vector3.zero;
            _pivotTransform.localRotation = Quaternion.identity;

            if (_inventorySlotsShowCts != null)
            {
                _inventorySlotsShowCts.CancelAndDispose();
                _inventorySlotsShowCts = null;
            }

            if (_displayedInventory != null)
            {
                _displayedInventory.InventoryContentsUpdated -= OnInventoryContentsUpdated;
                _displayedInventory = null;
            }

            if (_playerEquiptors != null)
            {
                foreach (var playerEquiptor in _playerEquiptors)
                {
                    playerEquiptor.EquiptedItemChanged -= OnPlayerEquiptorsEquipt;
                }
                _playerEquiptors = null;
            }

            foreach (var inventorySlot in _inventorySlots)
            {
                inventorySlot.ClearSlot();
            }
        }

        private void OnPlayerEquiptorsEquipt()
        {
            if (_inventorySlotsShowCts != null)
            {
                _inventorySlotsShowCts.CancelAndDispose();
            }

            _inventorySlotsShowCts = new CancellationTokenSource();

            RefreshInventorySlots(_inventorySlotsShowCts.Token).Forget();
        }

        private void OnInventoryContentsUpdated()
        {
            if (_inventorySlotsShowCts != null)
            {
                _inventorySlotsShowCts.CancelAndDispose();
            }

            _inventorySlotsShowCts = new CancellationTokenSource();

            RefreshInventorySlots(_inventorySlotsShowCts.Token).Forget();
        }

        private async UniTask RefreshInventorySlots(CancellationToken cancellationToken)
        {
            var itemDefinitions = _displayedInventory.AllDefinitions();

            if (itemDefinitions.Length > _inventorySlots.Length)
            {
                Logger.LogWarning($"Player inventory has more items than can be displayed. Only first {_inventorySlots.Length} will be shown");
            }

            var heldItems = new Dictionary<IItemDefinition, int>();
            foreach (var itemEquiptor in _playerEquiptors)
            {
                if (itemEquiptor.EquiptedItem != null)
                {
                    if (!heldItems.ContainsKey(itemEquiptor.EquiptedItem.ItemDefinition))
                    {
                        heldItems[itemEquiptor.EquiptedItem.ItemDefinition] = 0;
                    }

                    heldItems[itemEquiptor.EquiptedItem.ItemDefinition] += 1;
                }
            }

            var inventorySlotDisplayTasks = new List<UniTask>();
            for (int i = 0; i < _inventorySlots.Length; i++)
            {
                var inventorySlot = _inventorySlots[i];

                if (i < itemDefinitions.Length)
                {
                    var itemDefintion = itemDefinitions[i];
                    int quantity = _displayedInventory.DefinitionQuantity(itemDefintion);
                    if (heldItems.ContainsKey(itemDefintion))
                    {
                        quantity -= heldItems[itemDefintion];
                    }

                    var displayTask = inventorySlot.DisplayItem(itemDefintion, quantity, cancellationToken);
                    inventorySlotDisplayTasks.Add(displayTask);
                }
                else
                {
                    inventorySlot.ClearSlot();
                }
            }

            await UniTask.WhenAll(inventorySlotDisplayTasks);
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var entity = GetComponentInParent<IEntity>();
            if (entity != null)
            {
                _eventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = entity
                });
            }
        }
    }
}
