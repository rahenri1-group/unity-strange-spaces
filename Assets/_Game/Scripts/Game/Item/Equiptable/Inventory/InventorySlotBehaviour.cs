﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Item;
using Game.Core.Render.Lighting;
using Game.Core.Resource;
using Game.Core.Space;
using System;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Inventory
{
    public class InventorySlotBehaviour : BaseInteractable, IWorldInteractable
    {
        public IPlayerItemDefinition ItemDefinition { get; private set; }

        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        [SerializeField] private GameObject _fallbackDisplayModelPrefab = null;

        [SerializeField] private TMP_Text _itemNameText = null;
        [SerializeField] private TMP_Text _itemCountText = null;

        [SerializeField] [TypeRestriction(typeof(ILight))] private Component _inventoryLightObj = null;
        private ILight _inventoryLight;

        private GameObject _loadedPreviewModel;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_fallbackDisplayModelPrefab);
            Assert.IsNotNull(_itemNameText);
            Assert.IsNotNull(_itemCountText);
            Assert.IsNotNull(_inventoryLightObj);

            _inventoryLight = _inventoryLightObj.GetComponentAsserted<ILight>();

            ItemDefinition = null;
            _itemNameText.enabled = false;
            _itemCountText.enabled = false;

            _loadedPreviewModel = null;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_loadedPreviewModel != null)
            {
                _gameObjectResourceManager.DestroyObject(_loadedPreviewModel);
                _loadedPreviewModel = null;
            }
        }

        public async UniTask DisplayItem(IItemDefinition itemDefinition, int quantity, CancellationToken cancellationToken)
        {
            ClearSlot();
            if (quantity <= 0) 
            {
                return;
            }

            ItemDefinition = (IPlayerItemDefinition)itemDefinition;

            _itemNameText.text = ItemDefinition.Name;

            if (quantity > 1)
            {
                _itemCountText.enabled = true;
                _itemCountText.text = $"x {quantity}";
            }
            else
            {
                _itemCountText.enabled = false;
            }

            if (!string.IsNullOrEmpty(ItemDefinition.InventoryPreviewModelAddress))
            {
                _loadedPreviewModel = await _gameObjectResourceManager.InstantiateAsync(
                    ItemDefinition.InventoryPreviewModelAddress,
                    _spaceManager.GetObjectSpace(gameObject),
                    transform.position,
                    transform.rotation,
                    Vector3.one);

                if (cancellationToken.IsCancellationRequested)
                {
                    _gameObjectResourceManager.DestroyObject(_loadedPreviewModel);
                    _loadedPreviewModel = null;
                    return;
                }
            }
            else
            {
                _loadedPreviewModel = Instantiate(_fallbackDisplayModelPrefab, transform);
            }

            _loadedPreviewModel.transform.SetParent(transform);
            _loadedPreviewModel.transform.localPosition = Vector3.zero;
            _loadedPreviewModel.transform.localRotation = Quaternion.identity;
            _loadedPreviewModel.transform.localScale = Vector3.one;

            _inventoryLight.LightEnabled = true;

            return;
        }

        public void ClearSlot()
        {
            ItemDefinition = null;
            _itemNameText.enabled = false;
            _itemCountText.enabled = false;

            _inventoryLight.LightEnabled = false;

            if (_loadedPreviewModel != null)
            {
                _gameObjectResourceManager.DestroyObject(_loadedPreviewModel);
                _loadedPreviewModel = null;
            }
        }

        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            if (ItemDefinition == null || interactor is IShadowWorldInteractor)
            {
                return Interactions.None;
            }

            return base.AllowedInteractions(interactor);
        }

        public override void OnHoverBegin(IInteractor hoverer)
        {
            base.OnHoverBegin(hoverer);

            if (ItemDefinition != null)
            {
                _itemNameText.enabled = true;
            }
        }

        public override void OnHoverEnd(IInteractor hoverer)
        {
            base.OnHoverEnd(hoverer);

            _itemNameText.enabled = false;
        }

        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            if (ItemDefinition is IPlayerEquiptableItemDefinition)
            {
                var itemEquiptor = interactor.GetComponent<IItemEquiptor>();
                if (itemEquiptor != null)
                {
                    (ItemDefinition as IPlayerEquiptableItemDefinition).EquiptInstance(itemEquiptor);
                }
            }
        }
    }
}
