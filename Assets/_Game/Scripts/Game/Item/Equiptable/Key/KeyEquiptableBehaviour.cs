﻿namespace Game.Item.Key
{
    /// <summary>
    /// A key that can be equipped by the player
    /// </summary>
    public class KeyEquiptableBehaviour : BaseEquiptableItem, IKeyItemDefinitionProvider
    {
        /// <inheritdoc/>
        public bool ContainsKeyItem(IKeyItemDefinition keyItemDefinition)
        {
            return ItemDefinition == keyItemDefinition;
        }
    }
}
