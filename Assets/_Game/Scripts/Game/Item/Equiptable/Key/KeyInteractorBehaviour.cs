﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Interaction;
using UnityEngine;

namespace Game.Item.Key
{
    /// <summary>
    /// Interactor for a key
    /// </summary>
    [RequireComponent(typeof(IEquiptableItem))]
    public class KeyInteractorBehaviour : BaseEntityInteractorBehaviour
    {
        /// <inheritdoc/>
        public override Vector3 Position => transform.position;
        /// <inheritdoc/>
        public override Quaternion Rotation => transform.rotation;
        /// <inheritdoc/>
        public override Rigidbody Body => _equiptableItem.CurrentInteractor?.Body;

        /// <inheritdoc/>
        public override GameObject InteractorOwner => gameObject;
        /// <inheritdoc/>
        public override bool IsInteractionEnabled => _equiptableItem.IsBeingInteractedUpon && base.IsInteractionEnabled;

        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;

        private IEquiptableItem _equiptableItem;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _equiptableItem = this.GetComponentAsserted<IEquiptableItem>();
            _equiptableItem.ItemEquip += OnItemEquip;
            _equiptableItem.ItemUnequip += OnItemUnequip;
        }

        /// <inheritdoc/>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            _equiptableItem.ItemEquip -= OnItemEquip;
            _equiptableItem.ItemUnequip -= OnItemUnequip;
        }

        private void OnItemEquip(IEquiptableItem item)
        {
            Entity = GetComponentInParent<IEntity>();
        }

        private void OnItemUnequip(IEquiptableItem item)
        {
            Entity = null;
        }

        /// <inheritdoc/>
        public override bool CanInteractWith(IInteractable interactble)
        {
            return interactble is IInteractableLock && base.CanInteractWith(interactble);
        }

        /// <inheritdoc />
        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (IsInteractionEnabled && !IsInteracting && InteractionTarget != null)
            {
                InteractBegin();
                InteractEnd();

                InteractionManager.Interact(_equiptableItem.CurrentInteractor, _equiptableItem);

                _gameObjectResourceManager.DestroyObject(gameObject);
            }
        }
    }
}
