﻿using Game.Core;
using Game.Core.Math;
using Game.Player;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverCylinderBehaviour : MonoBehaviour
    {
        public event Action CylinderLoaded;

        public bool ReadyToFire => FullSlots > 0;

        public int FullSlots => _cartridgeModels.Length - OpenSlots;
        public int OpenSlots { get; private set; }

        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private RevolverYokeBehaviour _revolverYoke = null;

        [SerializeField] private FloatReadonlyReference _cylinderBaseAngle = null;
        [SerializeField] private FloatReadonlyReference _cylinderRotateStep = null;

        [SerializeField] private GameObject[] _cartridgeModels = new GameObject[0];

        private bool _cylinderLocked;

        private float _currentRotationStart;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_revolverYoke);

            OpenSlots = _cartridgeModels.Length;

            _cylinderLocked = false;

            _currentRotationStart = _cylinderBaseAngle;

            UpdateCartridgeModels();
        }

        private void OnEnable()
        {
            _revolver.TriggerMoved += OnTriggerMoved;
            _revolver.TriggerReset += OnTriggerReset;
            _revolver.GunDryFired += OnTriggerFired;
            _revolver.GunShot += OnTriggerFired;
            _revolverYoke.Opened += OnYokeOpened;
            _revolverYoke.Closed += OnYokeClosed;
        }

        private void OnDisable()
        {
            _revolver.TriggerMoved -= OnTriggerMoved;
            _revolver.TriggerReset -= OnTriggerReset;
            _revolver.GunDryFired -= OnTriggerFired;
            _revolver.GunShot -= OnTriggerFired;
            _revolverYoke.Opened -= OnYokeOpened;
            _revolverYoke.Closed -= OnYokeClosed;
        }

        private void OnTriggerMoved()
        {
            if (!_cylinderLocked && !_revolverYoke.IsOpen)
            {
                SetRotation(Mathf.LerpAngle(_currentRotationStart, _currentRotationStart + _cylinderRotateStep, _revolver.TriggerPosition));
            }
        }

        private void OnTriggerReset()
        {
            if (!_revolverYoke.IsOpen)
            {
                _currentRotationStart = MathUtil.NormalizeAngle360(_currentRotationStart + _cylinderRotateStep);
                SetRotation(_currentRotationStart);
            }

            _cylinderLocked = false;
        }

        private void OnTriggerFired()
        {
            _cylinderLocked = true;
        }

        private void OnYokeOpened()
        {
            _currentRotationStart = MathUtil.NormalizeAngle360(_cylinderBaseAngle - FullSlots * _cylinderRotateStep);
            SetRotation(_currentRotationStart);
        }

        private void OnYokeClosed()
        {
            _currentRotationStart = MathUtil.NormalizeAngle360(_cylinderBaseAngle - FullSlots * _cylinderRotateStep);
            SetRotation(_currentRotationStart);
        }

        public void LoadCartridges(int quantity)
        {
            if (quantity > OpenSlots)
            {
                Debug.LogError($"Attempt to load revolver with ${quantity} when there are only {OpenSlots} open slots");
                return;
            }

            OpenSlots -= quantity;

            UpdateCartridgeModels();

            CylinderLoaded?.Invoke();
        }

        public void FireReadiedCartridge()
        {
            if (!ReadyToFire)
            {
                Debug.LogError($"Attempt to fire revolver when it does not have a cartridge ready");
                return;
            }

            OpenSlots += 1;
            UpdateCartridgeModels();
        }

        public void UnloadCartridges()
        {
            // return unshot ammo to inventory
            var ammoInventory = _revolver.CurrentInteractor.InteractorOwner.GetComponent<IPlayer>().Inventory;
            ammoInventory.AddItems(_revolver.AmmoItemDefinition, FullSlots);

            OpenSlots = _cartridgeModels.Length;
            UpdateCartridgeModels();
        }

        private void SetRotation(float rotation)
        {
            transform.localEulerAngles = new Vector3(0f, 0f, rotation);
        }

        private void UpdateCartridgeModels()
        {
            for (int i = 0; i < _cartridgeModels.Length; i++)
            {
                _cartridgeModels[i].SetActive(i < FullSlots);
            }
        }
    }
}