﻿using Game.Core;
using Game.Core.Item;
using Game.Input;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverEquiptableBehaviour : BaseEquiptableItem
    {
        /// <summary>
        /// Event raised when the revolver successfully fires a round
        /// </summary>
        public event Action GunShot;
        /// <summary>
        /// Event raised when there is an attempt to fire the revolver but it is empty
        /// </summary>
        public event Action GunDryFired;
        /// <summary>
        /// Event raised when the trigger is reset
        /// </summary>
        public event Action TriggerReset;
        /// <summary>
        /// Event raised when the value of the trigger has changed
        /// </summary>
        public event Action TriggerMoved;

        /// <summary>
        /// The item definition of of the ammo for the revolver
        /// </summary>
        public IItemDefinition AmmoItemDefinition { get; private set; }

        /// <summary>
        /// Value 0 to 1 indicating the how much the user has pulled the trigger
        /// </summary>
        public float TriggerPosition { get; private set; }

        /// <summary>
        /// Has the trigger been reset? The revolver will not fire until the trigger has been reset.
        /// </summary>
        public bool IsTriggerReset { get; private set; }

        [SerializeField] [TypeRestriction(typeof(IItemDefinition))] UnityEngine.Object _ammoItemDefinitionObj = null;

        [SerializeField] private RevolverYokeBehaviour _revolverYoke = null;
        [SerializeField] private RevolverCylinderBehaviour _revolverCylinder = null;

        [SerializeField] private FloatReadonlyReference _triggerResetThreshold = null;

        private IEnumerator _triggerReadCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_ammoItemDefinitionObj);
            AmmoItemDefinition = (IItemDefinition)_ammoItemDefinitionObj;
            Assert.IsNotNull(_revolverYoke);
            Assert.IsNotNull(_revolverCylinder);
            Assert.IsTrue(0f <= _triggerResetThreshold && _triggerResetThreshold <= 1f);

            IsTriggerReset = false;

            _triggerReadCoroutine = null;
        }

        /// <inheritdoc/>
        private void OnEnable()
        {
            if (CurrentEquipter != null && _triggerReadCoroutine == null)
            {
                _triggerReadCoroutine = TriggerRead();
                StartCoroutine(_triggerReadCoroutine);
            }
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            if (_triggerReadCoroutine != null)
            {
                StopCoroutine(_triggerReadCoroutine);
                _triggerReadCoroutine = null;
            }
        }

        protected override void OnItemEquip(IItemEquiptor equiptor)
        {
            base.OnItemEquip(equiptor);

            IsTriggerReset = true;
            TriggerPosition = 0f;
            TriggerMoved?.Invoke();

            if (_triggerReadCoroutine == null)
            {
                _triggerReadCoroutine = TriggerRead();
                StartCoroutine(_triggerReadCoroutine);
            }

            equiptor.ItemInput.UseItemActionTriggered += OnUseItemAction;
            equiptor.ItemInput.ItemAlternateActionTriggered += OnItemAlternateAction;
        }

        protected override void OnItemUnequip(IItemEquiptor equiptor)
        {
            base.OnItemUnequip(equiptor);

            if (_triggerReadCoroutine != null)
            {
                StopCoroutine(_triggerReadCoroutine);
                _triggerReadCoroutine = null;
            }

            equiptor.ItemInput.UseItemActionTriggered -= OnUseItemAction;
            equiptor.ItemInput.ItemAlternateActionTriggered -= OnItemAlternateAction;
        }

        private void OnUseItemAction(InputContext context, bool useItem)
        {
            if (useItem && IsTriggerReset)
            {
                IsTriggerReset = false;

                if (!_revolverYoke.IsOpen && _revolverCylinder.ReadyToFire)
                {
                    _revolverCylinder.FireReadiedCartridge();

                    GunShot?.Invoke();
                }
                else
                {
                    GunDryFired?.Invoke();
                }
            }
        }

        public override void PreDestroyItem()
        {
            base.PreDestroyItem();

            if (_revolverCylinder.FullSlots > 0)
            {
                _revolverCylinder.UnloadCartridges();
            }
        }

        private void OnItemAlternateAction(InputContext context, bool value)
        {
            if (!_revolverYoke.IsOpen)
            {
                _revolverYoke.Open();
            }
        }

        private IEnumerator TriggerRead()
        {
            YieldInstruction wait = null;

            while (true)
            {
                var oldTriggerPosition = TriggerPosition;

                TriggerPosition = CurrentEquipter?.ItemInput.ItemShootActionValue ?? 0f;
                
                if(!IsTriggerReset && TriggerPosition <= _triggerResetThreshold)
                {
                    IsTriggerReset = true;
                    TriggerReset?.Invoke();
                }

                if (oldTriggerPosition != TriggerPosition)
                {
                    TriggerMoved?.Invoke();
                }

                yield return wait;
            }
        }
    }
}
