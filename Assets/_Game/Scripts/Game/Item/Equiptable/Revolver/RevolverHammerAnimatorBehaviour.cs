﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverHammerAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;

        [SerializeField] private Transform _hammerTransform = null;
        [SerializeField] private Vector3 _animationAxis = new Vector3();

        [SerializeField] private FloatReadonlyReference _hammerDownAngle = null;
        [SerializeField] private FloatReadonlyReference _hammerCockedAngle = null;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_hammerTransform);
            Assert.IsTrue(_animationAxis.magnitude == 1f);
        }

        private void OnEnable()
        {
            _revolver.TriggerMoved += OnRevolverUpdate;
            _revolver.GunShot += OnRevolverUpdate;
            _revolver.GunDryFired += OnRevolverUpdate;
        }

        private void OnDisable()
        {
            _revolver.TriggerMoved -= OnRevolverUpdate;
            _revolver.GunShot -= OnRevolverUpdate;
            _revolver.GunDryFired -= OnRevolverUpdate;
        }

        private void OnRevolverUpdate()
        {
            float triggerAngle = _hammerDownAngle;
            if (_revolver.IsTriggerReset)
            {
                triggerAngle = Mathf.LerpAngle(_hammerDownAngle, _hammerCockedAngle, _revolver.TriggerPosition);
            }
            _hammerTransform.localEulerAngles = triggerAngle * _animationAxis;
        }
    }
}
