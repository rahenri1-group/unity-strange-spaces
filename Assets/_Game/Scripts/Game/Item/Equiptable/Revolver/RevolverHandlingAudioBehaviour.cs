﻿using Game.Core;
using Game.Core.Audio;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverHandlingAudioBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private RevolverCylinderBehaviour _revolverCylinder = null;
        [SerializeField] private RevolverYokeBehaviour _revolverYoke = null;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _dryFireAudioClipsObj = null;
        private IAudioClipCollection _dryFireAudioClips = null;

        [SerializeField] [TypeRestriction(typeof(IAudioClipCollection), false)] private Object _triggerResetClipsObj = null;
        private IAudioClipCollection _triggerResetClips = null;

        [SerializeField][TypeRestriction(typeof(IAudioClipCollection), false)] private Object _cylinderLoadClipsObj = null;
        private IAudioClipCollection _cylinderLoadClips = null;

        [SerializeField][TypeRestriction(typeof(IAudioClipCollection), false)] private Object _yokeOpenClipsObj = null;
        private IAudioClipCollection _yokeOpenClips = null;

        [SerializeField][TypeRestriction(typeof(IAudioClipCollection), false)] private Object _yokeCloseClipsObj = null;
        private IAudioClipCollection _yokeCloseClips = null;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_revolverCylinder);
            Assert.IsNotNull(_revolverYoke);

            _dryFireAudioClips = _dryFireAudioClipsObj != null ? (IAudioClipCollection)_dryFireAudioClipsObj : null;
            _triggerResetClips = _triggerResetClipsObj != null ? (IAudioClipCollection)_triggerResetClipsObj : null;

            _cylinderLoadClips = _cylinderLoadClipsObj != null ? (IAudioClipCollection)_cylinderLoadClipsObj : null;

            _yokeOpenClips = _yokeOpenClipsObj != null ? (IAudioClipCollection)_yokeOpenClipsObj : null;
            _yokeCloseClips = _yokeCloseClipsObj != null ? (IAudioClipCollection)_yokeCloseClipsObj : null;
        }

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();

            _revolver.GunDryFired += OnDryFired;
            _revolver.TriggerReset += OnTriggerReset;

            _revolverCylinder.CylinderLoaded += OnCylinderLoaded;

            _revolverYoke.Opened += OnYokeOpened;
            _revolverYoke.Closed += OnYokeClosed;
        }

        /// <inheritdoc />
        protected override void OnDisable()
        {
            base.OnDisable();

            _revolver.GunDryFired -= OnDryFired;
            _revolver.TriggerReset -= OnTriggerReset;

            _revolverCylinder.CylinderLoaded -= OnCylinderLoaded;

            _revolverYoke.Opened -= OnYokeOpened;
            _revolverYoke.Closed -= OnYokeClosed;
        }

        private void OnDryFired()
        {
            if (_dryFireAudioClips != null)
            {
                PlayClip(_dryFireAudioClips.GetNextAudioClip());
            }
        }

        private void OnTriggerReset()
        {
            if (_triggerResetClips != null)
            {
                PlayClip(_triggerResetClips.GetNextAudioClip());
            }
        }

        private void OnCylinderLoaded()
        {
            if (_cylinderLoadClips != null)
            {
                PlayClip(_cylinderLoadClips.GetNextAudioClip());
            }
        }

        private void OnYokeOpened()
        {
            if (_yokeOpenClips != null)
            {
                PlayClip(_yokeOpenClips.GetNextAudioClip());
            }
        }

        private void OnYokeClosed()
        {
            if (_yokeCloseClips != null)
            {
                PlayClip(_yokeCloseClips.GetNextAudioClip());
            }
        }
    }
}
