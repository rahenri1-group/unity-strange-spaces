﻿using Game.Core;
using Game.Core.Interaction;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverHapticBehaviour : MonoBehaviour
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;

        [SerializeField] private FloatReadonlyReference _hapticAmplitude = null;
        [SerializeField] private FloatReadonlyReference _hapticFrequency = null;
        [SerializeField] private FloatReadonlyReference _hapticDuration = null;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsTrue(_hapticAmplitude > 0f);
            Assert.IsTrue(_hapticFrequency > 0f);
            Assert.IsTrue(_hapticDuration > 0f);
        }

        private void OnEnable()
        {
            _revolver.GunShot += OnGunShot;
        }

        private void OnDisable()
        {
            _revolver.GunShot -= OnGunShot;
        }

        private void OnGunShot()
        {
            var haptic = _revolver.CurrentEquipter.GetComponent<IInteractorHaptic>();
            if (haptic != null)
            {
                haptic.HapticPulse(_hapticAmplitude, _hapticFrequency, _hapticDuration);
            }
        }
    }
}
