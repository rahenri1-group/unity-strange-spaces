﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Events;
using Game.Physics;
using Game.Physics.PhysicsMaterial.Event;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverHitScannerBehaviour : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private IPlayerManager _playerManager = null;
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] private ISpacePhysics _spacePhysics = null;

        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;

        [SerializeField] [Layer] private int[] _queryLayers = new int[0];

        [SerializeField] private FloatReadonlyReference _maxRange = null;
        [SerializeField] private IntReadonlyReference _damageAmount = null;
        [SerializeField] private FloatReadonlyReference _hitForce = null;

        private int _queryMask;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_revolver);
            Assert.IsTrue(_maxRange > 0f);
            Assert.IsTrue(_damageAmount > 0f);
            Assert.IsTrue(_hitForce > 0f);

            _queryMask = LayerUtil.ConstructMaskForLayers(_queryLayers);
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            _revolver.GunShot += OnGunShot;
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            _revolver.GunShot -= OnGunShot;
        }

        private void OnGunShot()
        {
            ISpaceData revolverSpace = _spaceManager.GetObjectSpace(gameObject);
            Vector3 shotPosition = transform.position;
            Vector3 shotDirection = transform.forward;

            var portal = CheckForPortal();
            if (portal != null)
            {
                revolverSpace = portal.EndPoint.Space;
                shotPosition = portal.CalculateEndPointPosition(transform.position);
                shotDirection = portal.CalculateEndPointDirection(transform.forward);
            }

            if (_spacePhysics.Raycast(
                revolverSpace,
                shotPosition,
                shotDirection,
                out var hitInfo,
                maxDistance: _maxRange,
                portalInteraction: QueryPortalInteraction.CastThroughTransporter,
                layerMask: _queryMask))
            {
                var hitImpactable = hitInfo.Collider.GetComponentInParent<IImpactable>();
                if (hitImpactable != null)
                {
                    hitImpactable.OnImpact(_hitForce, hitInfo.HitDirection);
                }
                else
                {
                    var hitRigidBody = hitInfo.Collider.GetComponentInParent<Rigidbody>();

                    if (hitRigidBody != null && hitRigidBody.isKinematic == false)
                    {
                        var force = _hitForce * hitInfo.HitDirection;
                        hitRigidBody.AddForce(force, ForceMode.Impulse);
                    }
                }

                var hitPosition = new SpacePosition(hitInfo.Space, hitInfo.HitPosition);

                var physicsMaterial = hitInfo.Collider.GetComponentInParent<IPhysicsMaterial>();
                if (physicsMaterial != null)
                {
                    physicsMaterial.InvokePhysicsEvent(new ShotImpactEvent
                    {
                        ImpactPosition = new SpacePosition(hitPosition.Space, hitPosition.Position),
                        ImpactDirection = hitInfo.HitDirection,
                        ImpactNormal = hitInfo.HitNormal
                    });
                }

                var damageable = hitInfo.Collider.GetComponentInParent<IDamageable>();
                if (damageable != null)
                {
                    damageable.ApplyDamage(_damageAmount, hitInfo.HitPosition, DamageType.Generic);
                }

                _eventBus.InvokeEvent(new PlayerNoiseEvent
                {
                    Player = _playerManager.Player,
                    NoisePosition = hitPosition,
                    NoiseIntensity = PlayerNoiseIntensity.High,
                    Description = "Revolver Shot"
                });
            }
        }

        private IPortal CheckForPortal()
        {
            if (_spaceManager.AreInSameSpace(_playerManager.Player.GameObject, gameObject))
            {
                var revolverSpace = _spaceManager.GetObjectSpace(gameObject);
                if (_spacePhysics.Raycast(
                        revolverSpace,
                        _playerManager.Player.HeadPosition,
                        (transform.position - _playerManager.Player.HeadPosition).normalized,
                        out var hitInfo,
                        Vector3.Distance(_playerManager.Player.HeadPosition, transform.position) * 1.1f,
                        portalInteraction: QueryPortalInteraction.Block,
                        layerMask: 0))
                {
                    return hitInfo.Collider.GetComponentInParent<IPortal>();
                }
            }

            return null;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.forward);
        }
    }
}
