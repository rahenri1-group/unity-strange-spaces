﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverParticlesBehaviour : MonoBehaviour
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private ParticleSystem _particleSystem = null;
        [SerializeField] private IntReadonlyReference _particleCount = null;

        /// <inheritdoc />
        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_particleSystem);
            Assert.IsTrue(_particleCount > 0);
        }

        /// <inheritdoc />
        private void OnEnable()
        {
            _revolver.GunShot += OnGunShot;
        }

        /// <inheritdoc />
        private void OnDisable()
        {
            _revolver.GunShot -= OnGunShot;
        }

        private void OnGunShot()
        {
            _particleSystem.Emit(_particleCount);
        }
    }
}
