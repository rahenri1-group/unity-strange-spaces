﻿using Game.Core;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverRecoilBehaviour : MonoBehaviour
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private FloatReadonlyReference _recoilDuration = null;

        [SerializeField] private Vector3 _recoilPosition = Vector3.zero;
        [SerializeField] private Quaternion _recoilRotation = Quaternion.identity;

        private IEnumerator _recoilCoroutine;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsTrue(_recoilDuration > 0f);

            _recoilCoroutine = null;
        }

        private void OnEnable()
        {
            _revolver.GunShot += OnGunShot;
        }

        private void OnDisable()
        {
            if (_recoilCoroutine != null)
            {
                StopCoroutine(_recoilCoroutine);
                _recoilCoroutine = null;
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            _revolver.GunShot -= OnGunShot;
        }

        private void OnGunShot()
        {
            if (_recoilCoroutine != null)
            {
                StopCoroutine(_recoilCoroutine);
            }
            _recoilCoroutine = Recoil();
            StartCoroutine(_recoilCoroutine);
        }

        private IEnumerator Recoil()
        {
            YieldInstruction wait = null;

            Vector3 startPosition = transform.localPosition;
            Quaternion startRotation = transform.localRotation;

            // move to recoil position
            float startTime = Time.time;
            while (Time.time - startTime < _recoilDuration) 
            {
                float lerp = Mathf.SmoothStep(0f, 1f, (Time.time - startTime) / _recoilDuration);

                transform.localPosition = Vector3.Lerp(startPosition, _recoilPosition, lerp);
                transform.localRotation = Quaternion.Lerp(startRotation, _recoilRotation, lerp);

                yield return wait;
            }

            transform.localPosition = _recoilPosition;
            transform.localRotation = _recoilRotation;

            // move back to start
            startTime = Time.time;
            while (Time.time - startTime < _recoilDuration)
            {
                float lerp = Mathf.SmoothStep(0f, 1f, (Time.time - startTime) / _recoilDuration);

                transform.localPosition = Vector3.Lerp(_recoilPosition, Vector3.zero, lerp);
                transform.localRotation = Quaternion.Lerp(_recoilRotation, Quaternion.identity, lerp);

                yield return wait;
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            _recoilCoroutine = null;
        }

        private void OnDrawGizmosSelected()
        {
            var worldRecoilPosition = transform.TransformPoint(_recoilPosition);
            var worldRecoilRotation = transform.TransformRotation(_recoilRotation);

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(worldRecoilPosition, worldRecoilPosition + 0.1f * (worldRecoilRotation * Vector3.forward));

            Gizmos.color = Color.green;
            Gizmos.DrawLine(worldRecoilPosition, worldRecoilPosition + 0.1f * (worldRecoilRotation * Vector3.up));
        }
    }
}