﻿using Game.Core.Interaction;
using Game.Core.Item;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverReloadBehaviour : BaseInteractable, IWorldInteractable
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private RevolverYokeBehaviour _revolverYoke = null;
        [SerializeField] private RevolverCylinderBehaviour _revolverCylinder = null;

        [SerializeField] private GameObject[] _cartridgeModels = new GameObject[0];

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_revolverYoke);
            Assert.IsNotNull(_revolverCylinder);
        }

        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            if (_revolverYoke.IsOpen && _revolver.CurrentEquipter != null && _revolverCylinder.OpenSlots > 0)
            {
                var inventory = _revolver.CurrentInteractor.InteractorOwner.GetComponent<IPlayer>().Inventory;
                if (inventory.ContainsItem(_revolver.AmmoItemDefinition))
                {
                    return base.AllowedInteractions(interactor);
                }
            }

            return Interactions.None;
        }

        public override void OnHoverBegin(IInteractor hoverer)
        {
            base.OnHoverBegin(hoverer);

            var cartridgesToLoad = Mathf.Min(_revolverCylinder.OpenSlots, _revolver.CurrentInteractor.InteractorOwner.GetComponent<IPlayer>().Inventory.DefinitionQuantity(_revolver.AmmoItemDefinition));

            for (int i = 0; i < _cartridgeModels.Length; i++)
            {
                if (i < _revolverCylinder.FullSlots || cartridgesToLoad <= 0)
                {
                    _cartridgeModels[i].SetActive(false);
                }
                else
                {
                    cartridgesToLoad -= 1;
                    _cartridgeModels[i].SetActive(true);
                }
            }
        }

        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            var inventory = _revolver.CurrentInteractor.InteractorOwner.GetComponent<IPlayer>().Inventory;
            var cartridgesToLoad = Mathf.Min(_revolverCylinder.OpenSlots, inventory.DefinitionQuantity(_revolver.AmmoItemDefinition));

            _revolverCylinder.LoadCartridges(cartridgesToLoad);
            inventory.RemoveItems(_revolver.AmmoItemDefinition, cartridgesToLoad);
        }
    }
}
