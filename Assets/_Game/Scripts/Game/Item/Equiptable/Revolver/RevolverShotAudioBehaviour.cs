﻿using Game.Core;
using Game.Core.Audio;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverShotAudioBehaviour : BaseModifiableAudioSource
    {
        /// <inheritdoc />
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;

        [SerializeField][TypeRestriction(typeof(IAudioClipCollection), false)] private Object _gunShotAudioClipsObj = null;
        private IAudioClipCollection _gunShotAudioClips = null;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_revolver);

            _gunShotAudioClips = _gunShotAudioClipsObj != null ? (IAudioClipCollection)_gunShotAudioClipsObj : null;
        }

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();

            _revolver.GunShot += OnGunShot;
        }

        /// <inheritdoc />
        protected override void OnDisable()
        {
            base.OnDisable();

            _revolver.GunShot -= OnGunShot;
        }

        private void OnGunShot()
        {
            if (_gunShotAudioClips != null)
            {
                PlayClip(_gunShotAudioClips.GetNextAudioClip());
            }
        }
    }
}