﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverTriggerAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;

        [SerializeField] private Transform _triggerTransform = null;
        [SerializeField] private Vector3 _animationAxis = new Vector3();

        [SerializeField] private FloatReadonlyReference _triggerReleasedAngle = null;
        [SerializeField] private FloatReadonlyReference _triggerPressedAngle = null;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsNotNull(_triggerTransform);
            Assert.IsTrue(_animationAxis.magnitude == 1f);
        }

        private void OnEnable()
        {
            _revolver.TriggerMoved += OnTriggerMoved;
        }

        private void OnDisable()
        {
            _revolver.TriggerMoved -= OnTriggerMoved;
        }

        private void OnTriggerMoved()
        {
            float triggerAngle = Mathf.LerpAngle(_triggerReleasedAngle, _triggerPressedAngle, _revolver.TriggerPosition);
            _triggerTransform.localEulerAngles = triggerAngle * _animationAxis;
        }
    }
}
