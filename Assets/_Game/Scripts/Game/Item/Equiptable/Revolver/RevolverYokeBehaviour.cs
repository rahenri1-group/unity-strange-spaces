﻿using Game.Core;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Item.Revolver
{
    public class RevolverYokeBehaviour : MonoBehaviour
    {
        public bool IsOpen 
        {
            get => _isOpen; 
            private set
            {
                if (_isOpen && !value)
                {
                    Closed?.Invoke();
                }
                else if (!_isOpen && value)
                {
                    Opened?.Invoke();
                }

                _isOpen = value;
            }
        }
        private bool _isOpen = false;

        public event Action Opened;
        public event Action Closed;

        [SerializeField] private RevolverEquiptableBehaviour _revolver = null;
        [SerializeField] private FloatReadonlyReference _openDuration = null;
        [SerializeField] private FloatReadonlyReference _closeDuration = null;
        [SerializeField] private FloatReadonlyReference _closeVelocityChangeThreshold = null;

        [SerializeField] private Quaternion _closeAngle = Quaternion.identity;
        [SerializeField] private Quaternion _openAngle = Quaternion.identity;

        private IEnumerator _updateCoroutine;

        private void Awake()
        {
            Assert.IsNotNull(_revolver);
            Assert.IsTrue(_openDuration > 0f);
            Assert.IsTrue(_closeDuration > 0f);
            Assert.IsTrue(_closeVelocityChangeThreshold > 0f);
            
            _updateCoroutine = null;
        }

        private void OnEnable()
        {
            _updateCoroutine = null;

            if (IsOpen)
            {
                transform.localRotation = _openAngle;

                _updateCoroutine = CheckForClose();
                StartCoroutine(_updateCoroutine);
            }
            else
            {
                transform.localRotation = _closeAngle;
            }
        }

        private void OnDisable()
        {
            if (_updateCoroutine != null)
            {
                StopCoroutine(_updateCoroutine);
                _updateCoroutine = null;
            }
        }

        public void Open()
        {
            if (IsOpen) return;

            IsOpen = true;

            if (_updateCoroutine != null)
            {
                StopCoroutine(_updateCoroutine);
            }

            _updateCoroutine = AnimateYoke(_openAngle, _openDuration, () =>
            {
                _updateCoroutine = CheckForClose();
                StartCoroutine(_updateCoroutine);
            });
            StartCoroutine(_updateCoroutine);
        }

        private IEnumerator CheckForClose()
        {
            YieldInstruction wait = new WaitForFixedUpdate();

            Vector3 lastPostion = _revolver.CurrentInteractor.InteractorOwner.transform.InverseTransformPoint(transform.position);
            float lastVelocity = 0f;

            while (true)
            {
                yield return wait;

                var newPosition = _revolver.CurrentInteractor.InteractorOwner.transform.InverseTransformPoint(transform.position);
                float velocity = Vector3.Distance(newPosition, lastPostion) / Time.fixedDeltaTime;

                if (lastVelocity - velocity >= _closeVelocityChangeThreshold)
                {
                    break;
                }

                lastVelocity = velocity;
                lastPostion = newPosition;
            }

            StopCoroutine(_updateCoroutine);
            _updateCoroutine = AnimateYoke(_closeAngle, _closeDuration, () =>
            {
                IsOpen = false;
            });
            StartCoroutine(_updateCoroutine);
        }

        private IEnumerator AnimateYoke(Quaternion targetRotation, float duration, Action onComplete = null)
        {
            YieldInstruction wait = null;

            var startRotation = transform.localRotation;
            float startTime = Time.time;

            while (Time.time - startTime < duration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / duration);
                transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, lerp); 
            }
            transform.localRotation = targetRotation;

            _updateCoroutine = null;

            onComplete?.Invoke();
        }
    }
}