﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Core.Space;

namespace Game.Item
{
    public class EquiptableItemDestructorBehaviour : InjectedBehaviour, IEquiptableItemReleasor
    {
        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] private IInteractionManager _interactionManager = null;
        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        public void OnPlayerAttemptReleaseItem(IEquiptableItem item)
        {
            item.PreDestroyItem();

            if (!string.IsNullOrEmpty(item.ItemDefinition.StashEffectAddress))
            {
                _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                    item.ItemDefinition.StashEffectAddress,
                    _spaceManager.GetObjectSpace(gameObject),
                    transform.position)
                    .Forget();
            }

            _interactionManager.InteractEnd(item.CurrentInteractor, item);
            _gameObjectResourceManager.DestroyObject(item.GameObject);
        }
    }
}
