﻿using Game.Core;
using Game.Core.Interaction;
using Game.Input;
using System;

namespace Game.Item
{
    /// <summary>
    /// An <see cref="IInteractor"/> that can hold and use items
    /// </summary>
    public interface IItemEquiptor : IInteractor, IGameObjectComponent
    {
        /// <summary>
        /// The input for item use
        /// </summary>
        IItemInput ItemInput { get; }

        /// <summary>
        /// The current item the equiptor has
        /// </summary>
        IEquiptableItem EquiptedItem { get; }

        /// <summary>
        /// Event raised when the equiptor equipts or unequipts an item
        /// </summary>
        event Action EquiptedItemChanged;
    }
}
