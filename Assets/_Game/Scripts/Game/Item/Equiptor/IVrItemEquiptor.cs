﻿using Game.Input;

namespace Game.Item
{
    /// <summary>
    /// <see cref="IVrItemEquiptor"/> for a VR player's hand
    /// </summary>
    public interface IVrItemEquiptor : IItemEquiptor
    {
        /// <summary>
        /// Is the equiptor for the vr players left hand
        /// </summary>
        bool IsLeftHand { get; }
    }
}
