﻿namespace Game.Item
{
    /// <summary>
    /// A script that acts upon a <see cref="IEquiptableItem"/> when the player attempts to release it
    /// </summary>
    public interface IEquiptableItemReleasor 
    {
        /// <summary>
        /// Called when a player attempts to release a <paramref name="item"/>
        /// </summary>
        void OnPlayerAttemptReleaseItem(IEquiptableItem item);
    }
}
