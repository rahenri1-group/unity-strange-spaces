﻿namespace Game.Item
{
    /// <summary>
    /// Interface for a component that can hold one or many key items
    /// </summary>
    public interface IKeyItemDefinitionProvider
    {
        bool ContainsKeyItem(IKeyItemDefinition keyItemDefinition);
    }
}
