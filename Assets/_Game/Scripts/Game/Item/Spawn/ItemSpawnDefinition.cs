﻿using Game.Core;
using Game.Core.Item;
using System;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Contains the spawning information of an <see cref="IWorldItemDefinition"/> in a <see cref="ItemSpawnGroup"/>
    /// </summary>
    [Serializable]
    public class ItemSpawnDefinition
    {
        /// <summary>
        /// The <see cref="IWorldItemDefinition"/> that is added to the world on world creation
        /// </summary>
        public IWorldItemDefinition ItemDefinition => (IWorldItemDefinition)_itemDefinition;

        /// <summary>
        /// The minimum amount of the item to spawn on world creation
        /// </summary>
        public int MinSpawnCount => _minSpawnCount;
        /// <summary>
        /// The maximum amount of the item to spawn on world creation
        /// </summary>
        public int MaxSpawnCount => _maxSpawnCount;

        [SerializeField] private BaseItemDefinition _itemDefinition = null;
        [SerializeField] private IntReadonlyReference _minSpawnCount = null;
        [SerializeField] private IntReadonlyReference _maxSpawnCount = null;
    }
}
