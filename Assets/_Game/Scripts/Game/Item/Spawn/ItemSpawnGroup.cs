﻿using Game.Core;
using Game.Core.Item;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// A scriptable object that hold a collection of <see cref="IItemDefinition"/>s for spawning into the world
    /// </summary>
    [CreateAssetMenu(menuName = "Game/Item/Item Spawn Group", order = -100)]
    public class ItemSpawnGroup : BaseGuidScriptableObject
    {
        /// <summary>
        /// The name of the spawn group
        /// </summary>
        public string Name => name;

        /// <summary>
        /// A collection of <see cref="IItemDefinition"/>s that will be spawned in
        /// </summary>
        public ItemSpawnDefinition[] ItemSpawnDefinitions => _itemSpawnDefinitions;

        [SerializeField] private ItemSpawnDefinition[] _itemSpawnDefinitions = new ItemSpawnDefinition[0];
    }
}
