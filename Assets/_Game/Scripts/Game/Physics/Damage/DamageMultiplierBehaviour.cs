﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Physics
{
    /// <summary>
    /// Multiplies received damage and applies it to another <see cref="IDamageable"/>
    /// </summary>
    public class DamageMultiplierBehaviour : MonoBehaviour, IDamageable
    {
        /// <inheritdoc/>
        public GameObject GameObject => gameObject;

        [SerializeField] [TypeRestriction(typeof(IDamageable))] private Component _damageableObj = null;
        private IDamageable _damageable;

        [SerializeField] private FloatReadonlyReference _damageMultiplier = null;

        /// <inheritdoc/>
        private void Awake()
        {
            Assert.IsNotNull(_damageableObj);
            _damageable = _damageableObj.GetComponentAsserted<IDamageable>();
        }

        /// <inheritdoc/>
        public void ApplyDamage(int damage, Vector3 damagePosition, DamageType damageType)
        {
            _damageable.ApplyDamage((int)(_damageMultiplier * damage), damagePosition, damageType);
        }
    }
}