﻿using Game.Core;
using UnityEngine;

namespace Game.Physics
{
    /// <summary>
    /// The type of damage
    /// </summary>
    public enum DamageType
    {
        /// <summary>
        /// General unspecified damage
        /// </summary>
        Generic = 0,

        /// <summary>
        /// Hidden damgage not from a direct source. Should not trigger effects
        /// </summary>
        Silent = 1
    }

    /// <summary>
    /// A component that can receive damage
    /// </summary>
    public interface IDamageable : IGameObjectComponent
    {
        /// <summary>
        /// Applies <paramref name="damage"/> to the gameobject
        /// </summary>
        void ApplyDamage(int damage, Vector3 damagePosition, DamageType damageType);
    }
}