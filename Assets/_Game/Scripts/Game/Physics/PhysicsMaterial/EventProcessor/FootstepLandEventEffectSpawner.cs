﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Physics;
using Game.Core.Resource;
using Game.Physics.PhysicsMaterial.Event;
using UnityEngine;

namespace Game.Physics.PhysicsMaterial.EventProcessor
{
    [CreateAssetMenu(menuName = "Game/Physics/Footstep Land Event Effect Spawner")]
    public class FootstepLandEventEffectSpawner : BasePhysicsMaterialEventProcessorObject<FootstepLandEvent>
    {
        public string EffectAddress => _effectAddress;

        [SerializeField] private StringReadonlyReference _effectAddress = null;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;

        public override void OnPhysicsEvent(IPhysicsMaterial physicsMaterial, FootstepLandEvent eventData)
        {
            InitializeIfNecessary();

            _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                _effectAddress, 
                eventData.FootstepPosition.Space, 
                eventData.FootstepPosition.Position)
                .Forget();
        }
    }
}
