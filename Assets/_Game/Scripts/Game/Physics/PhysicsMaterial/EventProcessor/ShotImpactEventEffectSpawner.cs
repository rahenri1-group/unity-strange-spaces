﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Physics;
using Game.Core.Resource;
using Game.Physics.PhysicsMaterial.Event;
using UnityEngine;

namespace Game.Physics.PhysicsMaterial.EventProcessor
{
    [CreateAssetMenu(menuName = "Game/Physics/Shot Impact Event Effect Spawner")]
    public class ShotImpactEventEffectSpawner : BasePhysicsMaterialEventProcessorObject<ShotImpactEvent>
    {
        public string EffectAddress => _effectAddress;

        [SerializeField] private StringReadonlyReference _effectAddress = null;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;

        public override void OnPhysicsEvent(IPhysicsMaterial physicsMaterial, ShotImpactEvent eventData)
        {
            InitializeIfNecessary();

            _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                _effectAddress,
                eventData.ImpactPosition.Space,
                eventData.ImpactPosition.Position,
                Quaternion.LookRotation(Vector3.Reflect(eventData.ImpactDirection, eventData.ImpactNormal), Vector3.up))
                .Forget();
        }
    }
}
