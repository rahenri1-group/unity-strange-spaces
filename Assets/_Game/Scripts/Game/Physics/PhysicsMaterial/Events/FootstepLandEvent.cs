﻿using Game.Core.Space;

namespace Game.Physics.PhysicsMaterial.Event
{
    public struct FootstepLandEvent
    {
        public SpacePosition FootstepPosition;
    }
}
