﻿using Game.Core.Space;

namespace Game.Physics.PhysicsMaterial.Event
{
    public struct FootstepWalkEvent
    {
        public SpacePosition FootstepPosition;
    }
}
