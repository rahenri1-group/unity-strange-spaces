﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Physics.PhysicsMaterial.Event
{
    public struct ShotImpactEvent
    {
        public SpacePosition ImpactPosition;
        public Vector3 ImpactDirection;
        public Vector3 ImpactNormal;
    }
}
