﻿using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Entity;
using System;

namespace Game.Physics
{
    public class CharacterEntityTriggerVolume : BaseTriggerVolume<ICharacterEntity, Guid> 
    {
        [Inject] private IEventBus _eventBus = null;

        /// <inheritdoc/>
        private void OnEnable()
        {
            _eventBus.Subscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        /// <inheritdoc/>
        private void OnDisable()
        {
            _eventBus.Unsubscribe<EntityDestroyEvent>(OnEntityDestroy);
        }

        /// <inheritdoc/>
        protected override Guid GetKeyForComponent(ICharacterEntity component)
        {
            return component.EntityId;
        }

        private void OnEntityDestroy(EntityDestroyEvent eventData)
        {
            PurgeComponent(eventData.EntityId);
        }
    }
}
