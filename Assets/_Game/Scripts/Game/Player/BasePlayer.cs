﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Item;
using Game.Core.Space;
using Game.Entity;
using Game.Events;
using Game.Physics;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player
{
    public abstract class BasePlayer : BaseDynamicEntity<EntitySerializedData>, IPlayer
    {
        /// <inheritdoc/>
        public int HealthMax => _playerHealthMax;
        /// <inheritdoc/>
        public int HealthCurrent => _playerHealth.Value;

        public IInventory Inventory { get; private set; }

        public Vector3 HeadPosition => HeadTransform.position;
        public Quaternion HeadRotation => HeadTransform.rotation;

        public Vector3 FeetPosition
        {
            get
            {
                Vector3 playerFeetPosition = HeadTransform.position;
                playerFeetPosition.y = transform.position.y;
                return playerFeetPosition;
            }
        }

        /// <inheritdoc/>
        public override bool DestroyIfOutOfBounds => false;

        public event PlayerEvent PlayerHeadMoved;
        public event EntityDamagedEvent EntityDamaged;

        [Inject] protected ILogRouter Logger;
        [Inject] protected ISpaceManager SpaceManager;

        protected float WalkSpeed => _walkSpeed;

        [SerializeField] private IntReadonlyReference _playerHealthMax = null;
        [SerializeField] private IntVariable _playerHealth = null;

        [SerializeField] private FloatReadonlyReference _walkSpeed = null;
        [SerializeField] private FloatReadonlyReference _lowestAllowedHeight = null;

        protected abstract Transform HeadTransform { get; }

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(HealthMax > 0);
            Assert.IsTrue(_walkSpeed > 0f);
            Assert.IsTrue(_lowestAllowedHeight < 0f);

            Inventory = new Inventory();
        }

        protected virtual void OnEnable()
        {
            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        protected virtual void OnDisable()
        {
            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        protected virtual void Update()
        {
            InvokePlayerHeadMovedIfNecessary();

            if (this.IsAlive() && transform.position.y <= _lowestAllowedHeight)
            {
                SetPlayerHealth(0);
            }
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (!System.Object.ReferenceEquals(eventArgs.Entity, this)) return;

            SpaceManager.SetActiveSpace(eventArgs.NewSpace);

            InvokePlayerHeadMovedIfNecessary();
        }

        public void SetPlayerHealth(int health)
        {
            int newHealth = Mathf.Clamp(health, 0, _playerHealthMax);
            int oldHealth = _playerHealth;

            _playerHealth.Value = newHealth;

            if (newHealth < oldHealth)
            {
                EntityDamaged?.Invoke(this, new EntityDamagedEventArgs
                {
                    DamageAmount = newHealth - oldHealth,
                    DamagePosition = HeadPosition,
                    DamageType = DamageType.Silent
                });

                if (!this.IsAlive())
                {
                    EventBus.InvokeEvent(new EntityKilledEvent
                    {
                        Entity = this
                    });
                }
            }
        }

        /// <inheritdoc/>
        public void ApplyDamage(int damage, Vector3 damagePosition, DamageType damageType)
        {
            if (damage <= 0)
            {
                Logger.LogWarning($"Invalid player damage amount '{damage}'");
                return;
            }

            int newHealth = Mathf.Clamp(_playerHealth.Value - damage, 0, _playerHealthMax);
            int oldHealth = _playerHealth;

            if (newHealth != oldHealth)
            {  
                _playerHealth.Value = newHealth;

                EntityDamaged?.Invoke(this, new EntityDamagedEventArgs
                {
                    DamageAmount = newHealth - oldHealth,
                    DamagePosition = damagePosition,
                    DamageType = damageType
                });
            }
        }

        public void ApplyHealingToPlayer(int healAmount)
        {
            if (healAmount <= 0)
            {
                Logger.LogWarning($"Invalid player heal amount '{healAmount}'");
                return;
            }

            int newHealth = Mathf.Clamp(_playerHealth.Value + healAmount, 0, _playerHealthMax);
            int oldHealth = _playerHealth;

            if (newHealth != oldHealth)
            {
                _playerHealth.Value = newHealth;
            }
        }

        protected void InvokePlayerHeadMovedIfNecessary()
        {
            if (HeadTransform.hasChanged)
            {
                PlayerHeadMoved?.Invoke(this);

                HeadTransform.hasChanged = false;
            }
        }
    }
}
