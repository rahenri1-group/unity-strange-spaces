﻿using Game.Core;
using Game.Core.Interaction;
using Game.Entity;

namespace Game.Player
{
    public abstract class BasePlayerInteractor : BaseEntityInteractorBehaviour
    {
        /// <inheritdoc />
        public override bool IsInteractionEnabled => Player.IsAlive() && base.IsInteractionEnabled;

        protected IPlayer Player { get; private set; }

        protected override void Awake()
        {
            base.Awake();

            Player = this.GetComponentInParentAsserted<IPlayer>();
        }

        protected virtual void OnEnable()
        {
            Player.EntityDamaged += OnPlayerDamaged;
        }

        protected virtual void OnDisable()
        {
            Player.EntityDamaged -= OnPlayerDamaged;
        }

        private void OnPlayerDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            if (!Player.IsAlive())
            {
                OnPlayerDeath();
            }
        }

        protected virtual void OnPlayerDeath()
        {
            InteractionManager.OnInteractorDisable(this);
        }
    }
}
