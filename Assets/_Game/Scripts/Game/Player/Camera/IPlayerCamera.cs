﻿using Game.Core.Render.Camera;

namespace Game.Player
{
    public interface IPlayerCamera : ICamera
    {
        IPlayer TrackedPlayer { get; set; }
    }
}
