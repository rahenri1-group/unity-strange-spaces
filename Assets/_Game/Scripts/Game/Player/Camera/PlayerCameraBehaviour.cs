﻿using Game.Core.Render.Camera;
using Game.Core.Space;

namespace Game.Player
{
    public class PlayerCameraBehaviour : BaseSpaceCamera, IPlayerCamera
    {
        public override CameraType CameraType => CameraType.PrimaryCamera;

        public IPlayer TrackedPlayer
        {
            get => _trackedPlayer;
            set
            {
                if (_trackedPlayer != null)
                {
                    _trackedPlayer.PlayerHeadMoved -= OnPlayerHeadMoved;
                }

                _trackedPlayer = value;
                if (_trackedPlayer != null)
                {
                    _trackedPlayer.PlayerHeadMoved += OnPlayerHeadMoved;
                    OnPlayerHeadMoved(_trackedPlayer);
                }

            }
        }
        private IPlayer _trackedPlayer = null;

        private void OnPlayerHeadMoved(IPlayer sender)
        {
            transform.position = _trackedPlayer.HeadPosition;
            transform.rotation = _trackedPlayer.HeadRotation;

            var currentViewTransformSpace = SpaceManager.GetEntitySpace(_trackedPlayer);
            if (!SpaceUtil.SpaceEquals(RenderedSpace, currentViewTransformSpace))
            {
                RenderedSpace = currentViewTransformSpace;
            }
        }
    }
}
