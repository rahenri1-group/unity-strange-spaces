﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Render.Camera;
using Game.Core.Resource;
using Game.Core.Space;

namespace Game.Player
{
    [Dependency(
        contract: typeof(ICameraFactory),
        lifetime: Lifetime.Singleton)]
    public class PlayerCameraFactory : BaseCameraFactory<IPlayerCamera>
    {
        public override CameraFactoryConfig FactoryConfig => Config;
        public CameraFactoryConfig Config = new CameraFactoryConfig();

        public PlayerCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ISpaceManager spaceManager)
            : base(gameObjectResourceManager, spaceManager) { }

        /// <inheritdoc/>
        public async override UniTask<ICamera> CreateCamera()
        {
            var camera = await CreateEmptyCamera();
            return camera.gameObject.AddComponent<PlayerCameraBehaviour>();
        }
    }
}
