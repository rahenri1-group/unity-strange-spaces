﻿using Game.Core.Space;
using UnityEngine;

namespace Game.Player
{
    public interface IPlayerSpawnData : ISpaceDataComponent
    {
        Vector3 Position { get; }
        Quaternion Rotation { get; }
    }
}
