﻿using System;
using UnityEngine;

namespace Game.Player
{
    [Serializable]
    public class PlayerSpawnDataComponent : IPlayerSpawnData
    {
        public Vector3 Position => _position;
        public Quaternion Rotation => _rotation;

        [SerializeField] private Vector3 _position = Vector3.zero;
        [SerializeField] private Quaternion _rotation = Quaternion.identity;
    }
}
