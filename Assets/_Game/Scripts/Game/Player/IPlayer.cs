﻿using Game.Core.Entity;
using Game.Core.Item;
using Game.Entity;
using UnityEngine;

namespace Game.Player
{
    public delegate void PlayerEvent(IPlayer sender);

    public interface IPlayer : ICharacterEntity, IMortalEntity, IDynamicEntity
    {
        IInventory Inventory { get; }

        Vector3 HeadPosition { get; }
        Quaternion HeadRotation { get; }
        Vector3 FeetPosition { get; }

        event PlayerEvent PlayerHeadMoved;

        void SetPlayerHealth(int health);
        void ApplyHealingToPlayer(int healAmount);
    }
}
