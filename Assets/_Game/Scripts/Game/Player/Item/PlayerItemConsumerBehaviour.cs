﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Physics;
using Game.Core.Resource;
using Game.Item;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.Item
{
    public class PlayerItemConsumerBehaviour : InjectedBehaviour
    {
        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] private IInteractionManager _interactionManager = null;

        [SerializeField] [TypeRestriction(typeof(IPlayer))] private Component _playerObj = null;
        private IPlayer _player;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _itemTriggerableObj = null;
        private ITriggerable _itemTriggerable;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_playerObj);
            Assert.IsNotNull(_itemTriggerableObj);

            _player = _playerObj.GetComponentAsserted<IPlayer>();
            _itemTriggerable = _itemTriggerableObj.GetComponentAsserted<ITriggerable>();
        }

        private void OnEnable()
        {
            _itemTriggerable.TriggerEnter += OnItemTriggerEnter;
        }

        private void OnDisable()
        {
            _itemTriggerable.TriggerEnter -= OnItemTriggerEnter;
        }

        private void OnItemTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            var item = args.Collider.GetComponentInParent<IConsumableEquiptableItem>();

            if (item == null || item.CurrentInteractor == null) // current interactor could be null if we've already consumed this
            {
                return;
            }

            var itemDefinition = item.PlayerConsumableItemDefinition;

            _interactionManager.InteractEnd(item.CurrentInteractor, item);
            _gameObjectResourceManager.DestroyObject(item.GameObject);

            itemDefinition.Consume(_player);
        }
    }
}
