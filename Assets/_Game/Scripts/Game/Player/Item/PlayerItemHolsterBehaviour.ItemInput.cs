﻿using Game.Input;
using System;

namespace Game.Player.Item
{
    public partial class PlayerItemHolsterBehaviour
    {
        private class HolsterItemInput : IItemInput
        {
            public bool ItemUseActionValue => false;
            public bool ItemAlternateActionValue => false;
            public float ItemShootActionValue => 0f;

            // Temporarily disable unused event warning
#pragma warning disable 67
            public event Action<InputContext, bool> UseItemActionTriggered;
            public event Action<InputContext, bool> ItemAlternateActionTriggered;
            public event Action<InputContext> ReleaseItemActionTriggered;
#pragma warning restore 67
        }
    }
}
