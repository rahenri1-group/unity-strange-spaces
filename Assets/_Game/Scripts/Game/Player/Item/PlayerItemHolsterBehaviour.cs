﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Event;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Entity;
using Game.Input;
using Game.Item;
using Game.Item.Inventory;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.Item
{
    public partial class PlayerItemHolsterBehaviour : InjectedBehaviour, IItemEquiptor, IEquiptableItemReleasor
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public Rigidbody Body => _body;

        /// <inheritdoc />
        public IEquiptableItem EquiptedItem { get; private set; }

        /// <inheritdoc />
        public IItemInput ItemInput { get; private set; }

        /// <inheritdoc />
        public GameObject InteractorOwner => _player.GameObject;

        /// <inheritdoc />
        public Vector3 Position => transform.position;
        /// <inheritdoc />
        public Quaternion Rotation => transform.rotation;

        /// <inheritdoc />
        public IInteractable InteractionTarget => EquiptedItem;

        /// <inheritdoc />
        public bool IsInteracting => EquiptedItem != null;

        /// <inheritdoc />
        public bool IsInteractionEnabled => _player.IsAlive();

        /// <inheritdoc />
        public event Action EquiptedItemChanged;

        [Inject] private IEventBus _eventBus = null;
        [Inject] private IInteractionManager _interactionManager = null;
        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        [SerializeField] private Rigidbody _body = null;
        [SerializeField] private Transform _itemHolder = null;

        private IPlayer _player;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_body);
            Assert.IsNotNull(_itemHolder);

            _player = this.GetComponentInParentAsserted<IPlayer>();

            ItemInput = new HolsterItemInput();

            EquiptedItem = null;
        }

        /// <inheritdoc />
        public bool CanInteractWith(IInteractable interactble)
        {
            // Inventory can not be put in holster
            if (interactble is InventoryEquiptableBehaviour)
            {
                return false;
            }

            return IsInteractionEnabled && interactble is IEquiptableItem;
        }

        /// <inheritdoc />
        public void OnInteractBegin(IInteractable interactable)
        {
            var item = interactable as IEquiptableItem;
            if (item != null)
            {
                EquiptedItem = item;

                var itemTransfrom = EquiptedItem.GameObject.transform;
                itemTransfrom.SetParent(_itemHolder);
                itemTransfrom.localPosition = Vector3.zero;
                itemTransfrom.localRotation = Quaternion.identity;
                itemTransfrom.localScale = Vector3.one;

                EquiptedItemChanged?.Invoke();

                _eventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = _player
                });
            }
        }

        /// <inheritdoc />
        public void OnInteractEnd()
        {
            if (EquiptedItem != null)
            {
                EquiptedItem.GameObject.transform.SetParent(null);
                EquiptedItem = null;

                EquiptedItemChanged?.Invoke();

                _eventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = _player
                });

                EquiptedItem = null;
            }
        }

        /// <inheritdoc />
        public void OnPlayerAttemptReleaseItem(IEquiptableItem item)
        {
            if (CanInteractWith(item))
            {
                _interactionManager.InteractEnd(item.CurrentInteractor, item);

                if (!string.IsNullOrEmpty(item.ItemDefinition.StashEffectAddress))
                {
                    _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                        item.ItemDefinition.StashEffectAddress,
                        _spaceManager.GetEntitySpace(_player),
                        _itemHolder.transform.position)
                        .Forget();
                }

                _interactionManager.AttemptInteractBegin(this, item);
            }
        }
    }
}