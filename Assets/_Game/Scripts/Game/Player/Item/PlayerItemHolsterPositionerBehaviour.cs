﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Storage.Settings;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.Item
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerItemHolsterPositionerBehaviour : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISettingsManager _settingsManager = null;

        [SerializeField] private GameObject _leftHolster = null;
        [SerializeField] private GameObject _rightHolster = null;

        [SerializeField] private FloatReadonlyReference _verticalOffset;
        [SerializeField] private FloatReadonlyReference _forwardOffset;
        [SerializeField] private FloatReadonlyReference _holsterSeparationOffset;

        private Rigidbody _body;

        private IPlayer _player;

        private IPreferencesVrSettingsGroup _inputSettingsGroup;

        private Vector3 _holsterOffset;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_leftHolster);
            Assert.IsNotNull(_rightHolster);

            _body = GetComponent<Rigidbody>();

            _player = this.GetComponentInParentAsserted<IPlayer>();

            _holsterOffset = new Vector3();
        }

        private void OnEnable()
        {
            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IPreferencesVrSettingsGroup>();

            CalculateOffsetVector();
            PositionHolsters();

            _inputSettingsGroup.SettingsGroupUpdated += OnSettingsGroupUpdated;

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnDisable()
        {
            _inputSettingsGroup.SettingsGroupUpdated -= OnSettingsGroupUpdated;

            _inputSettingsGroup = null;

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnSettingsGroupUpdated(ISettingsGroup group)
        {
            CalculateOffsetVector();
            PositionHolsters();
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventArgs)
        {
            if (_inputSettingsGroup != null)
            {
                _inputSettingsGroup.SettingsGroupUpdated -= OnSettingsGroupUpdated;
            }

            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IPreferencesVrSettingsGroup>();
            _inputSettingsGroup.SettingsGroupUpdated += OnSettingsGroupUpdated;

            CalculateOffsetVector();
            PositionHolsters();
        }

        private void LateUpdate()
        {
            var rotation = _player.HeadRotation.GetYAxisRotation().normalized;
            _body.rotation = rotation;
            _body.position = _player.HeadPosition + rotation * _holsterOffset;
        }

        private void CalculateOffsetVector()
        {
            _holsterOffset.x = 0f;
            _holsterOffset.y = _verticalOffset + 0.01f * _inputSettingsGroup.HolsterVerticalOffset;
            _holsterOffset.z = _forwardOffset + 0.01f * _inputSettingsGroup.HolsterForwardOffset;
        }

        private void PositionHolsters()
        {
            float separation = _holsterSeparationOffset + 0.01f * _inputSettingsGroup.HolsterSeparationOffset;
            _leftHolster.transform.localPosition = new Vector3(-separation, 0f, 0f);
            _rightHolster.transform.localPosition = new Vector3(separation, 0f, 0f);
        }
    }
}