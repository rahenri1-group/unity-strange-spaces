﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Event;
using Game.Core.Render.Camera;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Entity;
using Game.World;
using System;
using UnityEngine;

namespace Game.Player
{
    public abstract class BasePlayerManager : BaseModule, IPlayerManager
    {
        public abstract BasePlayerManagerConfig BaseConfig { get; }

        protected readonly ICameraManager CameraManager;
        protected readonly IEventBus EventBus;
        protected readonly IGameObjectResourceManager GameObjectResourceManager;
        protected readonly ISpaceLoader SpaceLoader;
        protected readonly ISpaceManager SpaceManager;
        protected readonly IWorldManager WorldManager;

        public IPlayer Player { get; private set; }
        private GameObject _playerGameObject;
        private bool _playerSpawned;

        public BasePlayerManager(
            ICameraManager cameraManager,
            IEventBus eventBus,
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager)
            : base(logger)
        {
            CameraManager = cameraManager;
            EventBus = eventBus;
            GameObjectResourceManager = gameObjectResourceManager;
            SpaceLoader = spaceLoader;
            SpaceManager = spaceManager;
            WorldManager = worldManager;

            Player = null;
            _playerGameObject = null;
            _playerSpawned = false;
        }

        public async override UniTask Initialize()
        {
            EventBus.Subscribe<SpaceLoadedEvent>(OnSpaceLoad);

            await base.Initialize();
        }

        public override UniTask Shutdown()
        {
            EventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoad);

            if (Player != null)
            {
                Player.EntityDamaged -= OnPlayerDamagedChanged;

                GameObjectResourceManager.DestroyObject(_playerGameObject);
                _playerGameObject = null;
                Player = null;
                _playerSpawned = false;
            }

            return base.Shutdown();
        }

        private void OnSpaceLoad(SpaceLoadedEvent eventData)
        {
            if (Player != null || _playerSpawned) return;

            var playerSpawnData = eventData.SpaceData.GetAdditionalData<IPlayerSpawnData>();

            if (playerSpawnData != null)
            {
                EventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoad);

                _playerSpawned = true;

                UniTask.Create(async () =>
                {
                    var playerData = await CreatePlayer(eventData.SpaceData, playerSpawnData.Position, playerSpawnData.Rotation);
                    if (playerData.Player != null)
                    {
                        _playerGameObject = playerData.PlayerGameObject;
                        Player = playerData.Player;

                        Player.SetPlayerHealth(Player.HealthMax);

                        var camera = await CameraManager.CreateCamera<IPlayerCamera>();
                        camera.TrackedPlayer = Player;

                        var cameraFade = camera.GetCameraEffect<ICameraFade>();
                        if (cameraFade != null && BaseConfig.PlayerSpawnFadeInDuration > 0f)
                        {
                            cameraFade.SetBlack();
                            cameraFade.FadeIn(BaseConfig.PlayerSpawnFadeInDuration);
                        }

                        Player.EntityDamaged += OnPlayerDamagedChanged;
                    }
                }).Forget();
            }
        }

        private void OnPlayerDamagedChanged(IMortalEntity player, EntityDamagedEventArgs args)
        {
            if (Player.IsAlive()) return;

            UniTask.Create(async () =>
            {
                await MovePlayerToHome(
                    async () =>
                    {
                        // reset player health
                        Player.SetPlayerHealth(Player.HealthMax);
                        Player.Inventory.RemoveAllItems();

                        if (WorldManager.IsWorldLoaded)
                        {
                            // unload the world
                            await WorldManager.DestroyCurrentWorld();
                        }
                    });
            }).Forget();
        }

        public async UniTask MovePlayerToHome(Func<UniTask> postFadeOutWork)
        {
            // make sure home space is loaded
            var homeSpace = SpaceManager.SpaceDataFromId(Guid.Parse(BaseConfig.HomeSpaceId));
            if (!SpaceLoader.IsSpaceLoaded(homeSpace))
            {
                await SpaceLoader.LoadSpace(homeSpace);
            }

            var fadeEffect = CameraManager.ActivePrimaryCamera?.GetCameraEffect<ICameraFade>();
            if (fadeEffect != null)
            {
                await fadeEffect.FadeOutAsync(BaseConfig.PlayerDeathFadeOutDuration);
            }

            // move player to home space
            var oldPlayerSpace = SpaceManager.GetEntitySpace(Player);

            await SpaceManager.MoveObjectToSpaceAsync(_playerGameObject, homeSpace);

            var spawnData = homeSpace.GetAdditionalData<IPlayerSpawnData>();
            Player.EntityMovement.Teleport(
                spawnData?.Position ?? Vector3.zero,
                spawnData?.Rotation ?? Quaternion.identity);

            EventBus.InvokeEvent(new EntityPostTeleportEvent
            {
                Entity = Player,
                OldSpace = oldPlayerSpace,
                NewSpace = homeSpace,
                Portal = null
            });

            if (postFadeOutWork != null)
            {
                await postFadeOutWork();
            }

            if (fadeEffect != null)
            {
                await fadeEffect.FadeInAsync(BaseConfig.PlayerSpawnFadeInDuration);
            }
        }

        protected abstract UniTask<(GameObject PlayerGameObject, IPlayer Player)> CreatePlayer(ISpaceData space, Vector3 position, Quaternion rotation);
    }
}
