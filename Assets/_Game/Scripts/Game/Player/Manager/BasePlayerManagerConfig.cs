﻿using Game.Core;

namespace Game.Player
{
    public class BasePlayerManagerConfig : ModuleConfig
    {
        public string PlayerAssetKey = string.Empty;

        public string HomeSpaceId = string.Empty;

        public float PlayerSpawnFadeInDuration = 3f;

        public float PlayerDeathFadeOutDuration = 3f;
    }
}
