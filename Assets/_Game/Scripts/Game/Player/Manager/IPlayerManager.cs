﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Render.Camera;
using System;

namespace Game.Player
{
    /// <summary>
    /// Manager for getting the current <see cref="IPlayer"/>
    /// </summary>
    public interface IPlayerManager : IModule
    {
        /// <summary>
        /// The current player
        /// </summary>
        IPlayer Player { get; }

        /// <summary>
        /// Moves the player back to the home spawn location. If the player is configured with a <see cref="ICameraFade"/> effect, it will be used.
        /// If <paramref name="postFadeOutWork"/> is not null, it will be executed while the camera is faded out.
        /// </summary>
        UniTask MovePlayerToHome(Func<UniTask> postFadeOutWork = null);
    }
}
