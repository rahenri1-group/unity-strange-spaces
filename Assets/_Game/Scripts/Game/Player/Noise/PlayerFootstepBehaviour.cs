﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Core.Space;
using Game.Events;
using Game.Physics.PhysicsMaterial.Event;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player
{
    public class PlayerFootstepBehaviour : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] private ISpacePhysics _spacePhysics = null;

        [SerializeField] private FloatReadonlyReference _triggerWalkDistance = null;
        [SerializeField] private FloatReadonlyReference _minWalkDistanceToTrigger = null;
        [SerializeField] private FloatReadonlyReference _walkResetTime = null;
        [SerializeField] private FloatReadonlyReference _runThresholdTime = null;

        [SerializeField] private FloatReadonlyReference _triggerLandSpeed = null;

        private IPlayer _player;
        private IEntityMovement _entityMovement;

        private ISpaceData _playerSpace;

        private bool _groundedPrev;
        private Vector3 _positionPrev;

        private float _distanceSinceLastWalk;
        private float _lastWalkTime;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_triggerWalkDistance > 0f);
            Assert.IsTrue(_minWalkDistanceToTrigger > 0f);
            Assert.IsTrue(_walkResetTime > 0f);
            Assert.IsTrue(_triggerLandSpeed > 0f);

            _player = this.GetComponentAsserted<IPlayer>();
            _entityMovement = this.GetComponentAsserted<IEntityMovement>();

            _groundedPrev = true;
            _distanceSinceLastWalk = 0f;
            _lastWalkTime = Time.time;
        }

        private void OnEnable()
        {
            _playerSpace = _spaceManager.GetEntitySpace(_player);
            _positionPrev = _entityMovement.Position;

            _entityMovement.EntityTeleported += OnEntityTeleport;
        }

        private void OnDisable()
        {
            _entityMovement.EntityTeleported -= OnEntityTeleport;
        }

        private void OnEntityTeleport()
        {
            _playerSpace = _spaceManager.GetEntitySpace(_player);
            _positionPrev = _entityMovement.Position;
        }
        
        private void LateUpdate()
        {
            if (!_entityMovement.IsGrounded)
            {
                _groundedPrev = false;
                _positionPrev = _entityMovement.Position;
                _distanceSinceLastWalk = 0f;
                _lastWalkTime = Time.time;
                return;
            }

            float distanceLastCheck = Vector3.Distance(_positionPrev, _entityMovement.Position);
            _distanceSinceLastWalk += distanceLastCheck;

            var footPosition = _entityMovement.Position;

            if (!_groundedPrev && _entityMovement.IsGrounded && Mathf.Abs(_entityMovement.Velocity.y) >= _triggerLandSpeed)
            {
                if (_spacePhysics.Raycast(_playerSpace, footPosition + 0.1f * transform.up, -transform.up, out var stepRaycastHit, 0.2f, QueryPortalInteraction.CastThroughTransporter))
                {
                    var noisePosition = new SpacePosition(_playerSpace, footPosition);

                    IPhysicsMaterial physicsMaterial = stepRaycastHit.Collider.gameObject.GetComponentInParent<IPhysicsMaterial>();
                    if (physicsMaterial != null)
                    {
                        physicsMaterial.InvokePhysicsEvent(new FootstepLandEvent
                        {
                            FootstepPosition = noisePosition
                        });
                    }

                    _eventBus.InvokeEvent(new PlayerNoiseEvent
                    {
                        Player = _player,
                        NoisePosition = noisePosition,
                        NoiseIntensity = PlayerNoiseIntensity.Medium,
                        Description = "Player Land"
                    });
                }

                _distanceSinceLastWalk = 0;
                _lastWalkTime = Time.time;
            } 
            else if (_distanceSinceLastWalk >= _triggerWalkDistance
                || (distanceLastCheck > _minWalkDistanceToTrigger && (Time.time - _lastWalkTime) >= _walkResetTime))
            {
                if (_spacePhysics.Raycast(_playerSpace, footPosition + 0.1f * transform.up, -transform.up, out var stepRaycastHit, 0.2f, QueryPortalInteraction.CastThroughTransporter))
                {
                    var noisePosition = new SpacePosition(_playerSpace, footPosition);

                    IPhysicsMaterial physicsMaterial = stepRaycastHit.Collider.gameObject.GetComponentInParent<IPhysicsMaterial>();
                    if (physicsMaterial != null)
                    {
                        physicsMaterial.InvokePhysicsEvent(new FootstepWalkEvent
                        {
                            FootstepPosition = noisePosition
                        });
                    }

                    if (Time.time - _lastWalkTime <= _runThresholdTime)
                    {
                        _eventBus.InvokeEvent(new PlayerNoiseEvent
                        {
                            Player = _player,
                            NoisePosition = noisePosition,
                            NoiseIntensity = PlayerNoiseIntensity.Medium,
                            Description = "Player Run"
                        });
                    }
                    else
                    {
                        _eventBus.InvokeEvent(new PlayerNoiseEvent
                        {
                            Player = _player,
                            NoisePosition = noisePosition,
                            NoiseIntensity = PlayerNoiseIntensity.Low,
                            Description = "Player Walk"
                        });
                    }
                }

                _distanceSinceLastWalk = 0;
                _lastWalkTime = Time.time;
            }

            _groundedPrev = _entityMovement.IsGrounded;
            _positionPrev = _entityMovement.Position;
        }
    }
}
