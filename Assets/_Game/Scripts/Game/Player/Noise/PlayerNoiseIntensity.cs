﻿namespace Game.Player
{
    /// <summary>
    /// Enum that describes the intensity of a noise a player makes
    /// </summary>
    public enum PlayerNoiseIntensity
    {
        Low = 0,
        Medium = 1,
        High = 2
    }
}