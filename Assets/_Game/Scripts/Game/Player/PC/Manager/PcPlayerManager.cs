using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Resource;
using UnityEngine;
using Game.Core.Space;
using Game.World;
using UnityEngine.Assertions;
using System;

namespace Game.Player.PC
{
    [Dependency(
        contract: typeof(IPlayerManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class PcPlayerManager : BasePlayerManager
    {
        public override string ModuleName => "PC Player Manager";

        public override ModuleConfig ModuleConfig => Config;
        public override BasePlayerManagerConfig BaseConfig => Config;

        public PcPlayerManagerConfig Config = new PcPlayerManagerConfig();

        public PcPlayerManager(
            ICameraManager cameraManager,
            IEventBus eventBus,
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager)
            : base(
                  cameraManager, 
                  eventBus, 
                  gameObjectResourceManager, 
                  logger,
                  spaceLoader,
                  spaceManager,
                  worldManager)
        { }

        public override UniTask Initialize()
        {
            Assert.IsTrue(GameObjectResourceManager.IsAssetKeyValid(Config.PlayerAssetKey));

            return base.Initialize();
        }

        protected override async UniTask<(GameObject PlayerGameObject, IPlayer Player)> CreatePlayer(ISpaceData space, Vector3 position, Quaternion rotation)
        {
            var player = await GameObjectResourceManager.InstantiateAsync<IPlayer>(Config.PlayerAssetKey, space, position, rotation);

            if (player.GameObject == null)
            {
                Logger.LogError($"Unable to load player asset {Config.PlayerAssetKey}");
                return (null, null);
            }

            player.GameObject.name = "@PC Player";
            player.Component.InitializeNew(Guid.NewGuid());

            return player;
        }
    }
}
