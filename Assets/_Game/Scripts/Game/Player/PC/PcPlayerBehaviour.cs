﻿using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Core.Math;
using Game.Entity;
using Game.Input;
using Game.Item;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.PC
{
    public class PcPlayerBehaviour : BasePlayer, IKeyItemDefinitionProvider
    {
        protected override Transform HeadTransform => _playerHead;

        [Inject] private IPlayerPcInput _playerInput;

        [SerializeField] private Transform _playerHead = null;

        private float _mouseXInput, _mouseYInput;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_playerHead);

            _mouseXInput = 0f;
            _mouseYInput = 0f;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _playerInput.InputEnabled = true;
            _playerInput.PlayerLookActionTriggered += OnPlayerLookAction;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _playerInput.PlayerLookActionTriggered -= OnPlayerLookAction;
        }

        private void OnPlayerLookAction(InputContext context, Vector2 lookAmount)
        {
            _mouseXInput += lookAmount.x;
            _mouseYInput += lookAmount.y;
        }

        protected override void Update()
        {
            ViewLogic();
            MovementLogic();

            _mouseXInput = 0f;
            _mouseYInput = 0f;

            base.Update();
        }

        public bool ContainsKeyItem(IKeyItemDefinition keyItemDefinition)
        {
            return Inventory.ContainsItem(keyItemDefinition);
        }

        private void ViewLogic()
        {
            float deltaX = _mouseXInput;
            float deltaY = -1 * _mouseYInput;

            if (deltaX != 0f)
                EntityMovement.Rotate(Quaternion.Euler(Vector3.up * deltaX));


            if (deltaY != 0)
            {
                float cameraUp = HeadRotation.eulerAngles.x;
                cameraUp = MathUtil.NormalizeAngle180(cameraUp);

                if (deltaY > 0.0f && (deltaY + cameraUp > 90))
                {
                    _playerHead.rotation = Quaternion.Euler(90, HeadRotation.eulerAngles.y, HeadRotation.eulerAngles.z);
                }
                else if (deltaY < 0.0f && (deltaY + cameraUp < -90))
                {
                    _playerHead.rotation = Quaternion.Euler(-90, HeadRotation.eulerAngles.y, HeadRotation.eulerAngles.z);
                }
                else
                {
                    _playerHead.transform.Rotate(Vector3.right, deltaY);
                }
            }
        }

        private void MovementLogic()
        {
            if (!this.IsAlive())
            {
                EntityMovement.Velocity = Vector3.zero;
                return;
            }

            if (EntityMovement.IsGrounded)
            {
                Vector3 horizontalVelocity = Vector3.zero;

                var playMoveActionValue = _playerInput.PlayerMoveActionValue;
                if ((playMoveActionValue.x != 0.0f) || (playMoveActionValue.y != 0.0f))
                {
                    horizontalVelocity.Set(playMoveActionValue.x, 0.0f, playMoveActionValue.y);

                    horizontalVelocity = horizontalVelocity * WalkSpeed;

                    horizontalVelocity = transform.TransformDirection(horizontalVelocity);
                }

                EntityMovement.Velocity = horizontalVelocity;
            }
        }
    }
}
