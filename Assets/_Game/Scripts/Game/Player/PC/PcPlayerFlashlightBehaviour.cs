﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Core.Render.Lighting;
using Game.Entity;
using Game.Input;
using Game.Item.Flashlight;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.PC
{
    public class PcPlayerFlashlightBehaviour : InjectedBehaviour, IFlashlight
    {
        /// <inheritdoc/>
        public event Action<IFlashlight> FlashlightActivated;

        [Inject] private IPlayerPcInput _playerInput;

        [SerializeField] [TypeRestriction(typeof(ILight))] private Component _lightObj = null;
        private ILight _light;

        [SerializeField] [TypeRestriction(typeof(IItemDefinition), false)] private UnityEngine.Object _flashlightDefinitionObj = null;
        private IItemDefinition _flashlightDefinition;

        private IPlayer _player;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_lightObj);
            _light = _lightObj.GetComponent<ILight>();

            Assert.IsNotNull(_flashlightDefinitionObj);
            _flashlightDefinition = (IItemDefinition)_flashlightDefinitionObj;

            _player = this.GetComponentInParentAsserted<IPlayer>();
        }

        private void OnEnable()
        {
            _player.EntityDamaged += OnPlayerDamaged;

            _playerInput.PlayerFlashlightActionTriggered += OnPlayerFlashlightAction;
        }

        private void OnDisable()
        {
            _player.EntityDamaged -= OnPlayerDamaged;

            _playerInput.PlayerFlashlightActionTriggered -= OnPlayerFlashlightAction;
        }

        private void OnPlayerDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            if (!_player.IsAlive())
            {
                _light.LightEnabled = false;
            }
        }

        private void OnPlayerFlashlightAction(InputContext context, bool flashlightTriggered)
        {
            if (flashlightTriggered && _player.Inventory.ContainsItem(_flashlightDefinition))
            {
                _light.LightEnabled = !_light.LightEnabled;

                FlashlightActivated?.Invoke(this);
            }
        }
    }
}
