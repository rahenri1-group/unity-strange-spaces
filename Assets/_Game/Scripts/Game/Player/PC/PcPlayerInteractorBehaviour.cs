﻿using Game.Core.DependencyInjection;
using Game.Input;

namespace Game.Player.PC
{
    public class PcPlayerInteractorBehaviour : BasePlayerInteractor
    {
        [Inject] private IPlayerPcInput _playerInput;

        protected override void OnEnable()
        {
            base.OnEnable();

            _playerInput.InteractActionTriggered += OnPlayerInteractAction;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _playerInput.InteractActionTriggered -= OnPlayerInteractAction;
        }

        private void OnPlayerInteractAction(InputContext context, bool interactTriggered)
        {
            if (InteractionTarget == null) return;

            if (interactTriggered && !IsInteracting)
            {
                InteractBegin();
            }
            else if (!interactTriggered && IsInteracting)
            {
                InteractEnd();
            }
        }
    }
}