﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Entity;
using Game.Physics;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player
{
    [RequireComponent(typeof(IPlayer))]
    public class PlayerDamageAudioBehaviour : InjectedBehaviour
    {
        [SerializeField] private StringReadonlyReference _damageEffectAddress = null;
        [SerializeField] private StringReadonlyReference _deathEffectAddress = null;

        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private IPlayer _player;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsFalse(string.IsNullOrEmpty(_damageEffectAddress));
            Assert.IsTrue(_poolableGameObjectManager.IsAssetKeyValid(_damageEffectAddress));

            Assert.IsFalse(string.IsNullOrEmpty(_deathEffectAddress));
            Assert.IsTrue(_poolableGameObjectManager.IsAssetKeyValid(_deathEffectAddress));

            _player = this.GetComponentAsserted<IPlayer>();
        }

        private void OnEnable()
        {
            _player.EntityDamaged += OnPlayerDamaged;
        }

        private void OnDisable()
        {
            _player.EntityDamaged -= OnPlayerDamaged;
        }

        private void OnPlayerDamaged(IMortalEntity sender, EntityDamagedEventArgs args)
        {
            if (args.DamageType == DamageType.Silent) return;


            var position = args.DamagePosition;
            var space = _spaceManager.GetEntitySpace(sender);

            _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(_damageEffectAddress, space, position).Forget();

            if (!_player.IsAlive())
            {
                _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(_deathEffectAddress, space, _player.HeadPosition).Forget();
            }
        }
    }
}
