﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Physics;
using Game.Entity;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player
{
    public class PlayerLaserRendererBehaviour : InjectedBehaviour
    {
        [Inject] private IPlayerManager _playerManager = null;

        [SerializeField] private LaserInteractableRaycastQuerierBehaviour _raycaster = null;
        [SerializeField] private LineRenderer _lineRenderer = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_raycaster);
            Assert.IsNotNull(_lineRenderer);

            _lineRenderer.enabled = false;
        }

        private void Update()
        {
            if (_playerManager.Player != null && _playerManager.Player.IsAlive() && _raycaster.HasValidResult)
            {
                _lineRenderer.enabled = true;
                _lineRenderer.SetPosition(0, new Vector3(0f, 0f, _raycaster.CurrentResultDistance));
            }
            else
            {
                _lineRenderer.enabled = false;
            }
        }
    }
}