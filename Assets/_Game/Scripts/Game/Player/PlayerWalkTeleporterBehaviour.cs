﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Core.Render.Camera;
using Game.Core.Space;
using Game.Entity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player
{
    public class PlayerWalkTeleporterBehaviour : InjectedBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _playerTriggerObj = null;
        private ITriggerable _playerTrigger;

        [SerializeField] private Transform _teleportDestination = null;

        [SerializeField] [TypeRestriction(typeof(ITriggerVolume<ICharacterEntity>))] private Component _teleportBlockerVolumeObj = null;
        private ITriggerVolume<ICharacterEntity> _teleportBlockerVolume;

        [SerializeField] private FloatReadonlyReference _teleportDelay = null;
        [SerializeField] private FloatReadonlyReference _teleportBlinkDuration = null;

        [SerializeField] private GameObject _teleportIndicationEffect = null;

        [Inject] private ICameraManager _cameraManager = null;
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISpaceManager _spaceManager = null;

        private HashSet<Collider> _playerColliders;

        private IEnumerator _teleportCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_teleportDestination);
            Assert.IsNotNull(_playerTriggerObj);
            Assert.IsNotNull(_teleportBlockerVolumeObj);
            Assert.IsTrue(_teleportDelay > 0f);
            Assert.IsTrue(_teleportBlinkDuration > 0f);

            _playerTrigger = _playerTriggerObj.GetComponent<ITriggerable>();

            _teleportBlockerVolume = _teleportBlockerVolumeObj.GetComponent<ITriggerVolume<ICharacterEntity>>();

            _playerColliders = new HashSet<Collider>();

            _teleportCoroutine = null;
        }

        private void OnEnable()
        {
            _playerTrigger.TriggerEnter += OnPlayerTriggerEnter;
            _playerTrigger.TriggerExit += OnPlayerTriggerExit;

            _teleportBlockerVolume.GameObjectEnter += OnTeleportBlockerEnter;

            SetTeleportIndicatorStatus(false);
        }

        private void OnDisable()
        {
            _playerColliders.Clear();

            if (_teleportCoroutine != null)
            {
                StopCoroutine(_teleportCoroutine);
                _teleportCoroutine = null;

                SetTeleportIndicatorStatus(false);
            }

            _playerTrigger.TriggerEnter -= OnPlayerTriggerEnter;
            _playerTrigger.TriggerExit -= OnPlayerTriggerExit;

            _teleportBlockerVolume.GameObjectEnter -= OnTeleportBlockerEnter;
        }

        private void OnPlayerTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            var player = args.Collider.GetComponentInParent<IPlayer>();
            if (player == null) return;

            _playerColliders.Add(args.Collider);

            if (_teleportCoroutine == null && _teleportBlockerVolume.IsEmpty())
            {
                _teleportCoroutine = DelayedTeleport(player);
                StartCoroutine(_teleportCoroutine);
            }
        }

        private void OnPlayerTriggerExit(ITriggerable sender, TriggerableEventArgs args)
        {
            if (args.Collider == null) return;

            var player = args.Collider.GetComponentInParent<IPlayer>();
            if (player == null) return;

            _playerColliders.Remove(args.Collider);

            if (_playerColliders.Count == 0 && _teleportCoroutine != null)
            {
                StopCoroutine(_teleportCoroutine);
                _teleportCoroutine = null;

                SetTeleportIndicatorStatus(false);
            }
        }

        private void OnTeleportBlockerEnter(ITriggerVolume<ICharacterEntity> sender, ICharacterEntity other)
        {
            // prevent teleportation if an enemy is on the stairs
            if (other is not IPlayer)
            {
                if (_teleportCoroutine != null)
                {
                    StopCoroutine(_teleportCoroutine);
                    _teleportCoroutine = null;

                    SetTeleportIndicatorStatus(false);
                }
            }
        }

        private void SetTeleportIndicatorStatus(bool active)
        {
            if (_teleportIndicationEffect != null)
                _teleportIndicationEffect.SetActive(active);
        }

        private IEnumerator DelayedTeleport(IPlayer player)
        {
            SetTeleportIndicatorStatus(true);

            yield return new WaitForSeconds(_teleportDelay);

            _playerColliders.Clear();

            var fadeEffect = _cameraManager.ActivePrimaryCamera?.GetCameraEffect<ICameraFade>();
            if (fadeEffect != null || fadeEffect.IsFading)
            {
                fadeEffect.Blink(_teleportBlinkDuration, () =>
                {
                    TeleportPlayer(player);
                });
            }
            else
            {
                TeleportPlayer(player);
            }

            _teleportCoroutine = null;
        }

        private void TeleportPlayer(IPlayer player)
        {
            player.EntityMovement.Teleport(_teleportDestination.position, _teleportDestination.rotation);
            SetTeleportIndicatorStatus(false);

            var playerSpace = _spaceManager.GetEntitySpace(player);
            _eventBus.InvokeEvent(new EntityPostTeleportEvent
            {
                Entity = player,
                Portal = null,
                OldSpace = playerSpace,
                NewSpace = playerSpace
            });

        }
    }
}
