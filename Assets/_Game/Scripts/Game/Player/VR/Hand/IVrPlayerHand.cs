﻿using Game.Core;
using Game.Input;
using System;

namespace Game.Player.VR
{
    public interface IVrPlayerHand : IGameObjectComponent
    {
        IVrTrackedHand HandInput { get; }
        IHapticVrHand HandHaptic { get; }

        bool IsEnabled { get; }

        bool IsLeftHand { get; }

        event Action Enabled;
        event Action Disabled;
    }
}
