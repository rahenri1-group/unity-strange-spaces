using Game.Core;
using Game.Core.Interaction;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    /// <summary>
    /// Base class for render components on a vr player's hand that should be hidden when the hand is interacting
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public abstract class BasePlayerHandMeshHider : MonoBehaviour
    {
        public float Alpha
        {
            get => MeshRenderer.material.color.a;
            protected set
            {
                var color = MeshRenderer.material.color;
                color.a = value;
                MeshRenderer.material.color = color;
            }
        }

        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _playerHandObj = null;
        private IVrPlayerHand _playerHand;

        [SerializeField] [TypeRestriction(typeof(IEntityInteractor))] private Component _playerInteractorObj = null;
        private IEntityInteractor _playerInteractor;

        [SerializeField] private FloatReadonlyReference _fadeDuration = null;

        protected Renderer MeshRenderer { get; private set; }

        private IEnumerator _fadeCoroutine;

        /// <inheritdoc/>
        protected virtual void Awake()
        {
            Assert.IsNotNull(_playerHandObj);
            _playerHand = _playerHandObj.GetComponent<IVrPlayerHand>();
            Assert.IsNotNull(_playerInteractorObj);
            _playerInteractor = _playerInteractorObj.GetComponent<IEntityInteractor>();
            Assert.IsTrue(_fadeDuration > 0f);

            MeshRenderer = GetComponent<Renderer>();

            _fadeCoroutine = null;
        }

        /// <inheritdoc/>
        protected virtual void OnEnable()
        {
            _playerHand.Enabled += OnPlayerHandEnabled;
            _playerHand.Disabled += OnPlayerHandDisabled;

            _playerInteractor.EntityInteractBegin += OnInteractBegin;
            _playerInteractor.EntityInteractEnd += OnInteractEnd;

            if (_playerHand.IsEnabled)
            {
                OnPlayerHandEnabled();
            }
            else
            {
                OnPlayerHandDisabled();
            }
        }

        /// <inheritdoc/>
        protected virtual void OnDisable()
        {
            if (_fadeCoroutine != null)
            {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;
            }

            _playerHand.Enabled -= OnPlayerHandEnabled;
            _playerHand.Disabled -= OnPlayerHandDisabled;

            _playerInteractor.EntityInteractBegin -= OnInteractBegin;
            _playerInteractor.EntityInteractEnd -= OnInteractEnd;
        }

        private void OnPlayerHandEnabled()
        {
            if (!_playerInteractor.IsInteracting)
            {
                ShowHand();
            }
        }

        private void OnPlayerHandDisabled()
        {
            HideHand();
        }

        private void OnInteractBegin(IEntityInteractor interactor)
        {
            if (interactor.InteractionTarget is IWorldInteractable)
            {
                HideHand();
            }
        }

        private void OnInteractEnd(IEntityInteractor interactor)
        {
            if (_playerHand.IsEnabled)
            {
                ShowHand();
            }
        }

        private void ShowHand()
        {
            if (_fadeCoroutine != null)
                StopCoroutine(_fadeCoroutine);

            _fadeCoroutine = Fade(_fadeDuration, 1f);
            StartCoroutine(_fadeCoroutine);
        }

        private void HideHand()
        {
            if (_fadeCoroutine != null)
                StopCoroutine(_fadeCoroutine);

            _fadeCoroutine = Fade(_fadeDuration, 0f);
            StartCoroutine(_fadeCoroutine);
        }

        private IEnumerator Fade(float duration, float targetAlpha)
        {
            YieldInstruction wait = null;

            float startAlpha = MeshRenderer.material.color.a;

            float startTime = Time.time;
            while (Time.time - startTime <= duration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / duration);
                Alpha = Mathf.SmoothStep(startAlpha, targetAlpha, lerp);
            }

            Alpha = targetAlpha;

            _fadeCoroutine = null;
        }
    }
}

