﻿namespace Game.Player.VR
{    
    /// <summary>
    /// Simple mesh hider for a vr player's hand
    /// </summary>
    public class VrPlayerHandMeshHiderBehaviour : BasePlayerHandMeshHider { }
}
