﻿using Game.Core;
using UnityEngine;

namespace Game.Player.VR
{
    /// <summary>
    /// Component that updates a renderer on a vr player's hand indicating their current health
    /// </summary>
    public class VrPlayerHealthMeshBehaviour : BasePlayerHandMeshHider
    {
        [SerializeField] private IntReadonlyReference _playerCurrentHealth = null;
        [SerializeField] private IntReadonlyReference _playerMaxHealth = null;

        [SerializeField] private ColorReadonlyReference _minHealthColor = null;
        [SerializeField] private ColorReadonlyReference _maxHealthColor = null;

        private int _lerpShaderId;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            _lerpShaderId = Shader.PropertyToID("_Lerp");
        }

        /// <inheritdoc/>
        protected override void OnEnable()
        {
            base.OnEnable();

            UpdateHealthRenderer();

            _playerCurrentHealth.ValueChanged += OnPlayerHealthChanged;
        }

        /// <inheritdoc/>
        protected override void OnDisable()
        {
            base.OnDisable();

            _playerCurrentHealth.ValueChanged -= OnPlayerHealthChanged;
        }

        private void OnPlayerHealthChanged()
        {
            UpdateHealthRenderer();
        }

        private void UpdateHealthRenderer()
        {
            MeshRenderer.material.SetFloat(_lerpShaderId, ((float)_playerCurrentHealth) / _playerMaxHealth);

            float alpha = Alpha;
            MeshRenderer.material.color = Color.Lerp(
                _minHealthColor, 
                _maxHealthColor,
                Mathf.Clamp01((_playerCurrentHealth - 1f) / (_playerMaxHealth - 1f))); // subtract 1 so that min color is displayed when player has 1 health left
            Alpha = alpha;
        }
    }
}