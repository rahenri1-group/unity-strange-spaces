﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerHandPositionerBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _playerHandObj = null;
        private IVrPlayerHand _playerHand;
        
        private Rigidbody _body;

        private void Awake()
        {
            Assert.IsNotNull(_playerHandObj);
            _playerHand = _playerHandObj.GetComponent<IVrPlayerHand>();

            _body = GetComponent<Rigidbody>();

            _playerHand.Enabled += OnHandEnabled;
        }

        private void OnDestroy()
        {
            _playerHand.Enabled -= OnHandEnabled;
        }

        private void OnHandEnabled()
        {
            var inputTransform = _playerHand.GameObject.transform;

            _body.rotation = inputTransform.rotation;
            _body.position = inputTransform.position;
        }

        private void FixedUpdate()
        {
            _body.velocity = Vector3.zero;
            _body.angularVelocity = Vector3.zero;

            if (_playerHand.IsEnabled)
            {
                var inputTransform = _playerHand.GameObject.transform;

                _body.MoveRotation(inputTransform.rotation);
                _body.velocity = (inputTransform.position - _body.position) / Time.fixedDeltaTime;
            }
        }
    }
}
