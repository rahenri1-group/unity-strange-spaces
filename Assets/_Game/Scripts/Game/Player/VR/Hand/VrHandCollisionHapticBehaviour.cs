﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    [RequireComponent(typeof(Rigidbody))]
    public class VrHandCollisionHapticBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _playerHandObj = null;
        private IVrPlayerHand _playerHand;

        [SerializeField] private FloatReadonlyReference _hapticDuration = null;
        [SerializeField] private FloatReadonlyReference _hapticFrequency = null;

        [SerializeField] private FloatReadonlyReference _hapticMinAmplitude = null;
        [SerializeField] private FloatReadonlyReference _hapticMaxAmplitude = null;

        [SerializeField] private FloatReadonlyReference _hapticMinVelocity = null;
        [SerializeField] private FloatReadonlyReference _hapticMaxVelocity = null;

        [SerializeField] private FloatReadonlyReference _minDistanceBetweenHaptics = null;
        [SerializeField] private FloatReadonlyReference _minTimeBetweenHaptics = null;

        private Rigidbody _body;
        private Vector3 _lastHapticPosition;
        private float _lastHapticTime;

        private void Awake()
        {
            Assert.IsTrue(_hapticDuration > 0f);
            Assert.IsTrue(_hapticFrequency > 0f);
            Assert.IsTrue(_minDistanceBetweenHaptics >= 0f);
            Assert.IsTrue(_minTimeBetweenHaptics >= 0f);

            Assert.IsTrue(_hapticMinAmplitude > 0f);
            Assert.IsTrue(_hapticMaxAmplitude >= _hapticMinAmplitude);

            Assert.IsTrue(_hapticMinVelocity >= 0f);
            Assert.IsTrue(_hapticMaxVelocity >= _hapticMinVelocity);

            Assert.IsNotNull(_playerHandObj);
            _playerHand = _playerHandObj.GetComponent<IVrPlayerHand>();

            _body = GetComponent<Rigidbody>();

            _lastHapticPosition = Vector3.zero;
            _lastHapticTime = 0f;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (_playerHand.IsEnabled
                && (Time.time - _lastHapticTime) >= _minTimeBetweenHaptics)
            {
                ProcessCollision(collision);
            }
        }

        private void OnCollisionStay(Collision collision)
        {
            if (_playerHand.IsEnabled
                && Vector3.Distance(_body.position, _lastHapticPosition) >= _minDistanceBetweenHaptics
                && (Time.time - _lastHapticTime) >= _minTimeBetweenHaptics)
            {
                ProcessCollision(collision);
            }
        }

        private void ProcessCollision(Collision collision)
        {
            _lastHapticPosition = _body.position;
            _lastHapticTime = Time.time;

            float amplitude = Mathf.Lerp(_hapticMinAmplitude, _hapticMaxAmplitude, Mathf.InverseLerp(_hapticMinVelocity, _hapticMaxVelocity, _body.velocity.magnitude));
            _playerHand.HandHaptic.SendHapticPulse(amplitude, _hapticFrequency, _hapticDuration);
        }
    }
}
