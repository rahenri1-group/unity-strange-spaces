﻿using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    [RequireComponent(typeof(Animator))]
    public class VrPlayerHandAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _handObj = null;
        private IVrPlayerHand _hand;

        private Animator _handAnimator;
        private int _gripAnimatorId;

        private void Awake()
        {
            Assert.IsNotNull(_handObj);
            _hand = _handObj.GetComponentAsserted<IVrPlayerHand>();

            _handAnimator = this.GetComponentAsserted<Animator>();
            
            _gripAnimatorId = Animator.StringToHash("Grip");
        }

        private void Update()
        {
            if (!_hand.IsEnabled) return;

            _handAnimator.SetFloat(_gripAnimatorId, _hand.HandInput.Grip);
        }
    }
}
