﻿using Game.Core.DependencyInjection;
using Game.Input;
using System;
using UnityEngine;

namespace Game.Player.VR
{
    public class VrPlayerHandBehaviour : BaseTrackedPlayerComponent, IVrPlayerHand
    {
        public bool IsEnabled { get; private set; } = false;

        public bool IsLeftHand => _isLeftHand;

        public override IVrTrackedDevice TrackedDevice => HandInput;

        public IVrTrackedHand HandInput { get; private set; }
        public IHapticVrHand HandHaptic { get; private set; }

        public event Action Enabled;
        public event Action Disabled;

        [Inject] private IHapticVrInput _hapticVrInput = null;

        [SerializeField] private bool _isLeftHand = false;

        protected override void Awake()
        {
            base.Awake();

            HandInput = (_isLeftHand) ? VrTrackingInput.HandLeft : VrTrackingInput.HandRight;
            HandHaptic = (_isLeftHand) ? _hapticVrInput.HandLeft : _hapticVrInput.HandRight;
        }

        private void OnDestroy()
        {
            HandInput = null;
            HandHaptic = null;
        }

        public override void Sync()
        {
            if (!InjectionComplete) return;

            if (TrackedDevice.Tracked)
            {
                base.Sync();

                if (!IsEnabled)
                {
                    IsEnabled = true;
                    Enabled?.Invoke();
                }
            }
            else
            {
                if (IsEnabled)
                {
                    IsEnabled = false;
                    Disabled?.Invoke();
                }
            }
        }
    }
}
