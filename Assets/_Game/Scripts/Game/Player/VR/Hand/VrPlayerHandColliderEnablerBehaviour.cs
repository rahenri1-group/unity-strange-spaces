﻿using Game.Core;
using Game.Core.Interaction;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    public class VrPlayerHandColliderEnablerBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IEntityInteractor))] private Component _playerInteractorObj = null;
        private IEntityInteractor _playerInteractor;

        [SerializeField] private FloatReadonlyReference _enableDelay = null;

        private Collider[] _handColliders;

        private IEnumerator _enableDelayCoroutine;

        private void Awake()
        {
            Assert.IsNotNull(_playerInteractorObj);
            _playerInteractor = _playerInteractorObj.GetComponent<IEntityInteractor>();

            _handColliders = GetComponentsInChildren<Collider>();

            _enableDelayCoroutine = null;
        }

        private void OnEnable()
        {
            if (_enableDelayCoroutine != null)
            {
                StopCoroutine(_enableDelayCoroutine);
                _enableDelayCoroutine = null;
            }

            if (_playerInteractor.IsInteracting)
            {
                SetCollidersEnabled(false);
            }
            else
            {
                _enableDelayCoroutine = EnableCollidersDelayed();
                StartCoroutine(_enableDelayCoroutine);
            }

            _playerInteractor.EntityInteractBegin += OnInteractBegin;
            _playerInteractor.EntityInteractEnd += OnInteractEnd;
        }

        private void OnDisable()
        {
            if (_enableDelayCoroutine != null)
            {
                StopCoroutine(_enableDelayCoroutine);
                _enableDelayCoroutine = null;
            }

            _playerInteractor.EntityInteractBegin -= OnInteractBegin;
            _playerInteractor.EntityInteractEnd -= OnInteractEnd;
        }

        private void OnInteractBegin(IEntityInteractor interactor)
        {
            if (interactor.InteractionTarget is IWorldInteractable)
            {
                if (_enableDelayCoroutine != null)
                {
                    StopCoroutine(_enableDelayCoroutine);
                    _enableDelayCoroutine = null;
                }

                SetCollidersEnabled(false);
            }
        }

        private void OnInteractEnd(IEntityInteractor interactor)
        {
            if (_enableDelayCoroutine != null)
            {
                StopCoroutine(_enableDelayCoroutine);
            }

            _enableDelayCoroutine = EnableCollidersDelayed();
            StartCoroutine(_enableDelayCoroutine);
        }

        private void SetCollidersEnabled(bool collidersEnabled)
        {
            foreach (var collider in _handColliders)
            {
                collider.enabled = collidersEnabled;
            }
        }

        private IEnumerator EnableCollidersDelayed()
        {
            if (_enableDelay > 0f)
            {
                yield return new WaitForSeconds(_enableDelay);
            }

            SetCollidersEnabled(true);

            _enableDelayCoroutine = null;
        }
    }
}
