﻿using Game.Input;

namespace Game.Player.VR
{
    public class VrPlayerHeadBehaviour : BaseTrackedPlayerComponent
    {
        public override IVrTrackedDevice TrackedDevice => VrTrackingInput.Hmd;
    }
}
