﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Storage.Settings;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    public class VrPlayerHealthDisplayToggler : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISettingsManager _settingsManager = null;

        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _handObj = null;
        private IVrPlayerHand _hand;

        [SerializeField] private GameObject[] _healthObjects = new GameObject[0];

        private IPreferencesVrSettingsGroup _preferencesSettingsGroup;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_handObj);
            _hand = _handObj.GetComponentAsserted<IVrPlayerHand>();
        }

        private void OnEnable()
        {
            _preferencesSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IPreferencesVrSettingsGroup>();

            UpdateHealthVisibility();

            _preferencesSettingsGroup.SettingsGroupUpdated += OnSettingsGroupUpdated;
            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnDisable()
        {
            _preferencesSettingsGroup.SettingsGroupUpdated -= OnSettingsGroupUpdated;

            _preferencesSettingsGroup = null;

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventData)
        {
            _preferencesSettingsGroup.SettingsGroupUpdated -= OnSettingsGroupUpdated;
            _preferencesSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IPreferencesVrSettingsGroup>();
            _preferencesSettingsGroup.SettingsGroupUpdated += OnSettingsGroupUpdated;

            UpdateHealthVisibility();
        }

        private void OnSettingsGroupUpdated(ISettingsGroup group)
        {
            UpdateHealthVisibility();
        }

        private void UpdateHealthVisibility()
        {
            bool shouldDisplay = (_preferencesSettingsGroup.HealthDisplayHand == VrHand.Left && _hand.IsLeftHand)
                || (_preferencesSettingsGroup.HealthDisplayHand == VrHand.Right && !_hand.IsLeftHand);

            foreach (var obj in _healthObjects)
            {
                obj.SetActive(shouldDisplay);
            }
        }
    }
}