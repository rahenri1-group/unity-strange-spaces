﻿using UnityEngine;

namespace Game.Player.VR
{
    public interface IVrPlayer : IPlayer 
    {
        Transform VrRigRoot { get; }
    }
}
