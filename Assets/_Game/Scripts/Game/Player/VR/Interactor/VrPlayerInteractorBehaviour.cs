﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Effect;
using Game.Core.Event;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Input;
using Game.Item;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    /// <inheritdoc cref="IVrItemEquiptor"/>
    public class VrPlayerInteractorBehaviour : BasePlayerInteractor, IVrItemEquiptor
    {
        /// <inheritdoc />
        public override bool IsInteractionEnabled => _hand.IsEnabled && base.IsInteractionEnabled;

        /// <inheritdoc />
        public IItemInput ItemInput => _playerHandInput;

        /// <inheritdoc />
        public IEquiptableItem EquiptedItem { get; private set; }

        /// <inheritdoc />
        public event Action EquiptedItemChanged;

        /// <inheritdoc />
        public bool IsLeftHand => _hand.IsLeftHand;

        [Inject] private IGameObjectResourceManager _gameObjectResourceManager = null;
        [Inject] private IPlayerVrInput _playerInput = null;
        [Inject] private IPoolableGameObjectManager _poolableGameObjectManager = null;
        [Inject] private IUiVrInput _uiVrInput = null;

        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _handObj = null;
        private IVrPlayerHand _hand;

        [SerializeField] private Transform _itemHolder = null;

        private IPlayerVrHandInput _playerHandInput;

        /// <inheritdoc />
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_handObj);
            _hand = _handObj.GetComponentAsserted<IVrPlayerHand>();

            Assert.IsNotNull(_itemHolder);

            _playerHandInput =  (_hand.IsLeftHand) ? _playerInput.HandLeft : _playerInput.HandRight;

            EquiptedItem = null;

            _hand.Disabled += OnHandDisabled;
        }

        /// <inheritdoc />
        protected override void OnDestroy()
        {
            base.OnDestroy();

            _hand.Disabled -= OnHandDisabled;
        }

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();

            _playerHandInput.InteractActionTriggered += OnPlayerInteractAction;

            if (_hand.IsLeftHand)
            {
                _uiVrInput.SelectLeftTriggered += OnPlayerSelectAction;
            }
            else
            {
                _uiVrInput.SelectRightTriggered += OnPlayerSelectAction;
            }
        }

        /// <inheritdoc />
        protected override void OnDisable()
        {
            base.OnDisable();

            _playerHandInput.InteractActionTriggered -= OnPlayerInteractAction;

            if (_hand.IsLeftHand)
            {
                _uiVrInput.SelectLeftTriggered -= OnPlayerSelectAction;
            }
            else
            {
                _uiVrInput.SelectRightTriggered -= OnPlayerSelectAction;
            }
        }

        /// <inheritdoc />
        public override void OnInteractBegin(IInteractable interactable)
        {
            base.OnInteractBegin(interactable);

            EquiptedItem = interactable as IEquiptableItem;
            if (EquiptedItem != null)
            {
                var itemTransfrom = EquiptedItem.GameObject.transform;
                itemTransfrom.SetParent(_itemHolder);
                itemTransfrom.localPosition = Vector3.zero;
                itemTransfrom.localRotation = Quaternion.identity;
                itemTransfrom.localScale = Vector3.one;

                EquiptedItemChanged?.Invoke();

                EventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = Player
                });

                if (!string.IsNullOrEmpty(EquiptedItem.ItemDefinition.PickupEffectAddress))
                {
                    _poolableGameObjectManager.GetPooledObjectAsync<IPoolableEffect>(
                        EquiptedItem.ItemDefinition.PickupEffectAddress,
                        SpaceManager.GetEntitySpace(Player),
                        transform.position)
                        .Forget();
                }
            }
        }

        /// <inheritdoc />
        public override void OnInteractEnd()
        {
            base.OnInteractEnd();

            if (EquiptedItem != null)
            {
                EquiptedItem.GameObject.transform.SetParent(null);
                EquiptedItem = null;

                EquiptedItemChanged?.Invoke();

                EventBus.InvokeEvent(new EntityModifiedEvent
                {
                    Entity = Player
                });
            }
        }

        private void OnHandDisabled()
        {
            if (EquiptedItem != null)
            {
                EquiptedItem.PreDestroyItem();
            }

            // cache value first, it will be nulled out after interaction end
            var equiptedItem = EquiptedItem;

            InteractionManager.OnInteractorDisable(this);

            if (equiptedItem != null)
            {
                _gameObjectResourceManager.DestroyObject(equiptedItem.GameObject);
            }
        }

        protected override void OnPlayerDeath()
        {
            // cache value first, it will be nulled out after base call
            var equiptedItem = EquiptedItem;

            base.OnPlayerDeath();

            if (equiptedItem != null)
            {
                _gameObjectResourceManager.DestroyObject(equiptedItem.GameObject);
            }
        }

        private void OnPlayerInteractAction(InputContext context, bool interactTriggered)
        {
            if (InteractionTarget == null || !_hand.IsEnabled) return;

            if (InteractionTarget is IWorldInteractable)
            {
                if (interactTriggered && !IsInteracting)
                {
                    InteractBegin();
                }
                else if (!interactTriggered && IsInteracting && EquiptedItem == null)
                {
                    InteractEnd();
                }
            }
        }

        private void OnPlayerSelectAction(InputContext context)
        {
            if (InteractionTarget == null || !_hand.IsEnabled) return;

            if (InteractionTarget is ILaserInteractable && !IsInteracting)
            {
                InteractBegin();
                InteractEnd();
            }
        }
    }
}
