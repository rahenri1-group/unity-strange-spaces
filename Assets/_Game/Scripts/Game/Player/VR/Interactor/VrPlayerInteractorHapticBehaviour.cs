﻿using Game.Core;
using Game.Core.Interaction;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    public class VrPlayerInteractorHapticBehaviour : MonoBehaviour, IInteractorHaptic
    {
        [SerializeField] [TypeRestriction(typeof(IVrPlayerHand))] private Component _handObj = null;
        private IVrPlayerHand _hand;

        private void Awake()
        {
            Assert.IsNotNull(_handObj);
            _hand = _handObj.GetComponentAsserted<IVrPlayerHand>();
        }

        public void HapticPulse(float amplitude, float frequency, float duration)
        {
            _hand.HandHaptic.SendHapticPulse(amplitude, frequency, duration);
        }
    }
}
