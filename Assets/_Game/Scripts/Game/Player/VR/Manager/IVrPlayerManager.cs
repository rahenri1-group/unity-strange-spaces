﻿namespace Game.Player.VR
{
    public interface IVrPlayerManager : IPlayerManager
    {
        IVrPlayer VrPlayer { get; }
    }
}
