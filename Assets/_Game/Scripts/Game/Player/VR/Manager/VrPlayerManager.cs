﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Render.Camera;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Input;
using Game.World;
using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR.Management;

namespace Game.Player.VR
{
    [Dependency(
        contract: typeof(IPlayerManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IVrPlayerManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class VrPlayerManager : BasePlayerManager, IVrPlayerManager
    {
        public override string ModuleName => "VR Player Manager";
        public override ModuleConfig ModuleConfig => Config;
        public override BasePlayerManagerConfig BaseConfig => Config;
        public VrPlayerManagerConfig Config = new VrPlayerManagerConfig();

        public IVrPlayer VrPlayer => (IVrPlayer)Player;

        private IHapticVrInput _hapticVrInput;
        private IVrTrackingInput _vrTrackingInput;

        public VrPlayerManager(
            ICameraManager cameraManager,
            IEventBus eventBus,
            IGameObjectResourceManager gameObjectResourceManager,
            IHapticVrInput hapticVrInput,
            ILogRouter logger,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager,
            IVrTrackingInput vrTrackingInput)
            : base(
                  cameraManager, 
                  eventBus, 
                  gameObjectResourceManager, 
                  logger, 
                  spaceLoader,
                  spaceManager,
                  worldManager)
        {
            _hapticVrInput = hapticVrInput;
            _vrTrackingInput = vrTrackingInput;
        }

        public override async UniTask Initialize()
        {
            Assert.IsTrue(GameObjectResourceManager.IsAssetKeyValid(Config.PlayerAssetKey));

            // XR Management needs 1 frame to startup
            await UniTask.DelayFrame(1);

            var xrManager = XRGeneralSettings.Instance.Manager;

            if (!xrManager.isInitializationComplete)
            {
                await XRGeneralSettings.Instance.Manager.InitializeLoader();
            }

            xrManager.StartSubsystems();

            // module is not initialized until we have hmd values
            _vrTrackingInput.InputEnabled = true;
            while (!_vrTrackingInput.Hmd.Tracked)
            {
                await UniTask.DelayFrame(1);
            }

            _hapticVrInput.InputEnabled = true;

            await base.Initialize();
        }

        public override UniTask Shutdown()
        {
            var xrManager = XRGeneralSettings.Instance.Manager;

            if (xrManager.isInitializationComplete)
            {
                xrManager.StopSubsystems();
                xrManager.DeinitializeLoader();
            }

            return base.Shutdown();
        }

        protected override async UniTask<(GameObject PlayerGameObject, IPlayer Player)> CreatePlayer(ISpaceData space, Vector3 position, Quaternion rotation)
        {
            var player = await GameObjectResourceManager.InstantiateAsync<IVrPlayer>(Config.PlayerAssetKey, space, position, rotation);

            if (player.GameObject == null)
            {
                Logger.LogError($"Unable to load player asset {Config.PlayerAssetKey}");
                return (null, null);
            }

            player.GameObject.name = "@VR Player";
            player.Component.InitializeNew(Guid.NewGuid());
            player.Component.EntityMovement.RotateTo(rotation);

            return player;
        }
    }
}
