﻿using Game.Core;
using Game.Core.Entity;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    [RequireComponent(typeof(IVrPlayer))]
    public class VrPlayerMovementBehaviour : EntityMovementCharacterControllerBehaviour
    {
        [SerializeField] private CapsuleCollider _capsuleBodyCollider = null;

        private IVrPlayer _vrPlayer;

        public override Vector3 Position 
        {
            get => _vrPlayer.FeetPosition;
            protected set => SetVrPlayerPosition(value); 
        }

        public override Quaternion Rotation 
        { 
            get => Quaternion.Euler(0f, _vrPlayer.HeadRotation.eulerAngles.y, 0f); 
            protected set => SetVrPlayerRotation(value); 
        }

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_capsuleBodyCollider);

            _vrPlayer = this.GetComponentAsserted<IVrPlayer>();

            // teleport so player head is where the rig was initially placed
            Teleport(transform.position, transform.rotation);
        }

        protected override void FixedUpdate()
        {
            RecalculateCharacterController();
            RecalculateBodyCapsule();

            base.FixedUpdate();
        }

        private void SetVrPlayerPosition(Vector3 position)
        {
            Vector3 playerOffset = _vrPlayer.HeadPosition - transform.position;
            playerOffset.y = 0f;
            transform.position = position - playerOffset;

            RecalculateCharacterController();
            RecalculateBodyCapsule();
        }

        private void SetVrPlayerRotation(Quaternion rotation)
        {
            float yRotation = rotation.eulerAngles.y; // only support rotating a vr player on the y axis

            Vector3 playerOrigPos = _vrPlayer.FeetPosition;

            Vector3 playerSpaceAngles = transform.eulerAngles;

            playerSpaceAngles.y = yRotation - (_vrPlayer.HeadRotation.eulerAngles.y - playerSpaceAngles.y);
            transform.eulerAngles = playerSpaceAngles;

            SetVrPlayerPosition(playerOrigPos);
        }

        private void RecalculateCharacterController()
        {
            CharacterController.height = CharacterController.transform.InverseTransformPoint(_vrPlayer.HeadPosition).y;

            var controllerPos = CharacterController.transform.InverseTransformPoint(_vrPlayer.HeadPosition);
            controllerPos.y = CharacterController.height / 2f + CharacterController.skinWidth;
            CharacterController.center = controllerPos;
        }

        private void RecalculateBodyCapsule()
        {
            _capsuleBodyCollider.height = _capsuleBodyCollider.transform.InverseTransformPoint(_vrPlayer.HeadPosition).y;

            var bodyPos = _capsuleBodyCollider.transform.InverseTransformPoint(_vrPlayer.HeadPosition);
            bodyPos.y = _capsuleBodyCollider.height / 2f;
            _capsuleBodyCollider.center = bodyPos;
        }
    }
}
