﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Input;
using System;
using UnityEngine;

namespace Game.Player.VR
{
    public abstract class BaseTrackedPlayerComponent : InjectedBehaviour, ITrackedPlayerComponent
    {
        public GameObject GameObject => gameObject;

        public abstract IVrTrackedDevice TrackedDevice { get; }

        public event Action<ITrackedPlayerComponent> TransformUpdated;

        [Inject] protected IVrTrackingInput VrTrackingInput = null;

        public virtual void Sync()
        {
            if (!InjectionComplete) return;

            transform.localPosition = TrackedDevice.Position;
            transform.localRotation = TrackedDevice.Rotation;

            TransformUpdated?.Invoke(this);
        }
    }
}
