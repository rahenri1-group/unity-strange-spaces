﻿using Game.Core;
using Game.Input;
using System;

namespace Game.Player.VR
{
    public interface ITrackedPlayerComponent : IGameObjectComponent
    {
        event Action<ITrackedPlayerComponent> TransformUpdated;

        IVrTrackedDevice TrackedDevice { get; }

        void Sync();
    }
}
