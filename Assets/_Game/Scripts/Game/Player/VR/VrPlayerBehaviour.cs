﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Render.Camera;
using Game.Core.Storage.Settings;
using Game.Entity;
using Game.Input;
using Game.Storage.Settings;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    public class VrPlayerBehaviour : BasePlayer, IVrPlayer
    {
        public Transform VrRigRoot => transform;

        protected override Transform HeadTransform => _playerHead.GameObject.transform;

        [Inject] private ICameraManager _cameraManager;
        [Inject] private IPlayerVrInput _playerInput;
        [Inject] private ISettingsManager _settingsManager;
        [Inject] private IVrTrackingInput _vrTrackingInput;

        [SerializeField] private FloatReadonlyReference _snapTurnBlinkDuration;
        [SerializeField] [TypeRestriction(typeof(ITrackedPlayerComponent))] private Component _playerHeadObj;
        [SerializeField] [TypeRestriction(typeof(ITrackedPlayerComponent))] private Component _playerHandLeftObj;
        [SerializeField] [TypeRestriction(typeof(ITrackedPlayerComponent))] private Component _playerHandRightObj;

        private ITrackedPlayerComponent _playerHead;
        private ITrackedPlayerComponent _playerHandLeft;
        private ITrackedPlayerComponent _playerHandRight;

        private IInputVrSettingsGroup _inputSettingsGroup;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_snapTurnBlinkDuration > 0f);
            Assert.IsNotNull(_playerHeadObj);
            Assert.IsNotNull(_playerHandLeftObj);
            Assert.IsNotNull(_playerHandRightObj);

            _playerHead = _playerHeadObj.GetComponent<ITrackedPlayerComponent>();
            _playerHandLeft = _playerHandLeftObj.GetComponent<ITrackedPlayerComponent>();
            _playerHandRight = _playerHandRightObj.GetComponent<ITrackedPlayerComponent>();
        }

        protected override void OnEnable()
        {
            _playerInput.InputEnabled = true;
            _vrTrackingInput.InputEnabled = true;

            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputVrSettingsGroup>();

            UpdateTrackedPositions();

            _playerInput.PlayerSnapLeftTriggered += OnPlayerSnapLeft;
            _playerInput.PlayerSnapRightTriggered += OnPlayerSnapRight;

            EventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);

            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _inputSettingsGroup = null;

            _playerInput.PlayerSnapLeftTriggered -= OnPlayerSnapLeft;
            _playerInput.PlayerSnapRightTriggered -= OnPlayerSnapRight;

            EventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        protected override void Update()
        {
            UpdateTrackedPositions();
            MovementLogic();

            base.Update();
        }

        private void OnPreRender()
        {
            UpdateTrackedPositions();
        }

        private void OnPlayerSnapLeft(InputContext context)
        {
            SnapRotatePlayer(-_inputSettingsGroup.SnapTurnAmount);
        }

        private void OnPlayerSnapRight(InputContext context)
        {
            SnapRotatePlayer(_inputSettingsGroup.SnapTurnAmount);
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventArgs)
        {
            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputVrSettingsGroup>();
        }

        private void UpdateTrackedPositions()
        {
            SyncTrackedComponent(_playerHead);
            SyncTrackedComponent(_playerHandLeft);
            SyncTrackedComponent(_playerHandRight);

            InvokePlayerHeadMovedIfNecessary();
        }

        private void SyncTrackedComponent(ITrackedPlayerComponent trackedComponent)
        {
            trackedComponent.Sync();
        }

        private void MovementLogic()
        {
            if (!this.IsAlive())
            {
                EntityMovement.Velocity = Vector3.zero;
                return;
            }

            if (EntityMovement.IsGrounded 
                && (_inputSettingsGroup.LocomotionType == VrLocomotion.ContinuousHand || _inputSettingsGroup.LocomotionType == VrLocomotion.ContinuousHead))
            {
                Vector3 horizontalVelocity = Vector3.zero;

                var playSmoothMoveActionValue = _playerInput.PlayerSmoothMoveActionValue;
                if ((playSmoothMoveActionValue.x != 0.0f) || (playSmoothMoveActionValue.y != 0.0f))
                {
                    horizontalVelocity.Set(playSmoothMoveActionValue.x, 0.0f, playSmoothMoveActionValue.y);

                    horizontalVelocity = horizontalVelocity * WalkSpeed;

                    Transform locomotionTransform = _playerHead.GameObject.transform;
                    if (_inputSettingsGroup.LocomotionType == VrLocomotion.ContinuousHand)
                    {
                        if (_inputSettingsGroup.PrimaryLocomotionHand == VrHand.Left)
                            locomotionTransform = _playerHandLeft.GameObject.transform;
                        else if (_inputSettingsGroup.PrimaryLocomotionHand == VrHand.Right)
                            locomotionTransform = _playerHandRight.GameObject.transform;
                    }

                    horizontalVelocity = Quaternion.AngleAxis(locomotionTransform.eulerAngles.y, Vector3.up) * horizontalVelocity;
                }

                EntityMovement.Velocity = horizontalVelocity;
            }
        }

        private void SnapRotatePlayer(float rotateAmount)
        {
            var fadeEffect = _cameraManager.ActivePrimaryCamera?.GetCameraEffect<ICameraFade>();
            if (fadeEffect != null || fadeEffect.IsFading)
            {
                fadeEffect.Blink(_snapTurnBlinkDuration, () =>
                {
                    EntityMovement.Rotate(Quaternion.AngleAxis(rotateAmount, Vector3.up));
                });
            }
            else
            {
                EntityMovement.Rotate(Quaternion.AngleAxis(rotateAmount, Vector3.up));
            }
        }
    }
}
