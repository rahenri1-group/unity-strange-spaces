﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Item;
using Game.Item.Inventory;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Player.VR
{
    public class VrPlayerInventorySpawnerBehaviour : BaseInteractable, IWorldInteractable
    {
        [SerializeField] private BackpackItemDefinition _backPackItemDefinition;

        [SerializeField] [TypeRestriction(typeof(IVrItemEquiptor))] private Component[] _itemEquiptorObjs = new Component[0];
        private IVrItemEquiptor[] _itemEquiptors;

        [Inject] private ILogRouter _logger = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_backPackItemDefinition);

            _itemEquiptors = _itemEquiptorObjs.GetComponentArrayAsserted<IVrItemEquiptor>();
        }

        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            var itemEquiptor = interactor as IItemEquiptor;
            if (itemEquiptor != null)
            {
                _backPackItemDefinition.EquiptInstance(itemEquiptor);
            }
            else
            {
                _logger.LogWarning($"{GameObject.name} has been interacted with a non {typeof(IItemEquiptor).Name}");
            }
        }

        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            var playerHoldingInventory = _itemEquiptors.Any(e => e.IsInteracting && e.InteractionTarget is InventoryEquiptableBehaviour);
            if (playerHoldingInventory)
            {
                return Interactions.None;
            }

            return base.AllowedInteractions(interactor);
        }
    }
}
