﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Math;
using Game.Core.Portal;
using Game.Input;
using Game.Player.VR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Game.Portal
{
    public class VrPortalCameraBehaviour : BasePortalCamera
    {
        public Dictionary<string, Shader> ShaderMappings = new Dictionary<string, Shader>();

        public override bool Enabled
        {
            get => base.Enabled;
            set
            {
                base.Enabled = value;

                if (value)
                {
                    UpdateRenderTextures();
                }
            }
        }

        [Inject] private IVrPlayerManager _vrPlayerManager;
        [Inject] private IVrTrackingInput _vrTrackingInput;

        private int _leftEyeTextureShaderId;
        private int _rightEyeTextureShaderId;

        private RenderTexture _leftEyeTexture = null;
        private RenderTexture _rightEyeTexture = null;

        public override void SetupForPortal(IPortal portal, Renderer portalRenderer)
        {
            base.SetupForPortal(portal, portalRenderer);

            _leftEyeTextureShaderId = Shader.PropertyToID("_LeftEyeTexture");
            _rightEyeTextureShaderId = Shader.PropertyToID("_RightEyeTexture");

            if (ShaderMappings.ContainsKey(PortalRenderer.material.shader.name))
            {
                PortalRenderer.material.shader = ShaderMappings[PortalRenderer.material.shader.name];
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_leftEyeTexture != null)
            {
                _leftEyeTexture.Release();
                _leftEyeTexture = null;
            }

            if (_rightEyeTexture != null)
            {
                _rightEyeTexture.Release();
                _rightEyeTexture = null;
            }
        }

        public override void Render()
        {
            base.Render();

            RenderForEye(_leftEyeTexture, Camera.StereoscopicEye.Left);
            RenderForEye(_rightEyeTexture, Camera.StereoscopicEye.Right);
        }

        private void RenderForEye(RenderTexture targetTexture, Camera.StereoscopicEye targetEye)
        {
            UnityCamera.targetTexture = targetTexture;

            var primaryCamera = CameraManager.ActivePrimaryCamera;

            var vrRigRoot = _vrPlayerManager.VrPlayer.VrRigRoot;
            var eyeInput = (targetEye == Camera.StereoscopicEye.Left) ? _vrTrackingInput.EyeLeft : _vrTrackingInput.EyeRight;
            var eyePosition = vrRigRoot.TransformPoint(eyeInput.Position);
            var eyeRotation = vrRigRoot.TransformRotation(eyeInput.Rotation);

            var eyeOffsetVector = MathUtil.ReverseAxisY * Portal.Transform.InverseTransformPoint(eyePosition);

            transform.position = Portal.EndPoint.Transform.TransformPoint(eyeOffsetVector);
            transform.rotation = Portal.EndPoint.Transform.rotation * MathUtil.ReverseAxisY * Quaternion.Inverse(Portal.Transform.rotation) * eyeRotation;

            Vector4 ClipPlane = CameraSpacePlane(
                UnityCamera.worldToCameraMatrix, 
                Portal.EndPoint.Transform.position, 
                Portal.EndPoint.Transform.TransformDirection(Vector3.back), 
                1.0f);

            CopyCameraSettings(primaryCamera);

            UnityCamera.projectionMatrix = primaryCamera.UnityCamera.GetStereoProjectionMatrix(targetEye);
            UnityCamera.projectionMatrix = UnityCamera.CalculateObliqueMatrix(ClipPlane);

            UnityCamera.Render();
        }

        private void UpdateRenderTextures()
        {
            UpdateLeftEyeRenderTexture();
            UpdateRightEyeRenderTexture();

            int aa = QualitySettings.antiAliasing == 0 ? 1 : QualitySettings.antiAliasing;
            _leftEyeTexture.antiAliasing = aa;
            _rightEyeTexture.antiAliasing = aa;
        }

        private void UpdateLeftEyeRenderTexture()
        {
            if (_leftEyeTexture != null && _leftEyeTexture.width == XRSettings.eyeTextureWidth && _leftEyeTexture.height == XRSettings.eyeTextureHeight) return;

            if (_leftEyeTexture != null)
            {
                _leftEyeTexture.Release();
                _leftEyeTexture = null;
            }

            _leftEyeTexture = new RenderTexture(XRSettings.eyeTextureWidth, Screen.height, 32);
            _leftEyeTexture.name = Portal.GameObject.name + "_LeftEyeRenderTexture";
            _leftEyeTexture.vrUsage = VRTextureUsage.OneEye;
            _leftEyeTexture.Create();

            PortalRenderer.material.SetTexture(_leftEyeTextureShaderId, _leftEyeTexture);
        }

        private void UpdateRightEyeRenderTexture()
        {
            if (_rightEyeTexture != null && _rightEyeTexture.width == XRSettings.eyeTextureWidth && _rightEyeTexture.height == XRSettings.eyeTextureHeight) return;

            if (_rightEyeTexture != null)
            {
                _rightEyeTexture.Release();
                _rightEyeTexture = null;
            }

            _rightEyeTexture = new RenderTexture(XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight, 32);
            _rightEyeTexture.name = Portal.GameObject.name + "_RightEyeRenderTexture";
            _leftEyeTexture.vrUsage = VRTextureUsage.OneEye;
            _rightEyeTexture.Create();

            PortalRenderer.material.SetTexture(_rightEyeTextureShaderId, _rightEyeTexture);
        }
    }
}
