﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Portal;
using Game.Core.Render.Camera;
using Game.Core.Resource;
using Game.Core.Space;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Portal
{
    [Dependency(
        contract: typeof(ICameraFactory),
        lifetime: Lifetime.Singleton)]
    public class VrPortalCameraFactory : BaseCameraFactory<IPortalCamera>
    {
        public override CameraFactoryConfig FactoryConfig => Config;
        public VrPortalCameraFactoryConfig Config = new VrPortalCameraFactoryConfig();

        private readonly ILogRouter _logger;

        private Dictionary<string, Shader> _replacementShaderMappings;

        public VrPortalCameraFactory(
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            ISpaceManager spaceManager)
            : base(gameObjectResourceManager, spaceManager)
        {
            _logger = logger;
        }

        public override UniTask InitializeCameraFactory()
        {
            _replacementShaderMappings = new Dictionary<string, Shader>();

            foreach (var pair in Config.ShaderMappings)
            {
                var shader = Shader.Find(pair.Value);

                if (shader != null)
                {
                    _replacementShaderMappings.Add(pair.Key, shader);
                }
                else
                {
                    _logger.LogError($"Unknown shader {pair.Value}");
                }
            }

            return base.InitializeCameraFactory();
        }

        /// <inheritdoc/>
        public async override UniTask<ICamera> CreateCamera()
        {
            var camera = await CreateEmptyCamera();
            var portalCamera = camera.gameObject.AddComponent<VrPortalCameraBehaviour>();
            foreach (var pair in _replacementShaderMappings)
            {
                portalCamera.ShaderMappings.Add(pair.Key, pair.Value);
            }
            return portalCamera;
        }
    }
}
