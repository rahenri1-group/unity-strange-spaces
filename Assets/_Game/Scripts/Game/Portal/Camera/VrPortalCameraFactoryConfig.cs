﻿using Game.Core.Render.Camera;
using System;
using System.Collections.Generic;

namespace Game.Portal
{
    [Serializable]
    public class VrPortalCameraFactoryConfig : CameraFactoryConfig
    {
        public Dictionary<string, string> ShaderMappings = new Dictionary<string, string>();
    }
}
