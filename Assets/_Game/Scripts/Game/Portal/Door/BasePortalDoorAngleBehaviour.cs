﻿using Game.Core;
using Game.Core.Physics;
using Game.Core.Portal;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Portal
{
    public abstract class BasePortalDoorAngleBehaviour : MonoBehaviour
    {
        protected float DoorsCombinedAngle => _doors.Sum(d => d.IsClosed ? 0f : d.Angle);

        [SerializeField] [TypeRestriction(typeof(IHinge))] private Component[] _doorObjs = null;
        private IHinge[] _doors;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalTriggerObj = null;
        private ITriggerable _portalTrigger;

        private IEnumerator _updateAudioModifiersCoroutine;

        protected virtual void Awake()
        {
            Assert.IsNotNull(_doorObjs);
            Assert.IsTrue(_doorObjs.Length > 0);
            _doors = _doorObjs.GetComponentArrayAsserted<IHinge>();

            Assert.IsNotNull(_portalTriggerObj);

            _portalTrigger = _portalTriggerObj.GetComponent<ITriggerable>();

            _updateAudioModifiersCoroutine = null;
        }

        private void OnEnable()
        {
            _portalTrigger.TriggerEnter += OnPortalEnter;
            foreach (var door in _doors)
            {
                door.Open += OnDoorOpen;
                door.Close += OnDoorClose;
            }
        }

        private void OnDisable()
        {
            _portalTrigger.TriggerEnter -= OnPortalEnter;
            foreach (var door in _doors)
            {
                door.Open -= OnDoorOpen;
                door.Close -= OnDoorClose;
            }

            if (_updateAudioModifiersCoroutine != null)
            {
                StopCoroutine(_updateAudioModifiersCoroutine);
                _updateAudioModifiersCoroutine = null;
            }
        }

        protected abstract void OnDoorAngleChanged();

        protected abstract void OnPortalDetected(IPortal portal);
        

        private void OnDoorOpen(IHinge sender, HingeEventArgs args)
        {
            if (_updateAudioModifiersCoroutine == null)
            {
                _updateAudioModifiersCoroutine = UpdateAudioModifiers();
                StartCoroutine(_updateAudioModifiersCoroutine);
            }
        }

        private void OnDoorClose(IHinge sender, HingeEventArgs args)
        {
            bool allDoorsClosed = true;
            foreach (var door in _doors)
            {
                if (!door.IsClosed)
                {
                    allDoorsClosed = false;
                    break;
                }
            }

            if (allDoorsClosed)
            {
                if (_updateAudioModifiersCoroutine != null)
                {
                    StopCoroutine(_updateAudioModifiersCoroutine);
                    _updateAudioModifiersCoroutine = null;
                }

                OnDoorAngleChanged();
            }
        }

        private void OnPortalEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            var portal = args.Collider.GetComponentInParent<IPortal>();
            if (portal != null)
            {
                OnPortalDetected(portal);
            }
        }

        private IEnumerator UpdateAudioModifiers()
        {
            YieldInstruction wait = new WaitForEndOfFrame();

            while (true)
            {
                yield return wait;

                OnDoorAngleChanged();
            }
        }
    }
}
