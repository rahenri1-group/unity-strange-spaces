﻿using Game.Core;
using Game.Core.Audio;
using Game.Core.Portal;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Portal
{
    public class PortalDoorAudioModifierDefinitionBehaviour : BasePortalDoorAngleBehaviour
    {
        [SerializeField] private FloatReadonlyReference _maxCombinedAngle = null;

        [SerializeReference] [SelectType(typeof(IAudioModifierDefinition))] private IAudioModifierDefinition[] _audioModifiers = new IAudioModifierDefinition[0];

        private HashSet<IPortalAudioLink> _portalAudioLinks;       

        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(_maxCombinedAngle > 0f);

            _portalAudioLinks = new HashSet<IPortalAudioLink>();
        }

        protected override void OnPortalDetected(IPortal portal)
        {
            var portalAudioLink = portal.GetComponent<IPortalAudioLink>();
            if (portalAudioLink == null) return;

            if (!_portalAudioLinks.Contains(portalAudioLink))
            {
                _portalAudioLinks.Add(portalAudioLink);

                portalAudioLink.AudioModifiers = _audioModifiers;
            }
        }

        protected override void OnDoorAngleChanged()
        {
            float lerp = 1f - Mathf.Clamp01(DoorsCombinedAngle / _maxCombinedAngle);

            foreach (var audioModifier in _audioModifiers)
            {
                audioModifier.Enabled = lerp > 0f;
                audioModifier.ModifierLerp = lerp;
            }
        }
    }
}
