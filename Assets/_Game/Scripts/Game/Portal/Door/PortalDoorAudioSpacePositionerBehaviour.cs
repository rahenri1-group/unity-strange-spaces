﻿using Game.Core;
using Game.Core.Audio;
using Game.Core.Physics;
using Game.Core.Portal;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Portal
{
    public class PortalDoorAudioSpacePositionerBehaviour : AudioSpacePositionerBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalTriggerObj = null;
        private ITriggerable _portalTrigger;

        private HashSet<IPortalAudioLink> _portalAudioLinks;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_portalTriggerObj);
            
            _portalTrigger = _portalTriggerObj.GetComponent<ITriggerable>();

            _portalAudioLinks = new HashSet<IPortalAudioLink>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _portalTrigger.TriggerEnter += OnPortalEnter;
        }

        protected override void OnDisable()
        {
            _portalTrigger.TriggerEnter -= OnPortalEnter;
        }

        protected override bool ShouldApplyPathAudioLink(IPortalAudioLink link)
        {
            return base.ShouldApplyPathAudioLink(link) && !_portalAudioLinks.Contains(link);
        }

        private void OnPortalEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            var portalAudioLink = args.Collider.GetComponentInParent<IPortalAudioLink>();
            if (portalAudioLink == null) return;

            if (!_portalAudioLinks.Contains(portalAudioLink))
            {
                _portalAudioLinks.Add(portalAudioLink);
            }
        }
    }
}
