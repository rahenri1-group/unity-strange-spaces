﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Physics;
using Game.Core.Portal;
using Game.Core.Space;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Portal
{
    public class PortalDoorRendererBehaviour : InjectedBehaviour
    {
        private class PortalData
        {
            public IPortal Portal;
            public ISpaceData Space;
            public IPortalRenderer Renderer;
            public IPortalLighting Lighting;
        }

        [Inject] private IEventBus _eventBus = null;

        [SerializeField] [TypeRestriction(typeof(IHinge))] private Component _doorObj = null;
        private IHinge _door;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalTriggerObj = null;
        private ITriggerable _portalTrigger;

        private List<PortalData> _portals;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_doorObj);
            Assert.IsNotNull(_portalTriggerObj);

            _door = _doorObj.GetComponent<IHinge>();
            _portalTrigger = _portalTriggerObj.GetComponent<ITriggerable>();

            _portals = new List<PortalData>();
        }

        private void OnEnable()
        {
            _portalTrigger.TriggerEnter += OnPortalEnter;
            _door.Open += OnDoorOpen;
            _door.Close += OnDoorClose;

            _eventBus.Subscribe<SpaceUnloadedEvent>(OnSpaceUnloaded);
        }

        private void OnDisable()
        {
            _portalTrigger.TriggerEnter -= OnPortalEnter;
            _door.Open -= OnDoorOpen;
            _door.Close -= OnDoorClose;

            _eventBus.Unsubscribe<SpaceUnloadedEvent>(OnSpaceUnloaded);
        }

        private void OnDoorOpen(IHinge sender, HingeEventArgs args)
        {
            UpdatePortalRendering();
        }

        private void OnDoorClose(IHinge sender, HingeEventArgs args)
        {
            UpdatePortalRendering();
        }

        private void OnPortalEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            var portal = args.Collider.GetComponentInParent<IPortal>();
            if (portal == null) return;

            if (!_portals.Any(p => p.Portal == portal))
            {
                _portals.Add(
                    new PortalData
                    {
                        Portal = portal,
                        Space = portal.Space,
                        Renderer = portal.GetComponentInChildren<IPortalRenderer>(),
                        Lighting = portal.GetComponentInChildren<IPortalLighting>()
                    });
            }

            UpdatePortalRendering();
        }

        private void OnSpaceUnloaded(SpaceUnloadedEvent eventData)
        {
            _portals.RemoveAll(p => p.Space == eventData.SpaceData);
        }

        private void UpdatePortalRendering()
        {
            foreach (var portalData in _portals)
            {
                if (portalData.Renderer != null)
                {
                    portalData.Renderer.RenderingEnabled = !_door.IsClosed;
                }

                if (portalData.Lighting != null)
                {
                    foreach (var light in portalData.Lighting.PortalLights)
                    {
                        light.ShadowingEnabled = !_door.IsClosed;
                    }
                }
            }
        }
    }
}
