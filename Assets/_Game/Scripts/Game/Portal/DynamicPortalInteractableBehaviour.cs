﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Portal;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Portal
{
    public class DynamicPortalInteractableBehaviour : InjectedBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IDynamicPortal))] private Component _portalObj = null;
        private IDynamicPortal _portal;

        [SerializeField] private string _destinationPortalId = null;
        [SerializeField] private string _destinationPortalSpaceId = null;

        [Inject] private IDynamicPortalManager _portalManager = null;

        private IInteractable _interactable;

        private IDynamicPortalConnection _connection;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsFalse(string.IsNullOrEmpty(_destinationPortalId));
            Assert.IsFalse(string.IsNullOrEmpty(_destinationPortalSpaceId));

            _interactable = this.GetComponentAsserted<IInteractable>();
            _portal = _portalObj.GetComponentAsserted<IDynamicPortal>();

            _connection = new DynamicPortalConnection(_portal, Guid.Parse(_destinationPortalId), Guid.Parse(_destinationPortalSpaceId));
        }

        private void OnEnable()
        {
            _interactable.InteractBegin += OnInteractBegin;
        }

        private void OnDisable()
        {
            _interactable.InteractBegin -= OnInteractBegin;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            if (!_portal.IsOpen)
            {
                _portalManager.AttemptOpenDynamicPortalConnection(_connection);
            }
            else
            {
                _portalManager.CloseDynamicPortalConnection(_connection);
            }
        }
    }
}
