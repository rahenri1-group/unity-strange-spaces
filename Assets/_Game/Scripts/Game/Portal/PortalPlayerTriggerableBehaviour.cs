﻿using Game.Core;
using Game.Core.Physics;
using Game.Core.Portal;
using Game.Player;
using UnityEngine;

namespace Game.Portal
{
    [RequireComponent(typeof(IPortal))]
    public class PortalPlayerTriggerableBehaviour : MonoBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalOpenTriggerObj = null;
        private ITriggerable _portalOpenTrigger;

        [SerializeField] [TypeRestriction(typeof(ITriggerable))] private Component _portalCloseTriggerObj = null;
        private ITriggerable _portalCloseTrigger;

        private IPortal _portal;

        private void Awake()
        {
            _portalOpenTrigger = (_portalOpenTriggerObj != null) ? _portalOpenTriggerObj.GetComponent<ITriggerable>() : null;
            _portalCloseTrigger = (_portalCloseTriggerObj != null) ? _portalCloseTriggerObj.GetComponent<ITriggerable>() : null;

            _portal = GetComponent<IPortal>();
        }

        private void OnEnable()
        {
            if (_portalOpenTrigger != null)
                _portalOpenTrigger.TriggerEnter += OnOpenTriggerEnter;

            if (_portalCloseTrigger != null)
                _portalCloseTrigger.TriggerExit += OnCloseTriggerEnter;
        }

        private void OnDisable()
        {
            if (_portalOpenTrigger != null)
                _portalOpenTrigger.TriggerEnter -= OnOpenTriggerEnter;

            if (_portalCloseTrigger != null)
                _portalCloseTrigger.TriggerExit -= OnCloseTriggerEnter;
        }

        private void OnOpenTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            if (!_portal.HasConnection || _portal.IsOpen) return;

            var player = args.Collider.GetComponentInParent<IPlayer>();
            if (player == null) return;

            _portal.AttemptOpen();
        }

        private void OnCloseTriggerEnter(ITriggerable sender, TriggerableEventArgs args)
        {
            if (!_portal.HasConnection || !_portal.IsOpen || args.Collider == null) return;

            var player = args.Collider.GetComponentInParent<IPlayer>();
            if (player == null) return;

            _portal.Close();
        }
    }
}
