﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Core.Portal;
using UnityEngine;

namespace Game.Portal
{
    public class StaticPortalInteractableBehaviour : InjectedBehaviour
    {
        [SerializeField] [TypeRestriction(typeof(IStaticPortal))] private Component _portalObj = null;
        private IStaticPortal _portal;

        [Inject] private IStaticPortalManager _portalManager = null;

        private IInteractable _interactable;

        protected override void Awake()
        {
            base.Awake();

            _interactable = this.GetComponentAsserted<IInteractable>();
            _portal = _portalObj.GetComponentAsserted<IStaticPortal>();
        }

        private void OnEnable()
        {
            _interactable.InteractBegin += OnInteractBegin;
        }

        private void OnDisable()
        {
            _interactable.InteractBegin -= OnInteractBegin;
        }

        private void OnInteractBegin(IInteractable sender, IInteractor interactor)
        {
            if (!_portal.IsOpen)
            {
                _portalManager.AttemptOpenStaticPortalConnection(_portal.PortalConnection);
            }
            else
            {
                _portalManager.CloseStaticPortalConnection(_portal.PortalConnection);
            }
        }
    }
}
