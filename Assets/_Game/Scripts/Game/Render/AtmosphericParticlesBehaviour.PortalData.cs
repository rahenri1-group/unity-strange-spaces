﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Portal;
using UnityEngine;

namespace Game.Render
{
    public partial class AtmosphericParticlesBehaviour
    {
        private class PortalData
        {
            public readonly IPortal Portal;
            public readonly IPortalRenderer PortalRenderer;

            private readonly AtmosphericParticlesBehaviour _atmosphericParticles;

            private GameObject _shadowParticlesGameObject;
            private PortalShadowParticleBehaviour _shadowParticles;

            public PortalData(AtmosphericParticlesBehaviour atmosphericParticles, IPortal portal, IPortalRenderer portalRenderer)
            {
                _atmosphericParticles = atmosphericParticles;
                Portal = portal;
                PortalRenderer = portalRenderer;

                _shadowParticlesGameObject = null;
                _shadowParticles = null;
            }

            public void CreateShadowForPortalIfNecessary()
            {
                if (_shadowParticlesGameObject && _shadowParticles)
                {
                    _shadowParticles.enabled = true;
                }
                else
                {
                    _shadowParticlesGameObject = new GameObject($"{_atmosphericParticles.ParticleSystem.gameObject.name.PrependIfMissing("@")}-ParticleShadow");
                    _shadowParticles = _shadowParticlesGameObject.AddComponent<PortalShadowParticleBehaviour>();

                    _shadowParticles.Init(Portal, _atmosphericParticles.ParticleSystem);

                    UniTask.Create(async () =>
                    {
                        await _atmosphericParticles.SpaceManager.MoveObjectToSpaceAsync(_shadowParticlesGameObject, Portal.EndPoint.Space);
                    }).Forget();
                }
            }

            public void DestroyShadowForPortal()
            {
                if (_shadowParticlesGameObject)
                {
                    Destroy(_shadowParticlesGameObject);
                }

                _shadowParticlesGameObject = null;
                _shadowParticles = null;
            }
        }
    }
}