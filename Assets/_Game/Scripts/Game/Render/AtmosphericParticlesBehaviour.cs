﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Render
{
    /// <summary>
    /// Controller for the atmospheric particles in a space
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public partial class AtmosphericParticlesBehaviour : SpaceInjectedBehaviour
    {
        [Inject] private IPlayerManager _playerManager = null;

        protected ParticleSystem ParticleSystem { get; private set; }

        private HashSet<PortalData> _portalDatas;

        private IEnumerator _playerTrackingCoroutine;

        protected override void Awake()
        {
            base.Awake();

            ParticleSystem = GetComponent<ParticleSystem>();
            var emission = ParticleSystem.emission;
            emission.enabled = false;

            _playerTrackingCoroutine = null;
        }

        protected override void OnSpaceLoaded()
        {
            base.OnSpaceLoaded();
            
            _portalDatas = new HashSet<PortalData>();

            var spacePortals = SpaceManager.FindLoadedInSpace<IPortal>(CurrentSpace);
            foreach (var portal in spacePortals)
            {
                var portalRenderer = portal.GetComponent<IPortalRenderer>();
                var portalTransporter = portal.GetComponent<IPortalTransporter>();
                if (portalRenderer != null && portalTransporter != null)
                {
                    _portalDatas.Add(new PortalData(this, portal, portalRenderer));
                }
            }

            if (_playerManager.Player != null && SpaceManager.GetEntitySpace(_playerManager.Player) == CurrentSpace)
            {
                StartPlayerTracking();
            }
        }

        protected override void OnSpaceUnloading()
        {
            base.OnSpaceUnloading();

            StopPlayerTracking();

            foreach (var portalData in _portalDatas)
            {
                portalData.DestroyShadowForPortal();
            }

            _portalDatas.Clear();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            EventBus.Subscribe<EntityCreateEvent>(OnEntityCreate);
            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            EventBus.Unsubscribe<EntityCreateEvent>(OnEntityCreate);
            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);

            if (_playerTrackingCoroutine != null)
            {
                StopCoroutine(_playerTrackingCoroutine);
                _playerTrackingCoroutine = null;
            }
        }

        private void OnEntityCreate(EntityCreateEvent eventArgs)
        {
            if (eventArgs.Entity is IPlayer)
            {
                if (SpaceManager.GetEntitySpace(eventArgs.Entity) == CurrentSpace)
                {
                    StartPlayerTracking();
                }
                else
                {
                    StopPlayerTracking();
                }
            }
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (eventArgs.Entity is IPlayer) {

                if (eventArgs.NewSpace == CurrentSpace)
                {
                    StartPlayerTracking();
                }
                else
                {
                    StopPlayerTracking();
                }
            }
        }

        private void StopPlayerTracking()
        {
            var emission = ParticleSystem.emission;
            emission.enabled = false;

            if (_playerTrackingCoroutine != null)
            {
                StopCoroutine(_playerTrackingCoroutine);
                _playerTrackingCoroutine = null;
            }
        }

        private void StartPlayerTracking()
        {
            var emission = ParticleSystem.emission;
            emission.enabled = true;

            if (_playerTrackingCoroutine == null)
            {
                _playerTrackingCoroutine = PlayerTracking();
                StartCoroutine(_playerTrackingCoroutine);
            }
        }

        private IEnumerator PlayerTracking()
        {
            YieldInstruction wait = null;

            while (true)
            {
                if (_playerManager.Player != null)
                {
                    transform.position = _playerManager.Player.HeadPosition;
                }

                foreach (var portalData in _portalDatas)
                {
                    if (portalData.Portal.IsOpen && portalData.PortalRenderer.RenderingEnabled)
                    {
                        portalData.CreateShadowForPortalIfNecessary();
                    }
                }

                yield return wait;
            }
        }
    }
}