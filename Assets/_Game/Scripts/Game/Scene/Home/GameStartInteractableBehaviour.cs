﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Interaction;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Events;
using Game.World;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Scene.Home
{
    public class GameStartInteractableBehaviour : BaseInteractable, IWorldInteractable
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISpaceLoader _spaceLoader = null;
        [Inject] private IWorldManager _worldManager = null;

        [SerializeField] [TypeRestriction(typeof(IDynamicPortal))] private Component _worldEntrancePortalObj = null;
        private IDynamicPortal _worldEntrancePortal;

        [SerializeField] [TypeRestriction(typeof(IObjectHider))] private Component _doorFrameHiderObj = null;
        private IObjectHider _doorFrameHider;

        [SerializeField] [TypeRestriction(typeof(IObjectHider))] private Component _imposterDoorHiderObj = null;
        private IObjectHider _imposterDoorHider;

        private IObjectHider _interactableHider;

        private bool _readyToCreateWorld;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_worldEntrancePortalObj);
            _worldEntrancePortal = _worldEntrancePortalObj.GetComponentAsserted<IDynamicPortal>();

            Assert.IsNotNull(_doorFrameHiderObj);
            _doorFrameHider = _doorFrameHiderObj.GetComponentAsserted<IObjectHider>();

            Assert.IsNotNull(_imposterDoorHiderObj);
            _imposterDoorHider = _imposterDoorHiderObj.GetComponentAsserted<IObjectHider>();

            _interactableHider = this.GetComponentAsserted<IObjectHider>();

            _readyToCreateWorld = true;
        }

        private void OnEnable()
        {
            _eventBus.Subscribe<WorldUnloadedEvent>(OnWorldUnloaded);
        }

        private void OnDisable()
        {
            _eventBus.Unsubscribe<WorldUnloadedEvent>(OnWorldUnloaded);
        }

        private void OnWorldUnloaded(WorldUnloadedEvent eventArgs)
        {
            _readyToCreateWorld = true;
            _interactableHider.ShowImmediate();

            _doorFrameHider.HideImmediate();
            _imposterDoorHider.HideImmediate();
        }

        public override Interactions AllowedInteractions(IInteractor interactor)
        {
            if (_readyToCreateWorld)
            {
                return base.AllowedInteractions(interactor);
            }

            return Interactions.None;
        }

        public override void OnInteractBegin(IInteractor interactor)
        {
            base.OnInteractBegin(interactor);

            _readyToCreateWorld = false;

            UniTask.Create(async () =>
            {
                await _worldManager.CreateNewWorld();

                await UniTask.WhenAll(
                    _spaceLoader.LoadSpace(_worldManager.CurrentWorld.WorldEntranceSpace),
                    _interactableHider.HideAnimated(),
                    _doorFrameHider.ShowAnimated(),
                    _imposterDoorHider.ShowAnimated());

                await _worldManager.ConnectPortalToWorldEntrance(_worldEntrancePortal);
                await _imposterDoorHider.HideAnimated();

            }).Forget();
        }
    }
}
