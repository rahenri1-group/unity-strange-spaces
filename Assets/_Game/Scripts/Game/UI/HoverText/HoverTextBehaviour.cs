﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Input;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI
{
    public class HoverTextBehaviour : InjectedBehaviour, IHoverText
    {
        [Inject] private IUiInputGroup _uiInput = null;

        [SerializeField] private TMP_Text _hoverText = null;

        private RectTransform _rectTransform;

        private IEnumerator _cursorFollowCoroutine;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_hoverText);

            _rectTransform = GetComponent<RectTransform>();

            _uiInput.InputEnabled = true;

            _cursorFollowCoroutine = null;
        }

        public void ShowText(string text)
        {
            _hoverText.text = text;

            if (_cursorFollowCoroutine == null)
            {
                _cursorFollowCoroutine = CursorFollow();
                StartCoroutine(_cursorFollowCoroutine);
            }
        }

        public void Hide()
        {
            _hoverText.text = string.Empty;

            if (_cursorFollowCoroutine != null)
            {
                StopCoroutine(_cursorFollowCoroutine);
                _cursorFollowCoroutine = null;
            }
        }

        private IEnumerator CursorFollow()
        {
            YieldInstruction wait = null;

            while (true)
            {
                var cursorPosition = _uiInput.PointerValue;

                RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    (RectTransform)_rectTransform.parent,
                    cursorPosition,
                    null, // screen space overlay canvases pass null for the camera
                    out var localCursorPosition);

                _rectTransform.anchoredPosition = localCursorPosition;

                yield return wait;
            }
        }
    }
}
