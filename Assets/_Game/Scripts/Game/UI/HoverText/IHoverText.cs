﻿namespace Game.UI
{
    public interface IHoverText
    {
        void ShowText(string text);

        void Hide();
    }
}
