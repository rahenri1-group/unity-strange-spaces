﻿using Cysharp.Threading.Tasks;
using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Core.Resource;
using Game.Item;
using Game.Player;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Game.UI.Inventory
{
    public class InventoryGridCellBehaviour : CursorHoverable
    {
        public IItemDefinition ItemDefinition { get; private set; }

        [Inject] private IAssetResourceManager _assetResourceManager = null;
        [Inject] private IPlayerManager _playerManager = null;

        [SerializeField] private Color _usableItemHighlightColor = Color.white;

        [SerializeField] private Sprite _fallbackItemSprite = null;

        [SerializeField] private Button _itemButton = null;
        [SerializeField] private Image _itemImage = null;
        [SerializeField] private TMP_Text _itemCountText = null;

        private IPlayerConsumableItemDefinition _consumableItemDefinition;

        private Sprite _loadedItemSprite;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_fallbackItemSprite);
            Assert.IsNotNull(_itemButton);
            Assert.IsNotNull(_itemImage);
            Assert.IsNotNull(_itemCountText);

            _itemImage.preserveAspect = true;

            ItemDefinition = null;
            _consumableItemDefinition = null;

            _loadedItemSprite = null;

            ClearCell();

            _itemButton.onClick.AddListener(OnButtonClick);
        }

        private void OnDestroy()
        {
            if (_loadedItemSprite != null)
            {
                _assetResourceManager.ReleaseAsset(_loadedItemSprite);
                _loadedItemSprite = null;
            }

            _itemButton.onClick.RemoveListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            if (_consumableItemDefinition == null) return;

            _consumableItemDefinition.Consume(_playerManager.Player);
        }

        public async UniTask DisplayItemForInventory(IInventory inventory, IItemDefinition itemDefinition, CancellationToken cancellationToken)
        {
            ItemDefinition = itemDefinition;
            var playerItemDefinition = ItemDefinition as IPlayerItemDefinition;
            _consumableItemDefinition = ItemDefinition as IPlayerConsumableItemDefinition;

            var colors = _itemButton.colors;
            colors.highlightedColor = (_consumableItemDefinition != null) ? _usableItemHighlightColor : Color.clear;
            _itemButton.colors = colors;

            _itemButton.interactable = _consumableItemDefinition != null;

            var quantity = inventory.DefinitionQuantity(ItemDefinition);
            if (quantity > 1)
            {
                _itemCountText.enabled = true;
                _itemCountText.text = $"x {quantity}";
            }
            else
            {
                _itemCountText.enabled = false;
            }

            _itemImage.enabled = false;

            if (playerItemDefinition != null && !string.IsNullOrEmpty(playerItemDefinition.InventorySpriteIconAddress))
            {
                _loadedItemSprite = await _assetResourceManager.LoadAsset<Sprite>(playerItemDefinition.InventorySpriteIconAddress);
                _itemImage.sprite = _loadedItemSprite;
            }
            else
            {
                _itemImage.sprite = _fallbackItemSprite;
            }


            if (cancellationToken.IsCancellationRequested)
            {
                if (_loadedItemSprite != null)
                {
                    _assetResourceManager.ReleaseAsset(_loadedItemSprite);
                    _loadedItemSprite = null;
                }
            }
            else
            {
                _itemImage.enabled = true;
            }
        }

        public void ClearCell()
        {
            var colors = _itemButton.colors;
            colors.highlightedColor = Color.clear;
            _itemButton.colors = colors;

            _itemButton.interactable = false;

            _itemImage.sprite = null;

            if (_loadedItemSprite != null)
            {
                _assetResourceManager.ReleaseAsset(_loadedItemSprite);
                _loadedItemSprite = null;
            }

            _itemImage.enabled = false;
            _itemCountText.enabled = false;

            ItemDefinition = null;
            _consumableItemDefinition = null;
        }
    }
}
