﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Item;
using Game.Core.UI;
using Game.Player;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.Inventory
{
    public class InventoryScreenBehaviour : BaseUiScreen
    {
        [Inject] private ILogRouter _logger = null;
        [Inject] private IPlayerManager _playerManager = null;

        [SerializeField] [TypeRestriction(typeof(IHoverText))] private Component _hoverTextObj = null;
        private IHoverText _hoverText;

        [SerializeField] private InventoryGridCellBehaviour[] _gridCells = new InventoryGridCellBehaviour[0];

        private IInventory _displayedInventory;

        private CancellationTokenSource _gridCellShowCts;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_hoverTextObj);
            Assert.IsTrue(_gridCells.Length > 0);

            _hoverText = _hoverTextObj.GetComponentAsserted<IHoverText>();

            _displayedInventory = null;
            _gridCellShowCts = null;
        }

        private void OnDestroy()
        {
            if (_gridCellShowCts != null)
            {
                _gridCellShowCts.CancelAndDispose();
                _gridCellShowCts = null;
            }
        }

        public override void Show()
        {
            base.Show();

            _displayedInventory = _playerManager.Player.Inventory;

            RefreshInventoryDisplay();

            foreach (var gridCell in _gridCells)
            {
                gridCell.CursorEnter += OnCursorEnterGridCell;
                gridCell.CursorExit += OnCursorExitGridCell;
            }

            _displayedInventory.InventoryContentsUpdated += OnInventoryContentsUpdated;
        }

        public override void Hide()
        {
            base.Hide();

            if (_gridCellShowCts != null)
            {
                _gridCellShowCts.CancelAndDispose();
                _gridCellShowCts = null;
            }

            if (_displayedInventory != null)
            {
                _displayedInventory.InventoryContentsUpdated -= OnInventoryContentsUpdated;
                _displayedInventory = null;
            }

            _hoverText.Hide();

            foreach (var gridCell in _gridCells)
            {
                gridCell.ClearCell();

                gridCell.CursorEnter -= OnCursorEnterGridCell;
                gridCell.CursorExit -= OnCursorExitGridCell;
            }
        }

        private void OnInventoryContentsUpdated()
        {
            RefreshInventoryDisplay();
        }

        private void OnCursorEnterGridCell(ICursorHoverable cursorHoverable)
        {
            var gridCell = cursorHoverable.GetComponentInParent<InventoryGridCellBehaviour>();

            if (gridCell.ItemDefinition != null)
            {
                _hoverText.ShowText(gridCell.ItemDefinition.Name);
            }
        }

        private void OnCursorExitGridCell(ICursorHoverable cursorHoverable)
        {
            _hoverText.Hide();
        }

        private void RefreshInventoryDisplay()
        {
            _hoverText.Hide();

            if (_gridCellShowCts != null)
            {
                _gridCellShowCts.CancelAndDispose();         
            }

            _gridCellShowCts = new CancellationTokenSource();

            var itemDefinitions = _displayedInventory.AllDefinitions();
            
            if (itemDefinitions.Length > _gridCells.Length)
            {
                _logger.LogWarning($"Player inventory has more items than can be displayed. Only first {_gridCells.Length} will be shown");
            }

            for (int i = 0; i < _gridCells.Length; i++)
            {
                var gridCell = _gridCells[i];

                if (i < itemDefinitions.Length)
                {
                    gridCell.DisplayItemForInventory(_displayedInventory, itemDefinitions[i], _gridCellShowCts.Token).Forget();
                }
                else
                {
                    gridCell.ClearCell();
                }
            }
        }
    }
}
