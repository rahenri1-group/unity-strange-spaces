using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.UI;
using Game.Input;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.Inventory
{
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class InventoryUiManager : BaseModule
    {
        public override string ModuleName => "Inventory UI Manager";

        public override ModuleConfig ModuleConfig => Config;
        public InventoryUiManagerConfig Config = new InventoryUiManagerConfig();

        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IPlayerInventoryInput _playerInventoryInput;
        private readonly IPlayerManager _playerManager;
        private readonly IUiManager _uiManager;

        private InventoryScreenBehaviour _inventoryScreen;
        private GameObject _inventoryScreenGameObject;

        public InventoryUiManager(
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            IPlayerInventoryInput playerInventoryInput,
            IPlayerManager playerManager,
            IUiManager uiManager)
            : base(logger)
        {
            _gameObjectResourceManager = gameObjectResourceManager;
            _playerInventoryInput = playerInventoryInput;
            _playerManager = playerManager;
            _uiManager = uiManager;

            _inventoryScreen = null;
            _inventoryScreenGameObject = null;
        }

        public async override UniTask Initialize()
        {
            Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(Config.InventoryUiAssetKey));

            _playerInventoryInput.InputEnabled = true;

            var inventoryScreen = await _gameObjectResourceManager.InstantiateAsync<InventoryScreenBehaviour>(Config.InventoryUiAssetKey, null, Vector3.zero);

            _inventoryScreen = inventoryScreen.Component;
            _inventoryScreenGameObject = inventoryScreen.GameObject;
            _inventoryScreenGameObject.name = "@InventoryScreen";

            _playerInventoryInput.PlayerInventoryToggleTriggered += OnPlayerInventoryToggle;

            await base.Initialize();
        }

        public override UniTask Shutdown()
        {
            if (_inventoryScreen != null)
            {
                _gameObjectResourceManager.DestroyObject(_inventoryScreenGameObject);

                _inventoryScreen = null;
                _inventoryScreenGameObject = null;
            }

            _playerInventoryInput.PlayerInventoryToggleTriggered -= OnPlayerInventoryToggle;

            return base.Shutdown();
        }

        private void OnPlayerInventoryToggle(InputContext context, bool inventoryTriggered)
        {
            if (!inventoryTriggered) return;
            if (_playerManager.Player == null) return;

            if (_inventoryScreen.Visible)
            {
                _uiManager.HideScreenSpaceUiScreen(_inventoryScreen);
            }
            else
            {
                _uiManager.ShowScreenSpaceUiScreen(_inventoryScreen);
            }
        }
    }
}
