﻿using Game.Core;

namespace Game.UI.Inventory
{
    public class InventoryUiManagerConfig : ModuleConfig
    {
        public string InventoryUiAssetKey = string.Empty;
    }
}
