﻿using Game.Core.Audio;
using Game.Core.Interaction;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuAudioBehaviour : BaseModifiableAudioSource
    {
        public override AudioSource UnityAudio => _unityAudio;
        [SerializeField] private AudioSource _unityAudio = null;

        [SerializeField] private GameObject _uiRoot = null;

        [SerializeField] private RandomAudioClipCollection _buttonClickAudioClips = null;
        [SerializeField] private RandomAudioClipCollection _buttonHoverAudioClips = null;

        private ILaserInteractable[] _uiButtons;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_uiRoot);

            _uiButtons = _uiRoot.GetComponentsInChildren<ILaserInteractable>(true);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            foreach (var button in _uiButtons)
            {
                button.InteractBegin += OnButtonClick;
                button.HoverBegin += OnButtonHover;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            foreach (var button in _uiButtons)
            {
                button.InteractBegin -= OnButtonClick;
                button.HoverBegin -= OnButtonHover;
            }
        }

        private void OnButtonClick(IInteractable sender, IInteractor interactor)
        {
            transform.position = sender.GameObject.transform.position;

            PlayClip(_buttonClickAudioClips.GetNextAudioClip());
        }

        private void OnButtonHover(IInteractable sender, IInteractor interactor)
        {
            if (IsPlaying) return;

            transform.position = sender.GameObject.transform.position;

            PlayClip(_buttonHoverAudioClips.GetNextAudioClip());
        }
    }
}