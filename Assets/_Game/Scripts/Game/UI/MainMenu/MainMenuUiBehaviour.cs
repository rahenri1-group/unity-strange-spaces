﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Interaction;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    /// <summary>
    /// Controller for the main menu ui
    /// </summary>
    public class MainMenuUiBehaviour : BaseDynamicEntity<EntitySerializedData>
    {
        [SerializeField] private TMP_Text _headerText = null;

        [SerializeField] private BaseMainMenuScreen _rootScreen = null;

        [SerializeField] [TypeRestriction(typeof(ILaserInteractable))] private Component _backButtonObj = null;
        private ILaserInteractable _backButton;

        [Inject] private ILogRouter _logger = null;

        private Stack<BaseMainMenuScreen> _screenStack;

        /// <inheritdoc/>
        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_headerText);
            Assert.IsNotNull(_rootScreen);
            Assert.IsNotNull(_backButtonObj);

            _backButton = _backButtonObj.GetComponentAsserted<ILaserInteractable>();

            _screenStack = new Stack<BaseMainMenuScreen>();
        }

        private void OnEnable()
        {
            _backButton.InteractBegin += OnBackButtonClicked;
        }

        private void OnDisable()
        {
            _backButton.InteractBegin -= OnBackButtonClicked;
        }

        public void OnShow()
        {
            ShowScreen(_rootScreen);
        }

        public void OnHide()
        {
            if (_screenStack.TryPeek(out var currentScreen))
            {
                currentScreen.OnHide();
                currentScreen.gameObject.SetActive(false);
            }

            _screenStack.Clear();
        }

        public void ShowScreen(BaseMainMenuScreen screen)
        {
            _screenStack.TryPeek(out var currentScreen);

            if (currentScreen == screen) return;
            
            if (currentScreen != null)
            {
                currentScreen.OnHide();
                currentScreen.gameObject.SetActive(false);
            }

            _screenStack.Push(screen);
            screen.gameObject.SetActive(true);
            _headerText.text = screen.Title;
            screen.OnShow();

            _backButton.GameObject.SetActive(_screenStack.Count > 1);
        }

        public void GoBackScreen()
        {
            if (_screenStack.Count == 1)
            {
                _logger.LogError("Attempt to go back while on root screen of main menu");
                return;
            }

            var oldScreen = _screenStack.Pop();
            oldScreen.OnHide();
            oldScreen.gameObject.SetActive(false);

            var newScreen = _screenStack.Peek();
            newScreen.gameObject.SetActive(true);
            _headerText.text = newScreen.Title;
            newScreen.OnShow();

            _backButton.GameObject.SetActive(_screenStack.Count > 1);
        }

        private void OnBackButtonClicked(IInteractable sender, IInteractor interactor)
        {
            GoBackScreen();
        }
    }
}