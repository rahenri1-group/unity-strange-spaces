﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Resource;
using Game.Core.Space;
using Game.Entity;
using Game.Input;
using Game.Player;
using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.UI.MainMenu
{
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class MainMenuUiManager : BaseModule
    {
        public override string ModuleName => "Main Menu UI Manager";

        public override ModuleConfig ModuleConfig => Config;
        public MainMenuUiManagerConfig Config = new MainMenuUiManagerConfig();

        private readonly IEventBus _eventBus;
        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IPlayerManager _playerManager;
        private readonly IPortalPathingManager _portalPathingManager;
        private readonly IResourceMetadataManager _metadataManager;
        private readonly ISpaceManager _spaceManager;
        private readonly ISpacePhysics _spacePhysics;
        private readonly IUiVrInput _uiInput;

        private int _spawnAvoidLayerMask;
        private IDynamicEntityMetadata _menuMetadata;

        private bool _isProcessing;
        private MainMenuUiBehaviour _mainMenu;

        private CancellationTokenSource _menuDistanceCheckCts;

        public MainMenuUiManager(
            IEventBus eventBus,
            IGameObjectResourceManager gameObjectResourceManager,
            ILogRouter logger,
            IPlayerManager playerManager,
            IPortalPathingManager portalPathingManager,
            IResourceMetadataManager metadataManager,
            ISpaceManager spaceManager,
            ISpacePhysics spacePhysics,
            IUiVrInput uiInput)
            : base(logger)
        {
            _eventBus = eventBus;
            _gameObjectResourceManager = gameObjectResourceManager;
            _playerManager = playerManager;
            _portalPathingManager = portalPathingManager;
            _metadataManager = metadataManager;
            _spaceManager = spaceManager;
            _spacePhysics = spacePhysics;
            _uiInput = uiInput;

            _isProcessing = false;
            _mainMenu = null;

            _menuDistanceCheckCts = null;
        }

        public override UniTask Initialize()
        {
            _spawnAvoidLayerMask = LayerUtil.ConstructMaskForLayers(Config.SpawnAvoidLayers);

            _menuMetadata = _metadataManager.GetResourceMetadataComponent<IDynamicEntityMetadata>(Config.MainMenuAssetKey);

            _uiInput.InputEnabled = true;

            _uiInput.MenuToggleTriggered += OnMenuToggle;

            _eventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);

            return base.Initialize();
        }

        public override UniTask Shutdown()
        {
            if (_menuDistanceCheckCts != null)
            {
                _menuDistanceCheckCts.CancelAndDispose();
                _menuDistanceCheckCts = null;
            }

            _uiInput.MenuToggleTriggered -= OnMenuToggle;

            _eventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);

            return base.Shutdown();
        }

        private void OnMenuToggle(InputContext context)
        {
            if (_playerManager.Player == null
                || !_playerManager.Player.IsAlive()
                || _isProcessing) return;

            if (_mainMenu == null)
            {
                CreateMainMenu().Forget();
            }
            else
            {
                HideMenu();
            }
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (eventArgs.Entity == _playerManager.Player)
            {
                CheckPlayerDistanceFromMenu();
            }
        }

        private async UniTask CreateMainMenu()
        {
            _isProcessing = true;

            var space = _spaceManager.GetEntitySpace(_playerManager.Player);
            var castRotation = _playerManager.Player.HeadRotation.GetYAxisRotation();
            var playerFloorPosition = _playerManager.Player.FeetPosition.y;
            var castCenter = _playerManager.Player.HeadPosition + _menuMetadata.MeshBounds.center;
            castCenter.y = Mathf.Clamp(
                castCenter.y,
                playerFloorPosition + Config.MenuMinSpawnDistanceFromFloor,
                playerFloorPosition + Config.MenuMaxSpawnDistanceFromFloor);

            _spacePhysics.BoxCast(
                space,
                castCenter,
                _menuMetadata.MeshBounds.extents,
                (castRotation * Vector3.forward).normalized,
                out var resultInfo,
                castRotation,
                Config.MenuSpawnDistance,
                QueryPortalInteraction.CastThroughTransporter,
                _spawnAvoidLayerMask);

            var menu = await _gameObjectResourceManager.InstantiateAsync<MainMenuUiBehaviour>(
                Config.MainMenuAssetKey,
                resultInfo.Space,
                resultInfo.CastShapePosition,
                resultInfo.CastShapeRotation);

            if (menu.GameObject == null)
            {
                ModuleLogError($"Unable to load main menu asset {Config.MainMenuAssetKey}");
                _isProcessing = false;
                return;
            }

            _mainMenu = menu.Component;
            menu.GameObject.name = "@Main Menu";
            _mainMenu.InitializeNew(Guid.NewGuid());

            _mainMenu.OnShow();

            _menuDistanceCheckCts = new CancellationTokenSource();
            CheckPlayerDistance(_menuDistanceCheckCts.Token).Forget();

            _isProcessing = false;
        }

        private void HideMenu()
        {
            if (_menuDistanceCheckCts != null)
            {
                _menuDistanceCheckCts.CancelAndDispose();
                _menuDistanceCheckCts = null;
            }

            _mainMenu.OnHide();
            _gameObjectResourceManager.DestroyObject(_mainMenu.GameObject);
            _mainMenu = null;
        }

        private void CheckPlayerDistanceFromMenu()
        {
            if (_mainMenu == null) return;

            if (_playerManager.Player == null
                || !_portalPathingManager.GetPath(
                new SpacePosition(_spaceManager.GetEntitySpace(_playerManager.Player), _playerManager.Player.HeadPosition),
                new SpacePosition(_spaceManager.GetEntitySpace(_mainMenu), _mainMenu.EntityMovement.Position),
                Config.MaxDistanceFromPlayer,
                out var _))
            {
                HideMenu();
            }
        }

        private async UniTask CheckPlayerDistance(CancellationToken cancellationToken)
        {
            int checkDelayMillis = (int)(1000 / Config.MenuDistanceCheckFrequency);

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    await UniTask.Delay(checkDelayMillis, cancellationToken: cancellationToken);

                    CheckPlayerDistanceFromMenu();
                }
                catch (TaskCanceledException) { }
            }
        }
    }
}