﻿using Game.Core;

namespace Game.UI.MainMenu
{
    public class MainMenuUiManagerConfig : ModuleConfig
    {
        /// <summary>
        /// The asset key of the main menu to be spawned in
        /// </summary>
        public string MainMenuAssetKey = string.Empty;

        /// <summary>
        /// The layers to check against when spawning the main menu
        /// </summary>
        public string[] SpawnAvoidLayers = new string[0];

        /// <summary>
        /// Distance the menu will attempt to spawn from player head
        /// </summary>
        public float MenuSpawnDistance = 0.5f;

        /// <summary>
        /// Min distance from the player root the menu will spawn
        /// </summary>
        public float MenuMinSpawnDistanceFromFloor = 0f;
        /// <summary>
        /// Max distance from the player root the menu will spawn
        /// </summary>
        public float MenuMaxSpawnDistanceFromFloor = 1f;

        /// <summary>
        /// If the player is farther than this distance, destroy the menu
        /// </summary>
        public float MaxDistanceFromPlayer = 2.5f;
        /// <summary>
        /// Frequency for checking the menu's distance to the player
        /// </summary>
        public float MenuDistanceCheckFrequency = 1f;
    }
}