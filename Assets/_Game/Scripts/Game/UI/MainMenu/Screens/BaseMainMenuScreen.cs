﻿using Game.Core;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public abstract class BaseMainMenuScreen : InjectedBehaviour
    {
        public abstract string Title { get; }

        protected MainMenuUiBehaviour MainMenu 
        {
            get
            {
                if (_mainMenu == null)
                {
                    _mainMenu = GetComponentInParent<MainMenuUiBehaviour>();
                }
                return _mainMenu;
            }
        }
        private MainMenuUiBehaviour _mainMenu = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(MainMenu);
        }

        public virtual void OnShow() { }

        public virtual void OnHide() { }
    }
}