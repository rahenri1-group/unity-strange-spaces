﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Core.UI;
using Game.Storage.Settings;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuAudioOptionsScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => "Audio";

        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISettingsManager _settingsManager = null;

        [SerializeField] private LaserUiCarouselComponent _masterVolumeCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _musicVolumeCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _effectVolumeCarousel = null;

        private AudioSettingsGroup _audioSettingsGroup;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_masterVolumeCarousel);
            Assert.IsNotNull(_musicVolumeCarousel);
            Assert.IsNotNull(_effectVolumeCarousel);
        }

        public void ShowAudioOptionsScreen()
        {
            MainMenu.ShowScreen(this);
        }

        public override void OnShow()
        {
            base.OnShow();

            _audioSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<AudioSettingsGroup>();

            _masterVolumeCarousel.Initialize(
                21,
                () => (int) (_audioSettingsGroup.MasterVolume / 5),
                (carouselValue) => _audioSettingsGroup.MasterVolume = carouselValue * 5,
                (carouselValue) => $"Master Volume: {_audioSettingsGroup.MasterVolume}");

            _musicVolumeCarousel.Initialize(
                21,
                () => (int)(_audioSettingsGroup.MusicVolume / 5),
                (carouselValue) => _audioSettingsGroup.MusicVolume = carouselValue * 5,
                (carouselValue) => $"Music Volume: {_audioSettingsGroup.MusicVolume}");

            _effectVolumeCarousel.Initialize(
                21,
                () => (int)(_audioSettingsGroup.EffectsVolume / 5),
                (carouselValue) => _audioSettingsGroup.EffectsVolume = carouselValue * 5,
                (carouselValue) => $"Sound Effect Volume: {_audioSettingsGroup.EffectsVolume}");

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        public override void OnHide()
        {
            base.OnHide();

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        private void OnSettingsProfileChangedEvent(ActiveSettingsProfileChangedEvent eventData)
        {
            _audioSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<AudioSettingsGroup>();

            RefreshSettingsDisplay();
        }

        private void RefreshSettingsDisplay()
        {
            _masterVolumeCarousel.Refresh();
            _musicVolumeCarousel.Refresh();
            _effectVolumeCarousel.Refresh();
        }
    }
}