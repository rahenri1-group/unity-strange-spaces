﻿using Game.Core.Interaction;
using Game.Core;
using UnityEngine;
using TMPro;
using UnityEngine.Assertions;
using System;

namespace Game.UI.MainMenu
{
    public class MainMenuConfirmationScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => "Confirm";

        [SerializeField] private TMP_Text _confirmText = null;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _confirmButtonObj = null;
        private ILaserInteractable _confirmButton;

        private Action _confirmAction = null;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_confirmText);
            Assert.IsNotNull(_confirmButtonObj);

            _confirmButton = _confirmButtonObj.GetComponent<ILaserInteractable>();
        }

        private void OnEnable()
        {
            _confirmButton.InteractBegin += OnConfirmButtonClicked;
        }

        private void OnDisable()
        {
            _confirmButton.InteractBegin -= OnConfirmButtonClicked;
        }

        public void ShowConfirmationScreen(string confirmMessage, Action confirmAction)
        {
            _confirmText.text = confirmMessage;
            _confirmAction = confirmAction;

            MainMenu.ShowScreen(this);
        }

        public override void OnHide()
        {
            base.OnHide();

            _confirmAction = null;
        }

        private void OnConfirmButtonClicked(IInteractable sender, IInteractor interactor)
        {
            _confirmAction?.Invoke();
        }
    }
}