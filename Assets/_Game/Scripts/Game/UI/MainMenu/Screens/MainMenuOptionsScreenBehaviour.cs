﻿using Game.Core.Interaction;
using Game.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuOptionsScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => "Options";

        [SerializeField] private MainMenuPreferencesScreenBehaviour _preferencesScreen = null;
        [SerializeField] private MainMenuVrMovementOptionsScreenBehaviour _vrMovementOptionsScreen = null;
        [SerializeField] private MainMenuAudioOptionsScreenBehaviour _audioOptionsScreen = null;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _preferencesButtonObj = null;
        private ILaserInteractable _preferencesButton;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _movementOptionsButtonObj = null;
        private ILaserInteractable _movementOptionsButton;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _audioOptionsButtonObj = null;
        private ILaserInteractable _audioOptionsButton;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_preferencesScreen);
            Assert.IsNotNull(_vrMovementOptionsScreen);
            Assert.IsNotNull(_audioOptionsScreen);
            Assert.IsNotNull(_preferencesButtonObj);
            Assert.IsNotNull(_movementOptionsButtonObj);
            Assert.IsNotNull(_audioOptionsButtonObj);

            _preferencesButton = _preferencesButtonObj.GetComponentAsserted<ILaserInteractable>();
            _movementOptionsButton = _movementOptionsButtonObj.GetComponentAsserted<ILaserInteractable>();
            _audioOptionsButton = _audioOptionsButtonObj.GetComponentAsserted<ILaserInteractable>();
        }

        public void ShowOptionsScreen()
        {
            MainMenu.ShowScreen(this);
        }

        public override void OnShow()
        {
            base.OnShow();

            _preferencesButton.InteractBegin += OnPreferencesButtonClick;
            _movementOptionsButton.InteractBegin += OnMovementOptionsButtonClick;
            _audioOptionsButton.InteractBegin += OnAudioOptionsButtonClick;
        }

        public override void OnHide()
        {
            base.OnHide();

            _preferencesButton.InteractBegin -= OnPreferencesButtonClick;
            _movementOptionsButton.InteractBegin -= OnMovementOptionsButtonClick;
            _audioOptionsButton.InteractBegin -= OnAudioOptionsButtonClick;
        }

        private void OnPreferencesButtonClick(IInteractable sender, IInteractor interactor)
        {
            _preferencesScreen.ShowPreferencesScreen();
        }

        private void OnMovementOptionsButtonClick(IInteractable sender, IInteractor interactor)
        {
            _vrMovementOptionsScreen.ShowMovementOptionsScreen();
        }

        private void OnAudioOptionsButtonClick(IInteractable sender, IInteractor interactor)
        {
            _audioOptionsScreen.ShowAudioOptionsScreen();
        }
    }
}