﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Core.UI;
using Game.Storage.Settings;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuPreferencesScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => "Preferences";

        private readonly int[] HOLSTER_OFFSET_VALUES = {-9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISettingsManager _settingsManager = null;

        [SerializeField] private LaserUiCarouselComponent _healthHandCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _holsterVerticalOffsetCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _holsterForwardOffsetCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _holsterSeparationOffsetCarousel = null;

        private PreferencesVrSettingsGroup _preferencesSettingsGroup;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_healthHandCarousel);
            Assert.IsNotNull(_holsterVerticalOffsetCarousel);
            Assert.IsNotNull(_holsterForwardOffsetCarousel);
            Assert.IsNotNull(_holsterSeparationOffsetCarousel);
        }

        public void ShowPreferencesScreen()
        {
            MainMenu.ShowScreen(this);
        }

        public override void OnShow()
        {
            base.OnShow();

            _preferencesSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<PreferencesVrSettingsGroup>();

            _healthHandCarousel.Initialize(
                Enum.GetValues(typeof(VrHand)).Length,
                () => (int)_preferencesSettingsGroup.HealthDisplayHand,
                (carouselValue) => _preferencesSettingsGroup.HealthDisplayHand = (VrHand)carouselValue,
                (carouselValue) => $"Watch Hand: {((VrHand)carouselValue).GetUserFacingName()}");

            _holsterVerticalOffsetCarousel.Initialize(
                HOLSTER_OFFSET_VALUES.Length,
                () => Array.FindIndex(HOLSTER_OFFSET_VALUES, val => val == _preferencesSettingsGroup.HolsterVerticalOffset),
                (carouselValue) => _preferencesSettingsGroup.HolsterVerticalOffset = HOLSTER_OFFSET_VALUES[carouselValue],
                (carouselValue) => $"Holster Vertical Offset: {HOLSTER_OFFSET_VALUES[carouselValue]}");

            _holsterForwardOffsetCarousel.Initialize(
                HOLSTER_OFFSET_VALUES.Length,
                () => Array.FindIndex(HOLSTER_OFFSET_VALUES, val => val == _preferencesSettingsGroup.HolsterForwardOffset),
                (carouselValue) => _preferencesSettingsGroup.HolsterForwardOffset = HOLSTER_OFFSET_VALUES[carouselValue],
                (carouselValue) => $"Holster Forward Offset: {HOLSTER_OFFSET_VALUES[carouselValue]}");

            _holsterSeparationOffsetCarousel.Initialize(
                HOLSTER_OFFSET_VALUES.Length,
                () => Array.FindIndex(HOLSTER_OFFSET_VALUES, val => val == _preferencesSettingsGroup.HolsterSeparationOffset),
                (carouselValue) => _preferencesSettingsGroup.HolsterSeparationOffset = HOLSTER_OFFSET_VALUES[carouselValue],
                (carouselValue) => $"Holster Separation Offset: {HOLSTER_OFFSET_VALUES[carouselValue]}");

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        public override void OnHide()
        {
            base.OnHide();

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        private void OnSettingsProfileChangedEvent(ActiveSettingsProfileChangedEvent eventData)
        {
            _preferencesSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<PreferencesVrSettingsGroup>();

            RefreshSettingsDisplay();
        }

        private void RefreshSettingsDisplay()
        {
            _healthHandCarousel.Refresh();
            _holsterVerticalOffsetCarousel.Refresh();
            _holsterForwardOffsetCarousel.Refresh();
            _holsterSeparationOffsetCarousel.Refresh();
        }
    }
}