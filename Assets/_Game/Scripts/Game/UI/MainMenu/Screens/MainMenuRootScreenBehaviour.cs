﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Interaction;
using Game.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuRootScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => $"{Application.productName} v{Application.version}";

        [Inject] private IPlayerManager _playerManager = null;

        [SerializeField] private MainMenuOptionsScreenBehaviour _settingsScreen = null;
        [SerializeField] private MainMenuConfirmationScreenBehaviour _confirmScreen = null;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _optionsButtonObj = null;
        private ILaserInteractable _optionsButton;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _homeButtonObj = null;
        private ILaserInteractable _homeButton;

        [SerializeField][TypeRestriction(typeof(ILaserInteractable))] private Component _quitButtonObj = null;
        private ILaserInteractable _quitButton;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_settingsScreen);
            Assert.IsNotNull(_confirmScreen);
            Assert.IsNotNull(_optionsButtonObj);
            Assert.IsNotNull(_homeButtonObj);
            Assert.IsNotNull(_quitButtonObj);

            _optionsButton = _optionsButtonObj.GetComponentAsserted<ILaserInteractable>();
            _homeButton = _homeButtonObj.GetComponentAsserted<ILaserInteractable>();
            _quitButton = _quitButtonObj.GetComponentAsserted<ILaserInteractable>();
        }

        public override void OnShow()
        {
            base.OnShow();

            _optionsButton.InteractBegin += OnOptionsButtonClick;
            _homeButton.InteractBegin += OnHomeButtonClick;
            _quitButton.InteractBegin += OnQuitButtonClick;
        }

        public override void OnHide()
        {
            base.OnHide();

            _optionsButton.InteractBegin -= OnOptionsButtonClick;
            _homeButton.InteractBegin -= OnHomeButtonClick;
            _quitButton.InteractBegin -= OnQuitButtonClick;
        }

        private void OnOptionsButtonClick(IInteractable sender, IInteractor interactor)
        {
            _settingsScreen.ShowOptionsScreen();
        }

        private void OnHomeButtonClick(IInteractable sender, IInteractor interactor)
        {
            _confirmScreen.ShowConfirmationScreen("Leave and go back to home?",
                () =>
                {
                    var player = _playerManager.Player;

                    player.SetPlayerHealth(0);
                });
        }

        private void OnQuitButtonClick(IInteractable sender, IInteractor interactor)
        {
            _confirmScreen.ShowConfirmationScreen("Are you sure you want to quit?",
                () =>
                {
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
                });
        }
    }
}