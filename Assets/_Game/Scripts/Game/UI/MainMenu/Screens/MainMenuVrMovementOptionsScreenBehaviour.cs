﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Core.UI;
using Game.Storage.Settings;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.MainMenu
{
    public class MainMenuVrMovementOptionsScreenBehaviour : BaseMainMenuScreen
    {
        public override string Title => "Movement";

        [Inject] private IEventBus _eventBus = null;
        [Inject] private ISettingsManager _settingsManager = null;

        [SerializeField] private LaserUiCarouselComponent _locomotionHandCarousel = null;
        [SerializeField] private LaserUiCarouselComponent _locomotionTypeCarousel = null;
        [SerializeField] private LaserUiToggleBehaviour _snapTurnToggle = null;
        [SerializeField] private LaserUiCarouselComponent _snapTurnAngleCarousel = null;

        private InputVrSettingsGroup _inputVrSettingsGroup;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_locomotionHandCarousel);
            Assert.IsNotNull(_locomotionTypeCarousel);
            Assert.IsNotNull(_snapTurnToggle);
            Assert.IsNotNull(_snapTurnAngleCarousel);
        }

        public void ShowMovementOptionsScreen()
        {
            MainMenu.ShowScreen(this);
        }

        public override void OnShow()
        {
            base.OnShow();

            _inputVrSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<InputVrSettingsGroup>();

            _locomotionHandCarousel.Initialize(
                Enum.GetValues(typeof(VrHand)).Length,
                () => (int)_inputVrSettingsGroup.PrimaryLocomotionHand,
                (carouselValue) => _inputVrSettingsGroup.PrimaryLocomotionHand = (VrHand)carouselValue,
                (carouselValue) => $"Movement Hand: {((VrHand)carouselValue).GetUserFacingName()}");

            _locomotionTypeCarousel.Initialize(
                Enum.GetValues(typeof(VrLocomotion)).Length,
                () => (int)_inputVrSettingsGroup.LocomotionType,
                (carouselValue) => _inputVrSettingsGroup.LocomotionType = (VrLocomotion)carouselValue,
                (carouselValue) => $"Movement Type: {((VrLocomotion)carouselValue).GetUserFacingName()}");

            _snapTurnToggle.Initialize(
                () => _inputVrSettingsGroup.SnapTurnEnabled,
                (toggleValue) => _inputVrSettingsGroup.SnapTurnEnabled = toggleValue,
                (toggleValue) => $"Snap Turn: {(toggleValue ? "On" : "Off")}");

            _snapTurnAngleCarousel.Initialize(
                3,
                () =>
                {
                    if (_inputVrSettingsGroup.SnapTurnAmount == 30f)
                        return 0;
                    else if (_inputVrSettingsGroup.SnapTurnAmount == 45f)
                        return 1;
                    else if (_inputVrSettingsGroup.SnapTurnAmount == 90f)
                        return 2;
                    else
                        return 0;
                },
                (carouselValue) =>
                {
                    switch (carouselValue)
                    {
                        case 0:
                            _inputVrSettingsGroup.SnapTurnAmount = 30f;
                            return;
                        case 1:
                            _inputVrSettingsGroup.SnapTurnAmount = 45f;
                            return;
                        case 2:
                            _inputVrSettingsGroup.SnapTurnAmount = 90f;
                            return;
                    }
                },
                (carouselValue) => $"Snap Turn Angle: {(int)_inputVrSettingsGroup.SnapTurnAmount}");

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        public override void OnHide()
        {
            base.OnHide();

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnSettingsProfileChangedEvent);
        }

        private void OnSettingsProfileChangedEvent(ActiveSettingsProfileChangedEvent eventData)
        {
            _inputVrSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<InputVrSettingsGroup>();

            RefreshSettingsDisplay();
        }

        private void RefreshSettingsDisplay()
        {
            _locomotionHandCarousel.Refresh();
            _locomotionTypeCarousel.Refresh();
            _snapTurnToggle.Refresh();
            _snapTurnAngleCarousel.Refresh();
        }
    }
}