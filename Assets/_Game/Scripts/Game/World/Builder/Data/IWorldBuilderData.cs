﻿using Game.Core.Space;

namespace Game.World.Builder.Data
{
    public enum RoomType { Hallway = 0, Room = 1, Landing = 2 }
    public enum WorldZone { None = -100, GroundFloor = 0, SecondFloor = 1, Basement = 2 }

    public interface IWorldBuilderData : ISpaceDataComponent
    {
        bool IsWorldEntrance { get; }

        RoomType RoomType { get; }

        WorldZone[] AllowedZones { get; }

        int DoorSidePriority { get; }

        int MaxLinkedSpaces { get; }

        PortalData[] DynamicPortals { get; }
    }
}
