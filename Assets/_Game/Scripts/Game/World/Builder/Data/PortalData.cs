﻿using System;
using UnityEngine;

namespace Game.World.Builder.Data
{
    [Serializable]
    public class PortalData
    {
        private Guid _parsedPortalId = Guid.Empty;
        public Guid PortalId
        {
            get
            {
                if (_parsedPortalId == Guid.Empty) _parsedPortalId = Guid.Parse(_portalId);
                return _parsedPortalId;
            }
        }

        public bool WorldEntrance { get => _worldEntrance; set => _worldEntrance = value; }

        public WorldZone ZoneEntrance { get => _zoneEntrance; set => _zoneEntrance = value; }

        public RoomType[] AllowedDestinationTypes { get => _allowedDestinationTypes; set => _allowedDestinationTypes = value; }

        public Vector3 Position { get => _portalPosition; set => _portalPosition = value; }
        public Quaternion Rotation { get => _portalRotation; set => _portalRotation = value; }
        public Vector3 Scale { get => _portalScale; set => _portalScale = value; }

        [SerializeField] private string _portalId;
        [SerializeField] private bool _worldEntrance;
        [SerializeField] private WorldZone _zoneEntrance;
        [SerializeField] private RoomType[] _allowedDestinationTypes;
        [SerializeField] private Vector3 _portalPosition;
        [SerializeField] private Quaternion _portalRotation;
        [SerializeField] private Vector3 _portalScale;

        public PortalData()
        {
            _portalId = string.Empty;
            _worldEntrance = false;
            _zoneEntrance = WorldZone.None;
            _allowedDestinationTypes = new RoomType[0];
            _portalPosition = Vector3.zero;
            _portalRotation = Quaternion.identity;
            _portalScale = Vector3.one;
        }

        public PortalData(Guid portalId)
        {
            _parsedPortalId = portalId;
            _portalId = portalId.ToString();

            _worldEntrance = false;

            _zoneEntrance = WorldZone.None;
            _allowedDestinationTypes = new RoomType[0];

            _portalPosition = Vector3.zero;
            _portalRotation = Quaternion.identity;
            _portalScale = Vector3.one;
        }
    }
}
