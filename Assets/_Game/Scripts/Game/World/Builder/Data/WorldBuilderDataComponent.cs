﻿using Game.Core;
using System;
using UnityEngine;

namespace Game.World.Builder.Data
{
    [Serializable]
    public class WorldBuilderDataComponent : IWorldBuilderData
    {
        public bool IsWorldEntrance => _worldEntrance;

        public RoomType RoomType => _roomType;

        public WorldZone[] AllowedZones => _allowedZones;

        public int MaxLinkedSpaces => _maxLinkedSpaces > 0 ? _maxLinkedSpaces : 999;
        public PortalData[] DynamicPortals { get => _dynamicPortals ?? new PortalData[0]; }

        public int DoorSidePriority
        {
            get
            {
                switch (RoomType)
                {
                    case RoomType.Hallway:
                        return 1000;
                    case RoomType.Room:
                        return 0;
                    default:
                        return 0;
                }
            }
        }

#if UNITY_EDITOR
        public PortalData[] DynamicPortalsEditor
        {
            get => _dynamicPortals;
            set => _dynamicPortals = value;
        }
#endif

        [SerializeField] private BoolReadonlyReference _worldEntrance = null;
        [SerializeField] private RoomType _roomType = 0;
        [SerializeField] private WorldZone[] _allowedZones = null;
        [SerializeField] private IntReadonlyReference _maxLinkedSpaces = null;
        [SerializeField] private PortalData[] _dynamicPortals = null;
    }
}
