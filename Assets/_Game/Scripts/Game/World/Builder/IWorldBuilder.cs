﻿using Cysharp.Threading.Tasks;

namespace Game.World.Builder
{
    public interface IWorldBuilder
    {
        UniTask LoadBuilderResources();
        UniTask ReleaseBuilderResources();

        UniTask<WorldDefinition> CreateWorldDefinition();

        UniTask<WorldDefinition> CreateWorldDefinition(int seed);
    }
}
