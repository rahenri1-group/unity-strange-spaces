﻿using Game.Core.Portal;
using Game.World.Builder.Data;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.World.Builder
{
    public class PortalDoorLandingDataBehaviour : MonoBehaviour
    {
        public WorldZone ZoneEntrance => _zoneEntrance;

        public RoomType[] AllowedDestinationTypes => _allowedDestinationTypes;

        [SerializeField] private WorldZone _zoneEntrance;
        [SerializeField] private RoomType[] _allowedDestinationTypes;

        private void Awake()
        {
            Assert.IsTrue(GetComponent<IDynamicPortal>() != null);

            Destroy(this);
        }
    }
}
