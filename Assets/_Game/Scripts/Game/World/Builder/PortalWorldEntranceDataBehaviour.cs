﻿using Game.Core.Portal;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.World.Builder
{
    public class PortalWorldEntranceDataBehaviour : MonoBehaviour
    {
        private void Awake()
        {
            Assert.IsTrue(GetComponent<IDynamicPortal>() != null);

            Destroy(this);
        }
    }
}
