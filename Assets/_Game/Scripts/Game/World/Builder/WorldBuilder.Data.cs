﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Portal;
using Game.Core.Space;
using Game.World.Builder.Data;
using Game.World.Item.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.World.Builder
{
    public partial class WorldBuilder
    {
        private class WorldDefinitionBuilderData
        {
            public readonly IRandom Random;

            public readonly List<RuntimeBuilderData> BuilderDataList;

            public readonly Dictionary<WorldZone, List<RuntimeBuilderData>> LinkableSpacesZoneMap;
            public bool LinkableSpacesEmpty
            {
                get
                {
                    foreach (var pair in LinkableSpacesZoneMap)
                    {
                        if (pair.Value.Count > 0)
                            return false;
                    }

                    return true;
                }
            }

            public readonly List<DynamicPortalConnection> PortalConnections;
            public readonly List<WorldEntityDefinition> Entities;

            public WorldDefinitionBuilderData(int seed)
            {
                Random = Core.Random.Create(seed);
                BuilderDataList = new List<RuntimeBuilderData>();

                LinkableSpacesZoneMap = new Dictionary<WorldZone, List<RuntimeBuilderData>>();
                LinkableSpacesZoneMap[WorldZone.Basement] = new List<RuntimeBuilderData>();
                LinkableSpacesZoneMap[WorldZone.GroundFloor] = new List<RuntimeBuilderData>();
                LinkableSpacesZoneMap[WorldZone.SecondFloor] = new List<RuntimeBuilderData>();

                PortalConnections = new List<DynamicPortalConnection>();
                Entities = new List<WorldEntityDefinition>();
            }

            public void AddLinkableSpace(RuntimeBuilderData space)
            {
                if (space.RemainingAvailablePortalsCount == 0) return;

                if (space.RoomType == RoomType.Landing)
                {
                    foreach (var zone in space.AllowedZones)
                    {
                        LinkableSpacesZoneMap[zone].Add(space);
                    }
                }
                else
                {
                    LinkableSpacesZoneMap[space.AssignedZone].Add(space);
                }
            }

            public void RemoveLinkableSpace(RuntimeBuilderData space)
            {
                if (space.RoomType == RoomType.Landing)
                {
                    foreach (var zone in space.AllowedZones)
                    {
                        LinkableSpacesZoneMap[zone].Remove(space);
                    }
                }
                else
                {
                    LinkableSpacesZoneMap[space.AssignedZone].Remove(space);
                }
            }

            public void RemoveLinkableSpacesFromZone(WorldZone zone)
            {
                LinkableSpacesZoneMap[zone].Clear();
            }
        }

        private class RuntimeBuilderData
        {
            public WorldZone AssignedZone = WorldZone.None;

            public RoomType RoomType => WorldBuilderData.RoomType;
            public WorldZone[] AllowedZones => WorldBuilderData.AllowedZones;

            public bool IsDeadEnd => WorldBuilderData.MaxLinkedSpaces == 1 || WorldBuilderData.DynamicPortals.Length == 1;

            public int RemainingSpaceLinks => Mathf.Clamp(WorldBuilderData.MaxLinkedSpaces - (WorldBuilderData.DynamicPortals.Length - AvailableStandardPortals.Count), 0, WorldBuilderData.MaxLinkedSpaces);
            public int RemainingAvailablePortalsCount => AvailableStandardPortals.Count;

            public readonly SpaceData SpaceData;
            public readonly IWorldBuilderData WorldBuilderData;
            public readonly IWorldItemsSpawnData ItemSpawnData;

            public readonly List<PortalData> AvailableWorldEntrancePortals;

            public readonly List<PortalData> AvailableStandardPortals;

            public readonly HashSet<ISpaceData> LinkedSpaces;

            public RuntimeBuilderData(SpaceData spaceData, IWorldBuilderData worldBuilderData)
            {
                SpaceData = spaceData;
                WorldBuilderData = worldBuilderData;
                ItemSpawnData = spaceData.GetAdditionalData<IWorldItemsSpawnData>();

                AvailableStandardPortals = new List<PortalData>(WorldBuilderData.DynamicPortals.Where(p => !p.WorldEntrance));
                AvailableWorldEntrancePortals = new List<PortalData>(WorldBuilderData.DynamicPortals.Where(p => p.WorldEntrance));
                LinkedSpaces = new HashSet<ISpaceData>();
            }
        }

        private Dictionary<SpaceData, IWorldBuilderData> _builderSpaces;

        public async UniTask LoadBuilderResources()
        {
            if (_builderSpaces != null)
            {
                Logger.LogError(LogCategory, $"World builder resources already loaded");
                return;
            }

            _builderSpaces = new Dictionary<SpaceData, IWorldBuilderData>();

            var worldBuilderKeys = await AssetResourceManager.GetAssetKeysWithLabel(Config.WorldBuilderSpacesLabel);

            foreach (var assetKey in worldBuilderKeys)
            {
                var spaceData = await AssetResourceManager.LoadAsset<SpaceData>(assetKey);
                var builderData = spaceData.GetAdditionalData<IWorldBuilderData>();

                if (builderData == null)
                {
                    Logger.LogError(LogCategory, $"Space {spaceData.Name} missing builder data");
                    continue;
                }

                _builderSpaces[spaceData] = builderData;
            }
        }

        public UniTask ReleaseBuilderResources()
        {
            if (_builderSpaces == null)
            {
                Logger.LogError(LogCategory, $"World builder resources already released");
                return UniTask.CompletedTask;
            }

            foreach (var pair in _builderSpaces)
            {
                var spaceData = pair.Key;
                AssetResourceManager.ReleaseAsset(spaceData);
            }

            return UniTask.CompletedTask;
        }
    }
}
