﻿using Game.Core.Math;
using Game.Core.Space;
using Game.Entity;
using Game.World.Builder.Data;
using System;
using UnityEngine;

namespace Game.World.Builder
{
    public partial class WorldBuilder
    {
        private Guid AddDoorEntityForPortal(WorldDefinitionBuilderData definitionBuilderData, ISpaceData space, PortalData portal, Guid? doorKeyId = null)
        {
            var doorGuid = Guid.NewGuid();

            var position = portal.Position + portal.Rotation * (-0.5f * portal.Scale.y * Vector3.up);
            var rotation = portal.Rotation * MathUtil.ReverseAxisY;

            definitionBuilderData.Entities.Add(
                new WorldEntityDefinition(
                    doorGuid,
                    Config.DoorAssetKey,
                    space,
                    position,
                    rotation,
                    Vector3.one,
                    JsonSerializer.Serialize(
                        new DoorEntityBehaviour.DoorEntityData
                        {
                            DoorKeyId = doorKeyId.ToString(),
                            IsLocked = doorKeyId != null
                        })
                ));

            return doorGuid;
        }

        private void AddShadowDoorEntity(WorldDefinitionBuilderData definitionBuilderData, Guid primaryDoorId, ISpaceData space, PortalData portal)
        {
            var shadowDoorGuid = Guid.NewGuid();

            var position = portal.Position + portal.Rotation * (-0.5f * portal.Scale.y * Vector3.up);

            definitionBuilderData.Entities.Add(
                new WorldEntityDefinition(
                    shadowDoorGuid,
                    Config.DoorShadowAssetKey,
                    space,
                    position,
                    portal.Rotation,
                    Vector3.one,
                    JsonSerializer.Serialize(
                        new DoorShadowEntityBehaviour.ShadowEntityData
                        {
                            DoorToShadowId = primaryDoorId.ToString()
                        })
                ));
        }
    }
}
