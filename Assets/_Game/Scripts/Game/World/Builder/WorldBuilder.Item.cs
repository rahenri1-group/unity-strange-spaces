﻿using Game.Core;
using Game.Core.Space;
using Game.Item;
using Game.World.Item.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.World.Builder
{
    public partial class WorldBuilder
    {
        private class ItemSpawnPosition
        {
            public ItemSpawnGroup[] ItemSpawnGroups { get; private set; }

            public ISpaceData SpaceData { get; private set; }

            public Vector3 Position {get; private set; }
            public Quaternion Rotation { get; private set; }

            public ItemSpawnPosition(ItemSpawnGroup[] itemSpawnGroups, ISpaceData spaceData, Vector3 position, Quaternion rotation)
            {
                ItemSpawnGroups = itemSpawnGroups;
                SpaceData = spaceData;
                Position = position;
                Rotation = rotation;
            }
        }

        private void AddItemEntities(WorldDefinitionBuilderData definitionBuilderData)
        {
            var spawnGroupLocations = new Dictionary<ItemSpawnGroup, List<ItemSpawnPosition>>();

            // build map of available spawns
            foreach (var builderData in definitionBuilderData.BuilderDataList)
            {
                if (builderData.ItemSpawnData == null) continue;

                foreach (var itemSpawnData in builderData.ItemSpawnData.ItemSpawns)
                {
                    var rotation = itemSpawnData.Rotation;
                    if (!itemSpawnData.FixedSpawnRotation)
                    {
                        rotation = Quaternion.Euler(0f, definitionBuilderData.Random.Range(0f, 360f), 0f);
                    }

                    var spawnPosition = new ItemSpawnPosition(itemSpawnData.ItemSpawnGroups, builderData.SpaceData, itemSpawnData.Position, rotation);
                    foreach (var spawnGroup in itemSpawnData.ItemSpawnGroups)
                    {
                        if (!spawnGroupLocations.ContainsKey(spawnGroup))
                        {
                            spawnGroupLocations.Add(spawnGroup, new List<ItemSpawnPosition>());
                        }

                        spawnGroupLocations[spawnGroup].Add(spawnPosition);
                    }
                }
            }

            // order spawn groups by id so spawning is deterministic
            var usedSpawnLocations = new HashSet<ItemSpawnPosition>();
            var spawnGroupSelectOrder = spawnGroupLocations
                .OrderBy(pair => pair.Key.Id)
                .ToArray();

            // keep track of recent spaces that have had an item spawned in
            // once all spaces have been spawned into, clear, then repeat
            var recentSpaceSpawns = new HashSet<ISpaceData>();

            // go through each spawn group in a deterministic ordering
            foreach (var pair in spawnGroupSelectOrder)
            {
                var spawnGroup = pair.Key;
                var possibleSpawnLocations = pair.Value;

                possibleSpawnLocations.Shuffle(definitionBuilderData.Random);

                // go through each item in the spawn group
                foreach (var item in spawnGroup.ItemSpawnDefinitions)
                {
                    int spawnCount = Core.Random.Default.Range(item.MinSpawnCount, item.MaxSpawnCount + 1);
                    for (int i = 0; i < spawnCount; i++)
                    {
                        var spawnPosition = possibleSpawnLocations.FirstOrDefault(s => !usedSpawnLocations.Contains(s) && !recentSpaceSpawns.Contains(s.SpaceData));
                        if (spawnPosition == null)
                        {
                            recentSpaceSpawns.Clear();
                            spawnPosition = possibleSpawnLocations.FirstOrDefault(s => !usedSpawnLocations.Contains(s));
                        }

                        if (spawnPosition != null)
                        {
                            usedSpawnLocations.Add(spawnPosition);
                            recentSpaceSpawns.Add(spawnPosition.SpaceData);

                            var itemGuid = Guid.NewGuid();
                            definitionBuilderData.Entities.Add(
                                new WorldEntityDefinition(
                                    itemGuid,
                                    item.ItemDefinition.ItemEntityAddress,
                                    spawnPosition.SpaceData,
                                    spawnPosition.Position,
                                    spawnPosition.Rotation,
                                    Vector3.one,
                                    string.Empty
                                ));
                        }
                        else
                        {
                            Logger.LogWarning(LogCategory, $"Unable to find spawn location for {item.ItemDefinition.Name}");
                        }
                    }
                }
            }
        }
    }
}
