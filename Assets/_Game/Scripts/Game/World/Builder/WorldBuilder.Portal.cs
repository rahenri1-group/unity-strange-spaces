﻿using Game.Core;
using Game.Core.Math;
using Game.World.Builder.Data;
using System;
using System.Linq;
using UnityEngine;

namespace Game.World.Builder
{
    public partial class WorldBuilder
    {
        private bool CanPortalConnectToSpace(PortalData portal, RuntimeBuilderData originalSpace, RuntimeBuilderData destinationSpace)
        {
            // if portal has a roomtype limitation, destination must have that roomtype
            if (portal.AllowedDestinationTypes.Length > 0 && !portal.AllowedDestinationTypes.Contains(destinationSpace.RoomType))
                return false;

            if (portal.ZoneEntrance == WorldZone.None)
            {
                // either in the same zone or one does not yet have a zone
                return originalSpace.AssignedZone == destinationSpace.AssignedZone
                    || destinationSpace.AssignedZone == WorldZone.None
                    || originalSpace.AssignedZone == WorldZone.None;
            }
            else
            {
                // we have a zone entrance on the portal, so the destination must match it
                return (destinationSpace.AssignedZone == WorldZone.None && destinationSpace.AllowedZones.Contains(portal.ZoneEntrance))
                    || (destinationSpace.AssignedZone == portal.ZoneEntrance);
            }
        }

        private void AttemptConnectRemainingSpacePortals(WorldDefinitionBuilderData definitionBuilderData)
        {
            while (!definitionBuilderData.LinkableSpacesEmpty)
            {
                // get zone with most linable spaces
                var zone = definitionBuilderData.LinkableSpacesZoneMap
                    .OrderByDescending(p => p.Value.Count)
                    .ThenBy(p => p.Key)
                    .First()
                    .Key;

                var spaces = definitionBuilderData.LinkableSpacesZoneMap[zone];
                spaces.Shuffle(definitionBuilderData.Random);

                bool linkFound = false;
                foreach (var space in spaces)
                {
                    RuntimeBuilderData spaceToLink = null;
                    foreach (var testSpace in spaces)
                    {
                        if (space == testSpace) continue;

                        if (CanSpacesBeLinked(definitionBuilderData, space, testSpace))
                        {
                            spaceToLink = testSpace;
                            break;
                        }
                    }

                    if (spaceToLink != null)
                    {
                        LinkWorldSpaces(definitionBuilderData, space, spaceToLink);
                        linkFound = true;
                        break;
                    }
                }

                // all links found for zone
                if (!linkFound)
                {
                    definitionBuilderData.RemoveLinkableSpacesFromZone(zone);
                }
            }
        }

        private void BlockRemainingPortals(WorldDefinitionBuilderData definitionBuilderData)
        {
            foreach (var room in definitionBuilderData.BuilderDataList)
            {
                foreach (var portal in room.AvailableStandardPortals)
                {
                    var guid = Guid.NewGuid();

                    var position = portal.Position + portal.Rotation * (-0.5f * portal.Scale.y * Vector3.up);
                    var rotation = portal.Rotation * MathUtil.ReverseAxisY;

                    definitionBuilderData.Entities.Add(
                        new WorldEntityDefinition(
                            guid,
                            Config.BlockedDoorAssetKey,
                            room.SpaceData,
                            position,
                            rotation,
                            Vector3.one,
                            string.Empty
                        ));
                }

                room.AvailableStandardPortals.Clear();
            }
        }
    }
}
