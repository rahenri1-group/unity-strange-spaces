﻿using Game.Core;
using Game.Core.Portal;
using Game.World.Builder.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.World.Builder
{
    public partial class WorldBuilder
    {
        private void LinkSpaces(WorldDefinitionBuilderData definitionBuilderData, RuntimeBuilderData[] spaces)
        {
            var unlinkedSpaces = new List<RuntimeBuilderData>(spaces);
            unlinkedSpaces.Shuffle(definitionBuilderData.Random);

            while (unlinkedSpaces.Count > 0)
            {
                bool foundLink = false;

                for (int i = 0; i < unlinkedSpaces.Count; i++)
                {
                    var unlinkedSpace = unlinkedSpaces[i];
                    RuntimeBuilderData spaceToLink = null;

                    // search in least linked zones first
                    var linkableZones = definitionBuilderData.LinkableSpacesZoneMap
                        .OrderBy(p => p.Value.Count)
                        .ThenBy(p => p.Key)
                        .ToArray();

                    foreach (var zoneTuple in linkableZones)
                    {
                        // work backwards through linked spaces 
                        spaceToLink = zoneTuple.Value.LastOrDefault(s =>
                        {
                            return CanSpacesBeLinked(definitionBuilderData, unlinkedSpace, s);
                        });

                        if (spaceToLink != null)
                        {
                            break;
                        }
                    }

                    if (spaceToLink != null)
                    {
                        LinkWorldSpaces(definitionBuilderData, unlinkedSpace, spaceToLink);

                        unlinkedSpaces.Remove(unlinkedSpace);

                        definitionBuilderData.AddLinkableSpace(unlinkedSpace);

                        if (unlinkedSpace.RoomType != RoomType.Landing)
                            Logger.LogInfo(LogCategory, $"{unlinkedSpace.SpaceData.Name} assigned zone {unlinkedSpace.AssignedZone}");

                        foundLink = true;
                        break;
                    }
                }

                if (!foundLink)
                {
                    // went through whole list without success
                    throw new WorldBuilderException("Unable to find link for remaining spaces");
                }
            }
        }

        private bool CanSpacesBeLinked(WorldDefinitionBuilderData definitionBuilderData, RuntimeBuilderData space1, RuntimeBuilderData space2)
        {
            if (space1.RemainingAvailablePortalsCount == 0 || space2.RemainingAvailablePortalsCount == 0)
            {
                return false;
            }

            if (!space1.LinkedSpaces.Contains(space2.SpaceData) && space1.RemainingSpaceLinks == 0)
            {
                return false;
            }

            if (!space2.LinkedSpaces.Contains(space1.SpaceData) && space2.RemainingSpaceLinks == 0)
            {
                return false;
            }

            // same type room can't link together
            if (space1.RoomType == space2.RoomType)
                return false;

            // hallways can link to rooms
            if ((space1.RoomType == RoomType.Hallway && space2.RoomType == RoomType.Room)
                || (space1.RoomType == RoomType.Room && space2.RoomType == RoomType.Hallway))
            {
                if ((space1.AssignedZone == space2.AssignedZone)
                    || (space1.AssignedZone == WorldZone.None && space1.AllowedZones.Contains(space2.AssignedZone))
                    || (space2.AssignedZone == WorldZone.None && space2.AllowedZones.Contains(space1.AssignedZone)))
                {
                    return true;
                }
            }

            // check landing allowed types against other space
            if (space1.RoomType == RoomType.Landing || space2.RoomType == RoomType.Landing)
            {
                var landingSpace = space1.RoomType == RoomType.Landing ? space1 : space2;
                var otherSpace = space1.RoomType == RoomType.Landing ? space2 : space1;

                foreach (var portal in landingSpace.AvailableStandardPortals)
                {
                    if (CanPortalConnectToSpace(portal, landingSpace, otherSpace))
                        return true;
                }
            }

            return false;
        }

        private void LinkWorldSpaces(WorldDefinitionBuilderData definitionBuilderData, RuntimeBuilderData space1, RuntimeBuilderData space2)
        {
            if (!CanSpacesBeLinked(definitionBuilderData, space1, space2))
            {
                throw new WorldBuilderException($"{space1.SpaceData.Id} and {space2.SpaceData.Id} can't be linked");
            }

            var portalList1 = new List<PortalData>(space1.AvailableStandardPortals);
            var portalList2 = new List<PortalData>(space2.AvailableStandardPortals);

            portalList1.Shuffle(definitionBuilderData.Random);
            portalList2.Shuffle(definitionBuilderData.Random);

            PortalData portal1 = null;
            PortalData portal2 = null;

            foreach (var p1 in portalList1)
            {
                foreach (var p2 in portalList2)
                {
                    if (CanPortalConnectToSpace(p1, space1, space2) && CanPortalConnectToSpace(p2, space2, space1))
                    {
                        portal1 = p1;
                        portal2 = p2;
                        break;
                    }
                }

                if (portal1 != null && portal2 != null) break;
            }

            if (portal1 == null || portal2 == null)
            {
                throw new WorldBuilderException($"Could not find linkable portals for {space1.SpaceData.Name} and {space2.SpaceData.Name}");
            }

            // update assigned zone
            if (space1.RoomType == RoomType.Landing)
            {
                space2.AssignedZone = portal1.ZoneEntrance;
            }
            else if (space2.RoomType == RoomType.Landing)
            {
                space1.AssignedZone = portal2.ZoneEntrance;
            }
            else if (space1.AssignedZone == WorldZone.None)
            {
                space1.AssignedZone = space2.AssignedZone;
            }
            else if (space2.AssignedZone == WorldZone.None)
            {
                space2.AssignedZone = space1.AssignedZone;
            }

            var portalConnection = new DynamicPortalConnection(
                portal1.PortalId, // portal 1 guid
                space1.SpaceData.Id, // portal 1 space guid
                portal2.PortalId, // portal 2 guid
                space2.SpaceData.Id // portal 2 space guid
            );

            definitionBuilderData.PortalConnections.Add(portalConnection);

            Guid? doorKeyId = null;
            if (portal1.PortalId.ToString() == Config.BasementPortalId || portal2.PortalId.ToString() == Config.BasementPortalId)
            {
                doorKeyId = Guid.Parse(Config.BasementDoorKeyId);
            }

            if (space1.WorldBuilderData.DoorSidePriority < space2.WorldBuilderData.DoorSidePriority)
            {
                var doorId = AddDoorEntityForPortal(definitionBuilderData, space1.SpaceData, portal1, doorKeyId);
                AddShadowDoorEntity(definitionBuilderData, doorId, space2.SpaceData, portal2);
            }
            else
            {
                var doorId = AddDoorEntityForPortal(definitionBuilderData, space2.SpaceData, portal2, doorKeyId);
                AddShadowDoorEntity(definitionBuilderData, doorId, space1.SpaceData, portal1);
            }

            space1.AvailableStandardPortals.Remove(portal1);
            space1.LinkedSpaces.Add(space2.SpaceData);

            space2.AvailableStandardPortals.Remove(portal2);
            space2.LinkedSpaces.Add(space1.SpaceData);

            if (space1.RemainingAvailablePortalsCount == 0)
            {
                definitionBuilderData.RemoveLinkableSpace(space1);
            }

            if (space2.RemainingAvailablePortalsCount == 0)
            {
                definitionBuilderData.RemoveLinkableSpace(space2);
            }
        }
    }
}
