﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.Serialization;
using Game.World.Builder.Data;
using System.Collections.Generic;
using System.Linq;

namespace Game.World.Builder
{
    [Dependency(
        contract: typeof(IWorldBuilder),
        lifetime: Lifetime.Singleton)]
    public partial class WorldBuilder : IWorldBuilder
    {
        private const string LogCategory = "World Builder";

        public WorldBuilderConfig Config = new WorldBuilderConfig();

        private readonly IAssetResourceManager AssetResourceManager;
        private readonly IJsonSerializer JsonSerializer;
        private readonly ILogRouter Logger;

        public WorldBuilder(
            IAssetResourceManager assetResourceManager,
            IJsonSerializer jsonSerializer,
            ILogRouter logger)
        {
            AssetResourceManager = assetResourceManager;
            JsonSerializer = jsonSerializer;
            Logger = logger;

            _builderSpaces = null;
        }

        public UniTask<WorldDefinition> CreateWorldDefinition()
        {
            for (int i = 0; i < Config.MaxBuildAttempts; i++)
            {
                int seed = 0;
                try
                {
                    seed = Core.Random.Default.Range(0, 65536);

                    return CreateWorldDefinition(seed);
                }
                catch (WorldBuilderException ex)
                {
                    Logger.LogWarning($"Unable to generate world for seed {seed}");
                    Logger.LogException(ex);
                }
            }

            throw new WorldBuilderException($"Unable to build a world after {Config.MaxBuildAttempts} attempts");
        }

        public UniTask<WorldDefinition> CreateWorldDefinition(int seed)
        {
            Logger.LogInfo(LogCategory, $"Creating world definition with seed '{seed}'");

            var definitionBuilderData = new WorldDefinitionBuilderData(seed);
            
            SetupWorldDefinitionBuilderData(definitionBuilderData);

            var unlinkedSpaces = new HashSet<RuntimeBuilderData>();
            unlinkedSpaces.UnionWith(definitionBuilderData.BuilderDataList);

            // Add starting space
            // start with landing with most possible spaces and work from there
            var startingSpace = unlinkedSpaces
                .Where(r => r.RoomType == RoomType.Landing)
                .Aggregate((space1, space2) =>
                {
                    if (space1.RemainingSpaceLinks == space2.RemainingSpaceLinks) // use build index for ties so there is a repeatable ordering
                        return space1.SpaceData.SceneBuildIndex > space2.SpaceData.SceneBuildIndex ? space1 : space2;

                    return space1.RemainingSpaceLinks > space2.RemainingSpaceLinks ? space1 : space2;
                });

            definitionBuilderData.AddLinkableSpace(startingSpace);

            unlinkedSpaces.Remove(startingSpace);

            // Add non-dead end spaces
            var nonDeadEndSpaces = unlinkedSpaces.Where(r => !r.IsDeadEnd).OrderBy(r => r.SpaceData.SceneBuildIndex).ToArray();
            LinkSpaces(definitionBuilderData, nonDeadEndSpaces);
            unlinkedSpaces.ExceptWith(nonDeadEndSpaces);

            // Add dead end spaces
            var deadEndSpaces = unlinkedSpaces.Where(r => r.IsDeadEnd).OrderBy(r => r.SpaceData.SceneBuildIndex).ToArray();
            LinkSpaces(definitionBuilderData, deadEndSpaces);
            unlinkedSpaces.ExceptWith(deadEndSpaces);

            // attempt to use up all remaining portals in linked spaces
            AttemptConnectRemainingSpacePortals(definitionBuilderData);

            // block all unused portals
            BlockRemainingPortals(definitionBuilderData);

            AddItemEntities(definitionBuilderData);

            var entrances = definitionBuilderData.BuilderDataList.Where(b => b.WorldBuilderData.IsWorldEntrance).ToList();
            entrances.Shuffle(definitionBuilderData.Random);
            var entrance = entrances[0];

            var entrancePortal = entrance.AvailableWorldEntrancePortals.RemoveRandomElement(definitionBuilderData.Random);

            var worldDefinition = new WorldDefinition
            (
                entrance.SpaceData,
                entrancePortal.PortalId,
                definitionBuilderData.BuilderDataList.Select(b => b.SpaceData).ToArray(),
                definitionBuilderData.PortalConnections.ToArray(),
                definitionBuilderData.Entities.ToArray()
            );

            Logger.LogInfo(LogCategory, "Successfully created world definition");

            return UniTask.FromResult(worldDefinition);
        }

        private void SetupWorldDefinitionBuilderData(WorldDefinitionBuilderData definitionBuilderData)
        {
            foreach (var pair in _builderSpaces)
            {
                var spaceData = pair.Key;
                var worldBuilderData = pair.Value;

                definitionBuilderData.BuilderDataList.Add(new RuntimeBuilderData(spaceData, worldBuilderData));
            }
        }
    }
}