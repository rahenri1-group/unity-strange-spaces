﻿using System;

namespace Game.World.Builder
{
    [Serializable]
    public class WorldBuilderConfig
    {
        public int MaxBuildAttempts = 5;

        public string WorldBuilderSpacesLabel = string.Empty;

        public string DoorAssetKey = string.Empty;
        public string DoorShadowAssetKey = string.Empty;
        public string BlockedDoorAssetKey = string.Empty;

        public string BasementDoorKeyId = string.Empty;
        public string BasementPortalId = string.Empty;
    }
}
