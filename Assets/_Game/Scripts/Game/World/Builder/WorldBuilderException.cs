﻿using System;

namespace Game.World.Builder
{
    public class WorldBuilderException : Exception
    {
        public WorldBuilderException(string message)
            : base(message) { }
    }
}
