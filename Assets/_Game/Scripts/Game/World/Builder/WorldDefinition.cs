﻿using Game.Core.Portal;
using Game.Core.Space;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.World.Builder
{
    public class WorldDefinition : IWorld
    {
        /// <inheritdoc/>
        public ISpaceData WorldEntranceSpace { get; private set; }

        public Guid WorldEntrancePortalId { get; private set; }

        /// <inheritdoc/>
        public ISpaceData[] WorldSpaces { get; private set; }
        public DynamicPortalConnection[] BuilderPortalConnections { get; private set; }
        public WorldEntityDefinition[] BuilderEntities { get; private set; }

        public WorldDefinition(
            ISpaceData worldEntranceSpace,
            Guid worldEntrancePortalId,
            ISpaceData[] spaces, 
            DynamicPortalConnection[] portalConnections, 
            WorldEntityDefinition[] entities)
        {
            WorldEntranceSpace = worldEntranceSpace;
            WorldEntrancePortalId = worldEntrancePortalId;

            WorldSpaces = spaces;
            BuilderPortalConnections = portalConnections;
            BuilderEntities = entities;
        }

        /// <inheritdoc/>
        public ISpaceData[] GetConnectedSpaces(ISpaceData space)
        {
            var spaceIds = new HashSet<Guid>();

            foreach (var connection in BuilderPortalConnections)
            {
                if (connection.SpaceIdA == space.SpaceId && connection.SpaceIdB != space.SpaceId)
                {
                    spaceIds.Add(connection.SpaceIdB);
                }
                else if (connection.SpaceIdA != space.SpaceId && connection.SpaceIdB == space.SpaceId)
                {
                    spaceIds.Add(connection.SpaceIdA);
                }
            }

            return WorldSpaces
                .Where(s => spaceIds.Contains(s.SpaceId))
                .ToArray();
        }

        public IDynamicPortalConnection[] ConnectionsForSpace(ISpaceData space)
        {
            return BuilderPortalConnections
                .Where(c => c.SpaceIdA == space.SpaceId || c.SpaceIdB == space.SpaceId)
                .ToArray();
        }
    }

    public class WorldEntityDefinition
    {
        public readonly Guid Id;
        public readonly string AssetKey;
        public readonly ISpaceData Space;
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;
        public readonly Vector3 Scale;
        public readonly string Json;

        public WorldEntityDefinition(Guid id, string assetKey, ISpaceData space, Vector3 position, Quaternion rotation, Vector3 scale, string json)
        {
            Id = id;
            AssetKey = assetKey;
            Space = space;
            Position = position;
            Rotation = rotation;
            Scale = scale;
            Json = json;
        }
    }
}
