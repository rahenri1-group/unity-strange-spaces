﻿using Game.Core;
using System;
using UnityEngine;

namespace Game.World.Haunt.Data
{
    [Serializable]
    public class HauntDataComponent : IHauntSpaceData
    {
        public int PassiveHauntRate => _passiveHauntRate;

        public Color MinHauntLevelAmbientLight => _minHauntLevelAmbientLight;
        public Color MaxHauntLevelAmbientLight => _maxHauntLevelAmbientLight;

        public HauntSpawnData[] HauntMonsterSpawns => _hauntMonsterSpawns;

        public int MaxTraps => _maxHauntTraps;
        public HauntSpawnData[] HauntTrapSpawns => _hauntTrapSpawns;

        [SerializeField] private IntReadonlyReference _passiveHauntRate = null;
        [SerializeField] private ColorHdrReadonlyReference _minHauntLevelAmbientLight = null;
        [SerializeField] private ColorHdrReadonlyReference _maxHauntLevelAmbientLight = null;
        [SerializeField] private HauntSpawnData[] _hauntMonsterSpawns = new HauntSpawnData[0];
        [SerializeField] private IntReadonlyReference _maxHauntTraps = null;
        [SerializeField] private HauntSpawnData[] _hauntTrapSpawns = new HauntSpawnData[0];
    }
}
