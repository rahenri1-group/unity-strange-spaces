﻿using System;
using UnityEngine;

namespace Game.World.Haunt.Data
{
    [Serializable]
    public class HauntSpawnData
    {
        public Vector3 Position => _position;
        public Quaternion Rotation => _rotation;

        [SerializeField] private Vector3 _position = Vector3.zero;
        [SerializeField] private Quaternion _rotation = Quaternion.identity;
    }
}
