﻿using Game.Core.Space;
using UnityEngine;

namespace Game.World.Haunt.Data
{
    public interface IHauntSpaceData : ISpaceDataComponent
    {
        int PassiveHauntRate { get; }

        Color MinHauntLevelAmbientLight { get; }
        Color MaxHauntLevelAmbientLight { get; }

        HauntSpawnData [] HauntMonsterSpawns { get; }

        int MaxTraps { get; }
        HauntSpawnData[] HauntTrapSpawns { get; }
    }
}
