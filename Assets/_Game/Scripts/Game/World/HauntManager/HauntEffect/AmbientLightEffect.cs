﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Space;
using Game.World.Haunt.Data;
using UnityEngine;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntEffect),
        lifetime: Lifetime.Singleton)]
    public class AmbientLightEffect : BaseHauntEffect
    {
        private readonly IEventBus _eventBus;
        private readonly ISpaceLoader _spaceLoader;
        private readonly ISpaceRenderer _spaceRenderer;

        public AmbientLightEffect(
            IEventBus eventBus,
            ISpaceLoader spaceLoader,
            ISpaceRenderer spaceRenderer)
        {
            _eventBus = eventBus;
            _spaceLoader = spaceLoader;
            _spaceRenderer = spaceRenderer;
        }

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            _eventBus.Subscribe<SpaceLoadedEvent>(OnSpaceLoadedEvent);

            HauntManager.HauntLevelAnimated.VariableChanged += OnHauntLevelChanged;
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            _eventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoadedEvent);

            HauntManager.HauntLevelAnimated.VariableChanged -= OnHauntLevelChanged;
        }

        private void OnSpaceLoadedEvent(SpaceLoadedEvent eventArgs)
        {
            var hauntData = eventArgs.SpaceData.GetAdditionalData<IHauntSpaceData>();
            if (hauntData != null)
            {
                var color = Color.Lerp(hauntData.MinHauntLevelAmbientLight, hauntData.MaxHauntLevelAmbientLight, Mathf.Clamp01(HauntManager.HauntLevelAnimated.Value / HauntManager.HauntLevelMax));
                _spaceRenderer.GetSpaceRenderSettings(eventArgs.SpaceData).AmbientLightColor = color;
            }
        }

        private void OnHauntLevelChanged()
        {
            float lerpValue = Mathf.Clamp01(HauntManager.HauntLevelAnimated.Value / HauntManager.HauntLevelMax);

            foreach (var space in _spaceLoader.LoadedSpaces)
            {
                var hauntData = space.GetAdditionalData<IHauntSpaceData>();
                if (hauntData != null)
                {
                    var color = Color.Lerp(hauntData.MinHauntLevelAmbientLight, hauntData.MaxHauntLevelAmbientLight, lerpValue);
                    _spaceRenderer.GetSpaceRenderSettings(space).AmbientLightColor = color;
                }
            }
        }
    }
}
