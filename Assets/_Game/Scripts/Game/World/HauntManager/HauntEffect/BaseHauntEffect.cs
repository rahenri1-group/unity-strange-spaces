﻿namespace Game.World.Haunt
{
    public abstract class BaseHauntEffect : IHauntEffect
    {
        protected IHauntManager HauntManager { get; private set; }

        public virtual void InitializeEffect(IHauntManager hauntManager) 
        {
            HauntManager = hauntManager;
        }

        public virtual void ShutdownEffect() { }

        public virtual void ResetEffect() { }
    }
}
