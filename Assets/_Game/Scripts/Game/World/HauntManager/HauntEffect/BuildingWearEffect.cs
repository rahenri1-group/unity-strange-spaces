﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntEffect),
        lifetime: Lifetime.Singleton)]
    public class BuildingWearEffect : BaseHauntEffect
    {
        public string BuildingWearShaderProperty = string.Empty;

        private int _buildingWearShaderId;

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            _buildingWearShaderId = Shader.PropertyToID(BuildingWearShaderProperty);

            Shader.SetGlobalFloat(_buildingWearShaderId, 0f);

            HauntManager.HauntLevelAnimated.VariableChanged += OnHauntLevelChanged;
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            HauntManager.HauntLevelAnimated.VariableChanged -= OnHauntLevelChanged;
        }

        private void OnHauntLevelChanged()
        {
            float wear = Mathf.Clamp01(HauntManager.HauntLevelAnimated.Value / HauntManager.HauntLevelMax);

            Shader.SetGlobalFloat(_buildingWearShaderId, wear);
        }
    }
}
