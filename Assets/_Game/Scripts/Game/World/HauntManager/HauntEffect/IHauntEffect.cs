﻿namespace Game.World.Haunt
{
    /// <summary>
    /// Manages a single effect for a haunt.
    /// Responsible for monitoring the current haunt level and making changes as necessary
    /// </summary>
    public interface IHauntEffect
    {
        /// <summary>
        /// Initializes the effect. Should only happen once at startup.
        /// </summary>
        void InitializeEffect(IHauntManager hauntManager);

        /// <summary>
        /// Resets the effect. Generally called when the current world is unloaded.
        /// </summary>
        void ResetEffect();

        /// <summary>
        /// Shuts down the effect. Should happen only once on shutdown.
        /// </summary>
        void ShutdownEffect();
    }
}
