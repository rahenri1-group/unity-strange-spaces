﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.World.Haunt.Data;
using System.Threading;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntEffect),
        lifetime: Lifetime.Singleton)]
    public class PassiveHauntIncreaseEffect : BaseHauntEffect
    {
        public float HauntIncreaseDelay = 20f;

        private readonly IEventBus _eventBus;

        private IHauntSpaceData _currentSpaceHauntData;

        private CancellationTokenSource _hauntIncreaseCts;

        public PassiveHauntIncreaseEffect(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            _currentSpaceHauntData = null;

            _eventBus.Subscribe<SpaceActivateEvent>(OnSpaceActivatedEvent);

            _hauntIncreaseCts = new CancellationTokenSource();
            HauntLevelIncrease(_hauntIncreaseCts.Token).Forget();
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            _currentSpaceHauntData = null;

            _eventBus.Unsubscribe<SpaceActivateEvent>(OnSpaceActivatedEvent);

            if (_hauntIncreaseCts != null)
            {
                _hauntIncreaseCts.CancelAndDispose();
                _hauntIncreaseCts = null;
            }
        }

        private void OnSpaceActivatedEvent(SpaceActivateEvent eventArgs)
        {
            _currentSpaceHauntData = eventArgs.SpaceData.GetAdditionalData<IHauntSpaceData>();
        }

        private async UniTask HauntLevelIncrease(CancellationToken cancellationToken)
        {
            int delayMillis = (int) (1000f * HauntIncreaseDelay);
            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Delay(delayMillis, cancellationToken: cancellationToken);

                if (_currentSpaceHauntData != null && _currentSpaceHauntData.PassiveHauntRate > 0)
                {
                    HauntManager.IncreaseHauntLevel(_currentSpaceHauntData.PassiveHauntRate);
                }
            }
        }
    }
}
