﻿using System;
using UnityEngine;

namespace Game.World.Haunt
{
    public abstract partial class BaseHauntSpawnerEffect<T_EntitySpawnData>
    {
        public abstract class EntitySpawnData
        {
            public abstract string Name { get; }

            /// <summary>
            /// The id of the entity
            /// </summary>
            public Guid EntityId { get; private set; }

            public abstract string EntityAddress { get; }

            public abstract float MinHauntLevel { get; }

            /// <summary>
            /// Has the entity been successfully added to the world
            /// </summary>
            public bool HasBeenAddedToWorld { get; set; }
            /// <summary>
            /// Is the entity currently active in the world or has it despawned due to it's space unloading
            /// </summary>
            public bool IsActive { get; private set; }
            /// <summary>
            /// If the entity did unload, when was that?
            /// </summary>
            public float UnloadTime { get; private set; }

            public EntitySpawnData()
            {
                EntityId = Guid.NewGuid();

                HasBeenAddedToWorld = false;
                IsActive = false;
                UnloadTime = 0f;
            }

            public void OnSpawn()
            {
                IsActive = true;
                UnloadTime = 0f;
            }

            public void OnUnload()
            {
                IsActive = false;
                UnloadTime = Time.time;
            }
        }
    }
}
