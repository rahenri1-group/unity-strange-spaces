﻿using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using Game.Core;
using Game.Player;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Game.World.Haunt.Data;
using UnityEngine;

namespace Game.World.Haunt
{
    public abstract partial class BaseHauntSpawnerEffect<T_EntitySpawnData> : BaseHauntEffect
        where T_EntitySpawnData : BaseHauntSpawnerEffect<T_EntitySpawnData>.EntitySpawnData
    {
        protected readonly IDynamicEntityManager EntityManager;
        protected readonly IEventBus EventBus;
        protected readonly ILogRouter Logger;
        protected readonly IPlayerManager PlayerManager;
        protected readonly ISpaceLoader SpaceLoader;
        protected readonly ISpaceManager SpaceManager;
        protected readonly IWorldManager WorldManager;

        /// <summary>
        /// Spawns that are waiting for the haunt level to increase
        /// </summary>
        protected HashSet<T_EntitySpawnData> SpawnsAwaitingHauntLevel;
        /// <summary>
        /// Spawns that have hit their haunt level but need a spawn location
        /// </summary>
        protected HashSet<T_EntitySpawnData> SpawnsAwaitingSpace;
        /// <summary>
        /// Spawns that have hit their haunt level, have a location, and are currently spawning in
        /// </summary>
        protected HashSet<T_EntitySpawnData> SpawnsLoading;
        /// <summary>
        /// Spawns that have hit their haunt level and have been added to the world
        /// </summary>
        protected HashSet<T_EntitySpawnData> AddedSpawns;

        public BaseHauntSpawnerEffect(
            IDynamicEntityManager entityManager,
            IEventBus eventBus,
            ILogRouter logger,
            IPlayerManager playerManager,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager)
        {
            EntityManager = entityManager;
            EventBus = eventBus;
            Logger = logger;
            PlayerManager = playerManager;
            SpaceLoader = spaceLoader;
            SpaceManager = spaceManager;
            WorldManager = worldManager;
        }

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            SpawnsAwaitingHauntLevel = new HashSet<T_EntitySpawnData>();
            SpawnsAwaitingSpace = new HashSet<T_EntitySpawnData>();
            SpawnsLoading = new HashSet<T_EntitySpawnData>();
            AddedSpawns = new HashSet<T_EntitySpawnData>();

            EventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
            EventBus.Subscribe<EntityCreateEvent>(OnEntityCreate);
            EventBus.Subscribe<EntityDestroyEvent>(OnEntityDestroy);

            HauntManager.HauntLevelCurrent.VariableChanged += OnHauntLevelChanged;
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            EventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
            EventBus.Unsubscribe<EntityCreateEvent>(OnEntityCreate);
            EventBus.Unsubscribe<EntityDestroyEvent>(OnEntityDestroy);

            HauntManager.HauntLevelCurrent.VariableChanged -= OnHauntLevelChanged;
        }

        /// <inheritdoc/>
        public override void ResetEffect()
        {
            base.ResetEffect();

            SpawnsAwaitingHauntLevel.Clear();
            SpawnsAwaitingSpace.Clear();

            foreach (var data in SpawnsLoading)
            {
                if (EntityManager.HasRecordForDynamicEntity(data.EntityId))
                {
                    EntityManager.RemoveDynamicEntityRecord(data.EntityId);
                }
            }
            SpawnsLoading.Clear();

            foreach (var data in AddedSpawns)
            {
                EntityManager.RemoveDynamicEntityRecord(data.EntityId);
            }
            AddedSpawns.Clear();
        }

        private void OnHauntLevelChanged()
        {
            var spawnDatas = SpawnsAwaitingHauntLevel
                .Where(m => HauntManager.HauntLevelCurrent.Value >= m.MinHauntLevel)
                .ToHashSet();

            if (spawnDatas.Count == 0) return;

            var playerSpace = SpaceManager.GetEntitySpace(PlayerManager.Player);
            var spawnLocations = GetSpawnLocationInSurroundingSpaces(playerSpace);

            foreach (var data in spawnDatas)
            {
                if (spawnLocations.Count != 0)
                {
                    var spawnLocation = spawnLocations.First();
                    spawnLocations.Remove(spawnLocation);

                    SpawnEntity(data, spawnLocation.Item1, spawnLocation.Item2, string.Empty);
                }
                else
                {
                    SpawnsAwaitingSpace.Add(data);
                }

                SpawnsAwaitingHauntLevel.Remove(data);
            }
        }

        private void OnEntityCreate(EntityCreateEvent eventArgs)
        {
            var spawnData = AddedSpawns.FirstOrDefault(s => s.EntityId == eventArgs.Entity.EntityId);
            spawnData?.OnSpawn();
        }

        private void OnEntityDestroy(EntityDestroyEvent eventArgs)
        {
            var spawnData = AddedSpawns.FirstOrDefault(s => s.EntityId == eventArgs.EntityId);
            spawnData?.OnUnload();
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (eventArgs.Entity != PlayerManager.Player || SpawnsAwaitingSpace.Count == 0)
            {
                return;
            }

            // don't spawn to the space the player just came from
            var spawnLocations = GetSpawnLocationInSurroundingSpaces(eventArgs.NewSpace, eventArgs.OldSpace);

            foreach (var spawnData in spawnLocations)
            {
                if (SpawnsAwaitingSpace.Count == 0) break;

                var data = SpawnsAwaitingSpace.First();
                SpawnsAwaitingSpace.Remove(data);

                SpawnEntity(data, spawnData.Item1, spawnData.Item2, string.Empty);
            }
        }

        protected List<(ISpaceData, HauntSpawnData)> GetSpawnLocationInSurroundingSpaces(ISpaceData playerSpace, ISpaceData ignoreSpace = null)
        {
            var spawnLocations = new List<(ISpaceData, HauntSpawnData)>();
            foreach (var space in WorldManager.CurrentWorld.GetConnectedSpaces(playerSpace))
            {
                if (ignoreSpace != null && SpaceUtil.SpaceEquals(space, ignoreSpace)) continue;

                var spawns = GetAvailableSpawnPositionsForSpace(space);
                foreach(var spawn in spawns)
                {
                    spawnLocations.Add(new (space, spawn));
                }
            }
            spawnLocations.Shuffle();

            return spawnLocations;
        }

        protected abstract HauntSpawnData[] GetAvailableSpawnPositionsForSpace(ISpaceData space);

        protected abstract void OnSpawnLocationSelected(T_EntitySpawnData entityData, ISpaceData spawnSpace, HauntSpawnData spawnData);

        protected void SpawnEntity(T_EntitySpawnData entityData, ISpaceData spawnSpace, HauntSpawnData spawnData, string entityJson)
        {
            Logger.LogInfo($"Spawning haunt {entityData.Name} in {spawnSpace.Name}");

            entityData.HasBeenAddedToWorld = true;
            entityData.OnSpawn();

            OnSpawnLocationSelected(entityData, spawnSpace, spawnData);

            if (!SpaceLoader.IsSpaceLoaded(spawnSpace))
            {
                EntityManager.CreateDynamicEntityRecord(
                    entityData.EntityId,
                    entityData.EntityAddress,
                    spawnSpace,
                    spawnData.Position,
                    spawnData.Rotation,
                    Vector3.one,
                    entityJson);

                AddedSpawns.Add(entityData);
            }
            else
            {
                SpawnsLoading.Add(entityData);
                UniTask.Create(async () =>
                {
                    var entity = await EntityManager.SpawnEntity(
                        entityData.EntityId,
                        entityData.EntityAddress,
                        spawnSpace,
                        spawnData.Position,
                        spawnData.Rotation,
                        Vector3.one,
                        entityJson);

                    if (entity != null && SpawnsLoading.Contains(entityData))
                    {
                        entityData.OnSpawn();

                        SpawnsLoading.Remove(entityData);
                        AddedSpawns.Add(entityData);
                    }
                    else
                    {
                        // reset triggered

                        if (EntityManager.HasRecordForDynamicEntity(entityData.EntityId))
                        {
                            EntityManager.RemoveDynamicEntityRecord(entityData.EntityId);
                        }

                        if (entity != null)
                        {
                            UnityEngine.Object.Destroy(entity.GameObject);
                        }
                    }
                });
            }
        }
    }
}
