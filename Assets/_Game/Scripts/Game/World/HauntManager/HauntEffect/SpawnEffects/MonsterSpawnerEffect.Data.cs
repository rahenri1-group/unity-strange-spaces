﻿using System;
using UnityEngine;

namespace Game.World.Haunt
{
    [Serializable]
    public class MonsterSpawnConfig
    {
        public string Name = string.Empty;

        public float MinHauntLevel = 0f;

        public string EntityAddress = string.Empty;

        public float RespawnDelay = 0f;
    }

    public partial class MonsterSpawnerEffect
    {
        public class MonsterEntitySpawnData : EntitySpawnData
        {
            public override string Name => !string.IsNullOrEmpty(_config.Name) ? _config.Name : _config.EntityAddress;

            public override string EntityAddress => _config.EntityAddress;

            public override float MinHauntLevel => _config.MinHauntLevel;

            /// <summary>
            /// Has the monster been killed (either by the player or environmental hazards)?
            /// Killed monsters will not be repositioned.
            /// </summary>
            public bool HasBeenKilled { get; set; }

            /// <summary>
            /// If the monster has been unloaded, is it ready to respawn?
            /// </summary>
            public bool IsEligibleForRespawn => HasBeenAddedToWorld
                && !IsActive
                && HasBeenKilled == false
                && (Time.time - UnloadTime) >= _config.RespawnDelay;

            private readonly MonsterSpawnConfig _config;

            public MonsterEntitySpawnData(MonsterSpawnConfig config)
                : base()
            {
                _config = config;
                HasBeenKilled = false;
            }
        }
    }
}
