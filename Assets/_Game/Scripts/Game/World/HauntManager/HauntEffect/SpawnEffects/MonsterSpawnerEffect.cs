﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using Game.Events;
using Game.Player;
using Game.World.Haunt.Data;
using System.Linq;
using System.Threading;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntEffect),
        lifetime: Lifetime.Singleton)]
    public partial class MonsterSpawnerEffect : BaseHauntSpawnerEffect<MonsterSpawnerEffect.MonsterEntitySpawnData>
    {
        public float MonsterRespawnTimerDelay = 10.0f;

        public MonsterSpawnConfig[] MonsterSpawnConfigs = new MonsterSpawnConfig[0];

        private CancellationTokenSource _respawnTimerCts;

        public MonsterSpawnerEffect(
            IDynamicEntityManager entityManager,
            IEventBus eventBus,
            ILogRouter logger,
            IPlayerManager playerManager,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager)
            : base(
                  entityManager,
                  eventBus,
                  logger,
                  playerManager,
                  spaceLoader,
                  spaceManager,
                  worldManager)
        { }

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            foreach (var config in MonsterSpawnConfigs)
            {
                SpawnsAwaitingHauntLevel.Add(new MonsterEntitySpawnData(config));
            }

            EventBus.Subscribe<EntityKilledEvent>(OnEntityKilled);

            _respawnTimerCts = new CancellationTokenSource();
            RespawnTimer(_respawnTimerCts.Token).Forget();
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            EventBus.Unsubscribe<EntityKilledEvent>(OnEntityKilled);

            if (_respawnTimerCts != null)
            {
                _respawnTimerCts.CancelAndDispose();
                _respawnTimerCts = null;
            }
        }

        /// <inheritdoc/>
        public override void ResetEffect()
        {
            base.ResetEffect();

            foreach (var config in MonsterSpawnConfigs)
            {
                SpawnsAwaitingHauntLevel.Add(new MonsterEntitySpawnData(config));
            }
        }

        private void OnEntityKilled(EntityKilledEvent eventArgs)
        {
            var monsterData = AddedSpawns.FirstOrDefault(s => s.EntityId == eventArgs.Entity.EntityId);
            if (monsterData != null)
            {
                monsterData.HasBeenKilled = true;
            }
        }

        protected override HauntSpawnData[] GetAvailableSpawnPositionsForSpace(ISpaceData space)
        {
            var hauntData = space.GetAdditionalData<IHauntSpaceData>();
            if (hauntData != null)
            {
                return hauntData.HauntMonsterSpawns;
            }

            return new HauntSpawnData[0];
        }

        protected override void OnSpawnLocationSelected(MonsterEntitySpawnData entityData, ISpaceData spawnSpace, HauntSpawnData spawnData) { }

        private async UniTask RespawnTimer(CancellationToken cancellationToken)
        {
            int delayMillis = (int)(1000f * MonsterRespawnTimerDelay);
            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Delay(delayMillis, cancellationToken: cancellationToken);

                var eligibleMonsters = AddedSpawns
                    .Where(m => m.IsEligibleForRespawn)
                    .ToList();

                if (eligibleMonsters.Count > 0)
                {
                    var playerSpace = SpaceManager.GetEntitySpace(PlayerManager.Player);
                    var spawnLocations = GetSpawnLocationInSurroundingSpaces(playerSpace);

                    foreach (var spawnData in spawnLocations)
                    {
                        if (eligibleMonsters.Count == 0) break;

                        var monsterData = eligibleMonsters.First();
                        eligibleMonsters.Remove(monsterData);

                        var entityJson = EntityManager.GetRecordForDynamicEntity(monsterData.EntityId).SerializedJson;
                        EntityManager.RemoveDynamicEntityRecord(monsterData.EntityId);
                        SpawnEntity(monsterData, spawnData.Item1, spawnData.Item2, entityJson);
                    }
                }
            }
        }
    }
}
