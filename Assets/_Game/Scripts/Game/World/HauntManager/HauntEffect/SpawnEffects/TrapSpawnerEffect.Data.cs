﻿using System;

namespace Game.World.Haunt
{
    [Serializable]
    public class TrapSpawnConfig
    {
        public string Name = string.Empty;

        public float MinHauntLevel = 0f;

        public string EntityAddress = string.Empty;
    }

    public partial class TrapSpawnerEffect
    {
        public class TrapEntitySpawnData : EntitySpawnData
        {
            public override string Name => !string.IsNullOrEmpty(_config.Name) ? _config.Name : _config.EntityAddress;

            public override string EntityAddress => _config.EntityAddress;

            public override float MinHauntLevel => _config.MinHauntLevel;

            private readonly TrapSpawnConfig _config;

            public TrapEntitySpawnData(TrapSpawnConfig config)
                : base()
            { 
                _config = config;
            }
        }
    }
}
