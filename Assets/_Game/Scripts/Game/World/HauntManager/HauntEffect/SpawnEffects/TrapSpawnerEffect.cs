﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Space;
using Game.Player;
using Game.World.Haunt.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntEffect),
        lifetime: Lifetime.Singleton)]
    public partial class TrapSpawnerEffect : BaseHauntSpawnerEffect<TrapSpawnerEffect.TrapEntitySpawnData>
    {
        public TrapSpawnConfig[] TrapSpawnConfigs = new TrapSpawnConfig[0];

        private Dictionary<ISpaceData, List<HauntSpawnData>> _availableSpawnPositions;

        public TrapSpawnerEffect(
            IDynamicEntityManager entityManager,
            IEventBus eventBus,
            ILogRouter logger,
            IPlayerManager playerManager,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IWorldManager worldManager)
            : base(
                  entityManager,
                  eventBus,
                  logger,
                  playerManager,
                  spaceLoader,
                  spaceManager,
                  worldManager)
        { }

        /// <inheritdoc/>
        public override void InitializeEffect(IHauntManager hauntManager)
        {
            base.InitializeEffect(hauntManager);

            _availableSpawnPositions = new Dictionary<ISpaceData, List<HauntSpawnData>>();

            foreach (var config in TrapSpawnConfigs)
            {
                SpawnsAwaitingHauntLevel.Add(new TrapEntitySpawnData(config));
            }
        }

        /// <inheritdoc/>
        public override void ResetEffect()
        {
            base.ResetEffect();

            _availableSpawnPositions.Clear();

            foreach (var config in TrapSpawnConfigs)
            {
                SpawnsAwaitingHauntLevel.Add(new TrapEntitySpawnData(config));
            }
        }

        /// <inheritdoc/>
        public override void ShutdownEffect()
        {
            base.ShutdownEffect();

            _availableSpawnPositions.Clear();
        }

        protected override HauntSpawnData[] GetAvailableSpawnPositionsForSpace(ISpaceData space)
        {
            if (!_availableSpawnPositions.ContainsKey(space))
            {
                var trapSpawnLocations = new List<HauntSpawnData>();

                var hauntData = space.GetAdditionalData<IHauntSpaceData>();
                if (hauntData != null)
                {
                    for (int i = 0; i < Mathf.Min(hauntData.HauntTrapSpawns.Length, hauntData.MaxTraps); i++)
                    {
                        var trapSpawn = hauntData.HauntTrapSpawns[i];
                        trapSpawnLocations.Add(trapSpawn);
                    }

                    trapSpawnLocations.Shuffle();
                }

                _availableSpawnPositions[space] = trapSpawnLocations;
            }

            return _availableSpawnPositions[space].ToArray();
        }

        protected override void OnSpawnLocationSelected(TrapEntitySpawnData entityData, ISpaceData spawnSpace, HauntSpawnData spawnData)
        {
            _availableSpawnPositions[spawnSpace].Remove(spawnData);
        }
    }
}
