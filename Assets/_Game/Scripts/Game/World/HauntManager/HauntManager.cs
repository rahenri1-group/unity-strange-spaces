﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Events;
using System;
using System.Threading;
using UnityEngine;

namespace Game.World.Haunt
{
    [Dependency(
        contract: typeof(IHauntManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class HauntManager : BaseModule, IHauntManager
    {
        public override string ModuleName => "Haunt Manager";

        public override ModuleConfig ModuleConfig => Config;
        public HauntManagerConfig Config = new HauntManagerConfig();

        public int HauntLevelMax => Config.MaxHauntLevel;
        public IReadonlyVariable<int> HauntLevelCurrent => _hauntLevelCurrent;
        public IReadonlyVariable<float> HauntLevelAnimated => _hauntLevelAnimated;

        private readonly IEventBus _eventBus;
        private readonly IVariableRegistry _variableRegistry;
        private readonly IHauntEffect[] _hauntEffects;

        private IVariable<int> _hauntLevelCurrent;
        private IVariable<float> _hauntLevelAnimated;

        private CancellationTokenSource _hauntLevelTransitionCts;

        public HauntManager(
            IEventBus eventBus,
            IHauntEffect[] hauntEffects,
            ILogRouter logger,
            IVariableRegistry variableRegistry)
            : base (logger)
        {
            _eventBus = eventBus;
            _variableRegistry = variableRegistry;

            _hauntEffects = hauntEffects;
        }

        public override UniTask Initialize()
        {
            _hauntLevelCurrent = _variableRegistry.GetVariableById<int>(Guid.Parse(Config.HauntLevelCurrentId));
            _hauntLevelCurrent.Value = 0;

            _hauntLevelAnimated = _variableRegistry.GetVariableById<float>(Guid.Parse(Config.HauntLevelAnimatedId));
            _hauntLevelAnimated.Value = 0f;

            _hauntLevelTransitionCts = null;

            HauntLevelCurrent.VariableChanged += OnHauntLevelChanged;

            foreach (var hauntEffect in _hauntEffects)
            {
                hauntEffect.InitializeEffect(this);
            }

            _eventBus.Subscribe<WorldLoadedEvent>(OnWorldLoaded);

            return base.Initialize();
        }

        public override UniTask Shutdown()
        {
            HauntLevelCurrent.VariableChanged -= OnHauntLevelChanged;

            if (_hauntLevelTransitionCts != null)
            {
                _hauntLevelTransitionCts.CancelAndDispose();
                _hauntLevelTransitionCts = null;
            }

            foreach (var hauntEffect in _hauntEffects)
            {
                hauntEffect.ShutdownEffect();
            }

            _eventBus.Unsubscribe<WorldLoadedEvent>(OnWorldLoaded);

            return base.Shutdown();
        }

        private void OnWorldLoaded(WorldLoadedEvent eventArgs)
        {
            if (_hauntLevelTransitionCts != null)
            {
                _hauntLevelTransitionCts.CancelAndDispose();
                _hauntLevelTransitionCts = null;
            }

            _hauntLevelAnimated.Value = 0f;
            _hauntLevelCurrent.Value = 0;

            foreach (var hauntEffect in _hauntEffects)
            {
                hauntEffect.ResetEffect();
            }
        }

        private void OnHauntLevelChanged()
        {
            if (_hauntLevelTransitionCts != null)
            {
                _hauntLevelTransitionCts.CancelAndDispose();
            }

            _hauntLevelTransitionCts = new CancellationTokenSource();

            TransitionHauntLevel(HauntLevelAnimated.Value, HauntLevelCurrent.Value, _hauntLevelTransitionCts.Token).Forget();
        }

        public void IncreaseHauntLevel(int hauntLevelIncrease)
        {
            if (hauntLevelIncrease <= 0)
            {
                Logger.LogWarning($"Invalid haunt increase amount '{hauntLevelIncrease}'");
                return;
            }

            _hauntLevelCurrent.Value = Mathf.Clamp(HauntLevelCurrent.Value + hauntLevelIncrease, 0, HauntLevelMax);
        }

        private async UniTask TransitionHauntLevel(float startHauntLevel, float endHauntLevel, CancellationToken cancellationToken)
        {
            if (startHauntLevel == endHauntLevel) return;

            float startTime = Time.time;

            try
            {
                while (Time.time - startTime < Config.HauntTransitionDuration)
                {
                    float lerp = Mathf.Clamp01((Time.time - startTime) / Config.HauntTransitionDuration);

                    _hauntLevelAnimated.Value = Mathf.SmoothStep(startHauntLevel, endHauntLevel, lerp);

                    await UniTask.DelayFrame(1, PlayerLoopTiming.Update, cancellationToken);
                }

                _hauntLevelAnimated.Value = endHauntLevel;
            }
            catch (OperationCanceledException)
            {
                return;
            }
        }
    }
}
