﻿using Game.Core;

namespace Game.World.Haunt
{
    public class HauntManagerConfig : ModuleConfig
    {
        public int MaxHauntLevel = 100;

        /// <summary>
        /// The current haunt level
        /// </summary>
        public string HauntLevelCurrentId = string.Empty;

        /// <summary>
        /// A transition version of the current haunt level. 
        /// When the haunt level changes, this variable will transition from the value of the old haunt level to the value of the new haunt level.
        /// </summary>
        public string HauntLevelAnimatedId = string.Empty;

        public float HauntTransitionDuration = 1f;
    }
}
