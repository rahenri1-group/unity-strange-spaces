﻿using Game.Core;

namespace Game.World.Haunt
{
    public interface IHauntManager : IModule
    {
        int HauntLevelMax { get; }

        IReadonlyVariable<int> HauntLevelCurrent { get; }
        IReadonlyVariable<float> HauntLevelAnimated { get; }

        void IncreaseHauntLevel(int hauntLevelIncrease);
    }
}
