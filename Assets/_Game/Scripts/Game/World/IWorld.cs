﻿using Game.Core.Space;

namespace Game.World
{
    /// <summary>
    /// Structure that defines the game world
    /// </summary>
    public interface IWorld
    {
        /// <summary>
        /// The entry space for the world
        /// </summary>
        ISpaceData WorldEntranceSpace { get; }

        /// <summary>
        /// All spaces that are a part of the world.
        /// The spaces may or may not be loaded.
        /// </summary>
        ISpaceData[] WorldSpaces { get; }

        /// <summary>
        /// All spaces with a portal connection to the provided <paramref name="space"/>.
        /// The spaces may or may not be loaded.
        /// </summary>
        ISpaceData[] GetConnectedSpaces(ISpaceData space);
    }
}
