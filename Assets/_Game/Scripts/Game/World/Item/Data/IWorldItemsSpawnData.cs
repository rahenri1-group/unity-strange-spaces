﻿using Game.Core.Space;

namespace Game.World.Item.Data
{
    public interface IWorldItemsSpawnData : ISpaceDataComponent
    {
        public ItemSpawnData[] ItemSpawns { get; }
    }
}
