﻿using Game.Item;
using System;
using UnityEngine;

namespace Game.World.Item.Data
{
    [Serializable]
    public class ItemSpawnData
    {
        public Vector3 Position => _position;
        public Quaternion Rotation => _rotation;
        public bool FixedSpawnRotation => _fixedSpawnRotation;

        public ItemSpawnGroup[] ItemSpawnGroups => _allowedItemGroups;

        [SerializeField] private Vector3 _position = Vector3.zero;
        [SerializeField] private Quaternion _rotation = Quaternion.identity;
        [SerializeField] private bool _fixedSpawnRotation = false;

        [SerializeField] private ItemSpawnGroup[] _allowedItemGroups = new ItemSpawnGroup[0];
    }
}
