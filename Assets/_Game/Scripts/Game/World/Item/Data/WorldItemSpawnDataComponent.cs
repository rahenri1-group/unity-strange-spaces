﻿using System;
using UnityEngine;

namespace Game.World.Item.Data
{
    [Serializable]
    public class WorldItemSpawnDataComponent : IWorldItemsSpawnData
    {
        public ItemSpawnData[] ItemSpawns => _itemSpawns;

        [SerializeField] private ItemSpawnData[] _itemSpawns = new ItemSpawnData[0];
    }
}
