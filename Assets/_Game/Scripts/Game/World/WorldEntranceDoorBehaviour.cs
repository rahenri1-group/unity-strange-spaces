﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Interaction;
using Game.Core.Physics;
using Game.Core.Space;
using Game.Player;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.World
{
    public class WorldEntranceDoorBehaviour : InjectedBehaviour
    {
        [Inject] private IEventBus _eventBus = null;
        [Inject] private IPlayerManager _playerManager = null;
        [Inject] private ISpaceManager _spaceManager = null;
        [Inject] private IWorldManager _worldManager = null;

        [SerializeField] [TypeRestriction(typeof(IHinge))] private Component[] _hingeObjs = null;
        private IHinge[] _hinges;

        [SerializeField] [TypeRestriction(typeof(IInteractable))] private Component[] _doorHandleObjs = null;
        private IInteractable[] _doorHandles;

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_hingeObjs);
            Assert.IsTrue(_hingeObjs.Length > 0);
            _hinges = _hingeObjs.GetComponentArrayAsserted<IHinge>();

            Assert.IsNotNull(_doorHandleObjs);
            Assert.IsTrue(_doorHandleObjs.Length > 0);
            _doorHandles = _doorHandleObjs.GetComponentArrayAsserted<IInteractable>();

            if (!_worldManager.IsWorldEntrancePortalConnected)
            {
                SetDoorEnabled(false);
            }
        }

        private void OnEnable()
        {
            foreach (var hinge in _hinges)
            {
                hinge.Close += OnHingeClose;
            }

            _eventBus.Subscribe<PortalOpenedEvent>(OnPortalOpened);
        }

        private void OnDisable()
        {
            foreach (var hinge in _hinges)
            {
                hinge.Close -= OnHingeClose;
            }

            _eventBus.Unsubscribe<PortalOpenedEvent>(OnPortalOpened);
        }

        private void OnHingeClose(IHinge sender, HingeEventArgs args)
        {
            bool allHingesClosed = true;
            foreach (var hinge in _hinges)
            {
                if (!hinge.IsClosed)
                {
                    allHingesClosed = false;
                    break;
                }
            }

            if (!allHingesClosed) return;
            if (_playerManager.Player == null) return;

            var playerSpace = _spaceManager.GetEntitySpace(_playerManager.Player);
            if (!_worldManager.CurrentWorld.WorldSpaces.Contains(playerSpace)) return; // player has not entered the world

            _worldManager.DestroyWorldEntrancePortalConnection().Forget();

            SetDoorEnabled(false);
        }

        private void OnPortalOpened(PortalOpenedEvent eventData)
        {
            if (_worldManager.IsWorldEntrancePortalConnected)
            {
                SetDoorEnabled(true);
            }
        }

        private void SetDoorEnabled(bool doorEnabled)
        {
            foreach (var doorHandle in _doorHandles)
            {
                doorHandle.GameObject.SetActive(doorEnabled);
            }
        }
    }
}
