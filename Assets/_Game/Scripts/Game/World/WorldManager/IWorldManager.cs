﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Portal;

namespace Game.World
{
    public interface IWorldManager : IModule
    {
        bool IsWorldLoaded { get; }
        bool IsWorldEntrancePortalConnected { get; }
        IWorld CurrentWorld { get; }

        UniTask CreateNewWorld();
        UniTask ConnectPortalToWorldEntrance(IDynamicPortal portal);
        UniTask DestroyWorldEntrancePortalConnection();

        UniTask DestroyCurrentWorld();
    }
}
