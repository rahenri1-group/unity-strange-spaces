﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity;
using Game.Core.Event;
using Game.Core.Portal;
using Game.Core.Space;
using Game.Events;
using Game.Player;
using Game.World.Builder;
using System.Linq;

namespace Game.World
{
    [Dependency(
        contract: typeof(IWorldManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class WorldManager : BaseModule, IWorldManager
    {
        public override string ModuleName => "World Manager";

        public override ModuleConfig ModuleConfig => Config;
        public WorldManagerConfig Config = new WorldManagerConfig();

        public bool IsWorldLoaded => _currentWorld != null;
        public bool IsWorldEntrancePortalConnected => _worldEntrancePortalConnection != null;
        public IWorld CurrentWorld => _currentWorld;

        private readonly IDynamicEntityManager _dynamicEntityManager;
        private readonly IDynamicPortalManager _dynamicPortalManager;
        private readonly IEventBus _eventBus;
        private readonly ISpaceLoader _spaceLoader;
        private readonly ISpaceManager _spaceManager;
        private readonly IStaticEntityManager _staticEntityManager;
        private readonly IWorldBuilder _worldBuilder;

        private WorldDefinition _currentWorld;
        private IDynamicPortalConnection _worldEntrancePortalConnection;

        public WorldManager(
            IDynamicEntityManager dynamicEntityManager,
            IDynamicPortalManager dynamicPortalManager,
            IEventBus eventBus,
            ILogRouter logger,
            ISpaceLoader spaceLoader,
            ISpaceManager spaceManager,
            IStaticEntityManager staticEntityManager,
            IWorldBuilder worldBuilder)
            : base(logger) 
        {
            _dynamicEntityManager = dynamicEntityManager;
            _dynamicPortalManager = dynamicPortalManager;
            _eventBus = eventBus;
            _spaceLoader = spaceLoader;
            _spaceManager = spaceManager;
            _staticEntityManager = staticEntityManager;
            _worldBuilder = worldBuilder;

            _currentWorld = null;
            _worldEntrancePortalConnection = null;
        }

        public async override UniTask Initialize()
        {
            await _worldBuilder.LoadBuilderResources();

            if (Config.CreateWorldOnInitialization)
            {
                await CreateNewWorld();
            }

            _eventBus.Subscribe<EntityCreateEvent>(OnEntityCreate);
            _eventBus.Subscribe<EntityPostTeleportEvent>(OnEntityTeleport);
            _eventBus.Subscribe<SpaceLoadedEvent>(OnSpaceLoaded);

            await base.Initialize();
        }

        public async override UniTask Shutdown()
        {
            _eventBus.Unsubscribe<EntityCreateEvent>(OnEntityCreate);
            _eventBus.Unsubscribe<EntityPostTeleportEvent>(OnEntityTeleport);
            _eventBus.Unsubscribe<SpaceLoadedEvent>(OnSpaceLoaded);

            await _worldBuilder.ReleaseBuilderResources();

            await base.Shutdown();
        }

        public async UniTask CreateNewWorld()
        {
            if (IsWorldLoaded)
            {
                ModuleLogError("Cannot create a new world while one currently exists");
                return;
            }

            WorldDefinition worldDefinition = null;
            if (Config.WorldSeed <= 0)
                worldDefinition = await _worldBuilder.CreateWorldDefinition();
            else
                worldDefinition = await _worldBuilder.CreateWorldDefinition(Config.WorldSeed);

            _currentWorld = worldDefinition;

            foreach (var portalConnection in _currentWorld.BuilderPortalConnections)
            {
                _dynamicPortalManager.RegisterDynamicPortalConnection(portalConnection);
            }

            foreach (var entity in _currentWorld.BuilderEntities)
            {
                _dynamicEntityManager.CreateDynamicEntityRecord(
                    entity.Id,
                    entity.AssetKey,
                    entity.Space,
                    entity.Position,
                    entity.Rotation,
                    entity.Scale,
                    entity.Json
                 );
            }

            ModuleLogInfo("World loaded");
            _eventBus.InvokeEvent(new WorldLoadedEvent());
        }

        public UniTask ConnectPortalToWorldEntrance(IDynamicPortal portal)
        {
            if (_currentWorld == null)
            {
                ModuleLogError("Cannot connect portal to entrance. No world has been created.");
                return UniTask.CompletedTask;
            }

            if (_worldEntrancePortalConnection != null)
            {
                ModuleLogError("Cannot connect portal to entrance. World entrance portal connection already exists");
                return UniTask.CompletedTask;
            }

            _worldEntrancePortalConnection = new DynamicPortalConnection(portal, _currentWorld.WorldEntrancePortalId, _currentWorld.WorldEntranceSpace.SpaceId);

            _dynamicPortalManager.AttemptOpenDynamicPortalConnection(_worldEntrancePortalConnection);

            return UniTask.CompletedTask;
        }

        public UniTask DestroyWorldEntrancePortalConnection()
        {
            if (_worldEntrancePortalConnection != null)
            {
                _dynamicPortalManager.UnregisterDynamicPortalConnection(_worldEntrancePortalConnection);
                _worldEntrancePortalConnection = null;
            }

            return UniTask.CompletedTask;
        }

        public async UniTask DestroyCurrentWorld()
        {
            if (!IsWorldLoaded)
            {
                ModuleLogError("No current world to destroy");
                return;
            }
            else if (_currentWorld.WorldSpaces.Contains(_spaceManager.ActiveSpace))
            {
                ModuleLogError("Change active space to non-world space before attempting to unload");
                return;
            }

            // unload all world spaces
            var loadedWorldSpaces = _currentWorld.WorldSpaces.Intersect(_spaceLoader.LoadedSpaces).ToArray();
            foreach (var worldSpace in loadedWorldSpaces)
            {
                await _spaceLoader.UnloadSpace(worldSpace);
            }

            // clear all portal connections
            foreach (var portalConnection in _currentWorld.BuilderPortalConnections)
            {
                _dynamicPortalManager.UnregisterDynamicPortalConnection(portalConnection);
            }

            await DestroyWorldEntrancePortalConnection();

            // clear out all entities
            foreach (var worldSpace in _currentWorld.WorldSpaces)
            {
                _dynamicEntityManager.RemoveDynamicEntityRecordsForSpace(worldSpace);
                _staticEntityManager.RemoveStaticEntityRecordsForSpace(worldSpace);
            }

            _currentWorld = null;

            ModuleLogInfo("World unloaded");
            _eventBus.InvokeEvent(new WorldUnloadedEvent());
        }

        private void OnEntityCreate(EntityCreateEvent eventArgs)
        {
            if (!IsWorldLoaded) return;

            var entity = eventArgs.Entity;
            if (entity is IPlayer)
            {
                OpenConnectionPortalsForSpace(_spaceManager.GetEntitySpace(entity));
            }
        }

        private void OnEntityTeleport(EntityPostTeleportEvent eventArgs)
        {
            if (!IsWorldLoaded) return;

            var entity = eventArgs.Entity;
            if (entity is IPlayer)
            {
                OpenConnectionPortalsForSpace(_spaceManager.GetEntitySpace(entity));
            }
        }

        private void OnSpaceLoaded(SpaceLoadedEvent eventArgs)
        {
            if (!IsWorldLoaded) return;

            var space = eventArgs.SpaceData;
            var connections = _currentWorld.ConnectionsForSpace(space);

            // If both ends of a connection are loaded, we should open the portals if they are not already openned
            foreach (var connection in connections)
            {
                var otherSpace = space.SpaceId != connection.SpaceIdA ? connection.SpaceIdA : connection.SpaceIdB;

                if (_spaceLoader.IsSpaceLoaded(otherSpace) 
                    && !_dynamicPortalManager.IsPortalConnectionOpen(connection))
                {
                    _dynamicPortalManager.AttemptOpenDynamicPortalConnection(connection);
                }
            }
        }

        private void OpenConnectionPortalsForSpace(ISpaceData space)
        {
            var connections = _currentWorld.ConnectionsForSpace(space);
            _dynamicPortalManager.AttemptOpenDynamicPortalConnections(connections);
        }
    }
}
