﻿using Game.Core;
using System;

namespace Game.World
{
    [Serializable]
    public class WorldManagerConfig : ModuleConfig
    {
        public bool CreateWorldOnInitialization = false;
        public int WorldSeed = 0;
    }
}
