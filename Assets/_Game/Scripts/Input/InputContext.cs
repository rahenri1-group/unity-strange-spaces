﻿namespace Game.Input
{
    public struct InputContext
    {
        public double StartTime { get; }
        public double Duration { get; }
        public double Time { get; }
        
        public bool Started { get; }
        public bool Performed { get; }
        public bool Canceled { get; }

        public InputContext(double startTime, double duration, double time, bool started, bool performed, bool canceled)
        {
            StartTime = startTime;
            Duration = duration;
            Time = time;

            Started = started;
            Performed = performed;
            Canceled = canceled;
        }
    }
}
