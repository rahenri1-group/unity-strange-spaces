﻿using Game.Core.DependencyInjection;
using Game.Core.Input;

namespace Game.Input
{
    [Dependency(
    contract: typeof(BaseInputGroup),
    lifetime: Lifetime.Singleton)]
    [Dependency(
    contract: typeof(IInputGroup),
    lifetime: Lifetime.Singleton)]
    public abstract class BaseInputGroup : IInputGroup
    {
        public abstract bool InputEnabled { get; set; }

        protected InputManager InputManager { get; private set; }

        public virtual void InitializeInputGroup(InputManager inputManager)
        {
            InputManager = inputManager;
        }

        public virtual void ShutdownInputGroup()
        {

        }
    }
}
