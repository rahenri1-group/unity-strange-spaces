﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IDeveloperInput),
    lifetime: Lifetime.Singleton)]
    public class DeveloperInputGroup : BaseInputGroup, IDeveloperInput
    {
        public override bool InputEnabled
        {
            get => InputManager.Input.Developer.enabled;
            set
            {
                if (value) InputManager.Input.Developer.Enable();
                else InputManager.Input.Developer.Disable();
            }
        }

        public event Action<InputContext, bool> DeveloperConsoleToggleTriggered;

        public Vector2 NavigateActionValue => InputManager.Input.UI.Navigate.ReadValue<Vector2>();
        public event Action<InputContext, Vector2> NavigateActionTriggered;

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            InputManager.Input.Developer.ToggleDeveloperConsole.performed += OnDeveloperConsoleToggleTriggered;
            InputManager.Input.Developer.ToggleDeveloperConsole.canceled += OnDeveloperConsoleToggleTriggered;
            InputManager.Input.Developer.NavigateConsole.performed += OnConsoleNavigateActionTriggered;
        }

        public override void ShutdownInputGroup()
        {
            base.ShutdownInputGroup();

            InputManager.Input.Developer.ToggleDeveloperConsole.performed -= OnDeveloperConsoleToggleTriggered;
            InputManager.Input.Developer.ToggleDeveloperConsole.canceled -= OnDeveloperConsoleToggleTriggered;
            InputManager.Input.Developer.NavigateConsole.performed -= OnConsoleNavigateActionTriggered;
        }

        private void OnDeveloperConsoleToggleTriggered(InputAction.CallbackContext context)
        {
            if (DeveloperConsoleToggleTriggered == null) return;

            DeveloperConsoleToggleTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                context.ReadValue<float>() > 0f);
        }

        private void OnConsoleNavigateActionTriggered(InputAction.CallbackContext context)
        {
            if (NavigateActionTriggered == null) return;

            NavigateActionTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                context.ReadValue<Vector2>());
        }
    }
}
