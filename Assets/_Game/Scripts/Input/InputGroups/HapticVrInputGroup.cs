﻿using Game.Core.DependencyInjection;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR.OpenXR.Input;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IHapticVrInput),
    lifetime: Lifetime.Singleton)]
    public class HapticVrInputGroup : BaseInputGroup, IHapticVrInput
    {
        private class HapticVrHand : IHapticVrHand
        {
            private bool _isLeftHand;
            private InputAction _hapticAction;

            public HapticVrHand(bool isLeftHand, InputAction hapticAction)
            {
                _isLeftHand = isLeftHand;
                _hapticAction = hapticAction;
            }

            public void SendHapticPulse(float amplitude, float frequency, float duration)
            {
                var contoller = (_isLeftHand) ? XRController.leftHand : XRController.rightHand;

                OpenXRInput.SendHapticImpulse(_hapticAction, amplitude, frequency, duration, contoller);
            }
        }

        public IHapticVrHand HandLeft => _handLeft;
        public IHapticVrHand HandRight => _handRight;

        public override bool InputEnabled
        {
            get => InputManager.Input.HapticVR.enabled;
            set
            {
                if (value) InputManager.Input.HapticVR.Enable();
                else InputManager.Input.HapticVR.Disable();
            }
        }

        private HapticVrHand _handLeft;
        private HapticVrHand _handRight;

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            _handLeft = new HapticVrHand(true, InputManager.Input.HapticVR.HapticLeftHand);
            _handRight = new HapticVrHand(false, InputManager.Input.HapticVR.HapticRightHand);
        }
    }
}
