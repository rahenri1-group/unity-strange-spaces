﻿using Game.Core.Input;
using System;
using UnityEngine;

namespace Game.Input
{
    public interface IDeveloperInput : IInputGroup
    {
        event Action<InputContext, bool> DeveloperConsoleToggleTriggered;

        Vector2 NavigateActionValue { get; }
        event Action<InputContext, Vector2> NavigateActionTriggered;
    }
}
