﻿using Game.Core.Input;

namespace Game.Input
{
    public interface IHapticVrHand
    {
        void SendHapticPulse(float amplitude, float frequency, float duration);
    }

    public interface IHapticVrInput : IInputGroup
    {
        IHapticVrHand HandLeft { get; }
        IHapticVrHand HandRight { get; }
    }
}
