﻿using System;

namespace Game.Input
{
    public interface IItemInput
    {
        bool ItemUseActionValue { get; }
        event Action<InputContext, bool> UseItemActionTriggered;

        bool ItemAlternateActionValue { get; }
        event Action<InputContext, bool> ItemAlternateActionTriggered;

        float ItemShootActionValue { get; }

        event Action<InputContext> ReleaseItemActionTriggered;
    }
}
