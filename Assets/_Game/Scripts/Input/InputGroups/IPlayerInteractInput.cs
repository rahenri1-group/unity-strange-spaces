﻿using System;

namespace Game.Input
{
    public interface IPlayerInteractInput
    {
        bool InteractActionValue { get; }
        event Action<InputContext, bool> InteractActionTriggered;
    }
}
