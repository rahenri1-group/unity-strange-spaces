﻿using Game.Core.Input;
using System;

namespace Game.Input
{
    public interface IPlayerInventoryInput : IInputGroup
    {
        event Action<InputContext, bool> PlayerInventoryToggleTriggered;
    }
}
