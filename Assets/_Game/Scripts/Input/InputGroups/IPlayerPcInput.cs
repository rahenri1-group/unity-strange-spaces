﻿using Game.Core.Input;
using System;
using UnityEngine;

namespace Game.Input
{
    public interface IPlayerPcInput : IInputGroup, IPlayerInteractInput
    {
        Vector2 PlayerMoveActionValue { get; }

        Vector2 PlayerLookActionValue { get; }
        event Action<InputContext, Vector2> PlayerLookActionTriggered;

        bool PlayerFlashlightActionValue { get; }
        event Action<InputContext, bool> PlayerFlashlightActionTriggered;
    }
}
