﻿using Game.Core.Input;
using System;
using UnityEngine;

namespace Game.Input
{
    public interface IPlayerVrHandInput : IItemInput, IPlayerInteractInput { }

    public interface IPlayerVrInput : IInputGroup
    {
        IPlayerVrHandInput HandLeft { get; }
        IPlayerVrHandInput HandRight { get; }

        Vector2 PlayerSmoothMoveActionValue { get; }

        event Action<InputContext> PlayerSnapLeftTriggered;
        event Action<InputContext> PlayerSnapRightTriggered;
    }
}
