﻿using Game.Core.Input;
using UnityEngine;

namespace Game.Input
{
    public interface IUiInputGroup : IInputGroup
    {
        Vector2 PointerValue { get; }
    }
}
