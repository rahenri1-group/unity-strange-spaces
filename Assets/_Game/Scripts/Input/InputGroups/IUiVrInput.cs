﻿using Game.Core.Input;
using System;

namespace Game.Input
{
    /// <summary>
    /// Input for the ui popup for a vr player
    /// </summary>
    public interface IUiVrInput : IInputGroup
    {
        event Action<InputContext> SelectLeftTriggered;
        event Action<InputContext> SelectRightTriggered;

        event Action<InputContext> MenuToggleTriggered;
    }
}