﻿using Game.Core.Input;
using UnityEngine;

namespace Game.Input
{
    public interface IVrTrackedDevice
    {
        bool Tracked { get; }

        Vector3 Position { get; }
        Quaternion Rotation { get; }
    }

    public interface IVrTrackedEye
    {
        Vector3 Position { get; }
        Quaternion Rotation { get; }
    }

    public interface IVrTrackedHand : IVrTrackedDevice
    {
        float Grip { get; }
    }

    public interface IVrTrackingInput : IInputGroup
    {
        IVrTrackedDevice Hmd { get; }

        IVrTrackedEye EyeLeft { get; }
        IVrTrackedEye EyeRight { get; }

        IVrTrackedHand HandLeft { get; }
        IVrTrackedHand HandRight { get; }
    }
}
