﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine.InputSystem;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IPlayerInventoryInput),
    lifetime: Lifetime.Singleton)]
    public class PlayerInventoryInputGroup : BaseInputGroup, IPlayerInventoryInput
    {
        public override bool InputEnabled
        {
            get => InputManager.Input.PlayerInventory.enabled;
            set
            {
                if (value) InputManager.Input.PlayerInventory.Enable();
                else InputManager.Input.PlayerInventory.Disable();
            }
        }

        public event Action<InputContext, bool> PlayerInventoryToggleTriggered;

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            InputManager.Input.PlayerInventory.ToggleInventory.performed += OnPlayerInventoryActionTriggered;
            InputManager.Input.PlayerInventory.ToggleInventory.canceled += OnPlayerInventoryActionTriggered;
        }

        public override void ShutdownInputGroup()
        {
            base.ShutdownInputGroup();

            InputManager.Input.PlayerInventory.ToggleInventory.performed -= OnPlayerInventoryActionTriggered;
            InputManager.Input.PlayerInventory.ToggleInventory.canceled -= OnPlayerInventoryActionTriggered;
        }

        private void OnPlayerInventoryActionTriggered(InputAction.CallbackContext context)
        {
            if (PlayerInventoryToggleTriggered == null) return;

            PlayerInventoryToggleTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                context.ReadValue<float>() > 0f);
        }
    }
}
