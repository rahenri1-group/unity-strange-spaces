﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Storage.Settings;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IPlayerPcInput),
    lifetime: Lifetime.Singleton)]
    public class PlayerPcInputGroup : BaseInputGroup, IPlayerPcInput
    {
        public override bool InputEnabled
        {
            get => InputManager.Input.PlayerPC.enabled;
            set
            {
                if (value) InputManager.Input.PlayerPC.Enable();
                else InputManager.Input.PlayerPC.Disable();
            }
        }

        public Vector2 PlayerMoveActionValue => InputManager.Input.PlayerPC.Move.ReadValue<Vector2>();

        public Vector2 PlayerLookActionValue {
            get
            {
                var look = InputManager.Input.PlayerPC.Look.ReadValue<Vector2>();
                if (_inputSettingsGroup.InvertMouse)
                {
                    look.y = -look.y;
                }
                look *= _inputSettingsGroup.MouseSensitivity;
                return look;
            }
        }
        public event Action<InputContext, Vector2> PlayerLookActionTriggered;

        public bool InteractActionValue => InputManager.Input.PlayerPC.Interact.ReadValue<float>() > 0f;
        public event Action<InputContext, bool> InteractActionTriggered;

        public bool PlayerFlashlightActionValue => InputManager.Input.PlayerPC.Flashlight.ReadValue<float>() > 0f;
        public event Action<InputContext, bool> PlayerFlashlightActionTriggered;

        private readonly IEventBus _eventBus;
        private readonly ISettingsManager _settingsManager;

        private IInputPcSettingsGroup _inputSettingsGroup;

        public PlayerPcInputGroup(IEventBus eventBus, ISettingsManager settingsManager)
        {
            _eventBus = eventBus;
            _settingsManager = settingsManager;
            _inputSettingsGroup = null;
        }

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputPcSettingsGroup>();

            InputManager.Input.PlayerPC.Look.performed += OnPlayerLookActionTriggered;
            InputManager.Input.PlayerPC.Interact.performed += OnPlayerInteractActionTriggered;
            InputManager.Input.PlayerPC.Interact.canceled += OnPlayerInteractActionTriggered;
            InputManager.Input.PlayerPC.Flashlight.performed += OnPlayerFlashlightActionTriggered;
            InputManager.Input.PlayerPC.Flashlight.canceled += OnPlayerFlashlightActionTriggered;

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        public override void ShutdownInputGroup()
        {
            base.ShutdownInputGroup();

            _inputSettingsGroup = null;

            InputManager.Input.PlayerPC.Look.performed -= OnPlayerLookActionTriggered;
            InputManager.Input.PlayerPC.Interact.performed -= OnPlayerInteractActionTriggered;
            InputManager.Input.PlayerPC.Interact.canceled -= OnPlayerInteractActionTriggered;
            InputManager.Input.PlayerPC.Flashlight.performed -= OnPlayerFlashlightActionTriggered;
            InputManager.Input.PlayerPC.Flashlight.canceled -= OnPlayerFlashlightActionTriggered;

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventArgs)
        {
            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputPcSettingsGroup>();
        }

        private void OnPlayerLookActionTriggered(InputAction.CallbackContext context)
        {
            if (PlayerLookActionTriggered == null) return;

            PlayerLookActionTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                PlayerLookActionValue);
        }

        private void OnPlayerInteractActionTriggered(InputAction.CallbackContext context)
        {
            if (InteractActionTriggered == null) return;

            InteractActionTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                context.ReadValue<float>() > 0f);
        }

        private void OnPlayerFlashlightActionTriggered(InputAction.CallbackContext context)
        {
            if (PlayerFlashlightActionTriggered == null) return;

            PlayerFlashlightActionTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ),
                context.ReadValue<float>() > 0f);
        }
    }
}
