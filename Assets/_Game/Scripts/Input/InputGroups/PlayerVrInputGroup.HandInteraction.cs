﻿using System;
using UnityEngine.InputSystem;

namespace Game.Input
{
    public partial class PlayerVrInputGroup : BaseInputGroup
    {
        private class HandInput : IPlayerVrHandInput
        {
            public bool InteractActionValue { get; private set; } = false;
            public event Action<InputContext, bool> InteractActionTriggered;

            public bool ItemUseActionValue { get; private set; } = false;
            public event Action<InputContext, bool> UseItemActionTriggered;

            public bool ItemAlternateActionValue { get; private set; } = false;
            public event Action<InputContext, bool> ItemAlternateActionTriggered;

            public float ItemShootActionValue => _itemShootAction.ReadValue<float>();

            public event Action<InputContext> ReleaseItemActionTriggered;

            private readonly InputAction _interactAction;
            private readonly InputAction _itemUseAction;
            private readonly InputAction _itemReleaseAction;
            private readonly InputAction _itemAlternateAction;
            private readonly InputAction _itemShootAction;

            public HandInput(
                InputAction interactAction, 
                InputAction itemUseAction,
                InputAction itemReleaseAction,
                InputAction itemAlternateAction,
                InputAction itemShootAction)
            {
                _interactAction = interactAction;
                _itemUseAction = itemUseAction;
                _itemReleaseAction = itemReleaseAction;
                _itemAlternateAction = itemAlternateAction;
                _itemShootAction = itemShootAction;

                _interactAction.performed += OnInteractActionTriggered;
                _interactAction.canceled += OnInteractActionCanceled;

                _itemUseAction.performed += OnUseItemActionTriggered;
                _itemUseAction.canceled += OnUseItemActionCanceled;

                _itemReleaseAction.canceled += OnReleaseItemActionCancelled;

                _itemAlternateAction.performed += OnAlternateActionTriggered;
                _itemAlternateAction.canceled += OnAlternateActionCanceled;
            }

            public void Shutdown()
            {
                _interactAction.performed -= OnInteractActionTriggered;
                _interactAction.canceled -= OnInteractActionCanceled;

                _itemUseAction.performed -= OnUseItemActionTriggered;
                _itemUseAction.canceled -= OnUseItemActionCanceled;

                _itemReleaseAction.canceled -= OnReleaseItemActionCancelled;

                _itemAlternateAction.performed -= OnAlternateActionTriggered;
                _itemAlternateAction.canceled -= OnAlternateActionCanceled;
            }

            private void OnInteractActionTriggered(InputAction.CallbackContext context)
            {
                InteractActionValue = true;

                InteractActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    InteractActionValue);
            }

            private void OnInteractActionCanceled(InputAction.CallbackContext context)
            {
                InteractActionValue = false;

                InteractActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    InteractActionValue);
            }

            private void OnUseItemActionTriggered(InputAction.CallbackContext context)
            {
                ItemUseActionValue = true;

                UseItemActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    ItemUseActionValue);
            }

            private void OnUseItemActionCanceled(InputAction.CallbackContext context)
            {
                ItemUseActionValue = false;

                UseItemActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    ItemUseActionValue);
            }

            private void OnReleaseItemActionCancelled(InputAction.CallbackContext context)
            {
                ReleaseItemActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ));
            }

            private void OnAlternateActionTriggered(InputAction.CallbackContext context)
            {
                ItemAlternateActionValue = true;

                ItemAlternateActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    ItemAlternateActionValue);
            }

            private void OnAlternateActionCanceled(InputAction.CallbackContext context)
            {
                ItemAlternateActionValue = false;

                ItemAlternateActionTriggered?.Invoke(
                    new InputContext
                    (
                        context.startTime,
                        context.duration,
                        context.time,

                        context.started,
                        context.performed,
                        context.canceled
                    ),
                    ItemAlternateActionValue);
            }
        }
    }
}
