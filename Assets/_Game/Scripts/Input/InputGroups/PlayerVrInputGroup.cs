﻿using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Storage.Settings;
using Game.Storage.Settings;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IPlayerVrInput),
    lifetime: Lifetime.Singleton)]
    public partial class PlayerVrInputGroup : BaseInputGroup, IPlayerVrInput
    {
        public IPlayerVrHandInput HandLeft => _handLeft;
        public IPlayerVrHandInput HandRight => _handRight;

        public override bool InputEnabled
        {
            get => InputManager.Input.PlayerVR.enabled;
            set
            {
                if (value) InputManager.Input.PlayerVR.Enable();
                else InputManager.Input.PlayerVR.Disable();
            }
        }

        public Vector2 PlayerSmoothMoveActionValue
        {
            get
            {
                if (_inputSettingsGroup.PrimaryLocomotionHand == VrHand.Left)
                    return InputManager.Input.PlayerVR.SmoothMoveLeftHand.ReadValue<Vector2>();
                else if (_inputSettingsGroup.PrimaryLocomotionHand == VrHand.Right)
                    return InputManager.Input.PlayerVR.SmoothMoveRightHand.ReadValue<Vector2>();
                else
                    return Vector2.zero;
            }
        }

        public event Action<InputContext> PlayerSnapLeftTriggered;
        public event Action<InputContext> PlayerSnapRightTriggered;

        private readonly IEventBus _eventBus;
        private readonly ISettingsManager _settingsManager;

        private IInputVrSettingsGroup _inputSettingsGroup;

        private HandInput _handLeft;
        private HandInput _handRight;

        public PlayerVrInputGroup(IEventBus eventBus, ISettingsManager settingsManager)
        {
            _eventBus = eventBus;
            _settingsManager = settingsManager;

            _inputSettingsGroup = null;
        }

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputVrSettingsGroup>();

            _handLeft = new HandInput(
                interactAction: InputManager.Input.PlayerVR.InteractLeftHand,
                itemUseAction: InputManager.Input.PlayerVR.ItemUseLeftHand,
                itemReleaseAction: InputManager.Input.PlayerVR.ItemReleaseLeftHand,
                itemAlternateAction: InputManager.Input.PlayerVR.ItemAlternateLeftHand,
                itemShootAction: InputManager.Input.PlayerVR.ItemShootLeftHand);

            _handRight = new HandInput(
                interactAction: InputManager.Input.PlayerVR.InteractRightHand,
                itemUseAction: InputManager.Input.PlayerVR.ItemUseRightHand,
                itemReleaseAction: InputManager.Input.PlayerVR.ItemReleaseRightHand,
                itemAlternateAction: InputManager.Input.PlayerVR.ItemAlternateRightHand,
                itemShootAction: InputManager.Input.PlayerVR.ItemShootRightHand);

            InputManager.Input.PlayerVR.SnapRotateLeftHand.performed += OnPlayerRotateLeftHandTriggered;
            InputManager.Input.PlayerVR.SnapRotateRightHand.performed += OnPlayerRotateRightHandTriggered;

            _eventBus.Subscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        public override void ShutdownInputGroup()
        {
            base.ShutdownInputGroup();

            _inputSettingsGroup = null;

            _handLeft.Shutdown();
            _handLeft = null;

            _handRight.Shutdown();
            _handRight = null;

            InputManager.Input.PlayerVR.SnapRotateLeftHand.performed -= OnPlayerRotateLeftHandTriggered;
            InputManager.Input.PlayerVR.SnapRotateRightHand.performed -= OnPlayerRotateRightHandTriggered;

            _eventBus.Unsubscribe<ActiveSettingsProfileChangedEvent>(OnActiveSettingsProfileChanged);
        }

        private void OnActiveSettingsProfileChanged(ActiveSettingsProfileChangedEvent eventArgs)
        {
            _inputSettingsGroup = _settingsManager.ActiveProfile.GetSettingsGroup<IInputVrSettingsGroup>();
        }

        private void OnPlayerRotateLeftHandTriggered(InputAction.CallbackContext context)
        {
            if (!_inputSettingsGroup.SnapTurnEnabled || _inputSettingsGroup.PrimaryLocomotionHand == VrHand.Left) return;

            var value = context.ReadValue<Vector2>();
            var direction = CardinalUtility.GetNearestCardinal(value);

            if (direction == Cardinal.West)
                RaisePlayerSnapLeft(context);
            else if (direction == Cardinal.East)
                RaisePlayerSnapRight(context);
        }

        private void OnPlayerRotateRightHandTriggered(InputAction.CallbackContext context)
        {
            if (!_inputSettingsGroup.SnapTurnEnabled || _inputSettingsGroup.PrimaryLocomotionHand == VrHand.Right) return;

            var value = context.ReadValue<Vector2>();
            var direction = CardinalUtility.GetNearestCardinal(value);

            if (direction == Cardinal.West)
                RaisePlayerSnapLeft(context);
            else if (direction == Cardinal.East)
                RaisePlayerSnapRight(context);
        }

        private void RaisePlayerSnapLeft(InputAction.CallbackContext context)
        {
            PlayerSnapLeftTriggered?.Invoke(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ));
        }

        private void RaisePlayerSnapRight(InputAction.CallbackContext context)
        {
            PlayerSnapRightTriggered?.Invoke(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ));
        }
    }
}
