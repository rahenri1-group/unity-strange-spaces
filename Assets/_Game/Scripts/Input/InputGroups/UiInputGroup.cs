﻿using Game.Core.DependencyInjection;
using UnityEngine;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IUiInputGroup),
    lifetime: Lifetime.Singleton)]
    public class UiInputGroup : BaseInputGroup, IUiInputGroup
    {
        public override bool InputEnabled
        {
            get => InputManager.Input.UI.enabled;
            set
            {
                if (value) InputManager.Input.UI.Enable();
                else InputManager.Input.UI.Disable();
            }
        }

        public Vector2 PointerValue => InputManager.Input.UI.Point.ReadValue<Vector2>();
    }
}
