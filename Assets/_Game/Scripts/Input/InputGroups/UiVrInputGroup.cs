﻿using Game.Core.DependencyInjection;
using System;
using UnityEngine.InputSystem;

namespace Game.Input
{
    /// <inheritdoc cref="IUiVrInput"/>
    [Dependency(
    contract: typeof(IUiVrInput),
    lifetime: Lifetime.Singleton)]
    public class UiVrInputGroup : BaseInputGroup, IUiVrInput
    {
        public override bool InputEnabled
        {
            get => InputManager.Input.UIVR.enabled;
            set
            {
                if (value) InputManager.Input.UIVR.Enable();
                else InputManager.Input.UIVR.Disable();
            }
        }

        public event Action<InputContext> SelectLeftTriggered;
        public event Action<InputContext> SelectRightTriggered;

        public event Action<InputContext> MenuToggleTriggered;

        /// <inheritdoc/>
        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            InputManager.Input.UIVR.SelectLeftHand.performed += OnSelectLeftHandTriggered;

            InputManager.Input.UIVR.SelectRightHand.performed += OnSelectRightHandTriggered;

            InputManager.Input.UIVR.MenuToggle.performed += OnMenuToggleTriggered;
        }

        /// <inheritdoc/>
        public override void ShutdownInputGroup()
        {
            base.ShutdownInputGroup();

            InputManager.Input.UIVR.SelectLeftHand.performed -= OnSelectLeftHandTriggered;

            InputManager.Input.UIVR.SelectRightHand.performed -= OnSelectRightHandTriggered;

            InputManager.Input.UIVR.MenuToggle.performed -= OnMenuToggleTriggered;
        }

        private void OnSelectLeftHandTriggered(InputAction.CallbackContext context)
        {
            if (SelectLeftTriggered == null) return;

            SelectLeftTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ));
        }

        private void OnSelectRightHandTriggered(InputAction.CallbackContext context)
        {
            if (SelectRightTriggered == null) return;

            SelectRightTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ));
        }

        private void OnMenuToggleTriggered(InputAction.CallbackContext context)
        {
            if (MenuToggleTriggered == null) return;

            MenuToggleTriggered(
                new InputContext
                (
                    context.startTime,
                    context.duration,
                    context.time,

                    context.started,
                    context.performed,
                    context.canceled
                ));
        }
    }
}