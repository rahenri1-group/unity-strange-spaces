﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Input
{
    public partial class VrTrackingInputGroup : BaseInputGroup
    {
        private class TrackedDevice : IVrTrackedDevice
        {
            public bool Tracked => _trackedAction.ReadValue<float>() > 0f;

            public Vector3 Position => _positionAction.ReadValue<Vector3>();
            public Quaternion Rotation => _rotationAction.ReadValue<Quaternion>();

            private readonly InputAction _trackedAction;
            private readonly InputAction _positionAction;
            private readonly InputAction _rotationAction;

            public TrackedDevice(InputAction trackedAction, InputAction positionAction, InputAction rotationAction)
            {
                _trackedAction = trackedAction;
                _positionAction = positionAction;
                _rotationAction = rotationAction;
            }
        }

        private class TrackedEye : IVrTrackedEye
        {
            public Vector3 Position => _positionAction.ReadValue<Vector3>();
            public Quaternion Rotation => _rotationAction.ReadValue<Quaternion>();

            private readonly InputAction _positionAction;
            private readonly InputAction _rotationAction;

            public TrackedEye(InputAction positionAction, InputAction rotationAction)
            {
                _positionAction = positionAction;
                _rotationAction = rotationAction;
            }
        }

        private class TrackedHand : TrackedDevice, IVrTrackedHand
        {
            public float Grip => _gripAction.ReadValue<float>();

            private readonly InputAction _gripAction;

            public TrackedHand(InputAction trackedAction, InputAction positionAction, InputAction rotationAction,InputAction gripAction)
                : base (trackedAction, positionAction, rotationAction)
            {
                _gripAction = gripAction;
            }
        }
    }
}
