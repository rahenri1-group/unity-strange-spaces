﻿using Game.Core.DependencyInjection;

namespace Game.Input
{
    [Dependency(
    contract: typeof(IVrTrackingInput),
    lifetime: Lifetime.Singleton)]
    public partial class VrTrackingInputGroup : BaseInputGroup, IVrTrackingInput
    {
        

        public override bool InputEnabled
        {
            get => InputManager.Input.VRTracking.enabled;
            set
            {
                if (value) InputManager.Input.VRTracking.Enable();
                else InputManager.Input.VRTracking.Disable();
            }
        }

        public IVrTrackedDevice Hmd => _hmd;
        public IVrTrackedEye EyeLeft => _eyeLeft;
        public IVrTrackedEye EyeRight => _eyeRight;

        public IVrTrackedHand HandLeft => _handLeft;
        public IVrTrackedHand HandRight => _handRight;

        private TrackedDevice _hmd;
        private TrackedEye _eyeLeft;
        private TrackedEye _eyeRight;

        private TrackedHand _handLeft;
        private TrackedHand _handRight;

        public override void InitializeInputGroup(InputManager inputManager)
        {
            base.InitializeInputGroup(inputManager);

            _hmd = new TrackedDevice(
                InputManager.Input.VRTracking.HMDTracked,
                InputManager.Input.VRTracking.HMDPosition,
                InputManager.Input.VRTracking.HMDRotation);

            _eyeLeft = new TrackedEye(
                InputManager.Input.VRTracking.EyeLeftPosition,
                InputManager.Input.VRTracking.EyeLeftRotation
                );

            _eyeRight = new TrackedEye(
                InputManager.Input.VRTracking.EyeRightPosition,
                InputManager.Input.VRTracking.EyeRightRotation
                );

            _handLeft = new TrackedHand(
                InputManager.Input.VRTracking.HandLeftTracked,
                InputManager.Input.VRTracking.HandLeftPosition,
                InputManager.Input.VRTracking.HandLeftRotation,
                InputManager.Input.VRTracking.HandLeftGrip);

            _handRight = new TrackedHand(
                InputManager.Input.VRTracking.HandRightTracked,
                InputManager.Input.VRTracking.HandRightPosition,
                InputManager.Input.VRTracking.HandRightRotation,
                InputManager.Input.VRTracking.HandRightGrip);
        }
    }
}
