﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Input;
using Game.Input.Generated;

namespace Game.Input
{
    [Dependency(
        contract: typeof(IInputManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class InputManager : BaseModule, IInputManager
    {
        public override string ModuleName => "Input Manager";

        public override ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        public IInputGroup[] InputGroups => _inputGroups;

        public StrangeSpacesInput Input { get; private set; }

        private BaseInputGroup[] _inputGroups;

        public InputManager(
            BaseInputGroup[] inputGroups,
            ILogRouter logger)
            :base(logger)
        {
            _inputGroups = inputGroups;

            Input = null;
        }

        public override UniTask Initialize()
        {
            Input = new StrangeSpacesInput();

            foreach (var inputGroup in _inputGroups)
            {
                inputGroup.InitializeInputGroup(this);
            }

            return base.Initialize();
        }

        public override UniTask Shutdown()
        {
            Input.Dispose();

            foreach (var inputGroup in _inputGroups)
            {
                inputGroup.ShutdownInputGroup();
            }

            Input = null;

            return base.Shutdown();
        }
    }
}
