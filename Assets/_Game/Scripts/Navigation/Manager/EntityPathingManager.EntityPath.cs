﻿using Game.Core.Entity.Pathing;
using Game.Core.Portal;
using Game.Core.Space;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Pathing
{
    public partial class EntityPathingManager
    {
        private class EntityPath : IEntityPath
        {
            public IEntityPathNode[] PathNodes { get; private set; }

            public bool IsPathStale => _portalPath.IsPathStale;

            private IPortalPath<IPortalTransporter> _portalPath;

            public EntityPath(IPortalPath<IPortalTransporter> portalPath, EntityPathNode[] pathNodes)
            {
                _portalPath = portalPath;
                PathNodes = pathNodes;
            }
        }

        private class EntityPathNode : IEntityPathNode
        {
            public ISpaceData NodeSpace => _portalNode.NodeSpace;

            public IPortalTransporter PortalToNextNode => _portalNode.Portal;

            public Vector3 StartPosition => _portalNode.SpaceStartPosition;

            public Vector3 EndPosition => _portalNode.SpaceEndPosition;

            public NavMeshPath Path { get; private set; }

            private IPortalPathNode<IPortalTransporter> _portalNode;

            public EntityPathNode(IPortalPathNode<IPortalTransporter> portalNode, NavMeshPath navMeshPath)
            {
                _portalNode = portalNode;
                Path = navMeshPath;
            }
        }
    }
}
