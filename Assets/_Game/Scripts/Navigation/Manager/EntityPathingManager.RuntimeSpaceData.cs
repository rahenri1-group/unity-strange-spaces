﻿using Unity.AI.Navigation;

namespace Game.Pathing
{
    public partial class EntityPathingManager
    {
        private class RuntimeSpaceData
        {
            public NavMeshSurface[] NavMeshSurfaces;

            public int NavigationArea;
        }
    }
}
