﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Entity.Pathing;
using Game.Core.Portal;
using Game.Core.Space;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine.AI;

namespace Game.Pathing
{
    /// <inheritdoc cref="IEntityPathingManager"/> 
    [Dependency(
        contract: typeof(IEntityPathingManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public partial class EntityPathingManager : BaseModule, IEntityPathingManager, ISpaceLoadProcessor, ISpaceUnloadProcessor
    {
        /// <inheritdoc/>
        public override string ModuleName => "Entity Pathing Manager";

        /// <inheritdoc/>
        public override ModuleConfig ModuleConfig => Config;
        public EntityPathingManagerConfig Config = new EntityPathingManagerConfig();

        private Dictionary<ISpaceData, RuntimeSpaceData> _spaceDataMap;

        private Queue<int> _availableNavigationAreas;

        private readonly IPortalPathingManager _portalPathingManager;
        private readonly ISpaceLoader _spaceLoader;
        private readonly ISpaceManager _spaceManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public EntityPathingManager(
            ILogRouter logger,
            IPortalPathingManager portalPathingManager,
            ISpaceManager spaceManager,
            ISpaceLoader spaceLoader)
            : base(logger)
        {
            _portalPathingManager = portalPathingManager;
            _spaceLoader = spaceLoader;
            _spaceManager = spaceManager;
        }

        /// <inheritdoc/>
        public override UniTask Initialize()
        {
            _spaceDataMap = new Dictionary<ISpaceData, RuntimeSpaceData>();

            _availableNavigationAreas = new Queue<int>(); ;
            foreach (var areaName in Config.SpaceNavigationAreas)
            {
                int area = NavMesh.GetAreaFromName(areaName);
                _availableNavigationAreas.Enqueue(area);
            }

            _spaceLoader.RegisterSpaceLoadProcessor(this);
            _spaceLoader.RegisterSpaceUnloadProcessor(this);        

            return base.Initialize();
        }

        /// <inheritdoc/>
        public override UniTask Shutdown()
        {
            _spaceDataMap.Clear();
            _availableNavigationAreas.Clear();

            _spaceLoader.UnregisterSpaceLoadProcessor(this);
            _spaceLoader.UnregisterSpaceUnloadProcessor(this);

            return base.Shutdown();
        }

        /// <inheritdoc/>
        public async UniTask ProcessSpaceForLoad(IRuntimeSpaceData runtimeSpace)
        {
            var space = runtimeSpace.SpaceData;

            var navMeshSurfaces = _spaceManager.FindLoadedInSpace<NavMeshSurface>(space);

            if (navMeshSurfaces.Length == 0)
                return;

            var navigationArea = _availableNavigationAreas.Dequeue();

            ModuleLogInfo($"'{space.Name}' assigned navigation area '{navigationArea}'");

            _spaceDataMap.Add(
                space,
                new RuntimeSpaceData
                {
                    NavMeshSurfaces = navMeshSurfaces,
                    NavigationArea = navigationArea
                });

            foreach (var navMeshSurface in navMeshSurfaces)
            {
                navMeshSurface.defaultArea = navigationArea;
                navMeshSurface.layerMask = 1 << runtimeSpace.SpaceRenderLayer; 
                await navMeshSurface.UpdateNavMesh(navMeshSurface.navMeshData);
            }

            return;
        }

        /// <inheritdoc/>
        public UniTask ProcessSpaceForUnload(IRuntimeSpaceData runtimeSpace)
        {
            var space = runtimeSpace.SpaceData;

            if (_spaceDataMap.ContainsKey(space))
            {
                var runtimeData = _spaceDataMap[space];
                _spaceDataMap.Remove(space);

                _availableNavigationAreas.Enqueue(runtimeData.NavigationArea);
            }

            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public void ConfigurePathingAgent(IEntityPathingAgent agent)
        {
            var agentSpace = _spaceManager.GetObjectSpace(agent.GameObject);

            if (!_spaceDataMap.ContainsKey(agentSpace))
            {
                ModuleLogWarning($"Can not configure agent {agent.GameObject.name} for space {agentSpace.Name} as it is not loaded");
                return;
            }

            agent.UnityAgent.areaMask = CalculateMaskForSpace(agentSpace);
        }

        /// <inheritdoc/>
        public bool GetPathForAgent(IEntityPathingAgent agent, SpacePosition destinationPosition, out IEntityPath path, float maxOffsetAllowed)
        {
            return GetPathForAgent(agent, destinationPosition, float.MaxValue, out path, maxOffsetAllowed);
        }

        /// <inheritdoc/>
        public bool GetPathForAgent(IEntityPathingAgent agent, SpacePosition destinationPosition, float maxDistance, out IEntityPath path, float maxOffsetAllowed)
        {
            path = null;

            IPortalPath<IPortalTransporter> portalPathInfo;
            if (_portalPathingManager.GetPath(
                new SpacePosition(_spaceManager.GetObjectSpace(agent.GameObject), agent.Position),
                destinationPosition, 
                maxDistance, 
                out portalPathInfo))
            {
                var pathNodes = new List<EntityPathNode>();
                foreach (var portalNode in portalPathInfo.Path)
                {
                    var navMeshPath = new NavMeshPath();

                    int maskForSpace = CalculateMaskForSpace(portalNode.NodeSpace);

                    bool pathToNodeFound = false;
                    if (portalNode.Portal != null) // pathing to portal on path
                    {
                        // add a small amount past the portal so that we go through it
                        pathToNodeFound = NavMesh.CalculatePath(portalNode.SpaceStartPosition, portalNode.SpaceEndPosition + 0.1f * portalNode.Portal.EntryPortal.Transform.forward, maskForSpace, navMeshPath);
                    }
                    else // pathing to final point
                    {
                        pathToNodeFound = NavMesh.CalculatePath(portalNode.SpaceStartPosition, portalNode.SpaceEndPosition, maskForSpace, navMeshPath);
                        
                        // if can't path to the actual location, try to find a nearby spot
                        if (!pathToNodeFound && maxOffsetAllowed > 0f)
                        {
                            if (NavMesh.SamplePosition(portalNode.SpaceEndPosition, out var navMeshHit, maxOffsetAllowed, maskForSpace))
                            {
                                pathToNodeFound = NavMesh.CalculatePath(portalNode.SpaceStartPosition, navMeshHit.position, maskForSpace, navMeshPath);
                            }
                        }
                    }

                    if (!pathToNodeFound)
                    {
                        return false;
                    }

                    pathNodes.Add(new EntityPathNode(portalNode, navMeshPath));
                }

                path = new EntityPath(portalPathInfo, pathNodes.ToArray());

                return true;
            }

            return false;
        }

        private int CalculateMaskForSpace(ISpaceData space)
        {
            if (!_spaceDataMap.ContainsKey(space))
            {
                ModuleLogWarning($"Can not calculate mask for space {space.Name} as it is not loaded");
                return 0;
            }

            return 1 << _spaceDataMap[space].NavigationArea;
        }
    }
}
