﻿using Game.Core;

namespace Game.Pathing
{
    public class EntityPathingManagerConfig : ModuleConfig
    {
        public string[] SpaceNavigationAreas = new string[0];
    }
}
