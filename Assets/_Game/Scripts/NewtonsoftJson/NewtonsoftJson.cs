using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Game.Serialization.Json
{
    /// <summary>
    /// Wrapper for Newtonsoft Json serialization and deserialization
    /// </summary>
    [Dependency(
        contract: typeof(IJsonDeserializer),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IJsonSerializer),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class NewtonsoftJson: IJsonDeserializer, IJsonSerializer
    {
        /// <inheritdoc/>
        public string ModuleName => "Json Utility";

        /// <inheritdoc/>
        public ModuleConfig ModuleConfig => Config;
        public ModuleConfig Config = new ModuleConfig();

        /// <inheritdoc/>
        public bool ModuleInitialized => true;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public NewtonsoftJson() { }

        /// <inheritdoc/>
        public UniTask Initialize()
        {
            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public UniTask Shutdown()
        {
            return UniTask.CompletedTask;
        }

        /// <inheritdoc/>
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <inheritdoc/>
        public string SerializeHumanReadable(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        /// <inheritdoc/>
        public T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <inheritdoc/>
        public object Deserialize(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        /// <inheritdoc/>
        public Dictionary<string, string> KeyValues(string json)
        {
            var keyValues = new Dictionary<string, string>();

            foreach (var pair in JObject.Parse(json))
            {
                keyValues[pair.Key] = pair.Value.ToString();
            }

            return keyValues;
        }

        /// <inheritdoc/>
        public void PopulateObject(object obj, string json)
        {
            JsonConvert.PopulateObject(json, obj);
        }
    }
}
