﻿namespace Game.Storage.Settings
{
    /// <summary>
    /// Extensions for enums in settings
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns the user facing name for <see cref="VrHand"/>
        /// </summary>
        public static string GetUserFacingName(this VrHand hand)
        {
            switch (hand)
            {
                case VrHand.Right:
                    return "Right";

                case VrHand.Left:
                default:
                    return "Left";
            }
        }

        /// <summary>
        /// Returns the user facing name for <see cref="VrLocomotion"/>
        /// </summary>
        public static string GetUserFacingName(this VrLocomotion hand)
        {
            switch (hand)
            {
                case VrLocomotion.ContinuousHand:
                    return "Continuous Hand";

                case VrLocomotion.ContinuousHead:
                default:
                    return "Continuous Head";
            }
        }
    }
}