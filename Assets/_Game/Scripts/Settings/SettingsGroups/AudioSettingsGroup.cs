﻿using Game.Core.Storage.Settings;
using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Game.Storage.Settings
{
    [Serializable]
    public class AudioSettingsGroup : IAudioSettingsGroup
    {
        public event Action<ISettingsGroup> SettingsGroupUpdated;

        [IgnoreDataMember] public string Name => "Audio";

        [Setting] public int MasterVolume
        {
            get => _masterVolume;
            set
            {
                value = Mathf.Clamp(value, 0, 100);
                if (_masterVolume == value) return;

                _masterVolume = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _masterVolume = 100;

        [Setting] public int MusicVolume
        {
            get => _musicVolume;
            set
            {
                value = Mathf.Clamp(value, 0, 100);
                if (_musicVolume == value) return;
                
                _musicVolume = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _musicVolume = 100;

        [Setting] public int EffectsVolume
        {
            get => _effectsVolume;
            set
            {
                value = Mathf.Clamp(value, 0, 100);
                if (_effectsVolume == value) return;

                _effectsVolume = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _effectsVolume = 100;
    }
}