﻿using Game.Core.Storage.Settings;

namespace Game.Storage.Settings
{
    public interface IAudioSettingsGroup : ISettingsGroup
    {
        /// <summary>
        /// Volume from 0 to 100
        /// </summary>
        int MasterVolume { get; }

        /// <summary>
        /// Volume from 0 to 100
        /// </summary>
        int MusicVolume { get; }

        /// <summary>
        /// Volume from 0 to 100
        /// </summary>
        int EffectsVolume { get; }
    }
}