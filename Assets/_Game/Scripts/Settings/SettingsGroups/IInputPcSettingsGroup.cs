﻿using Game.Core.Storage.Settings;

namespace Game.Storage.Settings
{
    public interface IInputPcSettingsGroup : ISettingsGroup
    {
        public float MouseSensitivity { get; }
        public bool InvertMouse { get; }
    }
}
