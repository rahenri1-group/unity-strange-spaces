﻿using Game.Core.Storage.Settings;

namespace Game.Storage.Settings
{
    public enum VrLocomotion { ContinuousHead = 0, ContinuousHand = 1 }
    public enum VrHand { Left = 0, Right = 1}

    public interface IInputVrSettingsGroup : ISettingsGroup
    {
        VrLocomotion LocomotionType { get; }
        VrHand PrimaryLocomotionHand { get; }

        bool SnapTurnEnabled { get; }
        float SnapTurnAmount { get; }
    }
}
