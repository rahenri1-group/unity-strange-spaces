﻿using Game.Core.Storage.Settings;

namespace Game.Storage.Settings
{
    public interface IPreferencesVrSettingsGroup : ISettingsGroup
    {
        VrHand HealthDisplayHand { get; }

        int HolsterVerticalOffset { get; }
        int HolsterForwardOffset { get; }
        int HolsterSeparationOffset { get; }
    }
}