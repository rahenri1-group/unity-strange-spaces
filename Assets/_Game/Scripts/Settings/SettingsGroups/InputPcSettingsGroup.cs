﻿using Game.Core.Storage.Settings;
using System;
using System.Runtime.Serialization;

namespace Game.Storage.Settings
{
    [Serializable]
    public class InputPcSettingsGroup : IInputPcSettingsGroup
    {
        public event Action<ISettingsGroup> SettingsGroupUpdated;

        [IgnoreDataMember] public string Name => "InputPc";

        [Setting] public float MouseSensitivity
        {
            get => _mouseSensitivity;
            set
            {
                if (_mouseSensitivity == value) return;

                _mouseSensitivity = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private float _mouseSensitivity = 0.3f;

        [Setting] public bool InvertMouse 
        { 
            get => _invertMouse;
            set
            {
                if (_invertMouse == value) return;

                _invertMouse = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private bool _invertMouse = false;
    }
}
