﻿using Game.Core.Storage.Settings;
using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Game.Storage.Settings
{
    [Serializable]
    public class InputVrSettingsGroup : IInputVrSettingsGroup
    {
        public event Action<ISettingsGroup> SettingsGroupUpdated;

        [IgnoreDataMember] public string Name => "InputVr";

        [Setting] public VrLocomotion LocomotionType
        {
            get => _locomotionType;
            set
            {
                if (_locomotionType == value) return;

                _locomotionType = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private VrLocomotion _locomotionType = VrLocomotion.ContinuousHead;

        [Setting] public VrHand PrimaryLocomotionHand
        {
            get => _primaryLocomotionHand;
            set
            {
                if (_primaryLocomotionHand == value) return;

                _primaryLocomotionHand = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private VrHand _primaryLocomotionHand = VrHand.Left;

        [Setting] public bool SnapTurnEnabled
        {
            get => _snapTurnEnabled;
            set
            {
                if (_snapTurnEnabled == value) return;

                _snapTurnEnabled = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private bool _snapTurnEnabled = true;

        [Setting] public float SnapTurnAmount
        {
            get => _snapTurnAmount;
            set
            {
                value = Mathf.Clamp(value, 1f, 180f);
                if (_snapTurnAmount == value) return;

                _snapTurnAmount = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private float _snapTurnAmount = 45f;
    }
}
