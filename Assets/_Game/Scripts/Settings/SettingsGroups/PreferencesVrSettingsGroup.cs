﻿using Game.Core.Storage.Settings;
using System;
using System.Runtime.Serialization;

namespace Game.Storage.Settings
{
    [Serializable]
    public class PreferencesVrSettingsGroup : IPreferencesVrSettingsGroup
    {
        public event Action<ISettingsGroup> SettingsGroupUpdated;

        [IgnoreDataMember] public string Name => "PreferencesVr";

        [Setting]
        public VrHand HealthDisplayHand
        {
            get => _healthDisplayHand;
            set
            {
                if (_healthDisplayHand == value) return;

                _healthDisplayHand = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private VrHand _healthDisplayHand = VrHand.Left;

        [Setting]
        public int HolsterVerticalOffset 
        {
            get => _holsterVerticalOffset;
            set
            {
                if (_holsterVerticalOffset == value) return;

                _holsterVerticalOffset = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _holsterVerticalOffset = 0;

        [Setting]
        public int HolsterForwardOffset 
        {
            get => _holsterForwardOffset;
            set
            {
                if (_holsterForwardOffset == value) return;

                _holsterForwardOffset = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _holsterForwardOffset = 0;

        [Setting]
        public int HolsterSeparationOffset 
        {
            get => _holsterSeparationOffset;
            set
            {
                if (_holsterSeparationOffset == value) return;

                _holsterSeparationOffset = value;
                SettingsGroupUpdated?.Invoke(this);
            }
        }
        private int _holsterSeparationOffset = 0;
    }
}