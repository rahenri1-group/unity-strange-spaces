﻿using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using Game.Core.Serialization;
using Game.Core.Storage.Settings;
using System;
using System.Collections.Generic;

namespace Game.Storage.Settings
{
    /// <inheritdoc/>
    [Dependency(
        contract: typeof(ISettingsManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class SettingsManager : BaseSettingsManager<SettingsManagerConfig>
    {
        /// <inheritdoc/>
        public override string ModuleName => "Settings Manager";

        protected override Dictionary<string, Type> AvailableSettingsGroups { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsManager(
            IEventBus eventBus,
            IJsonDeserializer jsonDeserializer,
            IJsonSerializer jsonSerializer,
            ILogRouter logger):
            base(eventBus, jsonDeserializer, jsonSerializer, logger) 
        {
            AvailableSettingsGroups = new Dictionary<string, Type>();
            AvailableSettingsGroups.Add("InputPc", typeof(InputPcSettingsGroup));
            AvailableSettingsGroups.Add("InputVr", typeof(InputVrSettingsGroup));
            AvailableSettingsGroups.Add("Audio", typeof(AudioSettingsGroup));
            AvailableSettingsGroups.Add("PreferencesVr", typeof(PreferencesVrSettingsGroup));
        }
    }
}
