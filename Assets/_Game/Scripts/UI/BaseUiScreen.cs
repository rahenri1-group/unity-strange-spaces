﻿using Game.Core;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.UI;
using UnityEngine;

namespace Game.UI
{
    [RequireComponent(typeof(Canvas))]
    public abstract class BaseUiScreen : InjectedBehaviour, IUiScreen
    {
        public virtual GameObject GameObject => gameObject;

        public virtual bool Visible => CanvasGroup.Visible;
        public virtual bool UsesCursor => true;

        [SerializeField] [TypeRestriction(typeof(ICanvasGroup))] private Component _CanvasGroupObj = null;
        protected ICanvasGroup CanvasGroup;

        [Inject] protected ICameraManager CameraManager;

        protected override void Awake()
        {
            base.Awake();

            CanvasGroup = _CanvasGroupObj.GetComponentAsserted<ICanvasGroup>();
            CanvasGroup.Hide(0f);

            var canvas = GetComponent<Canvas>();
            canvas.worldCamera = CameraManager.ActiveUiCamera.UnityCamera;
        }

        public virtual void Show()
        {
            CanvasGroup.Show();
        }

        public virtual void Hide()
        {
            CanvasGroup.Hide();
        }
    }
}
