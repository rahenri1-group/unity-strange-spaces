﻿using UnityEngine;
using System.Collections;
using Game.Core.UI;
using Game.Core;

namespace Game.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class CanvasGroupFadeBehaviour : MonoBehaviour, ICanvasGroup
    {
        public bool Visible => gameObject.activeInHierarchy;

        [SerializeField] private FloatReadonlyReference _defaultFadeDuration = null;

        private CanvasGroup CanvasGroup
        {
            get
            {
                if (_canvasGroup == null) _canvasGroup = GetComponent<CanvasGroup>();
                return _canvasGroup;
            }
        }
        private CanvasGroup _canvasGroup;

        private IEnumerator _displayCoroutine = null;

        public void Show()
        {
            Show(_defaultFadeDuration);
        }

        public void Show(float transitionDuration)
        {
            if (_displayCoroutine != null)
            {
                StopCoroutine(_displayCoroutine);
                _displayCoroutine = null;
            }

            if (transitionDuration > 0f)
            {
                gameObject.SetActive(true);

                _displayCoroutine = Fade(transitionDuration, true);
                StartCoroutine(_displayCoroutine);
            }
            else
            {
                CanvasGroup.alpha = 1f;
                gameObject.SetActive(true);
            }
        }

        public void Hide()
        {
            Hide(_defaultFadeDuration);
        }

        public void Hide(float transitionDuration)
        {
            if (_displayCoroutine != null)
            {
                StopCoroutine(_displayCoroutine);
                _displayCoroutine = null;
            }

            if (!Visible) return;

            if (transitionDuration > 0f)
            {
                _displayCoroutine = Fade(transitionDuration, false);
                StartCoroutine(_displayCoroutine);
            }
            else
            {
                CanvasGroup.alpha = 0f;
                gameObject.SetActive(false);
            }
        }

        private IEnumerator Fade(float duration, bool enabledAtEnd)
        {
            YieldInstruction wait = null;

            float startAlpha = CanvasGroup.alpha;

            float startTime = Time.time;
            while (Time.time - startTime <= _defaultFadeDuration)
            {
                yield return wait;

                float lerp = Mathf.Clamp01((Time.time - startTime) / _defaultFadeDuration);
                CanvasGroup.alpha = Mathf.SmoothStep(startAlpha, (enabledAtEnd) ? 1f : 0f, lerp);
            }

            CanvasGroup.alpha = (enabledAtEnd) ? 1f : 0f;
            gameObject.SetActive(enabledAtEnd);

            _displayCoroutine = null;
        }
    }
}