﻿using Cysharp.Threading.Tasks;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.UI;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.Command
{
    /// <summary>
    /// Config for <see cref="ShowFpsCommandProcessor"/>
    /// </summary>
    [Serializable]
    public class ShowFpsCommandProcessorConfig
    {
        /// <summary>
        /// The asset key for the UI
        /// </summary>
        public string FpsUiAssetKey = string.Empty;
    }

    /// <summary>
    /// Command to display the current fps
    /// </summary>
    [Serializable]
    public class ShowFpsCommand : ICommand
    {
        /// <summary>
        /// Should the fps be displayed
        /// </summary>
        public bool ShowFps;
    }

    /// <summary>
    /// Command processor for <see cref="ShowFpsCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class ShowFpsCommandProcessor : BaseConsoleCommandProcessor<ShowFpsCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "fps-show";

        /// <inheritdoc/>
        public override string CommandDescription => "Shows/Hides the current fps";

        public ShowFpsCommandProcessorConfig Config = new ShowFpsCommandProcessorConfig();

        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IUiManager _uiManager;

        private GameObject _fpsScreenGameObject;
        private IUiScreen _fpsScreen;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public ShowFpsCommandProcessor(
            IGameObjectResourceManager gameObjectResourceManager,
            IUiManager uiManager)
        {
            _gameObjectResourceManager = gameObjectResourceManager;
            _uiManager = uiManager;

            _fpsScreenGameObject = null;
            _fpsScreen = null;
        }

        /// <inheritdoc/>
        public override void Initialize()
        {
            base.Initialize();

            Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(Config.FpsUiAssetKey));
        }

        /// <inheritdoc/>
        public override ShowFpsCommand ParseCommand(string[] args)
        {
            bool showFps = true;

            if (args.Length > 0)
            {
                showFps = ParseBool(args[0], true);
            }

            return new ShowFpsCommand
            {
                ShowFps = showFps
            };
        }

        /// <inheritdoc/>
        public override async UniTask Execute(ShowFpsCommand command)
        {
            if (command.ShowFps && _fpsScreen == null)
            {
                var uiSCreen = await _gameObjectResourceManager.InstantiateAsync<IUiScreen>(Config.FpsUiAssetKey, null, Vector3.zero);
                _fpsScreenGameObject = uiSCreen.GameObject;
                _fpsScreen = uiSCreen.Component;

                _uiManager.ShowScreenSpaceUiScreen(_fpsScreen);
            }
            else if (!command.ShowFps && _fpsScreen != null)
            {
                _uiManager.HideScreenSpaceUiScreen(_fpsScreen);

                await UniTask.Delay(500);

                _gameObjectResourceManager.DestroyObject(_fpsScreenGameObject);

                _fpsScreenGameObject = null;
                _fpsScreen = null;
            }
        }
    }
}
