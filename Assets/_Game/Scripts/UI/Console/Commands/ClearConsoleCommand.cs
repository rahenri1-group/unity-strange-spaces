﻿using Cysharp.Threading.Tasks;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using System;

namespace Game.UI.Console.Command
{
    /// <summary>
    /// Command to clear the console
    /// </summary>
    [Serializable]
    public class ClearConsoleCommand : ICommand { }

    /// <summary>
    /// Command processor for <see cref="ClearConsoleCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class ClearConsoleCommandProcessor : BaseConsoleCommandProcessor<ClearConsoleCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "clear";

        /// <inheritdoc/>
        public override string CommandDescription => "Clears all logs from the console";

        private IConsoleManager _consoleManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public ClearConsoleCommandProcessor(IConsoleManager consoleManager)
        {
            _consoleManager = consoleManager;
        }

        /// <inheritdoc/>
        public override UniTask Execute(ClearConsoleCommand command)
        {
            _consoleManager.DeveloperConsole.ClearConsole();

            return UniTask.CompletedTask;
        }
    }
}