﻿using Cysharp.Threading.Tasks;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using System;

namespace Game.UI.Console.Command
{
    /// <summary>
    /// Command to set the font size of the console
    /// </summary>
    [Serializable]
    public class ConsoleFontSizeCommand : ICommand
    {
        /// <summary>
        /// The font size
        /// </summary>
        public int FontSize;
    }

    /// <summary>
    /// Command processor for <see cref="ConsoleFontSizeCommand"/>
    /// </summary>
    [Dependency(
        contract: typeof(ICommandProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IConsoleCommandProcessor),
        lifetime: Lifetime.Singleton)]
    public class ConsoleFontSizeCommandProcessor : BaseConsoleCommandProcessor<ConsoleFontSizeCommand>
    {
        /// <inheritdoc/>
        public override string CommandName => "font-size";

        /// <inheritdoc/>
        public override string CommandDescription => "Sets the console font size";

        private IConsoleManager _consoleManager;

        /// <summary>
        /// Injection constructor
        /// </summary>
        public ConsoleFontSizeCommandProcessor(IConsoleManager consoleManager)
        {
            _consoleManager = consoleManager;
        }

        /// <inheritdoc/>
        public override ConsoleFontSizeCommand ParseCommand(string[] args)
        {
            if (args.Length == 0) return null;

            int fontSize = ParseInt(args[0], 50);

            return new ConsoleFontSizeCommand
            {
                FontSize = fontSize
            };
        }

        /// <inheritdoc/>
        public override UniTask Execute(ConsoleFontSizeCommand command)
        {
            _consoleManager.DeveloperConsole.LogView.FontSize = command.FontSize;

            return UniTask.CompletedTask;
        }
    }
}
