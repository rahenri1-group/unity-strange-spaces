﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Command;
using Game.Core.DependencyInjection;
using Game.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Game.UI.Console
{
    public class DeveloperConsoleBehaviour : BaseUiScreen
    {
        public IConsoleLogView LogView => _logView;

        public int MaxConsoleHistory { get; set; } = 10;

        [SerializeField] private DeveloperConsoleLogViewBehaviour _logView = null;
        [SerializeField] private ScrollRect _consoleScrollRect = null;
        [SerializeField] private TMP_InputField _commentInputField = null;
        
        [Inject] private ICommandManager _commandManager = null;
        [Inject] private IDeveloperInput _consoleInput = null;
        [Inject] private ILogRouter _logRouter = null;

        private LinkedList<string> _consoleHistory;
        private LinkedListNode<string> _consoleHistoryPosition;

        private IEnumerator _resetViewCoroutine;

        private void Start()
        {
            Assert.IsNotNull(LogView);
            Assert.IsNotNull(_commentInputField);
            Assert.IsNotNull(_consoleScrollRect);

            _consoleHistory = new LinkedList<string>();
            _consoleHistoryPosition = null;

            _commentInputField.onSubmit.AddListener(OnCommandSubmit);
            _consoleInput.NavigateActionTriggered += OnUiNavigate;

            _resetViewCoroutine = null;
        }

        private void OnDestroy()
        {
            _consoleHistory.Clear();
            _consoleHistoryPosition = null;

            _commentInputField.onSubmit.RemoveListener(OnCommandSubmit);
            _consoleInput.NavigateActionTriggered -= OnUiNavigate;
        }

        public override void Show()
        {
            base.Show();

            if (_resetViewCoroutine == null)
            {
                _resetViewCoroutine = ResetView();
                StartCoroutine(_resetViewCoroutine);
            }
        }

        public void ClearConsole()
        {
            LogView.ClearLogs();
        }

        private void OnCommandSubmit(string commandText)
        {
            if (_commentInputField.wasCanceled) return;
            if (string.IsNullOrWhiteSpace(_commentInputField.text)) return;

            commandText = commandText.Trim();

            _consoleHistory.AddFirst(commandText);
            while (_consoleHistory.Count > MaxConsoleHistory)
                _consoleHistory.RemoveLast();

            LogView.ProcessLog(new LogData(string.Empty, LogLevel.Info, commandText));

            UniTask.Create(async () =>
            {
                try
                {
                    await _commandManager.Execute(commandText);
                }
                catch(Exception e)
                {
                    _logRouter.LogException(e);
                }

                if (_resetViewCoroutine == null)
                {
                    _resetViewCoroutine = ResetView();
                    StartCoroutine(_resetViewCoroutine);
                }
            }).Forget();
        }

        private void OnUiNavigate(InputContext context, Vector2 navigation)
        {
            if (!Visible) return;
            if (_consoleHistory.Count == 0) return;
            if (navigation.y == 0) return;

            if (navigation.y > 0)
            {
                if (_consoleHistoryPosition == null)
                {
                    _consoleHistoryPosition = _consoleHistory.First;
                }
                else if (_consoleHistoryPosition.Next != null)
                {
                    _consoleHistoryPosition = _consoleHistoryPosition.Next;
                }
            }
            else if (navigation.y < 0)
            {
                if (_consoleHistoryPosition != null && _consoleHistoryPosition.Previous != null)
                {
                    _consoleHistoryPosition = _consoleHistoryPosition.Previous;
                }
            }

            if (_consoleHistoryPosition != null)
            {
                UniTask.Create(async () =>
                {
                    await UniTask.DelayFrame(1);

                    string command = _consoleHistoryPosition.Value;
                    _commentInputField.text = command;
                    _commentInputField.caretPosition = command.Length;
                    _commentInputField.ForceLabelUpdate();
                }).Forget();
            }
        }

        private IEnumerator ResetView()
        {
            _consoleHistoryPosition = null;

            yield return new WaitForEndOfFrame();

            _consoleScrollRect.verticalNormalizedPosition = 0f;

            _commentInputField.text = string.Empty;
            _commentInputField.Select();
            _commentInputField.ActivateInputField();

            _resetViewCoroutine = null;
        }
    }
}
