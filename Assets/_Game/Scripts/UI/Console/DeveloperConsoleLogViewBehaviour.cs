﻿using System;
using System.Collections.Generic;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Event;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.Console
{
    public class DeveloperConsoleLogViewBehaviour : InjectedBehaviour, IConsoleLogView
    {
        public int FontSize
        { 
            get => _fontSize;
            set
            {
                _fontSize = value;
                foreach (var text in _displayedLogs)
                {
                    text.fontSize = _fontSize;
                }
            }
        }
        private int _fontSize = 50;

        public int MaxNumberOfLogs { get; set; } = 10;

        [SerializeField] private TMP_Text _baseLogText = null;

        [SerializeField] private Color _logDebugColor = Color.white;
        [SerializeField] private Color _logInfoColor = Color.white;
        [SerializeField] private Color _logWarningColor = Color.white;
        [SerializeField] private Color _logErrorColor = Color.white;
        [SerializeField] private Color _logExceptionColor = Color.white;

        [Inject] private IConsoleLogProcessor _consoleLogProcessor = null;
        [Inject] private IEventBus _eventBus = null;

        private LinkedList<TMP_Text> _displayedLogs = new LinkedList<TMP_Text>();

        private bool _readyForLogs;

        private void Start()
        {            
            Assert.IsNotNull(_baseLogText);
            Assert.IsFalse(_baseLogText.gameObject.activeInHierarchy);

            _readyForLogs = true;

            _consoleLogProcessor.SetLogView(this);

            _eventBus.Subscribe<ModulesShutdownEvent>(OnModulesShutdown);
        }

        private void OnDestroy()
        {
            _consoleLogProcessor.SetLogView(null);

            _eventBus.Unsubscribe<ModulesShutdownEvent>(OnModulesShutdown);
        }

        private void OnModulesShutdown(ModulesShutdownEvent eventArgs)
        {
            _readyForLogs = false;
        }

        public void ClearLogs()
        {
            foreach (var log in _displayedLogs)
            {
                Destroy(log.gameObject);
            }

            _displayedLogs.Clear();
        }

        public void ProcessLog(LogData log)
        {
            if (!_readyForLogs)
            {
                return;
            }

            var logObject = CreateLogObject();

            logObject.text = !string.IsNullOrEmpty(log.Category) ? $"{log.Category}: " : string.Empty;

            if (!string.IsNullOrEmpty(log.Message))
            {
                logObject.text += log.Message;
            }

            if (log.Exception != null)
            {
                if (!string.IsNullOrEmpty(log.Exception.Message))
                {
                    logObject.text += log.Exception.Message;
                }

                logObject.text += log.Exception.StackTrace;
            }

            switch (log.Level)
            {
                case LogLevel.Debug:
                    logObject.color = _logDebugColor;
                    break;
                case LogLevel.Info:
                    logObject.color = _logInfoColor;
                    break;
                case LogLevel.Warning:
                    logObject.color = _logWarningColor;
                    break;
                case LogLevel.Error:
                    logObject.color = _logErrorColor;
                    break;
                case LogLevel.Exception:
                    logObject.color = _logExceptionColor;
                    break;
            }
        }

        private TMP_Text CreateLogObject()
        {
            var text = Instantiate(_baseLogText, transform);
            text.gameObject.name = "@Log";
            text.text = string.Empty;
            text.fontSize = FontSize;
            text.gameObject.SetActive(true);

            _displayedLogs.AddLast(text);

            while (_displayedLogs.Count > MaxNumberOfLogs)
            {
                var oldLogText = _displayedLogs.First.Value;

                _displayedLogs.RemoveFirst();

                Destroy(oldLogText.gameObject);
            }

            return text;
        }
    }
}