﻿using Game.Core;

namespace Game.UI.Console
{
    public interface IConsoleLogView
    {
        int FontSize { get; set; }
        int MaxNumberOfLogs { get; set; }

        void ProcessLog(LogData log);

        void ClearLogs();
    }
}
