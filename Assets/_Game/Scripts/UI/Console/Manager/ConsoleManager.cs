﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.DependencyInjection;
using Game.Core.Resource;
using Game.Core.UI;
using Game.Input;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI.Console
{
    [Dependency(
        contract: typeof(IConsoleManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class ConsoleManager : BaseModule, IConsoleManager
    {
        public override string ModuleName => "Console Manager";

        public override ModuleConfig ModuleConfig => Config;
        public ConsoleManagerConfig Config = new ConsoleManagerConfig();

        public DeveloperConsoleBehaviour DeveloperConsole { get; private set; } = null;

        private readonly IDeveloperInput _developerInput;
        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IUiManager _uiManager;

        private GameObject _developerConsoleGameObject;

        public ConsoleManager(
            IDeveloperInput developerInput,
            IGameObjectResourceManager gameObjectResourceManager,
            IUiManager uiManager,
            ILogRouter logger)
            : base(logger)
        {
            _developerInput = developerInput;
            _gameObjectResourceManager = gameObjectResourceManager;
            _uiManager = uiManager;

            DeveloperConsole = null;
            _developerConsoleGameObject = null;
        }

        public async override UniTask Initialize()
        {
            Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(Config.ConsoleAssetKey));

            _developerInput.InputEnabled = true;

            var devConsole = await _gameObjectResourceManager.InstantiateAsync<DeveloperConsoleBehaviour>(Config.ConsoleAssetKey, null, Vector3.zero);

            _developerConsoleGameObject = devConsole.GameObject;
            _developerConsoleGameObject.name = "@DeveloperConsole";

            DeveloperConsole = devConsole.Component;
            DeveloperConsole.LogView.MaxNumberOfLogs = Config.MaxNumberOfLogs;
            DeveloperConsole.LogView.FontSize = Config.ConsoleFontSize;

            _developerInput.DeveloperConsoleToggleTriggered += OnDeveloperConsoleToggle;

            await base.Initialize();
        }

        public override UniTask Shutdown()
        {
            _developerInput.DeveloperConsoleToggleTriggered -= OnDeveloperConsoleToggle;

            if (DeveloperConsole != null)
            {
                _gameObjectResourceManager.DestroyObject(_developerConsoleGameObject);

                DeveloperConsole = null;
                _developerConsoleGameObject = null;
            }

            return base.Shutdown();
        }

        private void OnDeveloperConsoleToggle(InputContext context, bool consoleTriggered)
        {
            if (!consoleTriggered) return;

            if (DeveloperConsole.Visible)
            {
                _uiManager.HideScreenSpaceUiScreen(DeveloperConsole);
            }
            else
            {
                _uiManager.ShowScreenSpaceUiScreen(DeveloperConsole);
            }
        }
    }
}
