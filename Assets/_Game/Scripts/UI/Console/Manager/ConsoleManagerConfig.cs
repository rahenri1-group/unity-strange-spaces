﻿using Game.Core;

namespace Game.UI.Console
{
    public class ConsoleManagerConfig : ModuleConfig
    {
        public string ConsoleAssetKey = string.Empty;

        public int MaxNumberOfLogs = 64;

        public int ConsoleFontSize = 50;
    }
}
