﻿using Game.Core;

namespace Game.UI.Console
{
    public interface IConsoleManager : IModule
    {
        DeveloperConsoleBehaviour DeveloperConsole { get; }
    }
}
