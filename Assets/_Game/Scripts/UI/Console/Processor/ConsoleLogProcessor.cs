﻿using Game.Core;
using Game.Core.DependencyInjection;

namespace Game.UI.Console
{
    [Dependency(
        contract: typeof(IConsoleLogProcessor),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(ILogProcessor),
        lifetime: Lifetime.Singleton)]
    public class ConsoleLogProcessor : BaseLogProcessor, IConsoleLogProcessor
    {
        protected override bool ReadyToProcess => _consoleLogView != null;

        private IConsoleLogView _consoleLogView;

        public ConsoleLogProcessor()
            : base()
        {
            _consoleLogView = null;
        }

        protected override void ProcessLogData(LogData log)
        {
            _consoleLogView.ProcessLog(log);
        }

        public void SetLogView(IConsoleLogView consoleLogView)
        {
            _consoleLogView = consoleLogView;

            if (ReadyToProcess)
            {
                ProcessCachedLogs();
            }
        }
    }
}
