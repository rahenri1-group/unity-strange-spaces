﻿namespace Game.UI.Console
{
    public interface IConsoleLogProcessor
    {
        void SetLogView(IConsoleLogView consoleLogView);
    }
}
