﻿using Game.Core;
using Game.Core.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.UI
{
    /// <inheritdoc cref="ICursorHoverable"/>
    public class CursorHoverable : InjectedBehaviour, ICursorHoverable, IPointerEnterHandler, IPointerExitHandler
    {
        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        public event Action<ICursorHoverable> CursorEnter;
        /// <inheritdoc />
        public event Action<ICursorHoverable> CursorExit;

        public void OnPointerEnter(PointerEventData eventData)
        {
            CursorEnter?.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            CursorExit?.Invoke(this);
        }
    }
}
