﻿using Game.Core;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class FpsTextBehaviour : MonoBehaviour
    {
        [SerializeField] private FloatReadonlyReference _updatePeriod = null;

        private TMP_Text _text;

        private IEnumerator _updateCoroutine;

        private void Awake()
        {
            Assert.IsTrue(_updatePeriod > 0f);

            _text = GetComponent<TMP_Text>();
            _text.text = "0";

            _updateCoroutine = null;
        }

        private void OnEnable()
        {
            _updateCoroutine = UpdateFps();
            StartCoroutine(_updateCoroutine);
        }

        private void OnDisable()
        {
            if (_updateCoroutine != null)
            {
                StopCoroutine(_updateCoroutine);
            }

            _updateCoroutine = null;
        }

        private IEnumerator UpdateFps()
        {
            YieldInstruction wait = new WaitForSeconds(_updatePeriod);

            while (true)
            {
                _text.text = string.Format("{0:N0}", 1f / Time.unscaledDeltaTime);

                yield return wait;
            }
        }
    }
}
