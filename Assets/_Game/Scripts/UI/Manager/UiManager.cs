﻿using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Render.Camera;
using Game.Core.DependencyInjection;
using Game.Core.Input;
using Game.Core.Resource;
using Game.Core.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

namespace Game.UI
{
    [Dependency(
        contract: typeof(IUiManager),
        lifetime: Lifetime.Singleton)]
    [Dependency(
        contract: typeof(IModule),
        lifetime: Lifetime.Singleton)]
    public class UiManager : BaseModule, IUiManager
    {
        public override string ModuleName => "UI Manager";

        public override ModuleConfig ModuleConfig => Config;
        public UiManagerConfig Config = new UiManagerConfig();

        private readonly ICameraManager _cameraManager;
        private readonly IGameObjectResourceManager _gameObjectResourceManager;
        private readonly IInputManager _inputManager;

        private IUiCamera _uiCamera;

        private GameObject _uiEventSystemGameObject;

        private HashSet<IUiScreen> _activeUiScreens;

        private bool _preUiCursorVisible;
        private CursorLockMode _preUiCursorLockMode;
        private Dictionary<IInputGroup, bool> _prevActionsEnabled;

        public UiManager(
            ICameraManager cameraManager,
            IGameObjectResourceManager gameObjectResourceManager,
            IInputManager inputManager,
            ILogRouter logger)
            : base(logger)
        {
            _cameraManager = cameraManager;
            _gameObjectResourceManager = gameObjectResourceManager;
            _inputManager = inputManager;

            _activeUiScreens = new HashSet<IUiScreen>();

            _preUiCursorVisible = true;
            _preUiCursorLockMode = CursorLockMode.None;
            _prevActionsEnabled = new Dictionary<IInputGroup, bool>();
        }

        public override async UniTask Initialize()
        {
            Assert.IsTrue(_gameObjectResourceManager.IsAssetKeyValid(Config.EventSystemAssetKey));

            _uiCamera = await _cameraManager.CreateCamera<IUiCamera>();

            var eventSystem = await _gameObjectResourceManager.InstantiateAsync<EventSystem>(Config.EventSystemAssetKey, null, Vector3.zero);
            _uiEventSystemGameObject = eventSystem.GameObject;

            await base.Initialize();
        }

        public override UniTask Shutdown()
        {
            _gameObjectResourceManager.DestroyObject(_uiEventSystemGameObject);

            _activeUiScreens.Clear();

            return base.Shutdown();
        }

        public void ShowScreenSpaceUiScreen(IUiScreen screen)
        {
            if (_activeUiScreens.Contains(screen)) return;

            screen.Show();

            if (_activeUiScreens.Where(s => s.UsesCursor).Count() == 0 && screen.UsesCursor)
            {
                _preUiCursorVisible = Cursor.visible;
                _preUiCursorLockMode = Cursor.lockState;
                foreach (var inputGroup in _inputManager.InputGroups)
                {
                    _prevActionsEnabled[inputGroup] = inputGroup.InputEnabled;
                }

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                foreach (var inputGroup in _inputManager.InputGroups)
                {
                    if (!Config.AllowedInputGroupTypes.Contains(inputGroup.GetType()))
                    {
                        inputGroup.InputEnabled = false;
                    }
                }
            }

            _activeUiScreens.Add(screen);
        }

        public void HideScreenSpaceUiScreen(IUiScreen screen)
        {
            if (!_activeUiScreens.Contains(screen)) return;

            screen.Hide();

            _activeUiScreens.Remove(screen);

            if (_activeUiScreens.Where(s => s.UsesCursor).Count() == 0)
            {
                Cursor.visible = _preUiCursorVisible;
                Cursor.lockState = _preUiCursorLockMode;
                foreach (var inputGroup in _inputManager.InputGroups)
                {
                    inputGroup.InputEnabled = _prevActionsEnabled[inputGroup];
                }
            }
        }
    }
}
