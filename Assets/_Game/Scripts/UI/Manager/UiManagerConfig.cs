﻿using Game.Core;
using System;

namespace Game.UI
{
    public class UiManagerConfig : ModuleConfig
    {
        public string EventSystemAssetKey = string.Empty;
        public Type[] AllowedInputGroupTypes = new Type[0];
    }
}
