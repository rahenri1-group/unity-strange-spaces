﻿using Game.Core;
using UnityEngine;

namespace Game.UI
{
    public class SimpleUiScreenBehaviour : BaseUiScreen
    {
        public override bool UsesCursor => _usesCursor;
        [SerializeField] private BoolReadonlyReference _usesCursor = null;
    }
}