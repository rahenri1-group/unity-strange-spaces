Shader "Custom/Portal"
{
	Properties
	{
		_BaseMap("Base Map", 2D) = "white"
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "RenderPipeline"="UniversalRenderPipeline" }
		
		Pass
		{
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD0;
			};

			TEXTURE2D(_BaseMap);
			SAMPLER(sampler_BaseMap);

			CBUFFER_START(UnityPerMaterial)
				sampler2D _BaseMap_ST;
			CBUFFER_END
			
			v2f vert(appdata v)
			{
				v2f o;

				VertexPositionInputs vertInputs = GetVertexPositionInputs(v.vertex.xyz); //This function calculates all the relative spaces of the objects vertices
				o.vertex = vertInputs.positionCS;
				o.screenPos = vertInputs.positionNDC; // grab position on screen

				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				float2 uv = (i.screenPos.xy / i.screenPos.w);
				float4 col = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				return col;
			}
			ENDHLSL
		}
	}
}
