Shader "Custom/PortalDither"
{
	Properties
	{
		_BaseMap("Base Map", 2D) = "white" {}
		_Alpha("Alpha", Range(0, 1)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "RenderPipeline"="UniversalRenderPipeline" }
		
		Pass
		{
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD0;
			};

			TEXTURE2D(_BaseMap);
			SAMPLER(sampler_BaseMap);

			CBUFFER_START(UnityPerMaterial)
				sampler2D _BaseMap_ST;
				half _Alpha;
			CBUFFER_END
			
			v2f vert(appdata v)
			{
				v2f o;

				VertexPositionInputs vertInputs = GetVertexPositionInputs(v.vertex.xyz); //This function calculates all the relative spaces of the objects vertices
				o.vertex = vertInputs.positionCS;
				o.screenPos = vertInputs.positionNDC; // grab position on screen

				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				float2 uv = (i.screenPos.xy / i.screenPos.w);
				float4 col = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);

				if (_Alpha < 1)
				{
					// Screen-door transparency: Discard pixel if below threshold.
					float4x4 thresholdMatrix =
					{ 1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
					  13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
					   4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
					  16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
					};
					float4x4 _RowAccess = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
					float2 pos = i.screenPos.xy / i.screenPos.w;
					pos *= _ScreenParams.xy; // pixel position
					clip(_Alpha - thresholdMatrix[fmod(pos.x, 4)] * _RowAccess[fmod(pos.y, 4)]);
				}

				return col;
			}
			ENDHLSL
		}
	}
}
