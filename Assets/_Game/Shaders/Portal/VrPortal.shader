Shader "Custom/VrPortal"
{
	Properties
	{
		_LeftEyeTexture("Left Eye Texture", 2D) = "white" {}
		_RightEyeTexture("Left Eye Texture", 2D) = "white" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" }

		Pass
		{
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile __ STEREO_RENDER

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD0;
			};

			TEXTURE2D(_LeftEyeTexture);
			SAMPLER(sampler_LeftEyeTexture);

			TEXTURE2D(_RightEyeTexture);
			SAMPLER(sampler_RightEyeTexture);

			CBUFFER_START(UnityPerMaterial)
				sampler2D _LeftEyeTexture_ST;
				sampler2D _RightEyeTexture_ST;
			CBUFFER_END

			v2f vert(appdata v)
			{
				v2f o;

				VertexPositionInputs vertInputs = GetVertexPositionInputs(v.vertex.xyz); //This function calculates all the relative spaces of the objects vertices
				o.vertex = vertInputs.positionCS;
				o.screenPos = vertInputs.positionNDC; // grab position on screen

				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				float2 uv = (i.screenPos.xy / i.screenPos.w);
				float4 col = float4(0.0, 0.0, 0.0, 0.0);
				if (unity_CameraProjection[0][2] < 0)
				{
					col = SAMPLE_TEXTURE2D(_LeftEyeTexture, sampler_LeftEyeTexture, uv);
				}
				else 
				{
					col = SAMPLE_TEXTURE2D(_RightEyeTexture, sampler_RightEyeTexture, uv);
				}

				return col;
			}

			ENDHLSL
		}
	}
}
