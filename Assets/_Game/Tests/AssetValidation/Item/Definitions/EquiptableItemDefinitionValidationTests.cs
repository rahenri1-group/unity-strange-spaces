﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Validation for instances of <see cref="EquiptableItemDefinition"/>
    /// </summary>
    public class EquiptableItemDefinitionValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private EquiptableItemDefinition[] _allEquiptableDefinitions;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allEquiptableDefinitions = AssetDatabaseUtil.FindAllScriptableObjectInstances<EquiptableItemDefinition>();
        }

        [TearDown]
        public void TearDown()
        {
            _allEquiptableDefinitions = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Equiptable_Address()
        {
            foreach (var itemDefinition in _allEquiptableDefinitions)
            {
                var equiptableAddress = itemDefinition.EquiptableAddress;

                Assert.IsFalse(string.IsNullOrEmpty(equiptableAddress), $"{itemDefinition.Name} missing a {nameof(EquiptableItemDefinition.EquiptableAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(equiptableAddress), $"{itemDefinition.Name} has an invalid {nameof(EquiptableItemDefinition.EquiptableAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(equiptableAddress), $"{itemDefinition.Name} {nameof(EquiptableItemDefinition.EquiptableAddress)} doesn't point to a GameObject");
            }
        }
    }
}
