﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Validation for instances of <see cref="HealthItemDefinition"/>
    /// </summary>
    public class HealthItemDefinitionValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private HealthItemDefinition[] _allHealthDefinitions;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allHealthDefinitions = AssetDatabaseUtil.FindAllScriptableObjectInstances<HealthItemDefinition>();
        }

        [TearDown]
        public void TearDown()
        {
            _allHealthDefinitions = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Heal_Amount_Is_Valid()
        {
            foreach (var itemDefinition in _allHealthDefinitions)
            {
                Assert.GreaterOrEqual(itemDefinition.HealAmount, 0, $"{itemDefinition.Name} has a negative {nameof(HealthItemDefinition.HealAmount)}");
            }
        }

        [Test]
        public void Has_Valid_Heal_Effect_Address()
        {
            foreach (var itemDefinition in _allHealthDefinitions)
            {
                string healEffectAddress = itemDefinition.HealEffectAddress;
                if (!string.IsNullOrEmpty(healEffectAddress))
                {
                    Assert.IsTrue(_assetResourceManager.IsAddressValid(healEffectAddress), $"{itemDefinition.Name} has an invalid {nameof(HealthItemDefinition.HealEffectAddress)}");
                    Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(healEffectAddress), $"{itemDefinition.Name} {nameof(HealthItemDefinition.HealEffectAddress)} doesn't point to a GameObject");
                }
            }
        }

        [Test]
        public void Has_Valid_Equiptable_Address()
        {
            foreach (var itemDefinition in _allHealthDefinitions)
            {
                var equiptableAddress = itemDefinition.EquiptableAddress;

                Assert.IsFalse(string.IsNullOrEmpty(equiptableAddress), $"{itemDefinition.Name} missing a {nameof(HealthItemDefinition.EquiptableAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(equiptableAddress), $"{itemDefinition.Name} has an invalid {nameof(HealthItemDefinition.EquiptableAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(equiptableAddress), $"{itemDefinition.Name} {nameof(HealthItemDefinition.EquiptableAddress)} doesn't point to a GameObject");
            }
        }
    }
}
