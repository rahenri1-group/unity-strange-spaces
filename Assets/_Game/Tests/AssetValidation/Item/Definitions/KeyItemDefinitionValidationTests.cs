﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Validation for instances of <see cref="KeyItemDefinition"/>
    /// </summary>
    public class KeyItemDefinitionValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private KeyItemDefinition[] _allKeyItemDefinitions;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allKeyItemDefinitions = AssetDatabaseUtil.FindAllScriptableObjectInstances<KeyItemDefinition>();
        }

        [TearDown]
        public void TearDown()
        {
            _allKeyItemDefinitions = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Equiptable_Address()
        {
            foreach (var itemDefinition in _allKeyItemDefinitions)
            {
                var equiptableAddress = itemDefinition.EquiptableAddress;

                Assert.IsFalse(string.IsNullOrEmpty(equiptableAddress), $"{itemDefinition.Name} missing a {nameof(KeyItemDefinition.EquiptableAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(equiptableAddress), $"{itemDefinition.Name} has an invalid {nameof(KeyItemDefinition.EquiptableAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(equiptableAddress), $"{itemDefinition.Name} {nameof(KeyItemDefinition.EquiptableAddress)} doesn't point to a GameObject");
            }
        }

        [Test]
        public void Has_Valid_Unlock_Model_Address()
        {
            foreach (var itemDefinition in _allKeyItemDefinitions)
            {
                var equiptableAddress = itemDefinition.UnlockModelAddress;

                Assert.IsFalse(string.IsNullOrEmpty(equiptableAddress), $"{itemDefinition.Name} missing a {nameof(KeyItemDefinition.UnlockModelAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(equiptableAddress), $"{itemDefinition.Name} has an invalid {nameof(KeyItemDefinition.UnlockModelAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(equiptableAddress), $"{itemDefinition.Name} {nameof(KeyItemDefinition.UnlockModelAddress)} doesn't point to a GameObject");
            }
        }
    }
}
