using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Game.Item
{
    /// <summary>
    /// Validation for instances of <see cref="BasePlayerItemDefinition"/>
    /// </summary>
    public class PlayerItemDefinitionValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private BasePlayerItemDefinition[] _allPlayerItemDefinitions;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allPlayerItemDefinitions = AssetDatabaseUtil.FindAllScriptableObjectInstances<BasePlayerItemDefinition>();
        }

        [TearDown]
        public void TearDown()
        {
            _allPlayerItemDefinitions = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Entity_Address()
        {
            foreach (var itemDefinition in _allPlayerItemDefinitions.OfType<IWorldItemDefinition>())
            {
                var entityAddress = itemDefinition.ItemEntityAddress;

                Assert.IsFalse(string.IsNullOrEmpty(entityAddress), $"{itemDefinition.Name} missing a {nameof(IWorldItemDefinition.ItemEntityAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(entityAddress), $"{itemDefinition.Name} has an invalid {nameof(IWorldItemDefinition.ItemEntityAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(entityAddress), $"{itemDefinition.Name} {nameof(IWorldItemDefinition.ItemEntityAddress)} doesn't point to a GameObject");
            }
        }

        [Test]
        public void Has_Valid_Inventory_Sprite_Address()
        {
            foreach (var itemDefinition in _allPlayerItemDefinitions)
            {
                var inventorySpriteAddress = itemDefinition.InventorySpriteIconAddress;
                Assert.IsFalse(string.IsNullOrEmpty(inventorySpriteAddress), $"{itemDefinition.Name} missing a {nameof(BasePlayerItemDefinition.InventorySpriteIconAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(inventorySpriteAddress), $"{itemDefinition.Name} has an invalid {nameof(BasePlayerItemDefinition.InventorySpriteIconAddress)}");
                Assert.AreEqual(typeof(Texture2D), _assetResourceManager.GetMainTypeOfAssetAtAddress(inventorySpriteAddress), $"{itemDefinition.Name} {nameof(BasePlayerItemDefinition.InventorySpriteIconAddress)} doesn't point to a Texture");
            }
        }

        [Test]
        public void Has_Valid_Inventory_Preview_Address()
        {
            foreach (var itemDefinition in _allPlayerItemDefinitions)
            {
                var inventoryPreviewAddress = itemDefinition.InventoryPreviewModelAddress;
                Assert.IsFalse(string.IsNullOrEmpty(inventoryPreviewAddress), $"{itemDefinition.Name} missing a {nameof(BasePlayerItemDefinition.InventoryPreviewModelAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(inventoryPreviewAddress), $"{itemDefinition.Name} has an invalid {nameof(BasePlayerItemDefinition.InventoryPreviewModelAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(inventoryPreviewAddress), $"{itemDefinition.Name} {nameof(BasePlayerItemDefinition.InventoryPreviewModelAddress)} doesn't point to a GameObject");
            }
        }

        [Test]
        public void Has_Valid_Pickup_Effect_Address()
        {
            foreach (var itemDefinition in _allPlayerItemDefinitions)
            {
                var pickupEffectAddress = itemDefinition.PickupEffectAddress;
                if (!string.IsNullOrEmpty(pickupEffectAddress))
                {
                    Assert.IsTrue(_assetResourceManager.IsAddressValid(pickupEffectAddress), $"{itemDefinition.Name} has an invalid {nameof(BasePlayerItemDefinition.PickupEffectAddress)}");
                    Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(pickupEffectAddress), $"{itemDefinition.Name} {nameof(BasePlayerItemDefinition.PickupEffectAddress)} doesn't point to a GameObject");
                }
            }
        }

        [Test]
        public void Has_Valid_Stash_Effect_Address()
        {
            foreach (var itemDefinition in _allPlayerItemDefinitions)
            {
                var stashEffectAddress = itemDefinition.StashEffectAddress;
                if (!string.IsNullOrEmpty(stashEffectAddress))
                {
                    Assert.IsTrue(_assetResourceManager.IsAddressValid(stashEffectAddress), $"{itemDefinition.Name} has an invalid {nameof(BasePlayerItemDefinition.StashEffectAddress)}");
                    Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(stashEffectAddress), $"{itemDefinition.Name} {nameof(BasePlayerItemDefinition.StashEffectAddress)} doesn't point to a GameObject");
                }
            }
        }
    }
}
