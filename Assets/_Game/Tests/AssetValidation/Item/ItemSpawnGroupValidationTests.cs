﻿using Game.Core.Item;
using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;

namespace Game.Item
{
    /// <summary>
    /// Validation for instances of <see cref="ItemSpawnGroup"/>
    /// </summary>
    public class ItemSpawnGroupValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private ItemSpawnGroup[] _allItemSpawnGroups;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allItemSpawnGroups = AssetDatabaseUtil.FindAllScriptableObjectInstances<ItemSpawnGroup>();
        }

        [TearDown]
        public void TearDown()
        {
            _allItemSpawnGroups = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Item_Definitions()
        {
            foreach (var spawnGroup in _allItemSpawnGroups)
            {
                Assert.Greater(spawnGroup.ItemSpawnDefinitions.Length, 0, $"{spawnGroup.Name} does not contain any {nameof(ItemSpawnDefinition)}");
            }
        }

        [Test]
        public void All_Spawn_Have_Definitions()
        {
            foreach (var spawnGroup in _allItemSpawnGroups)
            {
                foreach (var spawnDefinition in spawnGroup.ItemSpawnDefinitions)
                {
                    Assert.IsNotNull(spawnDefinition.ItemDefinition, $"{spawnGroup.Name} has spawn missing a {nameof(IItemDefinition)}");
                }
            }
        }

        [Test]
        public void All_Spawn_Max_Is_Valid()
        {
            foreach (var spawnGroup in _allItemSpawnGroups)
            {
                foreach (var spawnDefinition in spawnGroup.ItemSpawnDefinitions)
                {
                    Assert.GreaterOrEqual(spawnDefinition.MaxSpawnCount, 0, $"{spawnGroup.Name} has a negative {nameof(ItemSpawnDefinition.MaxSpawnCount)}");
                }
            }
        }

        [Test]
        public void All_Spawn_Min_Is_Valid()
        {
            foreach (var spawnGroup in _allItemSpawnGroups)
            {
                foreach (var spawnDefinition in spawnGroup.ItemSpawnDefinitions)
                {
                    Assert.GreaterOrEqual(spawnDefinition.MinSpawnCount, 0, $"{spawnGroup.Name} has a negative {nameof(ItemSpawnDefinition.MinSpawnCount)}");
                }
            }
        }

        [Test]
        public void All_Spawn_Max_Is_Greater_Than_Min()
        {
            foreach (var spawnGroup in _allItemSpawnGroups)
            {
                foreach (var spawnDefinition in spawnGroup.ItemSpawnDefinitions)
                {
                    Assert.GreaterOrEqual(spawnDefinition.MaxSpawnCount, spawnDefinition.MinSpawnCount, $"{spawnGroup.Name} {nameof(ItemSpawnDefinition.MinSpawnCount)} is greater than {nameof(ItemSpawnDefinition.MaxSpawnCount)}");
                }
            }
        }
    }
}
