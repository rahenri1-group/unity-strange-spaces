using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Game.Physics.PhysicsMaterial.EventProcessor
{
    /// <summary>
    /// Validation for instances of <see cref="FootstepWalkEventEffectSpawner"/>
    /// </summary>
    public class FootstepWalkEventEffectSpawnerValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private FootstepWalkEventEffectSpawner[] _allEffectSpawners;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allEffectSpawners = AssetDatabaseUtil.FindAllScriptableObjectInstances<FootstepWalkEventEffectSpawner>();
        }

        [TearDown]
        public void TearDown()
        {
            _allEffectSpawners = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Effect_Address()
        {
            foreach (var effectSpawner in _allEffectSpawners)
            {
                var entityAddress = effectSpawner.EffectAddress;

                Assert.IsFalse(string.IsNullOrEmpty(entityAddress), $"{effectSpawner.name} missing a {nameof(FootstepWalkEventEffectSpawner.EffectAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(entityAddress), $"{effectSpawner.name} has an invalid {nameof(FootstepWalkEventEffectSpawner.EffectAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(entityAddress), $"{effectSpawner.name} {nameof(FootstepWalkEventEffectSpawner.EffectAddress)} doesn't point to a GameObject");
            }
        }
    }
}
