﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Game.Physics.PhysicsMaterial.EventProcessor
{
    /// <summary>
    /// Validation for instances of <see cref="ShotImpactEventEffectSpawner"/>
    /// </summary>
    public class ShotImpactEventEffectSpawnerValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private ShotImpactEventEffectSpawner[] _allEffectSpawners;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allEffectSpawners = AssetDatabaseUtil.FindAllScriptableObjectInstances<ShotImpactEventEffectSpawner>();
        }

        [TearDown]
        public void TearDown()
        {
            _allEffectSpawners = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Effect_Address()
        {
            foreach (var effectSpawner in _allEffectSpawners)
            {
                var entityAddress = effectSpawner.EffectAddress;

                Assert.IsFalse(string.IsNullOrEmpty(entityAddress), $"{effectSpawner.name} missing a {nameof(ShotImpactEventEffectSpawner.EffectAddress)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(entityAddress), $"{effectSpawner.name} has an invalid {nameof(ShotImpactEventEffectSpawner.EffectAddress)}");
                Assert.AreEqual(typeof(GameObject), _assetResourceManager.GetMainTypeOfAssetAtAddress(entityAddress), $"{effectSpawner.name} {nameof(ShotImpactEventEffectSpawner.EffectAddress)} doesn't point to a GameObject");
            }
        }
    }
}
