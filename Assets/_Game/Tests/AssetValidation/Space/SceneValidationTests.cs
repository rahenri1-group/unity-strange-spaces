﻿using Game.Core.AI.Interest;
using Game.Core.Space;
using Game.Editor.Core;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace Game.Space
{
    /// <summary>
    /// Validation for instances of <see cref="UnityEngine.SceneManagement.Scene"/>
    /// </summary>
    public class SceneValidationTests
    {
        private SpaceData[] _allSpaceDatas;

        [SetUp]
        public void Setup()
        {
            _allSpaceDatas = AssetDatabaseUtil.FindAllScriptableObjectInstances<SpaceData>();
        }

        [TearDown]
        public void TearDown()
        {
            EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Ensure_Stalker_NavMesh_In_Scene()
        {
            var skipSpaceNames = new HashSet<string>(new string[] 
            { 
                "Init",
                "Home",
                "TestSpaceCube",
                "TestSpaceSphere"
            });

            var stalkerNavMeshId = GetNavMeshIdByName("Stalker");

            foreach (var spaceData in _allSpaceDatas)
            {
                if (skipSpaceNames.Contains(spaceData.Name)) continue;

                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                NavMeshSurface stalkerNavMesh = null;
                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    var navMeshSurfaces = rootObject.GetComponentsInChildren<NavMeshSurface>();
                    stalkerNavMesh = navMeshSurfaces.FirstOrDefault(s => s.agentTypeID == stalkerNavMeshId);

                    if (stalkerNavMesh != null) break;
                }

                Assert.IsNotNull(stalkerNavMesh, $"Stalker navmesh missing from {spaceData.Name}");
                if (stalkerNavMesh != null)
                {
                    Assert.AreEqual(stalkerNavMesh.defaultArea, NavMesh.GetAreaFromName("Walkable"), $"Stalker NavMesh not set to 'Walkable' in {spaceData.Name}");
                    Assert.AreEqual(stalkerNavMesh.collectObjects, CollectObjects.Children, $"Stalker NavMesh not set to children objects only in  {spaceData.Name}");
                }
            }
        }

        private int GetNavMeshIdByName(string name)
        {
            for (int i = 0; i < NavMesh.GetSettingsCount(); i++)
            {
                NavMeshBuildSettings settings = NavMesh.GetSettingsByIndex(i);
                if (name == NavMesh.GetSettingsNameFromID(settings.agentTypeID))
                {
                    return settings.agentTypeID;
                }
            }

            return -1;
        }

        [Test]
        public void Ensure_Patrol_Points_In_Scene()
        {
            var skipSpaceNames = new HashSet<string>(new string[]
            {
                "Init",
                "Home",
                "TestSpaceCube",
                "TestSpaceSphere"
            });

            foreach (var spaceData in _allSpaceDatas)
            {
                if (skipSpaceNames.Contains(spaceData.Name)) continue;

                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                var patrolPoints = new List<PatrolPointBehaviour>();
                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    patrolPoints.AddRange(rootObject.GetComponentsInChildren<PatrolPointBehaviour>());
                }

                Assert.IsTrue(patrolPoints.Count > 1, $"Not enought patrol points in {spaceData.Name}");
            }
        }
    }
}
