﻿using Game.Core.Space;
using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEditor;

namespace Game.Core.Entity
{
    /// <summary>
    /// Validation for instances of <see cref="EntityManifestComponent"/>
    /// </summary>
    public class EntityManifestValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private EntityManifestComponent[] _allEntityManifests;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

             var allSpaceDatas = AssetDatabaseUtil.FindAllScriptableObjectInstances<SpaceData>();

            var entityManifests = new List<EntityManifestComponent>();
            foreach (var spaceData in allSpaceDatas)
            {
                var entityManifest = spaceData.GetAdditionalData<EntityManifestComponent>();
                if (entityManifest != null)
                {
                    entityManifests.Add(entityManifest);
                }
            }

            _allEntityManifests = entityManifests.ToArray();
        }

        [TearDown]
        public void TearDown()
        {
            _allEntityManifests = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Unique_Ids_For_All_Entities()
        {
            var ids = new HashSet<Guid>();

            foreach (var manifest in _allEntityManifests)
            {
                foreach (var entityData in manifest.Entities)
                {
                    var id = entityData.Id;
                    Assert.IsFalse(ids.Contains(id), $"Duplicate entity id {id}");

                    ids.Add(id);
                }
            }
        }

        [Test]
        public void Entity_Asset_Keys_Valid()
        {
            foreach (var manifest in _allEntityManifests)
            {
                foreach (var entityData in manifest.Entities)
                {
                    Assert.IsTrue(_assetResourceManager.IsAddressValid(entityData.EntityAssetKey), $"Invalid asset key {entityData.EntityAssetKey}");
                }
            }
        }

        [Test]
        public void Entity_Scales_Valid()
        {
            foreach (var manifest in _allEntityManifests)
            {
                foreach (var entityData in manifest.Entities)
                {
                    var scale = entityData.Scale;
                    Assert.IsTrue(scale.x > 0f && scale.y > 0f && scale.z > 0f, $"Invalid scale for asset id {entityData.Id}");
                }
            }
        }
    }
}
