﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;

namespace Game.Core.Entity
{
    /// <summary>
    /// Validation tests for the <see cref="IDynamicEntityMetadata"/> for <see cref="IEntity"/>s
    /// </summary>
    public class EntityMetadataValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;
        private IMetadataAssetManager _metadataAssetManager;
        private GUID[] _entityAssetIds;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();
            _metadataAssetManager = EditorModuleContext.DiContainer.Resolve<IMetadataAssetManager>();

            _entityAssetIds = _assetResourceManager.GetAllAssetsForLabel("Entity");
        }

        [TearDown]
        public void TearDown()
        {
            _assetResourceManager = null;
            _metadataAssetManager = null;
            _entityAssetIds = null; 
        }

        [Test]
        public void Do_All_Entities_Have_Metadata()
        {
            foreach (var guid in _entityAssetIds)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                Assert.IsTrue(_metadataAssetManager.MetadataExists(assetPath), $"Metadata missing for {assetPath}");
            }
        }

        [Test]
        public void Does_All_Entity_Metadata_Have_IDynamicEntityMetadata()
        {
            foreach (var guid in _entityAssetIds)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                var entityMetadata = _metadataAssetManager.GetMetadataForResource(assetPath).GetMetadataComponent<IDynamicEntityMetadata>();
                Assert.IsNotNull(entityMetadata, $"Metadata missing for {assetPath}");
            }
        }
    }
}
