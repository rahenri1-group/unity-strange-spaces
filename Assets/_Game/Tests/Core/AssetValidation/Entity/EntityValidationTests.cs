﻿using Game.Editor.Core;
using NUnit.Framework;
using System;
using UnityEditor;
using UnityEngine;

namespace Game.Core.Entity
{
    /// <summary>
    /// Tests for instances of <see cref="IEntity"/>
    /// </summary>
    public class EntityValidationTests
    {
        private IEntity[] _allEntities;

        [SetUp]
        public void Setup()
        {
            _allEntities = AssetDatabaseUtil.FindAllPrefabsWithScriptAtRoot<IEntity>();
        }

        [TearDown]
        public void TearDown()
        {
            _allEntities = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Ensure_Zero_Id_For_All_Entities()
        {
            foreach (var entity in _allEntities)
            {
                Assert.AreEqual(Guid.Empty, entity.EntityId, $"Entity {entity.GameObject.name} has non-empty id");
            }
        }

        [Test]
        public void Ensure_Colliders_Have_No_Renderers()
        {
            foreach (var entity in _allEntities)
            {
                foreach (var collider in entity.GetComponentsInChildren<Collider>(true))
                {
                    Assert.IsNull(collider.GetComponent<Renderer>(), $"Renderer on collider {collider.name} in space {entity.GameObject.name}");
                    Assert.IsNull(collider.GetComponent<Canvas>(), $"Canvas on collider {collider.name} in space {entity.GameObject.name}");
                    Assert.IsNull(collider.GetComponent<Light>(), $"Light on collider {collider.name} in space {entity.GameObject.name}");
                }
            }
        }
    }
}
