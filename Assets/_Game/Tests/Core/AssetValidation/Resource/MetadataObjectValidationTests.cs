﻿using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using UnityEditor;

namespace Game.Core.Resource
{
    /// <summary>
    /// Validation for instances of <see cref="MetadataObject"/>
    /// </summary>
    public class MetadataObjectValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private MetadataObject[] _allMetadataObjects;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allMetadataObjects = AssetDatabaseUtil.FindAllScriptableObjectInstances<MetadataObject>();
        }

        [TearDown]
        public void TearDown()
        {
            _allMetadataObjects = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Has_Valid_Resource_Key_Address()
        {
            foreach (var metadataObject in _allMetadataObjects)
            {
                var resourceKey = metadataObject.ResourceKey;

                Assert.IsFalse(string.IsNullOrEmpty(resourceKey), $"{metadataObject.name} missing a {nameof(MetadataObject.ResourceKey)}");
                Assert.IsTrue(_assetResourceManager.IsAddressValid(resourceKey), $"{metadataObject.name} has an invalid {nameof(MetadataObject.ResourceKey)}");
            }
        }
    }
}
