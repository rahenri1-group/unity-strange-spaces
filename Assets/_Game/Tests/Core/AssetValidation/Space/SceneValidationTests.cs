﻿using Game.Core.Entity;
using Game.Core.Physics;
using Game.Core.Portal;
using Game.Editor.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <summary>
    /// Validation for instances of <see cref="Scene"/>
    /// </summary>
    public class SceneValidationTests
    {
        private SpaceData[] _allSpaceDatas;

        [SetUp]
        public void Setup()
        {
            _allSpaceDatas = AssetDatabaseUtil.FindAllScriptableObjectInstances<SpaceData>();
        }

        [TearDown]
        public void TearDown()
        {
            EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Ensure_No_Dynamic_Entities_In_Scene()
        {
            foreach (var spaceData in _allSpaceDatas)
            {
                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    Assert.IsNull(rootObject.GetComponentInChildren<IDynamicEntity>(true), $"Dynamic entity found in {spaceData.Name}");
                }
            }
        }

        [Test]
        public void Ensure_No_Duplicate_Portal_Ids()
        {
            var portalIds = new HashSet<Guid>();

            foreach (var spaceData in _allSpaceDatas)
            {
                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    foreach (var portal in rootObject.GetComponentsInChildren<IDynamicPortal>(true))
                    {
                        var id = portal.PortalId;

                        Assert.IsFalse(portalIds.Contains(id), $"Duplicate portal id {id} found in {spaceData.Name}");

                        portalIds.Add(id);
                    }
                }
            }
        }

        [Test]
        public void Ensure_Colliders_Have_No_Renderers()
        {
            foreach (var spaceData in _allSpaceDatas)
            {
                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    foreach (var collider in rootObject.GetComponentsInChildren<Collider>(true))
                    {
                        Assert.IsNull(collider.GetComponent<Renderer>(), $"Renderer on collider {collider.name} in space {spaceData.Name}");
                        Assert.IsNull(collider.GetComponent<Canvas>(), $"Canvas on collider {collider.name} in space {spaceData.Name}");
                        Assert.IsNull(collider.GetComponent<Light>(), $"Light on collider {collider.name} in space {spaceData.Name}");
                    }
                }
            }
        }

        [Test]
        public void Ensure_Colliders_Have_Physics_Materials()
        {
            var ignoreLayers = new HashSet<int>();
            ignoreLayers.Add(LayerUtil.GetLayerId("Portal"));
            ignoreLayers.Add(LayerUtil.GetLayerId("Player Blocker"));

            foreach (var spaceData in _allSpaceDatas)
            {
                var scene = EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex), OpenSceneMode.Single);

                foreach (var rootObject in scene.GetRootGameObjects())
                {
                    foreach (var collider in rootObject.GetComponentsInChildren<Collider>(true))
                    {
                        if (!collider.isTrigger
                            && !ignoreLayers.Contains(collider.gameObject.layer))
                        {
                            Assert.IsNotNull(collider.GetComponentInParent<IPhysicsMaterial>(true), $"No {nameof(IPhysicsMaterial)} for collider {collider.gameObject.GameObjectPath()} in space {spaceData.Name}");
                        }
                    }
                }
            }
        }
    }
}
