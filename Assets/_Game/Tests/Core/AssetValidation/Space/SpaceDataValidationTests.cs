using Game.Editor.Core;
using Game.Editor.Core.Resource;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace Game.Core.Space
{
    /// <summary>
    /// Validation for instances of <see cref="SpaceData"/>
    /// </summary>
    public class SpaceDataValidationTests
    {
        private IEditorAssetResourceManager _assetResourceManager;

        private SpaceData[] _allSpaceDatas;

        [SetUp]
        public void Setup()
        {
            _assetResourceManager = EditorModuleContext.DiContainer.Resolve<IEditorAssetResourceManager>();

            _allSpaceDatas = AssetDatabaseUtil.FindAllScriptableObjectInstances<SpaceData>();
        }

        [TearDown]
        public void TearDown()
        {
            _allSpaceDatas = null;

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        [Test]
        public void Scene_Build_Indexes_Valid()
        {
            var observedSceneIndexes = new HashSet<int>();

            foreach (var spaceData in _allSpaceDatas)
            {
                Assert.IsFalse(string.IsNullOrEmpty(SceneUtility.GetScenePathByBuildIndex(spaceData.SceneBuildIndex)), $"{spaceData.Name}:{spaceData.SceneBuildIndex} has an invalid build index");
                Assert.IsFalse(observedSceneIndexes.Contains(spaceData.SceneBuildIndex), $"Duplicate scene index found for {spaceData.Name}:{spaceData.SceneBuildIndex}");

                observedSceneIndexes.Add(spaceData.SceneBuildIndex);
            }
        }
    }
}
