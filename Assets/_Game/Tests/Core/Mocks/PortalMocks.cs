﻿#pragma warning disable 67

using Game.Core.Space;
using UnityEngine;

namespace Game.Core.Portal
{
    public class MockPortal : IPortal
    {
        public IPortal EndPoint { get; set; }

        public bool IsOpen { get; set; }

        public bool HasConnection { get; set; }

        public Vector2 PortalSize { get; set; }

        public Plane PortalPlane { get; set; }

        public ISpaceData Space { get; set; }

        public Transform Transform { get; set; }

        public GameObject GameObject { get; set; }

        public event PortalEvent PortalOpened;
        public event PortalEvent PortalClosed;

        public virtual void AttemptOpen()
        {
            throw new System.NotImplementedException();
        }

        public virtual Vector3 CalculateEndPointPosition(Vector3 inputPosition)
        {
            throw new System.NotImplementedException();
        }

        public virtual Quaternion CalculateEndPointRotation(Quaternion inputRotation)
        {
            throw new System.NotImplementedException();
        }

        public virtual Vector3 CalculateEndPointDirection(Vector3 inputDirection)
        {
            throw new System.NotImplementedException();
        }

        public virtual void CalculateEndPointTransform(Vector3 inputPosition, Quaternion inputRotation, out Vector3 endPointPosition, out Quaternion endPointRotation)
        {
            throw new System.NotImplementedException();
        }

        public virtual void Close()
        {
            throw new System.NotImplementedException();
        }

        public virtual Vector3 InverseCalculateEndPointPosition(Vector3 inputPosition)
        {
            throw new System.NotImplementedException();
        }

        public virtual Quaternion InverseCalculateEndPointRotation(Quaternion inputRotation)
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnPortalClosed()
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnPortalOpened(IPortalConnection portalConnection, IPortal otherEndPoint)
        {
            throw new System.NotImplementedException();
        }

        public Vector3 InverseCalculateEndPointDirection(Vector3 inputDirection)
        {
            throw new System.NotImplementedException();
        }
    }

    public class MockPortalPathLink : IPortalPathLink
    {
        public IPortal EntryPortal { get; set; }

        public IPortal ExitPortal { get; set; }
    }
}

#pragma warning restore 67