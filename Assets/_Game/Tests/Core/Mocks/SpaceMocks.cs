﻿#pragma warning disable 67

using System;

namespace Game.Core.Space
{
    public class MockSpaceData : ISpaceData
    {
        public string Name { get; set; }

        public Guid SpaceId { get; set; }

        public bool Equals(ISpaceData other)
        {
            return SpaceId == other.SpaceId;
        }

        public virtual T GetAdditionalData<T>() where T : class, ISpaceDataComponent
        {
            throw new NotImplementedException();
        }
    }
}

#pragma warning restore 67