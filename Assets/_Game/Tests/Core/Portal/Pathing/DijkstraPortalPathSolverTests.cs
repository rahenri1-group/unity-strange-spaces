using Game.Core.Space;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Portal
{
    public class DijkstraPortalPathSolverTests
    {
        private ISpaceData _space1;
        private ISpaceData _space2;
        private ISpaceData _space3;

        private List<GameObject> _testObjects;

        private DijkstraPortalPathSolver<MockPortalPathLink> _dijkstraPortalPathSolver;

        [SetUp]
        public void Setup()
        {
            _testObjects = new List<GameObject>();

            _space1 = new MockSpaceData
            {
                Name = "Space 1",
                SpaceId = Guid.Parse("1f594522-adb5-4b6d-9818-b7a149987c31")
            };
            _space2 = new MockSpaceData
            {
                Name = "Space 2",
                SpaceId = Guid.Parse("c8375742-56ad-40fc-9e93-cff18e0d4cec")
            };
            _space3 = new MockSpaceData
            {
                Name = "Space 3",
                SpaceId = Guid.Parse("cc9a54ab-cefc-4f4f-9907-9fc116e8aa3f")
            };

            _dijkstraPortalPathSolver = new DijkstraPortalPathSolver<MockPortalPathLink>();
        }

        [TearDown]
        public void TearDown()
        {
            _space1 = null;
            _space2 = null;
            _space3 = null;

            foreach (var go in _testObjects)
            {
                UnityEngine.Object.DestroyImmediate(go);
            }
            _testObjects = null;

            _dijkstraPortalPathSolver = null;
        }

        [Test]
        public void Same_Space_In_Range()
        {
            Assert.IsTrue(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space1, new Vector3(0f, 0f, 1f)),
                2f,
                out var path));

            Assert.AreEqual(path.Path.Length, 1);

            Assert.AreEqual(path.Path[0].SpaceStartPosition, new Vector3(0f, 0f, 0f));
            Assert.AreEqual(path.Path[0].SpaceEndPosition, new Vector3(0f, 0f, 1f));
            Assert.IsNull(path.Path[0].Portal);
            Assert.AreEqual(path.Path[0].Distance, 1);
            Assert.AreEqual(path.Path[0].NodeSpace, _space1);
        }

        [Test]
        public void Same_Space_Out_Of_Range()
        {
            Assert.IsFalse(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space1, new Vector3(0f, 0f, 10f)),
                9.9f,
                out var path));

            Assert.AreEqual(path.Path.Length, 0);
        }

        [Test]
        public void Diff_Space_No_Portals()
        {
            Assert.IsFalse(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space2, new Vector3(0f, 0f, 10f)),
                out var path));

            Assert.AreEqual(path.Path.Length, 0);
        }

        [Test]
        public void Path_Through_Portals()
        {
            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, 20f));

            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, -10f));

            CreateMockLinks(
                _space2, new Vector3(0f, 0f, 25f),
                _space3, new Vector3(0f, 0f, 40f));

            Assert.IsTrue(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space3, new Vector3(0f, 0f, 30f)),
                out var path));

            Assert.AreEqual(path.Path.Length, 3);

            Assert.AreEqual(path.Path[0].NodeSpace, _space1);
            Assert.AreEqual(path.Path[0].SpaceStartPosition, new Vector3(0f, 0f, 0f));
            Assert.AreEqual(path.Path[0].SpaceEndPosition, new Vector3(0f, 0f, 10f));
            Assert.IsNotNull(path.Path[0].Portal);

            Assert.AreEqual(path.Path[1].NodeSpace, _space2);
            Assert.AreEqual(path.Path[1].SpaceStartPosition, new Vector3(0f, 0f, 20f));
            Assert.AreEqual(path.Path[1].SpaceEndPosition, new Vector3(0f, 0f, 25f));
            Assert.IsNotNull(path.Path[1].Portal);

            Assert.AreEqual(path.Path[2].NodeSpace, _space3);
            Assert.AreEqual(path.Path[2].SpaceStartPosition, new Vector3(0f, 0f, 40f));
            Assert.AreEqual(path.Path[2].SpaceEndPosition, new Vector3(0f, 0f, 30f));
            Assert.IsNull(path.Path[2].Portal);

            Assert.AreEqual(path.TotalDistance, 25);
        }

        [Test]
        public void Path_Through_Portals_Out_Of_Range()
        {
            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, 20f));

            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, -10f));

            CreateMockLinks(
                _space2, new Vector3(0f, 0f, 25f),
                _space3, new Vector3(0f, 0f, 40f));

            Assert.IsFalse(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space3, new Vector3(0f, 0f, 60f)),
                30f,
                out var path));

            Assert.AreEqual(path.Path.Length, 0);
        }

        [Test]
        public void No_Path_Through_Portals()
        {
            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, 20f));

            CreateMockLinks(
                _space1, new Vector3(0f, 0f, 10f),
                _space2, new Vector3(0f, 0f, -10f));

            Assert.IsFalse(_dijkstraPortalPathSolver.GetPath(
                new SpacePosition(_space1, new Vector3(0f, 0f, 0f)),
                new SpacePosition(_space3, new Vector3(0f, 0f, 60f)),
                out var path));

            Assert.AreEqual(path.Path.Length, 0);
        }

        private void CreateMockLinks(ISpaceData space1, Vector3 position1, ISpaceData space2, Vector3 position2)
        {
            var portal1GameObject = new GameObject();
            portal1GameObject.transform.position = position1;

            var portal1 = new MockPortal
            {
                Space = space1,
                Transform = portal1GameObject.transform,
                GameObject = portal1GameObject
            };

            var portal2GameObject = new GameObject();
            portal2GameObject.transform.position = position2;

            var portal2 = new MockPortal
            {
                Space = space2,
                Transform = portal2GameObject.transform,
                GameObject = portal2GameObject
            };

            portal1.EndPoint = portal2;
            portal2.EndPoint = portal1;

            _testObjects.Add(portal1GameObject);
            _testObjects.Add(portal2GameObject);

            _dijkstraPortalPathSolver.AddPortal(new MockPortalPathLink
            {
                EntryPortal = portal1,
                ExitPortal = portal2
            });

            _dijkstraPortalPathSolver.AddPortal(new MockPortalPathLink
            {
                EntryPortal = portal2,
                ExitPortal = portal1
            });
        }
    }
}

