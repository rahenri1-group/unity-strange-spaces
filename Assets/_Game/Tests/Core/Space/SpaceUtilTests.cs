using NUnit.Framework;
using System;

namespace Game.Core.Space
{
    public class SpaceUtilTests
    {
        private ISpaceData _space1;
        private ISpaceData _space2;

        [SetUp]
        public void Setup()
        {
            _space1 = new MockSpaceData
            {
                Name = "Space 1",
                SpaceId = Guid.Parse("1f594522-adb5-4b6d-9818-b7a149987c31")
            };

            _space2 = new MockSpaceData
            {
                Name = "Space 2",
                SpaceId = Guid.Parse("c8375742-56ad-40fc-9e93-cff18e0d4cec")
            };
        }

        [TearDown]
        public void TearDown()
        {
            _space1 = null;
            _space2 = null;
        }

        [Test]
        public void Both_Spaces_Null()
        {
            Assert.IsTrue(SpaceUtil.SpaceEquals(null, null));
        }

        [Test]
        public void One_Space_Null()
        {
            Assert.IsFalse(SpaceUtil.SpaceEquals(_space1, null));
            Assert.IsFalse(SpaceUtil.SpaceEquals(null, _space1));
        }

        [Test]
        public void Same_Space_Object()
        {
            Assert.IsTrue(SpaceUtil.SpaceEquals(_space1, _space1));
        }

        [Test]
        public void Different_Space_Object()
        {
            Assert.IsFalse(SpaceUtil.SpaceEquals(_space1, _space2));
        }

        [Test]
        public void Same_Space_Id()
        {
            Assert.IsTrue(SpaceUtil.SpaceEquals(
                _space1, 
                new MockSpaceData
                {
                    Name = "Space 1",
                    SpaceId = Guid.Parse("1f594522-adb5-4b6d-9818-b7a149987c31")
                }));
        }
    }
}
