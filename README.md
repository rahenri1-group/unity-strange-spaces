#  Strange Spaces

A VR haunted house that is dynamically generated (room placement, item spawns, enemy spawns, trap spawns, etc). As time goes on, the 'Haunt Level' goes up triggering enemy spawns, lighting changes, and texture changes.

This repository does not contain art assets for the project. Those are kept in a seperate, private repository to keep the size of the repo small and to avoid any issues with the accidental sharing of purchased assets.

### Noteable Features
* Custom portal system - all door frames are actually portals, allowing for non-Euclidean linking of rooms
* All rooms are fully isolated and dynamically loaded and unload as needed
    * Dedicated render layer for each loaded room
    * Seperate physics scene for each loaded room
    * Entity state is saved on unload and restored on load
* Node based AI behavior tree
* Custom dependency injection system
    * All non-Unity classes (managers, factories, commands, etc) are defined in json files
    * Only scripts that need to directly reference scene objects live in scene hierarchy
* Supports both VR and pancake mode

### Demos
* [Haunted House](https://youtu.be/w4dZRfPoEhA)
* [Portal System](https://youtu.be/4suwoVn4lCU)

### Third Party Libraries Used
* [UniTask](https://github.com/Cysharp/UniTask)
* [Newtonsoft.Json for Unity](https://openupm.com/packages/jillejr.newtonsoft.json-for-unity/)